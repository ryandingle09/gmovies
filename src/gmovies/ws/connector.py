################################################################################
#
# Deprecated (10/21/2016). Remove or delete this file in the future.
# Use the soap_connector.py for the Soap related APIs.
#
################################################################################


import suds
import datetime
import logging
import hashlib
import re
from operator import itemgetter
from collections import defaultdict

from types import *

log = logging.getLogger(__name__)

SCREENING_TYPE = [ None, 'Reserved Seating', 'Guaranteed Seats', 'Free Seating'] 
SEAT_STATUS = ['AVAILABLE', 'TAKEN', 'RESERVED', 'HOUSESEAT']

def enum(**named_values):
    return type('Enum', (), named_values)

class SoapConnector(object):
    def __init__(self, wsdl):
        self.ws_client = suds.client.Client(wsdl)

class WSData(SoapConnector):
    def __init__(self, wsdl):
        super(WSData, self).__init__(wsdl)

        # showing filters = (ALL, NOWSHOWING, COMINGSOON, NEXTATTRACTION)
        self.showing_filters = self.ws_client.factory.create('ShowingFilters')

        # cinema filters = (ALL, REGULAR, IMAX, DC, DIGITAL)
        self.cinema_filters = self.ws_client.factory.create('CinemaFilters')

    def get_movies_nowshowing(self):
        try:
            nowshowing = self.ws_client.service.GetMoviesByShowingType(filter=self.showing_filters.NOWSHOWING)

            return [{'movie_code':movie.Movie_Code, 'movie_title':movie.Movie_Name, \
                     'mtrcb_rating':movie.Mtrcb_Rating, 'running_time': movie.RunningTime, \
                     'genre': movie.Genre, 'casts': unicode(movie.StarringCast), \
                     'synopsis': unicode(movie.Synopsis), 'thumbnail_url': movie.Image, \
                     'banner_url': movie.Image2 } for movie in nowshowing.XMovie]

        except(suds.WebFault, AttributeError) as error:
            log.warn("Error fetching nowshowing: {}".format(error))

    def get_movies_comingsoon(self):
        try:
            comingsoon = self.ws_client.service.GetMoviesByShowingType(filter=self.showing_filters.COMINGSOON)

            return [{'movie_code':movie.Movie_Code, 'movie_title':movie.Movie_Name, \
                     'mtrcb_rating':movie.Mtrcb_Rating, 'running_time': movie.RunningTime, \
                     'genre': movie.Genre, 'casts': unicode(movie.StarringCast), \
                     'synopsis': unicode(movie.Synopsis), 'thumbnail_url': movie.Image, \
                     'banner_url': movie.Image2 } for movie in comingsoon.XMovie]

        except(suds.WebFault, AttributeError) as error:
            log.warn("Error fetching comingsoon: {}".format(error))

    def get_all_movies(self):
        now_showing = self.get_movies_nowshowing()
        coming_soon = self.get_movies_comingsoon()
        all_movies = now_showing + coming_soon
        log.info("Lucky Chinatown All Movies: {}".format(all_movies))
        return all_movies

    def get_branch_theaters(self, theater_code):
        running =  False # shows all schedules on a given date
        cinema_filter = self.cinema_filters.ALL # select all cinema type
        today = datetime.datetime.today().isoformat()
        try:
            theaters = self.ws_client.service.GetBranchesWithSchedules(date=today, filter=cinema_filter, running=running)
            return [ { 'branch_key': theater.Branch_Key, 'branch_code': theater.Branch_Code, 'branch_name': theater.Branch_Name, 'cinema_count': int(theater.Cinema_Count)} for theater in theaters.XBranch]
        except(suds.WebFault, AttributeError) as error:
            log.warn("Error fetching theaters: {}".format(error))

    def get_schedules(self, theater_code, days_delta=None):
        try:            
            branch_key = self.__get_theater(theater_code)[0]['branch_key']
        except (AttributeError, TypeError, Exception) as error:
            log.warn("Can't find {0} Branch. Details: {1}".format(theater_code, error))
        date_range = self.__generate_delta_dates(days_delta)
        movies_with_schedules = self.__get_movies_with_schedules(date_range)

        schedules = []
        log.info("Movie All: {}".format(movies_with_schedules))

        for movies_by_date in movies_with_schedules:
            for m in movies_by_date:
                log.info('Movie Details: {}'.format(m))
                try:
                    sched_data = self.ws_client.service.GetSchedulesByBranchAndMovie(
                            movie_code=m['movie_code'], date=m['show_date'],
                            branch=branch_key, filter=self.cinema_filters.ALL,
                            running=False)
                    
                    scheds = [{'schedule_id': s.MCT_Key,
                            'cinema_code': s.Cinema_Code,
                            'cinema_name': s.Cinema_Name,
                            'screening_type': SCREENING_TYPE[int(s.Screening_Type)],
                            'start_time': s.Start_Time,
                            'end_time': s.End_Time, 'price': s.Price,
                            'allow_purchase': s.Allow_Online_Purchase} for s in sched_data.XSchedule]

                    log.info("Got Scheds: {}".format(scheds))

                    movie_scheds = {'movie_name':m['movie_name'],
                            'movie_code': m['movie_code'],
                            'variant': m['variant'],
                            'schedules': scheds}

                    schedules.append(movie_scheds)

                except(suds.WebFault, AttributeError, Exception) as error:
                    log.warn("Error fetching movies with schedules on date: {0} for movie: {1} Error Details: {2}".format(m['show_date'], m['movie_name'], error))

        return schedules

    def __get_movies_with_schedules(self, date_range):
        movies_with_scheds = []
        for d in date_range:
            try:
                movies = self.ws_client.service.GetMoviesWithSchedules(
                        date=d.isoformat(), filter=self.cinema_filters.ALL,
                        running=False)
                movie_variants = self.__get_movie_variants(d)

                movies_with_scheds.append([{'show_date': d.isoformat(),
                        'movie_code': m.Movie_Code, 'movie_name': m.Movie_Name,
                        'variant': movie_variants.get(m.Movie_Code, ''),
                        'cinema_type': m.CinemaType} for m in movies.XMovie])
            except(suds.WebFault, AttributeError) as error:
                log.warn("Error fetching movies with schedules on date: {0} Error Details: {1}".format(d.isoformat(), error))

        return movies_with_scheds

    def __get_movie_variants(self, show_date):
        variants = {}

        try:
            movies = self.ws_client.service.GetMoviesWithDetails(
                    date=show_date.isoformat())

            for movie in movies.XMovie:
                variants[movie.Movie_Code] = movie.FilmFormat
        except(suds.WebFault, AttributeError) as error:
            log.warn("Error fetching movie variants on date: {0}, Error Details: {1}".format(show_date.isoformat(), error))

        return variants

    def __generate_delta_dates(self, delta):
        today = datetime.datetime.today()
        if delta is not None:
            date_max_delta = today + datetime.timedelta(days=int(delta))
            return [(today + datetime.timedelta(days=distance)) for distance in range((date_max_delta - today).days + 1)]
        else:
            return [today]

    def __get_theater(self, theater_code):
            theaters = self.get_branch_theaters(theater_code)
            return filter(lambda t: t['branch_code'] == theater_code, theaters)


class WSAuth(SoapConnector):
    
    def __init__(self, wsdl):
       super(WSAuth, self).__init__(wsdl)
       self.session_token = None
       self.auth_token = None
       self.member_token = None
       self.client_key = None
       self.__aquire_session_token()

    def __aquire_session_token(self):
        try:
            session_data = self.ws_client.service.GetSessionToken()
            if session_data.Type == 'SESSION' and session_data.Result == 0:
                self.session_token = session_data.Data
                log.info("Acquired SESSION TOKEN: {}".format(self.session_token))
            elif session_data.Result == 1:
                log.warn('Failed to aquire session token! Details: {}'.format(session_data.Message))
        except(suds.WebFault, AttributeError) as error:
            log.warn("Error aquiring session token. Error Details: {}".format(error))

    def get_session_token(self):
        return self.session_token

    def authenticate_session(self, username=None, password=None):
        if username is not None and password is not None:
            encodedpassword = self.__hash_password(username, password)
            try:
                authdata = self.ws_client.service.AuthenticateSession(username=username, password=encodedpassword, session=self.session_token)
                log.info("Auth Type: {0}, Result: {1}, Data: {2}, Message: {3}".format(authdata.Type, authdata.Result, authdata.Data, authdata.Message))
                if authdata.Type == 'AUTHENTICATEUSER' and authdata.Result == 0:
                    return ('success', self.session_token)

                elif authdata.Result == 1:
                    log.warn('Failed to Authenticate! Details: {}'.format(authdata.Message))
                    return ('error', authdata.Message)

            except (suds.WebFault, AttributeError) as error:
                log.warn("Session Authentication failed! Details: {}".format(error))
                return ('error', error)
        else:
            log.warn('Empty username or password!')
            return 'error', 'Cannot authenticate session!'

    def get_session_and_authenticate(self, username=None, password=None):
        status, msg = self.authenticate_session(username, password)
        if status == 'success':
            log.info("Session Authenticated: {}".format(msg))
            return ('success', self.session_token)
        else:
            log.info("Session Authentication failed: {}".format(msg))
            return ('error', msg)

    def authenticate_by_email(self, email=None, password=None, client_ip=None):
        if email is not None and password is not None and client_ip is not None:
            encodedpassword = self.__hash_password(email, password)
            try:
                authdata = self.ws_client.service.AuthenticateMemberByEmail(email=email, password=encodedpassword, session=self.session_token, ipAdress=client_ip )
                log.info("Auth Type: {0}, Result: {1}, Data: {2}, Message: {3}".format(authdata.Type, authdata.Result, authdata.Data, authdata.Message))
                if authdata.Type == 'AUTHENTICATEMEMBER' and authdata.Result == 0:
                    self.member_token = authdata.Data
                elif authdata.Result == 1:
                    log.warn('Failed to Authenticate Member by Email! Details: {}'.format(authdata.Message))
                    return ('error', authdata.Message)
            except (suds.WebFault, AttributeError) as error:
                log.warn("Authentication by Email failed! Details: {}".format(error))
                return ('error', authdata.Message)
        else:
            log.warn('Empty email or password!')
        return ('success', self.member_token)
    
    def __hash_password(self, username, password):
        usernamepasswordhash = hashlib.sha512(username + password).hexdigest().upper()
        log.info("usernamepasswordhash: {}".format(usernamepasswordhash))
        encodedpassword = hashlib.sha512(self.session_token + usernamepasswordhash).hexdigest().upper()
        log.info("Encoded Session/Username/Password: {}".format(encodedpassword))
        return encodedpassword


class WSSeatInfo(SoapConnector):
    """docstring for WSSeatInfo"""
    def __init__(self, wsdl):
        super(WSSeatInfo, self).__init__(wsdl)
        self.schedule_info = {}
        self.seats = []

    def get_scheduleseats_info(self, branch_id, schedule_id):
        try:
            self.seats = []
            ss = self.ws_client.service.GetScheduleSeats(Branch_Key=branch_id, MCT_Key=schedule_id)
            schedule_info = {
                            'branch_id': ss.Branch, 
                            'schedule_id': ss.MCT_Key, 
                            'cinema_code': ss.CinemaCode,
                            'cinema_name': ss.CinemaName,
                            'cinema_type': ss.CinemaType,
                            'movie_code': ss.MovieCode,
                            'movie_name': ss.MovieName,
                            'screening_type': SCREENING_TYPE[ss.ScreeningType],
                            'screening_start': ss.ScreeningStart,
                            'screening_end': ss.ScreeningEnd,
                            'maximum_row': ss.MaxRow,
                            'maximum_column': ss.MaxCol,
                            'capacity': ss.Capacity,
                            'seats_taken': ss.Taken,
                            'seats_available': ss.Available,
                            'seats_reserved': ss.HouseSeats,
                            'percentage': ss.Percentage,
                            'screen_position': ss.ScreenPosition
                            }

            self.schedule_info = schedule_info
            seats_list = ss.Seats

            if seats_list:
                self.seats = [{'seat_id': seat.ID, 'seat_name': seat.Name,
                        'status': SEAT_STATUS[int(seat.Status)],
                        'row': int(seat.Row), 'column': int(seat.Col)} for seat in seats_list.Seat]

            return ('success', self.schedule_info, self.seats)
        except (suds.WebFault, AttributeError, Exception) as error:
            log.warn("Session Authentication failed...")
            log.error(error)

            return ('error', self.schedule_info, self.seats)

    def get_available_seats(self):
        if self.seats is not None:
            return filter(lambda x : x['status'] == 'AVAILABLE', self.seats)

        else:
            return []


#    def get_seatmaps(self, row_reverse=False, col_reverse=False):
#        if self.seats is not None:
#            row_sorted = []
#            row_sorted_seats = sorted(self.seats, key=itemgetter('row'), reverse=row_reverse)
#            rows_keys = sorted(set([seat_row['row'] for seat_row in row_sorted_seats]), reverse=row_reverse)
#            for row in rows_keys:
#                rs = [seats for seats in row_sorted_seats if seats['row'] == row]
#                if rs is not None:
#                    col_sorted_seats = sorted(rs, key=itemgetter('column'), reverse=col_reverse)
#                    row_sorted.append(col_sorted_seats)
#            return row_sorted
#        else:
#            return []

class WSTheaterInfo(SoapConnector):
    """use Transaction WS here"""
    def __init__(self, wsdl):
        super(WSTheaterInfo, self).__init__(wsdl)
        
        
    def get_cinemas(self, branch_key):
        if not branch_key:
            return ('error', 'Branch key is required')
        try:
            cinemas = []
            cinema_data = self.ws_client.service.GetCinemaLookUpKey(Branch_key=branch_key)

            for cinema in cinema_data.CinemaLookUp:
                c = { 'cinema_name': str(cinema.CinemaName),
                        'cinema_code': str(cinema.CinemaCode),
                        'cinema_key': str(cinema.CinemaKey),
                        'max_col': cinema.MaxCol,
                        'max_row': cinema.MaxRow
                    }

                cinemas.append(c)
                
            return ('success', cinemas)

        except(suds.WebFault, AttributeError) as error:
            log.warn("Error fetching cinemas. Error Details: {}".format(error))
            return ('error', '"Error fetching cinemas.')

    def get_seatplan(self, branch_key, cinema_key, row_reverse=False, col_reverse=False):

        if branch_key is None or cinema_key is None:
            return 'error', 'Missing branch key or cinema key'

        status, cinema_data = self.get_cinemas(branch_key)
        if status == 'error':
            return ('error', 'Cannot retrieve seat plan')

        elif status == 'success':
            log.info('Cinema Key: {0} Cinema Data: {1}'.format(cinema_key, cinema_data))
            cdata = [c for c in cinema_data if c['cinema_key'] == str(cinema_key)]
            row_range = range(cdata[0]['max_row'], 0, -1)
            col_range = range(1, cdata[0]['max_col'] + 1)

            log.info("Got Cinema: {0} Rows: {1}, Columns: {2}".format(cdata, row_range, col_range))

        try:
            seatmap_data = self.ws_client.service.GetSeatMap(Branch_Key=branch_key, cinema_key=cinema_key)
            seats = getattr(seatmap_data, 'SeatsMap', None)
                
            if seats is not None:
                row_sorted = []
                row_sorted_seats = sorted(seats, key=itemgetter('Row'), reverse=row_reverse)
                rows_keys = sorted(set([seat_row['Row'] for seat_row in row_sorted_seats]), reverse=row_reverse)
                for row in rows_keys:
                    rs = [seats for seats in row_sorted_seats if seats['Row'] == row]
                    if rs is not None:
                        col_sorted_seats = sorted(rs, key=itemgetter('Col'), reverse=col_reverse)
                        row_sorted += col_sorted_seats
                
                seats_listdict = [ {'seat_id': r.Id, 'seat_name': str(r.Name), 'seat_row': r.Row, 'seat_col': r.Col} for r in row_sorted]
                #log.info('Sorted Seats: {}'.format(seats_listdict))

                seat_name_pattern = re.compile("^[A-Z]{1,2}([0-9]){1,2}$")
                filtered_seats = [ fs for fs in seats_listdict if seat_name_pattern.match(fs['seat_name']) ]
                #log.info("Filtered Seats: {}".format(filtered_seats))
                
                seats_by_row = defaultdict(list)

                for s in filtered_seats:
                    seats_by_row[s['seat_row']].append(s)

                all_seats = []
                cur_seats = []
                filled_rows = []

                log.info("Filling rows empty columns")
                for row, rs  in seats_by_row.items():
                    filled_rows.append(row)
                    cur_seats = self.__add_space(rs, col_range, row)
                    all_seats.append({'row': row, 'row_seats': cur_seats})

                empty_rows = set(row_range) - set(filled_rows)
                log.info('Empty Rows: {}'.format(empty_rows))

                log.info("Filling empty rows")
                for _, erow in enumerate(empty_rows):
                    row_seats = self.__fill_empty_row(erow, col_range)
                    if erow - 1 < 0:
                        all_seats.insert(0, {'row': erow, 'row_seats': row_seats})
                    else:
                        all_seats.insert(erow - 1, {'row': erow, 'row_seats': row_seats})
                return {'seat_count': len(filtered_seats), 'seats': all_seats}

            else:
                return {'seat_count': None, 'seats': None}
                
        except(suds.WebFault, AttributeError, Exception) as error:
            log.warn("Error fetching seatplans. Error Details: {}".format(error))
            return 'error', '"Error fetching seatplans.'


    def __add_space(self, row_seats, col_range, row):
        curr_seats = []
        seat_index = 0
        col_indexes = col_range[:]
        
        while row_seats:
            while col_indexes:
                curr_col= col_indexes.pop(0)
                
                if row_seats:
                    if row_seats[seat_index]['seat_col'] == int(curr_col):
                        curr_seats.append(row_seats.pop(0))
                    else:
                        curr_seats.insert(curr_col, { 'seat_row': row, 'seat_col': curr_col, 'seat_name': "b(" + str(curr_col) + ")", 'seat_id': None })
                        
                elif col_indexes is not None:
                        curr_seats.insert(curr_col, { 'seat_row': row, 'seat_col': curr_col, 'seat_name': "b(" + str(curr_col) + ")", 'seat_id': None })
            else:
                break
        return curr_seats

    def __fill_empty_row(self, row, col_range):
        col_indexes = col_range[:]
        curr_seats = []
        while col_indexes:
            curr_col = col_indexes.pop(0)
            curr_seats.insert(curr_col, { 'seat_row': row, 'seat_col': curr_col, 'seat_name': "b(" + str(curr_col) + ")", 'seat_id': None })

        return curr_seats


class WSUser(SoapConnector):
    def __init__(self, wsdl):
        super(WSUser, self).__init__(wsdl)
        self.client_id = None
        self.client_info = None

    def login(self, email, password):
        STATUS = [None, "VERIFIED", "UNVERIFIED"]
        if email is not None and password is not None:
            try:
                clientdata = self.ws_client.service.LogIn(email=email, password=password)

                if clientdata.Result == 0:
                    self.client_id = clientdata.ClientId
                    self.client_info = { 'firstname': clientdata.FirstName,
                                         'lastname': clientdata.Lastname,
                                         'email': clientdata.Email,
                                         'phone': clientdata.Phonenumber,
                                         'mobile': clientdata.Mobilenumber,
                                         'status': STATUS[clientdata.Status]
                                        }
                elif clientdata.Result == 1:
                    return ('fail', None, None, clientdata.Message)

            except (suds.WebFault, AttributeError, Exception) as error:
                log.warn("Authentication by Email failed! Details: {}".format(error))
                return ('error', None, None, error)
        else:
            log.warn('Empty email or password!')

        return ('success', self.client_id, self.client_info, None)

    def register(self, reg_data):
        log.info("Registration Payload: {}".format(reg_data))

        first_name = reg_data.get('first_name', '')
        middle_name = reg_data.get('middle_name', '')
        last_name = reg_data.get('last_name', '')
        Address = reg_data.get('address', '')
        city = reg_data.get('city', '')
        bdate = reg_data.get('bdate', None)
        mobile_num = reg_data.get('mobile_num', None)
        phone_num = reg_data.get('phone_num', None)
        email = reg_data.get('email', None)
        password = reg_data.get('password', None)

        if (first_name is not None and last_name is not None and bdate is not None and
            mobile_num is not None and email is not None and password is not None): 

            bdate = datetime.datetime.strptime(bdate, '%B %d, %Y').isoformat()

            try:
                regresult = self.ws_client.service.RegisterClient( first_name=first_name,
                                                                    middle_name=middle_name,
                                                                    last_name=last_name,
                                                                    Address=Address,
                                                                    city=city,
                                                                    bdate=bdate,
                                                                    mobile_num=mobile_num,
                                                                    phone_num=phone_num,
                                                                    email=email, 
                                                                    password=password)

                log.info("Result: {0}, Message: {1}".format(regresult.Result, regresult.Message))
                if regresult.Result == 0:
                    _, registration_token = regresult.Message.split("|")
                    return ('success', registration_token)
                elif regresult.Result == 1 or regresult.Result == -1:
                    log.warn('New member registration failed! Details: {}'.format(regresult.Message))
                    return ('fail', regresult.Message)

            except (suds.WebFault, AttributeError, Exception) as error:
                log.warn("New member registration failed! Details: {}".format(error))
                return ('error', 'Registration Failed!')
        else:
            return ('error', 'Invalid parameters')

    def verify(self, registration_token):
        if not registration_token:
            return ('error', 'Missing registration token')
        else:
            try:
                verificationresult = self.ws_client.service.VerifyEmail(token=registration_token)
                log.info("Verification Result: {}".format(verificationresult))

                if verificationresult.Result == 0:
                    return ('success', verificationresult.Message)
                elif verificationresult.Result == 1:
                    log.warn('Email verification failed! Details: {}'.format(verificationresult.Message))
                    return ('error', verificationresult.Message)
            except (suds.WebFault, AttributeError, Exception) as error:
                log.warn("Email verification failed! Details {}".format(error))
                return ('error', 'Email verification failed!')

    def forgot_password(self, session_token, email):
        if not email:
            return ('error', 'Missing email address', None)
        else:
            try:
                wsresult = self.ws_client.service.ForgotPassword(Session=str(session_token), email=str(email))
                if wsresult.Result == 0:
                    msg, new_password = wsresult.Message.split("|")
                    return ('success', new_password, msg)
                elif wsresult.Result == 1:
                    log.warn('Forgot Password Failed! Details: {}'.format(wsresult.Message))
                    return ('error', wsresult.Message, None)
            except (suds.WebFault, AttributeError) as error:
                log.warn("Forgot Password Failed! Details {}".format(error))             
                return ('error', 'Password recovery failed!', None)

    def update_password(self, session_token, email, new_password, old_password):
        if email is None or new_password is None or old_password is None:
            return('error', 'Missing email address/new password/old password')
        else:
            try:
                wsresult = self.ws_client.service.UpdatePassword(Session=str(session_token), userName=str(email), \
                                                                password=str(new_password), oldPass=str(old_password))
                if wsresult.Result == 0:
                    return ('success', wsresult.Message, None)
                elif wsresult.Result == 1:
                    log.warn('Password Update Failed! Details: {}'.format(wsresult.Message))
                    return ('fail', wsresult.Message, None)
            except (suds.WebFault, AttributeError) as error:
                log.warn("Password Update Failed! Details {}".format(error))
                return ('error', 'Password update failed!', None)



class WSTransaction(SoapConnector):
    SEATINGTYPE = enum(FREE_SEATING='Free Seating', GUARANTEED_SEATS='Guaranteed Seats', RESERVED_SEATING='Reserved Seating')

    """docstring for WSTransaction"""
    def __init__(self, tx_wsdl, session_wsdl, account_username, account_password, client_id):
        super(WSTransaction, self).__init__(tx_wsdl)
        self.Session = WSAuth(session_wsdl)

        _, self.auth_token = self.Session.get_session_and_authenticate(account_username, account_password)
        self.client_id = str(client_id)

    def reserve_tx(self, branch_id, schedule_id, seat_list, seating_type=None):
        reservation_data = {}

        if seating_type == self.SEATINGTYPE.GUARANTEED_SEATS or seating_type == self.SEATINGTYPE.FREE_SEATING:
            #assert type(seat_list) is IntType, "Seating type is free seating, was expecting an integer type"
            seatids = {'string': [str(seat_list) for _ in xrange(int(seat_list))] }
            log.info("{0}: {1}".format(seating_type, seatids))

        elif seating_type == self.SEATINGTYPE.RESERVED_SEATING:
            #assert isinstance(seat_list, (list, tuple))
            seatids = {'string': [ str(sid) for sid in seat_list] }
            log.info("{0}: {1}".format(seating_type, seatids))

        try:
            log.info("Payload::session_token: {0} client_id: {1}, branch_id: {2}, schedule_id: {3}, seats_list: {4}".format(self.auth_token, self.client_id, branch_id, schedule_id, seatids))
            reservation_response = self.ws_client.service.CreateTransaction(SessionToken=self.auth_token,
                                                                            ClientId=self.client_id,
                                                                            Branch_Key=branch_id,
                                                                            MCT_Key=schedule_id,
                                                                            Seatlist=seatids,
                                                                            SendSMS=False)
            if reservation_response.Result == 1:
                return ('error', reservation_response.Message)
            elif reservation_response.Result == 0:
                log.info("Reservation response: {}".format(reservation_response.Message))
                reference_no, amount, pin_no = reservation_response.Message.split('|')
                reservation_data = { 'reference_no': reference_no, 'pin_no': pin_no, 'amount': amount }
                log.info("Reservation data: {}".format(reservation_data))
                return ('success', reservation_data)
        except (suds.WebFault, AttributeError) as error:
            log.warn("Seat reservation failed! Details: {}".format(error))
            return ('error', 'Seat Reservation Failed!')

    def finalize_tx(self, payment_details):
        finalize_result = {}

        #payload
        session_token   = self.auth_token
        client_id  = self.client_id
        reference_no    = payment_details.get('reference_no', '')
        amount          = payment_details.get('amount','')
        payment_type    = payment_details.get('payment_type', 1) # 1 - Credit Card :: See documentation 2.4.3.1
        payment_reference = payment_details.get('payment_reference', '1') # Payment reference number from payment gateway.
        pin_number      = payment_details.get('pin_no', '') #Returned data from CreateTransaction() 
        receipt_id      = payment_details.get('receipt_id', '') #ReceiptID returned by payment gateway

        log.info("FINALIZE TX PAYLOAD \n Session: {0}  Client ID: {1} REFNO: {2} Amount: {3} PTYPE: {4} PREF: {5} PIN: {6} RECIEIPTID {7}".format(session_token, client_id, reference_no, amount, payment_type, payment_reference, pin_number, receipt_id))
        try:
            finalize_response = self.ws_client.service.FinalizeTransaction(SessionToken=str(session_token),
                                                                            ClientId=str(client_id),
                                                                            ReferenceNumber=str(reference_no),
                                                                            Amount=float(amount), #decimal
                                                                            PaymentType=int(payment_type),
                                                                            PaymentReference=str(payment_reference),
                                                                            pin=str(pin_number),
                                                                            receiptId=str(receipt_id))
            log.info("Finalize Response: {}".format(finalize_response))

            if finalize_response.Result == 1:
                log.info("Finalize failed! Message: {}".format(finalize_response.Message))
                return ('error', finalize_response.Message)
            elif finalize_response.Result == 0 and finalize_response.Type == "TRANSACTIONAPI":
                log.info("Finalize success! BookingID: {0}, Data: {1}, Message: {2}".format(finalize_response.BookingId, finalize_response.Data, finalize_response.Message))
                return ('success', finalize_response.BookingId)
        except (suds.WebFault, AttributeError, Exception) as error:
            log.warn("Finalize reservation failed! Details: {}".format(error))
            return ('error', 'Finalize Reservation Failed!')
