# WEB SERVICES ENDPOINTS

# CINEMA PARTNER/THIRD PARTY APIs: STAGING
# MEGAWORLD (LUCKY CHINATOWN)
# LCT_BASE_URL_STAGING = 'http://luckychinatowncinemas.com' # OLD BASE URL (LCT)
LCT_BASE_URL_STAGING = 'http://blockbusterseats.com'
LCT_SCHEDULES_API_STAGING = '%s/xyzdataapiws/dataapiws.asmx?wsdl' % LCT_BASE_URL_STAGING
LCT_SESSION_API_STAGING = '%s/xyzservicesws/servicesws.asmx?wsdl' % LCT_BASE_URL_STAGING
LCT_TRANSACTION_API_STAGING = '%s/xyztransactionws/transactionws.asmx?wsdl' % LCT_BASE_URL_STAGING
# SM MALLS
SM_BASE_URL_STAGING = 'http://202.57.62.40:8081'
SM_SCHEDULES_API_STAGING = '%s/DataAPIWS/Dataapiws.asmx?wsdl' % SM_BASE_URL_STAGING
#SM_SESSION_API_STAGING = '%s/ServicesWS/servicesws.asmx?wsdl' % SM_BASE_URL_STAGING
SM_SESSION_API_STAGING = ' https://cms.smcinema.com/stagingservicesws/servicesws.asmx?wsdl'
#SM_TRANSACTION_API_STAGING = '%s/PurchaseWs/Purchasews.asmx?wsdl' % SM_BASE_URL_STAGING
SM_TRANSACTION_API_STAGING = 'https://cms.smcinema.com/StagingPurchaseWSIntegration/purchasews.asmx?wsdl'
SM_EPLUS_API_STAGING = '%s/EplusOnlineWalletWS/eplusws.asmx?wsdl' % SM_BASE_URL_STAGING
# ROBINSONS MALLS
ROBINSONS_BASE_URL_STAGING = 'http://api.robinsonsmovieworld.com'
ROBINSONS_SCHEDULES_API_STAGING = '%s/testdataapiwsintegration/dataapiws.asmx?wsdl' % ROBINSONS_BASE_URL_STAGING
ROBINSONS_SESSION_API_STAGING = '%s/testServiceWS/ServicesWS.asmx?wsdl' % ROBINSONS_BASE_URL_STAGING
ROBINSONS_TRANSACTION_API_STAGING = '%s/testtransactionapiwsintegration/transactionws.asmx?wsdl' % ROBINSONS_BASE_URL_STAGING

# CINEMA PARTNER/THIRD PARTY APIs: PRODUCTION
# MEGAWORLD (LUCKY CHINATOWN)
# LCT_BASE_URL_PRODUCTION = 'http://luckychinatowncinemas.com' # OLD BASE URL (LCT)
LCT_BASE_URL_PRODUCTION = 'http://blockbusterseats.com'
LCT_SCHEDULES_API_PRODUCTION = '%s/dataapiws2/dataapiws.asmx?wsdl' % LCT_BASE_URL_PRODUCTION
LCT_SESSION_API_PRODUCTION = '%s/servicesws/servicesws.asmx?wsdl' % LCT_BASE_URL_PRODUCTION
LCT_TRANSACTION_API_PRODUCTION = '%s/TransactionWSIntegration/TransactionWS.asmx?wsdl' % LCT_BASE_URL_PRODUCTION
# SM MALLS
SM_BASE_URL_PRODUCTION = 'http://tickets.smcinema.com'
SM_SCHEDULES_API_PRODUCTION = '%s/DataApiWSIntegration/DataAPIWS.asmx?wsdl' % SM_BASE_URL_PRODUCTION
SM_SESSION_API_PRODUCTION = '%s/ServicesWS/ServicesWS.asmx?wsdl' % SM_BASE_URL_PRODUCTION
SM_TRANSACTION_API_PRODUCTION = '%s/PurchaseWSIntegration/PurchaseWS.asmx?wsdl' % SM_BASE_URL_PRODUCTION
SM_EPLUS_API_PRODUCTION = '%s/EplusOnlineWalletWS/eplusWS.asmx?wsdl' % SM_BASE_URL_PRODUCTION
# ROBINSONS MALLS
ROBINSONS_BASE_URL_PRODUCTION = 'http://api.robinsonsmovieworld.com'
ROBINSONS_SCHEDULES_API_PRODUCTION = '%s/dataapiwsintegration/dataapiws.asmx?wsdl' % ROBINSONS_BASE_URL_PRODUCTION
ROBINSONS_SESSION_API_PRODUCTION = '%s/servicews/servicesws.asmx?wsdl' % ROBINSONS_BASE_URL_PRODUCTION
ROBINSONS_TRANSACTION_API_PRODUCTION = '%s/transactionapiintegration/transactionws.asmx?wsdl' % ROBINSONS_BASE_URL_PRODUCTION

# PAYNAMICS CCService API: STAGING
PAYNAMICS_CCSERVICE_URL_STAGING = 'https://testpti.payserv.net/paygate/ccservice.asmx?wsdl'
PAYNAMICS_TARGETNAMESPACE_STAGING = 'http://paygate.paynamics.net/'
PAYNAMICS_CCSERVICE_HEADER_HOST_STAGING = 'testpti.payserv.net'
PAYNAMICS_CCSERVICE_HEADER_ACTION_REBILL_WTOKEN_STAGING = 'http://paygate.paynamics.net/rebill_with_token'
PAYNAMICS_CCSERVICE_HEADER_ACTION_REBILL_WTOKEN_3D_STAGING = 'http://paygate.paynamics.net/rebill_with_token_3d'
PAYNAMICS_CCSERVICE_HEADER_ACTION_REFUND_STAGING = 'http://paygate.paynamics.net/refund'
PAYNAMICS_CCSERVICE_HEADER_ACTION_REVERSAL_STAGING = 'http://paygate.paynamics.net/reversal'
PAYNAMICS_CCSERVICE_XML_REBILL_WTOKEN_STAGING = u"""<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns1="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns0="http://paygate.paynamics.net/">
<SOAP-ENV:Header/>
<ns1:Body>
<ns0:rebill_with_token>
<ns0:merchantid>{0}</ns0:merchantid>
<ns0:request_id>{1}</ns0:request_id>
<ns0:ip_address>{2}</ns0:ip_address>
<ns0:org_trxid>{3}</ns0:org_trxid>
<ns0:token_id>{4}</ns0:token_id>
<ns0:trx_type>{5}</ns0:trx_type>
<ns0:amount>{6}</ns0:amount>
<ns0:notification_url>{7}</ns0:notification_url>
<ns0:response_url>{8}</ns0:response_url>
<ns0:signature>{9}</ns0:signature>
</ns0:rebill_with_token>
</ns1:Body>
</SOAP-ENV:Envelope>"""
PAYNAMICS_CCSERVICE_XML_REBILL_WTOKEN_3D_STAGING = u"""<?xml version="1.0" encoding="UTF-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns1="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns0="http://paygate.paynamics.net/">
<SOAP-ENV:Header/>
<ns1:Body>
<ns0:rebill_with_token_3d>
<ns0:merchantid>{0}</ns0:merchantid>
<ns0:request_id>{1}</ns0:request_id>
<ns0:ip_address>{2}</ns0:ip_address>
<ns0:org_trxid>{3}</ns0:org_trxid>
<ns0:token_id>{4}</ns0:token_id>
<ns0:trx_type>{5}</ns0:trx_type>
<ns0:amount>{6}</ns0:amount>
<ns0:notification_url>{7}</ns0:notification_url>
<ns0:response_url>{8}</ns0:response_url>
<ns0:secure3d_policy>{9}</ns0:secure3d_policy>
<ns0:signature>{10}</ns0:signature>
</ns0:rebill_with_token_3d>
</ns1:Body>
</SOAP-ENV:Envelope>"""
PAYNAMICS_CCSERVICE_XML_REFUND_STAGING = u"""<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns1="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns0="http://paygate.paynamics.net/">
<SOAP-ENV:Header/>
<ns1:Body>
<ns0:refund>
<ns0:merchantid>{0}</ns0:merchantid>
<ns0:request_id>{1}</ns0:request_id>
<ns0:ip_address>{2}</ns0:ip_address>
<ns0:org_trxid>{3}</ns0:org_trxid>
<ns0:amount>{4}</ns0:amount>
<ns0:notification_url>{5}</ns0:notification_url>
<ns0:response_url>{6}</ns0:response_url>
<ns0:signature>{7}</ns0:signature>
</ns0:refund>
</ns1:Body>
</SOAP-ENV:Envelope>"""
PAYNAMICS_CCSERVICE_XML_REVERSAL_STAGING = u"""<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns1="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns0="http://paygate.paynamics.net/">
<SOAP-ENV:Header/>
<ns1:Body>
<ns0:reversal>
<ns0:merchantid>{0}</ns0:merchantid>
<ns0:request_id>{1}</ns0:request_id>
<ns0:ip_address>{2}</ns0:ip_address>
<ns0:org_trxid>{3}</ns0:org_trxid>
<ns0:amount>{4}</ns0:amount>
<ns0:notification_url>{5}</ns0:notification_url>
<ns0:response_url>{6}</ns0:response_url>
<ns0:signature>{7}</ns0:signature>
</ns0:reversal>
</ns1:Body>
</SOAP-ENV:Envelope>"""

# PAYNAMICS CCService API: PRODUCTION
PAYNAMICS_CCSERVICE_URL_PRODUCTION = 'https://ptipaygate.paynamics.net/ccservice/ccservice.asmx?wsdl'
# PAYNAMICS_CCSERVICE_URL_PRODUCTION = 'https://paygate.paynamics.net/ccservice/ccservice.asmx?wsdl'
PAYNAMICS_TARGETNAMESPACE_PRODUCTION = 'http://paygate.paynamics.net/'
PAYNAMICS_CCSERVICE_HEADER_HOST_PRODUCTION = 'ptipaygate.paynamics.net'
PAYNAMICS_CCSERVICE_HEADER_ACTION_REBILL_WTOKEN_PRODUCTION = 'https://ptipaygate.paynamics.net/ccservice/rebill_with_token'
PAYNAMICS_CCSERVICE_HEADER_ACTION_REBILL_WTOKEN_3D_PRODUCTION = 'https://ptipaygate.paynamics.net/ccservice/rebill_with_token_3d'
PAYNAMICS_CCSERVICE_HEADER_ACTION_REFUND_PRODUCTION = 'https://ptipaygate.paynamics.net/ccservice/refund'
PAYNAMICS_CCSERVICE_HEADER_ACTION_REVERSAL_PRODUCTION = 'https://ptipaygate.paynamics.net/ccservice/reversal'
PAYNAMICS_CCSERVICE_XML_REBILL_WTOKEN_PRODUCTION = u"""<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns1="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns0="https://ptipaygate.paynamics.net/ccservice">
<SOAP-ENV:Header/>
<ns1:Body>
<ns0:rebill_with_token>
<ns0:merchantid>{0}</ns0:merchantid>
<ns0:request_id>{1}</ns0:request_id>
<ns0:ip_address>{2}</ns0:ip_address>
<ns0:org_trxid>{3}</ns0:org_trxid>
<ns0:token_id>{4}</ns0:token_id>
<ns0:trx_type>{5}</ns0:trx_type>
<ns0:amount>{6}</ns0:amount>
<ns0:notification_url>{7}</ns0:notification_url>
<ns0:response_url>{8}</ns0:response_url>
<ns0:signature>{9}</ns0:signature>
</ns0:rebill_with_token>
</ns1:Body>
</SOAP-ENV:Envelope>"""
PAYNAMICS_CCSERVICE_XML_REBILL_WTOKEN_3D_PRODUCTION = u"""<?xml version="1.0" encoding="UTF-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns1="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns0="https://ptipaygate.paynamics.net/ccservice">
<SOAP-ENV:Header/>
<ns1:Body>
<ns0:rebill_with_token_3d>
<ns0:merchantid>{0}</ns0:merchantid>
<ns0:request_id>{1}</ns0:request_id>
<ns0:ip_address>{2}</ns0:ip_address>
<ns0:org_trxid>{3}</ns0:org_trxid>
<ns0:token_id>{4}</ns0:token_id>
<ns0:trx_type>{5}</ns0:trx_type>
<ns0:amount>{6}</ns0:amount>
<ns0:notification_url>{7}</ns0:notification_url>
<ns0:response_url>{8}</ns0:response_url>
<ns0:secure3d_policy>{9}</ns0:secure3d_policy>
<ns0:signature>{10}</ns0:signature>
</ns0:rebill_with_token_3d>
</ns1:Body>
</SOAP-ENV:Envelope>"""
PAYNAMICS_CCSERVICE_XML_REFUND_PRODUCTION = u"""<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns1="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns0="https://ptipaygate.paynamics.net/ccservice">
<SOAP-ENV:Header/>
<ns1:Body>
<ns0:refund>
<ns0:merchantid>{0}</ns0:merchantid>
<ns0:request_id>{1}</ns0:request_id>
<ns0:ip_address>{2}</ns0:ip_address>
<ns0:org_trxid>{3}</ns0:org_trxid>
<ns0:amount>{4}</ns0:amount>
<ns0:notification_url>{5}</ns0:notification_url>
<ns0:response_url>{6}</ns0:response_url>
<ns0:signature>{7}</ns0:signature>
</ns0:refund>
</ns1:Body>
</SOAP-ENV:Envelope>"""
PAYNAMICS_CCSERVICE_XML_REVERSAL_PRODUCTION = u"""<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns1="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns0="https://ptipaygate.paynamics.net/ccservice">
<SOAP-ENV:Header/>
<ns1:Body>
<ns0:reversal>
<ns0:merchantid>{0}</ns0:merchantid>
<ns0:request_id>{1}</ns0:request_id>
<ns0:ip_address>{2}</ns0:ip_address>
<ns0:org_trxid>{3}</ns0:org_trxid>
<ns0:amount>{4}</ns0:amount>
<ns0:notification_url>{5}</ns0:notification_url>
<ns0:response_url>{6}</ns0:response_url>
<ns0:signature>{7}</ns0:signature>
</ns0:reversal>
</ns1:Body>
</SOAP-ENV:Envelope>"""

# PAYNAMICS PNX QUERY API: STAGING
PAYNAMICS_PNXQUERY_URL_STAGING = 'https://testpti.payserv.net/pnxquery/queryservice.asmx?wsdl'
PAYNAMICS_PNXQUERY_TARGETNAMESPACE_STAGING = 'https://testpti.payserv.net/pnxquery'
PAYNAMICS_PNXQUERY_HEADER_HOST_STAGING = 'testpti.payserv.net'
PAYNAMICS_PNXQUERY_HEADER_ACTION_QUERY_STAGING = 'https://testpti.payserv.net/pnxquery/query'
PAYNAMICS_PNXQUERY_XML_QUERY_STAGING = u"""<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:ns1="https://testpti.payserv.net/pnxquery" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns0="http://schemas.xmlsoap.org/soap/envelope/">
<SOAP-ENV:Header/>
<ns0:Body>
<ns1:query>
<ns1:merchantid>{0}</ns1:merchantid>
<ns1:request_id>{1}</ns1:request_id>
<ns1:org_trxid>{2}</ns1:org_trxid>
<ns1:org_trxid2>{3}</ns1:org_trxid2>
<ns1:signature>{4}</ns1:signature>
</ns1:query>
</ns0:Body>
</SOAP-ENV:Envelope>"""

# PAYNAMICS PNX QUERY API: PRODUCTION
PAYNAMICS_PNXQUERY_URL_PRODUCTION = 'https://ptiservice.paynamics.net/pnxquery/queryservice.asmx?wsdl'
PAYNAMICS_PNXQUERY_TARGETNAMESPACE_PRODUCTION = 'http://tempuri.org/'
PAYNAMICS_PNXQUERY_HEADER_HOST_PRODUCTION = 'ptiservice.paynamics.net'
PAYNAMICS_PNXQUERY_HEADER_ACTION_QUERY_PRODUCTION = 'https://ptiservice.paynamics.net/pnxquery/query'
PAYNAMICS_PNXQUERY_XML_QUERY_PRODUCTION = u"""<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:ns1="https://ptiservice.paynamics.net/pnxquery" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns0="http://schemas.xmlsoap.org/soap/envelope/">
<SOAP-ENV:Header/>
<ns0:Body>
<ns1:query>
<ns1:merchantid>{0}</ns1:merchantid>
<ns1:request_id>{1}</ns1:request_id>
<ns1:org_trxid>{2}</ns1:org_trxid>
<ns1:org_trxid2>{3}</ns1:org_trxid2>
<ns1:signature>{4}</ns1:signature>
</ns1:query>
</ns0:Body>
</SOAP-ENV:Envelope>"""

###############
#
# SET THIS TO PRODUCTION WHEN READY!
#
###############

from gmovies.settings import IS_STAGING

if IS_STAGING:
    # STAGING SETTINGS
    LCT_SCHEDULES_API = LCT_SCHEDULES_API_STAGING
    LCT_SESSION_API = LCT_SESSION_API_STAGING
    LCT_TRANSACTION_API = LCT_TRANSACTION_API_STAGING
    SM_SCHEDULES_API = SM_SCHEDULES_API_STAGING
    SM_SESSION_API = SM_SESSION_API_STAGING
    SM_TRANSACTION_API = SM_TRANSACTION_API_STAGING
    SM_EPLUS_API = SM_EPLUS_API_STAGING
    ROBINSONS_SCHEDULES_API = ROBINSONS_SCHEDULES_API_STAGING
    ROBINSONS_SESSION_API = ROBINSONS_SESSION_API_STAGING
    ROBINSONS_TRANSACTION_API = ROBINSONS_TRANSACTION_API_STAGING
    PAYNAMICS_CCSERVICE_URL = PAYNAMICS_CCSERVICE_URL_STAGING
    PAYNAMICS_TARGETNAMESPACE = PAYNAMICS_TARGETNAMESPACE_STAGING
    PAYNAMICS_PNXQUERY_URL = PAYNAMICS_PNXQUERY_URL_STAGING
    PAYNAMICS_PNXQUERY_TARGETNAMESPACE = PAYNAMICS_PNXQUERY_TARGETNAMESPACE_STAGING
    PAYNAMICS_CCSERVICE_HEADER_HOST = PAYNAMICS_CCSERVICE_HEADER_HOST_STAGING
    PAYNAMICS_CCSERVICE_HEADER_ACTION_REBILL_WTOKEN = PAYNAMICS_CCSERVICE_HEADER_ACTION_REBILL_WTOKEN_STAGING
    PAYNAMICS_CCSERVICE_HEADER_ACTION_REBILL_WTOKEN_3D = PAYNAMICS_CCSERVICE_HEADER_ACTION_REBILL_WTOKEN_3D_STAGING
    PAYNAMICS_CCSERVICE_HEADER_ACTION_REFUND = PAYNAMICS_CCSERVICE_HEADER_ACTION_REFUND_STAGING
    PAYNAMICS_CCSERVICE_HEADER_ACTION_REVERSAL = PAYNAMICS_CCSERVICE_HEADER_ACTION_REVERSAL_STAGING
    PAYNAMICS_CCSERVICE_XML_REBILL_WTOKEN = PAYNAMICS_CCSERVICE_XML_REBILL_WTOKEN_STAGING
    PAYNAMICS_CCSERVICE_XML_REBILL_WTOKEN_3D = PAYNAMICS_CCSERVICE_XML_REBILL_WTOKEN_3D_STAGING
    PAYNAMICS_CCSERVICE_XML_REFUND = PAYNAMICS_CCSERVICE_XML_REFUND_STAGING
    PAYNAMICS_CCSERVICE_XML_REVERSAL = PAYNAMICS_CCSERVICE_XML_REVERSAL_STAGING
    PAYNAMICS_PNXQUERY_HEADER_HOST = PAYNAMICS_PNXQUERY_HEADER_HOST_STAGING
    PAYNAMICS_PNXQUERY_HEADER_ACTION_QUERY = PAYNAMICS_PNXQUERY_HEADER_ACTION_QUERY_STAGING
    PAYNAMICS_PNXQUERY_XML_QUERY = PAYNAMICS_PNXQUERY_XML_QUERY_STAGING
else:
    # PRODUCTION SETTINGS
    LCT_SCHEDULES_API = LCT_SCHEDULES_API_PRODUCTION
    LCT_SESSION_API = LCT_SESSION_API_PRODUCTION
    LCT_TRANSACTION_API = LCT_TRANSACTION_API_PRODUCTION
    SM_SCHEDULES_API = SM_SCHEDULES_API_PRODUCTION
    SM_SESSION_API = SM_SESSION_API_PRODUCTION
    SM_TRANSACTION_API = SM_TRANSACTION_API_PRODUCTION
    SM_EPLUS_API = SM_EPLUS_API_PRODUCTION
    ROBINSONS_SCHEDULES_API = ROBINSONS_SCHEDULES_API_PRODUCTION
    ROBINSONS_SESSION_API = ROBINSONS_SESSION_API_PRODUCTION
    ROBINSONS_TRANSACTION_API = ROBINSONS_TRANSACTION_API_PRODUCTION
    PAYNAMICS_CCSERVICE_URL = PAYNAMICS_CCSERVICE_URL_PRODUCTION
    PAYNAMICS_TARGETNAMESPACE = PAYNAMICS_TARGETNAMESPACE_PRODUCTION
    PAYNAMICS_PNXQUERY_URL = PAYNAMICS_PNXQUERY_URL_PRODUCTION
    PAYNAMICS_PNXQUERY_TARGETNAMESPACE = PAYNAMICS_PNXQUERY_TARGETNAMESPACE_PRODUCTION
    PAYNAMICS_CCSERVICE_HEADER_HOST = PAYNAMICS_CCSERVICE_HEADER_HOST_PRODUCTION
    PAYNAMICS_CCSERVICE_HEADER_ACTION_REBILL_WTOKEN = PAYNAMICS_CCSERVICE_HEADER_ACTION_REBILL_WTOKEN_PRODUCTION
    PAYNAMICS_CCSERVICE_HEADER_ACTION_REBILL_WTOKEN_3D = PAYNAMICS_CCSERVICE_HEADER_ACTION_REBILL_WTOKEN_3D_PRODUCTION
    PAYNAMICS_CCSERVICE_HEADER_ACTION_REFUND = PAYNAMICS_CCSERVICE_HEADER_ACTION_REFUND_PRODUCTION
    PAYNAMICS_CCSERVICE_HEADER_ACTION_REVERSAL = PAYNAMICS_CCSERVICE_HEADER_ACTION_REVERSAL_PRODUCTION
    PAYNAMICS_CCSERVICE_XML_REBILL_WTOKEN = PAYNAMICS_CCSERVICE_XML_REBILL_WTOKEN_PRODUCTION
    PAYNAMICS_CCSERVICE_XML_REBILL_WTOKEN_3D = PAYNAMICS_CCSERVICE_XML_REBILL_WTOKEN_3D_PRODUCTION
    PAYNAMICS_CCSERVICE_XML_REFUND = PAYNAMICS_CCSERVICE_XML_REFUND_PRODUCTION
    PAYNAMICS_CCSERVICE_XML_REVERSAL = PAYNAMICS_CCSERVICE_XML_REVERSAL_PRODUCTION
    PAYNAMICS_PNXQUERY_HEADER_HOST = PAYNAMICS_PNXQUERY_HEADER_HOST_PRODUCTION
    PAYNAMICS_PNXQUERY_HEADER_ACTION_QUERY = PAYNAMICS_PNXQUERY_HEADER_ACTION_QUERY_PRODUCTION
    PAYNAMICS_PNXQUERY_XML_QUERY = PAYNAMICS_PNXQUERY_XML_QUERY_PRODUCTION