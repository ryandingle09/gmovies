import datetime
import hashlib
import logging
import re
import suds
import requests
import urllib
import traceback

from gmovies.tx import query
from operator import itemgetter
from types import *
from lxml import etree
from gmovies.models import Movie, Schedule
from gmovies.util import admin
from gmovies.feeds.util import map_movies
from gmovies import orgs

from endpoints import (PAYNAMICS_CCSERVICE_HEADER_HOST, PAYNAMICS_CCSERVICE_HEADER_ACTION_REBILL_WTOKEN,
                       PAYNAMICS_CCSERVICE_HEADER_ACTION_REBILL_WTOKEN_3D, PAYNAMICS_CCSERVICE_HEADER_ACTION_REFUND,
                       PAYNAMICS_CCSERVICE_HEADER_ACTION_REVERSAL, PAYNAMICS_CCSERVICE_XML_REBILL_WTOKEN,
                       PAYNAMICS_CCSERVICE_XML_REBILL_WTOKEN_3D, PAYNAMICS_CCSERVICE_XML_REFUND,
                       PAYNAMICS_CCSERVICE_XML_REVERSAL, PAYNAMICS_PNXQUERY_HEADER_HOST,
                       PAYNAMICS_PNXQUERY_HEADER_ACTION_QUERY, PAYNAMICS_PNXQUERY_XML_QUERY)

log = logging.getLogger(__name__)

SCREENING_TYPE = {'1': 'Reserved Seating', '2': 'Guaranteed Seats', '3': 'Free Seating'}
SEAT_STATUS = {'0': 'AVAILABLE', '1': 'TAKEN', '2': 'RESERVED', '3': 'HOUSESEAT'}
MOVIES_SHOWING_TYPE = {'ALL': 0, 'NOWSHOWING': 1, 'NEXTATTRACTION': 2, 'COMINGSOON': 3}


def enum(**named_values):
    return type('Enum', (), named_values)


class SoapConnector(object):
    def __init__(self, wsdl):
        self.ws_client = suds.client.Client(wsdl)


class DataWSMGi(SoapConnector):
    def __init__(self, wsdl):
        super(DataWSMGi, self).__init__(wsdl)
        self.showing_filters = self.ws_client.factory.create('ShowingFilters') # showing filters = (ALL, NOWSHOWING, COMINGSOON, NEXTATTRACTION)
        self.cinema_filters = self.ws_client.factory.create('CinemaFilters') # cinema filters = (ALL, REGULAR, IMAX, DC, DIGITAL)

    def get_movies_by_showing_type(self, showing_type, theater_movie_code):
        results = []

        try:
            if showing_type == 'NOWSHOWING':
                movies = self.ws_client.service.GetMoviesByShowingType(filter=self.showing_filters.NOWSHOWING)
            elif showing_type == 'COMINGSOON':
                movies = self.ws_client.service.GetMoviesByShowingType(filter=self.showing_filters.COMINGSOON)
            elif showing_type == 'NEXTATTRACTION':
                movies = self.ws_client.service.GetMoviesByShowingType(filter=self.showing_filters.NEXTATTRACTION)
            else:
                movies = self.ws_client.service.GetMoviesByShowingType(filter=self.showing_filters.ALL)

            movies_xmovie = getattr(movies, 'XMovie', [])

            for movie in movies_xmovie:
                movie_code = getattr(movie, 'Movie_Code', None)
                # fix ascii conversion errors
                movie_name = getattr(movie, 'Movie_Name', None).replace(u"\u2019", "'").replace(u"\u2018", "'")
                cast = getattr(movie, 'StarringCast', '')
                running_time = getattr(movie, 'RunningTime', '0.0')
                mtrcb_rating = getattr(movie, 'Mtrcb_Rating', '')
                synopsis = getattr(movie, 'Synopsis', '')
                genre = getattr(movie, 'Genre', '')

                if movie_code is None:
                    log.warn("SKIP!, get_movies_by_showing_type, attribute Movie_Code doesn't exist...")

                    continue

                if movie_name is None:
                    log.warn("SKIP!, get_movies_by_showing_type, attribute Movie_Name doesn't exist...")

                    continue

                movie_dict = {}
                movie_dict['id'] = '%s::%s' % (theater_movie_code, movie_code)
                movie_dict['movie_title'] = movie_name
                movie_dict['cast'] = self.__parse_cast(unicode(cast))
                movie_dict['runtime_mins'] = float(running_time)
                movie_dict['advisory_rating'] = mtrcb_rating
                movie_dict['synopsis'] = unicode(synopsis)
                movie_dict['genre'] = genre

                # modified for force schedule movie mappings
                api_title = movie_name
                api_movie_id = '%s::%s' % (theater_movie_code, movie_code)
                
                uuid = str(orgs.SM_MALLS)

                if 'RMW' in theater_movie_code:
                    uuid = str(orgs.ROBINSONS_MALLS) 
                
                if 'MGW' in theater_movie_code:
                    uuid = str(orgs.MEGAWORLD_MALLS) 
                
                map_movies(api_title, api_movie_id, uuid)

                results.append(movie_dict)
        except Exception, e:
            log.warn("ERROR!, GetMoviesByShowingType with showing_type: %s..." % showing_type)
            log.error(e)

        log.debug('movie_list: %s' % (results))
        return results

    def get_movies_by_branch(self, branch_key, show_date, theater_movie_code):
        results = []

        try:
            movies = self.ws_client.service.GetMoviesByBranch(date=show_date.isoformat(),
                    branch=branch_key, filter=self.cinema_filters.ALL, running=False)
            movies_xmovie = getattr(movies, 'XMovie', [])

            for movie in movies_xmovie:
                movie_code = getattr(movie, 'Movie_Code', None)
                # fix ascii conversion errors
                movie_name = getattr(movie, 'Movie_Name', None).replace(u"\u2019", "'").replace(u"\u2018", "'")
                cast = getattr(movie, 'StarringCast', '')
                running_time = getattr(movie, 'RunningTime', '0.0')
                mtrcb_rating = getattr(movie, 'Mtrcb_Rating', '')
                synopsis = getattr(movie, 'Synopsis', '')
                genre = getattr(movie, 'Genre', '')
                film_format = getattr(movie, 'FilmFormat', '')

                if movie_code is None:
                    log.warn("SKIP!, get_movies_by_branch, attribute Movie_Code doesn't exist...")

                    continue

                if movie_name is None:
                    log.warn("SKIP!, get_movies_by_branch, attribute Movie_Name doesn't exist...")

                    continue

                movie_dict = {}
                movie_dict['id'] = '%s::%s' % (theater_movie_code, movie_code)
                movie_dict['movie_title'] = movie_name
                movie_dict['movie_code'] = movie_code
                movie_dict['cast'] = self.__parse_cast(unicode(cast))
                movie_dict['runtime_mins'] = float(running_time)
                movie_dict['advisory_rating'] = mtrcb_rating
                movie_dict['synopsis'] = unicode(synopsis)
                movie_dict['genre'] = genre
                movie_dict['film_format'] = film_format

                results.append(movie_dict)
        except Exception, e:
            log.warn("ERROR!, GetMoviesByBranch with branch_key: %s..." % branch_key)
            log.error(e)

        return results

    # Not existing in MGi SM MALLS APIs.
    def get_movies_with_schedules(self, show_date, theater_movie_code):
        results = []

        try:
            movies = self.ws_client.service.GetMoviesWithSchedules(date=show_date.isoformat(),
                    filter=self.cinema_filters.ALL, running=False)
            movies_xmovie = getattr(movies, 'XMovie', [])

            for movie in movies_xmovie:
                movie_code = getattr(movie, 'Movie_Code', None)
                movie_name = getattr(movie, 'Movie_Name', None)
                cast = getattr(movie, 'StarringCast', '')
                running_time = getattr(movie, 'RunningTime', '0.0')
                mtrcb_rating = getattr(movie, 'Mtrcb_Rating', '')
                synopsis = getattr(movie, 'Synopsis', '')
                genre = getattr(movie, 'Genre', '')

                if movie_code is None:
                    log.warn("SKIP!, get_movies_with_schedules, attribute Movie_Code doesn't exist...")

                    continue

                if movie_name is None:
                    log.warn("SKIP!, get_movies_with_schedules, attribute Movie_Name doesn't exist...")

                    continue

                movie_dict = {}
                movie_dict['id'] = '%s::%s' % (theater_movie_code, movie_code)
                movie_dict['movie_title'] = movie_name
                movie_dict['cast'] = self.__parse_cast(unicode(cast))
                movie_dict['runtime_mins'] = float(running_time)
                movie_dict['advisory_rating'] = mtrcb_rating
                movie_dict['synopsis'] = unicode(synopsis)
                movie_dict['genre'] = genre

                results.append(movie_dict)
        except Exception, e:
            log.warn("ERROR!, GetMoviesWithSchedules...")
            log.error(e)

        return results

    def get_movies_with_schedules2(self, showing_type, theater_movie_code, days_delta=1):
        results = []
        start_date = datetime.datetime.today()
        end_date = start_date + datetime.timedelta(days=days_delta)

        try:
            if showing_type == 'NOWSHOWING':
                movies = self.ws_client.service.GetMoviesWithSchedules2(start_date=start_date.isoformat(),
                        end_date=end_date.isoformat(), cinema_type=self.cinema_filters.ALL,
                        showing_type=self.showing_filters.NOWSHOWING, running=False)
            elif showing_type == 'COMINGSOON':
                movies = self.ws_client.service.GetMoviesWithSchedules2(start_date=start_date.isoformat(),
                        end_date=end_date.isoformat(), cinema_type=self.cinema_filters.ALL,
                        showing_type=self.showing_filters.COMINGSOON, running=False)
            elif showing_type == 'NEXTATTRACTION':
                movies = self.ws_client.service.GetMoviesWithSchedules2(start_date=start_date.isoformat(),
                        end_date=end_date.isoformat(), cinema_type=self.cinema_filters.ALL,
                        showing_type=self.showing_filters.NEXTATTRACTION, running=False)
            else:
                movies = self.ws_client.service.GetMoviesWithSchedules2(start_date=start_date.isoformat(),
                        end_date=end_date.isoformat(), cinema_type=self.cinema_filters.ALL,
                        showing_type=self.showing_filters.ALL, running=False)

            movies_xmovie = getattr(movies, 'XMovie', [])

            for movie in movies_xmovie:
                movie_code = getattr(movie, 'Movie_Code', None)
                movie_name = getattr(movie, 'Movie_Name', None)
                cast = getattr(movie, 'Cast', '')
                running_time = getattr(movie, 'RunningTime', '0.0')
                mtrcb_rating = getattr(movie, 'Mtrcb_Rating', '')
                synopsis = getattr(movie, 'Synopsis', '')
                genre = getattr(movie, 'Genre', '')

                if movie_code is None:
                    log.warn("SKIP!, get_movies_with_schedules2, attribute Movie_Code doesn't exist...")

                    continue

                if movie_name is None:
                    log.warn("SKIP!, get_movies_with_schedules2, attribute Movie_Name doesn't exist...")

                    continue

                movie_dict = {}
                movie_dict['id'] = '%s::%s' % (theater_movie_code, movie_code)
                movie_dict['movie_title'] = movie_name
                movie_dict['cast'] = self.__parse_cast(unicode(cast))
                movie_dict['runtime_mins'] = float(running_time)
                movie_dict['advisory_rating'] = mtrcb_rating
                movie_dict['synopsis'] = unicode(synopsis)
                movie_dict['genre'] = genre

                # modified for force schedule movie mappings
                api_title = movie_name
                api_movie_id = '%s::%s' % (theater_movie_code, movie_code)
                
                uuid = str(orgs.SM_MALLS)

                if 'RMW' in theater_movie_code:
                    uuid = str(orgs.ROBINSONS_MALLS) 
                
                if 'MGW' in theater_movie_code:
                    uuid = str(orgs.MEGAWORLD_MALLS) 
                
                map_movies(api_title, api_movie_id, uuid)

                results.append(movie_dict)
        except Exception, e:
            log.warn("ERROR!, GetMoviesWithSchedules2 with showing_type: %s, days_delta: %s..." % (showing_type, days_delta))
            log.error(e)

        
        log.debug('movie_list: %s' % (results))
        return results

    def get_movie_via_movie_code1(self, movie_code, theater_movie_code):
        try:
            results = {}
            movie = self.ws_client.service.GetMovie(movie_code=movie_code)
            movie_xmovie = getattr(movie, 'XMovie', None)

            if movie_xmovie is None:
                log.warn("SKIP!, get_movie_via_movie_code1, attribute XMovie doesn't exist...")

                return results

            movie_code = getattr(movie_xmovie, 'Movie_Code', None)
            movie_name = getattr(movie_xmovie, 'Movie_Name', None)
            cast = getattr(movie_xmovie, 'StarringCast', '')
            running_time = getattr(movie_xmovie, 'RunningTime', '0.0')
            mtrcb_rating = getattr(movie_xmovie, 'Mtrcb_Rating', '')
            synopsis = getattr(movie_xmovie, 'Synopsis', '')
            genre = getattr(movie_xmovie, 'Genre', '')

            if movie_code is None:
                log.warn("SKIP!, get_movie_via_movie_code1, attribute Movie_Code doesn't exist...")

                return results

            if movie_name is None:
                log.warn("SKIP!, get_movie_via_movie_code1, attribute Movie_Name doesn't exist...")

                return results

            results['id'] = '%s::%s' % (theater_movie_code, movie_code)
            results['movie_title'] = movie_name
            results['cast'] = self.__parse_cast(unicode(cast))
            results['runtime_mins'] = float(running_time)
            results['advisory_rating'] = mtrcb_rating
            results['synopsis'] = unicode(synopsis)
            results['genre'] = genre
        except Exception, e:
            log.warn("ERROR!, GetMovie with movie_code: %s..." % movie_code)
            log.error(e)

            results = {}

        return results

    def get_movie_via_movie_code2(self, movie_code, theater_movie_code):
        try:
            results = {}
            movie = self.ws_client.service.GetMovie2(movie_code=movie_code)
            movie_movie = getattr(movie, 'Movie', None)

            if movie_movie is None:
                log.warn("SKIP!, get_movie_via_movie_code2, attribute Movie doesn't exist...")

                return results

            movie_code = getattr(movie_movie, 'Movie_Code', movie_code)
            movie_name = getattr(movie_movie, 'Movie_Name', None)
            cast = getattr(movie_movie, 'StarringCast', '')
            running_time = getattr(movie_movie, 'RunningTime', '0.0')
            mtrcb_rating = getattr(movie_movie, 'Mtrcb_Rating', '')
            synopsis = getattr(movie_movie, 'Synopsis', '')
            genre = getattr(movie_movie, 'Genre', '')

            if movie_code is None:
                log.warn("SKIP!, get_movie_via_movie_code2, attribute Movie_Code doesn't exist...")

                return results

            if movie_name is None:
                log.warn("SKIP!, get_movie_via_movie_code2, attribute Movie_Name doesn't exist...")

                return results

            results['id'] = '%s::%s' % (theater_movie_code, movie_code)
            results['movie_title'] = movie_name
            results['cast'] = self.__parse_cast(unicode(cast))
            results['runtime_mins'] = float(running_time)
            results['advisory_rating'] = mtrcb_rating
            results['synopsis'] = unicode(synopsis)
            results['genre'] = genre
        except Exception, e:
            log.warn("ERROR!, GetMovie2 with movie_code: %s..." % movie_code)
            log.error(e)

            results = {}

        return results

    def get_branches(self):
        results = []

        try:
            branches = self.ws_client.service.GetBranches()
            # This is for Robinsons, BranchResult returns BranchList->Branch
            branch_list = getattr(branches, 'BranchList', [])
            is_robinsons = False
            if branch_list:
                branches_branch = getattr(branch_list, 'Branch', [])
                is_robinsons = True
            else:
                branches_branch = getattr(branches, 'Branch', [])

            for branch in branches_branch:
                branch_dict = {}
                branch_key = getattr(branch, 'Branch_Key', None) if is_robinsons else getattr(branch, 'BranchID', None)
                branch_code = getattr(branch, 'Branch_Code', None)
                branch_name = getattr(branch, 'Branch_Name', None)
                branch_company = getattr(branch, 'Company', None)

                if branch_key is None:
                    log.warn("SKIP!, get_branches, attribute BranchID doesn't exist...")

                    continue

                if branch_code is None:
                    log.warn("SKIP!, get_branches, attribute Branch_Code doesn't exist...")

                    continue

                branch_dict['branch_key'] = branch_key
                branch_dict['branch_code'] = branch_code
                branch_dict['branch_name'] = branch_name
                branch_dict['branch_company'] = branch_company
                results.append(branch_dict)
        except Exception, e:
            log.warn("ERROR!, GetBranches...")
            log.error(e)

        return results

    def get_branches_with_schedules(self):
        results = []
        today = datetime.datetime.today().isoformat()

        try:
            branches = self.ws_client.service.GetBranchesWithSchedules(date=today, filter=self.cinema_filters.ALL, running=False)
            branches_xbranch = getattr(branches, 'XBranch', [])

            for branch in branches_xbranch:
                branch_dict = {}
                branch_key = getattr(branch, 'Branch_Key', None)
                branch_code = getattr(branch, 'Branch_Code', None)
                branch_name = getattr(branch, 'Branch_Name', None)

                if branch_key is None:
                    log.warn("SKIP!, get_branches_with_schedules, attribute Branch_Key doesn't exist...")

                    continue

                if branch_code is None:
                    log.warn("SKIP!, get_branches_with_schedules, attribute Branch_Code doesn't exist...")

                    continue

                branch_dict['branch_key'] = branch_key
                branch_dict['branch_code'] = branch_code
                branch_dict['branch_name'] = branch_name
                branch_dict['branch_company'] = ''
                results.append(branch_dict)
        except Exception, e:
            log.warn("ERROR!, GetBranchesWithSchedules...")
            log.error(e)

        return results

    def get_schedules_by_branch(self, org_uuid, branch_key, branch_code, show_date, movies):
        results = []

        try:
            schedules = self.ws_client.service.GetSchedulesByBranch(date=show_date.isoformat(),
                    branchID=branch_key)
            is_robinsons = False
            schedules_schedule = getattr(schedules, 'Schedule', [])
            if not schedules_schedule:
                schedules_schedule = getattr(schedules, 'XSchedule', [])
                is_robinsons = True

            # if branch_code == 'SMSJ' and branch_key == '57':
            #     schedules_schedule = [
            #         {
            #             "BranchID": "57",
            #             "MovieID": "tt4244998",
            #             "MovieCode": "ALPHA-2D",
            #             "ScheduleID": "26087",
            #             "Screening_Type": "2",
            #             "Cinema_Code": "4",
            #             "CinemaType": "DIGITAL",
            #             "FilmType": "F2D",
            #             "Start_Time": "2018-09-11T12:30:00",
            #             "End_Time": "2018-09-11T14:14:00",
            #             "Price": "220.0000",
            #             "Allow_Online_Purchase": "true",
            #             "CinemaName": "Digital Cinema 4",
            #             "CinemaKey": "4"
            #         },
            #         {
            #             "BranchID": "57",
            #             "MovieID": "tt4244998",
            #             "MovieCode": "ALPHA-2D",
            #             "ScheduleID": "26085",
            #             "Screening_Type": "2",
            #             "Cinema_Code": "4",
            #             "CinemaType": "DIGITAL",
            #             "FilmType": "F2D",
            #             "Start_Time": "2018-09-11T14:45:00",
            #             "End_Time": "2018-09-11T16:29:00",
            #             "Price": "220.0000",
            #             "Allow_Online_Purchase": "true",
            #             "CinemaName": "Digital Cinema 4",
            #             "CinemaKey": "4"
            #         },
            #         {
            #             "BranchID": "57",
            #             "MovieID": "tt4244998",
            #             "MovieCode": "ALPHA-2D",
            #             "ScheduleID": "26084",
            #             "Screening_Type": "1",
            #             "Cinema_Code": "4",
            #             "CinemaType": "DIGITAL",
            #             "FilmType": "F2D",
            #             "Start_Time": "2018-09-11T17:05:00",
            #             "End_Time": "2018-09-11T18:49:00",
            #             "Price": "220.0000",
            #             "Allow_Online_Purchase": "true",
            #             "CinemaName": "Digital Cinema 4",
            #             "CinemaKey": "4"
            #         },
            #         {
            #             "BranchID": "57",
            #             "MovieID": "tt4244998",
            #             "MovieCode": "ALPHA-2D",
            #             "ScheduleID": "26083",
            #             "Screening_Type": "1",
            #             "Cinema_Code": "4",
            #             "CinemaType": "DIGITAL",
            #             "FilmType": "F2D",
            #             "Start_Time": "2018-09-11T19:15:00",
            #             "End_Time": "2018-09-11T20:59:00",
            #             "Price": "220.0000",
            #             "Allow_Online_Purchase": "true",
            #             "CinemaName": "Digital Cinema 4",
            #             "CinemaKey": "4"
            #         },
            #         {
            #             "BranchID": "57",
            #             "MovieID": "tt4244998",
            #             "MovieCode": "ALPHA-2D",
            #             "ScheduleID": "26107",
            #             "Screening_Type": "1",
            #             "Cinema_Code": "4",
            #             "CinemaType": "DIGITAL",
            #             "FilmType": "F2D",
            #             "Start_Time": "2018-09-11T21:25:00",
            #             "End_Time": "2018-09-11T23:09:00",
            #             "Price": "220.0000",
            #             "Allow_Online_Purchase": "true",
            #             "CinemaName": "Digital Cinema 4",
            #             "CinemaKey": "4"
            #         },
            #         {
            #             "BranchID": "57",
            #             "MovieID": "",
            #             "MovieCode": "GOYO-2D",
            #             "ScheduleID": "26080",
            #             "Screening_Type": "2",
            #             "Cinema_Code": "2",
            #             "CinemaType": "DIGITAL",
            #             "FilmType": "F2D",
            #             "Start_Time": "2018-09-11T11:45:00",
            #             "End_Time": "2018-09-11T14:20:00",
            #             "Price": "220.0000",
            #             "Allow_Online_Purchase": "true",
            #             "CinemaName": "Digital Cinema 2",
            #             "CinemaKey": "2"
            #         },
            #         {
            #             "BranchID": "57",
            #             "MovieID": "",
            #             "MovieCode": "GOYO-2D",
            #             "ScheduleID": "26081",
            #             "Screening_Type": "2",
            #             "Cinema_Code": "2",
            #             "CinemaType": "DIGITAL",
            #             "FilmType": "F2D",
            #             "Start_Time": "2018-09-11T14:55:00",
            #             "End_Time": "2018-09-11T17:30:00",
            #             "Price": "220.0000",
            #             "Allow_Online_Purchase": "true",
            #             "CinemaName": "Digital Cinema 2",
            #             "CinemaKey": "2"
            #         },
            #         {
            #             "BranchID": "57",
            #             "MovieID": "",
            #             "MovieCode": "GOYO-2D",
            #             "ScheduleID": "26079",
            #             "Screening_Type": "1",
            #             "Cinema_Code": "2",
            #             "CinemaType": "DIGITAL",
            #             "FilmType": "F2D",
            #             "Start_Time": "2018-09-11T18:15:00",
            #             "End_Time": "2018-09-11T20:50:00",
            #             "Price": "220.0000",
            #             "Allow_Online_Purchase": "true",
            #             "CinemaName": "Digital Cinema 2",
            #             "CinemaKey": "2"
            #         },
            #         {
            #             "BranchID": "57",
            #             "MovieID": "",
            #             "MovieCode": "GOYO-2D",
            #             "ScheduleID": "26106",
            #             "Screening_Type": "1",
            #             "Cinema_Code": "2",
            #             "CinemaType": "DIGITAL",
            #             "FilmType": "F2D",
            #             "Start_Time": "2018-09-11T21:20:00",
            #             "End_Time": "2018-09-11T23:55:00",
            #             "Price": "220.0000",
            #             "Allow_Online_Purchase": "true",
            #             "CinemaName": "Digital Cinema 2",
            #             "CinemaKey": "2"
            #         },
            #         {
            #             "BranchID": "57",
            #             "MovieID": "",
            #             "MovieCode": "HOWSOFUS-2D",
            #             "ScheduleID": "26091",
            #             "Screening_Type": "1",
            #             "Cinema_Code": "3",
            #             "CinemaType": "DIGITAL",
            #             "FilmType": "F2D",
            #             "Start_Time": "2018-09-11T21:15:00",
            #             "End_Time": "2018-09-11T23:12:00",
            #             "Price": "230.0000",
            #             "Allow_Online_Purchase": "true",
            #             "CinemaName": "Digital Cinema 3",
            #             "CinemaKey": "3"
            #         },
            #         {
            #             "BranchID": "57",
            #             "MovieID": "",
            #             "MovieCode": "HOWSOFUS-2D",
            #             "ScheduleID": "26090",
            #             "Screening_Type": "1",
            #             "Cinema_Code": "3",
            #             "CinemaType": "DIGITAL",
            #             "FilmType": "F2D",
            #             "Start_Time": "2018-09-11T17:55:00",
            #             "End_Time": "2018-09-11T19:52:00",
            #             "Price": "230.0000",
            #             "Allow_Online_Purchase": "true",
            #             "CinemaName": "Digital Cinema 3",
            #             "CinemaKey": "3"
            #         },
            #         {
            #             "BranchID": "57",
            #             "MovieID": "",
            #             "MovieCode": "HOWSOFUS-2D",
            #             "ScheduleID": "26088",
            #             "Screening_Type": "2",
            #             "Cinema_Code": "3",
            #             "CinemaType": "DIGITAL",
            #             "FilmType": "F2D",
            #             "Start_Time": "2018-09-11T15:05:00",
            #             "End_Time": "2018-09-11T17:02:00",
            #             "Price": "230.0000",
            #             "Allow_Online_Purchase": "true",
            #             "CinemaName": "Digital Cinema 3",
            #             "CinemaKey": "3"
            #         },
            #         {
            #             "BranchID": "57",
            #             "MovieID": "",
            #             "MovieCode": "HOWSOFUS-2D",
            #             "ScheduleID": "26089",
            #             "Screening_Type": "2",
            #             "Cinema_Code": "3",
            #             "CinemaType": "DIGITAL",
            #             "FilmType": "F2D",
            #             "Start_Time": "2018-09-11T12:15:00",
            #             "End_Time": "2018-09-11T14:12:00",
            #             "Price": "230.0000",
            #             "Allow_Online_Purchase": "true",
            #             "CinemaName": "Digital Cinema 3",
            #             "CinemaKey": "3"
            #         },
            #         {
            #             "BranchID": "57",
            #             "MovieID": "tt5814060",
            #             "MovieCode": "THENUN-2D",
            #             "ScheduleID": "26101",
            #             "Screening_Type": "1",
            #             "Cinema_Code": "1",
            #             "CinemaType": "DIGITAL",
            #             "FilmType": "F2D",
            #             "Start_Time": "2018-09-11T21:10:00",
            #             "End_Time": "2018-09-11T22:56:00",
            #             "Price": "220.0000",
            #             "Allow_Online_Purchase": "true",
            #             "CinemaName": "Digital Cinema 1",
            #             "CinemaKey": "1"
            #         },
            #         {
            #             "BranchID": "57",
            #             "MovieID": "tt5814060",
            #             "MovieCode": "THENUN-2D",
            #             "ScheduleID": "26102",
            #             "Screening_Type": "1",
            #             "Cinema_Code": "1",
            #             "CinemaType": "DIGITAL",
            #             "FilmType": "F2D",
            #             "Start_Time": "2018-09-11T18:05:00",
            #             "End_Time": "2018-09-11T19:51:00",
            #             "Price": "220.0000",
            #             "Allow_Online_Purchase": "true",
            #             "CinemaName": "Digital Cinema 1",
            #             "CinemaKey": "1"
            #         },
            #         {
            #             "BranchID": "57",
            #             "MovieID": "tt5814060",
            #             "MovieCode": "THENUN-2D",
            #             "ScheduleID": "26105",
            #             "Screening_Type": "2",
            #             "Cinema_Code": "1",
            #             "CinemaType": "DIGITAL",
            #             "FilmType": "F2D",
            #             "Start_Time": "2018-09-11T15:25:00",
            #             "End_Time": "2018-09-11T17:11:00",
            #             "Price": "220.0000",
            #             "Allow_Online_Purchase": "true",
            #             "CinemaName": "Digital Cinema 1",
            #             "CinemaKey": "1"
            #         },
            #         {
            #             "BranchID": "57",
            #             "MovieID": "tt5814060",
            #             "MovieCode": "THENUN-2D",
            #             "ScheduleID": "2610344",
            #             "Screening_Type": "2",
            #             "Cinema_Code": "1",
            #             "CinemaType": "DIGITAL",
            #             "FilmType": "F2D5",
            #             "Start_Time": "2018-09-11T12:45:00",
            #             "End_Time": "2018-09-11T14:31:00",
            #             "Price": "220.0000",
            #             "Allow_Online_Purchase": "true",
            #             "CinemaName": "Digital Cinema 1",
            #             "CinemaKey": "1"
            #         }
            #     ]

            # for deleted feed code porpose only 
            # create array list of feed codes
            feed_codes = []
            for schedule2 in schedules_schedule:
                allow_purchase = getattr(schedule2, 'Allow_Online_Purchase', False)

                # if str(branch_code) == 'SMSJ' and str(branch_key) == '57': # for test
                #     allow_purchase = schedule2['Allow_Online_Purchase']

                if allow_purchase:
                    schedule_id = getattr(schedule2, 'ScheduleID', None)
                    # schedule_id = schedule2['ScheduleID'] # for test

                    if schedule_id is None:
                        log.warn("SKIP!, no feed code")
                        continue
                    
                    feed_codes.append(schedule_id)  

            log.debug("FEED_CODE_LIST: %s" % (feed_codes))       

            for schedule in schedules_schedule:
                allow_purchase = getattr(schedule, 'Allow_Online_Purchase', False)

                # if str(branch_code) == 'SMSJ' and str(branch_key) == '57': # for test
                #      allow_purchase = schedule['Allow_Online_Purchase']

                if allow_purchase:
                    schedule_id = getattr(schedule, 'ScheduleID', None)
                    screening_type = getattr(schedule, 'Screening_Type', None)
                    start_time = getattr(schedule, 'Start_Time', None)
                    price = getattr(schedule, 'Price', None)
                    cinema_code = getattr(schedule, 'Cinema_Code', None)
                    cinema_key = getattr(schedule, 'CinemaKey', None)
                    cinema_name = getattr(schedule, 'Cinema_Name', '') if is_robinsons else getattr(schedule, 'CinemaName', '')
                    film_type = getattr(schedule, 'FilmType', '')
                    movie_code_schedule = getattr(schedule, 'MovieCode', '')

                    # if str(branch_code) == 'SMSJ' and str(branch_key) == '57': # for test
                    #     schedule_id = schedule['ScheduleID']
                    #     screening_type = schedule['Screening_Type']
                    #     start_time = schedule['Start_Time']
                    #     price = schedule['Price']
                    #     cinema_code = schedule['Cinema_Code']
                    #     cinema_key = schedule['CinemaKey']
                    #     cinema_name = schedule['CinemaName']
                    #     film_type = schedule['FilmType']
                    #     movie_code_schedule = schedule['MovieCode']

                    #     cinema_id = '%s::%s' % (cinema_name, str(datetime.datetime.strptime(start_time,'%Y-%m-%dT%H:%M:%S').date()).replace("-", ""))
                    #     query.check_schedule(cinema_id, branch_code, org_uuid, schedule, feed_codes)
                        
                    if schedule_id is None:
                        log.warn("SKIP!, get_schedules_by_branch_and_movie, attribute ScheduleID doesn't exist...")

                        continue

                    if screening_type is None:
                        log.warn("SKIP!, get_schedules_by_branch_and_movie, attribute Screening_Type doesn't exist...")

                        continue

                    if start_time is None:
                        log.warn("SKIP!, get_schedules_by_branch_and_movie, attribute Start_Time doesn't exist...")

                        continue

                    if price is None:
                        log.warn("SKIP!, get_schedules_by_branch_and_movie, attribute Price doesn't exist...")

                        continue

                    if cinema_key is None:
                        log.warn("SKIP!, get_schedules_by_branch_and_movie, attribute CinemaKey doesn't exist...")

                        continue

                    if cinema_code is None:
                        log.warn("SKIP!, get_schedules_by_branch_and_movie, attribute Cinema_Code doesn't exist...")

                        continue

                    movie_details = filter(lambda m: m['movie_code'] == movie_code_schedule, movies)

                    log.debug("movie_details:" % (movie_details))

                    if len(movie_details) != 1:
                        log.warn("SKIP!, conflicts, movie_code_schedule: %s, len(movie_details): %s..." % (movie_code_schedule, len(movie_details)))

                        continue

                    movie_id = movie_details[0]['id']
                    movie_title = movie_details[0]['movie_title']
                    cinema_id = '%s::%s' % (cinema_key, cinema_code)

                    schedule_dict = {}
                    schedule_dict['id'] = str(schedule_id)
                    schedule_dict['seat_type'] = SCREENING_TYPE[str(screening_type)]
                    schedule_dict['variant'] = film_type
                    schedule_dict['screening'] = self.__parse_datetime_to_string(start_time)
                    schedule_dict['movie_id'] = movie_id
                    schedule_dict['movie_title'] = movie_title
                    schedule_dict['theater_code'] = branch_code
                    schedule_dict['cinema_id'] = cinema_id
                    schedule_dict['cinema_name'] = cinema_name
                    schedule_dict['price'] = str(price)
                    schedule_dict['uuid'] = org_uuid

                    results.append(schedule_dict)
        except Exception, e:
            log.warn("ERROR!, GetSchedulesByBranch with branch_key: %s..." % branch_key)
            log.error(e)
            log.error(traceback.format_exc(e))

        return results

    def get_schedules_by_branch_and_movie(self, org_uuid, branch_key, branch_code, show_date, movie_details):
        results = []

        try:
            movie_id = movie_details['id']
            movie_code = movie_details['movie_code']
            movie_title = movie_details['movie_title']
            film_format = movie_details['film_format']
            schedules = self.ws_client.service.GetSchedulesByBranchAndMovie(date=show_date.isoformat(),
                    branch=branch_key, movie_code=movie_code, filter=self.cinema_filters.ALL, running=False)
            schedules_xschedule = getattr(schedules, 'XSchedule', [])

            for schedule in schedules_xschedule:
                allow_purchase = getattr(schedule, 'Allow_Online_Purchase', False)

                if allow_purchase:
                    schedule_id = getattr(schedule, 'MCT_Key', None)
                    screening_type = getattr(schedule, 'Screening_Type', None)
                    start_time = getattr(schedule, 'Start_Time', None)
                    price = getattr(schedule, 'Price', None)
                    cinema_code = getattr(schedule, 'Cinema_Code', None)
                    cinema_key = getattr(schedule, 'CinemaKey', None)
                    cinema_name = getattr(schedule, 'Cinema_Name', '')
                    film_type = getattr(schedule, 'FilmType', film_format)
                    cinema_id = str(cinema_code)

                    if schedule_id is None:
                        log.warn("SKIP!, get_schedules_by_branch_and_movie, attribute MCT_Key doesn't exist...")

                        continue

                    if screening_type is None:
                        log.warn("SKIP!, get_schedules_by_branch_and_movie, attribute Screening_Type doesn't exist...")

                        continue

                    if start_time is None:
                        log.warn("SKIP!, get_schedules_by_branch_and_movie, attribute Start_Time doesn't exist...")

                        continue

                    if price is None:
                        log.warn("SKIP!, get_schedules_by_branch_and_movie, attribute Price doesn't exist...")

                        continue

                    if cinema_code is None:
                        log.warn("SKIP!, get_schedules_by_branch_and_movie, attribute Cinema_Code doesn't exist...")

                        continue

                    if cinema_key is not None:
                        cinema_id = '%s::%s' % (cinema_key, cinema_code)

                    schedule_dict = {}
                    schedule_dict['id'] = str(schedule_id)
                    schedule_dict['seat_type'] = SCREENING_TYPE[str(screening_type)]
                    schedule_dict['variant'] = film_type
                    schedule_dict['screening'] = self.__parse_datetime_to_string(start_time)
                    schedule_dict['movie_id'] = movie_id
                    schedule_dict['movie_title'] = movie_title
                    schedule_dict['theater_code'] = branch_code
                    schedule_dict['cinema_id'] = cinema_id
                    schedule_dict['cinema_name'] = cinema_name
                    schedule_dict['price'] = str(price)
                    schedule_dict['uuid'] = org_uuid

                    results.append(schedule_dict)
        except Exception, e:
            log.warn("ERROR!, GetSchedulesByBranchAndMovie with branch_key: %s..." % branch_key)
            log.error(e)

        return results

    def __generate_delta_dates(self, days_delta):
        today = datetime.datetime.today()

        if days_delta is not None:
            date_max_delta = today + datetime.timedelta(days=int(days_delta))

            return [(today + datetime.timedelta(days=distance)) for distance in range((date_max_delta - today).days + 1)]
        else:
            return [today]

    def __parse_cast(self, cast):
        if not cast:
            return []

        cast_list = cast.split(',')

        addtl_cast_list = cast_list[-1].split('&amp;')
        cast_list.pop(-1)
        cast_list.extend(addtl_cast_list)

        addtl_cast_list = cast_list[-1].split('&')
        cast_list.pop(-1)
        cast_list.extend(addtl_cast_list)

        return [c.strip() for c in cast_list]

    def __parse_datetime_to_string(self, datetime_parameter, datetime_format='%m/%d/%Y %I:%M:%S %p'):
        try:
            string_datetime = datetime.datetime.strftime(datetime_parameter, datetime_format)
        except ValueError:
            log.error('ERROR!, Failed to parse string(%s) to date...' % datetime_value)

            string_datetime = None

        return string_datetime


class ServicesWSMGi(SoapConnector):
    def __init__(self, wsdl):
        super(ServicesWSMGi, self).__init__(wsdl)
        self.session_token = ''
        self.__do_get_session_token()

    def __do_get_session_token(self):
        try:
            session = self.ws_client.service.GetSessionToken()

            if session.Type == 'SESSION' and session.Result == 0:
                self.session_token = session.Data

                log.info("SUCCESS!, GetSessionToken with session_token: %s..." % self.session_token)
            else:
                log.warn("FAILED!, GetSessionToken...")
        except Exception, e:
            log.warn("ERROR!, GetSessionToken...")
            log.error(e)

    def __do_hash_password(self, username, password):
        usernamepasswordhash = hashlib.sha512(username + password).hexdigest().upper()
        encoded_password = hashlib.sha512(self.session_token + usernamepasswordhash).hexdigest().upper()

        return encoded_password

    def do_get_session_token(self):
        return self.session_token

    def do_authenticate_session(self, username, password):
        try:
            encoded_password = self.__do_hash_password(username, password)
            auth_session = self.ws_client.service.AuthenticateSession(session=self.session_token,
                    username=username, password=encoded_password)

            if auth_session.Type == 'AUTHENTICATEUSER' and auth_session.Result == 0:
                return 'success', None
            else:
                return 'error', auth_session.Message
        except Exception, e:
            log.warn("ERROR!, AuthenticateSession with username: %s, password: %s..." % (username, password))
            log.error(e)

        return 'error', 'Failed to authenticate session.'

    def do_authenticate_member_by_email(self, email, password, ipaddress):
        try:
            encoded_password = self.__do_hash_password(username, password)
            auth_member = self.ws_client.service.AuthenticateMemberByEmail(session=self.session_token,
                    email=email, password=encoded_password, ipAddress=ipaddress)

            if auth_member.Type == 'AUTHENTICATEMEMBER' and auth_member.Result == 0:
                return 'success', None
            else:
                return 'error', auth_member.Message
        except Exception, e:
            log.warn("ERROR!, AuthenticateMemberByEmail with email: %s, password: %s, ipaddress: %s" % (email, password, ipaddress))
            log.error(e)

        return 'error', 'Failed to authenticate member.'


class TransactionsWSMGi(SoapConnector):
    def __init__(self, wsdl, service_wsdl, username, password):
        super(TransactionsWSMGi, self).__init__(wsdl)
        self.ServicesWS = ServicesWSMGi(service_wsdl)
        self.session_token = self.ServicesWS.do_get_session_token()
        self.auth_session_status, self.auth_session_message = self.ServicesWS.do_authenticate_session(username, password)

    def get_branch_cinemas(self, branch_key):
        results = []

        if self.auth_session_status == 'error':
            log.warn("ERROR!, get_branch_cinemas. %s..." % self.auth_session_message)

            return results

        try:
            cinemas = self.ws_client.service.GetBranchCinemas(SessionToken=self.session_token, Branch_key=branch_key)
            cinemas_cinema = getattr(cinemas, 'CinemaLookUp', [])

            for cinema in cinemas_cinema:
                cinema_dict = {}
                cinema_key = getattr(cinema, 'CinemaKey', None)
                cinema_code = getattr(cinema, 'CinemaCode', None)
                cinema_name = getattr(cinema, 'Cinemaname', None)
                max_row = getattr(cinema, 'MaxRow', 0)
                max_column = getattr(cinema, 'MaxCol', 0)

                if cinema_key is None:
                    log.warn("SKIP!, get_branch_cinemas, attribute CinemaKey doesn't exist...")

                    continue

                if cinema_code is None:
                    log.warn("SKIP!, get_branch_cinemas, attribute CinemaCode doesn't exist...")

                    continue

                if cinema_name is None:
                    log.warn("SKIP!, get_branch_cinemas, attribute Cinemaname doesn't exist...")

                    continue

                cinema_dict['cinema_key'] = cinema_key
                cinema_dict['cinema_code'] = cinema_code
                cinema_dict['cinema_name'] = cinema_name
                cinema_dict['max_row'] = int(max_row)
                cinema_dict['max_column'] = int(max_column)
                results.append(cinema_dict)
        except Exception, e:
            log.warn("ERROR!, GetBranchCinemas with session_token: %s, branch_key: %s" % (self.session_token, branch_key))
            log.error(e)

        return results

    def get_cinema_lookup_key(self, branch_key):
        results = []

        if self.auth_session_status == 'error':
            log.warn("ERROR!, get_cinema_lookup_key. %s..." % self.auth_session_message)

            return results

        try:
            cinemas = self.ws_client.service.GetCinemaLookUpKey(SessionToken=self.session_token, Branch_key=branch_key)
            cinemas_cinema = getattr(cinemas, 'CinemaLookUp', [])

            for cinema in cinemas_cinema:
                cinema_dict = {}
                cinema_key = getattr(cinema, 'CinemaKey', None)
                cinema_code = getattr(cinema, 'CinemaCode', None)
                cinema_name = getattr(cinema, 'CinemaName', None)
                max_row = getattr(cinema, 'MaxRow', 0)
                max_column = getattr(cinema, 'MaxCol', 0)

                if cinema_key is None:
                    log.warn("SKIP!, get_cinema_lookup_key, attribute CinemaKey doesn't exist...")

                    continue

                if cinema_code is None:
                    log.warn("SKIP!, get_cinema_lookup_key, attribute CinemaCode doesn't exist...")

                    continue

                if cinema_name is None:
                    log.warn("SKIP!, get_cinema_lookup_key, attribute Cinemaname doesn't exist...")

                    continue

                cinema_dict['cinema_key'] = cinema_key
                cinema_dict['cinema_code'] = cinema_code
                cinema_dict['cinema_name'] = cinema_name
                cinema_dict['max_row'] = int(max_row)
                cinema_dict['max_column'] = int(max_column)
                results.append(cinema_dict)
        except Exception, e:
            log.warn("ERROR!, GetCinemaLookUpKey with session_token: %s, branch_key: %s" % (self.session_token, branch_key))
            log.error(e)

        return results

    def get_seat_map(self, branch_key, cinema_key, max_row, max_column):
        seat_count = 0
        seatmap_matrix = []

        if self.auth_session_status == 'error':
            log.warn("ERROR!, get_seat_map. %s..." % self.auth_session_message)

            return seat_count, seatmap_matrix

        try:
            cinema_seatmap = self.ws_client.service.GetSeatMap(SessionToken=self.session_token,
                    Branch_Key=branch_key, cinema_key=cinema_key)
            seatmap_matrix = self.__generate_seatmap_matrix(max_row, max_column)
            seats = getattr(cinema_seatmap, 'SeatsMap', [])

            for seat in seats:
                seat_row = getattr(seat, 'Row', None)
                seat_column = getattr(seat, 'Col', None)
                seat_id = getattr(seat, 'Id', None)
                seat_name = getattr(seat, 'Name', None)

                if seat_row is None:
                    log.warn("SKIP!, get_seat_map, attribute Row doesn't exist...")

                    continue

                if seat_column is None:
                    log.warn("SKIP!, get_seat_map, attribute Col doesn't exist...")

                    continue

                if seat_id is None:
                    log.warn("SKIP!, get_seat_map, attribute Id doesn't exist...")

                    continue

                if seat_name is None:
                    log.warn("SKIP!, get_seat_map, attribute Name doesn't exist...")

                    continue

                # making sure that the variables are on the right data type.
                seat_row = int(seat_row)
                seat_column = int(seat_column)
                seat_id = str(seat_id)
                seat_name = str(seat_name)

                if seat_row <= max_row and seat_column <= max_column:
                    s_matrix = seatmap_matrix[seat_row-1][seat_column-1]

                    if s_matrix['row'] == seat_row and s_matrix['column'] == seat_column:
                        s_matrix['seat_id'] = seat_id
                        s_matrix['seat_name'] = seat_name
                        seat_count += 1
        except Exception, e:
            log.warn("ERROR!, GetSeatMap with session_token: %s, branch_key: %s, cinema_key: %s" % (self.session_token, branch_key, cinema_key))
            log.error(e)

            # Encountered error. Reset the values for seat_count and seatmap_matrix.
            seat_count = 0
            seatmap_matrix = []

        return seat_count, seatmap_matrix

    def get_seat_map2(self, branch_key, cinema_key, max_row, max_column):
        seat_count = 0
        seatmap_matrix = []

        if self.auth_session_status == 'error':
            log.warn("ERROR!, get_seat_map2. %s..." % self.auth_session_message)

            return seat_count, seatmap_matrix

        try:
            cinema_seatmap = self.ws_client.service.GetSeatMap2(SessionToken=self.session_token,
                    Branch_Key=branch_key, cinema_key=cinema_key)
            screen_position = getattr(cinema_seatmap, 'ScreenPosition', None) # top=1, bottom=2
            seat_map = getattr(cinema_seatmap, 'CinemaSeats', None)

            if screen_position is None:
                log.warn("ERROR!, get_seat_map2, attribute ScreenPosition doesn't exist...")

                return seat_count, seatmap_matrix

            if seat_map is None:
                log.warn("ERROR!, get_seat_map2, attribute CinemaSeats doesn't exist... cinema_key: %s" % cinema_key)

                return seat_count, seatmap_matrix

            screen_position = int(screen_position)
            seatmap_matrix = self.__generate_seatmap_matrix(max_row, max_column)
            seats = getattr(seat_map, 'SeatsMap', [])

            for seat in seats:
                seat_row = getattr(seat, 'Row', None)
                seat_column = getattr(seat, 'Col', None)
                seat_id = getattr(seat, 'Id', None)
                seat_name = getattr(seat, 'Name', None)

                if seat_row is None:
                    log.warn("SKIP!, get_seat_map2, attribute Row doesn't exist...")

                    continue

                if seat_column is None:
                    log.warn("SKIP!, get_seat_map2, attribute Col doesn't exist...")

                    continue

                if seat_id is None:
                    log.warn("SKIP!, get_seat_map2, attribute Id doesn't exist...")

                    continue

                if seat_name is None:
                    log.warn("SKIP!, get_seat_map2, attribute Name doesn't exist...")

                    continue

                # making sure that the variables are on the right data type.
                seat_row = int(seat_row)
                seat_column = int(seat_column)
                seat_id = str(seat_id)
                seat_name = str(seat_name)

                # reverse the seatmap position with the screen on top.
                if screen_position == 2:
                    seat_row = (max_row - seat_row) + 1
                    seat_column = (max_column - seat_column) + 1

                if seat_row <= max_row and seat_column <= max_column:
                    s_matrix = seatmap_matrix[seat_row-1][seat_column-1]

                    if s_matrix['row'] == seat_row and s_matrix['column'] == seat_column:
                        s_matrix['seat_id'] = seat_id
                        s_matrix['seat_name'] = seat_name
                        seat_count += 1
        except Exception, e:
            log.warn("ERROR!, GetSeatMap2 with session_token: %s, branch_key: %s, cinema_key: %s" % (self.session_token, branch_key, cinema_key))
            log.error(e)

            # Encountered error. Reset the values for seat_count and seatmap_matrix.
            seat_count = 0
            seatmap_matrix = []

        return seat_count, seatmap_matrix

    def get_schedule_seats(self, branch_key, mct_key):
        results = []

        if self.auth_session_status == 'error':
            log.warn("ERROR!, get_schedule_seats. %s..." % self.auth_session_message)

            return 'error', None

        try:
            schedule_seats = self.ws_client.service.GetScheduleSeats(SessionToken=self.session_token,
                    Branch_Key=branch_key, MCT_Key=mct_key)

            return 'success', schedule_seats
        except Exception, e:
            log.warn("ERROR!, GetScheduleSeats session_token: %s, branch_key: %s, mct_key: %s..." % (self.session_token, branch_key, mct_key))
            log.error(e)

        return 'error', None

    def do_create_transaction(self, **kwargs):
        # default, assuming this transaction encountered an error.
        results = {}
        results['token'] = ''
        results['reference_number'] = ''
        results['message'] = 'An error occurred during your transaction. Kindly contact support@gmovies.ph to address your issue.'
        results['status'] = 'error'

        if self.auth_session_status == 'error':
            log.warn("ERROR!, do_create_transaction. %s..." % self.auth_session_message)

            return results

        try:
            branch_key = kwargs['branch_key']
            schedule_key = kwargs['schedule_key']
            email = kwargs['email']
            seat_list = {'string': kwargs['seat_list']}
            send_sms = kwargs['send_sms']
            partner_name = kwargs['partner_name'] if 'partner_name' in kwargs else '' # tagging for the selected branch partner.

            # handles the request.
            if partner_name in ['SM Malls']:
                #force key 59 to change to key 3 for sm maison
                log.debug("BRANCH KEY %s for SM MAISON DETECTED" % (branch_key))
                log.debug("CHANGING BRANCH KEY %s for SM MAISON TO KEY 3" % (branch_key))
                if branch_key == '59':
                    branch_key = '3'
                
                full_name = kwargs['full_name']
                payment_type = kwargs['payment_type']

                transaction = self.ws_client.service.CreateTransaction(SessionToken=self.session_token, email=email,
                        full_name=full_name, Branch_Key=branch_key, MCT_Key=schedule_key, Seatlist=seat_list,
                        payment_type=payment_type, SendSMS=send_sms)
            elif partner_name in ['Robinsons Malls']:
                client_info = kwargs['client_info']
                payment_type = kwargs['payment_type']
                payment_gw_id = kwargs['payment_gw_id']
                food_bundle = {'string': kwargs['food_bundle']}

                transaction = self.ws_client.service.CreateTransactionWithSession(SessionToken=self.session_token,
                        clientInfo=client_info, Branch_Key=branch_key, MCT_Key=schedule_key,
                        Seatlist=seat_list, SendSMS=send_sms, FoodBundle=food_bundle, Payment_Gateway_id=payment_gw_id)
            elif partner_name in ['Megaworld Malls']:
                transaction = self.ws_client.service.CreateTransaction(SessionToken=self.session_token, email=email,
                        Branch_Key=branch_key, MCT_Key=schedule_key, Seatlist=seat_list, SendSMS=send_sms)
            else:
                log.warn("ERROR!, do_create_transaction. Selected branch partner is not supported...")

                return results

            # handles the response/results.
            if partner_name in ['SM Malls'] and transaction.Type == 'PURCHASEAPI' and transaction.Result == 0:
                log.info("SUCCESS!, CreateTransaction. %s" % transaction.Message)

                results['token'] = transaction.Token # use for pin parameter in FinalizeTransaction.
                results['reference_number'] = transaction.Data
                results['message'] = str(transaction.Message) # if 0 contains the amount.
                results['status'] = 'success'
            elif partner_name in ['Megaworld Malls', 'Robinsons Malls'] and transaction.Result == 0:
                log.info("SUCCESS!, CreateTransaction. %s" % transaction.Message)

                refno, amount, pin = transaction.Message.split('|') # if 0 contains the refno, amount, and pin.
                results['token'] = pin # use for pin parameter in FinalizeTransaction.
                results['reference_number'] = refno
                results['message'] = amount.replace(',','')
                results['status'] = 'success'
            else:
                log.warn("ERROR!, CreateTransaction. %s" % transaction.Message)

                results['message'] = transaction.Message # if 1 contains the error message.

                # replace the error message when user reached the allowed transactions per day.
                if (transaction.Message == 'Transaction Error: You have -1 Remaining' or
                        transaction.Message == 'Transaction Error: You have 0 Remaining'):
                    results['message'] = 'You have reached the maximum number of transactions today. You may start buying tickets again tomorrow.'
        except Exception, e:
            log.warn("ERROR!, CreateTransaction...")
            import traceback
            log.error(str(traceback.format_exc()))
            results['exception_msg'] = str(e)

        return results

    def do_finalize_transaction(self, **kwargs):
        # default, assuming this transaction encountered an error.
        results = {}
        results['token'] = ''
        results['payment_reference'] = ''
        results['booking_id'] = ''
        results['message'] = 'An error occurred during your transaction. Kindly contact support@gmovies.ph to address your issue.'
        results['status'] = 'error'

        if self.auth_session_status == 'error':
            log.warn("ERROR!, do_finalize_transaction. %s..." % self.auth_session_message)

            return results

        try:
            reference_number = kwargs['reference_number']
            amount = kwargs['amount']
            payment_type = kwargs['payment_type']
            payment_reference = kwargs['payment_reference']
            pin = kwargs['pin']
            partner_name = kwargs['partner_name'] if 'partner_name' in kwargs else '' # tagging for the selected branch partner.

            # handles the request.
            if partner_name in ['SM Malls']:
                transaction = self.ws_client.service.FinalizeTransaction(SessionToken=self.session_token,
                        ReferenceNumber=reference_number, Amount=amount, PaymentType=payment_type,
                        PaymentReference=payment_reference, pin=pin)
            elif partner_name in ['Robinsons Malls']:
                client_info = kwargs['client_info'] if kwargs['client_info'] else ''
                receipt_id = kwargs['receipt_id']
                transaction = self.ws_client.service.FinalizeTransactionWithSession(SessionToken=self.session_token,
                        ClientDetails=client_info, ReferenceNumber=reference_number, Amount=amount,
                        PaymentType=payment_type, PaymentReference=payment_reference, pin=pin, receiptId=receipt_id)
            elif partner_name in ['Megaworld Malls']:
                email = kwargs['email']
                receipt_id = kwargs['receipt_id']

                transaction = self.ws_client.service.FinalizeTransaction(SessionToken=self.session_token, email=email,
                        ReferenceNumber=reference_number, Amount=amount, PaymentType=payment_type,
                        PaymentReference=payment_reference, pin=pin, receiptId=receipt_id)
            else:
                log.warn("ERROR!, do_finalize_transaction. Selected branch partner is not supported...")

                return results

            # handles the response/results.
            if ((partner_name in ['SM Malls'] and transaction.Type == 'PURCHASEAPI' and transaction.Result == 0) or
                (partner_name in ['Robinsons Malls'] and transaction.Type == 'TRANSACTIONAPI' and transaction.Result == 0)):
                log.info("SUCCESS!, FinalizeTransaction. %s" % transaction.Message)

                results['token'] = transaction.Token
                results['payment_reference'] = transaction.Data
                results['message'] = transaction.Message
                results['status'] = 'success'
            elif partner_name in ['Megaworld Malls'] and transaction.Type == 'TRANSACTIONAPI' and transaction.Result == 0:
                log.info("SUCCESS!, FinalizeTransaction. %s" % transaction.Message)
                log.info("SUCCESS!, FinalizeTransaction. %s" % transaction.BookingId)

                results['booking_id'] = transaction.BookingId.split('|')[0] # disregard other parts of booking id
                results['message'] = transaction.Message
                results['status'] = 'success'
            else:
                log.warn("ERROR!, FinalizeTransaction. %s" % transaction.Message)

                results['message'] = transaction.Message
        except Exception, e:
            log.warn("ERROR!, FinalizeTransaction...")
            log.error(e)
            results['exception_msg'] = str(e)

        return results

    def do_check_transaction_finalized(self, reference_number):
            # default, assuming this transaction encountered an error.
            results = {}
            results['result'] = ''
            results['txn_receipt'] = ''
            results['message'] = 'An error occurred during your transaction. Kindly contact support@gmovies.ph to address your issue.'
            results['status'] = 'error'

            if self.auth_session_status == 'error':
                log.warn("ERROR!, do_check_transaction_finalized. %s..." % self.auth_session_message)

                return results

            try:
                transaction = self.ws_client.service.CheckTransactionFinalized(SessionToken=self.session_token, ReferenceNumber=reference_number)
                log.debug("do_check_transaction_finalized: response")
                log.debug(transaction)

                results['result'] = transaction.Result
                results['message'] = transaction.Message
                if (transaction.Type == 'TransactionAPI' and transaction.Result == 0):
                    results['txn_receipt'] = transaction.TransactionReceipt
                    results['status'] = 'success'

            except Exception, e:
                log.warn("ERROR!, CheckTransactionFinalized...")
                log.error(e)

            return results

    def __generate_seatmap_matrix(self, max_row, max_column):
        results = []

        for row in range(1, max_row+1):
            rows = []

            for col in range(1, max_column+1):
                column = {}
                column['seat_id'] = ''
                column['row'] = row
                column['column'] = col
                column['seat_name'] = ''

                rows.append(column)

            results.append(rows)

        return results


class EPlusWSMGi(SoapConnector):
    def __init__(self, wsdl):
        super(EPlusWSMGi, self).__init__(wsdl)
        self.signature = ''

    def __do_generate_signature(self, password, card_number, pin, merchant_reference, amount, currency):
        try:
            info = card_number + pin + merchant_reference + amount + currency
            self.signature = self.ws_client.service.GenerateSignature(info=info, password=password)

            log.info("SUCCESS!, GenerateSignature with signature: %s..." % self.signature)
        except Exception, e:
            log.warn("ERROR!, GenerateSignature...")
            log.error(e)

    def do_online_wallet_member_transaction(self, card_number, pin, secret_key, **kwargs):
        # default, assuming this transaction encountered an error.
        results = {}
        results['reference_number'] = ''
        results['remaining_balance'] = ''
        results['message'] = 'An error occurred during your transaction. Kindly contact support@gmovies.ph to address your issue.'
        results['status'] = 'error'

        try:
            reference_number = kwargs['reference_number']
            merchant_reference = kwargs['merchant_reference']
            transaction_type = kwargs['transaction_type']
            amount = kwargs['amount']
            currency = kwargs['currency']
            merchant_id = kwargs['merchant_id']
            merchant_code = kwargs['merchant_code']
            description = kwargs['description']

            # generates signature needed to create E-Plus transaction.
            self.__do_generate_signature(secret_key, card_number, pin, merchant_reference, str(amount), currency)

            if not self.signature:
                log.warn("ERROR!, OnlineWalletMemberTransaction. No signature generated, run __do_generate_signature...")

                return results

            transaction = self.ws_client.service.OnlineWalletMemberTransaction(signature=self.signature,
                    cardnumber=card_number, refno=reference_number, merchantrefno=merchant_reference,
                    pin=pin, type=transaction_type, amount=amount, currency=currency,
                    merchantID=merchant_id, merchant_code=merchant_code, description=description)

            results['reference_number'] = transaction.RefNo
            results['remaining_balance'] = transaction.RemainingAmount
            results['message'] = transaction.Message

            if transaction.Result == 0:
                log.info("SUCCESS!, OnlineWalletMemberTransaction. %s..." % transaction.Message)

                results['status'] = 'success'
            else:
                log.warn("ERROR!, OnlineWalletMemberTransaction. %s..." % transaction.Message)
        except Exception, e:
            log.warn("ERROR!, OnlineWalletMemberTransaction...")
            log.error(e)

        return results

    def do_online_wallet_member_check_balance(self, service_wsdl, username, password, card_number):
        # default, assuming this transaction encountered an error.
        results = {}
        results['message'] = 'An error occurred during your transaction. Kindly contact support@gmovies.ph to address your issue.'
        results['status'] = 'error'

        try:
            ServicesWS = ServicesWSMGi(service_wsdl)
            session_token = ServicesWS.do_get_session_token()
            auth_session_status, auth_session_message = ServicesWS.do_authenticate_session(username, password)

            if auth_session_status == 'error':
                log.warn("ERROR!, do_online_wallet_member_check_balance. %s..." % auth_session_message)

                return results

            check_balance = self.ws_client.service.OnlineWalletMemberCheckBalance(session=session_token, cardnumber=card_number)
            results['message'] = str(check_balance.Message) # if 0 contains the amount, if 1 contains the message.

            if check_balance.Result == 0:
                log.info("SUCCESS!, OnlineWalletMemberCheckBalance. %s..." % check_balance.Message)

                results['status'] = 'success'
            else:
                log.warn("ERROR!, OnlineWalletMemberCheckBalance. %s..." % check_balance.Message)
        except Exception, e:
            log.warn("ERROR! OnlineWalletMemberCheckBalance...")
            log.error(e)

        return results

def parse_xml_response(xml_response, response_dict):
    xml_data = etree.fromstring(xml_response)
    log.debug("parse_xml_response, xml_data")
    log.debug(xml_data)

    for item in xml_data.getiterator():
        key_name = re.sub(r'\{.*?\}', '', item.tag)
        if key_name in response_dict and item.text:
            response_dict[key_name] = item.text
    return response_dict

def parse_xml_response_query(xml_response, response_dict):
    xml_data = etree.fromstring(xml_response)
    log.debug("parse_xml_response_query, xml_data")
    log.debug(xml_data)

    txns_ctr = 0
    # for getting token details
    items = []
    values = []

    for item in xml_data.getiterator():
        parent   = ''
        p_parent = ''
        key_name = re.sub(r'\{.*?\}', '', item.tag)

        # get parent and parent of parent for proper population of data
        parent_raw = item.getparent()
        if parent_raw is not None:
            parent = re.sub(r'\{.*?\}', '', parent_raw.tag)

        p_parent_raw = None if parent_raw is None else parent_raw.getparent()
        if p_parent_raw is not None:
            p_parent = re.sub(r'\{.*?\}', '', p_parent_raw.tag)

        # to get token details in metadata/subdata
        if key_name == 'item':
            items.append(item.text)
        if key_name == 'value':
            values.append(item.text)

        if key_name in response_dict and item.text:
            if 'queryResult' == p_parent:
                response_dict[key_name] = item.text
            if 'ServiceResponse' == p_parent:
                results_txns_key = 'txns::%d' % (txns_ctr-1)
                response_dict[results_txns_key][key_name] = item.text
        if key_name == 'ServiceResponse':
            results_txns_key = 'txns::%d' % (txns_ctr)
            response_dict[results_txns_key] = {}
            txns_ctr += 1

    # populate token details
    if items and len(items) == len(values) and 'txns::0' in response_dict:
        for i in xrange(len(items)):
            response_dict['txns::0'][items[i]] = values[i]

    return response_dict

class CCServiceWSPaynamics():
    def __init__(self, wsdl, targetnamespace):
        log.info("CCServiceWSPaynamics, wsdl, %s..." % wsdl)
        log.info("CCServiceWSPaynamics, targetnamespace, %s..." % targetnamespace)

        # used requests lib for soap calls instead of suds
        # schema_url = 'http://www.w3.org/2001/XMLSchema'
        # schema_import = suds.xsd.doctor.Import(schema_url)
        # schema_import.filter.add(targetnamespace)
        # schema_doctor = suds.xsd.doctor.ImportDoctor(schema_import)
        # self.ws_client = suds.client.Client(url=wsdl, doctor=schema_doctor)

        self.wsdl          = wsdl
        self.header_host   = ''
        self.header_action = ''
        self.xml_request   = ''

    def do_requery(self, merchant_id, request_id, org_trxid, org_trxid2, signature):
        log.debug("CCServiceWSPaynamics, do_requery, start...")
        log.info("CCServiceWSPaynamics, do_requery, merchant_id, %s..." % merchant_id)
        log.info("CCServiceWSPaynamics, do_requery, request_id, %s..." % request_id)
        log.info("CCServiceWSPaynamics, do_requery, org_trxid, %s..." % org_trxid)
        log.info("CCServiceWSPaynamics, do_requery, org_trxid2, %s..." % org_trxid2)
        log.info("CCServiceWSPaynamics, do_requery, signature, %s..." % signature)

        results = {}

        try:
            self.header_host = PAYNAMICS_PNXQUERY_HEADER_HOST
            self.header_action = PAYNAMICS_PNXQUERY_HEADER_ACTION_QUERY
            self.xml_request = PAYNAMICS_PNXQUERY_XML_QUERY

            self.xml_request = self.xml_request.format(merchant_id, request_id,
                org_trxid, org_trxid2, signature)

            encoded_request = self.xml_request.encode('utf-8')
            log.info("CCServiceWSPaynamics, do_requery, encoded_request")
            log.info(encoded_request)

            headers = {"Host": self.header_host,
                "Content-Type": "text/xml; charset=utf-8",
                "Content-Length": str(len(encoded_request)),
                "SOAPAction": self.header_action
            }

            requery_response = requests.post(url = self.wsdl, headers = headers, data = encoded_request, timeout = 60)
            log.info("CCServiceWSPaynamics, do_requery, requery_response")
            log.info(requery_response.status_code)
            log.info(requery_response.content)

            if 200 != requery_response.status_code:
                return results

            results['rebill_id']        = ''
            results['merchantid']       = ''
            results['request_id']       = ''
            results['response_id']      = ''
            results['timestamp']        = ''
            results['signature']        = ''
            results['response_code']    = ''
            results['response_message'] = ''
            results['response_advise']  = ''

            results = parse_xml_response_query(requery_response.content, results)

            """
            requery_response = self.ws_client.service.query(merchantid=merchant_id, request_id=request_id,
                    org_trxid=org_trxid, org_trxid2=org_trxid2, signature=signature)
            requery_response_application = requery_response.application
            requery_response_status = requery_response.responseStatus
            requery_response_txns = requery_response.txns

            log.debug("CCServiceWSPaynamics, last_sent...")
            log.debug(self.ws_client.last_sent())
            log.debug("CCServiceWSPaynamics, last_received...")
            log.debug(self.ws_client.last_received())
            log.debug("CCServiceWSPaynamics, do_requery, requery_response.application, {}...".format(requery_response_application))
            log.debug("CCServiceWSPaynamics, do_requery, requery_response.responseStatus, {}...".format(requery_response_status))
            log.debug("CCServiceWSPaynamics, do_requery, requery_response.txns, {}...".format(requery_response_txns))

            # paynamics requery response outside txns.
            results['rebill_id'] = ''
            results['merchantid'] = requery_response_application.merchantid
            results['request_id'] = requery_response_application.request_id
            results['response_id'] = requery_response_application.response_id
            results['timestamp'] = requery_response_application.timestamp
            results['signature'] = requery_response_application.signature
            results['response_code'] = requery_response_status.response_code
            results['response_message'] = requery_response_status.response_message
            results['response_advise'] = requery_response_status.response_advise

            # paynamics requery response inside txns.
            if requery_response_txns:
                txns_counter = 0

                for txns in requery_response_txns.ServiceResponse:
                    txns_application = txns.application
                    txns_status = txns.responseStatus

                    log.debug("CCServiceWSPaynamics, do_requery, txns_application, {}...".format(txns_application))
                    log.debug("CCServiceWSPaynamics, do_requery, txns_status, {}...".format(txns_status))

                    results_txns_key = 'txns::%d' % txns_counter
                    results[results_txns_key] = {}
                    results[results_txns_key]['merchantid'] = txns_application.merchantid
                    results[results_txns_key]['request_id'] = txns_application.request_id
                    results[results_txns_key]['response_id'] = txns_application.response_id
                    results[results_txns_key]['timestamp'] = txns_application.timestamp
                    results[results_txns_key]['response_code'] = txns_status.response_code
                    results[results_txns_key]['response_message'] = txns_status.response_message
                    results[results_txns_key]['response_advise'] = txns_status.response_advise

                    # add cc tokenization details
                    if hasattr(txns, 'MetaData'):
                        txns_metadata = txns.MetaData
                        log.debug("CCServiceWSPaynamics, do_requery, txns_metadata, {}...".format(txns_metadata))
                        if hasattr(txns_metadata, 'SubItem'):
                            for sub_item in txns_metadata.SubItem:
                                results[results_txns_key][sub_item.item] = sub_item.value

                    txns_counter += 1
            """
        except suds.WebFault, e:
            log.warn("ERROR!, CCServiceWSPaynamics, do_requery, failed paynamics requery...")
            log.error(e)
        except Exception, e:
            log.warn("ERROR!, CCServiceWSPaynamics, do_requery, something went wrong...")
            log.error(e)

        log.debug("CCServiceWSPaynamics, do_requery, results, {}...".format(results))

        return results

    def do_rebill_with_token(self, merchant_id, request_id, ip_address, response_id, token_id, trx_type,
            amount, notification_url, response_url, signature):

        log.debug("CCServiceWSPaynamics, do_rebill_with_token, start...")
        log.info("CCServiceWSPaynamics, do_rebill_with_token, merchant_id, %s..." % merchant_id)
        log.info("CCServiceWSPaynamics, do_rebill_with_token, request_id, %s..." % request_id)
        log.info("CCServiceWSPaynamics, do_rebill_with_token, ip_address, %s..." % ip_address)
        log.info("CCServiceWSPaynamics, do_rebill_with_token, token_id, %s..." % token_id)
        log.info("CCServiceWSPaynamics, do_rebill_with_token, trx_type, %s..." % trx_type)
        log.info("CCServiceWSPaynamics, do_rebill_with_token, amount, %s..." % amount)
        log.info("CCServiceWSPaynamics, do_rebill_with_token, notification_url, %s..." % notification_url)
        log.info("CCServiceWSPaynamics, do_rebill_with_token, response_url, %s..." % response_url)
        log.info("CCServiceWSPaynamics, do_rebill_with_token, signature, %s..." % signature)

        results = {}
        results['message'] = 'Sorry, an error occurred. Please try re-launching the app or contact support@gmovies.ph.'
        results['status'] = 'error'

        try:
            log.debug("CCServiceWSPaynamics, do_rebill_with_token, START REBILLING")

            self.header_host = PAYNAMICS_CCSERVICE_HEADER_HOST
            self.header_action = PAYNAMICS_CCSERVICE_HEADER_ACTION_REBILL_WTOKEN
            self.xml_request = PAYNAMICS_CCSERVICE_XML_REBILL_WTOKEN

            self.xml_request = self.xml_request.format(merchant_id, request_id, ip_address, response_id,
                token_id, trx_type, amount, notification_url, response_url, signature)

            encoded_request = self.xml_request.encode('utf-8')
            log.info("CCServiceWSPaynamics, do_rebill_with_token, encoded_request")
            log.info(encoded_request)

            headers = {"Host": self.header_host,
                "Content-Type": "text/xml; charset=utf-8",
                "Content-Length": str(len(encoded_request)),
                "SOAPAction": self.header_action
            }

            rebill_wtoken_response = requests.post(url = self.wsdl, headers = headers, data = encoded_request, timeout = 60)
            log.info("CCServiceWSPaynamics, do_rebill_with_token, rebill_wtoken_response")
            log.info(rebill_wtoken_response.status_code)
            log.info(rebill_wtoken_response.content)

            if 200 != rebill_wtoken_response.status_code:
                return results

            results['response_code']    = ''
            results['response_message'] = ''
            results['response_advise']  = ''
            results['response_id']      = ''
            results['status']           = 'success'

            results = parse_xml_response(rebill_wtoken_response.content, results)

            """
            rebill_wtoken_response = self.ws_client.service.rebill_with_token(merchantid=merchant_id,
                    request_id=request_id, ip_address=ip_address, org_trxid=response_id, token_id=token_id,
                    trx_type=trx_type, amount=amount, notification_url=notification_url,
                    response_url=response_url, signature=signature)

            log.debug(rebill_wtoken_response)
            if not rebill_wtoken_response:
                log.debug("CCServiceWSPaynamics, do_rebill_with_token, rebill_wtoken_response is None")
                results['exception_msg'] = "do_rebill_with_token, rebill_wtoken_response is None"
                return results

            rebill_wtoken_response_application = rebill_wtoken_response.application if hasattr(rebill_wtoken_response, 'application') else None
            rebill_wtoken_response_status = rebill_wtoken_response.responseStatus if hasattr(rebill_wtoken_response, 'responseStatus') else None
            if (rebill_wtoken_response_application is None) or (rebill_wtoken_response_status is None):
                log.debug("CCServiceWSPaynamics, do_rebill_with_token, rebill_wtoken_response_application or rebill_wtoken_response_status is None")
                results['exception_msg'] = "do_rebill_with_token, rebill_wtoken_response_application or rebill_wtoken_response_status is None"
                return results

            log.debug("CCServiceWSPaynamics, do_rebill_with_token, rebill_wtoken_response.application, {}...".format(rebill_wtoken_response_application))
            log.debug("CCServiceWSPaynamics, do_rebill_with_token, rebill_wtoken_response.responseStatus, {}...".format(rebill_wtoken_response_status))

            results['response_code'] = rebill_wtoken_response_status.response_code
            results['response_message'] = rebill_wtoken_response_status.response_message
            results['response_advise'] = rebill_wtoken_response_status.response_advise
            results['response_id'] = rebill_wtoken_response_application.response_id
            results['status'] = 'success'
            """

        except suds.WebFault, e:
            log.warn("ERROR!, CCServiceWSPaynamics, do_rebill_with_token, failed paynamics requery...")
            log.error(e)
            results['exception_msg'] = str(e)
        except Exception, e:
            log.warn("ERROR!, CCServiceWSPaynamics, do_rebill_with_token, something went wrong...")
            log.error(e)
            results['exception_msg'] = str(e)

        log.debug("CCServiceWSPaynamics, do_rebill_with_token, results, {}...".format(results))

        return results

    def do_rebill_with_token_3d(self, merchant_id, request_id, ip_address, response_id, token_id, trx_type,
            amount, notification_url, response_url, secure3d_policy, signature):

        log.debug("CCServiceWSPaynamics, do_rebill_with_token_3d, start...")
        log.info("CCServiceWSPaynamics, do_rebill_with_token_3d, merchant_id, %s..." % merchant_id)
        log.info("CCServiceWSPaynamics, do_rebill_with_token_3d, request_id, %s..." % request_id)
        log.info("CCServiceWSPaynamics, do_rebill_with_token_3d, ip_address, %s..." % ip_address)
        log.info("CCServiceWSPaynamics, do_rebill_with_token_3d, org_trxid, %s..." % response_id)
        log.info("CCServiceWSPaynamics, do_rebill_with_token_3d, token_id, %s..." % token_id)
        log.info("CCServiceWSPaynamics, do_rebill_with_token_3d, trx_type, %s..." % trx_type)
        log.info("CCServiceWSPaynamics, do_rebill_with_token_3d, amount, %s..." % amount)
        log.info("CCServiceWSPaynamics, do_rebill_with_token_3d, notification_url, %s..." % notification_url)
        log.info("CCServiceWSPaynamics, do_rebill_with_token_3d, response_url, %s..." % response_url)
        log.info("CCServiceWSPaynamics, do_rebill_with_token_3d, secure3d_policy, %s..." % secure3d_policy)
        log.info("CCServiceWSPaynamics, do_rebill_with_token_3d, signature, %s..." % signature)

        results = {}
        results['message'] = 'Sorry, an error occurred. Please try re-launching the app or contact support@gmovies.ph.'
        results['status'] = 'error'

        try:
            log.debug("CCServiceWSPaynamics, do_rebill_with_token_3d, START REBILLING")

            self.header_host = PAYNAMICS_CCSERVICE_HEADER_HOST
            self.header_action = PAYNAMICS_CCSERVICE_HEADER_ACTION_REBILL_WTOKEN_3D
            self.xml_request = PAYNAMICS_CCSERVICE_XML_REBILL_WTOKEN_3D

            self.xml_request = self.xml_request.format(merchant_id, request_id, ip_address, response_id,
                token_id, trx_type, amount, notification_url, response_url, secure3d_policy, signature)

            encoded_request = self.xml_request.encode('utf-8')
            log.info("CCServiceWSPaynamics, do_rebill_with_token_3d, encoded_request")
            log.info(encoded_request)

            headers = {"Host": self.header_host,
                "Content-Type": "text/xml; charset=utf-8",
                "Content-Length": str(len(encoded_request)),
                "SOAPAction": self.header_action
            }
            log.info(headers)

            rebill_wtoken_response = requests.post(url = self.wsdl, headers = headers, data = encoded_request, timeout = 60)
            log.info("CCServiceWSPaynamics, do_rebill_with_token_3d, rebill_wtoken_response")
            log.info(rebill_wtoken_response.status_code)
            log.info(rebill_wtoken_response.content)

            if 200 != rebill_wtoken_response.status_code:
                return results

            results['response_code']        = ''
            results['response_message']     = ''
            results['response_advise']      = ''
            results['response_id']          = ''
            results['secure_page_redirect'] = ''
            results['securePageRedirect']   = ''
            results['status']               = 'success'

            results = parse_xml_response(rebill_wtoken_response.content, results)
            results['secure_page_redirect'] = urllib.unquote(results['securePageRedirect'])

            """
            rebill_wtoken_response = self.ws_client.service.rebill_with_token_3d(merchantid=merchant_id,
                    request_id=request_id, ip_address=ip_address, org_trxid=response_id, token_id=token_id,
                    trx_type=trx_type, amount=amount, notification_url=notification_url,
                    response_url=response_url, secure3d_policy=secure3d_policy, signature=signature)

            log.debug("CCServiceWSPaynamics, last_sent...")
            log.debug(self.ws_client.last_sent())
            log.debug("CCServiceWSPaynamics, last_received...")
            log.debug(self.ws_client.last_received())
            log.debug(rebill_wtoken_response)
            if not rebill_wtoken_response:
                log.debug("CCServiceWSPaynamics, do_rebill_with_token_3d, rebill_wtoken_response is None")
                results['exception_msg'] = "do_rebill_with_token_3d, rebill_wtoken_response is None"
                return results

            rebill_wtoken_response_application = rebill_wtoken_response.application if hasattr(rebill_wtoken_response, 'application') else None
            rebill_wtoken_response_status = rebill_wtoken_response.responseStatus if hasattr(rebill_wtoken_response, 'responseStatus') else None
            rebill_wtoken_secure3d = rebill_wtoken_response.secure3d if hasattr(rebill_wtoken_response, 'secure3d') else None
            if ((rebill_wtoken_response_application is None) or (rebill_wtoken_response_status is None)
                    or (rebill_wtoken_secure3d is None)):
                log.debug("CCServiceWSPaynamics, do_rebill_with_token_3d, rebill_wtoken_response_application or rebill_wtoken_response_status or rebill_wtoken_secure3d is None")
                results['exception_msg'] = "do_rebill_with_token_3d, rebill_wtoken_response_application or rebill_wtoken_response_status or rebill_wtoken_secure3d is None"
                return results

            log.debug("CCServiceWSPaynamics, do_rebill_with_token_3d, rebill_wtoken_response.application, {}...".format(rebill_wtoken_response_application))
            log.debug("CCServiceWSPaynamics, do_rebill_with_token_3d, rebill_wtoken_response.responseStatus, {}...".format(rebill_wtoken_response_status))
            log.debug("CCServiceWSPaynamics, do_rebill_with_token_3d, rebill_wtoken_response.secure3d, {}...".format(rebill_wtoken_secure3d))

            results['response_code'] = rebill_wtoken_response_status.response_code
            results['response_message'] = rebill_wtoken_response_status.response_message
            results['response_advise'] = rebill_wtoken_response_status.response_advise
            results['response_id'] = rebill_wtoken_response_application.response_id
            results['secure_page_redirect'] = urllib.unquote(rebill_wtoken_secure3d.securePageRedirect)
            results['status'] = 'success'
            """

        except suds.WebFault, e:
            log.warn("ERROR!, CCServiceWSPaynamics, do_rebill_with_token_3d, failed paynamics requery...")
            log.error(e)
            results['exception_msg'] = str(e)
        except Exception, e:
            log.warn("ERROR!, CCServiceWSPaynamics, do_rebill_with_token_3d, something went wrong...")
            log.error(e)
            results['exception_msg'] = str(e)

        log.debug("CCServiceWSPaynamics, do_rebill_with_token_3d, results, {}...".format(results))

        return results

    def do_refund(self, merchant_id, request_id, ip_address, response_id,
            amount, notification_url, response_url, signature):

        log.debug("CCServiceWSPaynamics, do_refund, start...")
        log.info("CCServiceWSPaynamics, do_refund, merchant_id, %s..." % merchant_id)
        log.info("CCServiceWSPaynamics, do_refund, request_id, %s..." % request_id)
        log.info("CCServiceWSPaynamics, do_refund, ip_address, %s..." % ip_address)
        log.info("CCServiceWSPaynamics, do_refund, org_trxid, %s..." % response_id)
        log.info("CCServiceWSPaynamics, do_refund, amount, %s..." % amount)
        log.info("CCServiceWSPaynamics, do_refund, notification_url, %s..." % notification_url)
        log.info("CCServiceWSPaynamics, do_refund, response_url, %s..." % response_url)
        log.info("CCServiceWSPaynamics, do_refund, signature, %s..." % signature)

        results = {}
        results['message'] = 'Sorry, an error occurred. Please try re-launching the app or contact support@gmovies.ph.'
        results['status'] = 'error'

        try:
            log.debug("CCServiceWSPaynamics, do_refund, START")

            self.header_host = PAYNAMICS_CCSERVICE_HEADER_HOST
            self.header_action = PAYNAMICS_CCSERVICE_HEADER_ACTION_REFUND
            self.xml_request = PAYNAMICS_CCSERVICE_XML_REFUND

            self.xml_request = self.xml_request.format(merchant_id, request_id, ip_address, response_id,
                amount, notification_url, response_url, signature)

            encoded_request = self.xml_request.encode('utf-8')
            log.info("CCServiceWSPaynamics, do_refund, encoded_request")
            log.info(encoded_request)

            headers = {"Host": self.header_host,
                "Content-Type": "text/xml; charset=utf-8",
                "Content-Length": str(len(encoded_request)),
                "SOAPAction": self.header_action
            }

            refund_response = requests.post(url = self.wsdl, headers = headers, data = encoded_request, timeout = 60)
            log.info("CCServiceWSPaynamics, do_refund, refund_response")
            log.info(refund_response.status_code)
            log.info(refund_response.content)

            if 200 != refund_response.status_code:
                return results

            results['response_code']    = ''
            results['response_message'] = ''
            results['response_advise']  = ''
            results['response_id']      = ''
            results['status']           = 'success'

            results = parse_xml_response(refund_response.content, results)

            """
            refund_response = self.ws_client.service.refund(merchantid=merchant_id,
                    request_id=request_id, ip_address=ip_address, org_trxid=response_id,
                    amount=amount, notification_url=notification_url,
                    response_url=response_url, signature=signature)

            log.debug(refund_response)
            if not refund_response:
                log.debug("CCServiceWSPaynamics, do_refund, refund_response is None")
                results['exception_msg'] = "do_refund, refund_response is None"
                return results

            refund_response_application = refund_response.application if hasattr(refund_response, 'application') else None
            refund_response_status = refund_response.responseStatus if hasattr(refund_response, 'responseStatus') else None
            if (refund_response_application is None) or (refund_response_status is None):
                log.debug("CCServiceWSPaynamics, do_refund, refund_response_application or refund_response_status is None")
                results['exception_msg'] = "do_refund, refund_response_application or refund_response_status is None"
                return results

            log.debug("CCServiceWSPaynamics, do_refund, refund_response.application, {}...".format(refund_response_application))
            log.debug("CCServiceWSPaynamics, do_refund, refund_response.responseStatus, {}...".format(refund_response_status))

            results['response_code'] = refund_response_status.response_code
            results['response_message'] = refund_response_status.response_message
            results['response_advise'] = refund_response_status.response_advise
            results['response_id'] = refund_response_application.response_id
            results['status'] = 'success'
            """

        except suds.WebFault, e:
            log.warn("ERROR!, CCServiceWSPaynamics, do_refund, failed paynamics requery...")
            log.error(e)
            results['exception_msg'] = str(e)
        except Exception, e:
            log.warn("ERROR!, CCServiceWSPaynamics, do_refund, something went wrong...")
            log.error(e)
            results['exception_msg'] = str(e)

        log.debug("CCServiceWSPaynamics, do_refund, results, {}...".format(results))

        return results

    def do_reversal(self, merchant_id, request_id, ip_address, response_id,
            amount, notification_url, response_url, signature):

        log.debug("CCServiceWSPaynamics, do_reversal, start...")
        log.info("CCServiceWSPaynamics, do_reversal, merchant_id, %s..." % merchant_id)
        log.info("CCServiceWSPaynamics, do_reversal, request_id, %s..." % request_id)
        log.info("CCServiceWSPaynamics, do_reversal, ip_address, %s..." % ip_address)
        log.info("CCServiceWSPaynamics, do_reversal, org_trxid, %s..." % response_id)
        log.info("CCServiceWSPaynamics, do_reversal, amount, %s..." % amount)
        log.info("CCServiceWSPaynamics, do_reversal, notification_url, %s..." % notification_url)
        log.info("CCServiceWSPaynamics, do_reversal, response_url, %s..." % response_url)
        log.info("CCServiceWSPaynamics, do_reversal, signature, %s..." % signature)

        results = {}
        results['message'] = 'Sorry, an error occurred. Please try re-launching the app or contact support@gmovies.ph.'
        results['status'] = 'error'

        try:
            log.debug("CCServiceWSPaynamics, do_reversal, START")

            self.header_host = PAYNAMICS_CCSERVICE_HEADER_HOST
            self.header_action = PAYNAMICS_CCSERVICE_HEADER_ACTION_REVERSAL
            self.xml_request = PAYNAMICS_CCSERVICE_XML_REVERSAL

            self.xml_request = self.xml_request.format(merchant_id, request_id, ip_address, response_id,
                amount, notification_url, response_url, signature)

            encoded_request = self.xml_request.encode('utf-8')
            log.info("CCServiceWSPaynamics, do_reversal, encoded_request")
            log.info(encoded_request)

            headers = {"Host": self.header_host,
                "Content-Type": "text/xml; charset=utf-8",
                "Content-Length": str(len(encoded_request)),
                "SOAPAction": self.header_action
            }

            reversal_response = requests.post(url = self.wsdl, headers = headers, data = encoded_request, timeout = 60)
            log.info("CCServiceWSPaynamics, do_reversal, reversal_response")
            log.info(reversal_response.status_code)
            log.info(reversal_response.content)

            if 200 != reversal_response.status_code:
                return results

            results['response_code']    = ''
            results['response_message'] = ''
            results['response_advise']  = ''
            results['response_id']      = ''
            results['status']           = 'success'

            results = parse_xml_response(reversal_response.content, results)

            """
            reversal_response = self.ws_client.service.reversal(merchantid=merchant_id,
                    request_id=request_id, ip_address=ip_address, org_trxid=response_id,
                    amount=amount, notification_url=notification_url,
                    response_url=response_url, signature=signature)

            log.debug(reversal_response)
            if not reversal_response:
                log.debug("CCServiceWSPaynamics, do_reversal, reversal_response is None")
                results['exception_msg'] = "do_reversal, reversal_response is None"
                return results

            reversal_response_application = reversal_response.application if hasattr(reversal_response, 'application') else None
            reversal_response_status = reversal_response.responseStatus if hasattr(reversal_response, 'responseStatus') else None
            if (reversal_response_application is None) or (reversal_response_status is None):
                log.debug("CCServiceWSPaynamics, do_reversal, reversal_response_application or reversal_response_status is None")
                results['exception_msg'] = "do_reversal, reversal_response_application or reversal_response_status is None"
                return results

            log.debug("CCServiceWSPaynamics, do_reversal, reversal_response.application, {}...".format(reversal_response_application))
            log.debug("CCServiceWSPaynamics, do_reversal, reversal_response.responseStatus, {}...".format(reversal_response_status))

            results['response_code'] = reversal_response_status.response_code
            results['response_message'] = reversal_response_status.response_message
            results['response_advise'] = reversal_response_status.response_advise
            results['response_id'] = reversal_response_application.response_id
            results['status'] = 'success'
            """

        except suds.WebFault, e:
            log.warn("ERROR!, CCServiceWSPaynamics, do_reversal, failed paynamics requery...")
            log.error(e)
            results['exception_msg'] = str(e)
        except Exception, e:
            log.warn("ERROR!, CCServiceWSPaynamics, do_reversal, something went wrong...")
            log.error(e)
            results['exception_msg'] = str(e)

        log.debug("CCServiceWSPaynamics, do_reversal, results, {}...".format(results))

        return results