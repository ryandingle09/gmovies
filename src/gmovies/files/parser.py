from gmovies import models, orgs
from google.appengine.ext import ndb
from uuid import uuid4 as uuid_gen
import datetime
import csv

NUMBER_OF_THEATER_VALUES = 5
NUMBER_OF_MOVIE_VALUES = 6
NUMBER_OF_SCHEDULE_VALUES = 6

def parse_theater_file(file, filename, theaterorg):
    theater_list = []
    error_list = []
    line_number = 0
    csv_file = csv.reader(file)
    for values_list in csv_file:
        line_number += 1
        try:
            check_number_of_values(values_list, NUMBER_OF_THEATER_VALUES)
            check_values_list(values_list)
            theater = values_list[0]
            geo_pt = check_geo_pt(values_list[1])
            theater_entity = get_existing_theater(theater, theaterorg)
            if theater_entity is None:
                cinema = create_cinema(values_list)
                theater_key = create_new_theater(theater, cinema, geo_pt, theaterorg)
                theater_list.append(theater_key)
            else:
                add_cinema(theater_entity.key, values_list)
        except (IndexError, ValueError), e:
            errorMsg = 'Error occurred at file:%s line#%s. Please check for formatting / value errors' % (filename, line_number)
            error_list.append(errorMsg)
    return (theater_list, error_list)

# parse into a dictionary to pass to movie heuristics
def parse_movie_file(file, filename):
    movie_list = []
    error_list = []
    line_number = 0
    csv_file = csv.reader(file)
    for values_list in csv_file:
        line_number += 1
        try:
            check_number_of_values(values_list, 6)
            check_values_list(values_list)
            movie_dict = {}
            movie_dict['id'] = values_list[0]
            movie_dict['movie_title'] = values_list[1]
            movie_dict['cast'] = convert_to_list(values_list[2])
            movie_dict['synopsis'] = values_list[3]
            movie_dict['runtime_mins'] = float(values_list[4])
            # parse ratings
            movie_dict['ratings'] = check_ratings(values_list[5])
            movie_list.append(movie_dict)
        except (IndexError, ValueError), e:
            errorMsg = 'Error occurred at file:%s line#%s. Please check for formatting / value errors' % (filename, line_number)
            error_list.append(errorMsg)
    return (movie_list, error_list)

#parse into a dictionary to pass to schedule transformer
def parse_schedule_file(file, filename):
    schedule_list = []
    error_list = []
    line_number = 0
    csv_file = csv.reader(file)
    for values_list in csv_file:
        line_number += 1
        try:
            check_number_of_values(values_list, NUMBER_OF_SCHEDULE_VALUES)
            schedule_dict = {}
            schedule_dict['id']=''
            schedule_dict['movie_title'] = values_list[0]
            schedule_dict['theater_code'] = values_list[1]
            schedule_dict['cinema'] = values_list[2]
            schedule_dict['screening'] = values_list[3]
            schedule_dict['uuid'] = orgs.GLOBE
            schedule_dict['price'] = values_list[4]
            schedule_dict['variant'] = values_list[5]
            schedule_list.append(schedule_dict)
        except (IndexError, ValueError), e:
            errorMsg = 'Error occurred at file:%s line#%s. Please check for formatting / value errors' % (filename, line_number)
            error_list.append(errorMsg)
    return (schedule_list, error_list)

# Parse upload csv for seatmap
def parse_seatmap_file(file, filename):
    seat_map = []
    seat_count = 0
    error_list = []
    line_number = 0
    csv_file = csv.reader(file)

    for row in csv_file:
        line_number += 1

        try:
            check_values_list(row)
            check_values_list_length(row)
            seat_count += len(filter(lambda seat: seat != 'b(1)', row))
            seat_map.append(row)
        except Exception, e:
            errorMsg = 'Error occurred at file:%s line#%s. Please check for formatting / value errors' % (filename, line_number)
            error_list.append(errorMsg)
    return seat_map, seat_count, error_list

def parse_screening(screening):
    datetime.datetime.strptime(screening, '%m/%d/%Y %I:%M:%S %p')
    return screening

def check_geo_pt(geo_pt):
    check_number_of_values(geo_pt.split('-'), 2)
    return convert_to_list(geo_pt)

def check_values_list(values_list):
    for value in values_list:
        if value == '':
            raise ValueError

def check_values_list_length(values_list):
    if len(values_list) == 0:
        raise ValueError

def check_number_of_values(values_list, value):
    if len(values_list) != value:
        raise ValueError

def check_ratings(ratings):
    check_number_of_values(ratings.split('-'), 2)
    return convert_to_list(ratings)

# formats 'A1-A2' to ['A1', 'A2']
# also formats 'Leonardo_Di_Caprio-Ken_Watanabe' to ['Leonardo Di Caprio', 'Ken Watanabe']
def convert_to_list(value):
    values_list = value.split('-')
    check_values_list(values_list)
    return values_list

def create_cinema(values_list):
    cinema = models.Cinema()
    cinema.name = values_list[2]
    cinema.seat_count = str(values_list[3])
    cinema.seat_map = convert_to_list(values_list[4])
    return cinema

def create_new_theater(theater_name, cinema, geo_pt, theaterorg):
    theaterorg_key = ndb.Key(models.TheaterOrganization, str(theaterorg))
    theater = models.Theater(name=theater_name, parent=theaterorg_key)
    theater.cinemas.append(cinema)
    theater.location = ndb.GeoPt(geo_pt[0], geo_pt[1])
    theater.put()
    return theater.key

# might be better to move to a util class, since this is similar to the theaters feed parser
def get_existing_theater(theater_name, theaterorg):
    parent_key = ndb.Key(models.TheaterOrganization, str(theaterorg))
    theater = models.Theater.query(models.Theater.name == theater_name, ancestor=parent_key).fetch(1)
    if len(theater) > 0:
        return theater[0]
    return None

def add_cinema(theater_key, values_list):
    theater = theater_key.get()
    cinema_names = [ c.name for c in theater.cinemas ]
    if values_list[2] not in cinema_names:
        cinema = create_cinema(values_list)
        theater.cinemas.append(cinema)
        theater.put()
