import geohash
import logging
import sys
import time

from datetime import date, datetime
from operator import attrgetter

from flask import Blueprint, jsonify, request

from google.appengine.api import memcache, search
from google.appengine.ext.ndb import AND, Key, get_multi
from google.appengine.runtime.apiproxy_errors import OverQuotaError

from .auth import authenticate_request
from gmovies import models, orgs
from gmovies.admin import notify_admin
from gmovies.exceptions.api import *
from gmovies.routes.api import (check_filter_and_sort_args,
        do_computed_distance, do_filter, do_get_schedules,
        do_location_search, org_with_theaters, theaterorg_theater_safety_check,
        DEFAULT_DIAMETER)
from gmovies.settings import API_VERSIONS_DEPRECATED, ENFORCE_AUTH
from gmovies.util import admin
from gmovies.util.id_encoder import decode_uuid, encode_uuid, encoded_theater_id, decoded_theater_id


log = logging.getLogger(__name__)
api_ver2 = Blueprint('api_ver2', __name__, template_folder='api_templates')

DEVICE_ID = 'X-GMovies-DeviceId'
AUTH_CLIENT_ID = 'X-GMovies-Auth-ClientId'
AUTH_TIMESTAMP = 'X-GMovies-Auth-Timestamp'
AUTH_SIGNATURE = 'X-GMovies-Auth-Signature'

API_VERSION = 'API2'


def abort(status_code, message='Error', error_code='ERROR_UNKNOWN',
          details=None, headers={}):
    raise APIException(status_code, message, error_code, details)


@api_ver2.errorhandler(AlreadyCancelledException)
def handle_already_cancelled_error(e):
    res = jsonify(message=e.message, error_code=e.error_code, details=e.details)
    res.status_code = e.code
    for m in e.details:
        res.headers.add('Allow', m)
    return res


@api_ver2.errorhandler(APIException)
def handle_api_error(e):
    res = jsonify(message=e.message, error_code=e.error_code, details=e.details)
    res.status_code = e.code
    return res


@api_ver2.errorhandler(OverQuotaError)
def handle_quota_error(e):
    res = jsonify(message='The backend has reached a resource quota. Please try again later.',
                  error_code='SYSTEM_OVER_QUOTA', details=e.message)
    res.status_code = 500
    notify_admin("API", e, sys.exc_info())
    return res


@api_ver2.errorhandler(Exception)
def handle_system_error(e):
    res = jsonify(message='There has been a system error. Please try again later.',
                  error_code='SYSTEM_GENERIC_ERROR', details=e.message)
    log.exception(e)
    res.status_code = 500
    notify_admin("API", e, sys.exc_info())
    return res

@api_ver2.before_request
def check_device_id():
    if DEVICE_ID not in request.headers:
        raise DeviceIdRequiredException()

@api_ver2.before_request
def check_api_version():
    log.debug("check_api_version, api version2...")

    if API_VERSION in API_VERSIONS_DEPRECATED:
        raise abort(401, error_code='API_VERSION_DEPRECATED', message='Client API version deprecated.')

@api_ver2.before_request
def log_caller_info():
    if 'User-Agent' in request.headers:
        ua = request.headers['User-Agent']
    else:
        ua = '(None)'
    if AUTH_CLIENT_ID in request.headers:
        client_id = request.headers[AUTH_CLIENT_ID]
    else:
        client_id = "(Not given)"
    device_id = request.headers[DEVICE_ID]
    log.info("API:\tUser-Agent: %s", ua)
    log.info("API:\tClient ID: %s\n\tDevice ID: %s" % (client_id, device_id))


@api_ver2.route('/')
def api_meta():
    return jsonify({ 'version' : '2.0' })


def query_model(cls, entity_name, request, id_encoder=lambda x:x.id(), is_minimum_info=False):
    check_filter_and_sort_args(cls, request.args)
    f, q = query_filters(cls, request)

    if cls == models.Theater:
        if is_minimum_info:
            l = [{'id': id_encoder(x.key), entity_name: x.to_minimum_entity()} for x in q]
        else:
            l = [{'id': id_encoder(x.key), entity_name: x.to_entity()} for x in q]
    else:
        l = [{'id': id_encoder(x.key), entity_name: x.to_entity()} for x in q]

    return (f, l)


def query_filters(cls, request):
    if cls == models.Theater:
        f = {}
        q = []
        args = request.args.copy()
        theaterorgs = models.TheaterOrganization.query(
                models.TheaterOrganization.is_published == True)

        for org in theaterorgs:
            theaters = cls.query(cls.is_published == True, ancestor=org.key)
            f, theaters = do_filter(cls, theaters, request.args)
            q.extend(theaters.fetch())

        # sort again
        if 'sort' in args:
            sort_cols = args.getlist('sort')

            if 'name' in sort_cols:
                q = sorted(q, key=attrgetter('name'))
    else:
        q = cls.query()
        f, q = do_filter(cls, q, request.args)

    return (f, q)


def do_get_distance(latitude, longitude, theater_keys, request, is_minimum_info=False):
    results = []

    if 'diameter' in request.args:
        diameter = request.args['diameter']
    else:
        diameter = DEFAULT_DIAMETER

    for theater in get_multi(theater_keys):
        if theater is not None:
            distance = do_computed_distance(latitude, longitude, theater)
            distance_in_meters = distance * float(1000)

            if round(distance_in_meters) < float(diameter):
                if is_minimum_info:
                    theater_dict = theater.to_minimum_entity()
                else:
                    theater_dict = theater.to_entity()

                theater_dict['distance'] = distance

                results.append({'id': encoded_theater_id(theater.key),
                        'theater': theater_dict})

    return results


@api_ver2.route('/theaters/')
def get_theaters():
    if (('location.lon' in request.args and  'location.lat' in request.args) or
            ('cinemas.location.lon' in request.args 
                and 'cinemas.location.lat' in request.args)):
        lat, lon, theater_keys = do_location_search(request)
        theater_list = do_get_distance(lat, lon, theater_keys, request)
        filters = {'match': {'location.lat': lat, 'location.lon': lon}}
    else:
        filters, theater_list = query_model(models.Theater, 'theater',
                request, id_encoder=encoded_theater_id)

    return jsonify({'filters': filters, 'results': theater_list})


@api_ver2.route('/theaters/min/')
def get_theaters_minimum():
    if (('location.lon' in request.args and  'location.lat' in request.args) or
            ('cinemas.location.lon' in request.args and 'cinemas.location.lat' in request.args)):
        lat, lon, theater_keys = do_location_search(request)
        theater_list = do_get_distance(lat, lon, theater_keys, request, is_minimum_info=True)
        filters = {'match': {'location.lat': lat, 'location.lon': lon}}
    else:
        filters, theater_list = query_model(models.Theater, 'theater',
                request, id_encoder=encoded_theater_id, is_minimum_info=True)

    # log.info("Theaters List: {}".format(theater_list))

    return jsonify({'filters': filters, 'results': theater_list})


@api_ver2.route('/theaterorgs/')
def get_theater_orgs():
    check_filter_and_sort_args(models.TheaterOrganization, request.args)

    # Default active theater organizations.
    ayala_key = Key(models.TheaterOrganization, str(orgs.AYALA_MALLS))
    festival_malls_key = Key(models.TheaterOrganization, str(orgs.FESTIVAL_MALLS))
    greenhills_malls_key = Key(models.TheaterOrganization, str(orgs.GREENHILLS_MALLS))
    robinsons_malls_key = Key(models.TheaterOrganization, str(orgs.ROBINSONS_MALLS))
    rockwell_malls_key = Key(models.TheaterOrganization, str(orgs.ROCKWELL_MALLS))
    gateway_malls_key = Key(models.TheaterOrganization, str(orgs.GATEWAY_MALLS))
    megaworld_malls_key = Key(models.TheaterOrganization, str(orgs.MEGAWORLD_MALLS))
    sm_malls_key = Key(models.TheaterOrganization, str(orgs.SM_MALLS))
    shangrila_malls_key = Key(models.TheaterOrganization, str(orgs.SHANGRILA_MALLS))

    # Default in-active theater organizations.
    globe_key = Key(models.TheaterOrganization, str(orgs.GLOBE))
    rotten_tomatoes_key = Key(models.TheaterOrganization, str(orgs.ROTTEN_TOMATOES))

    non_ayala_malls = [megaworld_malls_key, rockwell_malls_key,
            greenhills_malls_key, sm_malls_key, gateway_malls_key,
            shangrila_malls_key, festival_malls_key, robinsons_malls_key]

    # Ayala Malls Theater Org should be first on the list due to inability of
    # the app to reorder the resulted list. We'll perform two query to do this.

    # Query Ayala Malls First
    query_ayala = models.TheaterOrganization.query(models.TheaterOrganization.key == ayala_key)
    f, q1 = do_filter(models.TheaterOrganization, query_ayala, request.args)
    l1 = [{'id': org.key.id(), 'theater_organization': org_with_theaters(org)}
            for org in q1]

    log.info("Ayala Malls: {}".format(l1))

    # Query Non-Ayala Malls
    query_non_ayala = models.TheaterOrganization.query(models.TheaterOrganization.key.IN(non_ayala_malls))
    _, q2 = do_filter(models.TheaterOrganization, query_non_ayala, request.args)
    l2 = [{'id': org.key.id(), 'theater_organization': org_with_theaters(org)}
            for org in q2]

    log.info("Non-Ayala Malls: {}".format(l1))

    l1.extend(l2) # combine query results

    log.info("Theaters List: {}".format(l1) )

    return jsonify({ 'filters': f, 'results': l1 })


@api_ver2.route('/theaterorgs/<theaterorg_id>/theaters/')
def get_theaterorg_theaters(theaterorg_id):
    org_key = Key(models.TheaterOrganization, theaterorg_id)
    org = org_key.get()

    if not org:
        raise NotFoundException(details={ 'theaterorg_id':
                                          theaterorg_id })

    if (('location.lon' in request.args and
        'location.lat' in request.args) or
        ('cinemas.location.lon' in request.args
         and 'cinemas.location.lat' in request.args)):
        lat, lon, theater_keys = do_location_search(request)
        org_theater_keys = models.Theater.query(ancestor=org_key).fetch(keys_only=True)

        matched_keys = set(theater_keys).intersection(set(org_theater_keys))
        theater_list = do_get_distance(lat, lon, matched_keys, request)
        filters = {'match': {'location.lat': lat, 'location.lon': lon}}
    else:
        check_filter_and_sort_args(models.Theater, request.args)
        q = models.Theater.query(ancestor=org_key)
        filters, q = do_filter(models.Theater, q, request.args)
        theater_list = [{'id': encoded_theater_id(x.key), 'theater': x.to_entity()}
                         for x in q]
                         
    log.info("Theaters List: {}".format(theater_list) )
    return jsonify({ 'filters': filters, 'results': theater_list })


@api_ver2.route('/schedules')
def get_schedules():
    return do_get_schedules()


@api_ver2.route('/theaterorgs/<theaterorg_id>/theaters/<ignored>~<int:theater_id>/schedules')
def get_theater_org_theater_schedules(theaterorg_id, ignored, theater_id):
    theaterorg_theater_safety_check(theaterorg_id, ignored, theater_id)
    return get_theater_schedules(ignored, theater_id)


@api_ver2.route('/theaters/<enc_org_uuid>~<int:theater_id>/schedules')
def get_theater_schedules(enc_org_uuid, theater_id):
    log.debug("Got theater ID pair %s %s" % (enc_org_uuid, theater_id))
    return do_get_schedules(theater_pair=(enc_org_uuid,theater_id))


@api_ver2.route('/movies/<movie_id>/schedules')
def get_movie_schedules(movie_id):
    return do_get_schedules(movie_id=movie_id)


@api_ver2.route('/entity~<entity_name>/version/')
def get_entity_version(entity_name):
    if entity_name == 'theater':
        queryset = models.Theater.query().order(-models.Theater.datetime_updated)
        theater = queryset.get()

        if theater:
            if theater.datetime_updated:
                dt = theater.datetime_updated
                ts = int(time.mktime(dt.timetuple()))
                version = str(ts)

                return jsonify({'version': version})

    return jsonify({'version': "0"})