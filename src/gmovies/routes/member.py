import logging

from flask import Blueprint, render_template, request

from google.appengine.ext.ndb import Key

from gmovies import models, orgs
from gmovies.admin import password_reset_confirmation
from gmovies.ws.connector import WSAuth, WSUser
from gmovies.ws.endpoints import LCT_SESSION_API, LCT_TRANSACTION_API


log = logging.getLogger(__name__)
member = Blueprint('member', __name__, template_folder='member_templates')


@member.route('/password/reset/<password_reset_id>/', methods=['GET'])
def password_reset(password_reset_id):
    message = ''
    password_reset_key = Key(models.MemberPasswordReset, int(password_reset_id))
    password_reset = password_reset_key.get()

    if password_reset is None:
        message = "Sorry, we couldn't find that password reset key in our database."

        return render_template('password_reset.html', message=message)

    if not password_reset.is_active:
        message = """Sorry, The password reset link was invalid.
                     Possibly because it has already been used,
                     or you requested another password reset."""

        return render_template('password_reset.html', message=message)

    theater_key = password_reset.theater
    theater = theater_key.get()

    # User registration token verification here
    auth = WSAuth(LCT_SESSION_API)
    session_token = auth.get_session_token()

    if not session_token:
        log.warn('Error aquiring session token')
        message = "Invalid Session Token"
    else:
        user = WSUser(LCT_TRANSACTION_API)
        status, data, _ = user.forgot_password(session_token=session_token, 
                email=password_reset.email)

        message = """Check your email for your new password.
                     If you don't receive an email this could mean you
                     signed up with different email address."""

        if status == 'success':
            password_reset.new_password = data

            password_reset_confirmation(user_email=password_reset.email,
                    new_password=data)

        password_reset.is_active = False
        password_reset.put()

    return render_template('password_reset.html', message=message)