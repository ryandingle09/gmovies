import functools
from flask import jsonify

from gmovies.exceptions.api import APIException

DEPRECATION_HEADER = 'X-GMovies-Deprecated'

def deprecated(date):
    def __wrapper(fn):
        @functools.wraps(fn)
        def __set_deprecation_header(*args):
            try:
                res = fn(*args)
                res.headers[DEPRECATION_HEADER] = date
                return res
            except APIException, e:
                res = jsonify(message=e.message, 
                              error_code=e.error_code,
                              details=e.details)
                res.status_code = e.code
                res.headers[DEPRECATION_HEADER] = date
                return res

        return __set_deprecation_header
    return __wrapper



