import ast
import calendar
import datetime
import json
import logging
import pickle
import pytz
import StringIO
import xlwt
import pprint

from collections import OrderedDict
from decimal import Decimal
from operator import attrgetter
from uuid import uuid4 as uuid_gen

from flask import Blueprint, jsonify, render_template, flash, redirect, url_for, request, abort, make_response
from flask.ext.bootstrap import Bootstrap

from google.appengine.ext import ndb
from google.appengine.api import memcache
from google.appengine.api.images import get_serving_url

from gmovies import models, orgs
from gmovies.admin import send_ticket_details
from gmovies.admin.email import send_blocked_screening_ticket_details
from gmovies.admin.forms import (AllowDiscountCodesWhitelistForm, AlternatePostersForm, AnalyticsServiceForm,
        BackgroundPosterImageForm, BlockedScreeningAssetsForm, CinemaForm, ClientForm, ComingSoonAssetsForm,
        ConvenienceFeeSettingsForm, CSRCSettingForm, EditReservationForm, FeedForm, FileUploadForm, MovieForm, MovieTitlesForm,
        MovieTitlesLookupForm, MultiResImageBlobForm, PosterImageForm, PromoEditForm, PromoNewForm, PromoSeatsStatusForm,
        SecondaryMovieTitlesForm, SecondaryMovieTitlesLookupForm, ScheduleForm, ScheduleSlotForm, TheaterForm, TheaterOrgForm,
        PaymentSettingsForm, PaynamicsPaymentSettingsForm, MIGSPaymentSettingsForm, IPay88PaymentSettingsForm,
        PaymentOptionSettingsForm, SystemSettingsForm, PesoPayPaymentSettingsCinemaForm)
from gmovies.bank_transfer import AccountSettings
from gmovies.feeds.rottentomatoes import search_rottentomatoes_details, RT
from gmovies.feeds.images import get_image_blob_key, save_poster_image, save_assets_image
from gmovies.heuristics import movies, movies_old, rottentomatoes
from gmovies.models import (AnalyticsService, AnalyticsServiceHistory, AssetsImageBlob, ConvenienceFeeSettings, Client,
        ClientInitiatedPaymentSettings, CSRCPayment, Device, Feed, FeedCorrelation, Movie, MovieTitles, MovieTitlesLookup,
        PaymentOptionSettings, PosterImageBlob, Promo, PromoReservationTransaction, ReservationTransaction, SecondaryMovieTitles,
        Schedule, ScheduleTimeSlot, Theater, TheaterOrganization, TicketTemplateImageBlob, ClientInitiatedPaymentSettingsCinema)
from gmovies.settings import SystemSettings
from gmovies.tx.actions import update, refund, update_state
from gmovies.tx.promo import state as promo_state
from gmovies.tx.ticket import generate_sample_ticket_image
from gmovies.tx.state_machine import state, to_state_str
from gmovies.tx.util import trigger_send_email_blasts_txdone
from gmovies.util import admin, cache_movie_runtime, id_encoder, parse_cast


tz = pytz.timezone('Asia/Manila')
log = logging.getLogger(__name__)
web = Blueprint('admin_web', __name__, template_folder='admin_templates')

ALLOWED_EXTENSIONS = set(['csv', 'txt'])
THEATER_ORGS = {str(orgs.AYALA_MALLS): 'Ayala', str(orgs.ROCKWELL_MALLS): 'Rockwell',
        str(orgs.GREENHILLS_MALLS): 'Greenhills', str(orgs.SM_MALLS): 'SM Malls',
        str(orgs.MEGAWORLD_MALLS): 'Megaworld', str(orgs.ROBINSONS_MALLS): 'Robinsons',
        str(orgs.SHANGRILA_MALLS): 'Shangri-la', str(orgs.CINEMA2000_MALLS): 'Cinema 2000',
        str(orgs.FESTIVAL_MALLS): 'Festival', str(orgs.GATEWAY_MALLS): 'Gateway',
        str(orgs.GLOBE): 'Globe', str(orgs.ROTTEN_TOMATOES): 'Rotten Tomatoes',
        str(orgs.CINEMA76_MALLS): 'Cinema 76', str(orgs.GLOBE_EVENTS): 'Globe Events',
        str(orgs.CITY_MALLS): 'CityMall'}
FEEDS = {str(orgs.AYALA_MALLS): 'Sureseats', str(orgs.ROCKWELL_MALLS): 'Rockwell',
        str(orgs.GREENHILLS_MALLS): 'Greenhills', str(orgs.SM_MALLS): 'SM Malls',
        str(orgs.MEGAWORLD_MALLS): 'Megaworld', str(orgs.GLOBE): 'Globe',
        str(orgs.ROTTEN_TOMATOES): 'Rotten Tomatoes', str(orgs.ROBINSONS_MALLS): 'Robinsons',
        str(orgs.CINEMA76_MALLS): 'Cinema 76', str(orgs.GLOBE_EVENTS): 'Globe Events',
        str(orgs.CITY_MALLS): 'CityMall'}


@web.route('/')
def admin_index():
    return render_template('index.html')


###############
#
# Theater Organizations, Theaters, and Cinemas
#
###############

@web.route('/theaterorganizations/')
def theaterorg_index():
    theater_orgs = dict([(t.key.id(), t.name) for t in TheaterOrganization.query().fetch(projection=['name'])])

    for org_id in THEATER_ORGS:
        if org_id not in theater_orgs:
            theater_orgs[org_id] = THEATER_ORGS[org_id]

    ordered_theater_orgs = OrderedDict(sorted(theater_orgs.items(), key=lambda(k,v):(v,k)))

    return render_template('theaterorg_index.html', theater_orgs=ordered_theater_orgs)

@web.route('/theaterorganizations/new/')
def theaterorg_new():
    form = TheaterOrgForm()

    return render_template('theaterorg_new.html', form=form)

@web.route('/theaterorganizations/', methods=['POST'])
def theaterorg_create():
    form = TheaterOrgForm()

    if form.validate_on_submit() and admin.check_theaterorg(form.name.data):
        theaterorg = admin.create_theaterorg_params(form)
        memcache.set('theaterorg::new::key', str(theaterorg.key.id()))

        flash("Success! Created New Theater Organization!")

        return redirect(url_for('.theaterorg_index'))
    elif not admin.check_theaterorg(form.name.data):
        form.errors['generic'] = 'Error! Theater Organization Already Exists!'

    return render_template('theaterorg_new.html', form=form)

@web.route('/theaterorganizations/<org_id>/')
def theaterorg_show(org_id):
    theaterorg_key = ndb.Key(TheaterOrganization, org_id)
    theaterorg = theaterorg_key.get()

    if theaterorg is None:
        theaterorg = TheaterOrganization(name=THEATER_ORGS[org_id])
        theaterorg.key = ndb.Key(TheaterOrganization, org_id)
        theaterorg.put()

    theaters = Theater.query(ancestor=theaterorg_key).fetch(projection=['name'])
    form = TheaterOrgForm(obj=theaterorg)
    form.ticket_remarks.data = theaterorg.ticket_remarks
    theaterorg_ticket_template_image = TicketTemplateImageBlob.query(TicketTemplateImageBlob.template_type=='theaterorg', ancestor=theaterorg_key).get()

    return render_template('theaterorg_show.html', theater_org=theaterorg, org_id=org_id, form=form,
            theaters=theaters, theaterorg_ticket_template_image=theaterorg_ticket_template_image)

@web.route('/theaterorganizations/<org_id>/', methods=['POST'])
def theaterorg_update(org_id):
    theaterorg_key = ndb.Key(TheaterOrganization, org_id)
    theaterorg = theaterorg_key.get()
    form = TheaterOrgForm()

    if form.validate_on_submit():
        theaterorg.name = form.name.data
        theaterorg.pubkey = form.pubkey.data
        theaterorg.template = form.template.data
        theaterorg.movie_ticket_template = form.movie_ticket_template.data
        theaterorg.theaterorg_ticket_template = form.theaterorg_ticket_template.data
        theaterorg.access_code = form.access_code.data
        theaterorg.is_published = form.is_published.data
        theaterorg.sort_priority = form.sort_priority.data

        if not form.ticket_remarks.data:
            theaterorg.ticket_remarks = None
        else:
            theaterorg.ticket_remarks = ast.literal_eval(form.ticket_remarks.data)

        theaterorg.is_qrcode_ticket = form.is_qrcode_ticket.data
        theaterorg.is_old_ticket_template = form.is_old_ticket_template.data
        theaterorg.is_theaterorg_ticket_template = form.is_theaterorg_ticket_template.data
        f = request.files[form.template_image_file.name]

        if f:
            template_image = f.read()
            theaterorg.template_image = template_image

        theaterorg.put()
        theaterorg_template_image_file = request.files[form.theaterorg_template_image_file.name]

        if theaterorg_template_image_file:
            ticket_template_image_data = theaterorg_template_image_file.read()
            ticket_template_image = admin.save_ticket_template_image(ticket_template_image_data, theaterorg.key, template_type='theaterorg')

    return redirect(url_for('.theaterorg_show', org_id=org_id))

@web.route('/theaters/')
def theater_index():
    # TODO: Hardcoded. Make this dynamic
    integrated_orgs = [str(orgs.AYALA_MALLS),] # for organizations with single integration point per theater
    integrated_theaters = ['LCT', 'RWPP'] # for theaters with different integration points

    theater_groups = []
    theater_orgs = TheaterOrganization.query().order(TheaterOrganization.name).fetch()
    switch_status = []
    for theater_org in theater_orgs:
        theaters = Theater.query(ancestor=theater_org.key).fetch()
        new_theater_key = memcache.get('theater::new::key')
        
        if new_theater_key is not None:
            new_theater = admin.get_theater(new_theater_key)

            if new_theater not in theaters and new_theater.key.parent == theater_org.key:
                theaters.append(new_theater)
        try:
            if theater_org.allow_reservation_master_switch is None:
                switch_status.append(0)
            else:
                switch_status.append(theater_org.allow_reservation_master_switch)
        except:
            switch_status.append(0)

        theater_groups.append(theaters)

    return render_template('theater_index.html', theater_groups=theater_groups,
            theater_orgs=theater_orgs, is_payment_enabled=admin.is_payment_enabled,
            integrated_orgs=integrated_orgs, integrated_theaters=integrated_theaters, 
            switch_status=switch_status)

@web.route('/theaters/new/')
def theater_new():
    theaterorgs = TheaterOrganization.query().order(TheaterOrganization.name)
    form = TheaterForm()

    return render_template('theater_new.html', form=form, theaterorgs=theaterorgs)

@web.route('/theaters/', methods=['POST'])
def theater_create():
    theaterorg = request.values['theater_organization']
    theaterorgs = TheaterOrganization.query().order(TheaterOrganization.name)
    form = TheaterForm()

    if form.validate_on_submit() and admin.check_theater(form.name.data, theaterorg):
        theater = admin.create_theater_params(form, theaterorg)
        memcache.set('theater::new::key', id_encoder.encoded_theater_id(theater.key))

        flash("Success! Created New Theater!")

        return redirect(url_for('.theater_index'))
    elif not admin.check_theater(form.name.data, theaterorg):
        form.errors['generic'] = 'Error! Theater Already Exists!'

    return render_template('theater_new.html', form=form, theaterorgs=theaterorgs)

@web.route('/theaters/<theater_id>/', methods=['GET', 'POST'])
def theater_show(theater_id):
    theater = admin.get_theater(theater_id)
    assets_entity = AssetsImageBlob.query(AssetsImageBlob.assets_group=='theaters-image', ancestor=theater.key).get()
    form = TheaterForm(obj=theater)

    if request.method == 'POST' and form.validate_on_submit():
        form.populate_obj(theater)
        theater_image_file = request.files[form.theater_image_file.name]

        if form.latitude.data and form.longitude.data:
            theater.location = ndb.GeoPt(lat=form.latitude.data, lon=form.longitude.data)
        else:
            theater.location = None

        if theater_image_file:
            if not assets_entity:
                assets_entity = AssetsImageBlob()
                assets_entity.key = ndb.Key(AssetsImageBlob, str(uuid_gen()), parent=theater.key)
                assets_entity.name = theater.name
                assets_entity.assets_group = 'theaters-image'
                assets_entity.is_active = True

            assets_entity.image = get_image_blob_key(assets_entity, theater_image_file, folder='assets/theaters-image/')
            assets_entity.put()

            cache_key = 'theater::image::key::%s' % id_encoder.encoded_theater_id(theater.key)
            image_url = get_serving_url(assets_entity.image)
            theater.image_url = image_url

            memcache.set(cache_key, image_url)

        theater.put()

        if admin.is_payment_enabled(theater.key.parent().id()):
            admin.update_org_client_control(theater.key.parent().id(), theater_id, form.allow_reservation.data)

    if theater.location:
        form.longitude.data = theater.location.lon
        form.latitude.data = theater.location.lat

    today = datetime.date.today()
    schedules = Schedule.query(Schedule.show_date>=today, ancestor=theater.key).order(-Schedule.show_date).fetch(projection=['cinema', 'show_date'])

    return render_template('theaters_show.html', form=form, theater=theater, schedules=schedules, theater_id=theater_id,
            assets_entity=assets_entity, has_slots_conflict=admin.has_slots_conflict, is_payment_enabled=admin.is_payment_enabled,
            get_serving_url=get_serving_url)

@web.route('/theaters/<theater_id>/cinemas/')
def cinema_new(theater_id):
    form = CinemaForm()

    return render_template('cinema_new.html', form=form, theater_id=theater_id)

@web.route('/theaters/<theater_id>/cinemas/', methods=['POST'])
def cinema_create(theater_id):
    form = CinemaForm()

    if form.validate_on_submit() and admin.check_cinema(form.name.data, theater_id):
        admin.create_cinema_params(form, theater_id)

        return redirect(url_for('.theater_show', theater_id=theater_id))
    elif not admin.check_cinema(form.name.data, theater_id):
        form.errors['generic'] = 'Error! Cinema Already Exists!'

    return render_template('cinema_new.html', form=form, theater_id=theater_id)

@web.route('/theaters/<theater_id>/cinemas/<cinema_name>/')
def cinema_show(theater_id, cinema_name):
    today = datetime.date.today()
    theater = admin.get_theater(theater_id)
    cinema = admin.get_cinema_entity(theater, cinema_name)
    cinema_seatmap = cinema.seat_map
    seatmap = ','.join(str(x) for x in cinema_seatmap)
    seat_rows = [','.join(row) for row in cinema_seatmap]
    schedules = Schedule.query(Schedule.show_date>=today, Schedule.cinema==cinema_name,
            ancestor=theater.key).order(-Schedule.show_date).fetch(projection=['show_date'])

    form = CinemaForm(obj=cinema)
    form.longitude.data = cinema.location.lon if cinema.location else None
    form.latitude.data = cinema.location.lat if cinema.location else None
    form.seat_map.choices = zip(seat_rows, seat_rows)
    form.seat_map.data = seat_rows

    return render_template('cinema_show.html', form=form, theater_id=theater_id, theater_name=theater.name,
            schedules=schedules, seatmap=seatmap, cinema=cinema, has_slots_conflict=admin.has_slots_conflict)

@web.route('/theaters/<theater_id>/cinemas/<cinema_name>/', methods=['POST'])
def cinema_update(theater_id, cinema_name):
    theater = admin.get_theater(theater_id)
    cinema = admin.get_cinema_entity(theater, cinema_name)
    form = CinemaForm()

    seatmap_orig_data = cinema.seat_map # original seatmap data before parse and update
    seat_rows = [','.join(row) for row in cinema.seat_map]
    form.seat_map.choices = zip(seat_rows, seat_rows)
    seatmap = ','.join(str(x) for x in form.seat_map.data)

    if form.validate_on_submit():
        form.populate_obj(cinema)

        # Seatmaps manual update is not allowed for Rockwell
        if str(theater.key.parent().id()) != str(orgs.ROCKWELL_MALLS):
            log.info("UPDATE!, Seatmaps for %s - Cinema %s..." % (theater.name, cinema.name))
            seat_map = admin.add_seats(form.addseats.data, form.seat_map.data)
            cinema.seat_map = seat_map
            log.info('{0} Cinema {1} Seatmap Details: {2}'.format(theater.name, cinema.name, seat_map))

            # FIXME: Unify with gmovies.feeds.theaters.get_seat_count
            cinema.seat_count = str(len(filter(lambda seat: seat != '', sum(seat_map, []))))
            seatmap = ','.join(str(x) for x in seat_map)
        else:
            log.info('Seatmap manual update is not allowed for Rockwell Cinema...')
            log.info('{0} Cinema {1} Seatmap Details: {2}'.format(theater.name, cinema.name, seatmap_orig_data))
            cinema.seat_map = seatmap_orig_data

        if form.latitude.data and form.longitude.data:
            geo_pt = ndb.GeoPt(form.latitude.data, form.longitude.data)
        else:
            geo_pt = None

        cinema.location = geo_pt
        theater.put()

        return redirect(url_for('.cinema_show', theater_id=theater_id, cinema_name=cinema.name))

    return render_template('cinema_show.html', form=form, theater_id=theater_id, cinema=cinema, seatmap=seatmap)


###############
#
# Movies, Feed, and FeedCorrelation
#
###############

@web.route('/movies/')
def movie_index():
    canonical_title = ''
    sort = '-release_date'
    inactive = False
    sortable_columns = ['canonical_title', 'release_date']
    cursor = ndb.Cursor(urlsafe=request.args.get('cursor'))

    if 'canonical_title' in request.args:
        canonical_title = request.args.get('canonical_title')

    if 'inactive' in request.args:
        if request.args.get('inactive').lower() == 'true':
            inactive = True

    if 'sort' in request.args:
        sort = request.args.get('sort')

    searchable_words = movies.get_searchable_words(canonical_title)

    if canonical_title:
        queryset = Movie.query(Movie.searchable_words.IN(searchable_words), Movie.is_inactive==inactive).order(Movie.key)
    else:
        queryset = Movie.query(Movie.is_inactive==inactive)

    if sort:
        queryset = admin.apply_sort(queryset, Movie, sort, sortable_columns)

    movies_page, next_cursor, more = queryset.fetch_page(100, start_cursor=cursor)

    return render_template('movie_index.html', movies_page=movies_page, next_cursor=next_cursor,
            more=more, canonical_title=canonical_title, sort=sort, inactive=inactive)

@web.route('/movies/new/')
def movie_new():
    form = MovieForm()

    return render_template('movie_new.html', form=form)

@web.route('/movies/', methods=['POST'])
def movie_create():
    form = MovieForm()

    if form.validate_on_submit():
        admin.create_movie(form)

        flash("Success! Created New Movie!")

        return redirect(url_for('.movie_index'))

    return render_template('movie_new.html', form=form)

@web.route('/movies/<movie_id>/delete', methods=['POST'])
def movie_delete(movie_id):
    new_movie_key = memcache.get('movie::new::key')
    movie_key = ndb.Key(Movie, movie_id)
    movie = movie_key.get()
    canonical_title = movie.canonical_title
    titles = movie.correlation_title
    movie_key.delete()

    if new_movie_key is not None:
        if movie.key == new_movie_key.key:
            memcache.delete('movie::new::key')

    for title in titles:
        memcache.delete("movie::correlation_title::%s" % title)

    flash("Deleted {}".format(canonical_title))

    return redirect(url_for('.movie_index'))

@web.route('/movies/<movie_id>/get_rottentomatoes_details', methods=['POST'])
def movie_get_rottentomatoes_details(movie_id):
    log.info("Fetching Rotten Tomatoes details: %s..." % request.values)

    movie_key = ndb.Key(Movie, movie_id)

    if admin.get_rottentomatoes_details(movie_key) is None:
        flash("Couldn't find Rotten Tomatoes ID in Movie Title Bank. Please fix the Movie Title Bank and try again.")
    else:
        flash("Saved Rotten Tomatoes details.")

    return redirect(url_for('.movie_update_and_show', movie_id=movie_id))

@web.route('/movies/<movie_id>/', methods=['GET', 'POST'])
def movie_update_and_show(movie_id):
    movie_key = ndb.Key(Movie, movie_id)
    movie = movie_key.get()
    cast = unicode(','.join(x for x in movie.cast))
    ratings = movie.ratings
    log.debug("ratings")
    log.debug(ratings)
    if ratings and str(orgs.ROTTEN_TOMATOES) in ratings:
        ratings = ratings[str(orgs.ROTTEN_TOMATOES)]
    critics_score   = ''
    audience_score  = ''
    critics_rating  = ''
    audience_rating = ''
    if ratings:
        critics_score   = ratings['critics'] if 'critics' in ratings else ''
        audience_score  = ratings['audience'] if 'audience' in ratings else ''
        critics_rating  = ratings['critics_comment'] if 'critics_comment' in ratings else ''
        audience_rating = ratings['audience_comment'] if 'audience_comment' in ratings else ''

    rt_mapping = None
    rt_key = ndb.Key(Feed, str(orgs.ROTTEN_TOMATOES))
    rt_corr = filter(lambda corr: corr.org_key == rt_key, movie.movie_correlation)

    if rt_corr:
        rt_mapping = rt_corr[0].movie_id

    sureseats_key = ndb.Key(Feed, str(orgs.AYALA_MALLS))
    sureseat_mappings = filter(lambda corr: corr.org_key == sureseats_key, movie.movie_correlation)

    rockwell_key = ndb.Key(Feed, str(orgs.ROCKWELL_MALLS))
    rockwell_mappings = filter(lambda corr: corr.org_key == rockwell_key, movie.movie_correlation)

    greenhills_key = ndb.Key(Feed, str(orgs.GREENHILLS_MALLS))
    greenhills_mappings = filter(lambda corr: corr.org_key == greenhills_key, movie.movie_correlation)

    sm_malls_key = ndb.Key(Feed, str(orgs.SM_MALLS))
    sm_malls_mappings = filter(lambda corr: corr.org_key == sm_malls_key, movie.movie_correlation)

    megaworld_key = ndb.Key(Feed, str(orgs.MEGAWORLD_MALLS))
    megaworld_mappings = filter(lambda corr: corr.org_key == megaworld_key, movie.movie_correlation)

    robinsons_key = ndb.Key(Feed, str(orgs.ROBINSONS_MALLS))
    robinsons_mappings = filter(lambda corr: corr.org_key == robinsons_key, movie.movie_correlation)

    cinema76_key = ndb.Key(Feed, str(orgs.CINEMA76_MALLS))
    cinema76_mappings = filter(lambda corr: corr.org_key == cinema76_key, movie.movie_correlation)

    globeevents_key = ndb.Key(Feed, str(orgs.GLOBE_EVENTS))
    globeevents_mappings = filter(lambda corr: corr.org_key == globeevents_key, movie.movie_correlation)

    citymall_key = ndb.Key(Feed, str(orgs.CITY_MALLS))
    citymall_mappings = filter(lambda corr: corr.org_key == citymall_key, movie.movie_correlation)

    form = MovieForm(obj=movie)

    if admin.is_old_variant_strip(form.canonical_title.data):
        correlation_titles = movies_old.get_all_correlation_titles(form.canonical_title.data)
    else:
        correlation_titles = movies.get_all_correlation_titles(form.canonical_title.data)

    if request.method == 'POST' and form.validate_on_submit() and admin.check_movie(movie.correlation_title, correlation_titles, movie_key):
        form.populate_obj(movie)

        if admin.is_old_variant_strip(form.canonical_title.data):
            movie.canonical_title = movies_old.get_canonical_title(form.canonical_title.data)
        else:
            movie.canonical_title = movies.get_canonical_title(form.canonical_title.data)

        if correlation_titles != movie.correlation_title:
            movie.correlation_title = correlation_titles

        movie.searchable_words = movies.get_searchable_words(form.canonical_title.data)
        movie.cast = parse_cast(form.cast_list.data)

        if rt_corr and form.rt_mapping.data:
            rt_corr[0].movie_id = form.rt_mapping.data
        elif not rt_corr and form.rt_mapping.data:
            movie.movie_correlation.append(FeedCorrelation(org_key=rt_key, movie_id=form.rt_mapping.data))

        if form.is_inactive.data:
            movie.is_expired = True

        movie.ratings = {}
        movie.ratings[str(orgs.ROTTEN_TOMATOES)] = {}
        movie.ratings[str(orgs.ROTTEN_TOMATOES)]['critics']   = form.critics_score.data
        movie.ratings[str(orgs.ROTTEN_TOMATOES)]['audience']  = form.audience_score.data
        movie.ratings[str(orgs.ROTTEN_TOMATOES)]['critics_comment']  = form.critics_rating.data
        movie.ratings[str(orgs.ROTTEN_TOMATOES)]['audience_comment'] = form.audience_rating.data
        movie.put()

        ticket_template_image_file = request.files[form.ticket_template_image_file.name]

        if ticket_template_image_file:
            ticket_template_image_data = ticket_template_image_file.read()
            ticket_template_image = admin.save_ticket_template_image(ticket_template_image_data, movie.key, template_type='movie')

        cache_movie_runtime(movie.key.id(), movie.runtime_mins)
        admin.refresh_movie_gmoviesdigitalventures(movie.key.id())

        flash("Saved")

        return redirect(url_for('.movie_update_and_show', movie_id=movie.key.id()))
    else:
        form.cast_list.data   = cast
        form.rt_mapping.data  = rt_mapping
        ticket_template_image = TicketTemplateImageBlob.query(TicketTemplateImageBlob.template_type=='movie', ancestor=movie.key).get()
        form.critics_score.data    = critics_score
        form.audience_score.data   = audience_score
        form.critics_rating.data   = critics_rating
        form.audience_rating.data  = audience_rating

        if not admin.check_movie(movie.correlation_title, correlation_titles, movie_key):
            form.errors['generic'] = 'The movie already exists.'

    today = datetime.date.today()
    schedules = Schedule.query(Schedule.slots.movie==movie_key, Schedule.show_date>=today).order(-Schedule.show_date).fetch(projection=['show_date', 'cinema'])
    schedule_parents = set([s.key.parent() for s in schedules])
    theater_names = {}
    theaters = ndb.get_multi(schedule_parents)

    for t_k, t in zip(schedule_parents, theaters):
        theater_names[t_k] = t.org_theater_code

    schedule_theaters = [theater_names[s.key.parent()] for s in schedules]
    schedule_pairs = zip(schedules, schedule_theaters)

    return render_template('movie_show.html', form=form, movie=movie, movie_id=movie_id, cast=cast, schedules=schedule_pairs,
            sureseat_mappings=sureseat_mappings, rockwell_mappings=rockwell_mappings, greenhills_mappings=greenhills_mappings,
            sm_malls_mappings=sm_malls_mappings, megaworld_mappings=megaworld_mappings, robinsons_mappings=robinsons_mappings,
            cinema76_mappings=cinema76_mappings, globeevents_mappings=globeevents_mappings, citymall_mappings=citymall_mappings,
            rt_corr=rt_corr, ticket_template_image=ticket_template_image)

@web.route('/movies/<movie_id>/alternate_posters/new/', methods=['GET', 'POST'])
def alternate_posters_new(movie_id):
    key_name = None
    movie_key = ndb.Key(Movie, movie_id)
    movie = movie_key.get()
    form = AlternatePostersForm()

    if request.method == 'POST' and form.validate_on_submit():
        if not movie.alternate_posters:
            movie.alternate_posters = {}

        key_name = '%s~%s~%s' % (form.resolution.data, form.orientation.data, form.application_type.data)
        movie.alternate_posters[key_name.lower()] = form.source_url.data
        movie.put()

        flash('Saved Alternate Poster')

        return redirect(url_for('.movie_update_and_show', movie_id=movie.key.id()))

    return render_template('movie_alternate_poster_form.html', form=form, movie=movie, key_name=key_name)

@web.route('/movies/<movie_id>/alternate_posters/<key_name>/', methods=['GET', 'POST'])
def alternate_posters_edit(movie_id, key_name):
    movie_key = ndb.Key(Movie, movie_id)
    movie = movie_key.get()
    form = AlternatePostersForm()

    if request.method == 'POST' and form.validate_on_submit():
        if not movie.alternate_posters:
            movie.alternate_posters = {}

        new_key_name = '%s~%s~%s' % (form.resolution.data, form.orientation.data, form.application_type.data)
        movie.alternate_posters[new_key_name.lower()] = form.source_url.data

        if key_name != new_key_name.lower():
            movie.alternate_posters.pop(key_name, None)

        movie.put()

        flash('Saved Alternate Poster')

        return redirect(url_for('.movie_alternate_posters', movie_id=movie.key.id()))
    else:
        resolution = key_name.split('~')[0]
        form.resolution.data = resolution
        form.source_url.data = movie.alternate_posters[key_name]

        try:
            orientation = key_name.split('~')[1]
            form.orientation.data = orientation
        except:
            pass

        try:
            application_type = key_name.split('~')[2]
            form.application_type.data = application_type
        except:
            pass

    return render_template('movie_alternate_poster_form.html', form=form, movie=movie, key_name=key_name)

@web.route('/movies/<movie_id>/alternate_posters/<key_name>/delete', methods=['POST'])
def alternate_posters_delete(movie_id, key_name):
    movie_key = ndb.Key(Movie, movie_id)
    movie = movie_key.get()

    if request.method == 'POST':
        movie.alternate_posters.pop(key_name, None)
        movie.put()

        flash('Deleted alternate poster %s' % key_name)

    return redirect(url_for('.movie_alternate_posters', movie_id=movie_id))

@web.route('/movies/<movie_id>/sample_ticket_image', methods=['GET'])
def movie_sample_ticket_image(movie_id):
    workspace = {'theaters.org_id': [str(orgs.AYALA_MALLS)]}

    if 'is_theaterorg_ticket_template' in request.args:
        is_theaterorg_ticket_template = False

        if request.args.get('is_theaterorg_ticket_template').lower() == 'true':
            is_theaterorg_ticket_template = True

        workspace['is_theaterorg_ticket_template'] = is_theaterorg_ticket_template

    movie_key = ndb.Key(models.Movie, str(movie_id))
    movie = movie_key.get()
    sample_ticket_image = generate_sample_ticket_image(movie, workspace=workspace)

    return render_template('movie_sample_ticket_image.html', movie=movie, sample_ticket_image=sample_ticket_image)

def find_canonical_title(movie_key):
    cache_key = "movie::canonical_title::%s" % movie_key.id()

    if not memcache.get(cache_key):
        memcache.set(cache_key, movie_key.get().canonical_title)

    return memcache.get(cache_key)

@web.route('/theaters/<theater_id>/schedules/<schedule_id>/')
def schedule_show(theater_id, schedule_id):
    theater = admin.get_theater(theater_id)
    schedule_key = ndb.Key(Schedule, schedule_id, parent=theater.key)
    schedule = schedule_key.get()

    return render_template('schedule_show.html', schedule=schedule, theater_id=theater_id, theater_name=theater.name,
            schedule_id=schedule_id, canonical_title=find_canonical_title, is_conflict=admin.is_conflict)

@web.route('/theaters/<theater_id>/schedules/new/')
def schedule_new(theater_id):
    org_id, _theater_id = id_encoder.decoded_theater_id(theater_id)
    theater_key = ndb.Key(TheaterOrganization, str(org_id), Theater, _theater_id)
    theater = theater_key.get()
    theater_name = theater.name
    cinema_choices = [(c.name, c.name) for c in theater.cinemas]
    form = ScheduleForm()

    if 'cinema' in request.args:
        form.cinema.data = request.args['cinema']

    form.cinema.choices = cinema_choices

    return render_template('schedule_new.html', form=form, theater_id=theater_id, theater_name=theater_name)

@web.route('/theaters/<theater_id>/schedules/', methods=['POST'])
def schedule_create(theater_id):
    theater = admin.get_theater(theater_id)
    cinema_choices = [(c.name, c.name) for c in theater.cinemas]
    form = ScheduleForm()
    form.cinema.choices = cinema_choices

    if form.validate_on_submit() and not admin.check_cinema(form.cinema.data, theater_id):
        schedule = Schedule.create_schedule(show_date=form.show_date.data, cinema=form.cinema.data, parent=theater.key)
        schedule.put()

        flash('Success')

        return redirect(url_for('.schedule_show', theater_id=theater_id, schedule_id=schedule.key.id()))
    else:
        form.errors['generic'] = 'Please verify the cinema / schedule information'

    return render_template('schedule_new.html', form=form, theater_id=theater_id)

@web.route('/theaters/<theater_id>/schedules/<schedule_id>/slots/new', methods=['GET', 'POST'])
def slot_new(theater_id, schedule_id):
    org_id, _theater_id = id_encoder.decoded_theater_id(theater_id)
    theater_key = ndb.Key(TheaterOrganization, str(org_id), Theater, _theater_id)
    theater_name = theater_key.get().name
    schedule_key = ndb.Key(Schedule, schedule_id, parent=theater_key)
    schedule = schedule_key.get()
    slot = ScheduleTimeSlot()
    movies = Movie.query(Movie.is_inactive == False).order(-Movie.release_date).fetch()
    movie_choices = [(m.key.id(), m.canonical_title) for m in movies]
    form = ScheduleSlotForm(obj=slot)
    form.movie_title.choices = movie_choices

    if request.method == 'POST' and form.validate_on_submit():
        form.populate_obj(slot)
        slot.movie = ndb.Key(Movie, form.movie_title.data)
        schedule.slots.append(slot)
        schedule.put()

        flash('Success')

        return redirect(url_for('.schedule_show', theater_id=theater_id, schedule_id=schedule_id))

    return render_template('schedule_slot_edit.html', form=form, schedule=schedule, slot=slot,
            theater_name=theater_name, theater_id=theater_id, schedule_id=schedule_id)

@web.route('/theaters/<theater_id>/schedules/<schedule_id>/slots/<int:slot_idx>', methods=['GET', 'POST'])
def slot_edit(theater_id, schedule_id, slot_idx):
    org_id, _theater_id = id_encoder.decoded_theater_id(theater_id)
    theater_key = ndb.Key(TheaterOrganization, str(org_id), Theater, _theater_id)
    theater_name = theater_key.get().name
    schedule_key = ndb.Key(Schedule, schedule_id, parent=theater_key)
    schedule = schedule_key.get()
    slot = schedule.sorted_slots()[slot_idx]
    movies = Movie.query(Movie.is_inactive == False).order(-Movie.release_date).fetch()
    movie_choices = [(m.key.id(), m.canonical_title) for m in movies]
    form = ScheduleSlotForm(obj=slot)
    form.movie_title.choices = movie_choices

    if request.method == 'POST' and form.validate_on_submit():
        form.populate_obj(slot)
        slot.movie = ndb.Key(Movie, form.movie_title.data)
        schedule.put()

        flash('Success')

        return redirect(url_for('.schedule_show', theater_id=theater_id, schedule_id=schedule_id))
    else:
        form.movie_title.data = slot.movie.id()

    return render_template('schedule_slot_edit.html', form=form, schedule=schedule, slot=slot,
            theater_name=theater_name, theater_id=theater_id, schedule_id=schedule_id, slot_idx=slot_idx)

@web.route('/theaters/<theater_id>/schedules/<schedule_id>/slots/<int:slot_idx>/delete', methods=['POST'])
def slot_delete(theater_id, schedule_id, slot_idx):
    org_id, _theater_id = id_encoder.decoded_theater_id(theater_id)
    theater_key = ndb.Key(TheaterOrganization, str(org_id), Theater, _theater_id)
    theater_name = theater_key.get().name
    schedule_key = ndb.Key(Schedule, schedule_id, parent=theater_key)
    schedule = schedule_key.get()
    slot = schedule.sorted_slots()[slot_idx]
    ndb_slot_idx = schedule.slots.index(slot)

    if request.method == 'POST':
        schedule.slots.pop(ndb_slot_idx)
        schedule.put()

        flash('Deleted {} Timeslot'.format(slot.start_time))

    return redirect(url_for('.schedule_show', theater_id=theater_id, schedule_id=schedule_id))

@web.route('/theaters/<theater_id>/schedules/<schedule_id>/slots/purge', methods=['POST'])
def slot_purge(theater_id, schedule_id):
    org_id, _theater_id = id_encoder.decoded_theater_id(theater_id)
    theater_key = ndb.Key(TheaterOrganization, str(org_id), Theater, _theater_id)
    theater_name = theater_key.get().name
    schedule_key = ndb.Key(Schedule, schedule_id, parent=theater_key)
    schedule = schedule_key.get()
    slots = schedule.sorted_slots()

    if request.method == 'POST':
        for idx, _ in enumerate(slots):
            schedule.slots.pop()

        schedule.put()

        flash('Timeslots Purged')

        if admin.is_ayala(theater_key.parent().get().name):
            now = datetime.datetime.now()
            admin.fetch_schedules_on_demand()

            flash("On demand schedules fetching for Ayala Malls initiated @ {}. This may take a minute or two. Refresh the page until new schedules appears.".format(now))

    return redirect(url_for('.schedule_show', theater_id=theater_id, schedule_id=schedule_id))

@web.route('/theaters/upload/')
def theater_upload():
    theaterorgs = TheaterOrganization.query().order(TheaterOrganization.name)
    form = FileUploadForm()

    return render_template('theaters_upload.html', form=form, theaterorgs=theaterorgs)

@web.route('/movies/upload/')
def movie_upload():
    form = FileUploadForm()

    return render_template('movies_upload.html', form=form)

@web.route('/theaters/<theater_id>/schedules/upload/')
def schedule_upload(theater_id):
    form = FileUploadForm()

    return render_template('schedules_upload.html', form=form, theater_id=theater_id)

@web.route('/theaters/upload/', methods=['POST'])
def upload_theaters_file():
    error_list = []
    form = FileUploadForm()
    theater_file = request.files[form.file_uploaded.name]
    theaterorg = request.values['theater_organization']
    theaterorgs = TheaterOrganization.query().order(TheaterOrganization.name)
    admin.check_upload_file(theater_file, error_list, 'theater', theaterorg)

    if len(error_list) > 0:
        return render_template('theaters_upload.html', form=form, error_list=error_list, theaterorgs=theaterorgs)
    else:
        flash('Sucessfully uploaded')

        return redirect(url_for('.theater_index'))

@web.route('/movies/upload/', methods=['POST'])
def upload_movies_file():
    error_list = []
    form = FileUploadForm()
    movie_file = request.files[form.file_uploaded.name]

    admin.check_upload_file(movie_file, error_list, 'movies')

    if len(error_list) > 0:
        return render_template('movies_upload.html', form=form, error_list=error_list)
    else:
        flash('Successfully uploaded')

        return redirect(url_for('.movie_index'))

@web.route('/theaters/<theater_id>/schedules/upload/', methods=['POST'])
def upload_schedules_file(theater_id):
    error_list = []
    form = FileUploadForm()
    schedule_file = request.files[form.file_uploaded.name]

    admin.check_upload_file(schedule_file, error_list, 'schedules')

    if len(error_list) > 0:
        return render_template('schedules_upload.html', form=form,theater_id=theater_id,error_list=error_list)
    else:
        flash('Successfully uploaded')

        return redirect(url_for('.theater_show', theater_id=theater_id))

@web.route('/feeds/')
def feed_index():
    feeds = dict([(f.key.id(), f.name) for f in Feed.query().fetch(projection=['name'])])

    for f in FEEDS:
        if f not in feeds:
            feeds[f] = FEEDS[f]

    return render_template('feed_index.html', feeds=feeds)

@web.route('/feeds/<feed_id>/')
def feed_show(feed_id):
    feed_key = ndb.Key(Feed, feed_id)
    feed = feed_key.get()

    if feed is None:
        feed = Feed(name=FEEDS[feed_id])
        feed.key = ndb.Key(Feed, feed_id)
        feed.image_density = 'mdpi'
        feed.put()

    form = FeedForm(obj=feed)

    return render_template('feed_show.html', feed=feed, feed_id=feed_id, form=form)

@web.route('/feeds/<feed_id>/', methods=['POST'])
def feed_update(feed_id):
    feed_key = ndb.Key(Feed, feed_id)
    feed = feed_key.get()
    form = FeedForm()

    if form.validate_on_submit():
        form.populate_obj(feed)
        feed.put()

        return redirect(url_for('.feed_show', feed_id=feed_id))

    return render_template('feed_show.html', feed=feed, feed_id=feed_id, form=form)

@web.route('/csrc_transactions/')
def csrc_transactions_index():
    t_cur = ndb.Cursor(urlsafe=request.args.get('t_c'))
    r_cur = ndb.Cursor(urlsafe=request.args.get('r_c'))
    d_cur = ndb.Cursor(urlsafe=request.args.get('d_c'))
    transactions = CSRCPayment.query(CSRCPayment.state.IN([CSRCPayment.proc_state.TX_STARTED,
            CSRCPayment.proc_state.TX_PAYMENT_HOLD])).order(-CSRCPayment.created, CSRCPayment.key)
    reaped_transactions = CSRCPayment.query(CSRCPayment.state==CSRCPayment.proc_state.TX_PAYMENT_ERROR).order(-CSRCPayment.created)
    done_transactions = CSRCPayment.query(CSRCPayment.state==CSRCPayment.proc_state.TX_DONE).order(-CSRCPayment.created)

    transactions_page, next_t_cur, t_more = transactions.fetch_page(25, start_cursor=t_cur)
    reaped_transactions_page, next_r_cur, r_more = reaped_transactions.fetch_page(25, start_cursor=r_cur)
    done_transactions_page, next_d_cur, d_more = done_transactions.fetch_page(25, start_cursor=d_cur)

    return render_template('csrc_transactions_index.html', transactions=transactions_page,
            t_c=t_cur, next_t_c=next_t_cur, t_more=t_more, reaped=reaped_transactions_page,
            r_c=r_cur, next_r_c=next_r_cur, r_more=r_more, done=done_transactions_page,
            d_c=d_cur, next_d_c=next_d_cur, d_more=d_more, camelcase=admin.camelcase,
            stringify_state=CSRCPayment.stringify_state)

@web.route('/csrc_transactions/<transaction_id>/')
def csrc_transaction_show(transaction_id):
    transaction = admin.get_csrc_transaction(transaction_id)

    return render_template('csrc_transactions_show.html', transaction=transaction, stringify_state=CSRCPayment.stringify_state)

@web.route('/transactions/')
def transactions_index():
    t_cur = ndb.Cursor(urlsafe=request.args.get('t_c'))
    r_cur = ndb.Cursor(urlsafe=request.args.get('r_c'))
    d_cur = ndb.Cursor(urlsafe=request.args.get('d_c'))
    transactions = ReservationTransaction.query(ReservationTransaction.state.IN([state.TX_START,
            state.TX_PREPARE, state.TX_STARTED, state.TX_RESERVATION_HOLD, state.TX_RESERVATION_OK,
            state.TX_PAYMENT_HOLD, state.TX_GENERATE_TICKET, state.TX_CLIENT_PAYMENT_HOLD, state.TX_EXTERNAL_PAYMENT_HOLD,
            state.TX_FINALIZE, state.TX_FINALIZE_HOLD, state.TX_PREPARE_ERROR, state.TX_RESERVATION_ERROR,
            state.TX_PAYMENT_ERROR])).order(-ReservationTransaction.date_created, ReservationTransaction.key)
    reaped_transactions = ReservationTransaction.query(ReservationTransaction.state==state.TX_CANCELLED).order(-ReservationTransaction.date_created)
    done_transactions = ReservationTransaction.query(ReservationTransaction.state==state.TX_DONE).order(-ReservationTransaction.date_created)

    transactions_page, next_t_cur, t_more = transactions.fetch_page(25, start_cursor=t_cur)
    reaped_transactions_page, next_r_cur, r_more = reaped_transactions.fetch_page(25, start_cursor=r_cur)
    done_transactions_page, next_d_cur, d_more = done_transactions.fetch_page(25, start_cursor=d_cur)

    return render_template('transactions_index.html',
            transactions=transactions_page, t_c=t_cur, next_t_c=next_t_cur, t_more=t_more,
            reaped=reaped_transactions_page, r_c=r_cur, next_r_c=next_r_cur, r_more=r_more,
            done=done_transactions_page, d_c=d_cur, next_d_c=next_d_cur, d_more=d_more)

@web.route('/transactions/filters/')
def transactions_filters():
    results = {}
    filter_by = request.args.get('filter_by')

    try:
        if filter_by == 'theater':
            theaterorg_id = request.args.get('theaterorg_id')
            theaterorg_key = ndb.Key(TheaterOrganization, theaterorg_id)
            theaterorg = theaterorg_key.get()
            theaters = Theater.query(ancestor=theaterorg_key).fetch()

            for theater in theaters:
                results[theater.name] = theater.key.id()
        elif filter_by == 'theater-v2':
            org_uuids = request.args.get('org_uuids', None)

            if org_uuids:
                theaters = []
                org_uuids = org_uuids.split(',')

                for org_uuid in org_uuids:
                    theaterorg_key = ndb.Key(TheaterOrganization, str(org_uuid))
                    theaterorg = theaterorg_key.get()

                    if theaterorg is not None:
                        theater_infos = Theater.query(ancestor=theaterorg_key).fetch()
                        theaters.extend(theater_infos)

                for theater in theaters:
                    results[theater.name] = theater.key.id()
        elif filter_by == 'promo_name':
            promo_name = request.args.get('promo_name')
            promos = Promo.query(Promo.name == promo_name.strip()).fetch()

            for promo in promos:
                if promo.theater not in results:
                    results[promo.theater] = promo.theater
    except Exception, e:
        log.warn('ERROR!, something went wrong...')
        log.error(e)

    log.info("TRANSACTIONS FILTER: {} {}".format(filter_by.strip(), results))

    return jsonify(results)


@web.route('/transactions/done/')
def transactions_done():
    date_today = datetime.datetime.today().date()
    cursor = ndb.Cursor(urlsafe=request.args.get('cursor'))
    transactions_from = request.args.get('transactions_from', None)
    transactions_to = request.args.get('transactions_to', None)
    theater_id = request.args.get('theater_id', None)
    platform = request.args.get('platform', None)
    payment_type = request.args.getlist('payment_type')
    org_uuids = request.args.getlist('org_uuids')
    payment_reference = request.args.get('payment_reference', None)
    reservation_reference = request.args.get('reservation_reference', None)

    if not transactions_from or transactions_from is None:
        transactions_from = str(date_today)

    if not transactions_to or transactions_to is None:
        transactions_to = str(date_today)

    theaters = []
    tx_from = admin.parse_string_to_date(transactions_from + ' 00:00:00', fdate='%Y-%m-%d %H:%M:%S')
    tx_to = admin.parse_string_to_date(transactions_to + ' 23:59:59', fdate='%Y-%m-%d %H:%M:%S')
    transactions = admin.get_date_range_string(transactions_from, transactions_to, dates={})
    queryset = admin.get_filtered_transactions(state.TX_DONE, tx_from=tx_from,
            tx_to=tx_to, payment_type=payment_type, org_uuids=org_uuids,
            theater_id=theater_id, platform=platform, payment_reference=payment_reference,
            reservation_reference=reservation_reference)

    for org_uuid in org_uuids:
        theaterorg_key = ndb.Key(TheaterOrganization, str(org_uuid))
        theaterorg = theaterorg_key.get()

        if theaterorg is not None:
            theater_infos = Theater.query(ancestor=theaterorg_key).fetch()
            theaters.extend(theater_infos)

    transactions_page, next_cursor, more = queryset.fetch_page(100, start_cursor=cursor)

    return render_template('transactions_done.html', transactions=transactions_page,
            next_cursor=next_cursor, more=more, transactions_from=transactions_from,
            transactions_to=transactions_to, platform=platform, theater_id=theater_id,
            org_uuids=org_uuids, payment_type=payment_type, theaters=theaters,
            parse_float_to_string=admin.parse_float_to_string, orgs=orgs,
            payment_reference=payment_reference, reservation_reference=reservation_reference)


@web.route('/transactions/cancelled/',  methods=['GET', 'POST'])
def transactions_cancelled():
    date_today = datetime.datetime.today().date()
    cursor = ndb.Cursor(urlsafe=request.args.get('cursor'))

    if request.method == 'POST':
        transactions_from = request.form['transactions_from']
        transactions_to = request.form['transactions_to']
        theater_id = request.form['theater_id']
        platform = request.form['platform']
        payment_type = request.form.getlist('payment_type')
        org_uuids = request.form.getlist('org_uuids')
        payment_reference = request.form['payment_reference']
        reservation_reference = request.form['reservation_reference']
    else:
        transactions_from = request.args.get('transactions_from', None)
        transactions_to = request.args.get('transactions_to', None)
        theater_id = request.args.get('theater_id', None)
        platform = request.args.get('platform', None)
        payment_type = request.args.getlist('payment_type')
        org_uuids = request.args.getlist('org_uuids')
        payment_reference = request.args.get('payment_reference', None)
        reservation_reference = request.args.get('reservation_reference', None)

    if not transactions_from or transactions_from is None:
        transactions_from = str(date_today)

    if not transactions_to or transactions_to is None:
        transactions_to = str(date_today)

    theaters = []
    tx_from = admin.parse_string_to_date(transactions_from + ' 00:00:00', fdate='%Y-%m-%d %H:%M:%S')
    tx_to = admin.parse_string_to_date(transactions_to + ' 23:59:59', fdate='%Y-%m-%d %H:%M:%S')
    transactions = admin.get_date_range_string(transactions_from, transactions_to, dates={})
    queryset = admin.get_filtered_transactions(state.TX_CANCELLED, tx_from=tx_from,
            tx_to=tx_to, payment_type=payment_type, org_uuids=org_uuids,
            theater_id=theater_id, platform=platform, payment_reference=payment_reference,
            reservation_reference=reservation_reference)

    for org_uuid in org_uuids:
        theaterorg_key = ndb.Key(TheaterOrganization, str(org_uuid))
        theaterorg = theaterorg_key.get()

        if theaterorg is not None:
            theater_infos = Theater.query(ancestor=theaterorg_key).fetch()
            theaters.extend(theater_infos)

    if request.method == 'POST':
        row_counter = 0
        workbook = xlwt.Workbook(encoding='utf-8')
        worksheet = workbook.add_sheet("Sheet 1")
        headers_font_style = xlwt.XFStyle()
        headers_font_style.font.bold = True
        font_style = xlwt.XFStyle()
        font_style.font.bold = False

        worksheet.write(0, 0, 'DEVICE ID', headers_font_style)
        worksheet.write(0, 1, 'TRANSACTION ID', headers_font_style)
        worksheet.write(0, 2, 'RESERVATION CODE', headers_font_style)
        worksheet.write(0, 3, 'EMAIL', headers_font_style)
        worksheet.write(0, 4, 'MOBILE', headers_font_style)
        worksheet.write(0, 5, 'PAYMENT TYPE', headers_font_style)
        worksheet.write(0, 6, 'PROMO_CODE', headers_font_style)
        worksheet.write(0, 7, 'CLAIM CODE', headers_font_style)
        worksheet.write(0, 8, 'TRANSACTION DATE', headers_font_style)
        worksheet.write(0, 9, 'TRANSACTION TIME', headers_font_style)

        for transaction in queryset:
            row_counter += 1
            email_address   = ''
            mobile_number   = 'N/A'
            promo_code      = 'N/A'
            claim_code      = 'N/A'

            if transaction.ticket: 
                if 'theater_name' in transaction.ticket.extra and transaction.ticket.extra['theater_name']:
                    cinema_partner = transaction.ticket.extra['theater_name']
                elif 'theater_and_cinema_name' in transaction.ticket.extra and transaction.ticket.extra['theater_and_cinema_name']:
                    cinema_partner = transaction.ticket.extra['theater_and_cinema_name'][0].split('-')[0].strip()
            if transaction.payment_info:
                if 'email' in transaction.payment_info and transaction.payment_info['email']:
                    email_address = transaction.payment_info['email']

                if 'mobile' in transaction.payment_info and transaction.payment_info['mobile']:
                    mobile_number = transaction.payment_info['mobile']
                elif transaction.user_info and 'mobile' in transaction.user_info and transaction.user_info['mobile']:
                    mobile_number = transaction.user_info['mobile']
                elif 'contact_number' in transaction.payment_info and transaction.payment_info['contact_number']:
                    mobile_number = transaction.payment_info['contact_number']
            if transaction.ticket:
                if 'total_amount' in transaction.ticket.extra and transaction.ticket.extra['total_amount']:
                    total_amount = transaction.ticket.extra['total_amount']
            if transaction.payment_info:
                if transaction.discount_info and 'claim_code' in transaction.discount_info and transaction.discount_info['claim_code']:
                    promo_code = transaction.discount_info['claim_code']

                if transaction.payment_info and 'promo_code' in transaction.payment_info and transaction.payment_info['promo_code']:
                    claim_code = transaction.payment_info['promo_code']

            transaction_datetime = admin.convert_timezone(transaction.date_created, 8, '+')
            transaction_date = admin.parse_date_to_string(transaction_datetime.date(), fdate='%m/%d/%Y')
            transaction_time = admin.parse_time_to_string(transaction_datetime.time(), ftime='%I:%M %p')

            worksheet.write(row_counter, 0, transaction.key.parent().id(), font_style)
            worksheet.write(row_counter, 1, transaction.key.id(), font_style)
            worksheet.write(row_counter, 2, transaction.reservation_reference, font_style)
            worksheet.write(row_counter, 3, email_address, font_style)
            worksheet.write(row_counter, 4, mobile_number, font_style)
            worksheet.write(row_counter, 5, transaction.payment_type, font_style)
            worksheet.write(row_counter, 6, promo_code, font_style)
            worksheet.write(row_counter, 7, claim_code, font_style)
            worksheet.write(row_counter, 8, transaction_date, font_style)
            worksheet.write(row_counter, 9, transaction_time, font_style)

        tx_reports = StringIO.StringIO()
        workbook.save(tx_reports)

        res = make_response(tx_reports.getvalue())
        res.headers['Content-Type'] = 'application/vnd.ms-excel'
        res.headers['Content-Disposition'] = 'attachment; filename=Cancelled-ReapedTransactions.xls'

        return res

    transactions_page, next_cursor, more = queryset.fetch_page(100, start_cursor=cursor)

    return render_template('transactions_cancelled.html', transactions=transactions_page,
            next_cursor=next_cursor, more=more, transactions_from=transactions_from,
            transactions_to=transactions_to, platform=platform, theater_id=theater_id,
            org_uuids=org_uuids, payment_type=payment_type, theaters=theaters,
            parse_float_to_string=admin.parse_float_to_string, orgs=orgs,
            payment_reference=payment_reference, reservation_reference=reservation_reference)

@web.route('/transactions/cancelled_cash/',  methods=['GET', 'POST'])
def transactions_cancelled_cash():
    cursor = ndb.Cursor(urlsafe=request.args.get('cursor'))
    date_today = datetime.datetime.today().date()
    payment_type = ['cash']
    if request.method == 'POST':
        transactions_from = request.form['transactions_from']
        transactions_to = request.form['transactions_to']
        theater_id = request.form['theater_id']
        platform = request.form['platform']
        org_uuids = request.form.getlist('org_uuids')
        payment_reference = request.form['payment_reference']
        reservation_reference = request.form['reservation_reference']
    else:
        transactions_from = request.args.get('transactions_from', None)
        transactions_to = request.args.get('transactions_to', None)
        theater_id = request.args.get('theater_id', None)
        platform = request.args.get('platform', None)
        org_uuids = request.args.getlist('org_uuids')
        payment_reference = request.args.get('payment_reference', None)
        reservation_reference = request.args.get('reservation_reference', None)

    theaters = []
    transactions_from = transactions_from if transactions_from else str(date_today)
    transactions_to = transactions_to if transactions_to else str(date_today)
    tx_from = admin.parse_string_to_date(transactions_from + ' 00:00:00', fdate='%Y-%m-%d %H:%M:%S')
    tx_to = admin.parse_string_to_date(transactions_to + ' 23:59:59', fdate='%Y-%m-%d %H:%M:%S')
    transactions = admin.get_date_range_string(transactions_from, transactions_to, dates={})
    queryset = admin.get_filtered_transactions(state.TX_CANCELLED_CASH, tx_from=tx_from, tx_to=tx_to,
            payment_type=payment_type, org_uuids=org_uuids, theater_id=theater_id, platform=platform,
            payment_reference=payment_reference, reservation_reference=reservation_reference)

    for org_uuid in org_uuids:
        theaterorg_key = ndb.Key(TheaterOrganization, str(org_uuid))
        theaterorg = theaterorg_key.get()

        if theaterorg is not None:
            theater_infos = Theater.query(ancestor=theaterorg_key).fetch()
            theaters.extend(theater_infos)

    if request.method == 'POST':
        row_counter = 0
        workbook = xlwt.Workbook(encoding='utf-8')
        worksheet = workbook.add_sheet("Sheet 1")
        headers_font_style = xlwt.XFStyle()
        headers_font_style.font.bold = True
        font_style = xlwt.XFStyle()
        font_style.font.bold = False

        worksheet.write(0, 0, 'DEVICE ID', headers_font_style)
        worksheet.write(0, 1, 'TRANSACTION ID', headers_font_style)
        worksheet.write(0, 2, 'RESERVATION CODE', headers_font_style)
        worksheet.write(0, 3, 'TICKET CODE', headers_font_style)
        worksheet.write(0, 4, 'CINEMA PARTNER', headers_font_style)
        worksheet.write(0, 5, 'EMAIL', headers_font_style)
        worksheet.write(0, 6, 'MOBILE', headers_font_style)
        worksheet.write(0, 7, 'PAYMENT TYPE', headers_font_style)
        worksheet.write(0, 8, 'TOTAL AMOUNT', headers_font_style)
        worksheet.write(0, 9, 'TRANSACTION DATE', headers_font_style)
        worksheet.write(0, 10, 'TRANSACTION TIME', headers_font_style)
        worksheet.write(0, 11, 'CANCEL DATE', headers_font_style)
        worksheet.write(0, 12, 'CANCEL TIME', headers_font_style)
        worksheet.write(0, 13, 'CANCEL REASON', headers_font_style)
        worksheet.write(0, 14, 'CANCELLED BY', headers_font_style)

        for transaction in queryset:
            row_counter += 1
            ticket_code     = transaction.ticket.code if transaction.ticket.code else ''
            cinema_partner  = ''
            email_address   = ''
            mobile_number   = 'N/A'
            total_amount    = ''
            transaction_cancel_datetime = ''
            transaction_cancel_date     = ''
            transaction_cancel_time     = ''
            transaction_cancel_reason   = 'N/A'
            transaction_cancel_by     = 'N/A'

            if 'theater_name' in transaction.ticket.extra and transaction.ticket.extra['theater_name']:
                cinema_partner = transaction.ticket.extra['theater_name']
            elif 'theater_and_cinema_name' in transaction.ticket.extra and transaction.ticket.extra['theater_and_cinema_name']:
                cinema_partner = transaction.ticket.extra['theater_and_cinema_name'][0].split('-')[0].strip()

            if 'email' in transaction.payment_info and transaction.payment_info['email']:
                email_address = transaction.payment_info['email']

            if 'mobile' in transaction.payment_info and transaction.payment_info['mobile']:
                mobile_number = transaction.payment_info['mobile']
            elif transaction.user_info and 'mobile' in transaction.user_info and transaction.user_info['mobile']:
                mobile_number = transaction.user_info['mobile']
            elif 'contact_number' in transaction.payment_info and transaction.payment_info['contact_number']:
                mobile_number = transaction.payment_info['contact_number']

            if 'total_amount' in transaction.ticket.extra and transaction.ticket.extra['total_amount']:
                total_amount = transaction.ticket.extra['total_amount']

            transaction_datetime = admin.convert_timezone(transaction.date_created, 8, '+')
            transaction_date = admin.parse_date_to_string(transaction_datetime.date(), fdate='%m/%d/%Y')
            transaction_time = admin.parse_time_to_string(transaction_datetime.time(), ftime='%I:%M %p')

            if transaction.date_cancelled:
                transaction_cancel_datetime = admin.convert_timezone(transaction.date_cancelled, 8, '+')
                transaction_cancel_date = admin.parse_date_to_string(transaction_cancel_datetime.date(), fdate='%m/%d/%Y')
                transaction_cancel_time = admin.parse_time_to_string(transaction_cancel_datetime.time(), ftime='%I:%M %p')
            if transaction.refund_reason:
                transaction_cancel_reason = transaction.refund_reason
            if transaction.refunded_by:
                transaction_cancel_by = transaction.refunded_by

            worksheet.write(row_counter, 0, transaction.key.parent().id(), font_style)
            worksheet.write(row_counter, 1, transaction.key.id(), font_style)
            worksheet.write(row_counter, 2, transaction.reservation_reference, font_style)
            worksheet.write(row_counter, 3, ticket_code, font_style)
            worksheet.write(row_counter, 4, cinema_partner, font_style)
            worksheet.write(row_counter, 5, email_address, font_style)
            worksheet.write(row_counter, 6, mobile_number, font_style)
            worksheet.write(row_counter, 7, transaction.payment_type, font_style)
            worksheet.write(row_counter, 8, total_amount, font_style)
            worksheet.write(row_counter, 9, transaction_date, font_style)
            worksheet.write(row_counter, 10, transaction_time, font_style)
            worksheet.write(row_counter, 11, transaction_cancel_date, font_style)
            worksheet.write(row_counter, 12, transaction_cancel_time, font_style)
            worksheet.write(row_counter, 13, transaction_cancel_reason, font_style)
            worksheet.write(row_counter, 14, transaction_cancelled_by, font_style)

        tx_reports = StringIO.StringIO()
        workbook.save(tx_reports)

        res = make_response(tx_reports.getvalue())
        res.headers['Content-Type'] = 'application/vnd.ms-excel'
        res.headers['Content-Disposition'] = 'attachment; filename=CancelledCashTransactions.xls'

        return res

    transactions_page, next_cursor, more = queryset.fetch_page(100, start_cursor=cursor)

    return render_template('transactions_cancelled_cash.html', transactions=transactions_page, next_cursor=next_cursor,
            more=more, transactions_from=transactions_from, transactions_to=transactions_to, platform=platform,
            theater_id=theater_id, org_uuids=org_uuids, theaters=theaters,
            parse_float_to_string=admin.parse_float_to_string, orgs=orgs,
            payment_reference=payment_reference, reservation_reference=reservation_reference)


@web.route('/transactions/refunded/',  methods=['GET', 'POST'])
def transactions_refunded():
    cursor = ndb.Cursor(urlsafe=request.args.get('cursor'))
    date_today = datetime.datetime.today().date()

    if request.method == 'POST':
        transactions_from = request.form['transactions_from']
        transactions_to = request.form['transactions_to']
        theater_id = request.form['theater_id']
        platform = request.form['platform']
        payment_type = request.form.getlist('payment_type')
        org_uuids = request.form.getlist('org_uuids')
        payment_reference = request.form['payment_reference']
        reservation_reference = request.form['reservation_reference']
    else:
        transactions_from = request.args.get('transactions_from', None)
        transactions_to = request.args.get('transactions_to', None)
        theater_id = request.args.get('theater_id', None)
        platform = request.args.get('platform', None)
        payment_type = request.args.getlist('payment_type')
        org_uuids = request.args.getlist('org_uuids')
        payment_reference = request.args.get('payment_reference', None)
        reservation_reference = request.args.get('reservation_reference', None)

    theaters = []
    transactions_from = transactions_from if transactions_from else str(date_today)
    transactions_to = transactions_to if transactions_to else str(date_today)
    tx_from = admin.parse_string_to_date(transactions_from + ' 00:00:00', fdate='%Y-%m-%d %H:%M:%S')
    tx_to = admin.parse_string_to_date(transactions_to + ' 23:59:59', fdate='%Y-%m-%d %H:%M:%S')
    transactions = admin.get_date_range_string(transactions_from, transactions_to, dates={})
    queryset = admin.get_filtered_transactions(state.TX_REFUNDED, tx_from=tx_from, tx_to=tx_to,
            payment_type=payment_type, org_uuids=org_uuids, theater_id=theater_id, platform=platform,
            payment_reference=payment_reference, reservation_reference=reservation_reference)

    for org_uuid in org_uuids:
        theaterorg_key = ndb.Key(TheaterOrganization, str(org_uuid))
        theaterorg = theaterorg_key.get()

        if theaterorg is not None:
            theater_infos = Theater.query(ancestor=theaterorg_key).fetch()
            theaters.extend(theater_infos)

    if request.method == 'POST':
        row_counter = 0
        workbook = xlwt.Workbook(encoding='utf-8')
        worksheet = workbook.add_sheet("Sheet 1")
        headers_font_style = xlwt.XFStyle()
        headers_font_style.font.bold = True
        font_style = xlwt.XFStyle()
        font_style.font.bold = False

        worksheet.write(0, 0, 'DEVICE ID', headers_font_style)
        worksheet.write(0, 1, 'TRANSACTION ID', headers_font_style)
        worksheet.write(0, 2, 'RESERVATION CODE', headers_font_style)
        worksheet.write(0, 3, 'TICKET CODE', headers_font_style)
        worksheet.write(0, 4, 'CINEMA PARTNER', headers_font_style)
        worksheet.write(0, 5, 'EMAIL', headers_font_style)
        worksheet.write(0, 6, 'MOBILE', headers_font_style)
        worksheet.write(0, 7, 'PAYMENT TYPE', headers_font_style)
        worksheet.write(0, 8, 'TOTAL AMOUNT', headers_font_style)
        worksheet.write(0, 9, 'PROMO_CODE', headers_font_style)
        worksheet.write(0, 10, 'PROMO AMOUNT', headers_font_style)
        worksheet.write(0, 11, 'CLAIM CODE', headers_font_style)
        worksheet.write(0, 12, 'TRANSACTION DATE', headers_font_style)
        worksheet.write(0, 13, 'TRANSACTION TIME', headers_font_style)
        worksheet.write(0, 14, 'REFUND DATE', headers_font_style)
        worksheet.write(0, 15, 'REFUND TIME', headers_font_style)
        worksheet.write(0, 16, 'REFUND REASON', headers_font_style)
        worksheet.write(0, 17, 'REFUNDED BY', headers_font_style)

        for transaction in queryset:
            row_counter += 1
            ticket_code     = transaction.ticket.code if transaction.ticket.code else ''
            cinema_partner  = ''
            email_address   = ''
            mobile_number   = 'N/A'
            total_amount    = ''
            promo_code      = 'N/A'
            promo_discount  = ''
            claim_code      = 'N/A'
            transaction_refund_datetime = ''
            transaction_refund_date     = ''
            transaction_refund_time     = ''
            transaction_refund_reason   = 'N/A'
            transaction_refunded_by     = 'N/A'

            if 'theater_name' in transaction.ticket.extra and transaction.ticket.extra['theater_name']:
                cinema_partner = transaction.ticket.extra['theater_name']
            elif 'theater_and_cinema_name' in transaction.ticket.extra and transaction.ticket.extra['theater_and_cinema_name']:
                cinema_partner = transaction.ticket.extra['theater_and_cinema_name'][0].split('-')[0].strip()

            if 'email' in transaction.payment_info and transaction.payment_info['email']:
                email_address = transaction.payment_info['email']

            if 'mobile' in transaction.payment_info and transaction.payment_info['mobile']:
                mobile_number = transaction.payment_info['mobile']
            elif transaction.user_info and 'mobile' in transaction.user_info and transaction.user_info['mobile']:
                mobile_number = transaction.user_info['mobile']
            elif 'contact_number' in transaction.payment_info and transaction.payment_info['contact_number']:
                mobile_number = transaction.payment_info['contact_number']

            if 'total_amount' in transaction.ticket.extra and transaction.ticket.extra['total_amount']:
                total_amount = transaction.ticket.extra['total_amount']

            if transaction.discount_info and 'claim_code' in transaction.discount_info and transaction.discount_info['claim_code']:
                promo_code = transaction.discount_info['claim_code']

            if transaction.discount_info and 'total_discount' in transaction.discount_info and transaction.discount_info['total_discount']:
                promo_discount = transaction.discount_info['total_discount']

            if transaction.payment_info and 'promo_code' in transaction.payment_info and transaction.payment_info['promo_code']:
                claim_code = transaction.payment_info['promo_code']

            transaction_datetime = admin.convert_timezone(transaction.date_created, 8, '+')
            transaction_date = admin.parse_date_to_string(transaction_datetime.date(), fdate='%m/%d/%Y')
            transaction_time = admin.parse_time_to_string(transaction_datetime.time(), ftime='%I:%M %p')

            if transaction.date_cancelled:
                transaction_refund_datetime = admin.convert_timezone(transaction.date_cancelled, 8, '+')
                transaction_refund_date = admin.parse_date_to_string(transaction_refund_datetime.date(), fdate='%m/%d/%Y')
                transaction_refund_time = admin.parse_time_to_string(transaction_refund_datetime.time(), ftime='%I:%M %p')
            if transaction.refund_reason:
                transaction_refund_reason = transaction.refund_reason
            if transaction.refunded_by:
                transaction_refunded_by = transaction.refunded_by

            worksheet.write(row_counter, 0, transaction.key.parent().id(), font_style)
            worksheet.write(row_counter, 1, transaction.key.id(), font_style)
            worksheet.write(row_counter, 2, transaction.reservation_reference, font_style)
            worksheet.write(row_counter, 3, ticket_code, font_style)
            worksheet.write(row_counter, 4, cinema_partner, font_style)
            worksheet.write(row_counter, 5, email_address, font_style)
            worksheet.write(row_counter, 6, mobile_number, font_style)
            worksheet.write(row_counter, 7, transaction.payment_type, font_style)
            worksheet.write(row_counter, 8, total_amount, font_style)
            worksheet.write(row_counter, 9, promo_code, font_style)
            worksheet.write(row_counter, 10, promo_discount, font_style)
            worksheet.write(row_counter, 11, claim_code, font_style)
            worksheet.write(row_counter, 12, transaction_date, font_style)
            worksheet.write(row_counter, 13, transaction_time, font_style)
            worksheet.write(row_counter, 14, transaction_refund_date, font_style)
            worksheet.write(row_counter, 15, transaction_refund_time, font_style)
            worksheet.write(row_counter, 16, transaction_refund_reason, font_style)
            worksheet.write(row_counter, 17, transaction_refunded_by, font_style)

        tx_reports = StringIO.StringIO()
        workbook.save(tx_reports)

        res = make_response(tx_reports.getvalue())
        res.headers['Content-Type'] = 'application/vnd.ms-excel'
        res.headers['Content-Disposition'] = 'attachment; filename=RefundedTransactions.xls'

        return res

    transactions_page, next_cursor, more = queryset.fetch_page(100, start_cursor=cursor)

    return render_template('transactions_refunded.html', transactions=transactions_page, next_cursor=next_cursor,
            more=more, transactions_from=transactions_from, transactions_to=transactions_to, platform=platform,
            theater_id=theater_id, org_uuids=org_uuids, payment_type=payment_type, theaters=theaters,
            parse_float_to_string=admin.parse_float_to_string, orgs=orgs,
            payment_reference=payment_reference, reservation_reference=reservation_reference)

@web.route('/transactions/sales_reports/',  methods=['GET', 'POST'])
def transactions_sales_reports():
    date_today = datetime.datetime.today().date()

    if request.method == 'POST':
        transactions_from = request.form['transactions_from']
        transactions_to = request.form['transactions_to']
        theater_id = request.form['theater_id']
        platform = request.form['platform']
        payment_type = request.form.getlist('payment_type')
        org_uuids = request.form.getlist('org_uuids')
    else:
        transactions_from = request.args.get('transactions_from', None)
        transactions_to = request.args.get('transactions_to', None)
        theater_id = request.args.get('theater_id', None)
        platform = request.args.get('platform', None)
        payment_type = request.args.getlist('payment_type')
        org_uuids = request.args.getlist('org_uuids')

    if not transactions_from or transactions_from is None:
        transactions_from = str(date_today)

    if not transactions_to or transactions_to is None:
        transactions_to = str(date_today)

    theaters = []
    total_transactions = 0
    total_tickets_sold = 0
    total_amount = 0.0
    total_net_revenue = 0.0
    tx_from = admin.parse_string_to_date(transactions_from + ' 00:00:00', fdate='%Y-%m-%d %H:%M:%S')
    tx_to = admin.parse_string_to_date(transactions_to + ' 23:59:59', fdate='%Y-%m-%d %H:%M:%S')
    transactions = admin.get_date_range_string(transactions_from, transactions_to, dates={})
    queryset = admin.get_filtered_transactions([state.TX_DONE, state.TX_REFUNDED], tx_from=tx_from,
            tx_to=tx_to, payment_type=payment_type, org_uuids=org_uuids,
            theater_id=theater_id, platform=platform)

    for org_uuid in org_uuids:
        theaterorg_key = ndb.Key(TheaterOrganization, str(org_uuid))
        theaterorg = theaterorg_key.get()

        if theaterorg is not None:
            theater_infos = Theater.query(ancestor=theaterorg_key).fetch()
            theaters.extend(theater_infos)

    for ent in queryset:
        tx_date_created = admin.convert_timezone(ent.date_created, 8, '+')
        date_to_string = admin.parse_date_to_string(tx_date_created, fdate='%m/%d/%Y')
        org_uuid = ent.reservations[0].theater.parent().id()
        total_ticket_price = '0.00'

        if ent.was_reaped != True:
            total_ticket_price = ent.ticket.extra['total_amount']
        else:
            if ent.discount_info and 'total_amount' in ent.discount_info:
                total_ticket_price = ent.discount_info['total_amount']

        seats = len(ent.reservations)

        if ent.was_reaped != True:
            convenience_fee = ent.ticket.extra['reservation_fee']
        else:
            if ent.payment_type in ['client-initiated', 'paynamics-payment-initiated', 'ipay88-payment-initiated']:
                convenience_fee = '20.00'
            else:
                convenience_fee = '0.00'

        net_revenue = admin.get_net_revenue(org_uuid, total_ticket_price, convenience_fee, seats)

        if date_to_string in transactions:
            transactions[date_to_string]['transactions'] += 1
            transactions[date_to_string]['seats'] += seats
            transactions[date_to_string]['amount'] += float(total_ticket_price)
            transactions[date_to_string]['net_revenue'] += net_revenue
            total_transactions += 1
            total_tickets_sold += seats
            total_amount += float(total_ticket_price)
            total_net_revenue += net_revenue

    transactions_dates = sorted(list(transactions))

    if request.method == 'POST':
        row_counter = 0
        workbook = xlwt.Workbook(encoding='utf-8')
        worksheet = workbook.add_sheet("Sheet 1")
        headers_font_style = xlwt.XFStyle()
        headers_font_style.font.bold = True
        font_style = xlwt.XFStyle()
        font_style.font.bold = False

        worksheet.write(0, 0, 'PURCHASED DATE', headers_font_style)
        worksheet.write(0, 1, 'TRANSACTIONS', headers_font_style)
        worksheet.write(0, 2, 'TICKETS SOLD', headers_font_style)
        worksheet.write(0, 3, 'NET SALES', headers_font_style)
        worksheet.write(0, 4, 'NET REVENUE', headers_font_style)

        for purchased_date in transactions_dates:
            row_counter += 1
            worksheet.write(row_counter, 0, purchased_date, font_style)

            if purchased_date in transactions:
                worksheet.write(row_counter, 1, transactions[purchased_date]['transactions'], font_style)
                worksheet.write(row_counter, 2, transactions[purchased_date]['seats'], font_style)
                worksheet.write(row_counter, 3, admin.parse_float_to_string(transactions[purchased_date]['amount']), font_style)
                worksheet.write(row_counter, 4, admin.parse_float_to_string(transactions[purchased_date]['net_revenue']), font_style)

        row_counter += 2
        worksheet.write(row_counter, 0, 'TOTAL:', font_style)
        worksheet.write(row_counter, 1, total_transactions, font_style)
        worksheet.write(row_counter, 2, total_tickets_sold, font_style)
        worksheet.write(row_counter, 3, admin.parse_float_to_string(total_amount), font_style)
        worksheet.write(row_counter, 4, admin.parse_float_to_string(total_net_revenue), font_style)

        sales_reports = StringIO.StringIO()
        workbook.save(sales_reports)

        res = make_response(sales_reports.getvalue())
        res.headers['Content-Type'] = 'application/vnd.ms-excel'
        res.headers['Content-Disposition'] = 'attachment; filename=MobileSalesReports.xls'

        return res

    if theater_id:
        theater_id = int(theater_id)

    return render_template('transactions_sales_reports.html',
            transactions=transactions, transactions_dates=transactions_dates,
            transactions_from=transactions_from, transactions_to=transactions_to,
            platform=platform, theater_id=theater_id, org_uuids=org_uuids,
            payment_type=payment_type, theaters=theaters, total_transactions=total_transactions,
            total_tickets_sold=total_tickets_sold, total_amount=total_amount,
            total_net_revenue=total_net_revenue, parse_float_to_string=admin.parse_float_to_string, orgs=orgs)

@web.route('/transactions/reports/',  methods=['GET', 'POST'])
def transactions_reports():
    cursor = ndb.Cursor(urlsafe=request.args.get('cursor'))
    date_today = datetime.datetime.today().date()

    if request.method == 'POST':
        transactions_from = request.form['transactions_from']
        transactions_to = request.form['transactions_to']
        theater_id = request.form['theater_id']
        platform = request.form['platform']
        payment_type = request.form.getlist('payment_type')
        org_uuids = request.form.getlist('org_uuids')
        payment_reference = request.form['payment_reference']
        reservation_reference = request.form['reservation_reference']
    else:
        transactions_from = request.args.get('transactions_from', None)
        transactions_to = request.args.get('transactions_to', None)
        theater_id = request.args.get('theater_id', None)
        platform = request.args.get('platform', None)
        payment_type = request.args.getlist('payment_type')
        org_uuids = request.args.getlist('org_uuids')
        payment_reference = request.args.get('payment_reference', None)
        reservation_reference = request.args.get('reservation_reference', None)

    theaters = []
    transactions_from = transactions_from if transactions_from else str(date_today)
    transactions_to = transactions_to if transactions_to else str(date_today)
    tx_from = admin.parse_string_to_date(transactions_from + ' 00:00:00', fdate='%Y-%m-%d %H:%M:%S')
    tx_to = admin.parse_string_to_date(transactions_to + ' 23:59:59', fdate='%Y-%m-%d %H:%M:%S')
    transactions = admin.get_date_range_string(transactions_from, transactions_to, dates={})
    queryset = admin.get_filtered_transactions([state.TX_DONE, state.TX_REFUNDED], tx_from=tx_from, tx_to=tx_to,
            payment_type=payment_type, org_uuids=org_uuids, theater_id=theater_id, platform=platform,
            payment_reference=payment_reference, reservation_reference=reservation_reference)

    for org_uuid in org_uuids:
        theaterorg_key = ndb.Key(TheaterOrganization, str(org_uuid))
        theaterorg = theaterorg_key.get()

        if theaterorg is not None:
            theater_infos = Theater.query(ancestor=theaterorg_key).fetch()
            theaters.extend(theater_infos)

    if request.method == 'POST':
        row_counter = 0
        workbook = xlwt.Workbook(encoding='utf-8')
        worksheet = workbook.add_sheet("Sheet 1")
        headers_font_style = xlwt.XFStyle()
        headers_font_style.font.bold = True
        font_style = xlwt.XFStyle()
        font_style.font.bold = False

        worksheet.write(0, 0, 'WEEK NUMBER', headers_font_style)
        worksheet.write(0, 1, 'TRANSACTION DAY', headers_font_style)
        worksheet.write(0, 2, 'TRANSACTION DATE', headers_font_style)
        worksheet.write(0, 3, 'TRANSACTION TIME', headers_font_style)
        worksheet.write(0, 4, 'TRANSACTION TIME CLUSTER', headers_font_style)
        worksheet.write(0, 5, 'MOVIE TITLE', headers_font_style)
        worksheet.write(0, 6, 'CINEMA PARTNER', headers_font_style)
        worksheet.write(0, 7, 'CINEMA NUMBER', headers_font_style)
        worksheet.write(0, 8, 'EMAIL', headers_font_style)
        worksheet.write(0, 9, 'MOBILE', headers_font_style)
        worksheet.write(0, 10, 'TICKET SOLD', headers_font_style)
        worksheet.write(0, 11, 'NET SALES', headers_font_style)
        worksheet.write(0, 12, 'TICKET DATE', headers_font_style)
        worksheet.write(0, 13, 'TICKET TIME', headers_font_style)
        worksheet.write(0, 14, 'TICKET TIME CLUSTER', headers_font_style)
        worksheet.write(0, 15, 'PLATFORM', headers_font_style)
        worksheet.write(0, 16, 'PROMO_CODE', headers_font_style)
        worksheet.write(0, 17, 'PROMO DISCOUNT', headers_font_style)
        worksheet.write(0, 18, 'DEVICE ID', headers_font_style)
        worksheet.write(0, 19, 'TRANSACTION ID', headers_font_style)
        worksheet.write(0, 20, 'RESERVATION CODE', headers_font_style)
        worksheet.write(0, 21, 'CLAIM CODE', headers_font_style)
        worksheet.write(0, 22, 'STATE', headers_font_style)
        worksheet.write(0, 23, 'REFUND DATE', headers_font_style)
        worksheet.write(0, 24, 'REFUND REASON', headers_font_style)
        worksheet.write(0, 25, 'REFUNDED BY', headers_font_style)
        worksheet.write(0, 26, 'STATUS', headers_font_style)

        for transaction in queryset:
            row_counter += 1
            movie_title = 'N/A'
            cinema_partner = 'N/A'
            cinema_number = 'N/A'
            email_address = 'N/A'
            mobile_number = 'N/A'
            gross_sales = ''
            promo_code = 'N/A'
            promo_discount = 'N/A'
            claim_code = 'N/A'
            transaction_refund_datetime = 'N/A'
            transaction_refund_reason   = 'N/A'
            transaction_refunded_by     = 'N/A'
            transaction_status     = 'unclaimed'

            if 'email' in transaction.payment_info and transaction.payment_info['email']:
                email_address = transaction.payment_info['email']

            if 'mobile' in transaction.payment_info and transaction.payment_info['mobile']:
                mobile_number = transaction.payment_info['mobile']
            elif transaction.user_info and 'mobile' in transaction.user_info and transaction.user_info['mobile']:
                mobile_number = transaction.user_info['mobile']
            elif 'contact_number' in transaction.payment_info and transaction.payment_info['contact_number']:
                mobile_number = transaction.payment_info['contact_number']

            if transaction.was_reaped != True:
                if 'movie_canonical_title' in transaction.ticket.extra and transaction.ticket.extra['movie_canonical_title']:
                    movie_title = transaction.ticket.extra['movie_canonical_title']
                elif 'movie_title' in transaction.ticket.extra and transaction.ticket.extra['movie_title']:
                    movie_title = transaction.ticket.extra['movie_title']

                if 'theater_name' in transaction.ticket.extra and transaction.ticket.extra['theater_name']:
                    cinema_partner = transaction.ticket.extra['theater_name']
                elif 'theater_and_cinema_name' in transaction.ticket.extra and transaction.ticket.extra['theater_and_cinema_name']:
                    cinema_partner = transaction.ticket.extra['theater_and_cinema_name'][0].split('-')[0].strip()
                if 'cinema_name' in transaction.ticket.extra and transaction.ticket.extra['cinema_name']:
                    cinema_number = transaction.ticket.extra['cinema_name']
                elif 'theater_and_cinema_name' in transaction.ticket.extra and transaction.ticket.extra['theater_and_cinema_name']:
                    cinema_number = transaction.ticket.extra['theater_and_cinema_name'][0].split('-')[-1].replace('Cinema:', '').strip()

                if len(cinema_number) <= 2:
                    cinema_number = 'Cinema %s' % cinema_number
                if 'total_amount' in transaction.ticket.extra and transaction.ticket.extra['total_amount']:
                    gross_sales = transaction.ticket.extra['total_amount']
            else:
                if transaction.discount_info and 'total_amount' in transaction.discount_info:
                    gross_sales = transaction.discount_info['total_amount']

            if transaction.discount_info and 'claim_code' in transaction.discount_info and transaction.discount_info['claim_code']:
                promo_code = transaction.discount_info['claim_code']

            if transaction.payment_info and 'promo_code' in transaction.payment_info and transaction.payment_info['promo_code']:
                claim_code = transaction.payment_info['promo_code']

            if transaction.discount_info and 'total_discount' in transaction.discount_info and transaction.discount_info['total_discount']:
                promo_discount = transaction.discount_info['total_discount']

            if transaction.date_cancelled:
                transaction_refund_datetime = admin.convert_timezone(transaction.date_cancelled, 8, '+')
                transaction_refund_datetime = admin.parse_date_to_string(transaction_refund_datetime, fdate='%m/%d/%Y %I:%M %p')
            if transaction.refund_reason:
                transaction_refund_reason = transaction.refund_reason
            if transaction.refunded_by:
                transaction_refunded_by = transaction.refunded_by
            transaction_datetime = admin.convert_timezone(transaction.date_created, 8, '+')
            isoyear, isoweek, isoday = transaction_datetime.isocalendar()

            if transaction.status:
                transaction_status = transaction.status

            week_number = isoweek
            transaction_day = admin.ISO_WEEKDAY[str(isoday)]
            transaction_date = admin.parse_date_to_string(transaction_datetime.date(), fdate='%m/%d/%Y')
            transaction_time = admin.parse_time_to_string(transaction_datetime.time(), ftime='%I:%M %p')
            transaction_time_cluster = admin.get_transaction_time_cluster(transaction_datetime.time().hour) if transaction_datetime else ''

            ticket_datetime = None

            if transaction.was_reaped != True:
                if 'show_datetime' in transaction.ticket.extra and transaction.ticket.extra['show_datetime']:
                    ticket_datetime = admin.parse_string_to_datetime(transaction.ticket.extra['show_datetime'], '%m/%d/%Y %I:%M %p')

            ticket_date = admin.parse_date_to_string(ticket_datetime.date(), fdate='%m/%d/%Y') if ticket_datetime else ''
            ticket_time = admin.parse_time_to_string(ticket_datetime.time(), ftime='%I:%M %p') if ticket_datetime else ''
            ticket_time_cluster = admin.get_ticket_time_cluster(ticket_datetime.time().hour) if ticket_datetime else ''

            worksheet.write(row_counter, 0, week_number, font_style)
            worksheet.write(row_counter, 1, transaction_day, font_style)
            worksheet.write(row_counter, 2, transaction_date, font_style)
            worksheet.write(row_counter, 3, transaction_time, font_style)
            worksheet.write(row_counter, 4, transaction_time_cluster, font_style)
            worksheet.write(row_counter, 5, movie_title, font_style)
            worksheet.write(row_counter, 6, cinema_partner, font_style)
            worksheet.write(row_counter, 7, cinema_number, font_style)
            worksheet.write(row_counter, 8, email_address, font_style)
            worksheet.write(row_counter, 9, mobile_number, font_style)
            worksheet.write(row_counter, 10, len(transaction.reservations), font_style)
            worksheet.write(row_counter, 11, gross_sales, font_style)
            worksheet.write(row_counter, 12, ticket_date, font_style)
            worksheet.write(row_counter, 13, ticket_time, font_style)
            worksheet.write(row_counter, 14, ticket_time_cluster, font_style)
            worksheet.write(row_counter, 15, transaction.platform, font_style)
            worksheet.write(row_counter, 16, promo_code, font_style)
            worksheet.write(row_counter, 17, promo_discount, font_style)
            worksheet.write(row_counter, 18, transaction.key.parent().id(), font_style)
            worksheet.write(row_counter, 19, transaction.key.id(), font_style)
            worksheet.write(row_counter, 20, transaction.reservation_reference, font_style)
            worksheet.write(row_counter, 21, claim_code, font_style)
            worksheet.write(row_counter, 22, to_state_str(transaction.state), font_style)
            worksheet.write(row_counter, 23, transaction_refund_datetime, font_style)
            worksheet.write(row_counter, 24, transaction_refund_reason, font_style)
            worksheet.write(row_counter, 25, transaction_refunded_by, font_style)
            worksheet.write(row_counter, 26, transaction_status, font_style)

        tx_reports = StringIO.StringIO()
        workbook.save(tx_reports)

        res = make_response(tx_reports.getvalue())
        res.headers['Content-Type'] = 'application/vnd.ms-excel'
        res.headers['Content-Disposition'] = 'attachment; filename=MobileTransactionReports.xls'

        return res

    transactions_page, next_cursor, more = queryset.fetch_page(100, start_cursor=cursor)

    return render_template('transactions_reports.html', transactions=transactions_page, next_cursor=next_cursor,
            more=more, transactions_from=transactions_from, transactions_to=transactions_to, platform=platform,
            theater_id=theater_id, org_uuids=org_uuids, payment_type=payment_type, theaters=theaters,
            parse_float_to_string=admin.parse_float_to_string, orgs=orgs,
            payment_reference=payment_reference, reservation_reference=reservation_reference)

@web.route('/transactions/<transaction_id>/', methods=['GET', 'POST'])
def transaction_show(transaction_id):
    email = None
    transaction = admin.get_transaction(transaction_id)
    override = request.args.get('override')
    
    if transaction.payment_info and 'email' in transaction.payment_info:
        email = transaction.payment_info['email']

    if request.method == 'POST':
        (device_id, txn_id) = id_encoder.decode_id_paths(transaction_id)
        code = request.form['pin']
        status, result = update_state(device_id, txn_id, code)
        flash(result)
        return redirect(url_for('.transaction_show', transaction_id=transaction_id, email=email, override=override))

    return render_template('transaction_show.html', transaction=transaction, email=email, override=override)

@web.route('/transactions/<transaction_id>/reservations/<info_index>/')
def reservation_info_show(transaction_id, info_index):
    transaction = admin.get_transaction(transaction_id)
    reservation_info = admin.get_reservation_info(transaction, info_index)
    schedules = Schedule.query(ancestor=reservation_info.theater).order(Schedule.show_date)
    schedule_reservation = reservation_info.schedule.get()

    form = EditReservationForm(obj=reservation_info)
    form.theater.choices = [(id_encoder.encoded_theater_id(t.key), t.name) for t in Theater.query()]
    form.schedule.choices = [(s.key.id(), admin.format_schedule_reservation_info(s)) for s in schedules]
    form.time.choices = [(t, admin.format_time_to_string(t)) for t in sorted(schedule_reservation.start_times)]
    form.theater.data = id_encoder.encoded_theater_id(reservation_info.theater)
    form.schedule.data = reservation_info.schedule.id()
    form.cinema.data = reservation_info.cinema
    form.seat_id.data = reservation_info.seat_id
    form.reservation_index.data = info_index

    return render_template('reservation_info_show.html', form=form, transaction_id=transaction_id, info_index=info_index)

@web.route('/transactions/<transaction_id>/reservations/', methods=['POST'])
def reservation_update(transaction_id):
    transaction = admin.get_transaction(transaction_id)
    
    form = EditReservationForm()

    theater_key = admin.get_theater(form.theater.data).key
    schedule_key = ndb.Key(Schedule, form.schedule.data, parent=theater_key)
    reservation_info = admin.get_reservation_info(transaction, form.reservation_index.data)
    schedules = Schedule.query(ancestor=theater_key)
    schedule_reservation = schedule_key.get()

    form.theater.choices = [(id_encoder.encoded_theater_id(t.key), t.name) for t in Theater.query()]
    form.schedule.choices = [(s.key.id(), admin.format_schedule_reservation_info(s)) for s in schedules]
    form.time.choices = [(t, admin.format_time_to_string(t)) for t in sorted(schedule_reservation.start_times)]

    if form.validate_on_submit():
        reservation_info.schedule = schedule_key
        reservation_info.theater = theater_key
        reservation_info.time = form.time.data
        reservation_info.cinema = form.cinema.data
        reservation_info.seat_id = form.seat_id.data
        transaction.put()

        return redirect(url_for('.transaction_show', transaction_id=transaction_id))

    return render_template('reservation_info_show.html', form=form, transaction_id=transaction_id, info_index=form.reservation_index.data)

@web.route('/transactions/<transaction_id>/update')
def transaction_update(transaction_id):
    (device_id, txn_id) = id_encoder.decode_id_paths(transaction_id)
    update(device_id, txn_id)
    flash("Updated transaction for processing")
    transaction = admin.get_transaction(transaction_id)

    return render_template('transaction_show.html', transaction=transaction)

@web.route('/transactions/<transaction_id>/refund')
def transaction_refund(transaction_id):
    refund_reason = "Dashboard tagged for refund."
    refunded_by = None
    if 'refund_reason' in request.args:
        refund_reason = request.args.get('refund_reason')
    if 'refunded_by' in request.args:
        refunded_by = request.args.get('refunded_by')
    (device_id, txn_id) = id_encoder.decode_id_paths(transaction_id)
    source = 'gae-dashboard'
    if 'source' in request.args:
        source = request.args.get('source')
    status, result = refund(device_id, txn_id, refund_reason, refunded_by, source)
    # flash("Transaction tagged for refund")
    flash(result)
    transaction = admin.get_transaction(transaction_id)

    return render_template('transaction_show.html', transaction=transaction)

@web.route('/transactions/<transaction_id>/email')
def transaction_email(transaction_id):
    (device_id, txn_id) = id_encoder.decode_id_paths(transaction_id)
    transaction = admin.get_transaction(transaction_id)
    email = transaction.payment_info['email']

    # GMovies API will send the email
    email_status, email_return = trigger_send_email_blasts_txdone(transaction)
    log.debug("transaction_email, status_code: %s..." % (email_status))
    log.debug("transaction_email, response: %s..." % (email_return))
    if 1 != email_status:
        log.warn("ERROR, TX %s, failed on trigger_send_email_blasts_txdone, status: %s, message: %s..." % (transaction_id, email_status, email_return))

    # send_ticket_details(transaction.payment_info['email'], transaction.ticket)
    flash("Sent transaction details to %s" % transaction.payment_info['email'])

    return render_template('transaction_show.html', transaction=transaction)

###############
#
# Free Tickets
#
###############

@web.route('/free_tickets')
def free_tickets():
    date_today = datetime.datetime.today().date()
    cursor = ndb.Cursor(urlsafe=request.args.get('cursor'))
    transactions_from = request.args.get('transactions_from', None)
    transactions_to = request.args.get('transactions_to', None)
    theater_id = request.args.get('theater_id', None)
    platform = request.args.get('platform', None)
    discount_type = request.args.getlist('discount_type')
    org_uuids = request.args.getlist('org_uuids')

    if not transactions_from or transactions_from is None:
        transactions_from = str(date_today)

    if not transactions_to or transactions_to is None:
        transactions_to = str(date_today)

    is_valid = True
    theaters = []
    tx_from = admin.parse_string_to_date(transactions_from + ' 00:00:00', fdate='%Y-%m-%d %H:%M:%S')
    tx_to = admin.parse_string_to_date(transactions_to + ' 23:59:59', fdate='%Y-%m-%d %H:%M:%S')
    free_tickets = admin.get_date_range_string(transactions_from, transactions_to, dates={})
    queryset = admin.get_filtered_free_tickets(is_valid, tx_from=tx_from,
            tx_to=tx_to, discount_type=discount_type, org_uuids=org_uuids,
            theater_id=theater_id, platform=platform)

    for org_uuid in org_uuids:
        theaterorg_key = ndb.Key(TheaterOrganization, str(org_uuid))
        theaterorg = theaterorg_key.get()

        if theaterorg is not None:
            theater_infos = Theater.query(ancestor=theaterorg_key).fetch()
            theaters.extend(theater_infos)

    free_ticket_page, next_cursor, more = queryset.fetch_page(100, start_cursor=cursor)

    return render_template('free_tickets.html', free_tickets=free_ticket_page,
            next_cursor=next_cursor, more=more, transactions_from=transactions_from,
            transactions_to=transactions_to, platform=platform, theater_id=theater_id,
            org_uuids=org_uuids, discount_type=discount_type, theaters=theaters,
            parse_float_to_string=admin.parse_float_to_string)

###############
#
# Payment Gateways Settings & Payment Options Settings
#
###############

@web.route('/payment_options_settings/')
def paymentoptions_settings_index():
    payment_options = PaymentOptionSettings.query().fetch()

    return render_template('payment_options_index.html', payment_options=payment_options)

@web.route('/payment_options_settings/new/', methods=['GET', 'POST'])
def paymentoptions_settings_new():
    form = PaymentOptionSettingsForm()

    if request.method == 'POST' and form.validate_on_submit():
        if admin.check_payment_option_settings(form.payment_option_identifier.data):
            paymentoption_image_file = request.files[form.paymentoption_image_file.name]
            paymentoption_header_file = request.files[form.paymentoption_header_file.name]
            payment_option = PaymentOptionSettings()
            payment_option.key = ndb.Key(PaymentOptionSettings, form.payment_option_identifier.data)
            payment_option.name = form.name.data
            payment_option.type = form.type.data
            payment_option.payment_gateway = form.payment_gateway.data
            payment_option.default_params = form.default_params.data
            payment_option.image_url = form.image_url.data
            payment_option.header_url = form.header_url.data
            payment_option.instruction_url = form.instruction_url.data
            payment_option.is_required_discount_codes = form.is_required_discount_codes.data

            if paymentoption_image_file:
                assets_entity_logo = AssetsImageBlob()
                assets_entity_logo.key = ndb.Key(AssetsImageBlob, str(uuid_gen()), parent=payment_option.key)
                assets_entity_logo.name = payment_option.name
                assets_entity_logo.assets_group = 'payment-options-image'
                assets_entity_logo.is_active = True
                assets_entity_logo.image = get_image_blob_key(assets_entity_logo, paymentoption_image_file, folder='assets/payment-options-image/')
                assets_entity_logo.put()

                cache_key_logo = 'paymentoption::image::identifier::%s' % payment_option.key.id()
                image_url = get_serving_url(assets_entity_logo.image)
                payment_option.image_url = image_url

                memcache.set(cache_key_logo, image_url)

            if paymentoption_header_file:
                assets_entity_header = AssetsImageBlob()
                assets_entity_header.key = ndb.Key(AssetsImageBlob, str(uuid_gen()), parent=payment_option.key)
                assets_entity_header.name = payment_option.name
                assets_entity_header.assets_group = 'payment-options-image-header'
                assets_entity_header.is_active = True
                assets_entity_header.image = get_image_blob_key(assets_entity_header, paymentoption_header_file, folder='assets/payment-options-image/')
                assets_entity_header.put()

                cache_key_header = 'paymentoption::image::header::identifier::%s' % payment_option.key.id()
                header_url = get_serving_url(assets_entity_header.image)
                payment_option.header_url = header_url

                memcache.set(cache_key_header, header_url)

            payment_option.put()

            flash('Success! Created New Payment Option Settings!')

            return redirect(url_for('.paymentoptions_settings_show', payment_option_identifier=payment_option.key.id()))
        elif not admin.check_payment_option_settings(form.payment_option_identifier.data):
            form.errors['generic'] = 'Error! Payment Option Settings Already Exists!'

    return render_template('payment_options_new.html', form=form)

@web.route('/payment_options_settings/<payment_option_identifier>/', methods=['GET', 'POST'])
def paymentoptions_settings_show(payment_option_identifier):
    payment_option_key = ndb.Key(PaymentOptionSettings, payment_option_identifier)
    payment_option = payment_option_key.get()
    assets_entity_logo = AssetsImageBlob.query(AssetsImageBlob.assets_group=='payment-options-image', ancestor=payment_option.key).get()
    assets_entity_header = AssetsImageBlob.query(AssetsImageBlob.assets_group=='payment-options-image-header', ancestor=payment_option.key).get()
    form = PaymentOptionSettingsForm(obj=payment_option)
    form.payment_option_identifier.data = payment_option.key.id()

    theaters = []
    theaterorgs_uuids = [orgs.AYALA_MALLS, orgs.SM_MALLS, orgs.ROCKWELL_MALLS, orgs.GREENHILLS_MALLS,
        orgs.MEGAWORLD_MALLS, orgs.ROBINSONS_MALLS, orgs.CINEMA76_MALLS, orgs.GLOBE_EVENTS, orgs.CITY_MALLS]
    theaterorgs_keys = [ndb.Key(TheaterOrganization, str(theaterorg_id)) for theaterorg_id in theaterorgs_uuids]
    theaterorgs = ndb.get_multi(theaterorgs_keys)

    for theaterorg_key in theaterorgs_keys:
        theaterorg_theaters = models.Theater.query(ancestor=theaterorg_key).fetch()
        theaters.extend(theaterorg_theaters)

    if request.method == 'POST' and form.validate_on_submit():
        paymentoption_image_file = request.files[form.paymentoption_image_file.name]
        paymentoption_header_file = request.files[form.paymentoption_header_file.name]
        form.populate_obj(payment_option)

        if paymentoption_image_file:
            if not assets_entity_logo:
                assets_entity_logo = AssetsImageBlob()
                assets_entity_logo.key = ndb.Key(AssetsImageBlob, str(uuid_gen()), parent=payment_option.key)
                assets_entity_logo.name = payment_option.name
                assets_entity_logo.assets_group = 'payment-options-image'
                assets_entity_logo.is_active = True

            assets_entity_logo.image = get_image_blob_key(assets_entity_logo, paymentoption_image_file, folder='assets/payment-options-image/')
            assets_entity_logo.put()

            cache_key_logo = 'paymentoption::image::identifier::%s' % payment_option.key.id()
            image_url = get_serving_url(assets_entity_logo.image)
            payment_option.image_url = image_url

            memcache.set(cache_key_logo, image_url)

        if paymentoption_header_file:
            if not assets_entity_header:
                assets_entity_header = AssetsImageBlob()
                assets_entity_header.key = ndb.Key(AssetsImageBlob, str(uuid_gen()), parent=payment_option.key)
                assets_entity_header.name = payment_option.name
                assets_entity_header.assets_group = 'payment-options-image-header'
                assets_entity_header.is_active = True

            assets_entity_header.image = get_image_blob_key(assets_entity_header, paymentoption_header_file, folder='assets/payment-options-image/')
            assets_entity_header.put()

            cache_key_header = 'paymentoption::image::header::identifier::%s' % payment_option.key.id()
            header_url = get_serving_url(assets_entity_header.image)
            payment_option.header_url = header_url

            memcache.set(cache_key_header, header_url)

        payment_option.put()

        flash('Success! Updated Payment Option Settings!')

        return redirect(url_for('.paymentoptions_settings_show', payment_option_identifier=payment_option.key.id()))

    return render_template('payment_options_show.html', form=form, payment_option=payment_option, assets_entity_logo=assets_entity_logo,
            assets_entity_header=assets_entity_header, theaterorgs=theaterorgs, theaters=theaters, get_serving_url=get_serving_url)

@web.route('/payment_options_settings/<payment_option_identifier>/allow_discount_codes_whitelist/<theaterorg_id>/', methods=['GET', 'POST'])
def paymentoptions_settings_discountcodes_whitelist(payment_option_identifier, theaterorg_id):
    allow_discount_codes_whitelist = {}
    payment_option_key = ndb.Key(models.PaymentOptionSettings, payment_option_identifier)
    payment_option = payment_option_key.get()
    theaterorg_key = ndb.Key(models.TheaterOrganization, theaterorg_id)
    theaterorg = theaterorg_key.get()
    theaters = models.Theater.query(ancestor=theaterorg_key).fetch()
    form = AllowDiscountCodesWhitelistForm()
    form.theaters_whitelist.choices = [(id_encoder.encoded_theater_id(theater.key), theater.name) for theater in theaters]

    if payment_option.allow_discount_codes_whitelist and type(payment_option.allow_discount_codes_whitelist) == dict:
        allow_discount_codes_whitelist = payment_option.allow_discount_codes_whitelist

    if request.method == 'POST' and form.validate_on_submit():
        allow_discount_codes_whitelist[theaterorg.key.id()] = form.theaters_whitelist.data
        payment_option.allow_discount_codes_whitelist = allow_discount_codes_whitelist
        payment_option.put()

        flash('Success! Added Theaters in allow discount codes whitelist!')

        return redirect(url_for('.paymentoptions_settings_show', payment_option_identifier=payment_option.key.id()))

    if theaterorg.key.id() in allow_discount_codes_whitelist:
        form.theaters_whitelist.data = allow_discount_codes_whitelist[theaterorg.key.id()]

    return render_template('payment_options_discountcodes_whitelist.html', form=form, payment_option=payment_option, theaterorg=theaterorg)

def get_paymentsettings_details(payment_settings):
    parent_key = payment_settings.key.parent()
    parent = parent_key.get()

    payment_settings_dict = {}
    payment_settings_dict['payment_setting_parent_id'] = payment_settings.key.parent().id()
    payment_settings_dict['parent'] = parent.name
    payment_settings_dict['payment_setting_id'] = payment_settings.key.id()
    payment_settings_dict['payment_setting_urlstring'] = payment_settings.key.urlsafe()
    payment_settings_dict['payment_identifier'] = payment_settings.payment_identifier

    return payment_settings_dict

def get_paymentsettings_theaterorgs_and_theaters(payment_identifier):
    payment_settings = ClientInitiatedPaymentSettings.query(ClientInitiatedPaymentSettings.payment_identifier==payment_identifier).fetch(keys_only=True)

    return payment_settings

def get_published_and_allowedreservation_theaterorgs():
    allowed_reservations = [str(orgs.AYALA_MALLS), str(orgs.ROCKWELL_MALLS), str(orgs.GREENHILLS_MALLS),
            str(orgs.SM_MALLS), str(orgs.ROBINSONS_MALLS), str(orgs.MEGAWORLD_MALLS)]
    theaterorgs = TheaterOrganization.query(TheaterOrganization.is_published==True).fetch()
    filtered_theaterorgs = filter(lambda x: x.key.id() in allowed_reservations, theaterorgs)

    return map(lambda x: {'key': x.key.urlsafe(), 'name': '%s (Theater Organization)' % x.name}, filtered_theaterorgs)

def get_published_and_allowedreservation_theaters():
    theaters = Theater.query(Theater.is_published==True, Theater.allow_reservation==True).fetch()

    return map(lambda x: {'key': x.key.urlsafe(), 'name': '%s (Theater)' % x.name}, theaters)

@web.route('/payment_settings/<payment_identifier>/')
def payment_settings_index(payment_identifier):
    payment_settings_raw = ClientInitiatedPaymentSettings.query(ClientInitiatedPaymentSettings.payment_identifier==payment_identifier).fetch()
    payment_settings = map(get_paymentsettings_details, payment_settings_raw)

    return render_template('payment_settings_index.html', payment_settings=payment_settings, payment_identifier=payment_identifier)

@web.route('/payment_settings/<payment_identifier>/new', methods=['GET', 'POST'])
def payment_settings_new(payment_identifier):
    if payment_identifier == 'paynamics':
        form = PaynamicsPaymentSettingsForm()
    elif payment_identifier == 'migs':
        form = MIGSPaymentSettingsForm()
    elif payment_identifier == 'ipay88':
        form = IPay88PaymentSettingsForm()
    else:
        form = PaymentSettingsForm()

    payment_settings = get_paymentsettings_theaterorgs_and_theaters(payment_identifier)
    parents_urlstrings = map(lambda s: s.parent().urlsafe(), payment_settings)
    theaterorgs_and_theaters = get_published_and_allowedreservation_theaterorgs() + get_published_and_allowedreservation_theaters()
    unassigned_theaterorgs_and_theaters = filter(lambda t: t['key'] not in parents_urlstrings, theaterorgs_and_theaters)
    form.theater.choices = [(t['key'], t['name']) for t in unassigned_theaterorgs_and_theaters]

    if request.method == 'POST' and form.validate_on_submit():
        parent_string = form.theater.data
        parent_key = ndb.Key(urlsafe=parent_string)
        parent_name = parent_key.get().name
        new_payment_setting = ClientInitiatedPaymentSettings(id=payment_identifier, parent=parent_key)

        if payment_identifier == 'paynamics':
            default_params = {'ip_address': form.ip_address.data,
                    'notification_url': form.notification_url.data,
                    'response_url': form.response_url.data,
                    'cancel_url': form.cancel_url.data,
                    'mtac_url': form.mtac_url.data,
                    'currency': form.currency.data,
                    'pmethod': form.pmethod.data, 'trxtype': form.trxtype.data,
                    'mlogo_url': form.mlogo_url.data,
                    'secure3d': form.secure3d.data,
                    'descriptor_note': form.descriptor_note.data}
            settings = {'paynamics::merchant-id': form.merchant_id.data,
                    'paynamics::merchant-name': form.merchant_name.data,
                    'paynamics::merchant-key': form.merchant_key.data,
                    'paynamics::moto-merchant-id': form.moto_merchant_id.data,
                    'paynamics::moto-merchant-name': form.moto_merchant_name.data,
                    'paynamics::moto-merchant-key': form.moto_merchant_key.data,
                    'paynamics::3d-token-merchant-id': form.tokenized_3d_merchant_id.data,
                    'paynamics::3d-token-merchant-name': form.tokenized_3d_merchant_name.data,
                    'paynamics::3d-token-merchant-key': form.tokenized_3d_merchant_key.data,
                    'default-form-target': form.default_form_target.data}
        elif payment_identifier == 'migs':
            default_params = {'vpc_Version': form.vpc_version.data,
                    'vpc_Command': form.vpc_command.data,
                    'vpc_Locale': form.vpc_locale.data,
                    'vpc_Currency': form.vpc_currency.data,
                    'vpc_ReturnURL': form.vpc_return_url.data}
            settings = {'migs::merchant-id': form.merchant_id.data,
                    'migs::access-code': form.access_code.data,
                    'migs::merchant-hash-secret': form.hash_secret.data,
                    'migs::username': form.username.data,
                    'migs::password': form.password.data,
                    'migs::title': form.title.data,
                    'default-form-target': form.default_form_target.data,
                    'requery-url': form.requery_url.data}
        elif payment_identifier == 'ipay88':
            default_params = {'PaymentId': form.payment_id.data,
                    'Currency': form.currency.data,
                    'ProdDesc': form.prod_desc.data,
                    'Remark': form.remark.data,
                    'Lang': form.lang.data,
                    'BackendURL': form.backend_url.data}
            settings = {'ipay88::merchant-code': form.merchant_code.data,
                    'ipay88::merchant-key': form.merchant_key.data,
                    'default-form-target': form.default_form_target.data}
        else:
            default_params = {'currCode': form.currency_code.data, 'payType': form.pay_type.data}
            settings = {'pesopay::merchant-id': form.merchand_id.data,
                    'pesopay::merchant-hash-secret': form.hash_secret.data,
                    'default-form-target': form.default_form_target.data}

        new_payment_setting.payment_identifier = payment_identifier
        new_payment_setting.default_params  = default_params
        new_payment_setting.settings = settings
        new_payment_setting.put()

        flash("Payment settings for %s has been created." % parent_name)

        return redirect(url_for('.payment_settings_update_and_show', payment_identifier=payment_identifier,
                payment_setting_id=new_payment_setting.key.urlsafe()))

    return render_template('payment_settings_new.html', form=form, payment_identifier=payment_identifier)

@web.route('/payment_settings/<payment_identifier>/<payment_setting_id>/', methods=['GET', 'POST'])
def payment_settings_update_and_show(payment_identifier, payment_setting_id):
    payment_setting_key = ndb.Key(urlsafe=payment_setting_id)
    payment_setting = payment_setting_key.get()
    parent_name = payment_setting.key.parent().get().name
    parent_url_string = payment_setting.key.parent().urlsafe()

    if not payment_setting:
        log.warn("ERROR!, payment_settings_update_and_show, abort, payment_settings not found...")

        abort(404)

    if payment_identifier == 'paynamics':
        form = PaynamicsPaymentSettingsForm()
    elif payment_identifier == 'migs':
        form = MIGSPaymentSettingsForm()
    elif payment_identifier == 'ipay88':
        form = IPay88PaymentSettingsForm()
    else:
        form = PaymentSettingsForm()

    form.theater.choices = []

    if request.method == 'POST' and form.validate_on_submit(): 
        validity = form.validate_on_submit()

        if payment_identifier == 'paynamics':
            payment_setting.default_params['ip_address'] = form.ip_address.data
            payment_setting.default_params['notification_url'] = form.notification_url.data
            payment_setting.default_params['response_url'] = form.response_url.data
            payment_setting.default_params['cancel_url'] = form.cancel_url.data
            payment_setting.default_params['mtac_url'] = form.mtac_url.data
            payment_setting.default_params['currency'] = form.currency.data
            payment_setting.default_params['pmethod'] = form.pmethod.data
            payment_setting.default_params['trxtype'] = form.trxtype.data
            payment_setting.default_params['mlogo_url'] = form.mlogo_url.data
            payment_setting.default_params['secure3d'] = form.secure3d.data
            payment_setting.default_params['descriptor_note'] = form.descriptor_note.data
            payment_setting.settings['paynamics::merchant-id'] = form.merchant_id.data
            payment_setting.settings['paynamics::merchant-name'] = form.merchant_name.data
            payment_setting.settings['paynamics::merchant-key'] = form.merchant_key.data
            payment_setting.settings['paynamics::moto-merchant-id'] = form.moto_merchant_id.data
            payment_setting.settings['paynamics::moto-merchant-name'] = form.moto_merchant_name.data
            payment_setting.settings['paynamics::moto-merchant-key'] = form.moto_merchant_key.data
            payment_setting.settings['paynamics::3d-token-merchant-id'] = form.tokenized_3d_merchant_id.data
            payment_setting.settings['paynamics::3d-token-merchant-name'] = form.tokenized_3d_merchant_name.data
            payment_setting.settings['paynamics::3d-token-merchant-key'] = form.tokenized_3d_merchant_key.data
            payment_setting.settings['default-form-target'] = form.default_form_target.data
        elif payment_identifier == 'migs':
            payment_setting.default_params['vpc_Version'] = form.vpc_version.data
            payment_setting.default_params['vpc_Command'] = form.vpc_command.data
            payment_setting.default_params['vpc_Locale'] = form.vpc_locale.data
            payment_setting.default_params['vpc_Currency'] = form.vpc_currency.data
            payment_setting.default_params['vpc_ReturnURL'] = form.vpc_return_url.data
            payment_setting.settings['migs::merchant-id'] = form.merchant_id.data
            payment_setting.settings['migs::access-code'] = form.access_code.data
            payment_setting.settings['migs::merchant-hash-secret'] = form.hash_secret.data
            payment_setting.settings['migs::username'] = form.username.data
            payment_setting.settings['migs::password'] = form.password.data
            payment_setting.settings['migs::title'] = form.title.data
            payment_setting.settings['default-form-target'] = form.default_form_target.data
            payment_setting.settings['requery-url'] = form.requery_url.data
        elif payment_identifier == 'ipay88':
            payment_setting.default_params['PaymentId'] = form.payment_id.data
            payment_setting.default_params['Currency'] = form.currency.data
            payment_setting.default_params['ProdDesc'] = form.prod_desc.data
            payment_setting.default_params['Remark'] = form.remark.data
            payment_setting.default_params['Lang'] = form.lang.data
            payment_setting.default_params['BackendURL'] = form.backend_url.data
            payment_setting.settings['ipay88::merchant-code'] = form.merchant_code.data
            payment_setting.settings['ipay88::merchant-key'] = form.merchant_key.data
            payment_setting.settings['default-form-target'] = form.default_form_target.data
        else:
            payment_setting.default_params['currCode'] = form.currency_code.data
            payment_setting.default_params['payType']  = form.pay_type.data
            payment_setting.settings['pesopay::merchant-id'] = form.merchand_id.data
            payment_setting.settings['pesopay::merchant-hash-secret'] = form.hash_secret.data
            payment_setting.settings['default-form-target'] = form.default_form_target.data

        payment_setting.payment_identifier = payment_identifier
        payment_setting.put()

        flash("Payment settings for %s has been updated." % parent_name)

        return redirect(url_for('.payment_settings_update_and_show', payment_identifier=payment_identifier,
                payment_setting_id=payment_setting.key.urlsafe()))

    if payment_identifier == 'paynamics':
        form.ip_address.data = payment_setting.default_params.get('ip_address', None)
        form.notification_url.data = payment_setting.default_params.get('notification_url', None)
        form.response_url.data = payment_setting.default_params.get('response_url', None)
        form.cancel_url.data = payment_setting.default_params.get('cancel_url', None)
        form.mtac_url.data = payment_setting.default_params.get('mtac_url', None)
        form.currency.data = payment_setting.default_params.get('currency', None)
        form.pmethod.data = payment_setting.default_params.get('pmethod', None)
        form.trxtype.data = payment_setting.default_params.get('trxtype', None)
        form.mlogo_url.data = payment_setting.default_params.get('mlogo_url', None)
        form.secure3d.data = payment_setting.default_params.get('secure3d', None)
        form.descriptor_note.data = payment_setting.default_params.get('descriptor_note', None)
        form.merchant_id.data = payment_setting.settings.get('paynamics::merchant-id', None)
        form.merchant_name.data = payment_setting.settings.get('paynamics::merchant-name', None)
        form.merchant_key.data = payment_setting.settings.get('paynamics::merchant-key', None)
        form.moto_merchant_id.data = payment_setting.settings.get('paynamics::moto-merchant-id', None)
        form.moto_merchant_name.data = payment_setting.settings.get('paynamics::moto-merchant-name', None)
        form.moto_merchant_key.data = payment_setting.settings.get('paynamics::moto-merchant-key', None)
        form.tokenized_3d_merchant_id.data = payment_setting.settings.get('paynamics::3d-token-merchant-id', None)
        form.tokenized_3d_merchant_name.data = payment_setting.settings.get('paynamics::3d-token-merchant-name', None)
        form.tokenized_3d_merchant_key.data = payment_setting.settings.get('paynamics::3d-token-merchant-key', None)
        form.default_form_target.data = payment_setting.settings.get('default-form-target', None)
    elif payment_identifier == 'migs':
        form.vpc_version.data = payment_setting.default_params.get('vpc_Version', None)
        form.vpc_command.data = payment_setting.default_params.get('vpc_Command', None)
        form.vpc_locale.data = payment_setting.default_params.get('vpc_Locale', None)
        form.vpc_currency.data = payment_setting.default_params.get('vpc_Currency', None)
        form.vpc_return_url.data = payment_setting.default_params.get('vpc_ReturnURL', None)
        form.merchant_id.data = payment_setting.settings.get('migs::merchant-id', None)
        form.access_code.data = payment_setting.settings.get('migs::access-code', None)
        form.hash_secret.data = payment_setting.settings.get('migs::merchant-hash-secret', None)
        form.username.data = payment_setting.settings.get('migs::username', None)
        form.password.data = payment_setting.settings.get('migs::password', None)
        form.title.data = payment_setting.settings.get('migs::title', None)
        form.default_form_target.data = payment_setting.settings.get('default-form-target', None)
        form.requery_url.data = payment_setting.settings.get('requery-url', None)
    elif payment_identifier == 'ipay88':
        form.payment_id.data = payment_setting.default_params.get('PaymentId', None)
        form.currency.data = payment_setting.default_params.get('Currency', None)
        form.prod_desc.data = payment_setting.default_params.get('ProdDesc', None)
        form.remark.data = payment_setting.default_params.get('Remark', None)
        form.lang.data = payment_setting.default_params.get('Lang', None)
        form.backend_url.data = payment_setting.default_params.get('BackendURL', None)
        form.merchant_code.data = payment_setting.settings.get('ipay88::merchant-code', None)
        form.merchant_key.data = payment_setting.settings.get('ipay88::merchant-key', None)
        form.default_form_target.data = payment_setting.settings.get('default-form-target', None)
    else:
        form.payment_identifier.data = parent_url_string
        form.payment_identifier.data = payment_setting.payment_identifier
        form.currency_code.data = payment_setting.default_params.get('currCode', None)
        form.pay_type.data = payment_setting.default_params.get('payType', None)
        form.merchand_id.data = payment_setting.settings.get('pesopay::merchant-id', None)
        form.hash_secret.data = payment_setting.settings.get('pesopay::merchant-hash-secret', None)
        form.default_form_target.data = payment_setting.settings.get('default-form-target', None)

    return render_template('payment_settings_show.html', payment_setting=payment_setting, form=form, payment_identifier=payment_identifier)

@web.route('/payment_settings/<payment_identifier>/<payment_setting_id>/cinema/new/', methods=['GET', 'POST'])
def payment_settings_cinema_new(payment_identifier, payment_setting_id):
    if payment_identifier not in ['pesopay']:
        log.warn("ERROR!, payment_settings_cinema_new, abort, payment_identifier not supported...")

        abort(404)

    form = PesoPayPaymentSettingsCinemaForm()
    payment_settings_key = ndb.Key(urlsafe=payment_setting_id)
    payment_setting = payment_settings_key.get()

    if not payment_setting:
        log.warn("ERROR!, payment_settings_cinema_new, abort, payment_setting not found...")

        abort(404)

    if payment_setting.check_cinema_settings(form.cinema.data):
        if request.method == 'POST' and form.validate_on_submit() and payment_identifier == 'pesopay':
            cinema_payment_settings = ClientInitiatedPaymentSettingsCinema()
            cinema_payment_settings.cinema = form.cinema.data
            cinema_payment_settings.payment_identifier = payment_setting.payment_identifier
            cinema_payment_settings.default_params  = {
                    'currCode': form.currency_code.data,
                    'payType': form.pay_type.data
            }
            cinema_payment_settings.settings = {
                    'pesopay::merchant-id': form.merchand_id.data,
                    'pesopay::merchant-hash-secret': form.hash_secret.data,
                    'default-form-target': form.default_form_target.data
            }

            payment_setting.cinema_settings.append(cinema_payment_settings)
            payment_setting.put()

            flash("Success! Created New PesoPay Payment Settings for specific Cinema!")

            return redirect(url_for('.payment_settings_update_and_show', payment_identifier=payment_identifier,
                    payment_setting_id=payment_setting.key.urlsafe()))
    elif not payment_setting.check_cinema_settings(form.cinema.data):
        form.errors['generic'] = 'Error! Cinema Payment Settings Already Exists!'

    return render_template('payment_settings_cinema_new.html', form=form, payment_setting=payment_setting,
            payment_identifier=payment_identifier)

@web.route('/payment_settings/<payment_identifier>/<payment_setting_id>/cinema/<cinema_name>/', methods=['GET', 'POST'])
def payment_settings_cinema_update_and_show(payment_identifier, payment_setting_id, cinema_name):
    if payment_identifier not in ['pesopay']:
        log.warn("ERROR!, payment_settings_cinema_update_and_show, abort, payment_identifier not supported...")

        abort(404)

    payment_settings_key = ndb.Key(urlsafe=payment_setting_id)
    payment_setting = payment_settings_key.get()

    if not payment_setting:
        log.warn("ERROR!, payment_settings_cinema_update_and_show, abort, payment_setting not found...")

        abort(404)

    cinema_payment_settings = payment_setting.get_cinema_settings(cinema_name)

    if not cinema_payment_settings:
        log.warn("ERROR!, payment_settings_cinema_update_and_show, abort, cinema_payment_settings not found...")

        abort(404)

    form = PesoPayPaymentSettingsCinemaForm()

    if payment_setting.check_cinema_settings(form.cinema.data, cinema_name):
        if request.method == 'POST' and form.validate_on_submit() and payment_identifier == 'pesopay':
            cinema_payment_settings.cinema = form.cinema.data
            cinema_payment_settings.payment_identifier = payment_setting.payment_identifier
            cinema_payment_settings.default_params['currCode'] = form.currency_code.data
            cinema_payment_settings.default_params['payType']  = form.pay_type.data
            cinema_payment_settings.settings['pesopay::merchant-id'] = form.merchand_id.data
            cinema_payment_settings.settings['pesopay::merchant-hash-secret'] = form.hash_secret.data
            cinema_payment_settings.settings['default-form-target'] = form.default_form_target.data
            payment_setting.put()

            flash("Success!, Updated PesoPay Payment Settings for specific Cinema!")

            return redirect(url_for('.payment_settings_update_and_show', payment_identifier=payment_identifier,
                    payment_setting_id=payment_setting.key.urlsafe()))
    elif not payment_setting.check_cinema_settings(form.cinema.data, cinema_name):
        form.errors['generic'] = 'Error! Cinema Payment Settings Already Exists!'

    form.cinema.data = cinema_payment_settings.cinema
    form.currency_code.data = cinema_payment_settings.default_params.get('currCode', None)
    form.pay_type.data = cinema_payment_settings.default_params.get('payType', None)
    form.merchand_id.data = cinema_payment_settings.settings.get('pesopay::merchant-id', None)
    form.hash_secret.data = cinema_payment_settings.settings.get('pesopay::merchant-hash-secret', None)
    form.default_form_target.data = cinema_payment_settings.settings.get('default-form-target', None)

    return render_template('payment_settings_cinema_show.html', form=form, payment_setting=payment_setting,
            payment_identifier=payment_identifier, cinema_name=cinema_name)

@web.route('/payment_settings/<payment_identifier>/<payment_setting_id>/cinema/<cinema_name>/delete/', methods=['POST'])
def payment_settings_cinema_delete(payment_identifier, payment_setting_id, cinema_name):
    if payment_identifier not in ['pesopay']:
        log.warn("ERROR!, payment_settings_cinema_update_and_show, abort, payment_identifier not supported...")

        abort(404)

    payment_settings_key = ndb.Key(urlsafe=payment_setting_id)
    payment_setting = payment_settings_key.get()

    if not payment_setting:
        log.warn("ERROR!, payment_settings_cinema_update_and_show, abort, payment_setting not found...")

        abort(404)

    cinema_payment_settings = payment_setting.get_cinema_settings(cinema_name)

    if not cinema_payment_settings:
        log.warn("ERROR!, payment_settings_cinema_update_and_show, abort, cinema_payment_settings not found...")

        abort(404)

    payment_setting.cinema_settings.remove(cinema_payment_settings)
    payment_setting.put()

    flash("Success!, Deleted PesoPay Payment Settings for specific Cinema!")

    return redirect(url_for('.payment_settings_update_and_show', payment_identifier=payment_identifier,
            payment_setting_id=payment_setting.key.urlsafe()))

@web.route('/clients/')
def clients_index():
    clients = Client.query().fetch()

    return render_template('clients_index.html', clients=clients)

@web.route('/new-client')
def client_new():
    form = ClientForm()

    return render_template('clients_new.html', form=form)

@web.route('/clients/', methods=['POST'])
def client_create():
    form = ClientForm()

    if form.validate_on_submit():
        if not Client.get_by_id(form.id.data): # Check that ID is not yet taken
            client = Client()
            form.populate_obj(client)
            client.key = ndb.Key(Client, form.id.data)
            client.put()

            flash("Registered client ID: %s" % client.key.id())

            return redirect(url_for('.client_update_and_show', client_id=client.key.id()))
        else:
            form.errors['generic'] = "A client with ID '%s' has already been registered" % form.id.data

    return render_template('clients_new.html', form=form)

@web.route('/clients/<client_id>', methods=['GET', 'POST'])
def client_update_and_show(client_id):
    client = Client.get_by_id(client_id)

    if not client:
        abort(404)

    form = ClientForm(obj=client)

    if request.method == 'POST' and form.validate_on_submit():
        form.populate_obj(client)
        client.put()

        return redirect(url_for('.client_update_and_show', client_id=client_id))

    return render_template('clients_show.html', client=client, form=form)

@web.route('/verify-unregister-client/<client_id>', methods=['POST'])
def client_unregister_verify(client_id):
    client = Client.get_by_id(client_id)

    if not client:
        abort(404)

    return render_template('clients_unregister_verify.html', client=client)

@web.route('/clients/<client_id>', methods=['DELETE'])
def client_unregister(client_id):
    client = Client.get_by_id(client_id)

    if not client:
        abort(404)

    flash("Client with ID '%s' unregistered" % client.key.id())
    client.key.delete()

    return redirect(url_for('.clients_index'))

@web.route('/settings', methods=['GET', 'POST'])
def system_knobs():
    settings = SystemSettings.get_settings()
    form = SystemSettingsForm(obj=settings)

    if request.method == 'POST' and form.validate_on_submit():
        form.populate_obj(settings)
        settings.put()

        return redirect(url_for('.system_knobs'))

    return render_template('knobs.html', settings=settings, form=form)

@web.route('/csrc_settings', methods=['GET', 'POST'])
def csrc_settings():
    DATETIME_FORMAT = "%a, %d %b %Y %H:%M"
    settings = AccountSettings.get_settings()
    log.info(settings)

    last_updated  = datetime.datetime.strftime(settings.last_updated, DATETIME_FORMAT)
    form = CSRCSettingForm(obj=settings)

    if request.method == 'POST' and form.validate_on_submit():
        form.populate_obj(settings)
        settings.last_updated = datetime.datetime.now()
        settings.put()

        return redirect(url_for('.csrc_settings'))

    return render_template('csrc_account_settings.html', settings=settings, last_updated=last_updated, form=form)

@web.route('/analytics-services/')
def services_index():
    services = AnalyticsService.query().fetch()
    log.info(services)

    return render_template('services_status_index.html', services=services)

@web.route('/analytics-services/', methods=['POST'])
def service_create():
    form = AnalyticsServiceForm()

    if form.validate_on_submit():
        msg, service = admin.create_service(form)

        if msg == 'error':
            flash(service)
            return redirect(url_for('.service_new'))

        flash('New service has been created')

        return redirect(url_for('.services_index'))
    else:
        return redirect(url_for('.service_new'))

@web.route('/analytics-services/new/')
def service_new():
    form = AnalyticsServiceForm()

    return render_template('service_new.html',form=form)

@web.route('/analytics-services/<service_id>/', methods=['GET'])
def service_show(service_id):
    service = AnalyticsService.get_by_id(int(service_id))
    form = AnalyticsServiceForm(obj=service)
    history = AnalyticsServiceHistory.get_history(service)

    return render_template('service_show.html', form=form, service=service, service_id=service_id, history=history)

@web.route('/analytics-services/<service_id>/', methods=['POST'])
def service_update(service_id):
    service = AnalyticsService.get_by_id(int(service_id))
    form = AnalyticsServiceForm(obj=service)

    if form.validate_on_submit():
        admin.log_history(service, form.active.data, form.platform.data)
        form.populate_obj(service)
        service.put()

        return redirect(url_for('.service_show', service_id=service_id))

    return render_template('service_show.html',form=form, service=service, service_id=service_id)


@web.route('/movies/<movie_id>/feed_correlations/<feed_name>~<corr_id>~<variant>/delete', methods=['POST'])
def feed_correlation_delete(movie_id, feed_name, corr_id, variant):
    feed_ids = {'sureseats': str(orgs.AYALA_MALLS), 'rottentomatoes': str(orgs.ROTTEN_TOMATOES),
            'globe': str(orgs.GLOBE), 'rockwell': str(orgs.ROCKWELL_MALLS),
            'greenhills': str(orgs.GREENHILLS_MALLS), 'sm-malls': str(orgs.SM_MALLS),
            'megaworld': str(orgs.MEGAWORLD_MALLS), 'robinsons': str(orgs.ROBINSONS_MALLS),
            'globeevents': str(orgs.GLOBE_EVENTS), 'citymall': str(orgs.CITY_MALLS)}
    movie_key = ndb.Key(Movie, movie_id)
    movie = movie_key.get()
    feed_key = ndb.Key(Feed, feed_ids[feed_name])
    feed = feed_key.get()
    feed_correlations = movie.movie_correlation

    if variant == 'None':
        variant = None

    if variant and variant is not None:
        variant = variant.replace('_', '/')

    corr = FeedCorrelation(movie_id=corr_id, org_key=feed_key, variant=variant)

    if request.method == 'POST':
        movie.movie_correlation.remove(corr)
        movie.put()

        flash('Deleted Feed Correlation {0}: {1}'.format(feed.name, corr_id))

    return redirect(url_for('.movie_update_and_show', movie_id=movie_id))


##########
#
# Dashboard for Movie Title Bank
#
##########

@web.route('/movie_titles/')
def movie_title_index():
    movie_title = ''
    is_active = True
    cursor = ndb.Cursor(urlsafe=request.args.get('cursor'))

    if 'movie_title' in request.args:
        movie_title = request.args.get('movie_title')

    if 'active' in request.args:
        if request.args.get('active').lower() == 'false':
            is_active = False

    searchable_words = movies.get_searchable_words(movie_title)

    if len(searchable_words) > 0:
        queryset = MovieTitles.query(MovieTitles.searchable_words.IN(searchable_words),
                MovieTitles.is_active == is_active).order(MovieTitles.title, MovieTitles.key)
    else:
        queryset = MovieTitles.query(MovieTitles.is_active == is_active).order(MovieTitles.title)

    movie_titles, next_cursor, more = queryset.fetch_page(100, start_cursor=cursor)

    return render_template('movie_title_index.html', movie_titles=movie_titles,
            next_cursor=next_cursor, more=more, movie_title=movie_title, is_active=is_active)

@web.route('/movie_titles/<movie_title_id>/', methods=['GET', 'POST'])
def movie_title_update_and_show(movie_title_id):
    movie_title_key = ndb.Key(MovieTitles, movie_title_id)
    movie_title = movie_title_key.get()
    form = MovieTitlesForm(obj=movie_title)
    old_correlation_titles = movie_title.correlation_title
    new_correlation_titles = movies.get_all_correlation_titles(form.title.data)

    if (request.method == 'POST' and form.validate_on_submit() and
            admin.check_movie_title(old_correlation_titles, new_correlation_titles, movie_title_key)):
        form.populate_obj(movie_title)

        movie_title.correlation_title = movies.get_all_correlation_titles(form.title.data)
        movie_title.searchable_words = movies.get_searchable_words(form.title.data)
        movie_title.searchable_title = movies.get_searchable_title(form.title.data)

        if form.rt_id.data:
            rottentomatoes_info = RT.info(str(form.rt_id.data))

            if rottentomatoes_info and 'error' not in rottentomatoes_info:
                rt_details = rottentomatoes.get_rt_details(rottentomatoes_info) if 'id' in rottentomatoes_info else None

                if rt_details:
                    movie_title.rt_details = rt_details

        movie_title.put()

        flash("Saved")

        return redirect(url_for('.movie_title_update_and_show', movie_title_id=movie_title_key.id()))
    else:
        if admin.check_movie_title(old_correlation_titles, new_correlation_titles, movie_title_key) == False:
            form.errors['generic'] = 'The movie title already exists in title bank.'

    return render_template('movie_title_show.html', form=form, movie_title=movie_title)

@web.route('/movie_titles/new/', methods=['GET', 'POST'])
def movie_title_new():
    is_lookup = False

    if 'is_lookup' in request.args:
        if request.args.get('is_lookup').lower() == 'true':
            is_lookup = True

    if is_lookup:
        movie_title_lookup = MovieTitlesLookup.query().fetch()
        form = MovieTitlesLookupForm()
        movie_title_lookup_choices = [(str(lookup.key.id()), lookup.movie_title) for lookup in movie_title_lookup]
        form.movie_title_lookup.choices = movie_title_lookup_choices

        if request.method == 'POST' and form.validate_on_submit():
            try:
                flash('success')
                movie = admin.create_movie_title(form, is_lookup=is_lookup)
                movie_title_id = movie.key.id()
                return redirect(url_for('.movie_title_update_and_show', movie_title_id=movie_title_id))                
            except Exception, e:
                log.error("ERROR!, Failed to add movie title from MovieTitlesLookup...")
                log.error(e)

            return redirect(url_for('.movie_title_index'))
    else:
        form = MovieTitlesForm()

        if request.method == 'POST' and form.validate_on_submit():
            flash('success')
            movie = admin.create_movie_title(form, is_lookup=is_lookup)
            movie_title_id = movie.key.id()
            return redirect(url_for('.movie_title_update_and_show', movie_title_id=movie_title_id))                

    return render_template('movie_title_new.html', form=form, is_lookup=is_lookup)

@web.route('/movie_titles/<movie_title_id>/rt_details/delete', methods=['POST'])
def rt_details_delete(movie_title_id):
    movie_title_key = ndb.Key(MovieTitles, movie_title_id)
    movie_title = movie_title_key.get()

    if request.method == 'POST':
        movie_title.rt_details = None
        movie_title.put()

        flash('Deleted Rotten Tomatoes Details: %s' % movie_title.title)

    return redirect(url_for('.movie_title_update_and_show', movie_title_id=movie_title_id))

@web.route('/movie_titles/<movie_title_id>/secondary_titles/new/', methods=['GET', 'POST'])
def secondary_title_new(movie_title_id):
    is_lookup = False
    movie_title_key = ndb.Key(MovieTitles, movie_title_id)
    movie_title = movie_title_key.get()
    secondary_title = SecondaryMovieTitles()

    if 'is_lookup' in request.args:
        if request.args.get('is_lookup').lower() == 'true':
            is_lookup = True

    if is_lookup:
        secondary_titles_lookup = MovieTitlesLookup.query().fetch()
        form = SecondaryMovieTitlesLookupForm()
        secondary_title_lookup_choices = [(str(lookup.key.id()), lookup.movie_title) for lookup in secondary_titles_lookup]
        form.secondary_title_lookup.choices = secondary_title_lookup_choices

        if request.method == 'POST' and form.validate_on_submit():
            try:
                lookup_key = ndb.Key(MovieTitlesLookup, int(form.secondary_title_lookup.data))
                lookup = lookup_key.get()

                if movie_title and lookup:
                    movie_title = add_secondary_title_from_lookup(movie_title, lookup, secondary_title)
                    movie_title.put()

                    log.info("Added secondary title from MovieTitlesLookup...")

                    lookup_key.delete()

                    log.info("Deleted secondary title in MovieTitlesLookup...")
            except Exception, e:
                log.error("ERROR!, Failed to add secondary title from MovieTitlesLookup...")
                log.error(e)

            return redirect(url_for('.movie_title_update_and_show',
                    movie_title_id=movie_title_id))
    else:
        form = SecondaryMovieTitlesForm()

        if request.method == 'POST' and form.validate_on_submit():
            correlation_titles = movies.get_all_correlation_titles(form.movie_title.data)
            secondary_title.movie_title = form.movie_title.data
            secondary_title.correlation_title_version_one = correlation_titles[0]
            secondary_title.correlation_title_version_two = correlation_titles[1]
            secondary_title.correlation_title_version_three = correlation_titles[2]
            secondary_title.correlation_title_version_four = correlation_titles[3]
            movie_title.secondary_titles.append(secondary_title)
            movie_title.put()

            flash('Success')

            return redirect(url_for('.movie_title_update_and_show',
                    movie_title_id=movie_title_id))

    return render_template('secondary_movie_title_edit.html', form=form,
            movie_title=movie_title, secondary_title=secondary_title, is_lookup=is_lookup)

@web.route('/movie_titles/<movie_title_id>/secondary_titles/<int:secondary_title_idx>/', methods=['GET', 'POST'])
def secondary_title_edit(movie_title_id, secondary_title_idx):
    movie_title_key = ndb.Key(MovieTitles, movie_title_id)
    movie_title = movie_title_key.get()
    secondary_title = movie_title.secondary_titles[secondary_title_idx]
    form = SecondaryMovieTitlesForm(obj=secondary_title)

    if request.method == 'POST' and form.validate_on_submit():
        correlation_titles = movies.get_all_correlation_titles(form.movie_title.data)
        form.populate_obj(secondary_title)
        secondary_title.correlation_title_version_one = correlation_titles[0]
        secondary_title.correlation_title_version_two = correlation_titles[1]
        secondary_title.correlation_title_version_three = correlation_titles[2]
        secondary_title.correlation_title_version_four = correlation_titles[3]
        movie_title.put()

        flash('Success')

        return redirect(url_for('.movie_title_update_and_show',
                movie_title_id=movie_title_id))

    return render_template('secondary_movie_title_edit.html', form=form,
            movie_title=movie_title, secondary_title=secondary_title)

@web.route('/movie_titles/<movie_title_id>/secondary_titles/<int:secondary_title_idx>/delete', methods=['POST'])
def secondary_title_delete(movie_title_id, secondary_title_idx):
    movie_title_key = ndb.Key(MovieTitles, movie_title_id)
    movie_title = movie_title_key.get()
    secondary_title = movie_title.secondary_titles[secondary_title_idx].movie_title

    if request.method == 'POST':
        movie_title.secondary_titles.pop(secondary_title_idx)
        movie_title.put()

        flash('Deleted secondary title %s' % secondary_title)

    return redirect(url_for('.movie_title_update_and_show', movie_title_id=movie_title_id))

@web.route('/movie_titles/lookup/')
def movie_titles_lookup_index():
    movie_titles_lookup = MovieTitlesLookup.query().fetch()

    return render_template('movie_titles_lookup_index.html',
            movie_titles_lookup=movie_titles_lookup)

@web.route('/movie_titles/search_rottentomatoes_id')
def search_rottentomatoes_id():
    title = ''

    try:
        if 'title' in request.args:
            title = request.args.get('title')

        searchable_words = movies.get_searchable_words(title)
        title_to_search = unicode(' '.join(searchable_words))

        movie_list = search_rottentomatoes_details(title_to_search)

        return jsonify(status='ok', results=movie_list)
    except Exception, e:
        log.error("ERROR!, Failed to search Rotten Tomatoes info...")
        log.error(e)

        return jsonify(status='error', message='Failed to fetch Rotten Tomatoes ID.')

def add_secondary_title_from_lookup(movie_title, lookup, secondary_title):
    if movie_title and lookup:
        log.info("Start Adding Secondary Title to Movie Title %s" % movie_title.title)

        correlation_titles = movies.get_all_correlation_titles(lookup.movie_title)
        secondary_title.movie_title = lookup.movie_title
        secondary_title.correlation_title_version_one = correlation_titles[0]
        secondary_title.correlation_title_version_two = correlation_titles[1]
        secondary_title.correlation_title_version_three = correlation_titles[2]
        secondary_title.correlation_title_version_four = correlation_titles[3]
        movie_title.secondary_titles.append(secondary_title)

        return movie_title


###############
#
# Dashboard for Uploading Posters and Assets Image
#
###############

@web.route('/movies/upload_default_poster/', methods=['GET', 'POST'])
def movie_upload_default_poster():
    movie = None
    form = PosterImageForm()

    if request.method == 'POST' and form.validate_on_submit():
        image_file = request.files[form.image_file.name]

        if admin.check_uploaded_file(image_file):
            poster_image_blob = save_poster_image(image_file, form,
                    is_default=True)

            try:
                cache_key = "movie::default::poster::%s::%s::%s" % (poster_image_blob.orientation,
                        poster_image_blob.resolution, poster_image_blob.application_type)
                poster_url = get_serving_url(poster_image_blob.image)

                memcache.set(cache_key, poster_url)

                log.info("Movie: Default poster cached: %s" % cache_key)
            except:
                pass

            flash('Uploaded Default Poster Image!')

            return redirect(url_for('.movie_index'))
        else:
            form.errors['generic'] = 'Invalid File!'

    return render_template('movie_upload_poster.html', form=form, movie=movie)

@web.route('/movies/<movie_id>/upload_poster/', methods=['GET', 'POST'])
def movie_upload_poster(movie_id):
    movie_key = ndb.Key(Movie, movie_id)
    movie = movie_key.get()
    form = PosterImageForm()

    if request.method == 'POST' and form.validate_on_submit():
        image_file = request.files[form.image_file.name]

        if admin.check_uploaded_file(image_file):
            poster_image_blob = save_poster_image(image_file, form, parent_key=movie_key)

            try:
                cache_key = "movie::poster::%s::%s::%s::%s" % (movie_key.id(), poster_image_blob.orientation,
                        poster_image_blob.resolution, poster_image_blob.application_type)
                poster_url = get_serving_url(poster_image_blob.image)

                memcache.set(cache_key, poster_url)

                log.debug("movie_upload_poster, %s poster cached, %s..." % (movie.canonical_title, cache_key))
            except Exception, e:
                log.warn("ERROR!, movie_upload_poster, caching...")
                log.error(e)

            admin.refresh_movie_gmoviesdigitalventures(movie.key.id())

            flash('Uploaded Poster Image!')

            return redirect(url_for('.movie_update_and_show', movie_id=movie_id))
        else:
            form.errors['generic'] = 'Invalid File!'

    return render_template('movie_upload_poster.html', form=form, movie=movie)

@web.route('/movies/<movie_id>/upload_background_poster/', methods=['GET', 'POST'])
def movie_upload_background_poster(movie_id):
    movie_key = ndb.Key(Movie, movie_id)
    movie = movie_key.get()
    form = BackgroundPosterImageForm()

    if request.method == 'POST' and form.validate_on_submit():
        image_file = request.files[form.image_file.name]

        if admin.check_uploaded_file(image_file):
            background_poster_image_blob = save_poster_image(image_file,
                    form, parent_key=movie_key, is_background=True)

            try:
                cache_key = "movie::background::poster::%s::%s" % (movie_key.id(),
                        background_poster_image_blob.application_type)
                background_poster_url = get_serving_url(background_poster_image_blob.image)

                memcache.set(cache_key, background_poster_url)

                log.info("Movie: %s background poster cached: %s" % (movie.canonical_title, cache_key))
            except:
                pass

            flash('Uploaded Background Poster Image!')

            return redirect(url_for('.movie_update_and_show', movie_id=movie_id))
        else:
            form.errors['generic'] = 'Invalid File!'

    return render_template('movie_upload_background_poster.html', form=form, movie=movie)

@web.route('/movies/<movie_id>/posters/')
def movie_posters(movie_id):
    movie_key = ndb.Key(Movie, movie_id)
    movie = movie_key.get()
    poster_image_blob = PosterImageBlob.query(ancestor=movie_key).fetch()

    return render_template('movie_poster_show.html', movie=movie,
            posters=poster_image_blob, get_serving_url=get_serving_url)

@web.route('/movies/default_posters/')
def movie_default_posters():
    movie = None
    poster_image_blob = PosterImageBlob.query(PosterImageBlob.is_default == True).fetch()

    return render_template('movie_poster_show.html', movie=movie,
            posters=poster_image_blob, get_serving_url=get_serving_url)

@web.route('/movies/<movie_id>/alternate_posters/')
def movie_alternate_posters(movie_id):
    movie_key = ndb.Key(Movie, movie_id)
    movie = movie_key.get()

    return render_template('movie_alternate_poster_show.html', movie=movie)

@web.route('/assets/coming_soon_page/')
def assets_coming_soon_page():
    coming_soon_tab_images = AssetsImageBlob.query(
            AssetsImageBlob.assets_group == 'coming_soon_tab').fetch()

    return render_template('assets_coming_soon_page.html',
            coming_soon_tab_images=coming_soon_tab_images,
            get_serving_url=get_serving_url)

@web.route('/assets/coming_soon_page/upload_tab_image/', methods=['GET', 'POST'])
def assets_coming_soon_page_upload_tab_image():
    form = ComingSoonAssetsForm()

    if request.method == 'POST' and form.validate_on_submit():
        assets_image_name = calendar.month_name[int(form.month.data)]
        image_file = request.files[form.image_file.name]

        if admin.check_uploaded_file(image_file):
            assets_coming_soon_page_image_blob = save_assets_image(image_file,
                    assets_image_name, assets_group='coming_soon_tab')

            try:
                cache_key = "movies::coming_soon::month_image_tab::%s" % assets_coming_soon_page_image_blob.name
                tab_image_url = get_serving_url(assets_coming_soon_page_image_blob.image)

                memcache.set(cache_key, tab_image_url)

                log.info("Movies: Coming Soon image tab for month cached: %s" % cache_key)
            except Exception, e:
                log.error(e)

            flash('Uploaded Coming Soon Page Assets Image!')

            return redirect(url_for('.assets_coming_soon_page'))
        else:
            form.errors['generic'] = 'Invalid File!'

    return render_template('assets_coming_soon_page_upload_tab_image.html',
            form=form)

@web.route('/convenience_fee_settings/')
def convenience_fee_settings_index():
    convenience_fee_settings = ConvenienceFeeSettings.query().fetch()

    return render_template('convenience_fee_settings_index.html',
            convenience_fee_settings=convenience_fee_settings)

@web.route('/convenience_fee_settings/new', methods=['GET', 'POST'])
def convenience_fee_settings_new():
    form = ConvenienceFeeSettingsForm()
    existing_settings = get_convenience_fee_settings_theaterorgs()
    parents_url_strings = map(lambda s: s.parent().urlsafe(), existing_settings)
    theaterorgs_list = get_theaterorgs()
    unassigned_theaterorgs = filter(lambda t: t['key'] not in parents_url_strings, theaterorgs_list)
    form.theaterorgs.choices = [(t['key'], t['name']) for t in unassigned_theaterorgs]

    if request.method == 'POST' and form.validate_on_submit():
        parent_string = form.theaterorgs.data
        parent_key = ndb.Key(urlsafe=parent_string)
        parent_name = parent_key.get().name

        convenience_fee_settings = ConvenienceFeeSettings(parent=parent_key)

        convenience_fees = {'credit_card': form.credit_card_convenience_fee.data,
                'bank_payment': form.bank_payment_convenience_fee.data,
                'gcash': form.gcash_convenience_fee.data}
        convenience_fees_message = {'credit_card': form.credit_card_convenience_fee_message.data,
                'bank_payment': form.bank_payment_convenience_fee_message.data,
                'gcash': form.gcash_convenience_fee_message.data}

        convenience_fee_settings.entity_class = 'theaterorgs'
        convenience_fee_settings.convenience_fees = convenience_fees
        convenience_fee_settings.convenience_fees_message = convenience_fees_message
        convenience_fee_settings.put()

        flash("Convenience fee settings for %s has been created." % parent_name)

        return redirect(url_for('.convenience_fee_settings_update_and_show',
                settings_id=convenience_fee_settings.key.urlsafe()))

    return render_template('convenience_fee_settings_new.html', form=form)

@web.route('/convenience_fee_settings/<settings_id>/', methods=['GET', 'POST'])
def convenience_fee_settings_update_and_show(settings_id):
    convenience_fee_settings_key = ndb.Key(urlsafe=settings_id)
    convenience_fee_settings = convenience_fee_settings_key.get()
    parent_name = convenience_fee_settings.key.parent().get().name
    parent_url_string = convenience_fee_settings.key.parent().urlsafe()

    if not convenience_fee_settings:
        abort(404)

    form = ConvenienceFeeSettingsForm()
    form.theaterorgs.choices = []

    if request.method == 'POST' and form.validate_on_submit():
        convenience_fee_settings.convenience_fees['credit_card'] = form.credit_card_convenience_fee.data
        convenience_fee_settings.convenience_fees_message['credit_card'] = form.credit_card_convenience_fee_message.data
        convenience_fee_settings.convenience_fees['bank_payment'] = form.bank_payment_convenience_fee.data
        convenience_fee_settings.convenience_fees_message['bank_payment'] = form.bank_payment_convenience_fee_message.data
        convenience_fee_settings.convenience_fees['gcash'] = form.gcash_convenience_fee.data
        convenience_fee_settings.convenience_fees_message['gcash'] = form.gcash_convenience_fee_message.data
        convenience_fee_settings.put()

        flash("Convenience fee settings for %s has been created." % parent_name)

        return redirect(url_for('.convenience_fee_settings_update_and_show',
                settings_id=convenience_fee_settings.key.urlsafe()))
    else:
        form.credit_card_convenience_fee.data = convenience_fee_settings.convenience_fees.get('credit_card', None)
        form.credit_card_convenience_fee_message.data = convenience_fee_settings.convenience_fees_message.get('credit_card', None)
        form.bank_payment_convenience_fee.data = convenience_fee_settings.convenience_fees.get('bank_payment', None)
        form.bank_payment_convenience_fee_message.data = convenience_fee_settings.convenience_fees_message.get('bank_payment', None)
        form.gcash_convenience_fee.data = convenience_fee_settings.convenience_fees.get('gcash', None)
        form.gcash_convenience_fee_message.data = convenience_fee_settings.convenience_fees_message.get('gcash', None)

    return render_template('convenience_fee_settings_show.html', form=form,
            convenience_fee_settings=convenience_fee_settings)

def get_convenience_fee_settings_theaterorgs():
    convenience_fee_settings = ConvenienceFeeSettings.query().fetch(keys_only=True)
    filtered = filter(lambda x: x.parent().kind() == 'TheaterOrganization', convenience_fee_settings)

    return filtered

def get_theaterorgs():
    theaterorgs_list = []
    theaterorgs = models.TheaterOrganization.query().fetch()
    key_name = map(lambda x: {'key': x.key.urlsafe(), 'name': x.name }, theaterorgs)
    theaterorgs_list.extend(key_name)

    return theaterorgs_list


###############
#
# GMovies Promo CMS Pages
#
###############

@web.route('/promos/', methods=['GET'])
def promo_index():
    promos = Promo.query().order(Promo.available_date, Promo.start_time, Promo.theater, Promo.cinema).fetch()

    return render_template('promo_index.html', promos=promos)

@web.route('/promos/new/', methods=['GET', 'POST'])
def promo_new():
    form = PromoNewForm()
    movie_choices = [('', '')]
    active_movies = Movie.query(Movie.is_expired==False, Movie.is_inactive==False).order(Movie.release_date).fetch()

    for movie in active_movies:
        movie_choices.append((movie.key.id(), movie.canonical_title))

    form.movie_title.choices = movie_choices

    if request.method == 'POST' and form.validate_on_submit():
        promo = Promo(key=ndb.Key(Promo, str(uuid_gen())))
        form.populate_obj(promo)

        if form.movie_title.data:
            promo.movie = ndb.Key(Movie, form.movie_title.data)
        else:
            promo.movie = None

        if form.start_time_timefield.data:
            promo.start_time = form.start_time_timefield.data
        else:
            promo.start_time = None

        if form.end_time_timefield.data:
            promo.end_time = form.end_time_timefield.data
        else:
            promo.end_time = None

        if not form.convenience_fees_text.data:
            promo.convenience_fees = {}
        else:
            promo.convenience_fees = ast.literal_eval(form.convenience_fees_text.data)

        if not form.convenience_fees_message_text.data:
            promo.convenience_fees_message = {}
        else:
            promo.convenience_fees_message = ast.literal_eval(form.convenience_fees_message_text.data)

        promo.seat_count = 0
        promo.seat_map = []
        promo.seats_whitelist = []
        promo.seats_blacklist = []
        promo.seats_locked = []
        promo.seats_purchased = []
        promo.payment_options_android = []
        promo.payment_options_ios = []
        promo.payment_options_website = []
        promo.platform = []
        promo.put()

        theater_logo_image_file = request.files[form.theater_logo_image_file.name]

        if theater_logo_image_file:
            assets_entity = AssetsImageBlob(key=ndb.Key(Promo, promo.key.id(), AssetsImageBlob, str(uuid_gen())))
            assets_entity.name = promo.key.id()
            assets_entity.assets_group = 'promos-theater-logo'
            assets_entity.is_active = True
            assets_entity.image = get_image_blob_key(assets_entity, theater_logo_image_file, folder='assets/blocked_screening/')
            assets_entity.put()

        admin.gmoviesdigitalventures_blockedscreening()

        flash("Success")

        return redirect(url_for('.promo_update_and_show', promo_id=promo.key.id()))

    return render_template('promo_new.html', form=form)

@web.route('/promos/<promo_id>/', methods=['GET', 'POST'])
def promo_update_and_show(promo_id):
    promo_key = ndb.Key(Promo, promo_id)
    promo = promo_key.get()
    form = PromoEditForm(obj=promo)
    assets_entity = AssetsImageBlob.query(AssetsImageBlob.assets_group=='promos-theater-logo', ancestor=promo_key).get()

    seat_rows = [','.join(row) for row in promo.seat_map]
    movie_choices = [('', '')]
    active_movies = Movie.query(Movie.is_expired==False, Movie.is_inactive==False).order(Movie.release_date).fetch()

    for movie in active_movies:
        movie_choices.append((movie.key.id(), movie.canonical_title))

    form.seat_map.choices = zip(seat_rows, seat_rows)
    form.movie_title.choices = movie_choices

    if request.method == 'POST' and form.validate_on_submit():
        form.populate_obj(promo)

        if form.movie_title.data:
            promo.movie = ndb.Key(Movie, form.movie_title.data)
        else:
            promo.movie = None

        if form.start_time_timefield.data:
            promo.start_time = form.start_time_timefield.data
        else:
            promo.start_time = None

        if form.end_time_timefield.data:
            promo.end_time = form.end_time_timefield.data
        else:
            promo.end_time = None

        if not form.convenience_fees_text.data:
            promo.convenience_fees = {}
        else:
            promo.convenience_fees = ast.literal_eval(form.convenience_fees_text.data)

        if not form.convenience_fees_message_text.data:
            promo.convenience_fees_message = {}
        else:
            promo.convenience_fees_message = ast.literal_eval(form.convenience_fees_message_text.data)

        seat_map = admin.add_seats(form.addseats.data, form.seat_map.data)
        promo.seat_map = seat_map
        promo.seat_count = len(filter(lambda seat: seat != '', sum(seat_map, [])))
        promo.put()

        ticket_template_image_file = request.files[form.ticket_template_image_file.name]
        theater_logo_image_file = request.files[form.theater_logo_image_file.name]

        if ticket_template_image_file:
            ticket_template_image_data = ticket_template_image_file.read()
            ticket_template_image = admin.save_ticket_template_image(ticket_template_image_data, promo.key, template_type='promo')

        if theater_logo_image_file:
            if not assets_entity:
                assets_entity = AssetsImageBlob(key=ndb.Key(Promo, promo_key.id(), AssetsImageBlob, str(uuid_gen())))
                assets_entity.name = promo_key.id()
                assets_entity.assets_group = 'promos-theater-logo'
                assets_entity.is_active = True

            assets_entity.image = get_image_blob_key(assets_entity, theater_logo_image_file, folder='assets/blocked_screening/')
            assets_entity.put()

        admin.gmoviesdigitalventures_blockedscreening()

        flash("Success")

        return redirect(url_for('.promo_update_and_show', promo_id=promo.key.id()))
    else:
        form.seat_map.data = seat_rows
        form.convenience_fees_text.data = promo.convenience_fees
        form.convenience_fees_message_text.data = promo.convenience_fees_message

        if promo.movie:
            form.movie_title.data = promo.movie.id()

        if promo.start_time:
            form.start_time_timefield.data = promo.start_time

        if promo.end_time:
            form.end_time_timefield.data = promo.end_time

        ticket_template_image = TicketTemplateImageBlob.query(TicketTemplateImageBlob.template_type=='promo', ancestor=promo.key).get()

    return render_template('promo_show.html', form=form, promo=promo, ticket_template_image=ticket_template_image,
            assets_entity=assets_entity, get_serving_url=get_serving_url)

@web.route('/promos/<promo_id>/seats_status/', methods=['GET', 'POST'])
def promo_seats_status(promo_id):
    seats = []
    form = PromoSeatsStatusForm()
    promo_key = ndb.Key(Promo, promo_id)
    promo = promo_key.get()

    if promo.seat_map:
        for row_seats in promo.seat_map:
            seats.extend(row_seats)

    form.seats_blacklist.choices = zip(seats, seats)
    form.seats_locked.choices = zip(seats, seats)
    form.seats_purchased.choices = zip(seats, seats)

    if request.method == 'POST' and form.validate_on_submit():
        promo.seats_blacklist = form.seats_blacklist.data
        promo.seats_locked = form.seats_locked.data
        promo.seats_purchased = form.seats_purchased.data
        promo.put()

        flash("Success")

        return redirect(url_for('.promo_update_and_show', promo_id=promo.key.id()))
    else:
        form.seats_blacklist.data = promo.seats_blacklist
        form.seats_locked.data = promo.seats_locked
        form.seats_purchased.data = promo.seats_purchased

    return render_template('promo_seats_status.html', form=form, promo=promo)

@web.route('/promos/<promo_id>/seatmap/upload/', methods=['GET', 'POST'])
def promo_seatmap_upload(promo_id):
    error_list = []
    promo_key = ndb.Key(Promo, promo_id)
    form = FileUploadForm()

    if request.method == 'POST':
        seatmap_file = request.files[form.file_uploaded.name]
        seat_map, seat_count, error_list = admin.check_seatmap_upload_file(seatmap_file)

        if len(error_list) == 0:
            promo = promo_key.get()
            promo.seat_map = seat_map
            promo.seat_count = seat_count
            promo.put()

            flash('Successfully uploaded seat map')

            return redirect(url_for('.promo_update_and_show', promo_id=promo_id))

    return render_template('promo_seatmap_upload.html', form=form,
            promo_id=promo_id, error_list=error_list)

@web.route('/promos/<promo_id>/posters/')
def promo_posters(promo_id):
    promo_key = ndb.Key(Promo, promo_id)
    promo = promo_key.get()
    poster_image_blob = PosterImageBlob.query(ancestor=promo_key).fetch()

    return render_template('promo_poster_show.html', promo=promo, posters=poster_image_blob, get_serving_url=get_serving_url)

@web.route('/promos/<promo_id>/upload_poster/', methods=['GET', 'POST'])
def promo_upload_poster(promo_id):
    promo_key = ndb.Key(Promo, promo_id)
    promo = promo_key.get()
    form = PosterImageForm()

    if request.method == 'POST' and form.validate_on_submit():
        image_file = request.files[form.image_file.name]

        if admin.check_uploaded_file(image_file):
            is_background = form.is_background.data
            poster_image_blob = save_poster_image(image_file, form, parent_key=promo_key, is_background=is_background)

            try:
                if is_background:
                    cache_key = "promo::background::poster::%s::%s" % (promo_key.id(), poster_image_blob.application_type)
                else:
                    cache_key = "promo::poster::%s::%s::%s::%s" % (promo_key.id(), poster_image_blob.orientation,
                            poster_image_blob.resolution, poster_image_blob.application_type)

                poster_url = get_serving_url(poster_image_blob.image)
                memcache.set(cache_key, poster_url)

                log.info("Promo: %s poster cached: %s" % (promo.name, cache_key))
            except Exception, e:
                log.warn("ERROR!, promo_upload_poster...")
                log.error(e)

            admin.gmoviesdigitalventures_blockedscreening()

            flash('Uploaded Poster Image!')

            return redirect(url_for('.promo_update_and_show', promo_id=promo_id))
        else:
            form.errors['generic'] = 'Invalid File!'

    return render_template('promo_upload_poster.html', form=form, promo=promo)

@web.route('/promos/<promo_id>/image_serving_urls/')
def image_get_serving_urls(promo_id):
    image_urls = {}
    promo_key = ndb.Key(Promo, promo_id)
    posters = PosterImageBlob.query(ancestor=promo_key).fetch()
    assets = AssetsImageBlob.query(AssetsImageBlob.is_active==True,
            ancestor=promo_key).fetch()

    for poster in posters:
        key_name = '%s-%s-%s' % (poster.orientation, poster.resolution, poster.application_type)
        image_urls[key_name] = get_serving_url(poster.image)
        log.info("PROMO, poster %s added to images_url, %s..." % (promo_key.id(), key_name))

    for asset in assets:
        key_name = asset.assets_group
        image_urls[key_name] = get_serving_url(asset.image)
        log.info("PROMO, asset %s added to images_url, %s..." % (promo_key.id(), key_name))

    promo = promo_key.get()
    promo.image_urls = image_urls
    promo.put()

    flash("Saved image serving urls!")

    return redirect(url_for('.promo_update_and_show', promo_id=promo_id))

@web.route('/assets/promos/')
def assets_promos_index():
    blocked_screening_assets = AssetsImageBlob.query(AssetsImageBlob.assets_group=='promos-blocked-screening').fetch()

    return render_template('assets_blocked_screening_index.html', blocked_screening_assets=blocked_screening_assets, get_serving_url=get_serving_url)

@web.route('/assets/promos/new/', methods=['GET', 'POST'])
def assets_promos_new():
    form = BlockedScreeningAssetsForm()

    if request.method == 'POST' and form.validate_on_submit():
        assets_image_name = form.name.data
        image_file = request.files[form.image_file.name]

        if admin.check_uploaded_file(image_file):
            assets_entity = AssetsImageBlob(key=ndb.Key(AssetsImageBlob, str(uuid_gen())))
            assets_entity.name = form.name.data
            assets_entity.platform = form.platform.data
            assets_entity.is_active = form.is_active.data
            assets_entity.assets_group = 'promos-blocked-screening'
            assets_entity.image = get_image_blob_key(assets_entity, image_file, folder='assets/blocked_screening/')
            assets_entity.put()

            try:
                cache_key = "assets::blocked_screening::poster::%s" % assets_entity.name
                blocked_screening_image_url = get_serving_url(assets_entity.image)
                memcache.set(cache_key, blocked_screening_image_url)

                log.info("ASSETS, blocked screening poster cached: %s" % cache_key)
            except Exception, e:
                log.warn("ASSETS, something went wrong...")
                log.error(e)

            flash('Uploaded Blocked Screening Assets Image!')

            return redirect(url_for('.assets_promos_index'))
        else:
            form.errors['generic'] = 'Invalid File!'

    return render_template('assets_blocked_screening_new.html', form=form)

@web.route('/assets/promos/<assets_id>/show/', methods=['GET', 'POST'])
def assets_promos_show(assets_id):
    assets_key = ndb.Key(AssetsImageBlob, assets_id)
    assets_entity = assets_key.get()
    form = BlockedScreeningAssetsForm(obj=assets_entity)

    if request.method == 'POST' and form.validate_on_submit():
        image_file = request.files[form.image_file.name]
        assets_entity.name = form.name.data
        assets_entity.platform = form.platform.data
        assets_entity.is_active = form.is_active.data

        if image_file:
            if admin.check_uploaded_file(image_file):
                assets_entity.image = get_image_blob_key(assets_entity,
                        image_file, folder='assets/blocked_screening/')

                try:
                    cache_key = "assets::blocked_screening::poster::%s" % assets_entity.name
                    blocked_screening_image_url = get_serving_url(assets_entity.image)
                    memcache.set(cache_key, blocked_screening_image_url)

                    log.info("ASSETS, blocked screening poster cached: %s" % cache_key)
                except Exception, e:
                    log.warn("ASSETS, something went wrong...")
                    log.error(e)
            else:
                form.errors['generic'] = 'Invalid File!'

        assets_entity.put()

        flash('Success!')

        return redirect(url_for('.assets_promos_index'))

    return render_template('assets_blocked_screening_show.html', form=form,
            assets_entity=assets_entity)

@web.route('/promos/transactions/')
def promo_transactions_index():
    t_cur = ndb.Cursor(urlsafe=request.args.get('t_c'))
    r_cur = ndb.Cursor(urlsafe=request.args.get('r_c'))
    d_cur = ndb.Cursor(urlsafe=request.args.get('d_c'))

    transactions = PromoReservationTransaction.query(
            PromoReservationTransaction.state.IN([promo_state.TX_START,
                    promo_state.TX_PREPARE, promo_state.TX_STARTED,
                    promo_state.TX_RESERVATION_HOLD,
                    promo_state.TX_RESERVATION_OK, promo_state.TX_PAYMENT_HOLD,
                    promo_state.TX_PAYNAMICS_PAYMENT_HOLD,
                    promo_state.TX_GENERATE_TICKET, promo_state.TX_PREPARE_ERROR,
                    promo_state.TX_RESERVATION_ERROR,
                    promo_state.TX_PAYMENT_ERROR])).order(-PromoReservationTransaction.date_created,
                            PromoReservationTransaction.key)
    reaped_transactions = PromoReservationTransaction.query(
            PromoReservationTransaction.state==promo_state.TX_CANCELLED).order(-PromoReservationTransaction.date_created)
    done_transactions = PromoReservationTransaction.query(
            PromoReservationTransaction.state==promo_state.TX_DONE).order(-PromoReservationTransaction.date_created)

    transactions_page, next_t_cur, t_more = transactions.fetch_page(25, start_cursor=t_cur)
    reaped_transactions_page, next_r_cur, r_more = reaped_transactions.fetch_page(25, start_cursor=r_cur)
    done_transactions_page, next_d_cur, d_more = done_transactions.fetch_page(25, start_cursor=d_cur)

    return render_template('promo_transactions_index.html',
            transactions=transactions_page, t_c=t_cur, next_t_c=next_t_cur, t_more=t_more,
            reaped=reaped_transactions_page, r_c=r_cur, next_r_c=next_r_cur, r_more=r_more,
            done=done_transactions_page, d_c=d_cur, next_d_c=next_d_cur, d_more=d_more)

@web.route('/promos/transactions/search/', methods=['GET', 'POST'])
def promo_transactions_search():
    date_today = datetime.datetime.today().date()
    render_data = {}
    render_data['filter_selected'] = {}
    render_data['filter_selected']['transactions_from'] = ""
    render_data['filter_selected']['transactions_to'] = ""
    render_data['filter_selected']['promo_name'] = ""
    render_data['filter_selected']['payment_ref'] = ""
    render_data['filter_selected']['reservation_ref'] = ""
    render_data['filter_selected']['payment_type'] = {}

    if request.method == 'POST':
        log.info("REQUEST METHOD: {}".format(request.method))
        render_data['filter_selected']['transactions_from'] = request.form['transactions_from']
        render_data['filter_selected']['transactions_to'] = request.form['transactions_to']
        render_data['filter_selected']['promo_name'] = request.form['promo_names']
        render_data['filter_selected']['payment_ref'] = request.form['payment_ref']
        render_data['filter_selected']['reservation_ref'] = request.form['reservation_ref']
        render_data['filter_selected']['payment_type'] = request.form['payment_type']
    else:
        log.info("REQUEST METHOD: {}".format(request.method))
        render_data['filter_selected']['transactions_from'] = request.args.get('transactions_from', None)
        render_data['filter_selected']['transactions_to'] = request.args.get('transactions_to', None)
        render_data['filter_selected']['promo_name'] = request.args.get('promo_names', None)
        render_data['filter_selected']['payment_ref'] = request.args.get('payment_ref', None)
        render_data['filter_selected']['reservation_ref'] = request.args.get('reservation_ref', None)
        render_data['filter_selected']['payment_type'] = request.args.get('payment_type', None)

    cursor = ndb.Cursor(urlsafe=request.args.get('cursor'))
    txpromo_state = request.args.get('state', str(promo_state.TX_DONE))
    txpromo_state = int(txpromo_state)

    transactions = PromoReservationTransaction.query(
            PromoReservationTransaction.state==txpromo_state).order(-PromoReservationTransaction.date_created)

    if not render_data['filter_selected']['transactions_from'] or render_data['filter_selected']['transactions_from'] is None:
        render_data['filter_selected']['transactions_from'] = str(date_today)

    if 'transactions_to' not in render_data['filter_selected'] or not render_data['filter_selected']['transactions_to'] or render_data['filter_selected']['transactions_to'] is None:
        render_data['filter_selected']['transactions_to'] = str(date_today)

    # pprint.pprint(render_data['filter_selected'])

    render_data['promo_transactions'] = {}

    datetime_from = admin.parse_string_to_date(render_data['filter_selected']['transactions_from'] + ' 00:00:00', fdate='%Y-%m-%d %H:%M:%S')
    datetime_to = admin.parse_string_to_date(render_data['filter_selected']['transactions_to'] + ' 23:59:59', fdate='%Y-%m-%d %H:%M:%S')

    log.info("FROM: {} TO: {}".format(datetime_from, datetime_to))
    transactions = transactions.filter(PromoReservationTransaction.date_created >= datetime_from, PromoReservationTransaction.date_created <= datetime_to)

    if render_data['filter_selected']['promo_name']:
        selected_promo_key = ndb.Key(Promo, render_data['filter_selected']['promo_name'])
        selected_promo = [selected_promo_key.get().key]

        transactions = transactions.filter(PromoReservationTransaction.reservations.promo.IN(selected_promo))

    if render_data['filter_selected']['payment_ref']:
        transactions = transactions.filter(PromoReservationTransaction.payment_reference == render_data['filter_selected']['payment_ref'])

    if render_data['filter_selected']['reservation_ref']:
        transactions = transactions.filter(PromoReservationTransaction.reservation_reference == render_data['filter_selected']['reservation_ref'])

    if render_data['filter_selected']['payment_type']:
        transactions = transactions.filter(PromoReservationTransaction.payment_type == render_data['filter_selected']['payment_type'])

    if request.method == "POST":
        log.info("DOWNLOAD EXCEL")

        row_counter = 0
        workbook = xlwt.Workbook(encoding='utf-8')
        worksheet = workbook.add_sheet("Sheet 1")
        headers_font_style = xlwt.XFStyle()
        headers_font_style.font.bold = True
        font_style = xlwt.XFStyle()
        font_style.font.bold = False

        # datetime_from
        # datetime_to

        filename_date = ""

        worksheet.write(0, 0, 'DATE CREATED', headers_font_style)
        worksheet.write(0, 1, 'PROMO RESERVATION ID', headers_font_style)
        worksheet.write(0, 2, 'PAYMENT REFERENCE', headers_font_style)
        worksheet.write(0, 3, 'RESERVATION REFERENCE', headers_font_style)
        worksheet.write(0, 4, 'PROMO NAME', headers_font_style)
        worksheet.write(0, 5, 'THEATER', headers_font_style)
        worksheet.write(0, 6, 'CINEMA', headers_font_style)
        worksheet.write(0, 7, 'AVAILABLE DATE', headers_font_style)
        worksheet.write(0, 8, 'START TIME', headers_font_style)
        worksheet.write(0, 9, 'TICKET SOLD', headers_font_style)
        worksheet.write(0, 10, 'TICKET PRICE', headers_font_style)
        worksheet.write(0, 11, 'RESERVATION FEE', headers_font_style)
        worksheet.write(0, 12, 'TOTAL AMOUNT', headers_font_style)
        worksheet.write(0, 13, 'PAYMENT TYPE', headers_font_style)
        worksheet.write(0, 14, 'SEATS', headers_font_style)
        worksheet.write(0, 15, 'CUSTOMER', headers_font_style)
        worksheet.write(0, 16, 'EMAIL ADDRESS', headers_font_style)
        worksheet.write(0, 17, 'ADDRESS 1', headers_font_style)
        worksheet.write(0, 18, 'ADDRESS 2', headers_font_style)
        worksheet.write(0, 19, 'CITY', headers_font_style)
        worksheet.write(0, 20, 'STATE', headers_font_style)
        worksheet.write(0, 21, 'COUNTRY', headers_font_style)
        worksheet.write(0, 22, 'ZIP CODE', headers_font_style)
        worksheet.write(0, 23, 'PHONE NUMBER', headers_font_style)
        worksheet.write(0, 24, 'MOBILE NUMBER', headers_font_style)

        # worksheet.col(0).width = len('PURCHASED DATE') * 328
        # worksheet.col(1).width = len('TRANSACTIONS') * 336
        # worksheet.col(2).width = len('TICKETS SOLD') * 384
        # worksheet.col(3).width = len('NET SALES') * 390
        # worksheet.col(4).width = len('NET REVENUE') * 390

        for transaction in transactions:
            row_counter += 1
            
            worksheet.write(row_counter, 0, transaction.date_created.strftime('%b %d, %Y %I:%M:%S %p'), font_style)
            worksheet.write(row_counter, 1, transaction.key.id(), font_style)
            worksheet.write(row_counter, 2, transaction.payment_reference, font_style)
            worksheet.write(row_counter, 3, transaction.reservation_reference, font_style)
            worksheet.write(row_counter, 4, transaction.ticket.extra['promo_name'], font_style)
            worksheet.write(row_counter, 5, transaction.ticket.extra['theater_name'], font_style)
            worksheet.write(row_counter, 6, transaction.ticket.extra['cinema_name'], font_style)
            worksheet.write(row_counter, 7, datetime.datetime.strptime(transaction.ticket.extra['available_date'], '%m/%d/%Y').strftime('%b %d, %Y'), font_style)
            worksheet.write(row_counter, 8, transaction.ticket.extra['start_time'], font_style)
            worksheet.write(row_counter, 9, transaction.ticket.extra['ticket_count'], font_style)
            worksheet.write(row_counter, 10, transaction.ticket.extra['price_per_seat'], font_style)
            worksheet.write(row_counter, 11, transaction.ticket.extra['reservation_fee'], font_style)
            worksheet.write(row_counter, 12, transaction.ticket.extra['total_amount'], font_style)
            worksheet.write(row_counter, 13, transaction.payment_type, font_style)
            worksheet.write(row_counter, 14, transaction.ticket.extra['seats'], font_style)

            fname, lname = ("","")

            if 'fname' in transaction.payment_info:
                if transaction.payment_info['fname']:
                    fname = transaction.payment_info['fname'].encode('utf-8')

            if 'lname' in transaction.payment_info:
                if transaction.payment_info['lname']:
                    lname = transaction.payment_info['lname'].encode('utf-8')

            worksheet.write(row_counter, 15, "{} {}".format(fname, lname), font_style)

            email = ""
            if 'address1' in transaction.payment_info:
                if transaction.payment_info['email']:
                    email = transaction.payment_info['email'].encode('utf-8')
            worksheet.write(row_counter, 16, email, font_style)

            address1 = ""
            if 'address1' in transaction.payment_info:
                if transaction.payment_info['address1']:
                    address1 = transaction.payment_info['address1'].encode('utf-8')
            worksheet.write(row_counter, 17, address1, font_style)

            address2 = ""
            if 'address2' in transaction.payment_info:
                if transaction.payment_info['address2']:
                    address2 = transaction.payment_info['address2'].encode('utf-8')
            worksheet.write(row_counter, 18, address2, font_style)

            state = ""
            if 'state' in transaction.payment_info:
                if transaction.payment_info['state']:
                    state = transaction.payment_info['state'].encode('utf-8')
            worksheet.write(row_counter, 20, state, font_style)

            country = ""
            if 'country' in transaction.payment_info:
                if transaction.payment_info['country']:
                    country = transaction.payment_info['country'].encode('utf-8')
            worksheet.write(row_counter, 21, country, font_style)

            zipcode = ""
            if 'zip' in transaction.payment_info:
                if transaction.payment_info['zip']:
                    zipcode = transaction.payment_info['zip'].encode('utf-8')
            worksheet.write(row_counter, 22, zipcode, font_style)

            phone_num = ""
            if 'phone' in transaction.payment_info:
                if transaction.payment_info['phone']:
                    phone_num = transaction.payment_info['phone'].encode('utf-8')
            worksheet.write(row_counter, 23, phone_num, font_style)

            mobile_num = ""
            if 'mobile' in transaction.payment_info:
                if transaction.payment_info['mobile']:
                    mobile_num = transaction.payment_info['mobile'].encode('utf-8')
            worksheet.write(row_counter, 24, mobile_num, font_style)

        purchased_reports = StringIO.StringIO()
        workbook.save(purchased_reports)

        res = make_response(purchased_reports.getvalue())
        res.headers['Content-Type'] = 'application/vnd.ms-excel'
        res.headers['Content-Disposition'] = 'attachment; filename=PromoTransactionsReport.xls'

        return res

    transactions_page, next_cursor, more = transactions.fetch_page(100, start_cursor=cursor)

    render_data['promo_transactions'] = transactions_page

    render_data['payment_option_label'] = {'mpass': 'MPass', 'gmovies-promo': 'Gmovies Promo/s', 'client-initiated': 'Client Payment', 'ipay88-payment-initiated': 'iPay88 Payment', 'paynamics-payment-initiated': 'Paynamics Payment'}

    render_data['filter_label'] = {}
    promos = Promo.query(Promo.is_active==True).order(-Promo.available_date).fetch()
    render_data['filter_label']['promo_names'] = {}

    for promo in promos:
        log.info("Promo Name: {} {} {} {} {} {}".format(promo.key.id(), promo.name, promo.theater, promo.cinema, promo.available_date.strftime('%m-%d-%Y'), promo.start_time.strftime('%H%M')))

        render_data['filter_label']['promo_names'][promo.key.id()] = {}
        render_data['filter_label']['promo_names'][promo.key.id()]['label'] = "{} {} {} {} {}".format(promo.name, promo.theater, promo.cinema, promo.available_date.strftime('%m-%d-%Y'), promo.start_time.strftime('%H%M'))
        render_data['filter_label']['promo_names'][promo.key.id()]['name'] = promo.name
        render_data['filter_label']['promo_names'][promo.key.id()]['theater'] = promo.theater
        render_data['filter_label']['promo_names'][promo.key.id()]['cinema_name'] = promo.cinema
        render_data['filter_label']['promo_names'][promo.key.id()]['available_date'] = promo.available_date.strftime('%m-%d-%Y')
        render_data['filter_label']['promo_names'][promo.key.id()]['start_time'] = promo.start_time.strftime('%H%M')

    for trans in render_data['promo_transactions']:
        log.info("TRANS: {} {}".format(trans.key.id(), [reservation.promo.get().key.id() for reservation in trans.reservations]))

    return render_template('promo_transactions_search.html',
            transactions=transactions_page, next_cursor=next_cursor, more=more,
            cursor=cursor, txpromo_state=txpromo_state, render_data=render_data)

@web.route('/promos/transactions/<transaction_id>/')
def promo_transaction_show(transaction_id):
    email = None
    transaction = admin.get_promo_transaction(transaction_id)

    if transaction.payment_info and 'email' in transaction.payment_info:
        email = transaction.payment_info['email']

    return render_template('promo_transaction_show.html',
            transaction=transaction, email=email)

@web.route('/promos/transactions/<transaction_id>/email')
def promo_transaction_email(transaction_id):
    (device_id, txn_id) = id_encoder.decode_id_paths(transaction_id)
    transaction = admin.get_promo_transaction(transaction_id)
    email = transaction.payment_info['email']
    send_ticket_details(transaction.payment_info['email'], transaction.ticket)
    flash("Sent transaction details to %s" % transaction.payment_info['email'])

    return render_template('promo_transaction_show.html', transaction=transaction)

@web.route('/promos/transactions/purchase_reports', methods=['GET', 'POST'])
def promo_transactions_purchase_report_index():
    log.info('HTTP METHOD: {}'.format(request.method))
    render_data = {}
    render_data['post_data'] = request.values

    date_today = datetime.datetime.today().date()

    render_data['filters'] = {}
    filters = render_data['filters']
    if request.method == 'POST':
        filters['transactions_from'] = request.form['transactions_from']
        filters['transactions_to'] = request.form['transactions_to']
        filters['payment_type'] = request.form['payment_type']
        filters['promo_name'] = request.form['promo_names']
        filters['theater_name'] = request.form['theater_names']
        filters['platform'] = request.form['platform']
    else:
        filters['transactions_from'] = request.args.get('transactions_from', None)
        filters['transactions_to'] = request.args.get('transactions_to', None)
        filters['payment_type'] = request.args.get('payment_type', None)
        filters['promo_name'] = request.args.get('promo_names', None)
        filters['theater_name'] = request.args.get('theater_names', None)
        filters['platform'] = request.args.get('platform', None)

    render_data['promo_names'] = list(set([promo.name for promo in Promo.query().fetch()]))

    render_data['promos'] = []
    if filters['promo_name']:
        render_data['promos'] = Promo.query(Promo.name == filters['promo_name'].strip())

        render_data['theater_names'] = list(set([promo.theater for promo in render_data['promos'].fetch()]))

        if filters['theater_name']:
            render_data['promos'] = render_data['promos'].filter(Promo.theater == filters['theater_name'])

        render_data['promos'] = render_data['promos'].fetch(keys_only=True)

    if not filters['transactions_from'] or filters['transactions_from'] is None:
        filters['transactions_from'] = str(date_today)

    if not filters['transactions_to'] or filters['transactions_to'] is None:
        filters['transactions_to'] = str(date_today)

    render_data['promo_transactions'] = {}

    datetime_from = admin.parse_string_to_date(render_data['filters']['transactions_from'] + ' 00:00:00', fdate='%Y-%m-%d %H:%M:%S')
    datetime_to = admin.parse_string_to_date(render_data['filters']['transactions_to'] + ' 23:59:59', fdate='%Y-%m-%d %H:%M:%S')

    # Removed. Cause of inconsistent computation of date
    # datetime_from = admin.asia_manila_to_utc(datetime_from)
    # datetime_to = admin.asia_manila_to_utc(datetime_to)

    promo_transactions = PromoReservationTransaction.query(
            PromoReservationTransaction.state == state.TX_DONE,
            PromoReservationTransaction.date_created >= datetime_from,
            PromoReservationTransaction.date_created <= datetime_to)

    if render_data['promos']:
        promo_transactions = promo_transactions.filter(PromoReservationTransaction.reservations.promo.IN(render_data['promos']))

    if render_data['filters']['payment_type'] and render_data['filters']['payment_type'] is not None:
        promo_transactions = promo_transactions.filter(PromoReservationTransaction.payment_type == render_data['filters']['payment_type'])

    if render_data['filters']['platform'] and render_data['filters']['platform'] is not None:
        promo_transactions = promo_transactions.filter(PromoReservationTransaction.platform == render_data['filters']['platform'])

    promo_transactions = promo_transactions.order(-PromoReservationTransaction.date_created, PromoReservationTransaction.key)

    render_data['promo_transactions']['grand_total'] = {}
    render_data['promo_transactions']['grand_total']['transactions'] = 0.0
    render_data['promo_transactions']['grand_total']['tickets_sold'] = 0.0
    render_data['promo_transactions']['grand_total']['gross_sales'] = 0.0
    render_data['promo_transactions']['grand_total']['net_revenue'] = 0.0

    if promo_transactions:
        render_data['promo_transactions']['summary'] = {}
        render_data['purchased_date'] = []
        for transaction in promo_transactions:
            purchased_date = transaction.date_created.date().strftime("%m-%d-%Y").strip()

            promo_transaction = {}

            if transaction.ticket.extra:
                promo_transaction['mall'] = ''
                promo_transaction['screening'] = ''
                promo_transaction['ticket_sold'] = []
                promo_transaction['ticket_price'] = 0.0
                promo_transaction['total_ticket_price'] = 0.0
                promo_transaction['ticket_selling_price'] = 0.0
                promo_transaction['purchase_price'] = 0.0
                promo_transaction['convenience_fee'] = 0.0
            
                if 'theater_name' in transaction.ticket.extra:
                    promo_transaction['mall'] = str(transaction.ticket.extra['theater_name'])

                if 'show_datetime' in transaction.ticket.extra:
                    promo_transaction['screening'] = str(transaction.ticket.extra['show_datetime'])

                if 'seats' in transaction.ticket.extra:
                    promo_transaction['ticket_sold'] = transaction.ticket.extra['seats'].split(",")

                if 'price_per_seat' in transaction.ticket.extra:
                    promo_transaction['ticket_price'] = float(transaction.ticket.extra['price_per_seat'])
                    if promo_transaction['ticket_sold']:
                        promo_transaction['total_ticket_price'] = promo_transaction['ticket_price'] * len(promo_transaction['ticket_sold'])

                if 'reservation_fee' in transaction.ticket.extra:
                    promo_transaction['convenience_fee'] = float(transaction.ticket.extra['reservation_fee'])

                    if promo_transaction['ticket_price']:
                        promo_transaction['ticket_selling_price'] = promo_transaction['ticket_price'] + float(transaction.ticket.extra['reservation_fee'])

                    if promo_transaction['ticket_sold']:
                        promo_transaction['purchase_price'] = promo_transaction['ticket_selling_price'] * len(promo_transaction['ticket_sold'])

                net_sales_multiplier = 0.964
                ## TODO: net sales multiplier in PROMO

                promo_transaction['net_sales'] = promo_transaction['purchase_price'] * net_sales_multiplier
                promo_transaction['net_revenue'] = promo_transaction['net_sales'] - promo_transaction['total_ticket_price']

            if purchased_date not in render_data['promo_transactions']['summary']:
                render_data['purchased_date'].append(transaction.date_created.date())
                render_data['promo_transactions']['summary'][purchased_date] = {}
                render_data['promo_transactions']['summary'][purchased_date]['mall_purchase'] = []
                render_data['promo_transactions']['summary'][purchased_date]['transactions'] = 0.0
                render_data['promo_transactions']['summary'][purchased_date]['tickets_sold'] = 0.0
                render_data['promo_transactions']['summary'][purchased_date]['gross_sales'] = 0.0
                render_data['promo_transactions']['summary'][purchased_date]['net_revenue'] = 0.0

            render_data['promo_transactions']['summary'][purchased_date]['transactions'] += 1
            render_data['promo_transactions']['summary'][purchased_date]['tickets_sold'] += len(promo_transaction['ticket_sold'])
            render_data['promo_transactions']['summary'][purchased_date]['gross_sales'] += promo_transaction['net_sales']
            render_data['promo_transactions']['summary'][purchased_date]['net_revenue'] += promo_transaction['net_revenue']

            render_data['promo_transactions']['summary'][purchased_date]['date'] = transaction.date_created.date().strftime("%b. %d %Y").strip()

            render_data['promo_transactions']['summary'][purchased_date]['mall_purchase'].append(promo_transaction)

            render_data['promo_transactions']['grand_total']['transactions'] += 1
            render_data['promo_transactions']['grand_total']['tickets_sold'] += len(promo_transaction['ticket_sold'])
            render_data['promo_transactions']['grand_total']['gross_sales'] += promo_transaction['net_sales']
            render_data['promo_transactions']['grand_total']['net_revenue'] += promo_transaction['net_revenue']

    render_data['purchased_date'] = sorted(render_data['purchased_date'])[::-1]

    if request.method == "POST":
        log.info("DOWNLOAD EXCEL")

        row_counter = 0
        workbook = xlwt.Workbook(encoding='utf-8')
        worksheet = workbook.add_sheet("Sheet 1")
        headers_font_style = xlwt.XFStyle()
        headers_font_style.font.bold = True
        font_style = xlwt.XFStyle()
        font_style.font.bold = False

        # datetime_from
        # datetime_to

        filename_date = ""

        worksheet.write(0, 0, 'PURCHASED DATE', headers_font_style)
        worksheet.write(0, 1, 'TRANSACTIONS', headers_font_style)
        worksheet.write(0, 2, 'TICKETS SOLD', headers_font_style)
        worksheet.write(0, 3, 'NET SALES', headers_font_style)
        worksheet.write(0, 4, 'NET REVENUE', headers_font_style)

        worksheet.col(0).width = len('PURCHASED DATE') * 328
        worksheet.col(1).width = len('TRANSACTIONS') * 336
        worksheet.col(2).width = len('TICKETS SOLD') * 384
        worksheet.col(3).width = len('NET SALES') * 390
        worksheet.col(4).width = len('NET REVENUE') * 390

        for purchased_date in render_data['purchased_date']:
            p_date = purchased_date.strftime("%m-%d-%Y")
            row_counter += 1
            worksheet.write(row_counter, 0, render_data['promo_transactions']['summary'][p_date]['date'], font_style)
            worksheet.write(row_counter, 1, render_data['promo_transactions']['summary'][p_date]['transactions'],font_style)
            worksheet.write(row_counter, 2, render_data['promo_transactions']['summary'][p_date]['tickets_sold'],font_style)
            worksheet.write(row_counter, 3, render_data['promo_transactions']['summary'][p_date]['gross_sales'],font_style)
            worksheet.write(row_counter, 4, render_data['promo_transactions']['summary'][p_date]['net_revenue'],font_style)

        purchased_reports = StringIO.StringIO()
        workbook.save(purchased_reports)

        res = make_response(purchased_reports.getvalue())
        res.headers['Content-Type'] = 'application/vnd.ms-excel'
        res.headers['Content-Disposition'] = 'attachment; filename=PromoPurchaseReports.xls'

        return res
        
    log.info("DONE")

    return render_template('promo_transaction_purchase_report.html', render_data=render_data)
