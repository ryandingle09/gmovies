import geohash
import logging

from datetime import datetime, date
from itertools import groupby
from math import atan2, cos, radians, sin, sqrt
from operator import attrgetter, itemgetter

from flask import jsonify, request

from google.appengine.api import memcache, search
from google.appengine.api.images import get_serving_url
from google.appengine.ext import ndb
from google.appengine.ext.ndb import (Key, get_multi, FloatProperty, IntegerProperty,
        ComputedProperty, BooleanProperty, DateProperty, KeyProperty)

from gmovies import models, orgs
from gmovies.exceptions.api import (APISystemError, BadValueException, InvalidFilterPropertyException,
        InvalidOrderPropertyException, NotFoundException)
from gmovies.feeds import get_total_reviews, get_average_rating
from gmovies.indexes.geolocation import THEATER_GEO_INDEX_NAME
from gmovies.util import admin
from gmovies.util.id_encoder import decode_uuid, decoded_theater_id, encoded_theater_id
from gmovies.tx.payment import check_promo_code


log = logging.getLogger(__name__)

MAX_DISTANCE = 200000 # maximum distance for query points (more or less) [NB: we don't specify antipodal distance, and 200km should be reasonable].
DEFAULT_DIAMETER = 3000 # default diameter for computation of nearby theaters.
SPECIAL_FILTERS = ['diameter', 'sort', 'related']
IS_INEQUALITY_MATCHER = lambda v: v and (v[0] == '>' or v[0] == '<')
HAS_INEQUALITY_MATCHERS = lambda args: len(filter(IS_INEQUALITY_MATCHER, args.values())) > 0
ENTITY_ATTACHMENT_PARENT_MODEL = {'movies': models.Movie, 'theaters': models.Theater}
ENTITY_KEY_FUNC = {'movies': lambda e_id: Key(models.Movie, e_id), 'theaters': lambda e_id: to_theater_key(e_id)}
MGI_THEATER_CODES = ['LCT']


def resolve_dotted_name(cls, k):
    if hasattr(cls, 'filter_mappings') and k in cls.filter_mappings:
        k = cls.filter_mappings[k]

    parts = k.split('.')
    root = cls

    for part in parts:
        if not hasattr(root, part):
            return None

        root = getattr(root, part)

    return root

def to_key_instance(attr_name, v):
    attr_to_key_type = {'theater': to_theater_key, 'movie': lambda v: Key(models.Movie, v)}

    return attr_to_key_type[attr_name](v)

def to_theater_key(enc_theater_id):
    org_uuid, theater_id = decoded_theater_id(enc_theater_id)

    return Key(models.TheaterOrganization, str(org_uuid), models.Theater, theater_id)

def apply_sort(q, cls, sort_cols):
    for col in sort_cols:
        if col[0] == '-':
            prop = resolve_dotted_name(cls, col[1:])
            q = q.order(-prop)
        else:
            q = q.order(resolve_dotted_name(cls, col))

    return q

def coerce_val(cls, attr, k, v):
    if isinstance(attr, FloatProperty):
        return float(v)
    elif isinstance(attr, IntegerProperty):
        return int(v)
    elif (isinstance(attr, ComputedProperty) and k[:3] == 'is_') or isinstance(attr, BooleanProperty):
        v = v.lower()

        if v == 'true' or not v:
            return True
        elif v == 'false':
            return False
        else:
            raise BadValueException(k, v)
    elif isinstance(attr, DateProperty):
        return datetime.strptime(v, models.DATE_FORMAT)
    elif isinstance(attr, KeyProperty):
        return to_key_instance(k, v)
    else:
        return v

def check_filter_and_sort_args(cls, args):
    blacklist = [] if not hasattr(cls, 'blacklist') else cls.blacklist
    whitelist = SPECIAL_FILTERS

    if hasattr(cls, 'whitelist'):
        whitelist += cls.whitelist

    check = filter(lambda tup: (not tup[1] and tup[0] not in whitelist and tup[0] != 'sort') or tup[0] in blacklist,
            [(k, resolve_dotted_name(cls, k)) for k in args])

    log.info("check_filter_and_sort_args, filter args check: %s..." % check)

    if len(check) > 0:
        unknown_filter_args = [k for k,v in check]
        log.warn("ERROR!, check_filter_and_sort_args, unknown_filter_args check: %s..." % unknown_filter_args)

        raise InvalidFilterPropertyException([k for k, v in check])

    def val_typecheck((k, v)):
        log.info("check_filter_and_sort_args, val_typecheck, k: %s, v: %s..." % (k, v))

        try:
            attr = resolve_dotted_name(cls, k)

            if IS_INEQUALITY_MATCHER(v):
                if v[1] == '=':
                    coerce_val(cls, attr, k, v[2:])
                else:
                    coerce_val(cls, attr, k, v[1:])
            else:
                coerce_val(cls, attr, k, v)

            return False
        except Exception, e:
            log.warn("ERROR!, check_filter_and_sort_args, val_typecheck...")
            log.error(e)

            return True
            
    typecheck = filter(val_typecheck, [(k, args[k]) for k in args])

    log.info("check_filter_and_sort_args, filter args typecheck: %s..." % typecheck)

    if len(typecheck) > 0:
        typecheck_fail_args = [k for k, v in typecheck]
        log.warn("ERROR!, check_filter_and_sort_args, typecheck_fail_args: %s..." % typecheck_fail_args)

        raise BadValueException(typecheck_fail_args, 'Wrong type for filter.')

    # special case: filter condition is 'is_showing'; only accept one value to filter against.
    if 'is_showing' in args and len(args.getlist('is_showing')) != 1:
        raise BadValueException('is_showing', 'Too many filters.')

    if 'related' in args and cls != models.Schedule:
        raise InvalidOrderPropertyException(['related'])

    if 'sort' in args:
        sort_cols = [col if col[0] != '-' else col[1:] for col in args.getlist('sort')]
        check = filter(lambda tup: (not tup[1] and tup[0] not in whitelist) or tup[0] in blacklist,
                [(k, resolve_dotted_name(cls, k)) for k in sort_cols])

        if len(check) > 0:
            unknown_sort_args = [k for k, v in check]
            log.warn("ERROR!, check_filter_and_sort_args, unknown_sort_args check: %s..." % unknown_filter_args)

            raise InvalidOrderPropertyException(unknown_sort_args)

    ineq = filter(lambda vtup: IS_INEQUALITY_MATCHER(vtup[1]), args.items())
    ineq = set([kv[0] for kv in ineq])

    if len(ineq) > 1:
        log.warn("ERROR!, check_filter_and_sort_args, inequality filters against multiple properties: %s..." % ineq)

        raise InvalidFilterPropertyException(ineq)

def remove_keys(method_name, keys):
    log.debug("remove_keys, method_name: %s..." % method_name)

    for key in [nkey for nkey in keys]:
        log.debug("remove_keys, key: %s..." % key)

        if method_name == 'do_location_search':
            theater = key.get()
        elif method_name == 'do_get_schedules' or method_name == 'do_get_schedules_theaters':
            theater = key.parent().get()
        else:
            log.warn("ERROR!, remove_keys, method_name is not supported...")

            break

        if not theater:
            log.warn("SKIP!, remove_keys, theater not found...")

            continue

        theaterorg_key = theater.key.parent()
        theaterorg = theaterorg_key.get()

        if not theaterorg:
            log.warn("SKIP! remove_keys, theaterorg not found...")

            continue

        if not theater.is_published or not theaterorg.is_published:
            log.debug("remove_keys, removed key: %s..." % key)

            keys.remove(key)

    return keys

def do_computed_distance(latitude, longitude, theater):
    radius = 6371 # earth's radius in kilometers
    latitude = float(latitude)
    longitude = float(longitude)
    theater_latitude = float(theater.location.lat)
    theater_longitude = float(theater.location.lon)
    longitude1, latitude1, longitude2, latitude2 = map(radians, [longitude, latitude, theater_longitude, theater_latitude])
    dlongitude = longitude2 - longitude1
    dlatitude = latitude2 - latitude1
    a = (sin(dlatitude/2))**2 + cos(latitude1) * cos(latitude2) * (sin(dlongitude/2))**2
    c = 2 * atan2(sqrt(a), sqrt(1-a))
    distance = radius * c

    return distance

def do_get_distance(latitude, longitude, theater_keys, request):
    results = []
    diameter = DEFAULT_DIAMETER

    if 'diameter' in request.args:
        diameter = request.args['diameter']

    for theater in get_multi(theater_keys):
        if not theater:
            log.warn("SKIP!, do_get_distance, theater not found...")

            continue

        distance = do_computed_distance(latitude, longitude, theater)
        distance_in_meters = distance * float(1000)

        if round(distance_in_meters) < float(diameter):
            theater_dict = theater.to_entity2()
            theater_dict['distance'] = distance
            results.append({'id': encoded_theater_id(theater.key), 'theater': theater_dict})

    return results

def do_location_search(request):
    # TODO: assert that only args are location-related; we can't apply additional filters as well.
    lat = request.args['location.lat']
    lon = request.args['location.lon']
    diameter = DEFAULT_DIAMETER

    if 'diameter' in request.args:
        diameter = request.args['diameter']

    # naive caching via geohash + diamter.
    cache_prefix = geohash.encode(float(lat), float(lon))
    cache_key = "geo::%s::%s" % (cache_prefix, diameter)
    cache_hit = memcache.get(cache_key)

    if cache_hit:
        log.debug("do_location_search, geolocation cache hit on: %s..." % cache_key)

        return cache_hit

    dist_fn = "distance(geopoint(%s,%s), loc)" % (lat, lon)
    q_str = "%s < %s" % (dist_fn, diameter)
    log.debug("do_location_search, geolocation query: %s..." % q_str)
    idx = search.Index(name=THEATER_GEO_INDEX_NAME)
    sort_expr = search.SortExpression(expression=dist_fn, direction=search.SortExpression.ASCENDING, default_value=MAX_DISTANCE)
    q_opt = search.QueryOptions(sort_options=search.SortOptions(expressions=[sort_expr]))
    q = search.Query(query_string=q_str, options=q_opt)

    try:
        key_docs = idx.search(q)
        log.debug("do_location_search, key_docs: %s..." % key_docs)
        theater_keys = [to_theater_key(doc.doc_id) for doc in key_docs]
        theater_keys = remove_keys('do_location_search', theater_keys)
        memcache.set(cache_key, (lat, lon, theater_keys))

        return lat, lon, theater_keys
    except (search.Error), e:
        log.warn("ERROR!, do_location_search, search error...")
        log.error(e)

        return lat, lon, []
    except Exception, e:
        log.warn("ERROR!, do_location_search...")
        log.error(e)

        return lat, lon, []

def do_filter(cls, q, args):
    f = {}
    special_case_sort = False

    for k in args:
        if k != 'sort' and k != 'related':
            if 'match' not in f:
                f['match'] = {}

            name = k.split('.')[-1]
            vals = args.getlist(k)
            prop = resolve_dotted_name(cls, k)

            log.info("do_filter, filter: %s (against %s)..." % (prop, vals))

            if cls == models.Movie:
                if k == "resolution" or k == "application_type":
                    if len(vals) > 1:
                        f['match'][k] = vals
                    else:
                        f['match'][k] = vals[0]

                    continue

            # check if any of the values specified is prefixed with an inequality matcher.
            if len(filter(IS_INEQUALITY_MATCHER, vals)) > 0:
                q = q.order(prop) # for inequalities, there is always an implicit sort applied to property being mapped as inequal.

                for v in vals:
                    if v[:2] == '>=':
                        log.debug("do_filter, inequality: >= %s..." % k)

                        q = q.filter(prop >= coerce_val(cls, prop, name, v[2:]))
                    elif v[:2] == '<=':
                        log.debug("do_filter, inequality: <= %s..." % k)

                        q = q.filter(prop <= coerce_val(cls, prop, name, v[2:]))
                    elif v[:1] == '>' and v[:2] != '>=':
                        log.debug("do_filter, inequality: > %s..." % k)

                        q = q.filter(prop > coerce_val(cls, prop, name, v[1:]))
                    elif v[:1] == '<' and v[:2] != '<=':
                        log.debug("do_filter, inequality: < %s..." % k)

                        q = q.filter(prop < coerce_val(cls, prop, name, v[1:]))
                    else:
                        log.warn("ERROR!, do_filter, we got an inequality matcher that doesn't match...")

                        raise APISystemError("Strange, we got an inequality matcher that doesn't match.")
            else:
                q = q.filter(prop.IN([coerce_val(cls, prop, name, v) for v in vals]))

            if len(vals) > 1:
                f['match'][k] = vals
            else:
                f['match'][k] = vals[0]

    if 'sort' in args:
        sort_cols = args.getlist('sort')
        q = apply_sort(q, cls, sort_cols)
        f['sort'] = sort_cols

    return f, q

def query_filters(cls, request):
    if cls == models.Theater:
        f = {}
        q = []
        args = request.args.copy()
        theaterorgs = models.TheaterOrganization.query(models.TheaterOrganization.is_published==True)

        for org in theaterorgs:
            theaters = cls.query(cls.is_published==True, ancestor=org.key)
            f, theaters = do_filter(cls, theaters, request.args)
            q.extend(theaters.fetch())

        if 'sort' in args:
            sort_cols = args.getlist('sort')

            if 'name' in sort_cols:
                q = sorted(q, key=attrgetter('name'))
    elif cls == models.Movie:
        q = cls.query(cls.is_inactive==False)
        f, q = do_filter(cls, q, request.args)
    else:
        q = cls.query()
        f, q = do_filter(cls, q, request.args)

    return f, q

def query_model(cls, entity_name, request, id_encoder=lambda x:x.id()):
    check_filter_and_sort_args(cls, request.args)
    f, q = query_filters(cls, request)

    if cls == models.Movie:
        posters_args = {}
        posters_args['resolution'] = request.args.get('resolution', 'uploaded')
        posters_args['application_type'] = request.args.get('application_type', 'mobile-app')
        portrait_default_poster, landscape_default_poster = get_default_poster_url(posters_args=posters_args)

        l = [{'id': id_encoder(x.key), entity_name: x.to_entity2(posters_args, portrait_default_poster, landscape_default_poster)} for x in q]
    else:
        l = [{'id': id_encoder(x.key), entity_name: x.to_entity2()} for x in q]

    return f, l

def query_model_ids(cls, entity_name, request, id_encoder=lambda x:x.id()):
    check_filter_and_sort_args(cls, request.args)

    f, q = query_filters(cls, request)
    l = [id_encoder(x.key) for x in q]

    return f, l

def strip_unmatched_slots(results, movie_keys):
    for sched in results:
        filt_slots = filter(lambda s: s.movie in movie_keys, sched.slots)
        sched.slots = filt_slots

    return filter(lambda sched: len(sched.slots) > 0, results)

def strip_unmatched_slots_seatingtype(seating_type, results):
    for sched in results:
        filt_slots = filter(lambda s: s.seating_type == seating_type, sched.slots)
        sched.slots = filt_slots

    return filter(lambda sched: len(sched.slots) > 0, results)

def get_schedule_query_filter_keyset(theaterorg_id, theater_pair, movie_id, args):
    movie_keys = []
    theater_keys = []

    if theaterorg_id:
        theaterorg_key = Key(models.TheaterOrganization, str(theaterorg_id))

        if not theaterorg_key.get():
            log.warn("ERROR! get_schedule_query_filter_keyset, theaterorg_id, theaterorg not found...")

            raise NotFoundException(details={'theaterorg_id': theaterorg_id})

        theater_keys = models.Theater.query(ancestor=theaterorg_key).fetch(keys_only=True)

    if theater_pair:
        org_uuid = decode_uuid(theater_pair[0])
        theater_key = Key(models.TheaterOrganization, str(org_uuid), models.Theater, theater_pair[1])

        if not theater_key.get():
            log.warn("ERROR! get_schedule_query_filter_keyset, theater_pair, theater not found...")

            raise NotFoundException(details={'theaterorg_id': str(org_uuid), 'theater_id': theater_pair[1]})

        if 'theater' in args:
            log.warn("ERROR!, get_schedule_query_filter_keyset, theater_pair, not allowed theater in args...")

            raise BadValueException('theater', 'Additional theater filter not allowed here.')

        if 'location.lon' in args:
            log.warn("ERROR!, get_schedule_query_filter_keyset, theater_pair, not allowed location.lon in args...")

            raise BadValueException('theater', 'Additional location.lon filter not allowed here.')

        if 'location.lat' in args:
            log.warn("ERROR!, get_schedule_query_filter_keyset, theater_pair, not allowed location.lat in args...")

            raise BadValueException('theater', 'Additional location.lat filter not allowed here.')

        theater_keys = [theater_key]

    if 'theater' in args:
        if 'location.lon' in args:
            log.warn("ERROR!, get_schedule_query_filter_keyset, theater in args, not allowed location.lon in args...")

            raise BadValueException('theater', 'Additional location.lon filter not allowed here.')

        if 'location.lat' in args:
            log.warn("ERROR!, get_schedule_query_filter_keyset, theater in args, not allowed location.lat in args...")

            raise BadValueException('theater', 'Additional location.lat filter not allowed here.')

        theater_keys = [to_theater_key(t_id) for t_id in args.getlist('theater')]

    if 'location.lon' in args and 'location.lat' in args:
        if 'theater' in args:
            log.warn("ERROR!, get_schedule_query_filter_keyset, lon and lat in args, not allowed theater in args...")

            raise BadValueException('theater', 'Additional theater filter not allowed here.')

        lat, lon, theater_keys = do_location_search(request)

    if movie_id:
        movie_key = Key(models.Movie, movie_id)

        if not movie_key.get():
            log.warn("ERROR! get_schedule_query_filter_keyset, movie_id, movie not found...")

            raise NotFoundException(details={'movie_id': movie_id})

        if 'movie' in args:
            log.warn("ERROR!, get_schedule_query_filter_keyset, movie_id, movie in args...")

            raise BadValueException('movie', 'Additional movie filter not allowed here.')

        movie_keys = [movie_key]

    if 'movie' in args:
        movie_keys = [Key(models.Movie, m_id) for m_id in args.getlist('movie')]

    # pop additional parameters if existing.
    args.pop('movie', None)
    args.pop('theater', None)
    args.pop('diameter', None)
    args.pop('location.lon', None)
    args.pop('location.lat', None)

    return theater_keys, movie_keys

def do_get_schedules(theaterorg_id=None, theater_pair=None, movie_id=None):
    check_filter_and_sort_args(models.Schedule, request.args)

    f = {}
    result_keys = []
    args = request.args.copy()
    special_case_sort = False

    if 'sort' in args:
        sort_cols = args.getlist('sort')
        special_case_sort = sort_cols and (('theater' in sort_cols) or HAS_INEQUALITY_MATCHERS(args))

    if special_case_sort and 'sort' in args:
        args.pop('sort')

    theater_keys, movie_keys = get_schedule_query_filter_keyset(theaterorg_id, theater_pair, movie_id, args)

    if theater_keys:
        for t in theater_keys:
            base_q = models.Schedule.query(ancestor=t)

            if movie_keys:
                if len(movie_keys) > 1:
                    base_q = base_q.filter(models.Schedule.slots.movie.IN(movie_keys))
                else:
                    base_q = base_q.filter(models.Schedule.slots.movie==movie_keys[0])

            f, q = do_filter(models.Schedule, base_q, args)
            result_keys.extend(q.fetch(keys_only=True))
    else:
        q = models.Schedule.query()

        if movie_keys:
            if len(movie_keys) > 1:
                q = q.filter(models.Schedule.slots.movie.IN(movie_keys))
            else:
                q = q.filter(models.Schedule.slots.movie==movie_keys[0])

        f, q = do_filter(models.Schedule, q, args)
        result_keys = q.fetch(keys_only=True)

    result_keys = remove_keys('do_get_schedules', result_keys)
    results = get_multi(result_keys)

    if movie_keys:
        results = strip_unmatched_slots(results, movie_keys)

    if special_case_sort:
        log.debug("do_get_schedules, special case triggered: manually sorting results...")

        # populate special: theater.
        for s in results:
            s.theater = s.key.parent().get().name

        results = sorted(results, key=attrgetter(*sort_cols))
        f['sort'] = sort_cols

    # since Schedule.to_entities() returns a list of API entities that a particular
    # schedule may map to, we will need to flatten it before passing to clients.
    l = []
    rel_key = None

    # strip unmatched slots seating_type in results.
    if 'seating_type' in request.args:
        log.debug("do_get_schedules, strip_unmatched_slots_seatingtype...")

        seating_type = request.args['seating_type']
        results = strip_unmatched_slots_seatingtype(seating_type, results)

    # get nearby schedules only.
    if 'location.lat' in request.args and 'location.lon' in request.args:
        log.debug("do_get_schedules, nearby schedules...")

        lat = request.args['location.lat']
        lon = request.args['location.lon']
        diameter = DEFAULT_DIAMETER

        if 'diameter' in request.args:
            diameter = request.args['diameter']

        for s in results:
            distance = do_computed_distance(lat, lon, s.key.parent().get())
            distance_in_meters = distance * float(1000)

            if round(distance_in_meters) < float(diameter):
                for s_id, s_ent in s.to_entities():
                    s_dict = {}
                    s_dict['id'] = s_id
                    s_dict['schedule'] = s_ent
                    s_dict['distance'] = distance

                    l.append(s_dict)

    if not l:
        log.debug("do_get_schedules, schedules...")

        l = [{'id': s_id, 'schedule': s_ent} for s in results for s_id, s_ent in s.to_entities()]

    if 'related' in request.args and (theater_pair or movie_id):
        rel_key = 'movie' if theater_pair else 'theater'
        rel = list(set([s['schedule'][rel_key] for s in l]))

        return jsonify(filters=f, results=l, related=rel)

    return jsonify(filters=f, results=l)

def do_get_schedules_theaters(theaterorg_id=None, theater_pair=None, movie_id=None):
    check_filter_and_sort_args(models.Schedule, request.args)

    f = {}
    result_keys = []
    args = request.args.copy()
    special_case_sort = False

    if 'sort' in args:
        sort_cols = args.getlist('sort')
        special_case_sort = sort_cols and (('theater' in sort_cols) or HAS_INEQUALITY_MATCHERS(args))

    if special_case_sort and 'sort' in args:
        args.pop('sort')

    theater_keys, movie_keys = get_schedule_query_filter_keyset(theaterorg_id, theater_pair, movie_id, args)

    if theater_keys:
        for t in theater_keys:
            base_q = models.Schedule.query(ancestor=t)

            if movie_keys:
                if len(movie_keys) > 1:
                    base_q = base_q.filter(models.Schedule.slots.movie.IN(movie_keys))
                else:
                    base_q = base_q.filter(models.Schedule.slots.movie==movie_keys[0])

            f, q = do_filter(models.Schedule, base_q, args)
            result_keys.extend(q.fetch(keys_only=True))
    else:
        q = models.Schedule.query()

        if movie_keys:
            if len(movie_keys) > 1:
                q = q.filter(models.Schedule.slots.movie.IN(movie_keys))
            else:
                q = q.filter(models.Schedule.slots.movie==movie_keys[0])

        f, q = do_filter(models.Schedule, q, args)
        result_keys = q.fetch(keys_only=True)

    result_keys = remove_keys('do_get_schedules_theaters', result_keys)
    results = get_multi(result_keys)

    if movie_keys:
        results = strip_unmatched_slots(results, movie_keys)

    if special_case_sort:
        log.debug("do_get_schedules, special case triggered: manually sorting results...")

        # populate special: theater.
        for s in results:
            s.theater = s.key.parent().get().name

        results = sorted(results, key=attrgetter(*sort_cols))
        f['sort'] = sort_cols

    # strip unmatched slots seating_type in results.
    if 'seating_type' in request.args:
        log.debug("do_get_schedules, strip_unmatched_slots_seatingtype...")

        seating_type = request.args['seating_type']
        results = strip_unmatched_slots_seatingtype(seating_type, results)

    rel_key = None
    l = list(set([s._to_entity_parent() for s in results]))

    if 'related' in request.args and (theater_pair or movie_id):
        rel_key = 'movie' if theater_pair else 'theater'
        rel = list(set([s['schedule'][rel_key] for s in l]))

        return jsonify(filters=f, results=l, related=rel)

    return jsonify(filters=f, results=l)

def get_default_poster_url(posters_args={}):
    portrait_default_poster = ''
    landscape_default_poster = ''
    resolution = posters_args.get('resolution', 'uploaded')
    application_type = posters_args.get('application_type', 'mobile-app')
    cache_key_portrait = "movie::default::poster::portrait::%s::%s" % (resolution, application_type)
    cache_key_landscape = "movie::default::poster::landscape::%s::%s" % (resolution, application_type)

    try:
        cache_hit_portrait = memcache.get(cache_key_portrait)

        if cache_hit_portrait:
            log.debug("get_default_poster_url, portrait, cache hit on: %s..." % cache_key_portrait)

            portrait_default_poster = cache_hit_portrait
            if portrait_default_poster and portrait_default_poster[-3:] != '=s0':
                portrait_default_poster += "=s0"
        else:
            portrait_poster_image_blob = models.PosterImageBlob.query(models.PosterImageBlob.resolution==resolution,
                    models.PosterImageBlob.orientation=='portrait', models.PosterImageBlob.application_type==application_type,
                    models.PosterImageBlob.is_default==True)

            if portrait_poster_image_blob.count() != 0:
                portrait_poster_image = portrait_poster_image_blob.get()
                portrait_default_poster = get_serving_url(portrait_poster_image.image)
                if portrait_default_poster and portrait_default_poster[-3:] != '=s0':
                    portrait_default_poster += "=s0"

                memcache.set(cache_key_portrait, portrait_default_poster)
    except Exception, e:
        log.warn("ERROR!, get_default_poster_url, portrait...")
        log.error(e)

    try:
        cache_hit_landscape = memcache.get(cache_key_landscape)

        if cache_hit_landscape:
            log.debug("get_default_poster_url, landscape, cache hit on: %s..." % cache_key_landscape)

            landscape_default_poster = cache_hit_landscape
            if landscape_default_poster and landscape_default_poster[-3:] != '=s0':
                landscape_default_poster += "=s0"
        else:
            landscape_poster_image_blob = models.PosterImageBlob.query(models.PosterImageBlob.resolution==resolution,
                    models.PosterImageBlob.orientation=='landscape', models.PosterImageBlob.application_type==application_type,
                    models.PosterImageBlob.is_default==True)

            if landscape_poster_image_blob.count() != 0:
                landscape_poster_image = landscape_poster_image_blob.get()
                landscape_default_poster = get_serving_url(landscape_poster_image.image)
                if landscape_default_poster and landscape_default_poster[-3:] != '=s0':
                    landscape_default_poster += "=s0"

                memcache.set(cache_key_landscape, landscape_default_poster)
    except Exception, e:
        log.warn("ERROR!, get_default_poster_url, landscape...")
        log.error(e)

    return portrait_default_poster, landscape_default_poster

# HACK: ensure we don't allow lookups for theaters with wrong encoded parts.
def theaterorg_theater_safety_check(theaterorg_id, ignored, theater_id):
    if str(decode_uuid(ignored)) != theaterorg_id:
        raise NotFoundException(details={'theaterorg_id': theaterorg_id, 'theater_id': "%s~%s" % (ignored, theater_id)})

def org_with_theaters(org):
    theater_keys = models.Theater.query(models.Theater.is_published==True,
            ancestor=org.key).order(models.Theater.name).fetch(keys_only=True)
    theaters = [encoded_theater_id(theater_key) for theater_key in theater_keys]
    entity = org.to_entity()
    entity['theaters'] = theaters

    return entity

def get_theater_info_common2(org_uuid, enc_org_uuid, theater_id):
    theater = Key(models.TheaterOrganization, org_uuid, models.Theater, theater_id).get()

    if not theater:
        log.warn("ERROR!, get_theater_info_common2, theater not found...")

        raise NotFoundException(details={'theater_id': "%s~%s" % (enc_org_uuid, theater_id)})

    return jsonify({'id': encoded_theater_id(theater.key), 'theater': theater.to_entity2()})

def get_theater_seatmap_common(org_uuid, enc_org_uuid, theater_id, cinema_name):
    theater = Key(models.TheaterOrganization, org_uuid, models.Theater, theater_id).get()

    if not theater:
        log.warn("ERROR!, get_theater_seatmap_common, theater not found...")

        raise NotFoundException(details={'theater_id': "%s~%s" % (enc_org_uuid, theater_id)})

    return jsonify({'id': encoded_theater_id(theater.key), 'theater': theater.to_entity_seatmap(cinema_name)})

def get_theater_paymentoptions_common(org_uuid, enc_org_uuid, theater_id, platform, claim_code=None):
    payment_options = []
    payment_option_identifiers = []
    theater = Key(models.TheaterOrganization, org_uuid, models.Theater, theater_id).get()

    if not theater:
        log.warn("ERROR!, get_theater_paymentoptions_common, theater not found...")

        raise NotFoundException(details={'theater_id': "%s~%s" % (enc_org_uuid, theater_id)})

    if platform.lower() == 'ios':
        payment_option_identifiers = theater.payment_options
    elif platform.lower() == 'android':
        payment_option_identifiers = theater.payment_options_android
    elif platform.lower() == 'website':
        payment_option_identifiers = theater.payment_options_website

    for identifier in payment_option_identifiers:
        payment_option_key = ndb.Key(models.PaymentOptionSettings, identifier)
        payment_option = payment_option_key.get()

        if not payment_option:
            log.warn("SKIP!, not existing payment_option_identifier, %s..." % identifier)
            continue

        if platform.lower() in ['ios', 'android'] and claim_code is not None:
            if payment_option.key.id() != 'claim_code':
                log.warn("SKIP!, return only claim_code payment option, %s..." % payment_option.key.id())
                continue
            else:
                # Add checking if PROMO_CODE is valid in theater
                status_code, code_details = check_promo_code(claim_code, str(org_uuid), theater_id)
                if status_code != 200:
                    log.warn("ERROR!, get_theater_paymentoptions_common, check_promo_code, status_code not equal to 200: %s..." % (status_code))
                    raise NotFoundException(details={'claim_code': "%s" % (claim_code)})
                if 'is_theater_valid' not in code_details['return']['claim_code_details']:
                    log.warn("ERROR!, get_theater_paymentoptions_common, missing key 'is_theater_valid' in json response...")
                    raise NotFoundException(details={'claim_code': "%s" % (claim_code)})

                is_theater_valid = code_details['return']['claim_code_details']['is_theater_valid']
                if not is_theater_valid:
                    log.warn("SKIP!, claim_code %s ,not allowed in theater..." % claim_code)
                    continue

        payment_options.append({'id': payment_option.key.id(), 'payment_option': payment_option.to_entity(theater=theater)})

    theater_entity = theater.to_entity2()
    theater_entity['payment_options'] = payment_options
    theater_entity['allow_reservation'] = True if platform.lower() in theater_entity['allow_reservation_platform'] else False

    theater_entity.pop('address', None)
    theater_entity.pop('allow_discount_code', None)
    theater_entity.pop('allow_reservation_platform', None)
    theater_entity.pop('cinemas', None)
    theater_entity.pop('error_message', None)
    theater_entity.pop('error_title', None)
    theater_entity.pop('link_name', None)
    theater_entity.pop('location', None)
    theater_entity.pop('payment_gateway', None)
    theater_entity.pop('payment_options_android', None)
    theater_entity.pop('payment_options_website', None)
    theater_entity.pop('theater_image', None)
    theater_entity.pop('theaterorg_id', None)
    theater_entity.pop('org_theater_code', None)

    return jsonify({'id': encoded_theater_id(theater.key), 'theater': theater_entity})

def do_parse_comingsoon(queryset, id_encoder=lambda x:x.id()):
    movie_list = []
    not_sorted_movies = []
    posters_args = {}
    posters_args['resolution'] = request.args.get('resolution', 'uploaded')
    posters_args['application_type'] = request.args.get('application_type', 'mobile-app')
    portrait_default_poster, landscape_default_poster = get_default_poster_url(posters_args=posters_args)

    for ent in queryset:
        month_year_to_string = admin.parse_date_to_string(ent.release_date, fdate='%m %Y')

        if month_year_to_string:
            movie_tuple = (month_year_to_string, {'id': id_encoder(ent.key), 'movie': ent.to_entity_comingsoon(posters_args=posters_args,
                    portrait_default_poster=portrait_default_poster, landscape_default_poster=landscape_default_poster)})
            not_sorted_movies.append(movie_tuple)

    sorted_movies = sorted(not_sorted_movies, key=itemgetter(0))
    movie_groups = groupby(sorted_movies, key=itemgetter(0))

    for key, value in movie_groups:
        month_year_to_date = admin.parse_string_to_date(key, fdate='%m %Y')
        month_year_to_string = admin.parse_date_to_string(month_year_to_date, fdate='%B %Y')

        if month_year_to_date and month_year_to_string:
            movie_list.append({'month': month_year_to_string, 'movies': [movie[1] for movie in value]})

    return movie_list

def get_movie_status(filters):
    is_showing = False
    is_expired = False
    is_nowshowing = False
    is_comingsoon = False

    if 'match' in filters:
        if 'is_showing' in filters['match'] and 'is_expired' in filters['match']:
            if filters['match']['is_showing'].lower() == 'true' or not filters['match']['is_showing']:
                is_showing = True

            if filters['match']['is_expired'].lower() == 'true' or not filters['match']['is_expired']:
                is_expired = True

    if is_showing and not is_expired:
        is_nowshowing = True
    elif not is_showing and not is_expired:
        is_comingsoon = True

    return is_nowshowing, is_comingsoon