import base64
import datetime
import logging
import re
import sys
import time

from itertools import groupby
from operator import attrgetter

from google.appengine.api import taskqueue
from google.appengine.ext.ndb import Key, put_multi
from google.appengine.runtime.apiproxy_errors import OverQuotaError

from flask import Blueprint, jsonify, redirect, url_for, request, make_response, json, Response

from gmovies import models, tx, orgs, settings
from gmovies.admin import notify_admin
from gmovies.exceptions.api import *
from gmovies.util.id_encoder import decode_uuid


log = logging.getLogger(__name__)
migs_cb = Blueprint('migs', __name__, template_folder='callback_templates')


@migs_cb.errorhandler(APIException)
def handle_api_error(e):
    res = jsonify(message=e.message, error_code=e.error_code, details=e.details)
    res.status_code = e.code

    return res

@migs_cb.errorhandler(OverQuotaError)
def handle_quota_error(e):
    res = jsonify(message='The backend has reached a resource quota. Please try again later.',  error_code='SYSTEM_OVER_QUOTA', details=e.message)
    res.status_code = 500
    notify_admin("MIGS Callback URL", e, sys.exc_info())

    return res

@migs_cb.errorhandler(Exception)
def handle_system_error(e):
    res = jsonify(message='There has been a system error. Please try again later.', error_code='SYSTEM_GENERIC_ERROR', details=e.message)
    log.exception(e)
    res.status_code = 500
    notify_admin("MIGS Callback URL", e, sys.exc_info())

    return res

@migs_cb.route('/<channel_name>', methods=['GET'])
def get_event(channel_name):
    log.info("Channel: %s", channel_name)
    log.info("Headers: {}".format(request.headers))

    def to_event(channel_name, event_dict, timestamp=None):
        event = models.Event()
        event.event_channel = channel_name
        event.org_id = event_dict['vpc_MerchTxnRef']
        event.payload = event_dict

        if timestamp:
            event.timestamp = timestamp

        return event

    if not request.args:
        log.warn("ERROR!, MIGS, get_event, has no request parameters...")

        raise BadValueException('body', 'You must supply request parameters.')

    event_dict = {}

    for key, value in request.args.items():
        log.info("MIGS, callback, add key and value pair, %s %s..." % (key, value))

        event_dict[key] = value

    # try:
        # theater_code = event_dict['vpc_MerchTxnRef'].split('~')[0]
        # theater = models.Theater.query(models.Theater.is_published==True, models.Theater.org_theater_code==theater_code).get()
        # org_key = theater.key.parent()
    # except Exception, e:
        # log.warn("ERROR!, MIGS, get_event, something went wrong in getting theater and org_key...")
        # log.error(e)

        # raise NotFoundException(details={'vpc_MerchTxnRef': event['vpc_MerchTxnRef']})

    # TODO: validate hashSecure if we will have html page to show the returnURL parameters...

    namespace = 'migs'
    response_code = event_dict['vpc_TxnResponseCode'] if 'vpc_TxnResponseCode' in event_dict else ''
    response_message = event_dict['vpc_Message'] if 'vpc_Message' in event_dict else ''

    if 'vpc_TxnResponseCode' in event_dict and event_dict['vpc_TxnResponseCode'] in ['?', '3', 'M']:
        log.warn("SKIP!, MIGS, get_event, vpc_TxnResponseCode: %s..." % event_dict['vpc_TxnResponseCode'])

        return redirect(url_for('response_url.get_response_url', namespace=namespace,
                response_code=response_code, response_message=response_message))

    listeners = models.Listener.get_listeners(channel_name)
    batch = [to_event(channel_name, event_dict)]

    put_multi(batch)

    log.info("MIGS, get_event, batch events: {}...".format(batch))
    log.info("MIGS, get_event, triggering %d listeners..." % listeners.count())

    if listeners.count() != 0:
        version_key = attrgetter('version')
        by_version = groupby(sorted(listeners, key=version_key), version_key)

        for version, _ in by_version:
            log.info("MIGS, get_event, triggering callback listener for channel: %s, version: %s..." % (channel_name, version))

            taskqueue.add(url=url_for('task.trigger_callback_listeners'), queue_name='transactions',
                    target=version, params={'channel_name': channel_name})

    return redirect(url_for('response_url.get_response_url', namespace=namespace,
            response_code=response_code, response_message=response_message))
