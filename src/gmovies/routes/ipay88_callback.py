import sys
import logging
log = logging.getLogger(__name__)

from itertools import groupby
from operator import attrgetter
import time

from google.appengine.ext.ndb import Key, put_multi
from google.appengine.api import taskqueue

from flask import Blueprint, jsonify, redirect, url_for, request, make_response, json, Response

ipay88 = Blueprint('ipay88', __name__, template_folder='ipay88_templates')

from gmovies import models, tx, orgs, settings
from gmovies.admin import notify_admin
from gmovies.exceptions.api import *

from .auth import authenticate_request

from google.appengine.runtime.apiproxy_errors import OverQuotaError

@ipay88.errorhandler(APIException)
def handle_api_error(e):
    res = jsonify(message=e.message, error_code=e.error_code, details=e.details)
    res.status_code = e.code
    return res

@ipay88.errorhandler(OverQuotaError)
def handle_quota_error(e):
    res = jsonify(message='The backend has reached a resource quota. Please try again later.', 
                  error_code='SYSTEM_OVER_QUOTA', details=e.message)
    res.status_code = 500
    notify_admin(e, sys.exc_info())
    return res

@ipay88.errorhandler(Exception)
def handle_system_error(e):
    res = jsonify(message='There has been a system error. Please try again later.',
                  error_code='SYSTEM_GENERIC_ERROR', details=e.message)
    log.exception(e)
    res.status_code = 500
    notify_admin(e, sys.exc_info())
    return res

#probably not needed for now
#@ipay88.before_request
#def check_callback_auth():
#    log.debug('Entered check_callback_auth()')
    #SANDBOX FOR TESTING
#    if request.base_url != settings.IPAY88_PAYMENT_URL_SANDBOX: 
#        log.error('Failed auth check')
#        raise ClientAuthException('Invalid caller url')

@ipay88.route('/<channel_name>', methods=['POST', 'GET'])
def post_event(channel_name):
    log.info("Channel: %s", channel_name)

    if channel_name == 'backend-response':
        log.info("Responding with RECEIVEOK")
        return make_response('RECEIVEOK', 200) 

    log.info("Headers: {}".format(request.headers))

    body = request.values
    log.info("Values: {}".format(body))
    has_body = True if body else False
    log.info("Has Body:{}".format(has_body))
    if not body:
        log.debug("Did not get body")
        raise BadValueException('body', 'You must supply body info')
    
    log.info("Getting events")

    event = {}

    org_refno = request.form["RefNo"]
    try:
        tcode, refno = org_refno.split('~')
    except IndexError:
        log.info("Can't obtain theater code and reference number")
    
    event["MerchantCode"]  = request.form["MerchantCode"]
    event["PaymentId"]     = request.form["PaymentId"]
    #event["RefNo"]         = refno 
    event["RefNo"]         = request.form["RefNo"] 
    event["Amount"]        = request.form["Amount"]
    event["Currency"]      = request.form["Currency"]
    event["Remark"]        = request.form["Remark"]
    event["TransId"]       = request.form["TransId"]
    event["AuthCode"]      = request.form["AuthCode"]
    event["Status"]        = request.form["Status"]
    event["ErrDesc"]       = request.form["ErrDesc"]
    event["Signature"]     = request.form["Signature"]

    log.info("Events Details: {}".format(event))

    try:
        t = models.Theater.query_by_code(tcode)
        org_key = t[0].key.parent()
    except Exception, e:
        log.warn("Error getting Theater Organization!!!")

    # FIXME: Hardcoded for Rockwell
    #org_uuid = str(orgs.ROCKWELL_MALLS)

    #log.info("Org: %s", org_uuid)
    #org_key = Key(models.TheaterOrganization, org_id)
    #org_key = Key(models.TheaterOrganization, org_uuid)
    org = org_key.get()
    log.info("Found {} Theater Organization".format(org.name))

    
    def to_event(e):
        ev = models.Event(parent=org_key)
        ev.event_channel = channel_name
        ev.org_id = "Ipay88"
        ev.timestamp = int(time.time())
        ev.payload = event

        return ev

    batch = [ to_event(event) ]
    put_multi(batch)
    log.info("Batch events: {}".format(batch))

    listeners = models.Listener.get_listeners(channel_name)
    listener_count = listeners.count()
    if listener_count != 0:
        version_key = attrgetter('version')
        by_version = groupby(sorted(listeners, key=version_key),
                                   version_key)
        log.debug("Triggering %d listeners", listener_count)
        for version,_ in by_version:
            log.info("Triggering callback listener for channel: {0} version {1}".format(channel_name, version))
            taskqueue.add(url=url_for('task.trigger_callback_listeners'),
                          queue_name='transactions',
                          target=version,
                          params={ 'channel_name': channel_name })

    #res = Response(status='204 No Content')
    #res.status_code=204
    #return make_response(res)
    log.info("Got Payment response from Ipay88. Responding with RECEIVEOK")
    return make_response('RECEIVEOK', 200)