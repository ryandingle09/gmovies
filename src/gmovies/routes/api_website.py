import logging
import sys
import time
import traceback

from datetime import date, datetime

from flask import jsonify, redirect, render_template, request, Blueprint

from google.appengine.api.images import get_serving_url
from google.appengine.ext import ndb
from google.appengine.ext.ndb import (get_multi, transactional, AND, OR, Key, GenericProperty,
        FloatProperty, IntegerProperty, ComputedProperty, BooleanProperty, DateProperty, KeyProperty)
from google.appengine.runtime.apiproxy_errors import OverQuotaError

from gmovies import models, orgs, tx
from gmovies.admin import notify_admin
from gmovies.exceptions.api import *
from gmovies.feeds.available_seats import get_available_seats
from gmovies.routes.auth import client_authenticate_request
from gmovies.routes.deprecation import deprecated
from gmovies.routes.util import (apply_sort, check_filter_and_sort_args, do_filter, do_get_distance, do_get_schedules,
        do_get_schedules_theaters, do_location_search, do_parse_comingsoon, get_default_poster_url, get_movie_status,
        get_theater_info_common2, get_theater_seatmap_common, get_theater_paymentoptions_common, org_with_theaters,
        query_model, query_model_ids, resolve_dotted_name, theaterorg_theater_safety_check, to_theater_key,
        MGI_THEATER_CODES)
from gmovies.settings import API_VERSIONS_DEPRECATED, ENFORCE_AUTH_WEBSITE, LCT_MERCHANT_KEY
from gmovies.tx import promo
from gmovies.util import admin
from gmovies.util.id_encoder import decode_uuid, encode_uuid, encoded_theater_id, decoded_theater_id
from gmovies.tx.payment import is_3d_token_paynamics
from gmovies.tx.actions import refund, update_status_by_qrcode
from gmovies.orgs import ROBINSONS_MALLS


log = logging.getLogger(__name__)
api_website = Blueprint('api_website', __name__, template_folder='api_templates')

DEVICE_ID = 'X-GMovies-DeviceId'
AUTH_CLIENT_ID = 'X-GMovies-Auth-ClientId'
AUTH_TIMESTAMP = 'X-GMovies-Auth-Timestamp'
AUTH_SIGNATURE = 'X-GMovies-Auth-Signature'

API_VERSION = 'APIWEBSITE0'


def abort(status_code, message='Error', error_code='ERROR_UNKNOWN', details=None, headers={}):
    raise APIException(status_code, message, error_code, details)

@transactional
def _start_tx(device_id, tx_info):
    transaction = tx.start(device_id, tx_info)
    tx.reschedule(transaction)

    return transaction

@transactional
def _start_tx_stopgap(device_id, tx_info):
    transaction = tx.start_stopgap(device_id, tx_info)
    tx.reschedule(transaction)

    return transaction

@transactional
def _start_tx_promo(device_id, tx_info):
    transaction = promo.start(device_id, tx_info)
    promo.reschedule(transaction)

    return transaction


###############
#
# Error and Before Request Handlers
#
###############

@api_website.errorhandler(AlreadyCancelledException)
def handle_already_cancelled_error(e):
    res = jsonify(message=e.message, error_code=e.error_code, details=e.details)
    res.status_code = e.code

    for m in e.details:
        res.headers.add('Allow', m)

    return res

@api_website.errorhandler(APIException)
def handle_api_error(e):
    res = jsonify(message=e.message, error_code=e.error_code, details=e.details)
    res.status_code = e.code

    return res

@api_website.errorhandler(OverQuotaError)
def handle_quota_error(e):
    res = jsonify(message='The backend has reached a resource quota. Please try again later.', error_code='SYSTEM_OVER_QUOTA', details=e.message)
    res.status_code = 500
    notify_admin("API Website Version 1", e, sys.exc_info())

    return res

@api_website.errorhandler(Exception)
def handle_system_error(e):
    res = jsonify(message='There has been a system error. Please try again later.', error_code='SYSTEM_GENERIC_ERROR', details=e.message)
    res.status_code = 500
    notify_admin("API Wesite Version 1", e, sys.exc_info())

    log.exception(e)

    return res

@api_website.before_request
def check_client_auth():
    log.debug("check_client_auth, api_website version1...")

    if AUTH_CLIENT_ID in request.headers and AUTH_TIMESTAMP in request.headers and AUTH_SIGNATURE in request.headers:
        client_id = request.headers[AUTH_CLIENT_ID]
        auth_ts = request.headers[AUTH_TIMESTAMP]
        client = ndb.Key(models.Client, client_id).get()

        if client:
            log.info("check_client_auth, client_id: %s, auth_ts: %s" % (client_id, auth_ts))

            client_authenticate_request(client.pubkey, auth_ts, request.headers[AUTH_SIGNATURE])
        else:
            log.warn("ERROR!, check_client_auth, client authentication failed...")

            raise ClientAuthException('Client authentication failed.')
    else:
        log.debug('check_client_auth, missing authentication headers...')

        if ENFORCE_AUTH_WEBSITE:
            log.warn("ERROR, check_client_auth, client authentication required...")

            abort(401, error_code='AUTH_REQUIRED', message='Client authentication required.')

@api_website.before_request
def check_device_id():
    log.debug("check_device_id, api_website version1...")

    if DEVICE_ID not in request.headers:
        raise DeviceIdRequiredException()

@api_website.before_request
def check_api_version():
    log.debug("check_api_version, api_website version1...")

    if API_VERSION in API_VERSIONS_DEPRECATED:
        raise abort(401, error_code='API_VERSION_DEPRECATED', message='Client API version deprecated.')

@api_website.before_request
def log_caller_info():
    ua = '(None)'
    client_id = "(Not given)"

    if 'User-Agent' in request.headers:
        ua = request.headers['User-Agent']
        
    if AUTH_CLIENT_ID in request.headers:
        client_id = request.headers[AUTH_CLIENT_ID]

    log.info("log_caller_info, api_website version1, User-Agent: %s..." % ua)
    log.info("log_caller_info, api_website version1, Client ID: %s..." % client_id)


###############
#
# API Meta
#
###############

@api_website.route('/')
def api_meta():
    return jsonify({'version': '1.21.0', 'api-website-version': '1.0'})


###############
#
# Movies Endpoints
#
###############

@api_website.route('/movies/')
def get_movies():
    filters, movie_list = query_model(models.Movie, 'movie', request)

    return jsonify({'filters': filters, 'results': movie_list})

@api_website.route('/movies/ids/')
def get_movies_ids():
    filters, movie_ids = query_model_ids(models.Movie, 'movie', request)

    return jsonify({'filters': filters, 'results': movie_ids})

@api_website.route('/movies/<movie_id>/')
def get_movie(movie_id):
    posters_args = {}
    posters_args['resolution'] = request.args.get('resolution', 'uploaded')
    posters_args['application_type'] = request.args.get('application_type', 'mobile-app')
    portrait_default_poster, landscape_default_poster = get_default_poster_url(posters_args=posters_args)
    movie = Key(models.Movie, movie_id).get()

    if not movie:
        raise NotFoundException(details={'movie_id': movie_id})

    return jsonify({'id': movie_id, 'movie': movie.to_entity2(posters_args, portrait_default_poster, landscape_default_poster)})

@api_website.route('/movies/<movie_id>/poster/')
def get_movie_poster(movie_id):
    posters_args = {}
    posters_args['resolution'] = request.args.get('resolution', 'uploaded')
    posters_args['application_type'] = request.args.get('application_type', 'mobile-app')

    orientation = request.args.get('orientation', 'portrait')
    movie_key = Key(models.Movie, movie_id)
    movie = movie_key.get()

    if not movie:
        raise NotFoundException(details={'movie_id': movie_id})

    poster = ''
    portrait_default_poster, landscape_default_poster = get_default_poster_url(posters_args=posters_args)
    portrait_poster, landscape_poster = movie.get_poster_url(posters_args=posters_args)

    if orientation == 'portrait':
        poster = portrait_default_poster

        if portrait_poster:
            poster = portrait_poster
    elif orientation == 'landscape':
        poster = landscape_default_poster

        if landscape_poster:
            poster = landscape_poster

    if not poster:
        raise NotFoundException(details={'movie_id': movie_id, 'orientation': orientation,
                'resolution': posters_args['resolution'], 'application_type': posters_args['application_type']})

    return redirect(poster)

@api_website.route('/movie_correlation/', methods=['GET'])
def get_movie_correlation():
    results = {}
    feed_id = request.args.get('feed_id', None)
    correlation_id = request.args.get('correlation_id', None)

    if feed_id is None:
        raise NotFoundException(details={'feed_id': feed_id})

    if correlation_id is None:
        raise NotFoundException(details={'correlation_id': correlation_id})

    feed_key = ndb.Key(models.Feed, feed_id)
    feed = feed_key.get()

    if not feed:
        raise NotFoundException(details={'feed_id': feed_id})

    if feed_id == str(orgs.ROCKWELL_MALLS):
        correlation_id = 'RW::%s' % correlation_id
    elif feed_id == str(orgs.GREENHILLS_MALLS):
        correlation_id = 'GH::%s' % correlation_id
    elif feed_id == str(orgs.SM_MALLS):
        correlation_id = 'SM::%s' % correlation_id
    elif feed_id == str(orgs.MEGAWORLD_MALLS):
        correlation_id = 'MGW::%s' % correlation_id

    movies = models.Movie.query(models.Movie.movie_correlation.movie_id==correlation_id, models.Movie.movie_correlation.org_key==feed_key).fetch()

    if len(movies) != 1:
        raise NotFoundException(details={'feed_id': feed_id, 'correlation_id': correlation_id})

    results['movie_id'] = movies[0].key.id()
    results['movie_title'] = movies[0].canonical_title

    return jsonify(results)


###############
#
# Theater Organizations Endpoints
#
###############

@api_website.route('/theaterorgs/')
def get_theaterorgs():
    check_filter_and_sort_args(models.TheaterOrganization, request.args)
    queryset = models.TheaterOrganization.query()
    filters, queryset = do_filter(models.TheaterOrganization, queryset, request.args)
    theaterorg_list = [{'id': org.key.id(), 'theater_organization': org_with_theaters(org)} for org in queryset]

    return jsonify({'filters': filters, 'results': theaterorg_list})

@api_website.route('/theaterorgs/<theaterorg_id>/')
def get_theaterorg_info(theaterorg_id):
    org_key = Key(models.TheaterOrganization, theaterorg_id)
    org = org_key.get()

    if not org:
        raise NotFoundException(details={'theaterorg_id': theaterorg_id})

    return jsonify({'id': theaterorg_id, 'theater_organization': org_with_theaters(org)})

@api_website.route('/theaterorgs/<theaterorg_id>/theaters/')
def get_theaterorg_theaters(theaterorg_id):
    org_key = Key(models.TheaterOrganization, theaterorg_id)
    org = org_key.get()

    if not org:
        raise NotFoundException(details={'theaterorg_id': theaterorg_id})

    if (('location.lon' in request.args and 'location.lat' in request.args) or
            ('cinemas.location.lon' in request.args and 'cinemas.location.lat' in request.args)):
        lat, lon, theater_keys = do_location_search(request)
        org_theater_keys = models.Theater.query(ancestor=org_key).fetch(keys_only=True)
        matched_keys = set(theater_keys).intersection(set(org_theater_keys))
        theater_list = do_get_distance(lat, lon, matched_keys, request)
        filters = {'match': {'location.lat': lat, 'location.lon': lon}}
    else:
        check_filter_and_sort_args(models.Theater, request.args)
        q = models.Theater.query(ancestor=org_key)
        filters, q = do_filter(models.Theater, q, request.args)
        theater_list = [{'id': encoded_theater_id(x.key), 'theater': x.to_entity2()} for x in q]

    return jsonify({'filters': filters, 'results': theater_list})

@api_website.route('/theaterorgs/<theaterorg_id>/theaters/<ignored>~<int:theater_id>/')
def get_theaterorg_theater_info(theaterorg_id, ignored, theater_id):
    theaterorg_theater_safety_check(theaterorg_id, ignored, theater_id)

    return get_theater_info_common2(theaterorg_id, ignored, theater_id)

@api_website.route('/theaterorgs/<theaterorg_id>/theaters/<ignored>~<int:theater_id>/seatmap/')
def get_theaterorg_theater_seatmap(theaterorg_id, ignored, theater_id):
    theaterorg_theater_safety_check(theaterorg_id, ignored, theater_id)

    if 'cinema' not in request.args:
        log.warn("ERROR!, get_theaterorg_theater_seatmap, missing parameter cinema...")

        raise BadValueException('cinema', 'You must specify a cinema.')

    cinema_name = request.args['cinema']

    return get_theater_seatmap_common(theaterorg_id, ignored, theater_id, cinema_name)

@api_website.route('/theaterorgs/<theaterorg_id>/theaters/<ignored>~<int:theater_id>/payment-options/')
def get_theaterorg_theater_paymentoptions(theaterorg_id, ignored, theater_id):
    theaterorg_theater_safety_check(theaterorg_id, ignored, theater_id)

    if 'platform' not in request.args:
        log.warn("ERROR!, get_theaterorg_theater_paymentoptions, missing parameter platform...")

        raise BadValueException('platform', 'You must specify a platform.')

    platform = request.args['platform']

    return get_theater_paymentoptions_common(theaterorg_id, ignored, theater_id, platform)

@api_website.route('/theaterorgs/<theaterorg_id>/theaters/<uncoded_id>/')
def dummy_theaterorg_theater_info(theaterorg_id, uncoded_id):
    raise NotFoundException(details={'theaterorg_id': theaterorg_id, 'theater_id': uncoded_id})


###############
#
# Theaters Endpoints
#
###############

@api_website.route('/theaters/')
def get_theaters():
    if (('location.lon' in request.args and  'location.lat' in request.args) or
            ('cinemas.location.lon' in request.args  and 'cinemas.location.lat' in request.args)):
        lat, lon, theater_keys = do_location_search(request)
        theater_list = do_get_distance(lat, lon, theater_keys, request)
        filters = {'match': {'location.lat': lat, 'location.lon': lon}}
    else:
        filters, theater_list = query_model(models.Theater, 'theater', request, id_encoder=encoded_theater_id)

    return jsonify({'filters': filters, 'results': theater_list})

@api_website.route('/theaters/<enc_org_uuid>~<int:theater_id>/')
def get_theater_info(enc_org_uuid, theater_id):
    org_uuid = decode_uuid(enc_org_uuid)

    return get_theater_info_common2(str(org_uuid), enc_org_uuid, theater_id)

@api_website.route('/theaters/<enc_org_uuid>~<int:theater_id>/seatmap/')
def get_theater_seatmap(enc_org_uuid, theater_id):
    if 'cinema' not in request.args:
        log.warn("ERROR!, get_theater_seatmap, missing parameter cinema...")

        raise BadValueException('cinema', 'You must specify a cinema.')

    org_uuid = decode_uuid(enc_org_uuid)
    cinema_name = request.args['cinema']

    return get_theater_seatmap_common(str(org_uuid), enc_org_uuid, theater_id, cinema_name)

@api_website.route('/theaters/<enc_org_uuid>~<int:theater_id>/payment-options/')
def get_theater_paymentoptions(enc_org_uuid, theater_id):
    if 'platform' not in request.args:
        log.warn("ERROR!, get_theater_paymentoptions, missing parameter platform...")

        raise BadValueException('platform', 'You must specify a platform.')

    org_uuid = decode_uuid(enc_org_uuid)
    platform = request.args['platform']
    claim_code = request.args['payment_code'] if 'payment_code' in request.args else None

    return get_theater_paymentoptions_common(str(org_uuid), enc_org_uuid, theater_id, platform, claim_code)

@api_website.route('/theaters/<uncoded_id>/')
def dummy_theater_info(uncoded_id):
    raise NotFoundException(details={'theater_id': uncoded_id})


###############
#
# Schedules and Available Seats Endpoints
#
###############

@api_website.route('/schedules/')
def get_schedules():
    return do_get_schedules()

@api_website.route('/theaterorgs/<theaterorg_id>/theaters/<ignored>~<int:theater_id>/schedules/')
def get_theaterorg_theater_schedules(theaterorg_id, ignored, theater_id):
    theaterorg_theater_safety_check(theaterorg_id, ignored, theater_id)

    return get_theater_schedules(ignored, theater_id)

@api_website.route('/theaters/<enc_org_uuid>~<int:theater_id>/schedules/')
def get_theater_schedules(enc_org_uuid, theater_id):
    return do_get_schedules(theater_pair=(enc_org_uuid,theater_id))

@api_website.route('/movies/<movie_id>/schedules/')
def get_movie_schedules(movie_id):
    return do_get_schedules(movie_id=movie_id)

@api_website.route('/movies/<movie_id>/schedules/theaters/')
def get_movie_schedules_theaters(movie_id):
    return do_get_schedules_theaters(movie_id=movie_id)

@api_website.route('/available_seats/')
def translate_and_get_available_seats():
    log.debug("translate_and_get_available_seats, api_website version1...")

    if 'theater' not in request.args:
        log.warn("ERROR!, translate_and_get_available_seats, missing parameter theater...")

        raise BadValueException('theater', 'You must specify a theater.')
    if 'schedule' not in request.args:
        log.warn("ERROR!, translate_and_get_available_seats, missing parameter schedule...")

        raise BadValueException('schedule', 'You must specify a schedule.')
    if 'time' not in request.args:
        log.warn("ERROR!, translate_and_get_available_seats, missing parameter time...")

        raise BadValueException('time', 'You must specify a time.')

    available = []
    remaining_seats = 0
    error_message = 'Unable to load the seats due to an unexpected error. Please try again later.'

    enc_theater_id = request.args['theater']
    sched_id_raw = request.args['schedule']
    sched_id = sched_id_raw.split(',')[0]
    sched_time = datetime.strptime(request.args['time'], models.TIME_FORMAT).time()
    theater_key = to_theater_key(enc_theater_id)
    theater = theater_key.get()

    if not theater:
        log.warn("ERROR!, translate_and_get_available_seats, theater is not found...")

        raise NotFoundException(details={'theater': enc_theater_id})

    log.info("translate_and_get_available_seats, theater.name: %s..." % theater.name)
    log.info("translate_and_get_available_seats, theater.org_theater_code: %s..." % theater.org_theater_code)
    log.info("translate_and_get_available_seats, theater.allow_reservation: %s..." % theater.allow_reservation)

    if theater.error_message:
        error_message = theater.error_message

    if not theater.allow_reservation:
        log.warn("ERROR!, translate_and_get_available_seats, reservation is not allowed...")

        res = {'theater': enc_theater_id, 'schedule': sched_id, 'available_seat_map': available,
                'available_seats': remaining_seats, 'error_message': error_message}

        return jsonify(res)

    theater_code = theater.org_theater_code
    schedule = Key(models.Schedule, sched_id, parent=theater.key).get()

    if not schedule:
        log.warn("ERROR!, translate_and_get_available_seats, schedule is not found...")

        raise NotFoundException(details={'schedule': sched_id})

    sched_slot = None

    for sl in schedule.slots:
        if sl.start_time == sched_time:
            sched_slot = sl

            break

    if not sched_slot:
        log.warn("ERROR!, translate_and_get_available_seats, schedule slot is not found...")

        raise NotFoundException(details={'time': request.args['time']})

    if schedule.show_date < date.today():
        log.warn("ERROR!, translate_and_get_available_seats, schedule is expired...")

        raise BadValueException('schedule', 'Expired schedule.')

    sched_code = sched_slot.feed_code_schedule
    sched_slot_code = sched_slot.feed_code
    seating_type = sched_slot.seating_type

    log.info("translate_and_get_available_seats, sched_code: %s..." % sched_code)
    log.info("translate_and_get_available_seats, sched_slot_code: %s..." % sched_slot_code)
    log.info("translate_and_get_available_seats, seating_type: %s..." % seating_type)

    # theater organization API lookup here; for now, hard-coded to use
    # SureSeats, Rockwell (Power Plant Mall), Greenhills, SM Malls, and Megaworld (LCT) API directly.
    if str(theater_key.parent().id()) == str(orgs.AYALA_MALLS): # get_available_seats for SureSeats.
        log.debug("translate_and_get_available_seats, SureSeats: get_available_seats...")

        # Check if ATC, add suffix for ATC Cinemas
        sched_cinema = schedule.cinema
        log.info("translate_and_get_available_seats, sched_cinema: %s..." % sched_cinema)
        if theater_code == 'ATC' and sched_cinema in ['1']:
            log.info("ATC! add suffix 1, update theater_code, %s, cinema, %s..." % (theater_code, sched_cinema))
            theater_code = '%s1' % theater_code
        elif theater_code == 'ATC' and sched_cinema in ['2', '3', '4', '5']:
            log.info("ATC! add suffix 2, update theater_code, %s, cinema, %s..." % (theater_code, sched_cinema))
            theater_code = '%s2' % theater_code

        status, available, remaining_seats = get_available_seats(theater_key.parent().id(), theater_code, sched_slot_code)
    elif str(theater_key.parent().id()) == str(orgs.ROCKWELL_MALLS): # get_available_seats for Rockwell (Power Plant Mall).
        log.debug("translate_and_get_available_seats, Rockwell (Power Plant Mall): get_available_seats...")

        status, available, remaining_seats = get_available_seats(theater_key.parent().id(), sched_code, sched_slot_code)
    elif str(theater_key.parent().id()) == str(orgs.GREENHILLS_MALLS): # get_available_seats for Greenhills Malls.
        log.debug("translate_and_get_available_seats, Greenhills Malls: get_available_seats...")

        schedslotcode_seatingtype = '%s::%s' % (sched_slot_code, seating_type)
        status, available, remaining_seats = get_available_seats(theater_key.parent().id(), sched_code, schedslotcode_seatingtype)
    elif str(theater_key.parent().id()) == str(orgs.CINEMA76_MALLS): # get_available_seats for Cinema 76 Malls.
        log.debug("translate_and_get_available_seats, Cinema 76 Malls: get_available_seats...")

        schedslotcode_seatingtype = '%s::%s' % (sched_slot_code, seating_type)
        status, available, remaining_seats = get_available_seats(theater_key.parent().id(), sched_code, schedslotcode_seatingtype)
    elif str(theater_key.parent().id()) == str(orgs.GLOBE_EVENTS): # get_available_seats for Globe Events.
        log.debug("translate_and_get_available_seats, Globe Events: get_available_seats...")

        schedslotcode_seatingtype = '%s::%s' % (sched_slot_code, seating_type)
        status, available, remaining_seats = get_available_seats(theater_key.parent().id(), sched_code, schedslotcode_seatingtype)
    elif str(theater_key.parent().id()) == str(orgs.SM_MALLS): # get_available_seats for SM Malls.
        log.debug("translate_and_get_available_seats, SM Malls: get_available_seats...")

        status, available, remaining_seats = get_available_seats(theater_key.parent().id(), theater, sched_slot_code, arg4=True)
    elif str(theater_key.parent().id()) == str(orgs.ROBINSONS_MALLS): # get_available_seats for Robinsons Malls.
        log.debug("translate_and_get_available_seats, Robinsons Malls: get_available_seats...")

        status, available, remaining_seats = get_available_seats(theater_key.parent().id(), theater, sched_slot_code, arg4=True)
    elif str(theater_key.parent().id()) == str(orgs.MEGAWORLD_MALLS): # get_available_seats for Megaworld Malls.
        log.debug("translate_and_get_available_seats, Megaworld Malls: get_available_seats...")

        if theater_code in MGI_THEATER_CODES:
            log.debug('translate_and_get_available_seats, MGi (LCT): get_available_seats...')

            status, available, remaining_seats = get_available_seats(theater_key.parent().id(), theater, sched_slot_code, 'MGI')
        else:
            log.warn("ERROR!, translate_and_get_available_seats, missing third party available seats endpoint...")

            status = 'error'
    else:
        log.warn("ERROR!, translate_and_get_available_seats, missing third party available seats endpoint...")

        status = 'error'

    if status == 'success':
        error_message = ''

    available_seats = {'theater': enc_theater_id, 'schedule': sched_id, 'available_seat_map': available,
            'available_seats': remaining_seats, 'error_message': error_message}

    return jsonify(available_seats)


###############
#
# Transactions Endpoints
#
###############

@api_website.route('/tx/', methods=['GET'])
def get_device_txs():
    state_filter = None
    device_id = request.headers[DEVICE_ID]
    device_key = Key(models.Device, device_id)
    q = models.ReservationTransaction.query(ancestor=device_key)

    if 'states' in request.args:
        state_strs = request.args.getlist('states')
        states = filter(None, [getattr(tx.state, s) if hasattr(tx.state, s) else None for s in state_strs])
        log.debug("States to match: %s" % states)
        q = q.filter(models.ReservationTransaction.state.IN(states))
        state_filter = state_strs
    else:
        q = q.filter(models.ReservationTransaction.state != tx.state.TX_DONE)

    if 'sort' in request.args:
        sort_cols = [col if col[0] != '-' else col[1:] for col in request.args.getlist('sort')]
        check = filter(lambda tup: not tup[1], [(k, resolve_dotted_name(models.ReservationTransaction, k)) for k in sort_cols])

        if len(check) > 0:
            unknown_sort_args = [k for k, v in check]

            raise InvalidOrderPropertyException(unknown_sort_args)

        q = apply_sort(q, models.ReservationTransaction, request.args.getlist('sort'))

    txs = [k.id() for k in q.fetch(keys_only=True)]

    return jsonify({'states': state_filter, 'transactions': txs})

@api_website.route('/tx/', methods=['POST'])
def start_tx():
    device_id = request.headers[DEVICE_ID]

    try:
        log.info("start_tx, request.json: {}...".format(request.json))

        tx_info = request.json

        # override robinsonsons-cardholder-promo to use paynamics payment gateway 
        # instead of mix that has been already set to cinema partner settings at theater
        if 'discount' in tx_info:
            log.debug("old_payment_gateway: %s" % (tx_info['payment']['discount']['payment_gateway']))
        
        theater_id = tx_info['reservations'][0]['theater']

        log.debug("theater_id: %s" % (theater_id))

        theater = admin.get_theater(theater_id)

        log.debug("theater: %s" % (theater))

        log.debug("theaterorg key id: %s" % (theater.key.parent().get().name))

        theater_name = str(theater.key.parent().get().name).encode('ascii', 'ignore').decode('ascii')

        if 'robinsons' in theater_name.lower():
            log.debug("detected as robinsons")

            if str(tx_info['payment']['type']) == 'robinsonsbank-cardholder-promo':
                tx_info['payment']['discount']['payment_gateway'] = 'paynamics'
                log.debug("new_payment_gateway: %s" % (tx_info['payment']['discount']['payment_gateway']))

        log.debug("new request.data: %s" % (tx_info))

    except Exception, e:
        log.info("start_tx, request.data: {}...".format(request.data))
        log.warn("ERROR!, start_tx, malformed request.json...")
        log.error(traceback.format_exc(e))

        data = unicode(request.data, 'latin-1')
        tx_info = json.loads(data)

    log.info("start_tx, tx_info: {}...".format(tx_info))

    if not tx_info:
        log.warn("ERROR!, start_tx, missing tx_info...")

        raise BadValueException('body', 'You must supply transaction information.')

    transaction = _start_tx(device_id, tx_info)

    return jsonify({'id': transaction.key.id(), 'transaction': transaction.to_entity2()})

@api_website.route('/tx/minimize/', methods=['GET'])
def get_device_txs_minimize():
    state_filter = None
    device_id = request.headers[DEVICE_ID]
    device_key = Key(models.Device, device_id)
    q = models.ReservationTransaction.query(ancestor=device_key)

    if 'states' in request.args:
        state_strs = request.args.getlist('states')
        states = filter(None, [getattr(tx.state, s) if hasattr(tx.state, s) else None for s in state_strs])
        log.debug("States to match: %s" % states)
        q = q.filter(models.ReservationTransaction.state.IN(states))
        state_filter = state_strs
    else:
        q = q.filter(models.ReservationTransaction.state != tx.state.TX_DONE)

    if 'sort' in request.args:
        sort_cols = [col if col[0] != '-' else col[1:] for col in request.args.getlist('sort')]
        check = filter(lambda tup: not tup[1], [(k, resolve_dotted_name(models.ReservationTransaction, k)) for k in sort_cols])

        if len(check) > 0:
            unknown_sort_args = [k for k, v in check]

            raise InvalidOrderPropertyException(unknown_sort_args)

        q = apply_sort(q, models.ReservationTransaction, request.args.getlist('sort'))

    txs = [{'id': t.key.id(), 'ticket': t.to_entity_minimize()} for t in q.fetch()]

    return jsonify({'states': state_filter, 'transactions': txs})

@api_website.route('/tx/<tx_id>/', methods=['PUT'])
def update_tx_info(tx_id):
    device_id = request.headers[DEVICE_ID]

    try:
        log.info("update_tx_info, request.json: {}...".format(request.json))

        tx_info = request.json
    except Exception, e:
        log.info("update_tx_info, request.data: {}...".format(request.data))
        log.warn("ERROR!, update_tx_info, malformed request.json...")
        log.error(e)

        data = unicode(request.data, 'latin-1')
        tx_info = json.loads(data)

    log.info("update_tx_info, tx_info: {}...".format(tx_info))

    if not tx_info:
        raise BadValueException('body', 'You must supply transaction information.')

    transaction = tx.update(device_id, tx_id, tx_info)

    return jsonify({'id' : tx_id, 'transaction': transaction.to_entity2()})

@api_website.route('/tx/<tx_id>/', methods=['DELETE'])
def cancel_tx(tx_id):
    device_id = request.headers[DEVICE_ID]
    tx.cancel(device_id, tx_id)

    return jsonify({'id' : tx_id, 'cancelled': True})

@api_website.route('/tx/<tx_id>/', methods=['GET'])
def get_tx_info(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = tx.query_info(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={'tx_id': tx_id})

    return jsonify({'id' : tx_id, 'transaction': transaction.to_entity2()})

@api_website.route('/tx/<tx_id>/state/', methods=['GET'])
def get_tx_state(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = tx.query_info(device_id, tx_id)
    state, msg = tx.query_state(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={'tx_id': tx_id})

    if (state, msg) == (None, None):
        raise NotFoundException(details={'tx_id': tx_id})

    tx_state = tx.to_state_str(state)

    if msg:
        log.info("get_tx_state, state: %s, message: %s..." % (tx_state, msg))

        # Set error msg for transactions that timed-out
        if 'TX_CANCELLED' == tx_state and msg == transaction.reservation_reference:
            msg = "An error has been encountered while processing your payment. Please try again later."

        return jsonify({'state': tx_state, 'message': msg})

    log.info("get_tx_state, state: %s..." % tx_state)

    if transaction.payment_type == models.ReservationTransaction.allowed_payment_types.PROMO_CODE:
        return jsonify({'state': tx_state, 'message': ''})

    return jsonify({'state': tx_state})

@api_website.route('/tx/<tx_id>/state/')
def force_tx_state(tx_id):
    abort(501)

@api_website.route('/tx/<tx_id>/refund/', methods=['PUT'])
def refund_tx(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = tx.query_info(device_id, tx_id)
    log.info("refund_tx, device_id: {}...".format(device_id))
    log.info("refund_tx, tx_id: {}...".format(tx_id))
    try:
        log.info("refund_tx, request.json: {}...".format(request.json))
        refund_info = request.json

    except Exception, e:
        log.info("refund_tx, request.data: {}...".format(request.data))
        log.warn("ERROR!, refund_tx, malformed request.json...")
        log.error(e)

    log.info("refund_tx, refund_info: {}...".format(refund_info))

    if not refund_info:
        log.warn("ERROR!, refund_tx, missing refund_info...")
        raise BadValueException('body', 'You must supply refund information.')

    status = 'error'
    result = 'Please check parameters.'
    error_msg = ''
    source = "via API"
    if 'refunded_by' in refund_info and 'refund_reason' in refund_info:
        if 'source' in refund_info:
            source = refund_info['source']
        status, result = refund(device_id, tx_id, refund_info['refund_reason'], refund_info['refunded_by'], source)

        if hasattr(transaction, 'error_info') and hasattr(transaction.error_info, 'error_msg'):
            error_msg = transaction.error_info['error_msg']

    log.debug("refund_tx, error_code: %s, result: %s, message: %s..." % (status.upper(), result, error_msg))
    return jsonify({'id': transaction.key.id(), 'error_code': status.upper(), 'result': result, 'message': error_msg})

@api_website.route('/tx/<tx_id>/client-initiate/', methods=['GET'])
def client_initiate_payment(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = tx.query_info(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={ 'tx_id': tx_id })

    state, msg = tx.query_state(device_id, tx_id)

    if state != tx.state.TX_CLIENT_PAYMENT_HOLD:
        raise TransactionConflictException([tx.state.TX_CLIENT_PAYMENT_HOLD])

    payment_info = transaction.workspace['payment::engine'] # FIXME: leaked constant.
    reference = transaction.reservation_reference
    org_id = transaction.workspace['theaters.org_id'][0] # FIXME: hardcoded reference.

    if org_id == str(orgs.AYALA_MALLS):
        theater_code = transaction.workspace['theaters.org_theater_code'][0]
        reference = "%s-%s" % (theater_code, reference)

    payment_info.form_parameters['amount'] = transaction.workspace['RSVP:totalamount'] # override amount specified.
    payment_info.form_parameters['secureHash'] = payment_info.generate_hash(reference) # FIXME: hardcoded hash key and calculation.

    log.info("client_initiate_payment, form_parameters...")
    log.info(payment_info.form_parameters)

    return render_template('client_initiated_payment.html', tx=transaction, reference=reference, payment_info=payment_info)

@api_website.route('/tx/<tx_id>/paynamics-initiate/', methods=['GET'])
def paynamics_initiate_payment(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = tx.query_info(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={'tx_id': tx_id})

    state, msg = tx.query_state(device_id, tx_id)

    if state != tx.state.TX_CLIENT_PAYMENT_HOLD:
        raise TransactionConflictException([tx.state.TX_CLIENT_PAYMENT_HOLD])

    client_ip = request.remote_addr
    org_uuid = transaction.workspace['theaters.org_id'][0]
    reservation_reference = transaction.reservation_reference.split('~')[-1] if (str(org_uuid) not in [str(orgs.SM_MALLS),
        str(orgs.ROBINSONS_MALLS), str(orgs.CINEMA76_MALLS), str(orgs.GLOBE_EVENTS)]) else transaction.reservation_reference

    payment_info = transaction.workspace['payment::engine']
    if is_3d_token_paynamics(transaction):
        if transaction.workspace['discount::is_discounted']:
            payment_info = transaction.workspace['payment::engine'].payment_engine
        log.info("paynamics_initiate_payment, is_3d_token_paynamics...")
        log.info(payment_info.form_method)
        log.info(payment_info.form_target)
        return render_template('paynamics_3d_initiated_payment.html', payment_info=payment_info)
    else:
        payment_info.form_parameters['amount'] = transaction.workspace['RSVP:totalamount']
        payment_info.form_parameters['request_id'] = reservation_reference
        payment_info.form_parameters['country'] = 'PH'
        payment_info.form_parameters['orders'] = payment_info.generate_orders_payload(transaction)
        payment_info.form_parameters['client_ip'] = client_ip
        payment_info.form_parameters['signature'] = payment_info.generate_signature()
        paymentrequest = payment_info.generate_paymentrequest()

        try:
            log.info("paynamics_initiate_payment, client_ip: {}...".format(client_ip))
            log.info("paynamics_initiate_payment, form_parameters: {}...".format(payment_info.form_parameters))
            log.info("paynamics_initiate_payment, signature: {}...".format(payment_info.form_parameters['signature']))
            log.info("paynamics_initiate_payment, paymentrequest: {}...".format(paymentrequest))
        except Exception, e:
            log.warn("paynamics_initiate_payment, failed to logs the paynamics-initiate details...")
            log.error(e)

        return render_template('paynamics_initiated_payment.html', payment_info=payment_info, paymentrequest=paymentrequest)

@api_website.route('/tx/<tx_id>/migs-initiate/', methods=['GET'])
def migs_initiate_payment(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = tx.query_info(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={'tx_id': tx_id})

    state, msg = tx.query_state(device_id, tx_id)

    if state != tx.state.TX_CLIENT_PAYMENT_HOLD:
        raise TransactionConflictException([tx.state.TX_CLIENT_PAYMENT_HOLD])

    org_uuid = transaction.workspace['theaters.org_id'][0]
    reservation_reference = transaction.reservation_reference
    payment_info = transaction.workspace['payment::engine']

    payment_info.form_parameters['vpc_Amount'] = transaction.workspace['RSVP:totalamount'].replace('.', '')
    payment_info.form_parameters['vpc_MerchTxnRef'] = reservation_reference
    payment_info.form_parameters['vpc_OrderInfo'] = payment_info.generate_order_information(transaction)
    payment_info.form_parameters['vpc_SecureHash'] = payment_info.generate_signature(payment_info.form_parameters)
    payment_info.form_parameters['vpc_SecureHashType'] = 'SHA256'

    return render_template('migs_initiated_payment.html', payment_info=payment_info)

@api_website.route('/tx/<tx_id>/ipay88-payment-initiate/', methods=['GET'])
def ipay88_initiate_payment(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = tx.query_info(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={'tx_id': tx_id})

    state, msg = tx.query_state(device_id, tx_id)

    if state != tx.state.TX_EXTERNAL_PAYMENT_HOLD:
        raise TransactionConflictException([tx.state.TX_EXTERNAL_PAYMENT_HOLD])

    payment_info = transaction.workspace['payment::engine']
    reservation_reference = transaction.reservation_reference
    signature = payment_info.generate_hash(reservation_reference, LCT_MERCHANT_KEY)
    payment_info.form_parameters["RefNo"] = reservation_reference
    payment_info.form_parameters["Signature"] = signature

    return jsonify({"form_method": payment_info.form_method, "form_target":payment_info.form_target, "payment_info": payment_info.form_parameters})


###############
#
# Payment Options Settings Endpoints
#
###############

@api_website.route('/payment-options/', methods=['GET'])
def get_payment_options():
    payment_options = models.PaymentOptionSettings.query().fetch()

    return jsonify({'filters': {}, 'results': [{'id': po.key.id(), 'payment_option': po.to_entity()} for po in payment_options]})

@api_website.route('/payment-options/<payment_option_identifier>/', methods=['GET'])
def get_payment_option(payment_option_identifier):
    payment_option_key = ndb.Key(models.PaymentOptionSettings, payment_option_identifier)
    payment_option = payment_option_key.get()

    if not payment_option:
        raise NotFoundException(details={'payment_option_identifier': payment_option_identifier})

    return jsonify({'id': payment_option.key.id(), 'payment_option': payment_option.to_entity()})


###############
#
# Services Endpoints
#
###############

@api_website.route('/services/', methods=['GET'])
def get_services():
    check_filter_and_sort_args(models.AnalyticsService, request.args)
    platform = request.headers['gmovies-platform'] if 'gmovies-platform' in request.headers else request.args.get('platform', '')
    queryset = models.AnalyticsService.query(models.AnalyticsService.active==True).fetch()
    services_list = [service.to_entity(platform=platform) for service in queryset]

    return jsonify({'filters': {}, 'results': services_list})

@api_website.route('/services/<service_code>/', methods=['GET'])
def get_service_status(service_code):
    results = {}
    platform = request.headers['gmovies-platform'] if 'gmovies-platform' in request.headers else request.args.get('platform', '')
    service = models.AnalyticsService.get_service_status(str(service_code))

    if not service:
        raise NotFoundException(details={'service_code': service_code})

    results['name'] = service[0].service_name
    results['code'] = service[0].service_code
    results['value'] = service[0].value

    if service[0].active:
        results['status'] = 'active'
    else:
        results['status'] = 'inactive'

    if platform not in service[0].platform:
        results['status'] = 'inactive'

    return jsonify({'id': service[0].key.id(), 'service': results})


###############
#
# Promos / Blocked Screenings / Ghost Theaters Endpoints
#
###############

@api_website.route('/assets/promos/')
def get_promos_assets():
    check_filter_and_sort_args(models.Promo, request.args)

    queryset = models.AssetsImageBlob.query(models.AssetsImageBlob.assets_group=='promos-blocked-screening')
    filters, queryset = do_filter(models.AssetsImageBlob, queryset, request.args)

    return jsonify({'filters': filters, 'results': [{'id': a.key.id(), 'image': get_serving_url(a.image)} for a in queryset]})

@api_website.route('/promos/')
def get_promos():
    check_filter_and_sort_args(models.Promo, request.args)

    posters_args = {}
    posters_args['resolution'] = request.args.get('resolution', 'uploaded')
    posters_args['application_type'] = request.args.get('application_type', 'mobile-app')

    queryset = models.Promo.query()
    filters, queryset = do_filter(models.Promo, queryset, request.args)
    # queryset = queryset.order(models.Promo.theater, models.Promo.cinema)

    # if 'platform' not in request.args:
    #     log.warn("ERROR!, get_promos, missing platform parameter...")
    #
    #     return jsonify({'filters': filters, 'results': []})

    return jsonify({'filters': filters, 'results': [{'id': p.key.id(), 'promo': p.to_entity2(posters_args)} for p in queryset]})

@api_website.route('/promos/<promo_id>/')
def get_promo(promo_id):
    posters_args = {}
    posters_args['resolution'] = request.args.get('resolution', 'uploaded')
    posters_args['application_type'] = request.args.get('application_type', 'mobile-app')

    promo_key = ndb.Key(models.Promo, promo_id)
    promo = promo_key.get()

    if not promo:
        raise NotFoundException(details={'promo_id': promo_id})

    return jsonify({'id': promo.key.id(), 'promo': promo.to_entity2(posters_args)})

@api_website.route('/promos/<promo_id>/available_seats/')
def get_available_seats_promo(promo_id):
    results = {}
    convenience_fees = {}
    convenience_fees_message = {}
    available_seat_map = []
    seats = []
    seats_taken = []
    seat_map = []
    payment_options = []
    payment_option_identifiers = []
    available_seats = 0

    platform = request.args.get('platform', None)
    promo_key = ndb.Key(models.Promo, promo_id)
    promo = promo_key.get()

    if not promo:
        raise NotFoundException(details={'promo_id': promo_id})

    try:
        payment_option_identifiers = {}
        payment_option_identifiers['ios'] = promo.payment_options_ios
        payment_option_identifiers['android'] = promo.payment_options_android
        payment_option_identifiers['website'] = promo.payment_options_website

        if platform:
            for identifier in payment_option_identifiers[platform]:
                payment_option_key = ndb.Key(models.PaymentOptionSettings, identifier)
                payment_option = payment_option_key.get()

                if not payment_option:
                    log.warn("SKIP!, get_available_seats_promo, not existing payment_option_identifier, %s..." % identifier)

                    continue

                payment_options.append({'id': payment_option.key.id(), 'payment_option': payment_option.to_entity()})

        if promo.seat_map:
            seat_map = promo.seat_map

            for row_seats in promo.seat_map:
                seats.extend(row_seats)

        if promo.seats_blacklist:
            seats_taken.extend(promo.seats_blacklist)

        if promo.seats_locked:
            seats_taken.extend(promo.seats_locked)

        if promo.seats_purchased:
            seats_taken.extend(promo.seats_purchased)

        if promo.convenience_fees:
            convenience_fees = promo.convenience_fees

        if promo.convenience_fees_message:
            convenience_fees_message = promo.convenience_fees_message

        available_seat_map = sorted(list(set(seats) - set(seats_taken)))
        available_seats = len(available_seat_map)
        error_message = ''
    except Exception, e:
        log.warn('ERROR!, get_promo_available_seatmap, %s get available seats...' % promo.key.id())
        log.error(e)

        error_message = 'Unable to load the seats due to an unexpected error. Please try again later.'

    results['promo'] = promo.key.id()
    results['allowed_seats'] = promo.allowed_seats
    results['available_seat_map'] = available_seat_map
    results['available_seats'] = available_seats
    results['convenience_fees'] = convenience_fees
    results['convenience_fees_message'] = convenience_fees_message
    results['payment_options'] = payment_options
    results['seat_map'] = seat_map
    results['error_message'] = error_message

    return jsonify(results)

@api_website.route('/tx_promo/', methods=['GET'])
def get_device_txs_promo():
    device_id = request.headers[DEVICE_ID]
    device_key = ndb.Key(models.Device, device_id)
    q = models.PromoReservationTransaction.query(ancestor=device_key)
    state_filter = None

    if 'states' in request.args:
        state_strs = request.args.getlist('states')
        states = filter(None, [getattr(promo.state, s) if hasattr(promo.state, s) else None for s in state_strs])

        log.debug("get_device_txs_promo, states to match: %s" % states)

        q = q.filter(models.PromoReservationTransaction.state.IN(states))
        state_filter = state_strs
    else:
        q = q.filter(models.PromoReservationTransaction.state != promo.state.TX_DONE)

    if 'sort' in request.args:
        sort_cols = [col if col[0] != '-' else col[1:]
                for col in request.args.getlist('sort')]

        check = filter(lambda tup: not tup[1], [(k, resolve_dotted_name(models.PromoReservationTransaction, k)) for k in sort_cols])

        if len(check) > 0:
            unknown_sort_args = [k for k,v in check]

            raise InvalidOrderPropertyException(unknown_sort_args)

        q = apply_sort(q, models.PromoReservationTransaction, request.args.getlist('sort'))

    txs = [k.id() for k in q.fetch(keys_only=True)]

    return jsonify({'states': state_filter, 'transactions': txs})

@api_website.route('/tx_promo/minimize/', methods=['GET'])
def get_device_txs_promo_minimize():
    device_id = request.headers[DEVICE_ID]
    device_key = ndb.Key(models.Device, device_id)
    q = models.PromoReservationTransaction.query(ancestor=device_key)
    state_filter = None

    if 'states' in request.args:
        state_strs = request.args.getlist('states')
        states = filter(None, [getattr(promo.state, s) if hasattr(promo.state, s) else None for s in state_strs])

        log.debug("get_device_txs_promo, states to match: %s" % states)

        q = q.filter(models.PromoReservationTransaction.state.IN(states))
        state_filter = state_strs
    else:
        q = q.filter(models.PromoReservationTransaction.state != promo.state.TX_DONE)

    if 'sort' in request.args:
        sort_cols = [col if col[0] != '-' else col[1:]
                for col in request.args.getlist('sort')]

        check = filter(lambda tup: not tup[1], [(k, resolve_dotted_name(models.PromoReservationTransaction, k)) for k in sort_cols])

        if len(check) > 0:
            unknown_sort_args = [k for k,v in check]

            raise InvalidOrderPropertyException(unknown_sort_args)

        q = apply_sort(q, models.PromoReservationTransaction, request.args.getlist('sort'))

    txs = [{'id': t.key.id(), 'ticket': t.to_entity_minimize()} for t in q.fetch()]

    return jsonify({'states': state_filter, 'transactions': txs})

@api_website.route('/tx_promo/', methods=['POST'])
def start_tx_promo():
    device_id = request.headers[DEVICE_ID]

    try:
        log.info("start_tx_promo, request.json: {}".format(request.json))

        tx_info = request.json
    except Exception, e:
        log.info("start_tx_promo, request.data: {}".format(request.data))
        log.warn("ERROR!, start_tx_promo, malformed request.json...")
        log.error(e)

        data = unicode(request.data, 'latin-1')
        tx_info = json.loads(data)

    log.info("start_tx_promo, tx_info: {}...".format(tx_info))

    if not tx_info:
        log.warn("ERROR!, start_tx_promo, missing tx_info...")

        raise BadValueException('body', 'You must supply transaction information.')

    transaction = _start_tx_promo(device_id, tx_info)

    return jsonify({'id': transaction.key.id(), 'transaction': transaction.to_entity2()})

@api_website.route('/tx_promo/<tx_id>/', methods=['GET'])
def get_tx_info_promo(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = promo.query_info(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={'tx_id': tx_id})

    return jsonify({'id' : tx_id, 'transaction': transaction.to_entity2()})

@api_website.route('/tx_promo/<tx_id>/', methods=['PUT'])
def update_tx_info_promo(tx_id):
    device_id = request.headers[DEVICE_ID]
    tx_info = request.json

    if not tx_info:
        log.warn("ERROR!, update_tx_info_promo, missing tx_info...")

        raise BadValueException('body', 'You must supply transaction information.')

    transaction = promo.update(device_id, tx_id, tx_info)

    return jsonify({'id' : tx_id, 'transaction': transaction.to_entity2()})

@api_website.route('/tx_promo/<tx_id>/state/', methods=['GET'])
def get_tx_state_promo(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = promo.query_info(device_id, tx_id)
    state, msg = promo.query_state(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={'tx_id': tx_id})

    if (state, msg) == (None, None):
        raise NotFoundException(details={'tx_id': tx_id})

    tx_state = promo.to_state_str_promo(state)

    if not msg:
        msg = ''

    return jsonify({'state': tx_state, 'message': msg})

@api_website.route('/tx_promo/<tx_id>/paynamics-initiate/', methods=['GET'])
def paynamics_initiate_payment_promo(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = promo.query_info(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={'tx_id': tx_id})

    state, msg = promo.query_state(device_id, tx_id)

    if state != promo.state.TX_PAYNAMICS_PAYMENT_HOLD:
        raise TransactionConflictException([promo.state.TX_PAYNAMICS_PAYMENT_HOLD])

    client_ip = request.remote_addr
    payment_info = transaction.workspace['payment::engine']

    log.info("paynamics_initiate_payment_promo, Paynamics parameter, client_ip: %s", client_ip)

    payment_info.form_parameters['amount'] = transaction.workspace['RSVP:totalamount']
    payment_info.form_parameters['request_id'] = transaction.reservation_reference
    payment_info.form_parameters['country'] = 'PH'
    payment_info.form_parameters['orders'] = payment_info.generate_orders_payload(transaction)
    payment_info.form_parameters['client_ip'] = client_ip
    payment_info.form_parameters['signature'] = payment_info.generate_signature()
    paymentrequest = payment_info.generate_paymentrequest()

    try:
        log.info("paynamics_initiate_payment_promo, form_parameters: {}".format(payment_info.form_parameters))
        log.info("paynamics_initiate_payment_promo, signature: {}".format(payment_info.form_parameters['signature']))
        log.info("paynamics_initiate_payment_promo, paymentrequest: {}".format(paymentrequest))
    except Exception, e:
        log.warn("ERROR!, paynamics_initiate_payment_promo, failed to logs the paynamics-initiate details...")
        log.error(e)

    return render_template('paynamics_initiated_payment.html', payment_info=payment_info, paymentrequest=paymentrequest)

###############
#
# For QR CODE api set transaction status for unclaimed to claimed
# and for cash transaction status to unclaimed to claimed, forfeit, or unchanged
#
###############

@api_website.route('/update/status/<tx_id>', methods=['GET'])
def update_status(tx_id):
    device_id = request.headers[DEVICE_ID]

    log.debug("TX_ID: %s, DEVICE_ID: %s" % (tx_id, device_id))

    status, result = update_status_by_qrcode(device_id, tx_id)

    log.debug("STATUS: %s, RESULT: %s" % (status, result))

    return jsonify({'status': status, 'message': result})
