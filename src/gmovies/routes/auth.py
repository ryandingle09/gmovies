import base64
import logging
import time

from Crypto.Hash import SHA
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5

from flask import request

from gmovies.admin import notify_admin
from gmovies.exceptions.api import ClientAuthException


log = logging.getLogger(__name__)

TIMESTAMP_MAX_INTERVAL = 10
WEB_TIMESTAMP_MAX_INTERVAL = 10
CLIENT_TIMESTAMP_MAX_INTERVAL = 10


def authenticate_request(caller_keytext, auth_ts, b64sig):
    current_ts = int(time.time())

    # Check that auth timestamp is an integer
    try:
        ts = int(auth_ts)
    except ValueError:
        log.warn("Auth: Invalid timestamp value: %s" % auth_ts)
        raise ClientAuthException('Client failed auth check')

    if current_ts - ts > TIMESTAMP_MAX_INTERVAL:
        log.warn('Auth: Timestamp too old; possible replay attack')
        raise ClientAuthException('Client failed auth check: Timestamp too old')

    # Automatic ClientAuthException when no key attached
    if not caller_keytext or caller_keytext == '':
        raise ClientAuthException('Failed auth check')

    sig = b64sig.decode('base64')

    sorted_params = sorted(request.args.items(multi=True))
    sorted_params = [ "%s=%s" % (k,v) for k,v in sorted_params ]
    sorted_params = reduce(lambda a,b: a+b, sorted_params, '')
    log.debug("Got params: %s" % request.args.items(multi=True))

    text = auth_ts + request.method + request.script_root + request.path + sorted_params

    log.debug("Request: %s", request)
    if request.data:
        text += SHA.new(request.data).hexdigest()

    log.debug("Calculated text for auth sig: %s" % text)

    caller_pubkey = RSA.importKey(caller_keytext).publickey()
    verifier = PKCS1_v1_5.new(caller_pubkey)
    text_hash = SHA.new(text)

    if not verifier.verify(text_hash, sig):
        log.warn("Client failed auth check, signature didn't check out")
        raise ClientAuthException('Client failed auth check')

def web_authenticate_request(public_key, auth_ts, b64sig):
    current_ts = int(time.time())

    # Check that auth timestamp is an integer
    try:
        ts = int(auth_ts)
    except ValueError:
        log.warn("Auth: Invalid timestamp value: %s" % auth_ts)
        raise ClientAuthException('Client failed auth check')

    log.info("CURRENT TS: {}".format(current_ts))
    log.warn("TS: {}".format(ts))
    log.warn("INTERVAL: {}".format(current_ts - ts))

    if current_ts - ts > WEB_TIMESTAMP_MAX_INTERVAL:
        log.warn("CURRENT TS: {}".format(current_ts))
        log.warn('Auth: Timestamp too old; possible replay attack')
        raise ClientAuthException('Client failed auth check: Timestamp too old')
    else:
        log.warn('Timestamp validation success.')

    # PRIVATE_KEY = RSA.importKey(private_key)

    # Automatic ClientAuthException when no key attached
    if not public_key or public_key == '':
        raise ClientAuthException('Failed auth check')

    log.info("BASE64 SIGNATURE: {}".format(b64sig))
    # sig = b64sig.decode('base64')
    sig = base64.b64decode(b64sig)
    log.info("SIGNATURE: {} {}".format(sig, type(sig)))

    sorted_params = sorted(request.args.items(multi=True))
    sorted_params = [ "%s=%s" % (k,v) for k,v in sorted_params ]
    sorted_params = reduce(lambda a,b: a+b, sorted_params, '')
    log.debug("Got params: %s" % request.args.items(multi=True))

    text = auth_ts + request.method + request.script_root + request.path + sorted_params

    # # ## Test backend of signature. to be remove
    # sig = SHA.new(text)
    # log.info("SHA BEFORE ENCODE: {}".format(sig.hexdigest()))
    # signer = PKCS1_v1_5.new(PRIVATE_KEY)
    # sig = signer.sign(sig)
    # log.info("SIGNER: {}".format(sig))
    # ##

    log.debug("Request: %s", request)
    log.info("Displaying post data value..... {}".format(request.data))
    if request.data:
        log.info("POST DATA: {} {}".format(request.data, type(request.data)))
        text += SHA.new(request.data).hexdigest()

    log.info("TEXT: {}".format(text))

    log.debug("Calculated text for auth sig: %s" % text)

    log.info("PUBLIC KEY: \n{} {}".format(public_key, type(public_key)))
    caller_pubkey = RSA.importKey(public_key).publickey()
    # caller_pubkey = RSA.importKey(public_key)
    verifier = PKCS1_v1_5.new(caller_pubkey)
    text_hash = SHA.new(text)
    log.info("TEXT HASHED HEXDIGEST: {}".format(text_hash.hexdigest()))

    log.info("VERIFY VALUE: {}".format(verifier.verify(text_hash, sig)))
    if not verifier.verify(text_hash, sig):
        log.warn("Client failed auth check, signature didn't check out")
        raise ClientAuthException('Client failed auth check')

    log.info("Client authentication SUCCESS.")

def client_authenticate_request(public_key, auth_ts, b64sig):
    current_ts = int(time.time())

    # check that authentication timestamp is an integer.
    try:
        ts = int(auth_ts)
    except ValueError:
        log.warn("ERROR!, client_authenticate_request, invalid timestamp value: %s..." % auth_ts)

        raise ClientAuthException('Client authentication failed.')

    interval_ts = current_ts - ts

    log.info("client_authenticate_request, current_ts: %s..." % current_ts)
    log.info("client_authenticate_request, ts: %s..." % ts)
    log.info("client_authenticate_request, interval: %s..." % interval_ts)

    if interval_ts > CLIENT_TIMESTAMP_MAX_INTERVAL:
        log.warn("ERROR!, client_authenticate_request, timestamp too old, possible replay attack...")

        raise ClientAuthException('Client authentication failed, timestamp too old.')
    else:
        log.debug("client_authenticate_request, timestamp validation success...")

    if not public_key or public_key == '':
        log.warn("ERROR!, client_authenticate_request, missing public_key...")

        raise ClientAuthException('Client authentication failed.')

    sig = base64.b64decode(b64sig)

    log.info("client_authenticate_request, encoded b64sig: %s..." % b64sig)
    log.info("client_authenticate_request, decoded b64sig: %s..." % sig)
    log.debug("client_authenticate_request, got parameters: %s..." % request.args.items(multi=True))

    sorted_params = sorted(request.args.items(multi=True))
    sorted_params = ["%s=%s" % (k, v) for k, v in sorted_params]
    sorted_params = reduce(lambda a, b: a+b, sorted_params, '')
    text = auth_ts + request.method + request.script_root + request.path + sorted_params

    log.info("client_authenticate_request, request: %s..." % request)

    if request.data:
        log.info("client_authenticate_request, request.data: %s..." % request.data)

        text += SHA.new(request.data).hexdigest()

    log.info("client_authenticate_request, calculated text for authentication signature: %s..." % text)
    log.info("client_authenticate_request, public_key: %s..." % public_key)

    caller_pubkey = RSA.importKey(public_key).publickey()
    verifier = PKCS1_v1_5.new(caller_pubkey)
    text_hash = SHA.new(text)

    log.info("client_authenticate_request, text_hash: %s..." % text_hash)
    log.info("client_authenticate_request, signature: %s..." % sig)

    if not verifier.verify(text_hash, sig):
        log.warn("ERROR!, client_authenticate_request, text_hash and signature verification failed...")

        raise ClientAuthException('Client authentication failed.')

    log.debug("client_authenticate_request, client authentication success...")