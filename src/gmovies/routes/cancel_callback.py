import datetime
import logging
import sys

from google.appengine.api import urlfetch
from google.appengine.ext.ndb import Key
from google.appengine.runtime.apiproxy_errors import OverQuotaError

from flask import Blueprint, jsonify, request

from gmovies.admin import notify_admin, send_refund_claimcodetx_notification
from gmovies.exceptions.api import *
from gmovies.models import ReservationTransaction, TheaterOrganization
from gmovies.orgs import AYALA_MALLS, GREENHILLS_MALLS, ROCKWELL_MALLS, SM_MALLS, CINEMA76_MALLS, GLOBE_EVENTS
from gmovies.settings import PROMOCODE_SERVER
from gmovies.tx import state, to_state_str
from gmovies.util.admin import convert_timezone, parse_string_to_datetime


log = logging.getLogger(__name__)
cancel_cb = Blueprint('cancel_callback', __name__, template_folder='callback_templates')

AUTH_THEATER_ORGANIZATION = 'X-GMovies-Auth-TheaterOrg'
AUTH_THEATER_CODE = 'X-GMovies-Auth-TheaterCode'

TIMEOUT_DEADLINE = 45
PROMOCODE_RESET_ENDPOINT = PROMOCODE_SERVER + '/reset'

CARDHOLDER_PROMOS = [ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO,
    ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO,
    ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO,
    ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO,
    ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO,
    ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO,
    ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO,
    ReservationTransaction.allowed_payment_types.UNIONBANK_CARDHOLDER_PROMO,
    ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO]

@cancel_cb.errorhandler(APIException)
def handle_api_error(e):
    res = jsonify(message=e.message, error_code=e.error_code, details=e.details)
    res.status_code = e.code

    return res

@cancel_cb.errorhandler(OverQuotaError)
def handle_quota_error(e):
    res = jsonify(message='The backend has reached a resource quota. Please try again later.',  error_code='SYSTEM_OVER_QUOTA', details=e.message)
    res.status_code = 500
    notify_admin("Cancel Callback URL", e, sys.exc_info())

    return res

@cancel_cb.errorhandler(Exception)
def handle_system_error(e):
    res = jsonify(message='There has been a system error. Please try again later.', error_code='SYSTEM_GENERIC_ERROR', details=e.message)
    log.exception(e)
    res.status_code = 500
    notify_admin("Cancel Callback URL", e, sys.exc_info())

    return res

@cancel_cb.before_request
def check_callback_auth():
    log.debug("cancel_callback, entered check_callback_auth()...")
    log.info("headers: {}...".format(request.headers))

    if AUTH_THEATER_ORGANIZATION in request.headers and AUTH_THEATER_CODE in request.headers:
        theaterorg_uuid = request.headers[AUTH_THEATER_ORGANIZATION]
        theaterorg = Key(TheaterOrganization, theaterorg_uuid).get()

        log.debug("cancel_callback, auth params: theaterorg_uuid: %s...", theaterorg_uuid)

        if not theaterorg:
            log.error("ERROR!, cancel_callback, theaterorg not found, cancel callback not allowed...")

            raise ClientAuthException('Failed authentication check.')
    else:
        log.error("ERROR!, cancel_callback, no auth headers, callback not allowed...")

        raise ClientAuthException('Theater organization authentication required.')

@cancel_cb.route('/cancel-transaction', methods=['POST'])
def cancel_transaction():
    log.debug("cancel_transaction, received cancellation request...")

    def __do_reset_claimcode(claim_code, mobnum):
        log.debug("cancel_transaction, __do_reset_claimcode, reseting claimcode %s, from %s..." % (claim_code, PROMOCODE_RESET_ENDPOINT))

        RESET_CLAIMCODE_PARAMETERS = '?promo_code=%s&mobnum=%s' % (claim_code, mobnum)
        RESET_CLAIMCODE_URL = PROMOCODE_RESET_ENDPOINT + RESET_CLAIMCODE_PARAMETERS
        result = urlfetch.fetch(RESET_CLAIMCODE_URL, deadline=TIMEOUT_DEADLINE)

        log.debug("cancel_transaction, __do_reset_claimcode, status_code, %s..." % result.status_code)

    def __do_check_schedule_isexpired(sshow_datetime):
        log.debug("cancel_transaction, __do_check_schedule, start check schedule...")

        is_expired = True
        show_datetime = parse_string_to_datetime(sshow_datetime, fdatetime='%m/%d/%Y %I:%M %p')

        if not show_datetime:
            log.warn("ERROR!, not show_datetime...")

            raise BadValueException('transaction', 'Transaction missing show_datetime.')

        if show_datetime > convert_timezone(datetime.datetime.now(), 8, '+'):
            log.debug("__do_check_schedule, show_datetime allowed cancellation...")

            is_expired = False

        return is_expired

    def __do_cancellation(reservation_reference, refund_reason=None):
        log.debug("cancel_transaction, __do_cancellation, start cancellation...")

        allowed_cancellation_states = [state.TX_DONE]
        allowed_cancellation_paymenttypes = [ReservationTransaction.allowed_payment_types.PROMO_CODE,
            ReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT] + CARDHOLDER_PROMOS
        transaction = ReservationTransaction.query(ReservationTransaction.reservation_reference==reservation_reference).get()

        if not transaction:
            log.warn("ERROR, transaction not found...")

            raise NotFoundException(details={'transaction_code': reservation_reference.split('~')[-1]})

        if transaction.state == state.TX_CANCELLED or transaction.state == state.TX_REFUNDED:
            log.warn("ERROR!, transaction already cancelled...")

            raise AlreadyCancelledException()

        if transaction.state not in allowed_cancellation_states:
            log.warn("ERROR!, transaction state not allowed for cancellation...")

            raise TransactionConflictException([to_state_str(s) for s in allowed_cancellation_states])

        if (transaction.payment_type not in allowed_cancellation_paymenttypes or
            (transaction.payment_type in CARDHOLDER_PROMOS and 'paynamics' != transaction.discount_info['payment_gateway'])):
            log.warn("ERROR!, transaction payment_type not allowed for cancellation...")

            raise BadValueException('transaction', 'Payment type not allowed to cancel transaction.')

        # Expired schedule can still be refunded.
        # if __do_check_schedule_isexpired(transaction.ticket.extra['show_datetime']):
            # log.warn("ERROR!, transaction schedule expired...")

            # raise BadValueException('transaction', "Attempting to cancel for an expired schedule.")

        transaction.state = state.TX_REFUNDED
        transaction.date_cancelled = datetime.datetime.now()
        transaction.refund_reason = refund_reason
        transaction.refunded_by   = "From cancel-transaction api."
        transaction.put()

        log.debug("cancel_transaction, __do_cancellation, transaction cancelled...")

        return transaction

    body = request.values
    has_body = True if body else False

    if not has_body:
        log.warn("ERROR!, cancel_transaction, not has_body...")

        raise BadValueException('body', 'You must supply cancel transaction information.')

    if 'transaction_code' not in request.form:
        log.warn("ERROR!, cancel_transaction, transaction_code not in request.form...")

        raise BadValueException('body', 'Transaction Code is required.')

    transaction_code = request.form['transaction_code']
    refund_reason = "Refund triggered from /cancel-transaction."
    if 'remark' in request.form:
        refund_reason = request.form['remark']
    theaterorg_uuid = request.headers[AUTH_THEATER_ORGANIZATION]
    theater_code = request.headers[AUTH_THEATER_CODE]

    if theaterorg_uuid in [str(ROCKWELL_MALLS), str(GREENHILLS_MALLS), str(CINEMA76_MALLS), str(GLOBE_EVENTS)]:
        log.debug("cancel_callback, allowed theaterorg_uuid, %s..." % theaterorg_uuid)

        reservation_reference = '%s~%s' % (theater_code, transaction_code) if theaterorg_uuid != str(AYALA_MALLS) else transaction_code
        transaction = __do_cancellation(reservation_reference, refund_reason)

        if transaction.payment_type == ReservationTransaction.allowed_payment_types.PROMO_CODE:
            payment_type = transaction.payment_type
            email_address = transaction.payment_info['email'] if 'email' in transaction.payment_info else ''
            full_name = transaction.payment_info['full_name'] if 'full_name' in transaction.payment_info else ''
            claim_code = transaction.payment_info['promo_code'] if 'promo_code' in transaction.payment_info else ''
            mobnum = transaction.payment_info['mobile'] if 'mobile' in transaction.payment_info else ''

            log.debug("reset and send notification, payment_type (%s), claim_code (%s), full_name (%s), email (%s), mobnum (%s)..." % (payment_type, claim_code, full_name, email_address, mobnum))
            log.info("reset and send notification, payment_type (%s), claim_code (%s), full_name (%s), email (%s), mobnum (%s)..." % (payment_type, claim_code, full_name, email_address, mobnum))

            __do_reset_claimcode(claim_code, mobnum)
            send_refund_claimcodetx_notification(email_address, full_name, claim_code)
    else:
        log.warn("cancel_callback, not allowed theaterorg_uuid, %s..." % theaterorg_uuid)

        raise BadValueException('transaction', 'Theater Organization not allowed to cancel transaction.')

    return jsonify(status='ok')
