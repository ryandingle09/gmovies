import sys, traceback
import logging
log = logging.getLogger(__name__)

from google.appengine.ext.ndb import Key, GenericProperty, FloatProperty, IntegerProperty, ComputedProperty, BooleanProperty, DateProperty, KeyProperty, OR, get_multi, transactional
from google.appengine.api import memcache, search
from google.appengine.ext.blobstore import BlobInfo
from flask import Blueprint, jsonify, redirect, url_for, request, make_response, json, render_template

api = Blueprint('api', __name__, template_folder='api_templates')

from uuid import uuid4 as uuid_gen
import base64 
from datetime import datetime, date, timedelta
from math import atan2, cos, radians, sin, sqrt
from operator import attrgetter, itemgetter
from itertools import groupby

import geohash

# gmovies
from gmovies.settings import API_VERSIONS_DEPRECATED, ENFORCE_AUTH, LCT_USERNAME, LCT_PASSWORD, LCT_MERCHANT_KEY

from gmovies import models, tx, orgs
from gmovies.util.id_encoder import decode_uuid, encode_uuid, encoded_theater_id, decoded_theater_id
from gmovies.exceptions.api import *
from gmovies.indexes.geolocation import THEATER_GEO_INDEX_NAME
from gmovies.feeds import get_total_reviews, get_average_rating, sureseats
from gmovies.feeds.available_seats import get_available_seats
from gmovies.feeds.rockwell import read_movies_nowshowing_powerplantmall, read_movies_comingsoon_powerplantmall
from gmovies.admin import notify_admin, password_reset_request
from gmovies.util import admin
from gmovies.registration_whitelist import REGISTRATION_ALLOWED_THEATER_CODES

#Web services calls
from gmovies.ws.connector import WSData, WSAuth, WSSeatInfo, WSUser

#LCT WS Endpoints
from gmovies.ws.endpoints import LCT_SCHEDULES_API, LCT_SESSION_API, LCT_TRANSACTION_API

# This should not be here, REFACTOR THIS
from gmovies.tx.csrc_payment_initiated import bind_listeners

from google.appengine.api.images import get_serving_url

from google.appengine.runtime.apiproxy_errors import OverQuotaError

from .auth import authenticate_request
from .deprecation import deprecated


DATETIME_FORMAT = models.DATETIME_FORMAT+ ' %p'

DEVICE_ID = 'X-GMovies-DeviceId'
AUTH_CLIENT_ID = 'X-GMovies-Auth-ClientId'
AUTH_TIMESTAMP = 'X-GMovies-Auth-Timestamp'
AUTH_SIGNATURE = 'X-GMovies-Auth-Signature'

API_VERSION = 'API0'
SPECIAL_FILTERS = ['diameter', 'sort', 'related']
DEPRECATED_THEATERORGS = [str(orgs.GLOBE), str(orgs.ROTTEN_TOMATOES)]
DEFAULT_THEATERORGS = [str(orgs.AYALA_MALLS), str(orgs.GLOBE),
        str(orgs.ROTTEN_TOMATOES)]

# Maximum distance for query points (more or less) [NB: We don't
# specify antipodal distance, and 200km should be reasonable]
MAX_DISTANCE = 200000

# Default diameter for computation of nearby theaters
DEFAULT_DIAMETER = 3000

IS_INEQUALITY_MATCHER = lambda v: v and (v[0] == '>' or v[0] == '<')
HAS_INEQUALITY_MATCHERS = lambda args: len(filter(IS_INEQUALITY_MATCHER, args.values())) > 0

MGI_THEATER_CODES = ['LCT']


@api.errorhandler(AlreadyCancelledException)
def handle_already_cancelled_error(e):
    res = jsonify(message=e.message, error_code=e.error_code, details=e.details)
    res.status_code = e.code
    for m in e.details:
        res.headers.add('Allow', m)
    return res

@api.errorhandler(APIException)
def handle_api_error(e):
    res = jsonify(message=e.message, error_code=e.error_code, details=e.details)
    res.status_code = e.code
    return res

@api.errorhandler(OverQuotaError)
def handle_quota_error(e):
    res = jsonify(message='The backend has reached a resource quota. Please try again later.', 
                  error_code='SYSTEM_OVER_QUOTA', details=e.message)
    res.status_code = 500
    notify_admin("API", e, sys.exc_info())
    return res

@api.errorhandler(Exception)
def handle_system_error(e):
    errtype, errinst, tb = sys.exc_info()
    traceback_desc = traceback.format_exception(errtype, errinst, tb)

    res = jsonify(message='There has been a system error. Please try again later.',
                  error_code='SYSTEM_GENERIC_ERROR', details=e.message)

    log.info("File: {}. Line# {}. Error_details: {}".format(tb.tb_frame.f_code.co_filename,str(sys.exc_traceback.tb_lineno), e))
    log.exception(e)
    res.status_code = 500
    notify_admin("API", e, sys.exc_info())
    return res

def abort(status_code, message='Error', error_code='ERROR_UNKNOWN',
          details=None, headers={}):
    raise APIException(status_code, message, error_code, details)

def check_filter_and_sort_args(cls, args):
    blacklist = [] if not hasattr(cls, 'blacklist') else cls.blacklist
    whitelist = SPECIAL_FILTERS
    if hasattr(cls, 'whitelist'):
        whitelist += cls.whitelist

    check = filter(lambda tup: (not tup[1] and tup[0] not in whitelist and tup[0] != 'sort') or tup[0] in blacklist,
                   [ (k, resolve_dotted_name(cls, k)) for k in args ])

    log.info("Filter args check: %s" % check)
    log.debug("Filter args check: %s" % check)

    if len(check) > 0:
        unknown_filter_args = [ k for k,v in check ]
        log.info("Failed filter args check: %s" % unknown_filter_args)
        log.debug("Failed filter args check: %s" % unknown_filter_args)
        raise InvalidFilterPropertyException([ k for k,v in check ])

    def val_typecheck((k, v)):
        try:
            attr = resolve_dotted_name(cls, k)
            if IS_INEQUALITY_MATCHER(v):
                if v[1] == '=':
                    coerce_val(cls, attr, k, v[2:])
                else:
                    coerce_val(cls, attr, k, v[1:])
            else:
                coerce_val(cls, attr, k, v)
            return False
        except:
            return True
    typecheck = filter(val_typecheck, [ (k, args[k]) for k in args ])

    log.info("Filter args typecheck: %s", typecheck)
    log.debug("Filter args typecheck: %s", typecheck)

    if len(typecheck) > 0:
        typecheck_fail_args = [ k for k,v in typecheck ]
        log.info("Failed args typecheck: %s", typecheck_fail_args)
        log.debug("Failed args typecheck: %s", typecheck_fail_args)
        raise BadValueException(typecheck_fail_args, 'Wrong type for filter')


    # Special case: filter condition is 'is_showing'; only
    # accept one value to filter against
    if 'is_showing' in args and len(args.getlist('is_showing')) != 1:
        raise BadValueException('is_showing', 'Too many filters')

    if 'related' in args and cls != models.Schedule:
        raise InvalidOrderPropertyException([ 'related' ])

    if 'sort' in args:
        sort_cols = [ col if col[0] != '-' else col[1:] 
                      for col in args.getlist('sort') ]

        check = filter(lambda tup: (not tup[1] and tup[0] not in whitelist) or tup[0] in blacklist,
                       [ (k, resolve_dotted_name(cls, k)) for k in sort_cols ])

        if len(check) > 0:
            unknown_sort_args = [ k for k,v in check ]
            raise InvalidOrderPropertyException(unknown_sort_args)


    ineq = filter(lambda vtup: IS_INEQUALITY_MATCHER(vtup[1]), args.items())
    ineq = set([ kv[0] for kv in ineq ])
    if len(ineq) > 1:
        log.warn("Inequality filters against multiple properties: %s" % ineq)
        raise InvalidFilterPropertyException(ineq)

@api.before_request
def check_device_id():
    if DEVICE_ID not in request.headers:
        raise DeviceIdRequiredException()

@api.before_request
def check_api_version():
    log.debug("check_api_version, api version0...")

    if API_VERSION in API_VERSIONS_DEPRECATED:
        raise abort(401, error_code='API_VERSION_DEPRECATED', message='Client API version deprecated.')

@api.before_request
def log_caller_info():
    if 'User-Agent' in request.headers:
        ua = request.headers['User-Agent']
    else:
        ua = '(None)'
    if AUTH_CLIENT_ID in request.headers:
        client_id = request.headers[AUTH_CLIENT_ID]
    else:
        client_id = "(Not given)"
    device_id = request.headers[DEVICE_ID]
    log.info("API:\tUser-Agent: %s", ua)
    log.info("API:\tClient ID: %s\n\tDevice ID: %s" % (client_id, device_id))


def coerce_val(cls, attr, k, v):
    if isinstance(attr, FloatProperty):
        return float(v)
    elif isinstance(attr, IntegerProperty):
        return int(v)
    elif (isinstance(attr, ComputedProperty) and k[:3] == 'is_') \
         or isinstance(attr, BooleanProperty):
        v = v.lower()
        if v == 'true' or not v:
            return True
        elif v == 'false':
            return False
        else:
            raise BadValueException(k, v)
    elif isinstance(attr, DateProperty):
        return datetime.strptime(v, models.DATE_FORMAT)
    elif isinstance(attr, KeyProperty):
        return to_key_instance(k, v)
    else:
        return v

def to_key_instance(attr_name, v):
    attr_to_key_type = {
        'theater': to_theater_key,
        'movie': lambda v: Key(models.Movie, v)
    }
    return attr_to_key_type[attr_name](v)

def to_theater_key(enc_theater_id):
    org_uuid, theater_id = decoded_theater_id(enc_theater_id)
    return Key(models.TheaterOrganization, str(org_uuid), models.Theater, theater_id)

def resolve_dotted_name(cls, k):
    if hasattr(cls, 'filter_mappings') and k in cls.filter_mappings:
        k = cls.filter_mappings[k]

    parts = k.split('.')

    root = cls
    for part in parts:
        if not hasattr(root, part):
            return None
        root = getattr(root, part)

    return root

def do_filter(cls, q, args):
    f = {}
    special_case_sort = False

    for k in args:
        if k != 'sort' and k != 'related':
            if 'match' not in f:
                f['match'] = {}

            name = k.split('.')[-1]

            vals = args.getlist(k)
            prop = resolve_dotted_name(cls, k)
            log.debug("Filter: %s (against %s)" % (prop, vals))

            if cls == models.Movie or cls == models.Promo:
                if k == "resolution" or k == "application_type":
                    if len(vals) > 1:
                        f['match'][k] = vals
                    else:
                        f['match'][k] = vals[0]

                    continue

            # Check if any of the values specified is prefixed with an
            # inequality matcher
            if len(filter(IS_INEQUALITY_MATCHER, vals)) > 0:
                # For inequalities, there is always an implicit sort
                # applied to property being mapped as inequal
                q = q.order(prop)
                for v in vals:
                    if v[:2] == '>=':
                        log.debug("Ineq: >= %s" % k)
                        q = q.filter(prop >= coerce_val(cls, prop, name, v[2:]))
                    elif v[:2] == '<=':
                        log.debug("Ineq: <= %s" % k)
                        q = q.filter(prop <= coerce_val(cls, prop, name, v[2:]))
                    elif v[:1] == '>' and v[:2] != '>=':
                        log.debug("Ineq: > %s" % k)
                        q = q.filter(prop > coerce_val(cls, prop, name, v[1:]))
                    elif v[:1] == '<' and v[:2] != '<=':
                        log.debug("Ineq: < %s" % k)
                        q = q.filter(prop < coerce_val(cls, prop, name, v[1:]))
                    else:
                        raise APISystemError("Strange, we got an inequality matcher that doesn't match...")
            else:
                q = q.filter(prop.IN([ coerce_val(cls, prop, name, v)
                                       for v in vals ]))

            if len(vals) > 1:
                f['match'][k] = vals
            else:
                f['match'][k] = vals[0]

    if 'sort' in args:
        sort_cols = args.getlist('sort')
        f['sort'] = sort_cols
        q = apply_sort(q, cls, sort_cols)

    return (f, q)

def apply_sort(q, cls, sort_cols):
    for col in sort_cols:
        if col[0] == '-':
            prop = resolve_dotted_name(cls, col[1:])
            q = q.order(-prop)
        else:
            q = q.order(resolve_dotted_name(cls, col))

    return q

def do_location_search(request):
    # TODO: Assert that only args are location-related; we can't apply
    # additional filters as well
    lat = request.args['location.lat']
    lon = request.args['location.lon']

    if request.path[:7] == '/api/0/':
        api_version = 'ver1'
    elif request.path[:7] == '/api/2/':
        api_version = 'ver2'
    else:
        api_version = None

    if 'diameter' in request.args:
        diameter = request.args['diameter']
    else:
        diameter = DEFAULT_DIAMETER

    # Naive caching via geohash + diamter
    cache_prefix = geohash.encode(float(lat), float(lon))
    cache_key = "geo::%s::%s::%s" % (api_version, cache_prefix, diameter)
    cache_hit = memcache.get(cache_key)

    if cache_hit:
        log.debug("Geolocation cache hit on: %s" % cache_key)
        return cache_hit

    dist_fn = "distance(geopoint(%s,%s), loc)" % (lat, lon)
    q_str = "%s < %s" % (dist_fn, diameter)
    log.debug("Geolocation query: %s" % q_str)
    idx = search.Index(name=THEATER_GEO_INDEX_NAME)
    sort_expr = search.SortExpression(expression=dist_fn,
                    direction=search.SortExpression.ASCENDING,
                    default_value=MAX_DISTANCE)
    q_opt = search.QueryOptions(
                    sort_options=search.SortOptions(expressions=[sort_expr]))
    q = search.Query(query_string=q_str, options=q_opt)

    try:
        key_docs = idx.search(q)
        log.debug("key_docs: %s" % key_docs)
        theater_keys = [to_theater_key(doc.doc_id) for doc in key_docs]
        theater_keys = remove_keys('do_location_search', theater_keys, api_version)

        memcache.set(cache_key, (lat, lon, theater_keys))

        return lat, lon, theater_keys
    except (search.Error), e:
        log.warn("Couldn't search: %s" % e)
        return lat, lon, []

def do_computed_distance(latitude, longitude, theater):
    radius = 6371 # Earth's Radius in kilometers
    latitude = float(latitude)
    longitude = float(longitude)
    theater_latitude = float(theater.location.lat)
    theater_longitude = float(theater.location.lon)

    longitude1, latitude1, longitude2, latitude2 = map(radians,
            [longitude, latitude, theater_longitude, theater_latitude])

    dlongitude = longitude2 - longitude1
    dlatitude = latitude2 - latitude1
    a = ((sin(dlatitude/2))**2 + cos(latitude1) * cos(latitude2) *
            (sin(dlongitude/2))**2)
    c = 2 * atan2(sqrt(a), sqrt(1-a))
    distance = radius * c

    return distance

def remove_keys(method_name, keys, api_version=None):
    for key in [nkey for nkey in keys]:
        if method_name == 'do_get_schedules':
            theater = key.parent().get()
        else:
            theater = key.get()

        if theater is not None:
            theaterorg_key = theater.key.parent()
            theaterorg = theaterorg_key.get()

            if str(theaterorg_key.id()) in DEFAULT_THEATERORGS and api_version == 'ver1':
                if not theater.is_published:
                    keys.remove(key)
            else:
                if str(theaterorg_key.id()) in DEPRECATED_THEATERORGS or api_version == 'ver1':
                    keys.remove(key)
                else:
                    if not theater.is_published or not theaterorg.is_published:
                        keys.remove(key)

    return keys

def query_model(cls, entity_name, request, id_encoder=lambda x:x.id()):
    check_filter_and_sort_args(cls, request.args)
    q = cls.query()
    f, q = query_filters(cls, request)

    if cls == models.Movie:
        posters_args = {}
        posters_args['resolution'] = request.args.get('resolution', 'uploaded')
        posters_args['application_type'] = request.args.get('application_type', 'mobile-app')

        portrait_default_poster, landscape_default_poster = get_default_poster_url(posters_args=posters_args)

        # Temporary, movies_ids, total_reviews, and averating_rating will remove in the future.
        movie_ids = q.fetch(keys_only=True)
        total_reviews = get_total_reviews(movie_ids=movie_ids)
        average_rating = get_average_rating(movie_ids=movie_ids)

        l = [{'id': id_encoder(x.key),
                entity_name: x.to_entity(total_reviews=total_reviews,
                        average_rating=average_rating,
                        posters_args=posters_args,
                        portrait_default_poster=portrait_default_poster,
                        landscape_default_poster=landscape_default_poster)} for x in q]
    else:
        l = [{'id': id_encoder(x.key), entity_name: x.to_entity()} for x in q]

    return (f, l)

def query_filters(cls, request):
    if cls == models.Theater:
        q = []
        ayala_key = Key(models.TheaterOrganization, str(orgs.AYALA_MALLS))
        globe_key = Key(models.TheaterOrganization, str(orgs.GLOBE))
        rotten_tomatoes_key = Key(models.TheaterOrganization,
                    str(orgs.ROTTEN_TOMATOES))
        theaterorgs = models.TheaterOrganization.query(OR(
                    models.TheaterOrganization.key == ayala_key,
                    models.TheaterOrganization.key == globe_key,
                    models.TheaterOrganization.key == rotten_tomatoes_key))
        for org in theaterorgs:
            theaters = cls.query(cls.is_published == True,
                            ancestor=org.key)
            f, theaters = do_filter(cls, theaters, request.args)
            q.extend(theaters.fetch())
    else:
        q = cls.query()
        f, q = do_filter(cls, q, request.args)

    return (f, q)

@api.route('/')
def api_meta():
    return jsonify({ 'version' : '1.19.0' })

def org_with_theaters(org):
    theater_keys = models.Theater.query(
                        models.Theater.is_published == True,
                        ancestor=org.key).order(
                                models.Theater.name).fetch(keys_only=True)

    theaters = [encoded_theater_id(theater_key) for theater_key in theater_keys]
    entity = org.to_entity()
    entity['theaters'] = theaters

    return entity

@api.route('/theaterorgs/')
def get_theater_orgs():
    check_filter_and_sort_args(models.TheaterOrganization, request.args)
    ayala_key = Key(models.TheaterOrganization, str(orgs.AYALA_MALLS))
    globe_key = Key(models.TheaterOrganization, str(orgs.GLOBE))
    rotten_tomatoes_key = Key(models.TheaterOrganization,
            str(orgs.ROTTEN_TOMATOES))

    q = models.TheaterOrganization.query(
            models.TheaterOrganization.key.IN([ayala_key, globe_key,
                    rotten_tomatoes_key]))
    f, q = do_filter(models.TheaterOrganization, q, request.args)
    l = [{'id': org.key.id(), 'theater_organization': org_with_theaters(org)}
            for org in q]

    return jsonify({ 'filters': f, 'results': l })

@api.route('/theaterorgs/<theaterorg_id>/')
def get_theaterorg_info(theaterorg_id):
    org_key = Key(models.TheaterOrganization, theaterorg_id)
    org = org_key.get()

    if not org:
        raise NotFoundException(details={ 'theaterorg_id':
                                          theaterorg_id })

    return jsonify({ 'id': theaterorg_id, 
                     'theater_organization':  org_with_theaters(org) })

@api.route('/theaterorgs/<theaterorg_id>/theaters/')
def get_theaterorg_theaters(theaterorg_id):
    org_key = Key(models.TheaterOrganization, theaterorg_id)
    org = org_key.get()

    if not org:
        raise NotFoundException(details={ 'theaterorg_id':
                                          theaterorg_id })

    if (('location.lon' in request.args and 
        'location.lat' in request.args) or
        ('cinemas.location.lon' in request.args
         and 'cinemas.location.lat' in request.args)):
        lat, lon, theater_keys = do_location_search(request)
        org_theater_keys = models.Theater.query(ancestor=org_key).fetch(keys_only=True)

        matched_keys = set(theater_keys).intersection(set(org_theater_keys))
        theater_list = [ { 'id': encoded_theater_id(x.key), 'theater': x.to_entity() }
                         for x in get_multi(matched_keys) if x is not None]
        filters = { 'match': { 'location.lat': lat, 'location.lon': lon } }
    else:
        check_filter_and_sort_args(models.Theater, request.args)
        q = models.Theater.query(ancestor=org_key)
        filters, q = do_filter(models.Theater, q, request.args)
        theater_list = [ { 'id': encoded_theater_id(x.key), 'theater': x.to_entity() } 
                         for x in q ]

    return jsonify({ 'filters': filters, 'results': theater_list })

def theaterorg_theater_safety_check(theaterorg_id, ignored, theater_id):
    # HACK: Ensure we don't allow lookups for theaters with wrong encoded parts
    if str(decode_uuid(ignored)) != theaterorg_id:
        raise NotFoundException(details={ 'theaterorg_id': theaterorg_id, 
                                          'theater_id': "%s~%s" % (ignored, theater_id) })

@api.route('/theaterorgs/<theaterorg_id>/theaters/<uncoded_id>/')
def dummy_theaterorg_theater_info(theaterorg_id, uncoded_id):
    raise NotFoundException(details={ 'theaterorg_id': theaterorg_id,
                                      'theater_id': uncoded_id })

@api.route('/theaterorgs/<theaterorg_id>/theaters/<ignored>~<int:theater_id>/')
def get_theaterorg_theater_info(theaterorg_id, ignored, theater_id):
    theaterorg_theater_safety_check(theaterorg_id, ignored, theater_id)
    return get_theater_info_common(theaterorg_id, ignored, theater_id)

@api.route('/theaters/')
def get_theaters():
    if (('location.lon' in request.args and 
        'location.lat' in request.args) or
        ('cinemas.location.lon' in request.args
         and 'cinemas.location.lat' in request.args)):
        lat, lon, theater_keys = do_location_search(request)
        theater_list = [ { 'id': encoded_theater_id(x.key), 'theater': x.to_entity() }
                         for x in get_multi(theater_keys) if x is not None]
        filters = { 'match': { 'location.lat': lat, 'location.lon': lon } }
    else:
        filters, theater_list = query_model(models.Theater, 'theater', request, id_encoder=encoded_theater_id)

    return jsonify({ 'filters': filters, 'results': theater_list })

@api.route('/theaters/<uncoded_id>/')
def dummy_theater_info(uncoded_id):
    raise NotFoundException(details={ 'theater_id': uncoded_id })

@api.route('/theaters/<enc_org_uuid>~<int:theater_id>/')
def get_theater_info(enc_org_uuid, theater_id):
    org_uuid = decode_uuid(enc_org_uuid)
    return get_theater_info_common(str(org_uuid), enc_org_uuid, theater_id)

@api.route('/theaters/<enc_org_uuid>~<int:theater_id>/min/')
def get_theater_info_minimum(enc_org_uuid, theater_id):
    org_uuid = decode_uuid(enc_org_uuid)
    return get_theater_info_common(str(org_uuid), enc_org_uuid, theater_id, is_minimum_info=True)

def get_theater_info_common(org_uuid, enc_org_uuid, theater_id, is_minimum_info=False):
    theater = Key(models.TheaterOrganization, org_uuid, 
            models.Theater, theater_id).get()

    if not theater:
        raise NotFoundException(
                details={'theater_id': "%s~%s" % (enc_org_uuid, theater_id)})

    if is_minimum_info:
        return jsonify({'id': theater_id, 'theater': theater.to_minimum_entity()})

    return jsonify({'id': theater_id, 'theater': theater.to_entity()})

@api.route('/movies/')
def get_movies():
    filters, movie_list = query_model(models.Movie, 'movie', request)
    return jsonify({ 'filters': filters, 'results': movie_list })

@api.route('/movies/<movie_id>/')
def get_movie(movie_id):
    posters_args = {}
    posters_args['resolution'] = request.args.get('resolution', 'uploaded')
    posters_args['application_type'] = request.args.get('application_type', 'mobile-app')

    portrait_default_poster, landscape_default_poster = get_default_poster_url(posters_args=posters_args)
    movie = Key(models.Movie, movie_id).get()

    if not movie:
        raise NotFoundException(details={ 'movie_id': movie_id })

    return jsonify({'id': movie_id,
            'movie': movie.to_entity_movie_info(posters_args=posters_args,
                        portrait_default_poster=portrait_default_poster,
                        landscape_default_poster=landscape_default_poster)})


@api.route('/movies/<movie_id>/poster/')
def get_movie_poster(movie_id):
    posters_args = {}
    posters_args['resolution'] = request.args.get('resolution', 'uploaded')
    posters_args['application_type'] = request.args.get('application_type', 'mobile-app')

    orientation = request.args.get('orientation', 'landscape')
    movie_key = Key(models.Movie, movie_id)
    movie = movie_key.get()

    if not movie:
        raise NotFoundException(details={'movie_id': movie_id})

    portrait_default_poster, landscape_default_poster = get_default_poster_url(posters_args=posters_args)
    portrait_poster, landscape_poster = movie.get_poster_url(posters_args=posters_args)

    if orientation == 'landscape':
        if landscape_poster:
            poster = landscape_poster
        else:
            poster = landscape_default_poster
    else:
        if portrait_poster:
            poster = portrait_poster
        else:
            poster = portrait_default_poster

    if not poster:
        raise NotFoundException(details={'movie_id': movie_id,
                'orientation': orientation, 'resolution': posters_args['resolution'],
                'application_type': posters_args['application_type']})

    return redirect(poster)


# Seat Availability API
@api.route('/available_seats')
def translate_and_get_available_seats():
    available = []
    remaining_seats = 0
    enc_theater_id = ''
    sched_id = ''
    error_message = 'Unable to load the seats due to an unexpected error. Please try again later.'

    try:
        if 'theater' not in request.args:
            raise BadValueException('theater', 'You must specify a theater')
        if 'schedule' not in request.args:
            raise BadValueException('schedule', 'You must specify a schedule')
        if 'time' not in request.args:
            raise BadValueException('time', 'You must specify a time')

        enc_theater_id = request.args['theater']
        sched_id_raw = request.args['schedule']
        sched_id = sched_id_raw.split(',')[0]
        sched_time = datetime.strptime(request.args['time'], models.TIME_FORMAT).time()
        theater_key = to_theater_key(enc_theater_id)
        theater = theater_key.get()

        log.info("theater_name: %s" % theater.name)

        if not theater:
            raise NotFoundException(details={'theater': theater_id})

        theater_code = theater.org_theater_code

        log.info("theater_code: %s, allow_reservation: %s" % (theater_code, theater.allow_reservation))

        if 'platform' in request.args:
            platform = request.args['platform'].lower()

            if platform == 'android' and theater.error_message_seatmaps_android:
                error_message = theater.error_message_seatmaps_android
            elif platform == 'ios' and theater.error_message_seatmaps_ios:
                error_message = theater.error_message_seatmaps_ios

        if not theater.allow_reservation:
            res = {'theater': enc_theater_id, 'schedule': sched_id,
                    'available_seat_map': available, 'available_seats': remaining_seats,
                    'error_message': error_message}

            return jsonify(res)

        schedule = Key(models.Schedule, sched_id, parent=theater.key).get()

        if not schedule:
            raise NotFoundException(details={'schedule': sched_id})

        sched_slot = None

        for sl in schedule.slots:
            if sl.start_time == sched_time:
                sched_slot = sl

                break

        if not sched_slot:
            raise NotFoundException(details={'time': request.args['time']})

        if schedule.show_date < date.today():
            raise BadValueException('schedule', 'Expired schedule.')

        sched_code = sched_slot.feed_code_schedule
        sched_slot_code = sched_slot.feed_code
        seating_type = sched_slot.seating_type

        log.info("Sched Code: %s" % sched_code)
        log.info("Sched Slot Code: %s" % sched_slot_code)

        # Organization API lookup here; for now, hard-coded to use
        # Sureseats, Rockwell, and Megaworld - LCT API directly
        if str(theater_key.parent().id()) == str(orgs.AYALA_MALLS): # Get Available Seats for Sureseats
            log.info('Sureseats: get_available_seats...')

            status, available, remaining_seats = get_available_seats(theater_key.parent().id(), theater_code, sched_slot_code)
        elif str(theater_key.parent().id()) == str(orgs.ROCKWELL_MALLS): # Get Available Seats for Rockwell
            log.info('Rockwell: get_available_seats...')

            status, available, remaining_seats = get_available_seats(theater_key.parent().id(), sched_code, sched_slot_code)
        elif str(theater_key.parent().id()) == str(orgs.GREENHILLS_MALLS): # Get Available Seats for Greenhills
            log.info('Greenhills: get_available_seats...')

            schedslotcode_seatingtype = '%s::%s' % (sched_slot_code, seating_type)
            status, available, remaining_seats = get_available_seats(theater_key.parent().id(), sched_code, schedslotcode_seatingtype)
        elif str(theater_key.parent().id()) == str(orgs.SM_MALLS): # Get Available Seats for SM Malls
            log.info('SM Malls: get_available_seats...')

            status, available, remaining_seats = get_available_seats(theater_key.parent().id(), theater, sched_slot_code, arg4=True)
        elif str(theater_key.parent().id()) == str(orgs.MEGAWORLD_MALLS): # Get Available Seats for Megaworld
            log.info('Megaworld: get_available_seats...')

            if theater_code in MGI_THEATER_CODES:
                log.info('MGI: get_available_seats...')

                status, available, remaining_seats = get_available_seats(theater_key.parent().id(), theater, sched_slot_code, arg3='MGI')
            else:
                log.warn('ERROR!, missing third party available seats endpoint...')

                status = 'error'
        else:
            log.warn('ERROR!, missing third party available seats endpoint...')

            status = 'error'

        if status == 'success':
            error_message = ''
    except Exception, e:
        errtype, errinst, tb = sys.exc_info()
        traceback_desc = traceback.format_exception(errtype, errinst, tb)

        log.info('ERROR!, Fetching Available Seats...')
        log.info('line# %s: %s' % (str(sys.exc_traceback.tb_lineno), e))

    available_seats = {'theater': enc_theater_id, 'schedule': sched_id,
            'available_seat_map': available, 'available_seats': remaining_seats,
            'error_message': error_message}

    return jsonify(available_seats)

# Attachments

ENTITY_ATTACHMENT_PARENT_MODEL = { 'movies': models.Movie, 'theaters': models.Theater }
ENTITY_KEY_FUNC = { 'movies': lambda e_id: Key(models.Movie, e_id), 
                    'theaters': lambda e_id: to_theater_key(e_id) }

@api.route('/<entity_class>/<entity_id>/image')
def get_entity_image(entity_class, entity_id):
    if entity_class not in ENTITY_ATTACHMENT_PARENT_MODEL:
        raise NotFoundException()

    cls = ENTITY_ATTACHMENT_PARENT_MODEL[entity_class]
    ent_key = ENTITY_KEY_FUNC[entity_class](entity_id)

    assert isinstance(ent_key, Key), ent_key
    q = models.MultiResImageBlob.query(ancestor=ent_key)

    res = 'mdpi'
    if 'device_resolution' in request.args:
        res = request.args['device_resolution'].lower()
    q = q.filter(models.MultiResImageBlob.resolution == res)

    if q.count() != 1:
        raise NotFoundException(details={ entity_class: entity_id, 'attachment': 'image' })

    multi_image = q.fetch(1)[0]

    return redirect(get_serving_url(multi_image.image, 
                                    size=0, crop=False))

@api.route('/schedules')
def get_schedules():
    return do_get_schedules()

@api.route('/theaterorgs/<theaterorg_id>/theaters/<ignored>~<int:theater_id>/schedules')
def get_theater_org_theater_schedules(theaterorg_id, ignored, theater_id):
    theaterorg_theater_safety_check(theaterorg_id, ignored, theater_id)
    return get_theater_schedules(ignored, theater_id)

@api.route('/theaters/<enc_org_uuid>~<int:theater_id>/schedules')
def get_theater_schedules(enc_org_uuid, theater_id):
    log.debug("Got theater ID pair %s %s" % (enc_org_uuid, theater_id))
    return do_get_schedules(theater_pair=(enc_org_uuid,theater_id))

@api.route('/movies/<movie_id>/schedules')
def get_movie_schedules(movie_id):
    return do_get_schedules(movie_id=movie_id)

def get_schedule_query_filter_keyset(theater_pair, movie_id, args):
    theater_keys = None
    movie_keys = []

    if theater_pair != None:
        org_uuid = decode_uuid(theater_pair[0])
        theater_key = Key(models.TheaterOrganization, str(org_uuid),
                          models.Theater, theater_pair[1])

        if 'theater' in args:
            raise BadValueException('theater', 'Additional theater filter not allowed here')

        theater_keys = [ theater_key ]
    elif movie_id != None:
        movie_key = Key(models.Movie, movie_id)

        if not movie_key.get():
            raise NotFoundException(details={ 'movie_id': movie_id })

        if 'movie' in args:
            raise BadValueException('movie', 'Additional movie filter not allowed here')

        movie_keys.append(movie_key)

    if 'theater' in args:
        if theater_keys == None:
            theater_keys = []

        theater_keys.extend([ to_theater_key(t_id) for t_id in
                              args.getlist('theater') ])
        args.pop('theater')

    if 'movie' in args:
        movie_keys.extend([ Key(models.Movie, m_id) for m_id in
                            args.getlist('movie') ])
        args.pop('movie')

    if ('location.lon' in args and 
        'location.lat' in args) and movie_id != None:
        if theater_keys == None:
            theater_keys = []

        lat, lon, loc_theater_keys = do_location_search(request)
        theater_keys.extend(loc_theater_keys)

        args.pop('location.lon', None)
        args.pop('location.lat', None)

        if 'diameter' in request.args:
            args.pop('diameter', None)

    return theater_keys, movie_keys

def strip_unmatched_slots(results, movie_keys):
    for sched in results:
        filt_slots = filter(lambda s: s.movie in movie_keys,
                            sched.slots)
        sched.slots = filt_slots

    return filter(lambda sched: len(sched.slots) > 0, 
                  results)

def do_get_schedules(theater_pair=None, movie_id=None):
    check_filter_and_sort_args(models.Schedule, request.args)
    args = request.args.copy()
    special_case_sort = False

    if request.path[:7] == '/api/0/':
        api_version = 'ver1'
    elif request.path[:7] == '/api/2/':
        api_version = 'ver2'
    else:
        api_version = None

    if 'sort' in args:
        sort_cols = args.getlist('sort')
        special_case_sort = sort_cols and (('theater' in sort_cols) or HAS_INEQUALITY_MATCHERS(args))

    if special_case_sort and 'sort' in args:
        args.pop('sort')

    theater_keys, movie_keys = get_schedule_query_filter_keyset(theater_pair, movie_id, args)

    f = { }
    result_keys = [ ]
    if theater_keys != None:
        for t in theater_keys:
            base_q = models.Schedule.query(ancestor=t)
            if movie_keys:
                if len(movie_keys) > 1:
                    base_q = base_q.filter(models.Schedule.slots.movie.IN(movie_keys))
                else:
                    base_q = base_q.filter(models.Schedule.slots.movie == movie_keys[0])

            f, q = do_filter(models.Schedule, base_q, args)
            result_keys.extend(q.fetch(keys_only=True))
    else:
        q = models.Schedule.query()
        if movie_keys:
            if len(movie_keys) > 1:
                q = q.filter(models.Schedule.slots.movie.IN(movie_keys))
            else:
                q = q.filter(models.Schedule.slots.movie == movie_keys[0])
        f, q = do_filter(models.Schedule, q, args)
        result_keys = q.fetch(keys_only=True)

    result_keys = remove_keys('do_get_schedules', result_keys, api_version)

    results = get_multi(result_keys)
    if movie_keys:
        results = strip_unmatched_slots(results, movie_keys)

    if special_case_sort:
        log.info("Special case triggered: manually sorting results")
        # Populate special: theater
        for s in results:
            s.theater = s.key.parent().get().name

        results = sorted(results, key=attrgetter(*sort_cols))

        f['sort'] = sort_cols

    # Since Schedule.to_entities() returns a list of API entities that
    # a particular schedule may map to, we will need to flatten it
    # before passing to clients
    l = []
    if api_version == 'ver2':
        if 'location.lat' in request.args and 'location.lon' in request.args:
            lat = request.args['location.lat']
            lon = request.args['location.lon']

            if 'diameter' in request.args:
                diameter = request.args['diameter']
            else:
                diameter = DEFAULT_DIAMETER

            for s in results:
                distance = do_computed_distance(lat, lon, s.key.parent().get())
                distance_in_meters = distance * float(1000)

                if round(distance_in_meters) < float(diameter):
                    for s_id, s_ent in s.to_entities():
                        s_dict = {}
                        s_dict['id'] = s_id
                        s_dict['schedule'] = s_ent
                        s_dict['distance'] = distance

                        l.append(s_dict)
        elif 'seating_type' in request.args:
            SEATING_TYPE = request.args['seating_type']

            for result in results:
                for result_id, result_entities in result.to_entities():
                    if result_entities["seating_type"] == SEATING_TYPE:
                        l.append({'id': result_id, 'schedule': result_entities})
    
    if not l:
        l = [ { 'id': s_id, 'schedule': s_ent }
            for s in results
            for s_id, s_ent in s.to_entities() ]

    rel_key = None
    if 'related' in request.args and (theater_pair or movie_id):
        rel_key = 'movie' if theater_pair else 'theater'
        rel = list(set([ s['schedule'][rel_key] for s in l ]))

        return jsonify(filters=f, results=l, related=rel)
    else:
        return jsonify(filters=f, results=l)

@api.route('/csrc-payment/', methods=['POST'])
def start_csrc_payment():
    # extract payment details and start recording process
    deposit_info = request.json
    log.info("Bank Deposit: {}".format(deposit_info))
    device_id = request.headers[DEVICE_ID]
    payment_proc = models.CSRCPayment.start_process(device_id, deposit_info)
    log.info("Payment Process: {}".format(payment_proc))
    return jsonify({"id": payment_proc.key.id()})

@api.route('/csrc-payment/<proc_id>/state', methods=['GET'])
def get_csrc_payment_state(proc_id):
    device_id = request.headers[DEVICE_ID]
    device_key = Key(models.Device, device_id)
    proc = models.CSRCPayment.get_by_id(proc_id, parent=device_key)
    return jsonify({"id": proc_id, "state": models.CSRCPayment.stringify_state(proc.state)})

@api.route('/csrc-payment/<proc_id>/form-data', methods=['GET'])
def show_bank_deposit_form(proc_id):
    device_id = request.headers[DEVICE_ID]
    device_key = Key(models.Device, device_id)
    csrc_instance = models.CSRCPayment.get_by_id(proc_id, parent=device_key)
    form_data = csrc_instance.get_form_data()
    log.info("BD Data: {}".format(form_data))
    #bind a listener for this process / REFACTOR THIS!!!
    bind_listeners(csrc_instance)
    return jsonify({"id": proc_id, "form_parameters": form_data})

@api.route('/tx/', methods=['GET'])
def get_device_txs():
    device_id = request.headers[DEVICE_ID]
    device_key = Key(models.Device, device_id)
    q = models.ReservationTransaction.query(ancestor=device_key)
    state_filter = None
    if 'states' in request.args:
        state_strs = request.args.getlist('states')
        states = filter(None, [ getattr(tx.state, s) if hasattr(tx.state, s) else None 
                                for s in state_strs ])
        log.debug("States to match: %s" % states)
        q = q.filter(models.ReservationTransaction.state.IN(states))
        state_filter = state_strs
    else:
        q = q.filter(models.ReservationTransaction.state != tx.state.TX_DONE)

    if 'sort' in request.args:
        sort_cols = [ col if col[0] != '-' else col[1:] 
                      for col in request.args.getlist('sort') ]

        check = filter(lambda tup: not tup[1],
                       [ (k, resolve_dotted_name(models.ReservationTransaction, k)) 
                         for k in sort_cols ])

        if len(check) > 0:
            unknown_sort_args = [ k for k,v in check ]
            raise InvalidOrderPropertyException(unknown_sort_args)

        q = apply_sort(q, models.ReservationTransaction,
                       request.args.getlist('sort'))

    txs = [ k.id() for k in q.fetch(keys_only=True) ]

    return jsonify({ 'states': state_filter, 'transactions': txs })


@api.route('/tx/<tx_id>/tickets/', methods=['GET'])
def get_tx_tickets(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = tx.query_info(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={'tx_id': tx_id})

    queryset = models.TicketImageBlob.query(ancestor=transaction.key)
    queryset = apply_sort(queryset, models.TicketImageBlob, request.args.getlist('sort'))
    tx_ticket_ids = [ticket.id() for ticket in queryset.fetch(keys_only=True)]

    return jsonify({'id': tx_id, 'transactions': tx_ticket_ids})


@api.route('/tx/<tx_id>/tickets/<ticket_id>/', methods=['GET'])
def get_ticket_info(tx_id, ticket_id):
    device_id = request.headers[DEVICE_ID]
    transaction = tx.query_info(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={'tx_id': tx_id})

    tx_ticket_image = Key(models.TicketImageBlob, ticket_id, parent=transaction.key).get()
    ticket_image = tx_ticket_image.to_entity(transaction)

    return jsonify({'id': tx_id, 'ticket_id': ticket_id,
            'transaction': ticket_image})

@api.route('/tx/<tx_id>/min/', methods=['GET'])
def get_minimum_ticket_info(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = tx.query_info(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={ 'tx_id': tx_id })

    trans = transaction.to_minimum_entity()

    return jsonify({'id' : tx_id, 'transaction': trans})

def is_purchase_allowed(tx_info):
    encoded_theater_id = tx_info['reservations'][0]['theater']
    (parent_org, theater_id) = decoded_theater_id(encoded_theater_id)
    theater_key = Key(models.Theater, theater_id, parent=Key(models.TheaterOrganization, str(parent_org)))
    theater = theater_key.get()
    log.info("THEATER_KEY: {}".format(theater_key))
    log.info("THEATER: {}".format(theater))
    log.info(' Theater: {0} Code: {1} Purchase Allowed {2}'.format(theater.name, theater.org_theater_code, theater.allow_reservation))
    return theater.allow_reservation

@api.route('/tx/', methods=['POST'])
def start_tx():
    log.info("Headers: {}".format(request.headers))

    device_id = request.headers[DEVICE_ID]

    try:
        log.info("request.json: {}".format(request.json))
        tx_info = request.json
    except Exception, e:
        log.warn("ERROR, malformed request.json...")
        log.error(e)
        log.info("request.data: {}".format(request.data))

        data = unicode(request.data, 'latin-1')
        tx_info = json.loads(data)

    log.info("tx_info: {}".format(tx_info))

    if not tx_info:
        log.warn("No tx_info...")

        raise BadValueException('body', 'You must supply transaction info')

    
    # VERY UGLY HACK: Restrict non-ipay88 payment options for ROCKWELL because of the inflexible iOS app
    payment_type = tx_info["payment"]["type"]
    encoded_theater_id = tx_info["reservations"][0]["theater"]
    (parent_org, theater_id) = decoded_theater_id(encoded_theater_id)
    log.info("Payment Type: {0} Theater Org: {1}".format(payment_type, str(parent_org)))

    if str(parent_org) == str(orgs.ROCKWELL_MALLS):
        log.info("Got Rockwell!!!")
        invalid_rockwell_payment_options = ["credit-card", "mpass", "client-initiated"]

        if payment_type in invalid_rockwell_payment_options:
            abort(400, error_code='PAYMENT_METHOD_NOT_ALLOWED', message='Invalid Payment Method.')

    if str(parent_org) == str(orgs.MEGAWORLD_MALLS):
        log.info("Got Megaworld!!!")
        theater_key = Key(models.TheaterOrganization, str(parent_org), models.Theater, theater_id)
        theater_code = theater_key.get().org_theater_code

        if theater_code =='LCT':
            invalid_lct_payment_options = ["credit-card", "mpass", "client-initiated", "promo-code"]

            if payment_type in invalid_lct_payment_options:
                abort(400, error_code='PAYMENT_METHOD_NOT_ALLOWED', message='Invalid Payment Method.')


    # restrict transactions for theaters with purchase-restricted status.
    # also serves as a filter for iOS apps with outdated purchased-enabled database 
    if not is_purchase_allowed(tx_info):
        log.info('Ticket purchase is not allowed, Aborting transaction')
        abort(400, error_code='RESERVATION_NOT_ALLOWED', message='Reservation for this cinema has been disabled.')
    else:
        log.info('Ticket purchase is allowed')
        transaction = _start_tx(device_id, tx_info)

        return jsonify({'id': transaction.key.id(), 'transaction': transaction.to_entity()})

@transactional
def _start_tx(device_id, tx_info):
    transaction = tx.start(device_id, tx_info)
    tx.reschedule(transaction)

    return transaction


@api.route('/tx-stopgap', methods=['POST'])
@deprecated('30 November 2013')
def start_tx_stopgap():
    device_id = request.headers[DEVICE_ID]
    tx_info = request.json
    if tx_info == None:
        raise BadValueException('body', 'You must supply transaction info')

    transaction = _start_tx_stopgap(device_id, tx_info)

    return jsonify({ 'id': transaction.key.id(), 
                     'transaction': transaction.to_entity() })

@transactional
def _start_tx_stopgap(device_id, tx_info):
    transaction = tx.start_stopgap(device_id, tx_info)
    tx.reschedule(transaction)

    return transaction

@api.route('/tx/<tx_id>/', methods=['GET'])
def get_tx_info(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = tx.query_info(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={ 'tx_id': tx_id })

    trans = transaction.to_entity()
    # (06/10/2014) Temporarily exclude barcode field from
    # response due to Android app not being able to handle 
    # long strings causing out of memory error
    trans['ticket']['barcode'] = ''
    trans['ticket']['qrcode'] = ''
    return jsonify({'id' : tx_id, 'ticket_id': '', 'transaction': trans})

@api.route('/tx/<tx_id>/client-initiate', methods=['GET'])
def client_initiate_payment(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = tx.query_info(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={ 'tx_id': tx_id })

    state, msg = tx.query_state(device_id, tx_id)

    if state != tx.state.TX_CLIENT_PAYMENT_HOLD:
        raise TransactionConflictException([tx.state.TX_CLIENT_PAYMENT_HOLD])

    log.info('Transaction Info: {}'.format(transaction))
    # FIXME: Leaked constant
    payment_info = transaction.workspace['payment::engine']

    log.info('Payment Info: {}'.format(payment_info))

    reference = transaction.reservation_reference

    log.info('Reservation Reference: {}'.format(reference))

    # FIXME: Hard-coded reference
    org_id = transaction.workspace['theaters.org_id'][0]
    if org_id == str(orgs.AYALA_MALLS):
        theater_code = transaction.workspace['theaters.org_theater_code'][0]
        reference = "%s-%s" % (theater_code, reference)

    # Override amount specified
    payment_info.form_parameters['amount'] = transaction.workspace['RSVP:totalamount']

    # FIXME: Hard-coded hash key and calculation
    payment_info.form_parameters['secureHash'] = payment_info.generate_hash(reference)

    log.info('Overriden Payment Info: {}'.format(payment_info.form_parameters))

    return render_template('client_initiated_payment.html', tx=transaction,
            reference=reference, payment_info=payment_info)

@api.route('/tx/<tx_id>/paynamics-initiate', methods=['GET'])
def paynamics_initiate_payment(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = tx.query_info(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={'tx_id': tx_id})

    state, msg = tx.query_state(device_id, tx_id)

    if state != tx.state.TX_CLIENT_PAYMENT_HOLD:
        raise TransactionConflictException([tx.state.TX_CLIENT_PAYMENT_HOLD])

    client_ip = request.remote_addr
    org_uuid = transaction.workspace['theaters.org_id'][0]
    payment_info = transaction.workspace['payment::engine']
    reservation_reference = transaction.reservation_reference.split('~')[-1] if str(org_uuid) != str(orgs.SM_MALLS) else transaction.reservation_reference
    payment_info.form_parameters['amount'] = transaction.workspace['RSVP:totalamount']
    payment_info.form_parameters['request_id'] = reservation_reference
    payment_info.form_parameters['country'] = 'PH'
    payment_info.form_parameters['orders'] = payment_info.generate_orders_payload(transaction)
    payment_info.form_parameters['client_ip'] = client_ip
    payment_info.form_parameters['signature'] = payment_info.generate_signature()
    paymentrequest = payment_info.generate_paymentrequest()

    try:
        log.info("form_parameters: {}".format(payment_info.form_parameters))
        log.info("signature: {}".format(payment_info.form_parameters['signature']))
        log.info("paymentrequest: {}".format(paymentrequest))
    except:
        log.warn("Failed to logs the paynamics-initiate details...")

    return render_template('paynamics_initiated_payment.html',
            payment_info=payment_info, paymentrequest=paymentrequest)

@api.route('/tx/<tx_id>/ipay88-payment-initiate', methods=['GET'])
def ipay88_initiate_payment(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = tx.query_info(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={'tx_id': tx_id})

    state, msg = tx.query_state(device_id, tx_id)

    if state != tx.state.TX_EXTERNAL_PAYMENT_HOLD:
        raise TransactionConflictException([tx.state.TX_EXTERNAL_PAYMENT_HOLD])

    payment_info = transaction.workspace['payment::engine']
    reservation_reference = transaction.reservation_reference
    signature = payment_info.generate_hash(reservation_reference, LCT_MERCHANT_KEY)
    payment_info.form_parameters["RefNo"] = reservation_reference
    payment_info.form_parameters["Signature"] = signature

    log.debug("form_parameters: {}".format(payment_info.form_parameters))
    log.debug("signature: {}".format(signature))

    return jsonify({"form_method": payment_info.form_method, "form_target":payment_info.form_target,
            "payment_info": payment_info.form_parameters})

@api.route('/tx/<tx_id>/', methods=['PUT'])
def update_tx_info(tx_id):
    device_id = request.headers[DEVICE_ID]
    tx_info = request.json
    if tx_info == None or tx_info == {}:
        raise BadValueException('body', 'You must supply transaction info')

    transaction = tx.update(device_id, tx_id, tx_info)

    return jsonify({ 'id' : tx_id, 'transaction': transaction.to_entity() })

@api.route('/tx/<tx_id>/', methods=['DELETE'])
def cancel_tx(tx_id):
    device_id = request.headers[DEVICE_ID]
    tx.cancel(device_id, tx_id)

    return jsonify({ 'id' : tx_id, 'cancelled': True })

@api.route('/tx/<tx_id>/state', methods=['GET'])
def get_tx_state(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = tx.query_info(device_id, tx_id)
    state, msg = tx.query_state(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={ 'tx_id': tx_id })

    if (state, msg) == (None, None):
        raise NotFoundException(details={ 'tx_id': tx_id })        

    tx_state = tx.to_state_str(state)

    if msg:
        log.info('State: {0} Message: {1}'.format(tx_state, msg))
        return jsonify({ 'state': tx_state, 'message': msg })
    else:
        log.info('State: {}'.format(tx_state))

        if transaction.payment_type == 'globe-promo':
            return jsonify({ 'state': tx_state, 'message': '' })

        return jsonify({ 'state': tx_state })

@api.route('/tx/<tx_id>/state', methods=['PUT'])
def force_tx_state(tx_id):
    abort(501)

@api.route('/tx/<tx_id>/ticket_details', methods=['GET'])
def get_tx_details(tx_id):
    device_id = request.headers[DEVICE_ID]
    state, __ = tx.query_state(device_id, tx_id)

    # transaction has no state yet
    if (state, __) == (None, None):
        raise NotFoundException(details={ 'tx_id': tx_id })  

    # transaction is not done
    if state != tx.state.TX_DONE:
        raise NotFoundException(details={ 'tx_id': tx_id })  

    q = tx.query_info(device_id, tx_id)

    transaction_detail = q.to_entity()

    ticket = {}
    ticket['title'] =  transaction_detail['ticket']['extra']['movie_canonical_title']
    ticket['theater_and_cinema_name'] = transaction_detail['ticket']['extra']['theater_and_cinema_name'] 
    ticket['reference_code'] = transaction_detail['ticket']['code']
    ticket['ticket_count'] = transaction_detail['ticket']['extra']['ticket_count']
    ticket['price'] = transaction_detail['ticket']['extra']['per_ticket_price']
    ticket['reservation_fee'] = transaction_detail['ticket']['extra']['reservation_fee']
    ticket['total_amount'] = transaction_detail['ticket']['extra']['total_amount']
    ticket['show_datetime'] = transaction_detail['ticket']['extra']['show_datetime']
    ticket['seats'] = transaction_detail['ticket']['extra']['seats']
    ticket['barcode'] = transaction_detail['ticket']['barcode']

    return jsonify(ticket)

@api.route('/device/completed_transactions', methods=['GET'])
def completed_device_transactions():

    device_id = request.headers[DEVICE_ID]
    device_key = Key(models.Device, device_id)
    reservations = models.ReservationTransaction.query(models.ReservationTransaction.state == tx.state.TX_DONE, ancestor=device_key).order(-models.ReservationTransaction.date_created).fetch()
    
    log.info("Reservations: {}".format(len(reservations)))

    def get_movie_details(m):
        if m is None:
            log.warn("No movie found")
            return None
        movie_key = Key(models.Movie, m.id())
        if movie_key is None:
            log.warn("No movie key")
            return None
        movie =  movie_key.get()
        if movie is None:
            return None
        return {'id': m.id(), 'title':movie.canonical_title}

    def get_scheds_movie(r):

        parent_org  = r.reservations[0].theater.parent().id()
        theater_id  = r.reservations[0].theater.id()
        schedule_id = r.reservations[0].schedule.id()
    
        theater_key = Key(models.TheaterOrganization, parent_org, models.Theater, theater_id) 
        schedule =  Key(models.Schedule, schedule_id, parent=theater_key ).get()
    
        if schedule is None:
            log.warn("No schedule data for {}".format(schedule_id))
            return None
        #extract slots 
        slots = schedule.slots
    
        #get movie keys from slots
        movie_keys = map(lambda x: x.movie, slots)
        #remove duplicate keys
        movie_keys_list = list(set(movie_keys)) #reduced
        
        #movies = []
        #for mk in movie_keys_list:
        #    movie_details = get_movie_details(mk)
        #    movies.append(movie_details)

        movie_details = get_movie_details(movie_keys_list[0])
        log.info("Movie Detail: {}".format(movie_details))
        if movie_details is not None:
            return { 'tx_id': r.key.id() ,'date_created': datetime.strftime(r.date_created, DATETIME_FORMAT), 'movie': movie_details }
        else:
            return None
                
    purchased_movies = filter(None, map(get_scheds_movie, reservations))
    log.info("Trans Movies {}".format(purchased_movies))

    return jsonify({'device_id': device_id, 'details': purchased_movies})

@api.route('/device/<device_id>/tx/<tx_id>/completed_transactions', methods=['GET'])
def completed_devices_transactions(device_id, tx_id):
    device_key = Key(models.Device, device_id)
    reservations = [Key(models.ReservationTransaction, tx_id, parent=device_key).get()]

    if not reservations:
        raise NotFoundException(details={ 'tx_id': tx_id })

    log.info("Reservations: {}".format(len(reservations)))

    def get_movie_details(m):
        if m is None:
            log.warn("No movie found")
            return None
        movie_key = Key(models.Movie, m.id())
        if movie_key is None:
            log.warn("No movie key")
            return None
        movie =  movie_key.get()
        if movie is None:
            return None
        return {'id': m.id(), 'title':movie.canonical_title}

    def get_scheds_movie(r):

        parent_org  = r.reservations[0].theater.parent().id()
        theater_id  = r.reservations[0].theater.id()
        schedule_id = r.reservations[0].schedule.id()
    
        theater_key = Key(models.TheaterOrganization, parent_org, models.Theater, theater_id) 
        schedule =  Key(models.Schedule, schedule_id, parent=theater_key ).get()
    
        if schedule is None:
            log.warn("No schedule data for {}".format(schedule_id))
            return None
        #extract slots 
        slots = schedule.slots
    
        #get movie keys from slots
        movie_keys = map(lambda x: x.movie, slots)
        #remove duplicate keys
        movie_keys_list = list(set(movie_keys)) #reduced
        
        #movies = []
        #for mk in movie_keys_list:
        #    movie_details = get_movie_details(mk)
        #    movies.append(movie_details)

        movie_details = get_movie_details(movie_keys_list[0])
        log.info("Movie Detail: {}".format(movie_details))
        if movie_details is not None:
            return { 'tx_id': r.key.id() ,'date_created': datetime.strftime(r.date_created, DATETIME_FORMAT), 'movie': movie_details }
        else:
            return None
                
    purchased_movies = filter(None, map(get_scheds_movie, reservations))
    log.info("Trans Movies {}".format(purchased_movies))

    return jsonify({'device_id': device_id, 'details': purchased_movies})

@api.route('/tx/<tx_id>/device/<device_id>/ticket_details', methods=['GET'])
def get_tx_details_by_device_id(tx_id, device_id):
    state, __ = tx.query_state(device_id, tx_id)

    # transaction has no state yet
    if (state, __) == (None, None):
        raise NotFoundException(details={ 'tx_id': tx_id })

    # transaction is not done
    if state != tx.state.TX_DONE:
        raise NotFoundException(details={ 'tx_id': tx_id })

    q = tx.query_info(device_id, tx_id)

    transaction_detail = q.to_entity()

    ticket = {}
    ticket['title'] =  transaction_detail['ticket']['extra']['movie_canonical_title']
    ticket['theater_and_cinema_name'] = transaction_detail['ticket']['extra']['theater_and_cinema_name']
    ticket['reference_code'] = transaction_detail['ticket']['code']
    ticket['ticket_count'] = transaction_detail['ticket']['extra']['ticket_count']
    ticket['price'] = transaction_detail['ticket']['extra']['per_ticket_price']
    ticket['reservation_fee'] = transaction_detail['ticket']['extra']['reservation_fee']
    ticket['total_amount'] = transaction_detail['ticket']['extra']['total_amount']
    ticket['show_datetime'] = transaction_detail['ticket']['extra']['show_datetime']
    ticket['seats'] = transaction_detail['ticket']['extra']['seats']
    ticket['barcode'] = transaction_detail['ticket']['barcode']

    return jsonify(ticket)

@api.route('/tx/<tx_id>/device/<device_id>/', methods=['GET'])
def get_tx_info_by_device_id(tx_id, device_id):
    transaction = tx.query_info(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={ 'tx_id': tx_id })

    return jsonify({ 'id' : tx_id, 'transaction': transaction.to_entity() })

@api.route('/movies/now_showing_coming_soon/', methods=['GET'])
def get_movies_with_coming_soon():
    id_encoder = lambda x: x.id()
    today = date.today()

    now_showing_query = models.Movie.query().filter(models.Movie.is_showing==True,
            models.Movie.is_expired==False, models.Movie.is_inactive==False).fetch()
    coming_soon_query = models.Movie.query().filter(models.Movie.release_date>today,
            models.Movie.is_expired==False, models.Movie.is_inactive==False).fetch()

    now_showing = [{ 'id': id_encoder(x.key), 'movie': x.to_entity(), 'status': 'now_showing' } for x in now_showing_query]
    coming_soon = [{ 'id': id_encoder(x.key), 'movie': x.to_entity(), 'status': 'coming_soon' } for x in coming_soon_query]

    movies = now_showing + coming_soon

    return jsonify({'results': movies})

@api.route('/movies/in_active/', methods=['GET'])
def get_in_active_movies():
    id_encoder = lambda x: x.id()
    query = models.Movie.query().filter(models.Movie.is_inactive==True).fetch()
    movies = [{ 'id': id_encoder(x.key), 'movie': x.to_entity() } for x in query]
    return jsonify({'results': movies})

@api.route('/services/', methods=['GET'])
def get_services():
    services_query = models.AnalyticsService.get_services()
    services = [{'id': s.key.id(), 'name': s.service_name,
            'code': s.service_code, 'value': s.value,
            'status': 'active' if s.active else 'inactive'} for s in services_query]

    return jsonify({'services': services})

@api.route('/services/<service_code>/', methods=['GET'])
def get_service_status(service_code):
    service = models.AnalyticsService.get_service_status(str(service_code))
    s = {'name': service[0].service_name, 'code': service[0].service_code,
            'value': service[0].value, 'status': 'active' if service[0].active else 'inactive'} if service else {}

    return jsonify({'service': s})

@api.route('/movies/coming_soon/')
def get_movies_comingsoon():
    id_encoder = lambda x:x.id()
    queryset = models.Movie.query(models.Movie.is_showing == False,
            models.Movie.is_expired == False, models.Movie.is_inactive == False,
            models.Movie.is_published == True).order(models.Movie.release_date)

    coming_soon_movies = do_parse_comingsoon(queryset, id_encoder=id_encoder)

    return jsonify({'results': coming_soon_movies})

@api.route('/movies/rockwell/', methods=['GET'])
def get_movies_rockwell():
    check_filter_and_sort_args(models.Movie, request.args)

    rockwell_ids = []
    rockwell_feed_key = Key(models.Feed, str(orgs.ROCKWELL_MALLS))

    id_encoder = lambda x:x.id()
    queryset = models.Movie.query()
    filters, queryset = query_filters(models.Movie, request)
    is_nowshowing, is_comingsoon = get_movie_status(filters)

    if is_nowshowing:
        rockwell_movies = read_movies_nowshowing_powerplantmall()
        rockwell_ids = [str(movie_dict['id']) for movie_dict in rockwell_movies]
    elif is_comingsoon:
        rockwell_movies = read_movies_comingsoon_powerplantmall()
        rockwell_ids = [str(movie_dict['id']) for movie_dict in rockwell_movies]

    queryset = queryset.filter(
            models.Movie.movie_correlation.org_key == rockwell_feed_key,
            models.Movie.movie_correlation.movie_id.IN(rockwell_ids))
    movie_list = [{'id': id_encoder(x.key), 'movie': x.to_entity()} for x in queryset]

    return jsonify({'filters': filters, 'results': movie_list})

@api.route('/movies/coming_soon/rockwell/')
def get_movies_comingsoon_rockwell():
    rockwell_ids = []
    id_encoder = lambda x:x.id()
    rockwell_feed_key = Key(models.Feed, str(orgs.ROCKWELL_MALLS))
    queryset = models.Movie.query(models.Movie.is_showing == False,
            models.Movie.is_expired == False, models.Movie.is_inactive == False,
            models.Movie.is_published == True).order(models.Movie.release_date)

    rockwell_movies = read_movies_comingsoon_powerplantmall()
    rockwell_ids = [str(movie_dict['id']) for movie_dict in rockwell_movies]
    queryset = queryset.filter(
            models.Movie.movie_correlation.org_key == rockwell_feed_key,
            models.Movie.movie_correlation.movie_id.IN(rockwell_ids))

    coming_soon_movies = do_parse_comingsoon(queryset, id_encoder=id_encoder)

    return jsonify({'results': coming_soon_movies})

@api.route('/theaterorgs/<theaterorg_id>/movies/')
def get_theaterorg_movies(theaterorg_id):
    check_filter_and_sort_args(models.Movie, request.args)

    ids = []
    movie_list = []

    feed_key = Key(models.Feed, str(theaterorg_id))
    feed = feed_key.get()

    if not feed:
        raise NotFoundException(details={'theaterorg_id': theaterorg_id})

    posters_args = {}
    posters_args['resolution'] = request.args.get('resolution', 'uploaded')
    posters_args['application_type'] = request.args.get('application_type', 'mobile-app')

    id_encoder = lambda x:x.id()
    queryset = models.Movie.query()
    filters, queryset = query_filters(models.Movie, request)
    is_nowshowing, is_comingsoon = get_movie_status(filters)
    portrait_default_poster, landscape_default_poster = get_default_poster_url(posters_args=posters_args)

    if is_nowshowing:
        if str(feed_key.id()) == str(orgs.AYALA_MALLS):
            movie_list = sureseats.read_movies_now_showing()
        elif str(feed_key.id()) == str(orgs.ROCKWELL_MALLS):
            movie_list = read_movies_nowshowing_powerplantmall()
    elif is_comingsoon:
        if str(feed_key.id()) == str(orgs.AYALA_MALLS):
            movie_list = sureseats.read_movies_coming_soon()
        elif str(feed_key.id()) == str(orgs.ROCKWELL_MALLS):
            movie_list = read_movies_comingsoon_powerplantmall()

    ids = [str(movie_dict['id']) for movie_dict in movie_list]
    queryset = queryset.filter(models.Movie.movie_correlation.org_key == feed_key)

    if is_nowshowing or is_comingsoon:
        if ids:
            queryset = queryset.filter(models.Movie.movie_correlation.movie_id.IN(ids))
        else:
            movie_entities = []

            return jsonify({'filters': filters, 'results': movie_entities})

    movie_entities = [{'id': id_encoder(x.key),
            'movie': x.to_entity(posters_args=posters_args,
                    portrait_default_poster=portrait_default_poster,
                    landscape_default_poster=landscape_default_poster)} for x in queryset]

    return jsonify({'filters': filters, 'results': movie_entities})

@api.route('/theaterorgs/<theaterorg_id>/movies/coming_soon/')
def get_theaterorg_movies_comingsoon(theaterorg_id):
    ids = []
    movie_list = []
    id_encoder = lambda x:x.id()
    feed_key = Key(models.Feed, str(theaterorg_id))
    feed = feed_key.get()

    if not feed:
        raise NotFoundException(details={'theaterorg_id': theaterorg_id})

    queryset = models.Movie.query(models.Movie.is_showing == False,
            models.Movie.is_expired == False, models.Movie.is_inactive == False,
            models.Movie.is_published == True).order(models.Movie.release_date)

    if str(feed_key.id()) == str(orgs.AYALA_MALLS):
        movie_list = sureseats.read_movies_coming_soon()
    elif str(feed_key.id()) == str(orgs.ROCKWELL_MALLS):
        movie_list = read_movies_comingsoon_powerplantmall()

    ids = [str(movie_dict['id']) for movie_dict in movie_list]
    queryset = queryset.filter(models.Movie.movie_correlation.org_key == feed_key)

    if ids:
        queryset = queryset.filter(models.Movie.movie_correlation.movie_id.IN(ids))
    else:
        coming_soon_movies = []

        return jsonify({'results': coming_soon_movies})

    coming_soon_movies = do_parse_comingsoon(queryset, id_encoder=id_encoder)

    return jsonify({'results': coming_soon_movies})

def do_parse_comingsoon(queryset, id_encoder=lambda x:x.id()):
    movie_list = []
    not_sorted_movies = []
    posters_args = {}
    posters_args['resolution'] = request.args.get('resolution', 'uploaded')
    posters_args['application_type'] = request.args.get('application_type', 'mobile-app')

    portrait_default_poster, landscape_default_poster = get_default_poster_url(posters_args=posters_args)

    for ent in queryset:
        month_year_to_string = admin.parse_date_to_string(ent.release_date, fdate='%m %Y')

        if month_year_to_string:
            movie_tuple = (month_year_to_string, {'id': id_encoder(ent.key),
                    'movie': ent.to_entity_comingsoon(posters_args=posters_args,
                            portrait_default_poster=portrait_default_poster,
                            landscape_default_poster=landscape_default_poster)})
            not_sorted_movies.append(movie_tuple)

    sorted_movies = sorted(not_sorted_movies, key=itemgetter(0))
    movie_groups = groupby(sorted_movies, key=itemgetter(0))

    for key, value in movie_groups:
        month_year_to_date = admin.parse_string_to_date(key, fdate='%m %Y')
        month_year_to_string = admin.parse_date_to_string(month_year_to_date, fdate='%B %Y')

        if month_year_to_date and month_year_to_string:
            tab_image_url = get_coming_soon_tab_image_url(month_year_to_string)
            movie_list.append({'month': month_year_to_string,
                    'month_image': tab_image_url,
                    'movies': [movie[1] for movie in value]})

    return movie_list


def get_movie_status(filters):
    is_nowshowing = None
    is_comingsoon = None

    if 'match' in filters:
        if 'is_showing' in filters['match'] and 'is_expired' in filters['match']:
            if filters['match']['is_showing'].lower() == 'true' or not filters['match']['is_showing']:
                is_showing = True
            else:
                is_showing = False

            if filters['match']['is_expired'].lower() == 'true' or not filters['match']['is_expired']:
                is_expired = True
            else:
                is_expired = False

            if is_showing and not is_expired:
                is_nowshowing = True
            elif not is_showing and not is_expired:
                is_comingsoon = True

    return is_nowshowing, is_comingsoon

def get_default_poster_url(posters_args={}):
    portrait_default_poster = ''
    landscape_default_poster = ''

    resolution = posters_args.get('resolution', 'uploaded')
    application_type = posters_args.get('application_type', 'mobile-app')

    cache_key_portrait = "movie::default::poster::portrait::%s::%s" % (resolution, application_type)
    cache_key_landscape = "movie::default::poster::landscape::%s::%s" % (resolution, application_type)

    try:
        cache_hit_portrait = memcache.get(cache_key_portrait)

        if cache_hit_portrait:
            log.info("Movie: Portrait, default poster cache hit on: %s" % cache_key_portrait)

            portrait_default_poster = cache_hit_portrait
        else:
            portrait_poster_image_blob = models.PosterImageBlob.query(
                    models.PosterImageBlob.resolution == resolution,
                    models.PosterImageBlob.orientation == 'portrait',
                    models.PosterImageBlob.application_type == application_type,
                    models.PosterImageBlob.is_default == True)

            if portrait_poster_image_blob.count() != 0:
                portrait_poster_image = portrait_poster_image_blob.get()
                portrait_default_poster = get_serving_url(portrait_poster_image.image)

                memcache.set(cache_key_portrait, portrait_default_poster)
    except:
        pass

    try:
        cache_hit_landscape = memcache.get(cache_key_landscape)

        if cache_hit_landscape:
            log.info("Movie: Landscape, default poster cache hit on: %s" % cache_key_landscape)

            landscape_default_poster = cache_hit_landscape
        else:
            landscape_poster_image_blob = models.PosterImageBlob.query(
                    models.PosterImageBlob.resolution == resolution,
                    models.PosterImageBlob.orientation == 'landscape',
                    models.PosterImageBlob.application_type == application_type,
                    models.PosterImageBlob.is_default == True)

            if landscape_poster_image_blob.count() != 0:
                landscape_poster_image = landscape_poster_image_blob.get()
                landscape_default_poster = get_serving_url(landscape_poster_image.image)

                memcache.set(cache_key_landscape, landscape_default_poster)
    except:
        pass

    return portrait_default_poster, landscape_default_poster

def get_coming_soon_tab_image_url(tab_name):
    tab_image_url = ''

    cache_key = "movies::coming_soon::month_image_tab::%s" % tab_name.split(' ')[0]

    try:
        cache_hit = memcache.get(cache_key)

        if cache_hit:
            log.info("Movies: Coming Soon image tab for month cache hit on: %s" % cache_key)

            tab_image_url = cache_hit
        else:
            tab_image_blob = models.AssetsImageBlob.query(
                    models.AssetsImageBlob.name == tab_name.split(' ')[0],
                    models.AssetsImageBlob.assets_group == 'coming_soon_tab')

            if tab_image_blob.count() == 0:
                return tab_image_url

            tab_image = tab_image_blob.get()
            tab_image_url = get_serving_url(tab_image.image)

            memcache.set(cache_key, tab_image_url)

            log.info("Movies: Coming Soon image tab for month cached: %s" % cache_key)
    except:
        pass

    return tab_image_url

# user management for MGI/Megaworld Cinemas
@api.route('/member/register/<theater_id>/', methods=['POST'])
def register_user(theater_id):
    """
    Headers:
    :: DEVICE_ID

    REST form_parameters
    :: theater_id

    Payload
    e.g.
    {"register": {
                  "first_name": "First Name",
                  "middle_name": "",
                  "last_name": "Last Name",
                  "address": "",
                  "city": "",
                  "bdate": "January 01, 2013",
                  "mobile_num": "09XXXXXXXXX",
                  "phone_num": "",
                  "email": "gmovies@localhost.com",
                  "password": "password"
                 }
    }
    """

    device_id = request.headers[DEVICE_ID]
    log.info("Device ID: {}".format(device_id))

    user_info = request.json
    log.info("USER INFO: {}".format(user_info))

    if user_info == None or 'register' not in user_info:
        log.warn("No USER_INFO")
        raise BadValueException('body', 'You must supply user info')

    org_id, _theater_id = decoded_theater_id(theater_id)
    theater_key = Key(models.TheaterOrganization, str(org_id), models.Theater, _theater_id)
    theater_code = theater_key.get().org_theater_code

    if not org_id:
        log.info("No TheaterOrganization ID")
        raise NotFoundException(details={ 'theaterorg_id':
                                          theaterorg_id })
    if not _theater_id:
        log.info("No Theater ID")
        raise NotFoundException(details={'theater_id': _theater_id})

    if theater_code not in REGISTRATION_ALLOWED_THEATER_CODES:
        log.info("Theater not allowed")
        raise InvalidActionException(message='This theater does not support this action')

    #User registration implementation here
    user = WSUser(LCT_TRANSACTION_API)
    status, data = user.register(user_info['register'])

    if status == 'error':
        raise ThirdPartyServiceError('An error occurred while connecting to this theater', data)

    if status == 'success':
        return jsonify({'status': 0, 'registration_token': data})

    if status == 'fail':
        if data == 'Duplicate Email':
            data = 'Email Address already registered. Please try other email address'

        return jsonify({'status': 1, 'message': data})
    
# user management for MGI/Megaword Cinemas
@api.route('/member/login/<theater_id>/', methods=['POST'])
def login_user(theater_id):
    """
    Headers:
    :: DEVICE_ID

    REST form_parameters
    :: theater_id

    Payload
    :: email - registered user email
    :: password - you know this is

    e.g.
    { "login_info": {
      "email": "gmovies@localhost.com",
      "password": "password"
       }
    }
    """
    device_id = request.headers[DEVICE_ID]
    log.info("Device ID: {}".format(device_id))

    login_info = request.json
    log.info("LOGIN INFO: {}".format(login_info))

    if login_info == None or 'email' not in login_info['login_info'] or 'password' not in login_info['login_info']:
        log.warn("No login details")
        raise BadValueException('body', 'You must supply login info')

    org_id, _theater_id = decoded_theater_id(theater_id)
    theater_key = Key(models.TheaterOrganization, str(org_id), models.Theater, _theater_id)
    theater_code = theater_key.get().org_theater_code

    if not org_id:
        raise NotFoundException(details={ 'theaterorg_id':
                                          theaterorg_id })
    if not _theater_id:
        raise NotFoundException(details={'theater_id': _theater_id})

    if theater_code not in REGISTRATION_ALLOWED_THEATER_CODES:
        raise InvalidActionException(message='This theater does not support this action')

    #User login implementation here
    user = WSUser(LCT_TRANSACTION_API)
    status, client_id, client_details, message = user.login(email=login_info['login_info']['email'],
                                                    password=login_info['login_info']['password'])
    if status == 'error':
        raise ThirdPartyServiceError('An error occurred while connecting to this theater')

    if status == 'success':
        return jsonify({'status': 0, 'client_id': client_id, 'client_info': client_details})

    if status == 'fail':
        return jsonify({'status': 1, 'message': message})

@api.route('/member/verify_registration/<theater_id>/', methods=['POST'])
def verify_registration(theater_id):
    """
    Headers:
    :: DEVICE_ID

    REST form_parameters
    :: theater_id

    Payload
    :: registration_token - generated after success registration
    e.g.
    { "registration_token": "randomtokencodehere"}
    """

    device_id = request.headers[DEVICE_ID]
    log.info("Device ID: {}".format(device_id))

    registration_info = request.json
    log.info("REGISTRATION_INFO: {}".format(registration_info))

    if registration_info == None or 'registration_token' not in registration_info:
        log.warn("No registration token")
        raise BadValueException('registration_token', 'Missing registration token')

    org_id, _theater_id = decoded_theater_id(theater_id)
    theater_key = Key(models.TheaterOrganization, str(org_id), models.Theater, _theater_id)
    theater_code = theater_key.get().org_theater_code

    if not org_id:
        raise NotFoundException(details={ 'theaterorg_id': theaterorg_id })

    if not _theater_id:
        raise NotFoundException(details={'theater_id': _theater_id})

    if theater_code not in REGISTRATION_ALLOWED_THEATER_CODES:
        raise InvalidActionException(message='This theater does not support this action')

    #User registration token verification here
    user = WSUser(LCT_TRANSACTION_API)
    status, data = user.verify(registration_info['registration_token'])

    if status == 'error':
        raise ThirdPartyServiceError('An error occurred while connecting to this theater. Failed to verify your email.', data)

    if status == 'success':
        return jsonify({ 'message': data })

@api.route('/member/reset_password/<org_theater_id>/', methods=['POST'])
def reset_password(org_theater_id):
    device_id = request.headers[DEVICE_ID]
    log.info("Device ID: {}".format(device_id))

    recovery_info = request.json
    log.info("USER_INFO: {}".format(recovery_info))

    if not recovery_info or 'email' not in recovery_info:
        log.warn("No email info")
        raise BadValueException('email', 'Email address is required')

    org_id, theater_id = decoded_theater_id(org_theater_id)
    theater_key = Key(models.TheaterOrganization, str(org_id), models.Theater, theater_id)
    theater = theater_key.get()

    if not org_id:
        raise NotFoundException(details={ 'theaterorg_id': org_id })

    if not theater_id:
        raise NotFoundException(details={'theater_id': theater_id})

    if theater.org_theater_code not in REGISTRATION_ALLOWED_THEATER_CODES:
        raise InvalidActionException(message='This theater does not support this action')

    queryset = models.MemberPasswordReset.query(
            models.MemberPasswordReset.email == recovery_info['email'],
            models.MemberPasswordReset.is_active == True)

    if queryset.count() == 1:
        existing_password_reset = queryset.get()
        existing_password_reset.is_active = False
        existing_password_reset.put()

    password_reset = models.MemberPasswordReset()
    password_reset.email = recovery_info['email']
    password_reset.theater = theater_key
    password_reset.put()

    password_reset_link = "%smember/password/reset/%s/" % (request.host_url, str(password_reset.key.id()))
    password_reset_request(user_email=recovery_info['email'],
            password_reset_link=password_reset_link, branch_account=theater.name)

    return jsonify({'message': 'Check your email for the next steps.'})

@api.route('/member/update_password/<theater_id>/', methods=['POST'])
def update_password(theater_id):
    """
    Headers:
    :: DEVICE_ID

    REST form_parameters
    :: theater_id

    Payload
    :: email - email address used in registration
    :: new_password
    :: old_password
    e.g.
    {
        "email": "gmovies@localhost.com",
        "new_password": "gmoviespassword",
        "old_password": "passowrd"
    }

    Response:
    ::msg -  password update message
    e.g:
    {
        "msg": "Change Password Successful!"
    }
    """

    device_id = request.headers[DEVICE_ID]
    log.info("Device ID: {}".format(device_id))

    update_info = request.json
    log.info("USER_INFO: {}".format(update_info))

    if update_info is None:
        log.warn("No data")
        raise BadValueException('email', 'Missing field data')

    if 'email' not in update_info or update_info['email'] == "":
        log.warn("No email info")
        raise BadValueException('email', 'Email address is required')

    if 'new_password' not in update_info or update_info['new_password'] == "":
        log.warn("Missing new password")
        raise BadValueException('new_password', 'New password is required')

    if 'old_password' not in update_info or update_info['old_password'] == "":
        log.warn("Missing old password")
        raise BadValueException('old_password', 'Old password is required')

    org_id, _theater_id = decoded_theater_id(theater_id)
    theater_key = Key(models.TheaterOrganization, str(org_id), models.Theater, _theater_id)
    theater_code = theater_key.get().org_theater_code

    if not org_id:
        raise NotFoundException(details={ 'theaterorg_id': theaterorg_id })

    if not _theater_id:
        raise NotFoundException(details={'theater_id': _theater_id})

    if theater_code not in REGISTRATION_ALLOWED_THEATER_CODES:
        raise InvalidActionException(message='This theater does not support this action')

    #User registration token verification here
    auth = WSAuth(LCT_SESSION_API)
    status, session_token = auth.get_session_and_authenticate(LCT_USERNAME, LCT_PASSWORD)

    if not session_token or status == 'error':
        log.warn('Error aquiring session token')
        raise ThirdPartyServiceError('An error occurred while connecting to this theater.')

    user = WSUser(LCT_TRANSACTION_API)
    status, data, _ = user.update_password(session_token=session_token, email=update_info['email'],
            new_password=update_info['new_password'], old_password=update_info['old_password'])

    if status == 'error':
        raise ThirdPartyServiceError('An error occurred while connecting to this theater. Failed to update your password.', data)

    if status == 'success':
        if data == 'Updating password Successful':
            data = 'Password Successfully Changed'

        return jsonify({'status': 0, 'message': data})

    if status == 'fail':
        return jsonify({'status': 1, 'message': data})