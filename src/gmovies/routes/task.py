import datetime
import json
import logging
import os
import requests
import sys
import calendar

from uuid import UUID
from lxml import etree

from flask import abort, jsonify, redirect, request, url_for, Blueprint

from google.appengine.api import memcache, taskqueue
from google.appengine.api.images import get_serving_url
from google.appengine.ext import deferred, ndb

from gmovies import tx
from gmovies.admin import notify_admin, notify_globe_csr, send_sales_reports, eplus_balance_notification
from gmovies.admin.email import send_transaction_reports
from gmovies.bank_transfer import AccountSettings
from gmovies.feeds import (greenhills, images, megaworld, mgi, rockwell, rottentomatoes,
        sureseats, theaters, scraper, cinema76, globeevents, citymall)
from gmovies.heuristics import schedules, transform_movies, transform_schedules
from gmovies.heuristics.rottentomatoes import rottentomatoes_transform_movies
from gmovies.heuristics.scraper_schedules import scraper_transform_schedules
from gmovies.models import CSRCPayment, Event, Feed, Listener, Movie, MultiResImageBlob, Theater, TheaterOrganization, TicketTemplateImageBlob
from gmovies.orgs import (AYALA_MALLS, CINEMA2000_MALLS, FESTIVAL_MALLS, GATEWAY_MALLS,
        GLOBE, GREENHILLS_MALLS, MEGAWORLD_MALLS, ROBINSONS_MALLS, ROCKWELL_MALLS,
        ROTTEN_TOMATOES, SHANGRILA_MALLS, SM_MALLS, CINEMA76_MALLS, GLOBE_EVENTS, CITY_MALLS)
from gmovies.routes.util import get_default_poster_url
from gmovies.settings import GMOVIES_DIGITALVENTURES_V1_BASE_URLS, NOTIFICATION_RETRY_THRESHOLD
from gmovies.tasks import (activate_movies_nowshowing, deactivate_movies_comingsoon, expire_movies, unexpire_movies, unexpire_movies_comingsoon,
        transform_poster_images, update_schema, update_schema_models)
from gmovies.tasks.admin import entity_fsck
from gmovies.tx import promo
from gmovies.util.admin import (generate_sales_reports_xls, generate_transaction_reports_xls, get_theater, is_payment_enabled, parse_string_to_date,
        get_filtered_transactions)
from gmovies.util import id_encoder
from gmovies.tx.sureseats import create_sureseats_request
from gmovies.tx.state_machine import state


log = logging.getLogger(__name__)
task = Blueprint('task', __name__, template_folder='task_templates')

TIMEOUT_DEADLINE = 45
LCT_THEATER_CODE = 'LCT'
THEATERORG_UUID = {str(AYALA_MALLS): AYALA_MALLS, str(SM_MALLS): SM_MALLS, str(GREENHILLS_MALLS): GREENHILLS_MALLS,
        str(ROCKWELL_MALLS): ROCKWELL_MALLS, str(MEGAWORLD_MALLS): MEGAWORLD_MALLS, str(ROBINSONS_MALLS): ROBINSONS_MALLS,
        str(SHANGRILA_MALLS): SHANGRILA_MALLS, str(CINEMA2000_MALLS): CINEMA2000_MALLS, str(CINEMA76_MALLS): CINEMA76_MALLS,
        str(GLOBE_EVENTS): GLOBE_EVENTS}
SURESEATS_ENDPOINT_PRIMARY = 'http://api.sureseats.com/globe.asp'
SURESEATS_ENDPOINT_STAGING = 'http://api.sureseats.com/globe_test.asp'
STATUS_ACTION = 'STATUS'
SOURCE_ID = '9'
IS_STAGING = True

####################
#
# Task Payload & Task Group Entities
#
####################

class HeuristicsTaskPayload(ndb.Model):
    payload = ndb.PickleProperty()


class TaskGroupControl(ndb.Model):
    feed_count = ndb.IntegerProperty()
    completion_count = ndb.IntegerProperty(default=0)
    payloads = ndb.PickleProperty()

    def incr_and_check_completion(self):
        self.completion_count += 1
        self.put()

        if self.completion_count >= self.feed_count:
            self.launch_heuristics()

    def pack_payload(self, org_id, movie_dictionary_list):
        self.payloads[org_id] = movie_dictionary_list
        self.put()

    def launch_heuristics(self):
        taskqueue.add(url=url_for('.run_movies_heuristics'), params={'ctrl_key': self.key.urlsafe()})

    def incr_and_check_completion_schedules(self):
        self.completion_count += 1
        self.put()

        if self.completion_count >= self.feed_count:
            self.launch_heuristics_schedules()

    def pack_payload_schedules(self, org_id, payloads):
        self.payloads[org_id] = payloads
        self.put()

    def launch_heuristics_schedules(self):
        taskqueue.add(url=url_for('.run_schedules_heuristics_taskgroup'), queue_name='schedulesqueue', params={'ctrl_key': self.key.urlsafe()})

    @classmethod
    def launch_singleton_taskgroup(cls, org_uuid, movie_dictionary_list):
        ctrl = cls()
        ctrl.payloads = {}
        ctrl.pack_payload(str(org_uuid), movie_dictionary_list)
        ctrl.launch_heuristics()


def create_task_payload(p):
    payload = HeuristicsTaskPayload(payload=p)
    payload.put()

    return payload.key.urlsafe()

def drop_task_payload(k):
    ndb.Key(urlsafe=k).delete()

def get_task_payload(k):
    p = ndb.Key(urlsafe=k).get()

    return p.payload if p else None

@ndb.transactional
def pack_taskgroup_payload(ctrl_key, org_uuid, payload):
    ctrl = ndb.Key(urlsafe=ctrl_key).get(use_cache=False, use_memcache=False)
    ctrl.pack_payload(str(org_uuid), payload)
    ctrl.incr_and_check_completion()

@ndb.transactional
def pack_taskgroup_payload_schedules(ctrl_key, org_uuid, payload):
    ctrl = ndb.Key(urlsafe=ctrl_key).get(use_cache=False, use_memcache=False)
    if ctrl:
        ctrl.pack_payload_schedules(str(org_uuid), payload)
        ctrl.incr_and_check_completion_schedules()


###############
#
# Error Handlers
#
###############

@task.errorhandler(Exception)
def handle_task_error(e):
    logging.exception(e)
    retry_count = -1

    if 'X-AppEngine-TaskRetryCount' in request.headers:
        retry_count = int(request.headers['X-AppEngine-TaskRetryCount'])

    if retry_count == -1 or retry_count >= NOTIFICATION_RETRY_THRESHOLD:
        notify_admin("backend task", e, sys.exc_info())

    res = jsonify(message='Task error', details=e.message)
    res.status_code = 500

    return res

@task.route('/')
def task_listing():
    return jsonify([])


###############
#
# Movie Tasks via CRON
#
###############

@task.route('/fetch/movies')
def fetch_all_movie_feeds():
    ctrl = TaskGroupControl()
    ctrl.feed_count = 8
    ctrl.payloads = {}
    ctrl.put()
    ctrl_key = ctrl.key.urlsafe()

    taskqueue.add(url=url_for('.fetch_movies_sureseats'), params=dict(ctrl_key=ctrl_key))
    # Rockwell already disabled - Aug 31, 2017
    # Rockwell enabled movie schedule listing ONLY - Dec 14, 2017
    # Rockwell remove movie schedule listing ONLY - May 16, 2018
    #taskqueue.add(url=url_for('.fetch_movies_powerplantmall'), params=dict(ctrl_key=ctrl_key))
    taskqueue.add(url=url_for('.fetch_movies_greenhills'), params=dict(ctrl_key=ctrl_key))
    # Cinema 76 enabled - Nov 22, 2017
    taskqueue.add(url=url_for('.fetch_movies_cinema76'), params=dict(ctrl_key=ctrl_key))
    taskqueue.add(url=url_for('.fetch_movies_globeevents'), params=dict(ctrl_key=ctrl_key))
    taskqueue.add(url=url_for('.fetch_movies_smmalls'), params=dict(ctrl_key=ctrl_key))
    taskqueue.add(url=url_for('.fetch_movies_robinsons'), params=dict(ctrl_key=ctrl_key))
    taskqueue.add(url=url_for('.fetch_movies_luckychinatown'), params=dict(ctrl_key=ctrl_key, theater_code=LCT_THEATER_CODE))
    taskqueue.add(url=url_for('.fetch_movies_advisory_ratings'), params=dict(ctrl_key=ctrl_key))

    return jsonify(status='ok')

# Separate cron for CityMall movies. Need to call per theater
@task.route('/fetch/movies-citymall')
def fetch_movie_feeds_citymall():
    ctrl = TaskGroupControl()
    ctrl.feed_count = 1
    ctrl.payloads = {}
    ctrl.put()
    ctrl_key = ctrl.key.urlsafe()

    taskqueue.add(url=url_for('.fetch_movies_citymall'), params=dict(ctrl_key=ctrl_key))

    return jsonify(status='ok')

@task.route('/fetch/movies/ayala', methods=['POST'])
def fetch_movies_sureseats():
    log.debug("fetch_movies_sureseats, start fetching Ayala Malls movies via SureSeats...")

    ctrl_key = request.values['ctrl_key']
    movies_nowshowing = sureseats.read_movies_now_showing()
    movies_comingsoon = sureseats.read_movies_coming_soon()
    movies = movies_nowshowing + movies_comingsoon

    pack_taskgroup_payload(ctrl_key, AYALA_MALLS, movies)

    return jsonify(status='ok')

@task.route('/fetch/movies/power-plant-mall', methods=['POST'])
def fetch_movies_powerplantmall():
    log.debug("fetch_movies_powerplantmall, start fetching Rockwell (Power Plant Mall) movies...")

    ctrl_key = request.values['ctrl_key']
    movies_nowshowing = rockwell.read_movies_nowshowing_powerplantmall()
    movies_comingsoon = rockwell.read_movies_comingsoon_powerplantmall()
    movies = movies_nowshowing + movies_comingsoon

    pack_taskgroup_payload(ctrl_key, ROCKWELL_MALLS, movies)

    return jsonify(status='ok')

@task.route('/fetch/movies/greenhills', methods=['POST'])
def fetch_movies_greenhills():
    log.debug("fetch_movies_greenhills, start fetching Greenhills Malls movies...")

    ctrl_key = request.values['ctrl_key']
    movies_nowshowing = greenhills.read_movies_nowshowing_greenhills()
    movies_comingsoon = greenhills.read_movies_comingsoon_greenhills()
    movies = movies_nowshowing + movies_comingsoon

    pack_taskgroup_payload(ctrl_key, GREENHILLS_MALLS, movies)

    return jsonify(status='ok')

@task.route('/fetch/movies/cinema76', methods=['POST'])
def fetch_movies_cinema76():
    log.debug("fetch_movies_cinema76, start fetching Cinema 76 Malls movies...")

    ctrl_key = request.values['ctrl_key']
    movies_nowshowing = cinema76.read_movies_nowshowing_cinema76()
    movies_comingsoon = cinema76.read_movies_comingsoon_cinema76()
    movies = movies_nowshowing + movies_comingsoon

    pack_taskgroup_payload(ctrl_key, CINEMA76_MALLS, movies)

    return jsonify(status='ok')

@task.route('/fetch/movies/globeevents', methods=['POST'])
def fetch_movies_globeevents():
    log.debug("fetch_movies_globeevents, start fetching Globe Events events...")

    ctrl_key = request.values['ctrl_key']
    movies_nowshowing = globeevents.read_movies_nowshowing_globeevents()
    movies_comingsoon = globeevents.read_movies_comingsoon_globeevents()
    movies = movies_nowshowing + movies_comingsoon

    pack_taskgroup_payload(ctrl_key, GLOBE_EVENTS, movies)

    return jsonify(status='ok')

@task.route('/fetch/movies/citymall', methods=['POST'])
def fetch_movies_citymall():
    log.debug("fetch_movies_citymall, start fetching CityMalls...")

    theaterorg_key = ndb.Key(TheaterOrganization, str(CITY_MALLS))
    theaters = Theater.query(ancestor=theaterorg_key).fetch()

    ctrl_key = request.values['ctrl_key']
    movies_list = []
    for theater in theaters:
        log.debug("fetch_movies_citymall, fetch movie tasks for %s..." % theater.name)

        movies_nowshowing = citymall.read_movies_nowshowing_citymall(theater.org_theater_code, theater.location_code)
        movies_comingsoon = citymall.read_movies_comingsoon_citymall(theater.org_theater_code, theater.location_code)
        movies = movies_nowshowing + movies_comingsoon
        movies_list += movies

    pack_taskgroup_payload(ctrl_key, CITY_MALLS, movies_list)

    return jsonify(status='ok')

@task.route('/fetch/movies/sm-malls', methods=['POST'])
def fetch_movies_smmalls():
    log.debug("fetch_movies_smmalls, start fetching SM Malls movies via MGi...")

    ctrl_key = request.values['ctrl_key']
    movies_nowshowing = mgi.read_movies('NOWSHOWING', SM_MALLS, is_mutiple_branch=True)
    movies_comingsoon = mgi.read_movies('COMINGSOON', SM_MALLS, is_mutiple_branch=True)
    movies = movies_nowshowing + movies_comingsoon

    pack_taskgroup_payload(ctrl_key, SM_MALLS, movies)

    return jsonify(status='ok')

@task.route('/fetch/movies/robinsons', methods=['POST'])
def fetch_movies_robinsons():
    log.debug("fetch_movies_robinsons, start fetching Robinsons Malls movies via MGi...")

    ctrl_key = request.values['ctrl_key']
    movies_nowshowing = mgi.read_movies('NOWSHOWING', ROBINSONS_MALLS, is_mutiple_branch=True)
    movies_comingsoon = mgi.read_movies('COMINGSOON', ROBINSONS_MALLS, is_mutiple_branch=True)
    movies_next_attraction = mgi.read_movies('NEXTATTRACTION', ROBINSONS_MALLS, is_mutiple_branch=True)
    movies = movies_nowshowing + movies_comingsoon + movies_next_attraction

    pack_taskgroup_payload(ctrl_key, ROBINSONS_MALLS, movies)

    return jsonify(status='ok')

@task.route('/fetch/movies/megaworld', methods=['POST'])
def fetch_movies_luckychinatown():
    log.debug("fetch_movies_luckychinatown, start fetching Megaworld (Lucky Chinatown) movies via MGi...")

    ctrl_key = request.values['ctrl_key']
    theater_code = request.values['theater_code'] if 'theater_code' in request.values else None
    movies_nowshowing = mgi.read_movies('NOWSHOWING', MEGAWORLD_MALLS, theater_code=theater_code)
    movies_comingsoon = mgi.read_movies('COMINGSOON', MEGAWORLD_MALLS, theater_code=theater_code)
    movies = movies_nowshowing + movies_comingsoon

    pack_taskgroup_payload(ctrl_key, MEGAWORLD_MALLS, movies)

    return jsonify(status='ok')

@task.route('/fetch/movies/advisory-ratings', methods=['POST'])
def fetch_movies_advisory_ratings():
    ctrl_key = request.values['ctrl_key']
    advisory_ratings = sureseats.read_advisory_ratings_for_movies() # in the future, load this from a different source.

    # FIXME: even if this is coming from Sureseats and should be tagged as org AYALA_MALLS, we can't (because of the semantics of TaskGroupControl).
    pack_taskgroup_payload(ctrl_key, GLOBE, advisory_ratings)

    return jsonify(status='ok')

@task.route('/heuristics/movies', methods=['POST'])
def run_movies_heuristics():
    ctrl_key = request.values['ctrl_key']
    ctrl = ndb.Key(urlsafe=ctrl_key).get()

    if ctrl:
        transform_movies(ctrl.payloads)
        ctrl.key.delete()
        refresh_movies()

        return jsonify(status='ok')

    return jsonify(status='skipped')


###############
#
# Theater & Cinema Tasks via CRON
#
###############

@task.route('/fetch/theaters/ayala')
def fetch_theaters_ayala():
    log.debug("fetch_theaters_ayala, start fetching Ayala Malls theaters via SureSeats...")

    deferred.defer(theaters.read_theaters, _queue='theatersqueue')

    return jsonify(status='ok')

@task.route('/fetch/theaters/sm-malls')
def fetch_theaters_smmalls():
    log.debug("fetch_theaters_smmalls, start fetching SM Malls theaters and cinemas via MGi...")

    theaterorg_key = ndb.Key(TheaterOrganization, str(SM_MALLS))
    theaters = Theater.query(ancestor=theaterorg_key).fetch()

    for theater in theaters:
        log.debug("fetch_theaters_smmalls, create theater tasks for %s..." % theater.name)

        deferred.defer(mgi.read_theaters, SM_MALLS, theater_code=theater.org_theater_code, is_mutiple_branch=True, _queue='theatersqueue')

    return jsonify(status='ok')

@task.route('/fetch/theaters/robinsons')
def fetch_theaters_robinsons():
    log.debug("fetch_theaters_robinsons, start fetching Robinsons Malls theaters and cinemas via MGi...")

    theaterorg_key = ndb.Key(TheaterOrganization, str(ROBINSONS_MALLS))
    theaters = Theater.query(ancestor=theaterorg_key).fetch()

    for theater in theaters:
        log.debug("fetch_theaters_robinsons, create theater tasks for %s..." % theater.name)

        deferred.defer(mgi.read_theaters, ROBINSONS_MALLS, theater_code=theater.org_theater_code, is_mutiple_branch=True, _queue='theatersqueue')

    return jsonify(status='ok')

@task.route('/fetch/theaters/megaworld')
def fetch_theaters_megaworld():
    log.debug("fetch_theaters_megaworld, start fetching Megaworld Malls theaters and cinemas via MGi...")

    mgi_branch_codes = tx.mgi.ALLOWED_MGI_RESERVATION_BRANCH_CODES
    theaterorg_key = ndb.Key(TheaterOrganization, str(MEGAWORLD_MALLS))
    theaters = Theater.query(Theater.org_theater_code.IN(mgi_branch_codes), ancestor=theaterorg_key).fetch()

    for theater in theaters:
        log.debug("fetch_theaters_megaworld, create theater tasks for %s..." % theater.name)

        deferred.defer(mgi.read_theaters, MEGAWORLD_MALLS, theater_code=theater.org_theater_code, is_mutiple_branch=False, _queue='theatersqueue')

    return jsonify(status='ok')

@task.route('/fetch/cinemas/power-plant-mall')
def fetch_cinemas_powerplantmall():
    log.debug("fetch_cinemas_powerplantmall, start fetching Rockwell (Power Plant Mall) cinemas...")

    deferred.defer(rockwell.read_cinema_feed_powerplantmall, _queue='theatersqueue')

    return jsonify(status='ok')

@task.route('/fetch/cinemas/greenhills')
def fetch_cinemas_greenhills():
    log.debug("fetch_cinemas_greenhills, start fetching Greenhills Malls cinemas...")

    deferred.defer(greenhills.read_cinemas_feed_greenhills, 'GHP', _queue='theatersqueue')
    deferred.defer(greenhills.read_cinemas_feed_greenhills, 'GHT', _queue='theatersqueue')

    return jsonify(status='ok')

@task.route('/fetch/cinemas/cinema76')
def fetch_cinemas_cinema76():
    log.debug("fetch_cinemas_cinema76, start fetching Cinema 76 Malls cinemas...")

    theaterorg_key = ndb.Key(TheaterOrganization, str(CINEMA76_MALLS))
    theaters = Theater.query(ancestor=theaterorg_key).fetch()

    for theater in theaters:
        log.debug("fetch_cinemas_cinema76, create theater tasks for %s..." % theater.name)

        deferred.defer(cinema76.read_cinemas_feed_cinema76, theater_code=theater.org_theater_code, _queue='theatersqueue')

    return jsonify(status='ok')

@task.route('/fetch/cinemas/globeevents')
def fetch_cinemas_globeevents():
    log.debug("fetch_cinemas_globeevents, start fetching Globe Events locations...")

    theaterorg_key = ndb.Key(TheaterOrganization, str(GLOBE_EVENTS))
    theaters = Theater.query(ancestor=theaterorg_key).fetch()

    for theater in theaters:
        log.debug("fetch_cinemas_globeevents, create theater tasks for %s..." % theater.name)

        deferred.defer(globeevents.read_cinemas_feed_globeevents, theater_code=theater.org_theater_code, _queue='theatersqueue')

    return jsonify(status='ok')

@task.route('/fetch/cinemas/citymall')
def fetch_cinemas_citymall():
    log.debug("fetch_cinemas_citymall, start fetching Globe Events locations...")

    theaterorg_key = ndb.Key(TheaterOrganization, str(CITY_MALLS))
    theaters = Theater.query(ancestor=theaterorg_key).fetch()

    for theater in theaters:
        log.debug("fetch_cinemas_citymall, create theater tasks for %s..." % theater.name)

        deferred.defer(citymall.read_cinemas_feed_citymall, theater_code=theater.org_theater_code, _queue='theatersqueue')

    return jsonify(status='ok')

###############
#
# Schedules TaskGroup via CRON
#
###############

@task.route('/fetch/schedules')
def fetch_all_schedules_feeds():
    ctrl = TaskGroupControl()
    ctrl.feed_count = 5
    ctrl.payloads = {}
    ctrl.put()
    ctrl_key = ctrl.key.urlsafe()

    taskqueue.add(url=url_for('.fetch_schedules_sureseats_taskgroup'), queue_name='schedulesqueue', params=dict(ctrl_key=ctrl_key))
    # Rockwell already disabled - Aug 31, 2017
    # Rockwell enabled movie schedule listing ONLY - Dec 14, 2017
    # Rockwell remove movie schedule listing ONLY - May 16, 2018
    #taskqueue.add(url=url_for('.fetch_schedules_powerplantmall_taskgroup'), queue_name='schedulesqueue', params=dict(ctrl_key=ctrl_key))
    taskqueue.add(url=url_for('.fetch_schedules_greenhills_taskgroup'), queue_name='schedulesqueue', params=dict(ctrl_key=ctrl_key))
    # Cinema 76 enabled - Nov 22, 2017
    taskqueue.add(url=url_for('.fetch_schedules_cinema76_taskgroup'), queue_name='schedulesqueue', params=dict(ctrl_key=ctrl_key))
    taskqueue.add(url=url_for('.fetch_schedules_globeevents_taskgroup'), queue_name='schedulesqueue', params=dict(ctrl_key=ctrl_key))
    taskqueue.add(url=url_for('.fetch_schedules_megaworld_taskgroup'), queue_name='schedulesqueue', params=dict(ctrl_key=ctrl_key))

    return jsonify(status='ok')

# separate the taskgroup for SM Malls, because of the numbers of it's cinemas.
@task.route('/fetch/schedules2')
def fetch_all_schedules_feeds2():
    number_of_tasks = 35

    # Divide processing of SM Malls theaters by 20 using their branch id
    for mod in range(0, number_of_tasks):
        ctrl = TaskGroupControl()
        ctrl.feed_count = 1
        ctrl.payloads = {}
        ctrl.put()
        ctrl_key = ctrl.key.urlsafe()

        taskqueue.add(url=url_for('.fetch_schedules_smmalls_taskgroup'), queue_name='schedulesqueue', params=dict(ctrl_key=ctrl_key, mod=mod, feed=number_of_tasks))

    return jsonify(status='ok')

# separate the taskgroup for Robinsons (like SM has many cinemas)
@task.route('/fetch/schedules-robinsons')
def fetch_all_schedules_feeds_robinsons():
    number_of_tasks = 3

    # Divide processing of Robinsons theaters by 3 using their branch id
    for mod in range(0, number_of_tasks):
        ctrl = TaskGroupControl()
        ctrl.feed_count = 1
        ctrl.payloads = {}
        ctrl.put()
        ctrl_key = ctrl.key.urlsafe()

        taskqueue.add(url=url_for('.fetch_schedules_robinsons_taskgroup'), queue_name='schedulesqueue', params=dict(ctrl_key=ctrl_key, mod=mod, feed=number_of_tasks))

    return jsonify(status='ok')

@task.route('/fetch/schedules-citymall')
def fetch_all_schedules_feeds_citymall():
    number_of_tasks = 2

    # Divide processing of theaters
    for mod in range(0, number_of_tasks):
        ctrl = TaskGroupControl()
        ctrl.feed_count = 1
        ctrl.payloads = {}
        ctrl.put()
        ctrl_key = ctrl.key.urlsafe()

        taskqueue.add(url=url_for('.fetch_schedules_citymall_taskgroup'), queue_name='schedulesqueue', params=dict(ctrl_key=ctrl_key, mod=mod, feed=number_of_tasks))

    return jsonify(status='ok')

@task.route('/fetch/schedules/ayala-malls/taskgroup', methods=['POST'])
def fetch_schedules_sureseats_taskgroup():
    log.debug("fetch_schedules_sureseats_taskgroup, start fetching Ayala Malls schedules via SureSeats...")

    ctrl_key = request.values['ctrl_key']
    scheds = sureseats.read_schedules()

    pack_taskgroup_payload_schedules(ctrl_key, AYALA_MALLS, scheds)

    return jsonify(status='ok')

@task.route('/fetch/schedules/power-plant-mall/taskgroup', methods=['POST'])
def fetch_schedules_powerplantmall_taskgroup():
    log.debug("fetch_schedules_powerplantmall_taskgroup, start fetching Rockwell (Power Plant Mall) schedules...")

    ctrl_key = request.values['ctrl_key']
    scheds = rockwell.read_schedules_feed_powerplantmall()

    pack_taskgroup_payload_schedules(ctrl_key, ROCKWELL_MALLS, scheds)

    return jsonify(status='ok')

@task.route('/fetch/schedules/greenhills/taskgroup', methods=['POST'])
def fetch_schedules_greenhills_taskgroup():
    log.debug("fetch_schedules_greenhills_taskgroup, start fetching Greenhills Malls schedules...")

    ctrl_key = request.values['ctrl_key']
    scheds = greenhills.read_schedules_feed_greenhills()

    pack_taskgroup_payload_schedules(ctrl_key, GREENHILLS_MALLS, scheds)

    return jsonify(status='ok')

@task.route('/fetch/schedules/cinema76/taskgroup', methods=['POST'])
def fetch_schedules_cinema76_taskgroup():
    log.debug("fetch_schedules_cinema76_taskgroup, start fetching Cinema 76 Malls schedules...")

    ctrl_key = request.values['ctrl_key']
    scheds = cinema76.read_schedules_feed_cinema76()

    pack_taskgroup_payload_schedules(ctrl_key, CINEMA76_MALLS, scheds)

    return jsonify(status='ok')

@task.route('/fetch/schedules/globeevents/taskgroup', methods=['POST'])
def fetch_schedules_globeevents_taskgroup():
    log.debug("fetch_schedules_globeevents_taskgroup, start fetching Globe Events schedules...")

    ctrl_key = request.values['ctrl_key']
    scheds = globeevents.read_schedules_feed_globeevents()

    pack_taskgroup_payload_schedules(ctrl_key, GLOBE_EVENTS, scheds)

    return jsonify(status='ok')

@task.route('/fetch/schedules/sm-malls/taskgroup', methods=['POST'])
def fetch_schedules_smmalls_taskgroup():
    log.debug("fetch_schedules_smmalls_taskgroup, start fetching SM Malls schedules...")

    org_uuid = SM_MALLS
    theaterorg_key = ndb.Key(TheaterOrganization, str(org_uuid))
    theaterorg = theaterorg_key.get()

    if not theaterorg:
        return jsonify(status='skipped')

    scheds     = []
    ctrl_key   = request.values['ctrl_key']
    mod        = request.values['mod']
    feed_count = request.values['feed']
    theaters   = Theater.query(ancestor=theaterorg_key).fetch()

    for theater in theaters:
        branch_key = theater.branch_id
        theater_code = theater.org_theater_code

        # Check if branch will be processed by this task
        if (int(branch_key) % int(feed_count)) != int(mod):
            log.debug("SKIP!, fetch_schedules_smmalls_taskgroup, branch_key: %s not processed in mod: %s..." % (branch_key, mod))
            continue
        log.debug("fetch_schedules_smmalls_taskgroup, branch_key: %s currently processed in mod: %s..." % (branch_key, mod))

        if not branch_key:
            log.warn("SKIP!, fetch_schedules_smmalls_taskgroup, missing branch_key...")

            continue

        if not theater_code:
            log.warn("SKIP!, fetch_schedules_smmalls_taskgroup, missing theater_code...")

            continue

        r_scheds = mgi.read_schedules(branch_key, str(org_uuid), theater_code, days_delta=90, is_mutiple_branch=True)
        scheds.extend(r_scheds)

    pack_taskgroup_payload_schedules(ctrl_key, org_uuid, scheds)

    return jsonify(status='ok')

@task.route('/fetch/schedules/robinsons/taskgroup', methods=['POST'])
def fetch_schedules_robinsons_taskgroup():
    log.debug("fetch_schedules_robinsons_taskgroup, start fetching Robinsons Malls schedules...")

    org_uuid = ROBINSONS_MALLS
    theaterorg_key = ndb.Key(TheaterOrganization, str(org_uuid))
    theaterorg = theaterorg_key.get()

    if not theaterorg:
        return jsonify(status='skipped')

    scheds     = []
    ctrl_key   = request.values['ctrl_key']
    mod        = request.values['mod']
    feed_count = request.values['feed']
    theaters   = Theater.query(ancestor=theaterorg_key).fetch()

    for theater in theaters:
        branch_key = theater.branch_id
        theater_code = theater.org_theater_code

        # Check if branch will be processed by this task
        if (int(branch_key) % int(feed_count)) != int(mod):
            log.debug("SKIP!, fetch_schedules_robinsons_taskgroup, branch_key: %s not processed in mod: %s..." % (branch_key, mod))
            continue
        log.debug("fetch_schedules_robinsons_taskgroup, branch_key: %s currently processed in mod: %s..." % (branch_key, mod))

        if not branch_key:
            log.warn("SKIP!, fetch_schedules_robinsons_taskgroup, missing branch_key...")

            continue

        if not theater_code:
            log.warn("SKIP!, fetch_schedules_robinsons_taskgroup, missing theater_code...")

            continue

        r_scheds = mgi.read_schedules(branch_key, str(org_uuid), theater_code, days_delta=31, is_mutiple_branch=True)
        scheds.extend(r_scheds)

    log.debug("ROBINSONS_SCHEDULE : %s" % (scheds))

    pack_taskgroup_payload_schedules(ctrl_key, org_uuid, scheds)

    return jsonify(status='ok')

@task.route('/fetch/schedules/citymall/taskgroup', methods=['POST'])
def fetch_schedules_citymall_taskgroup():
    log.debug("fetch_schedules_citymall_taskgroup, start fetching City Malls schedules...")

    org_uuid = CITY_MALLS
    theaterorg_key = ndb.Key(TheaterOrganization, str(org_uuid))
    theaterorg = theaterorg_key.get()

    if not theaterorg:
        return jsonify(status='skipped')

    scheds     = []
    ctrl_key   = request.values['ctrl_key']
    mod        = request.values['mod']
    feed_count = request.values['feed']
    theaters   = Theater.query(ancestor=theaterorg_key).fetch()
    th_ctr     = 0

    for theater in theaters:
        theater_code = theater.org_theater_code
        th_ctr += 1

        # Check if branch will be processed by this task
        if (int(th_ctr) % int(feed_count)) != int(mod):
            log.debug("SKIP!, fetch_schedules_citymall_taskgroup, th_ctr: %s not processed in mod: %s..." % (th_ctr, mod))
            continue
        log.debug("fetch_schedules_citymall_taskgroup, th_ctr: %s currently processed in mod: %s..." % (th_ctr, mod))

        if not theater_code:
            log.warn("SKIP!, fetch_schedules_citymall_taskgroup, missing theater_code...")
            continue

        r_scheds = citymall.read_schedules_feed_citymall(theater_code, theater.location_code)
        scheds.extend(r_scheds)

    pack_taskgroup_payload_schedules(ctrl_key, org_uuid, scheds)

    return jsonify(status='ok')

@task.route('/fetch/schedules/megaworld/taskgroup', methods=['POST'])
def fetch_schedules_megaworld_taskgroup():
    log.debug("fetch_schedules_megaworld_taskgroup, start fetching Megaworld Malls schedules via MGi...")

    org_uuid = MEGAWORLD_MALLS
    mgi_branch_codes = tx.mgi.ALLOWED_MGI_RESERVATION_BRANCH_CODES
    theaterorg_key = ndb.Key(TheaterOrganization, str(org_uuid))
    theaterorg = theaterorg_key.get()

    if not theaterorg:
        return jsonify(status='skipped')

    scheds = []
    ctrl_key = request.values['ctrl_key']
    theaters = Theater.query(Theater.org_theater_code.IN(mgi_branch_codes), ancestor=theaterorg_key).fetch()

    for theater in theaters:
        branch_key = theater.branch_id
        theater_code = theater.org_theater_code

        if not branch_key:
            log.warn("SKIP!, fetch_schedules_megaworld_taskgroup, missing branch_key...")

            continue

        if not theater_code:
            log.warn("SKIP!, fetch_schedules_megaworld_taskgroup, missing theater_code...")

            continue

        r_scheds = mgi.read_schedules2(branch_key, str(org_uuid), theater_code, is_mutiple_branch=False)
        scheds.extend(r_scheds)

    pack_taskgroup_payload_schedules(ctrl_key, org_uuid, scheds)

    return jsonify(status='ok')

@task.route('/heuristics/schedules/taskgroup', methods=['POST'])
def run_schedules_heuristics_taskgroup():
    ctrl_key = request.values['ctrl_key']
    ctrl = ndb.Key(urlsafe=ctrl_key).get()

    if ctrl:
        schedules.transform_schedules_taskgroup(ctrl.payloads)
        ctrl.key.delete()
        refresh_movies(queue_name='schedulesqueue')

        return jsonify(status='ok')

    return jsonify(status='skipped')


###############
#
# Schedule Tasks via CRON
#
###############

@task.route('/fetch/schedules/ayala')
def fetch_schedules_sureseats():
    log.debug("fetch_schedules_sureseats, start fetching Ayala Malls schedules via SureSeats...")

    scheds = sureseats.read_schedules()
    payload = create_task_payload(dict(schedule_dictionary_list=scheds))

    taskqueue.add(url=url_for('.run_schedules_heuristics'), queue_name='schedulesqueue', params={'payload': payload})

    return jsonify(status='ok')

@task.route('/fetch/schedules/power-plant-mall')
def fetch_schedules_powerplantmall():
    log.debug("fetch_schedules_powerplantmall, start fetching Rockwell (Power Plant Mall) schedules...")

    schedules = rockwell.read_schedules_feed_powerplantmall()
    payload = create_task_payload(dict(schedule_dictionary_list=schedules))

    taskqueue.add(url=url_for('.run_schedules_heuristics2'), queue_name='schedulesqueue', params={'payload': payload})

    return jsonify(status='ok')

@task.route('/fetch/schedules/greenhills')
def fetch_schedules_greenhills():
    log.debug("fetch_schedules_greenhills, start fetching Greenhills Malls schedules...")

    schedules = greenhills.read_schedules_feed_greenhills()
    payload = create_task_payload(dict(schedule_dictionary_list=schedules))

    taskqueue.add(url=url_for('.run_schedules_heuristics2'), queue_name='schedulesqueue', params={'payload': payload})

    return jsonify(status='ok')

@task.route('/fetch/schedules/sm-malls')
def fetch_schedules_smmalls():
    log.debug("fetch_schedules_smmalls, start fetching SM Malls schedules via MGi...")

    theaterorg_key = ndb.Key(TheaterOrganization, str(SM_MALLS))
    theaters = Theater.query(ancestor=theaterorg_key).fetch()

    for theater in theaters:
        log.debug("fetch_schedules_smmalls, create schedule tasks for %s..." % theater.name)

        taskqueue.add(url=url_for('.run_schedules_heuristics_mgi'), queue_name='schedulesqueue',
                params={'theaterorg_id': str(SM_MALLS), 'theater_id': theater.key.id()})

    return jsonify(status='ok')

@task.route('/fetch/schedules/megaworld')
def fetch_schedules_megaworld():
    log.debug("fetch_schedules_megaworld, start fetching Megaworld Malls schedules via MGi...")

    mgi_branch_codes = tx.mgi.ALLOWED_MGI_RESERVATION_BRANCH_CODES
    theaterorg_key = ndb.Key(TheaterOrganization, str(MEGAWORLD_MALLS))
    theaters = Theater.query(Theater.org_theater_code.IN(mgi_branch_codes), ancestor=theaterorg_key).fetch()

    for theater in theaters:
        log.debug("fetch_schedules_megaworld, create schedule tasks for %s..." % theater.name)

        taskqueue.add(url=url_for('.run_schedules_heuristics_mgi2'), queue_name='schedulesqueue',
                params={'theaterorg_id': str(MEGAWORLD_MALLS), 'theater_id': theater.key.id()})

    return jsonify(status='ok')

@task.route('/heuristics/schedules', methods=['POST'])
def run_schedules_heuristics():
    payload_key = request.values['payload']
    payload = get_task_payload(payload_key)

    if payload:
        transform_schedules(**payload)
        drop_task_payload(payload_key)

    return jsonify(status='ok')

@task.route('/heuristics/schedules2', methods=['POST'])
def run_schedules_heuristics2():
    payload_key = request.values['payload']
    payload = get_task_payload(payload_key)

    if payload:
        schedules.transform_schedules_by_theaterorg(**payload)
        drop_task_payload(payload_key)

    return jsonify(status='ok')

@task.route('/heuristics/schedules/mgi', methods=['POST'])
def run_schedules_heuristics_mgi():
    theaterorg_id = request.values['theaterorg_id']
    theater_id = int(request.values['theater_id'])
    theater_key = ndb.Key(TheaterOrganization, theaterorg_id, Theater, theater_id)
    theater = theater_key.get()

    if not theater:
        log.warn("SKIP!, theater not found with theaterorg_id: %s, theater_id: %s..." % (theaterorg_id, theater_id))

        return jsonify(status='ok')

    if theaterorg_id not in THEATERORG_UUID:
        log.warn("SKIP!, Theater Organization UUID %s doesn't exist..." % theaterorg_id)

        return jsonify(status='ok')

    org_uuid = THEATERORG_UUID[theaterorg_id]
    branch_key = theater.branch_id
    theater_code = theater.org_theater_code

    if branch_key:
        schedule_list = mgi.read_schedules(branch_key, org_uuid, theater_code, days_delta=3, is_mutiple_branch=True)
        schedule_dictionary_list = {'schedule_dictionary_list': schedule_list}
        schedules.transform_schedules_by_theaterorg(**schedule_dictionary_list)

    return jsonify(status='ok')

@task.route('/heuristics/schedules/mgi2', methods=['POST'])
def run_schedules_heuristics_mgi2():
    theaterorg_id = request.values['theaterorg_id']
    theater_id = int(request.values['theater_id'])
    theater_key = ndb.Key(TheaterOrganization, theaterorg_id, Theater, theater_id)
    theater = theater_key.get()

    if not theater:
        log.warn("SKIP!, theater not found with theaterorg_id: %s, theater_id: %s..." % (theaterorg_id, theater_id))

        return jsonify(status='ok')

    if theaterorg_id not in THEATERORG_UUID:
        log.warn("SKIP!, Theater Organization UUID %s doesn't exist..." % theaterorg_id)

        return jsonify(status='ok')

    org_uuid = THEATERORG_UUID[theaterorg_id]
    branch_key = theater.branch_id
    theater_code = theater.org_theater_code

    if branch_key:
        schedule_list = mgi.read_schedules2(branch_key, org_uuid, theater_code, is_mutiple_branch=False)
        schedule_dictionary_list = {'schedule_dictionary_list': schedule_list}
        schedules.transform_schedules_by_theaterorg(**schedule_dictionary_list)

    return jsonify(status='ok')


###############
#
# Refresh Movies Tasks via CRON
#
###############

@task.route('/refresh-movies')
def refresh_movies(queue_name='default'):
    activate_movies_nowshowing() # ensure that all now showing movies are marked is_showing = True.
    deactivate_movies_comingsoon() # ensure that all coming soon movies are marked is_showing = False.

    taskqueue.add(url=url_for('.refresh_movies_expire'), queue_name=queue_name)
    taskqueue.add(url=url_for('.refresh_movies_unexpire'), queue_name=queue_name)
    taskqueue.add(url=url_for('.refresh_movies_unexpire_comingsoon'), queue_name=queue_name)
    taskqueue.add(url=url_for('.refresh_movies_gmoviesdigitalventures'), queue_name=queue_name)

    return jsonify(status='ok')

@task.route('/refresh-movies/expire', methods=['POST'])
def refresh_movies_expire():
    expire_movies()

    return jsonify(status='ok')

@task.route('/refresh-movies/unexpire', methods=['POST'])
def refresh_movies_unexpire():
    unexpire_movies()

    return jsonify(status='ok')

@task.route('/refresh-movies/unexpire-coming-soon', methods=['POST'])
def refresh_movies_unexpire_comingsoon():
    unexpire_movies_comingsoon()

    return jsonify(status='ok')

@task.route('/refresh-movies/gmovies-digital-ventures', methods=['POST'])
def refresh_movies_gmoviesdigitalventures():
    reqs = []

    try:
        if 'movie_id' in request.values:
            log.debug("refresh_movies_gmoviesdigitalventures, movie_id found...")

            movie_id = request.values['movie_id']
            movie_key = ndb.Key(Movie, movie_id)
            movie = movie_key.get()

            if not movie:
                return jsonify(status='skipped')

            posters_args = {}
            portrait_default_poster, landscape_default_poster = get_default_poster_url()
            headers = {'Content-type': 'application/json'}
            params = json.dumps(movie.to_entity2(posters_args, portrait_default_poster, landscape_default_poster))

            for base_url in GMOVIES_DIGITALVENTURES_V1_BASE_URLS:
                path_url = '/movies-update/%s' % movie_id
                request_url = '%s%s' % (base_url, path_url)

                log.debug("refresh_movies_gmoviesdigitalventures, request_url, %s..." % request_url)

                req = requests.Request('POST', url=request_url, headers=headers, data=params).prepare()
                reqs.append(req)
        else:
            log.debug("refresh_movies_gmoviesdigitalventures, movie_id not found...")

            for base_url in GMOVIES_DIGITALVENTURES_V1_BASE_URLS:
                path_url = '/movies-update'
                request_url = '%s%s' % (base_url, path_url)

                log.debug("refresh_movies_gmoviesdigitalventures, request_url, %s..." % request_url)

                req = requests.Request('GET', url=request_url).prepare()
                reqs.append(req)
    except Exception, e:
        log.warn("ERROR!, refresh_movies_gmoviesdigitalventures...")
        log.error(e)

        return jsonify(status='skipped')

    for req in reqs:
        try:
            s = requests.Session()
            s.send(req, timeout=TIMEOUT_DEADLINE)
        except Exception, e:
            log.warn("ERROR!, refresh_movies_gmoviesdigitalventures, connection/response issues...")
            log.error(e)

    return jsonify(status='ok')


###############
#
# Transaction Tasks
#
###############

@task.route('/process_tx', methods=['POST'])
def process_tx():
    log.debug("Got request: %s" % request.values)
    state = tx.next_state(request.values)
    log.debug('TX process exit-----------------------------------')

    return jsonify(state=tx.to_state_str(state))

@task.route('/reap_tx', methods=['POST'])
def reap_tx():
   device_id = request.values['device_id']
   tx_id = request.values['tx_id']
   log.debug('Reaping outstanding transactions which have exceeded their timeout values: tx %s via device %s' % (tx_id, device_id))
   id_reaped = (device_id, tx_id)
   tx.reap_stale_tx(device_id, tx_id)

   return jsonify(reaped=id_reaped)

@task.route('/requery_tx', methods=['POST'])
def requery_tx():
    device_id = request.values['device_id']
    tx_id = request.values['tx_id']

    log.info("requery_tx, requery transactions, tx %s via device %s" % (tx_id, device_id))

    id_requery = (device_id, tx_id)
    tx.requery_stale_tx(device_id, tx_id)

    return jsonify(requery=id_requery)

@task.route('/callback/trigger_listeners', methods=['GET', 'POST'])
def trigger_callback_listeners():
    channel_name = request.values['channel_name']
    log.info("Channel name: {}".format(channel_name))
    version = os.environ['CURRENT_VERSION_ID'].split('.')
    major_version = version[0]
    listeners = Listener.get_listeners_by_version(channel_name, major_version)
    log.info("Listeners: {}".format(listeners))
    outstanding = Event.get_events(channel_name)

    for l in listeners:
        log.info("Processing listener callback: {} for channel: {}".format(l.callback, outstanding))
        l.callback(outstanding)

    ndb.put_multi(outstanding)

    return jsonify(status='ok')


###############
#
# GMovies Digital Ventures (GMovies Digital Ventures)
#
###############

@task.route('/gmovies-digital-ventures/blocked-screening')
def gmoviesdigitalventures_blockedscreening():
    reqs = []

    try:
        log.debug("gmoviesdigitalventures_blockedscreening, start...")

        for base_url in GMOVIES_DIGITALVENTURES_V1_BASE_URLS:
            path_url = '/block-screening-update/'
            request_url = '%s%s' % (base_url, path_url)

            log.debug("gmoviesdigitalventures_blockedscreening, request_url, %s..." % request_url)
            log.info("gmoviesdigitalventures_blockedscreening, request_url, %s..." % request_url)

            req = requests.Request('GET', url=request_url).prepare()
            reqs.append(req)
    except Exception, e:
        log.warn("ERROR!, gmoviesdigitalventures_blockedscreening...")
        log.error(e)

        return jsonify(status='skipped')

    for req in reqs:
        try:
            s = requests.Session()
            s.send(req, timeout=TIMEOUT_DEADLINE)
        except Exception, e:
            log.warn("ERROR!, gmoviesdigitalventures_blockedscreening, connection/response issues...")
            log.error(e)

    return jsonify(status='ok')


###############
#
# Blocked Screening Transasction Tasks
#
###############

@task.route('/process_tx_promo', methods=['POST'])
def process_tx_promo():
    log.debug("PROMO: Got request: %s" % request.values)
    state = promo.next_state(request.values)
    log.debug('TX process exit-----------------------------------')

    return jsonify(state=promo.to_state_str_promo(state))

@task.route('/reap_tx_promo', methods=['POST'])
def reap_tx_promo():
   device_id = request.values['device_id']
   tx_id = request.values['tx_id']
   log.debug("PROMO: Reaping outstanding transactions which have exceeded their timeout values: tx %s via device %s" % (tx_id, device_id))
   id_reaped = (device_id, tx_id)
   promo.reap_stale_tx(device_id, tx_id)

   return jsonify(reaped=id_reaped)

@task.route('/requery_tx_promo', methods=['POST'])
def requery_tx_promo():
    device_id = request.values['device_id']
    tx_id = request.values['tx_id']
    log.info("BLOCKED SCREENING: Requery transactions: tx %s via device %s" % (tx_id, device_id))
    id_requery = (device_id, tx_id)
    promo.paynamics_requery_tx(device_id, tx_id)

    return jsonify(requery=id_requery)


##########
#
# Rotten Tomatoes Movie Tasks via CRON
#
##########

@task.route('/fetch/movies/rottentomatoes')
def fetch_movies_rottentomatoes():
    rottentomatoes_movies = rottentomatoes.get_movie_details()
    payload = create_task_payload(dict(movie_dictionary_list=rottentomatoes_movies))

    taskqueue.add(url=url_for('.run_movies_heuristics_rottentomatoes'), params={'payload': payload})

    return jsonify(status='ok')

@task.route('/fetch/ratings/rottentomatoes')
def fetch_ratings_rottentomatoes():
    rottentomatoes_ratings = rottentomatoes.get_rt_movie_ratings()

    TaskGroupControl.launch_singleton_taskgroup(ROTTEN_TOMATOES, rottentomatoes_ratings)

    return jsonify(status='ok')

@task.route('/heuristics/movies/rottentomatoes', methods=['POST'])
def run_movies_heuristics_rottentomatoes():
    payload_key = request.values['payload']
    payload = get_task_payload(payload_key)

    if payload:
        rottentomatoes_transform_movies(**payload)
        drop_task_payload(payload_key)

    return jsonify(status='ok')


###############
#
# Scraper's Schedules Tasks via CRON
#
###############

@task.route('/fetch/schedules/scraper')
def fetch_schedules_scraper():
    log.debug("fetch_schedules_scraper, start fetching Robinsons, Megaworld, Greenhills, Festival, Gateway, and Shangri-La schedules via scraper...")

    scraper_schedules = []
    org_uuids = [MEGAWORLD_MALLS, FESTIVAL_MALLS, GATEWAY_MALLS, SHANGRILA_MALLS, CINEMA2000_MALLS, ROCKWELL_MALLS]

    for org_uuid in org_uuids:
        schedules = scraper.scraper_read_schedules(org_uuid)
        scraper_schedules += schedules

    payload = create_task_payload(dict(scraper_schedules=scraper_schedules))

    taskqueue.add(url=url_for('.run_schedules_heuristics_scraper'), queue_name='schedulesqueue', params={'payload': payload})

    return jsonify(status='ok')

@task.route('/heuristics/schedules/scraper', methods=['POST'])
def run_schedules_heuristics_scraper():
    payload_key = request.values['payload']
    payload = get_task_payload(payload_key)

    if payload:
        scraper_transform_schedules(**payload)
        drop_task_payload(payload_key)
        refresh_movies(queue_name='schedulesqueue')

    return jsonify(status='ok')


###############
#
# Update Schema Tasks
#
###############

@task.route('/admin/movies/update-schema/')
def movie_update_schema():
    deferred.defer(update_schema_models, Movie)

    return jsonify(status='started')

@task.route('/admin/ticket_template/update-schema/')
def ticket_template_update_schema():
    deferred.defer(update_schema_models, TicketTemplateImageBlob)

    return jsonify(status='started')


###############
#
# Additional Tasks
#
###############

@task.route('/fetch/poster_image/scaler/', methods=['POST'])
def scale_poster_images():
    source_key = ndb.Key(urlsafe=request.values['source_key'])
    image_filename = request.values['image_filename']
    image_density = request.values['image_density']
    target_density = request.values['target_density']

    try:
        image_blob = transform_poster_images(source_key.get(), image_filename, image_density, target_density)
        cache_key = "movie::poster::%s::%s::%s::%s" % (image_blob.key.parent().id(), image_blob.orientation, image_blob.resolution, image_blob.application_type)
        poster_url = get_serving_url(image_blob.image)

        if image_blob.is_default or not image_blob.key.parent() or image_blob.key.parent() is None:
            cache_key = "movie::default::poster::%s::%s::%s" % (image_blob.orientation, image_blob.resolution, image_blob.application_type)

        memcache.set(cache_key, poster_url)

        log.debug("scale_poster_images, movie scaler, poster image cached: %s..." % cache_key)
    except Exception, e:
        log.warn("ERROR!, scale_poster_images, failed to scale poster image...")
        log.error(e)

    return jsonify(status='ok')

@task.route('/admin/schema-migrate')
def migrate_schema():
    deferred.defer(update_schema)

    return jsonify(status='started')

@task.route('/admin/fsck/<model>')
def check_datastore_consistency(model):
    if not entity_fsck(model):
        abort(404)

    return jsonify(status='started')

@task.route('/eplus-balance-inquiry')
def eplus_balance_inquiry():
    eplus_balance = mgi.get_eplus_balance()
    eplus_balance_notification(eplus_balance='\n'.join(eplus_balance).encode('utf-8'))

    return jsonify(status='ok')

@task.route('/sales_reports/email', methods=['GET'])
def send_mail_sales_reports():
    attachments = []
    org_uuids = [AYALA_MALLS, ROCKWELL_MALLS, GREENHILLS_MALLS, MEGAWORLD_MALLS]
    yesterday = datetime.date.today() - datetime.timedelta(days=1)
    tx_from = datetime.datetime.strptime(str(yesterday) + ' 00:00:00', '%Y-%m-%d %H:%M:%S')
    tx_to = datetime.datetime.today()
    str_tx_from = datetime.datetime.strftime(tx_from, '%Y-%m-%d %I:%M %p')
    str_tx_to = datetime.datetime.strftime(tx_to, '%Y-%m-%d %I:%M %p')

    for org_uuid in org_uuids:
        theaterorg_key = ndb.Key(TheaterOrganization, str(org_uuid))
        theaterorg = theaterorg_key.get()

        if theaterorg is None:
            continue

        if org_uuid == AYALA_MALLS:
            payment_type = 'client-initiated'
        elif org_uuid == MEGAWORLD_MALLS:
            payment_type = 'ipay88-payment-initiated'
        else:
            payment_type = 'paynamics-payment-initiated'

        file_name, file_data =  generate_sales_reports_xls(tx_from=tx_from,
                tx_to=tx_to, theaterorg=theaterorg, payment_type=payment_type,
                platform=['android', 'ios', 'website'])
        attachments.append((file_name, file_data))

    send_sales_reports(str_tx_from, str_tx_to, attachments)

    return jsonify(status='ok')

@task.route('/transaction_reports/email', methods=['GET'])
def send_mail_transactions_reports(payment_type=None, report_type=None, org_ids=None, th_code=None):
    attachments = []
    # Cinema 76 enabled - Nov 22, 2017
    org_uuids = [AYALA_MALLS, GREENHILLS_MALLS, MEGAWORLD_MALLS, SM_MALLS, ROBINSONS_MALLS, CINEMA76_MALLS, GLOBE_EVENTS]
    if org_ids: # Fetch only transactions from org_uuids provided
        org_uuids = org_ids
    if 'g-cash-app' == payment_type and 'daily' != report_type:
        org_uuids = [SM_MALLS, AYALA_MALLS, ROBINSONS_MALLS]

    # convert to Philippine time (GMT +8)
    today = datetime.datetime.today() + datetime.timedelta(hours=8)
    log.debug("send_mail_transactions_reports, today: %s" % (today))

    if 'daily' == report_type:
        yesterday         = today - datetime.timedelta(days=1)
        report_year       = yesterday.year
        report_month      = yesterday.month
        report_day        = yesterday.day
        report_month_name = calendar.month_name[report_month] + " " + str(yesterday.day)
        tx_from           = datetime.datetime(report_year, report_month, report_day, 0, 0, 0)
        tx_to             = datetime.datetime(report_year, report_month, report_day, 23, 59, 59)
    else:
        td_month = today.month
        td_year = today.year

        if 1 == td_month:
            report_month = 12
            report_year = td_year -1
        else:
            report_month = td_month - 1
            report_year = td_year

        report_month_name = calendar.month_name[report_month]
        f, last_day = calendar.monthrange(report_year, report_month)
        tx_from = datetime.datetime(report_year, report_month, 1, 0, 0, 0)
        tx_to = datetime.datetime(report_year, report_month, last_day, 23, 59, 59)

    log.debug("send_mail_transactions_reports, tx_from: %s, tx_to: %s" % (tx_from, tx_to))
    log.debug("send_mail_transactions_reports, month: %s, year: %s" % (report_month, report_year))
    log.debug("send_mail_transactions_reports, payment_type: %s" % (payment_type))
    for org_uuid in org_uuids:
        theaterorg_key = ndb.Key(TheaterOrganization, str(org_uuid))
        theaterorg = theaterorg_key.get()

        if theaterorg is None:
            continue

        file_name, file_data =  generate_transaction_reports_xls(tx_from=tx_from,
                tx_to=tx_to, theaterorg=theaterorg, month=report_month_name,
                year=report_year, payment_type=payment_type, report_type=report_type)
        attachments.append((file_name, file_data))

    send_transaction_reports(report_year, report_month_name, attachments, payment_type, report_type, th_code)

    return jsonify(status='ok')

@task.route('/transaction_reports/email/gcash_app', methods=['GET'])
def send_mail_transactions_reports_gcash_app():
    payment_type = 'g-cash-app'
    return send_mail_transactions_reports(payment_type=payment_type)

@task.route('/transaction_reports/daily/robinsons', methods=['GET'])
def send_mail_transactions_reports_daily_robinsons():
    report_type = 'daily'
    org_uuids   = [ROBINSONS_MALLS]
    return send_mail_transactions_reports(report_type=report_type, org_ids=org_uuids, th_code='ROB')

@task.route('/transaction_reports/daily/ayala', methods=['GET'])
def send_mail_transactions_reports_daily_ayala():
    report_type = 'daily'
    payment_type = 'g-cash-app'
    org_uuids   = [AYALA_MALLS]
    return send_mail_transactions_reports(report_type=report_type, org_ids=org_uuids, payment_type=payment_type, th_code='AYL')

@task.route('/notify/globe-csr', methods=['GET'])
def run_notification_worker():
    account = AccountSettings.get_by_id('csrc_settings')
    thirty_days_ago = datetime.datetime.now() - datetime.timedelta(days=30)
    last_notif = account.last_notif
    globe_csr = account.globe_csr_addresses

    log.info("Last Notification Sent: {0} CSR: 30 days ago: {1} CSR Recipients {2} ".format(last_notif, thirty_days_ago, globe_csr))
    if last_notif is not None and last_notif.date() == thirty_days_ago.date():
        log.info("Processing 30 days donors list")
        donors_list = CSRCPayment.get_bulk_monthly(last_notif)
        deferred.defer(notify_globe_csr, donors_list, globe_csr, True)
        account.last_notif = datetime.datetime.now()
    else:
        log.info("Last notification was sent on {}".format(last_notif))

    donors_latest = CSRCPayment.get_last_10()
    if donors_latest is not None and len(donors_latest) >= 10:
        log.info("Processing last 10 donors")
        deferred.defer(notify_globe_csr, donors_latest, globe_csr, False)
    else:
        log.info("Latest donors are still less than 10")
   
    account.put()
    
    return jsonify(status='ok')

@task.route('/theaters/master-reservation-switch', methods=['POST'])
def enable_disable_theater_reservations():
    if request.method == "POST":
        data = json.loads(json.dumps(request.json))

        new_status = data['new_status']
        org_id = data['theaterorg']

        theaterorg_key = ndb.Key(TheaterOrganization, str(org_id))
        theaterorg = theaterorg_key.get()
        theaterorg.allow_reservation_master_switch = int(new_status)
        theaterorg.put()

        theaters = Theater.query(ancestor=theaterorg.key).fetch()
        new_theater_key = memcache.get('theater::new::key')
        
        if new_theater_key is not None:
            new_theater = get_theater(new_theater_key)

            if new_theater not in theaters and new_theater.key.parent == theater_org.key:
                theaters.append(new_theater)

        for t in theaters:
            if is_payment_enabled(t.key.parent().id()):
                theater_id = id_encoder.encoded_theater_id(t.key)
                theater = get_theater(theater_id)
                reservation_platform = t.allow_reservation_platform

                if int(new_status):
                    if theater.previous_reservation_platform:
                        if 'None' in theater.previous_reservation_platform:
                            theater.allow_reservation_platform = []
                        else:
                            theater.allow_reservation_platform = theater.previous_reservation_platform
                        theater.previous_reservation_platform = []
                        theater.allow_reservation = True
                else:
                    if not reservation_platform and theater.allow_reservation:
                        reservation_platform = ['None']
                    theater.previous_reservation_platform = reservation_platform
                    theater.allow_reservation_platform = []
                    theater.allow_reservation = False

                theater.put()
            else:
                continue

        return jsonify(status='ok')

@task.route('/fetch/cash_reservation')
def ticket_reservation_claims():
    log.info("Ticket_Reservation_Claims...")
    date_yesterday = ((datetime.datetime.today() - datetime.timedelta(days=1)) + datetime.timedelta(hours=8)).date()
    tx_from = parse_string_to_date(str(date_yesterday) + ' 00:00:00', fdate='%Y-%m-%d %H:%M:%S')
    tx_to = parse_string_to_date(str(date_yesterday) + ' 23:59:59', fdate='%Y-%m-%d %H:%M:%S')
    log.info("Ticket_Reservation_Claims, date: %s" % str(date_yesterday))

    queryset = get_filtered_transactions(state.TX_DONE, tx_from=tx_from,
            tx_to=tx_to, payment_type=['cash'], org_uuids=[],
            theater_id=None, platform=None, payment_reference=None,
            reservation_reference=None)

    transactions = queryset.fetch()

    if not transactions:
        log.debug("Ticket_Reservation_Claims, no cash transactions yesterday...")

    if IS_STAGING:
        endpoint = SURESEATS_ENDPOINT_STAGING
    else:
        endpoint = SURESEATS_ENDPOINT_PRIMARY

    for transaction in transactions:
        status = ''
        transaction_status = str(transaction.status)
        code_list = (transaction.ticket).transaction_code_list
        
        log.info("Ticket_Reservation_Claims, for loop, status: %s" % transaction_status)
        log.info("Ticket_Reservation_Claims, for loop, transaction code list: %s" % str(code_list))
        
        for code in code_list:
            status_payload = {'src': SOURCE_ID, 'action': STATUS_ACTION, 'claim_code': str(code)}
            log.info("payload: %s" % str(status_payload))
            req = create_sureseats_request(endpoint, status_payload)

            log.info("######################### START ############################")
            log.info(req.url)
            log.info("########################## END #############################")
            s = requests.Session()
            r = s.send(req, timeout=TIMEOUT_DEADLINE)

            if r.status_code != 200:
                log.warn("ERROR!, ticket_reservation_claims, cannot perform status retrieval, there was an error calling SureSeats...")
                return jsonify(status='skipped')
            
            log.info("ticket_reservation_claims, got response: %s..." % str(r.text))

            xml = etree.fromstring(str(r.text))
            status = str(xml.xpath('//TransStatus/Status/text()')[0]).lower()

        if status:
            transaction_data = transaction.key.get()
            if status == 'claimed':
                transaction_data.status = 'claimed over-the-counter'
                transaction_data.put()
            elif status in ['forfeited', 'cancelled']:
                transaction_data.status = status
                transaction_data.put()

    return jsonify(status='ok')