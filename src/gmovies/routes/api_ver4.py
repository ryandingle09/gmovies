import logging
import sys
import time
import traceback

from datetime import date, datetime
from operator import attrgetter

from flask import Blueprint, jsonify, request, render_template

from google.appengine.api.images import get_serving_url
from google.appengine.ext import ndb
from google.appengine.ext.ndb import (Key, GenericProperty, FloatProperty,
        IntegerProperty, ComputedProperty, BooleanProperty, DateProperty,
        KeyProperty, OR, get_multi, transactional)
from google.appengine.runtime.apiproxy_errors import OverQuotaError

from .auth import web_authenticate_request
from .util import (apply_sort, check_filter_and_sort_args, do_filter, do_get_distance, do_get_schedules,
        do_location_search, do_parse_comingsoon, get_default_poster_url, get_movie_status, org_with_theaters,
        query_filters, resolve_dotted_name, theaterorg_theater_safety_check, to_theater_key,
        ENTITY_ATTACHMENT_PARENT_MODEL, ENTITY_KEY_FUNC)

from gmovies import models, orgs, tx
from gmovies.admin import notify_admin
from gmovies.exceptions.api import *
from gmovies.settings import API_VERSIONS_DEPRECATED, ENFORCE_AUTH
from gmovies.registration_whitelist import REGISTRATION_ALLOWED_THEATER_CODES
from gmovies.util import admin
from gmovies.util.id_encoder import decode_uuid, encode_uuid, encoded_theater_id, decoded_theater_id
from gmovies.feeds import sureseats
from gmovies.feeds.rockwell import read_movies_nowshowing_powerplantmall, read_movies_comingsoon_powerplantmall
from gmovies.feeds.available_seats import get_available_seats
from gmovies.ws.connector import WSAuth, WSUser
from gmovies.ws.endpoints import LCT_SCHEDULES_API, LCT_SESSION_API, LCT_TRANSACTION_API
from gmovies.tx.csrc_payment_initiated import bind_listeners


log = logging.getLogger(__name__)
api_ver4 = Blueprint('api_ver4', __name__, template_folder='api_templates')

DEVICE_ID = 'X-GMovies-DeviceId'
AUTH_CLIENT_ID = 'X-GMovies-Auth-ClientId'
AUTH_TIMESTAMP = 'X-GMovies-Auth-Timestamp'
AUTH_SIGNATURE = 'X-GMovies-Auth-Signature'

API_VERSION = 'API4'
MGI_THEATER_CODES = ['LCT']


def abort(status_code, message='Error', error_code='ERROR_UNKNOWN', details=None, headers={}):
    raise APIException(status_code, message, error_code, details)

@transactional
def _start_tx(device_id, tx_info):
    transaction = tx.start(device_id, tx_info)
    tx.reschedule(transaction)

    return transaction

@transactional
def _start_tx_stopgap(device_id, tx_info):
    transaction = tx.start_stopgap(device_id, tx_info)
    tx.reschedule(transaction)

    return transaction

def get_theater_info_common(org_uuid, enc_org_uuid, theater_id, is_minimum_info=False):
    theater = Key(models.TheaterOrganization, org_uuid, models.Theater, theater_id).get()

    if not theater:
        raise NotFoundException(details={'theater_id': "%s~%s" % (enc_org_uuid, theater_id)})

    if is_minimum_info:
        return jsonify({'id': theater_id, 'theater': theater.to_minimum_entity()})

    return jsonify({'id': theater_id, 'theater': theater.to_entity()})

def query_model(cls, entity_name, request, id_encoder=lambda x:x.id(), is_minimum_info=False):
    check_filter_and_sort_args(cls, request.args)
    f, q = query_filters(cls, request)

    if cls == models.Theater:
        if is_minimum_info:
            l = [{'id': id_encoder(x.key), entity_name: x.to_minimum_entity()} for x in q]
        else:
            l = [{'id': id_encoder(x.key), entity_name: x.to_entity()} for x in q]
    elif cls == models.Movie:
        posters_args = {}
        posters_args['resolution'] = request.args.get('resolution', 'uploaded')
        posters_args['application_type'] = request.args.get('application_type', 'mobile-app')
        portrait_default_poster, landscape_default_poster = get_default_poster_url(posters_args=posters_args)

        # temporary, movies_ids, total_reviews, and averating_rating will remove in the future.
        movie_ids = q.fetch(keys_only=True)
        total_reviews = get_total_reviews(movie_ids=movie_ids)
        average_rating = get_average_rating(movie_ids=movie_ids)

        l = [{'id': id_encoder(x.key), entity_name: x.to_entity(total_reviews=total_reviews,
                average_rating=average_rating, posters_args=posters_args, portrait_default_poster=portrait_default_poster,
                landscape_default_poster=landscape_default_poster)} for x in q]
    else:
        l = [{'id': id_encoder(x.key), entity_name: x.to_entity()} for x in q]

    return (f, l)

def is_purchase_allowed(tx_info):
    encoded_theater_id = tx_info['reservations'][0]['theater']
    parent_org, theater_id = decoded_theater_id(encoded_theater_id)
    theater_key = Key(models.Theater, theater_id, parent=Key(models.TheaterOrganization, str(parent_org)))
    theater = theater_key.get()

    return theater.allow_reservation


###############
#
# Error and Before Request Handlers
#
###############

@api_ver4.errorhandler(AlreadyCancelledException)
def handle_already_cancelled_error(e):
    res = jsonify(message=e.message, error_code=e.error_code, details=e.details)
    res.status_code = e.code

    for m in e.details:
        res.headers.add('Allow', m)

    return res

@api_ver4.errorhandler(APIException)
def handle_api_error(e):
    res = jsonify(message=e.message, error_code=e.error_code, details=e.details)
    res.status_code = e.code

    return res

@api_ver4.errorhandler(OverQuotaError)
def handle_quota_error(e):
    res = jsonify(message='The backend has reached a resource quota. Please try again later.',
            error_code='SYSTEM_OVER_QUOTA', details=e.message)
    res.status_code = 500
    notify_admin("API Version 4", e, sys.exc_info())

    return res

@api_ver4.errorhandler(Exception)
def handle_system_error(e):
    log.info("e: {} {}".format(e, type(e)))
    log.info("sys.exc_info(): {} {}".format(sys.exc_info(), sys.exc_info()))

    res = jsonify(message='There has been a system error. Please try again later.',
            error_code='SYSTEM_GENERIC_ERROR', details=e.message)
    res.status_code = 500

    notify_admin("API Version 4", e, sys.exc_info())

    log.exception(e)

    return res

@api_ver4.before_request
def check_client_auth():
    log.info('Entered CHECK CLIENT AUTH in API_VER4')
    log.debug('Entered check_client_auth()')

    if (AUTH_CLIENT_ID in request.headers and
            AUTH_TIMESTAMP in request.headers and
            AUTH_SIGNATURE in request.headers):
        client_id = request.headers[AUTH_CLIENT_ID]
        auth_ts = request.headers[AUTH_TIMESTAMP]
        client = ndb.Key(models.Client, client_id).get()

        log.info("CLIENT: {}".format(client_id))

        if client:
            web_authenticate_request(client.pubkey, auth_ts,
                    request.headers[AUTH_SIGNATURE])
        else:
            raise ClientAuthException('Client failed auth check')
    else:
        log.warn('No auth headers')

        if ENFORCE_AUTH:
            abort(401, error_code='AUTH_REQUIRED', message='Client authentication required')

@api_ver4.before_request
def check_device_id():
    if DEVICE_ID not in request.headers:
        raise DeviceIdRequiredException()

@api_ver4.before_request
def check_api_version():
    log.debug("check_api_version, api version4...")

    if API_VERSION in API_VERSIONS_DEPRECATED:
        raise abort(401, error_code='API_VERSION_DEPRECATED', message='Client API version deprecated.')

@api_ver4.before_request
def log_caller_info():
    if 'User-Agent' in request.headers:
        ua = request.headers['User-Agent']
    else:
        ua = '(None)'

    if AUTH_CLIENT_ID in request.headers:
        client_id = request.headers[AUTH_CLIENT_ID]
    else:
        client_id = "(Not given)"

    device_id = request.headers[DEVICE_ID]

    log.info("API:\tUser-Agent: %s", ua)
    log.info("API:\tClient ID: %s\n\tDevice ID: %s" % (client_id, device_id))


###############
#
# API Meta
#
###############

@api_ver4.route('/')
def api_meta():
    return jsonify({'version': '1.21.0', 'api-version': '4.0'})


###############
#
# Theater Organizations Endpoints
#
###############

@api_ver4.route('/theaterorgs/')
def get_theater_orgs():
    check_filter_and_sort_args(models.TheaterOrganization, request.args)
    queryset = models.TheaterOrganization.query()
    filters, queryset = do_filter(models.TheaterOrganization, queryset, request.args)
    theaterorg_list = [{'id': org.key.id(), 'theater_organization': org_with_theaters(org)} for org in queryset]

    return jsonify({'filters': filters, 'results': theaterorg_list})
    
@api_ver4.route('/theaterorgs/<theaterorg_id>/')
def get_theaterorg_info(theaterorg_id):
    org_key = Key(models.TheaterOrganization, theaterorg_id)
    org = org_key.get()

    if not org:
        raise NotFoundException(details={'theaterorg_id': theaterorg_id})

    return jsonify({'id': theaterorg_id, 'theater_organization': org_with_theaters(org)})

@api_ver4.route('/theaterorgs/<theaterorg_id>/movies/')
def get_theaterorg_movies(theaterorg_id):
    check_filter_and_sort_args(models.Movie, request.args)

    ids = []
    movie_list = []
    feed_key = Key(models.Feed, str(theaterorg_id))
    feed = feed_key.get()

    if not feed:
        raise NotFoundException(details={'theaterorg_id': theaterorg_id})

    posters_args = {}
    posters_args['resolution'] = request.args.get('resolution', 'uploaded')
    posters_args['application_type'] = request.args.get('application_type', 'mobile-app')

    id_encoder = lambda x:x.id()
    queryset = models.Movie.query()
    filters, queryset = query_filters(models.Movie, request)
    is_nowshowing, is_comingsoon = get_movie_status(filters)
    portrait_default_poster, landscape_default_poster = get_default_poster_url(posters_args=posters_args)

    if is_nowshowing:
        if str(feed_key.id()) == str(orgs.AYALA_MALLS):
            movie_list = sureseats.read_movies_now_showing()
        elif str(feed_key.id()) == str(orgs.ROCKWELL_MALLS):
            movie_list = read_movies_nowshowing_powerplantmall()
    elif is_comingsoon:
        if str(feed_key.id()) == str(orgs.AYALA_MALLS):
            movie_list = sureseats.read_movies_coming_soon()
        elif str(feed_key.id()) == str(orgs.ROCKWELL_MALLS):
            movie_list = read_movies_comingsoon_powerplantmall()

    ids = [str(movie_dict['id']) for movie_dict in movie_list]
    queryset = queryset.filter(models.Movie.movie_correlation.org_key == feed_key)

    if is_nowshowing or is_comingsoon:
        if ids:
            queryset = queryset.filter(models.Movie.movie_correlation.movie_id.IN(ids))
        else:
            movie_entities = []

            return jsonify({'filters': filters, 'results': movie_entities})

    movie_entities = [{'id': id_encoder(x.key),
            'movie': x.to_entity(posters_args=posters_args,
                    portrait_default_poster=portrait_default_poster,
                    landscape_default_poster=landscape_default_poster)} for x in queryset]

    return jsonify({'filters': filters, 'results': movie_entities})

@api_ver4.route('/theaterorgs/<theaterorg_id>/movies/coming_soon/')
def get_theaterorg_movies_comingsoon(theaterorg_id):
    ids = []
    movie_list = []
    id_encoder = lambda x:x.id()
    feed_key = Key(models.Feed, str(theaterorg_id))
    feed = feed_key.get()

    if not feed:
        raise NotFoundException(details={'theaterorg_id': theaterorg_id})

    queryset = models.Movie.query(models.Movie.is_showing == False,
            models.Movie.is_expired == False, models.Movie.is_inactive == False,
            models.Movie.is_published == True).order(models.Movie.release_date)

    if str(feed_key.id()) == str(orgs.AYALA_MALLS):
        movie_list = sureseats.read_movies_coming_soon()
    elif str(feed_key.id()) == str(orgs.ROCKWELL_MALLS):
        movie_list = read_movies_comingsoon_powerplantmall()

    ids = [str(movie_dict['id']) for movie_dict in movie_list]
    queryset = queryset.filter(models.Movie.movie_correlation.org_key == feed_key)

    if ids:
        queryset = queryset.filter(models.Movie.movie_correlation.movie_id.IN(ids))
    else:
        coming_soon_movies = []

        return jsonify({'results': coming_soon_movies})

    coming_soon_movies = do_parse_comingsoon(queryset, id_encoder=id_encoder)

    return jsonify({'results': coming_soon_movies})

@api_ver4.route('/theaterorgs/<theaterorg_id>/theaters/')
def get_theaterorg_theaters(theaterorg_id):
    org_key = Key(models.TheaterOrganization, theaterorg_id)
    org = org_key.get()

    if not org:
        raise NotFoundException(details={'theaterorg_id': theaterorg_id})

    if (('location.lon' in request.args and 'location.lat' in request.args) or
            ('cinemas.location.lon' in request.args and 'cinemas.location.lat' in request.args)):
        lat, lon, theater_keys = do_location_search(request)
        org_theater_keys = models.Theater.query(ancestor=org_key).fetch(keys_only=True)
        matched_keys = set(theater_keys).intersection(set(org_theater_keys))
        theater_list = do_get_distance(lat, lon, matched_keys, request)
        filters = {'match': {'location.lat': lat, 'location.lon': lon}}
    else:
        check_filter_and_sort_args(models.Theater, request.args)
        q = models.Theater.query(ancestor=org_key)
        filters, q = do_filter(models.Theater, q, request.args)
        theater_list = [{'id': encoded_theater_id(x.key), 'theater': x.to_entity()} for x in q]
                         
    return jsonify({'filters': filters, 'results': theater_list})

@api_ver4.route('/theaterorgs/<theaterorg_id>/theaters/<uncoded_id>/')
def dummy_theaterorg_theater_info(theaterorg_id, uncoded_id):
    raise NotFoundException(details={'theaterorg_id': theaterorg_id, 'theater_id': uncoded_id})

@api_ver4.route('/theaterorgs/<theaterorg_id>/theaters/<ignored>~<int:theater_id>/')
def get_theaterorg_theater_info(theaterorg_id, ignored, theater_id):
    theaterorg_theater_safety_check(theaterorg_id, ignored, theater_id)

    return get_theater_info_common(theaterorg_id, ignored, theater_id)


###############
#
# Theaters Endpoints
#
###############

@api_ver4.route('/theaters/')
def get_theaters():
    if (('location.lon' in request.args and  'location.lat' in request.args) or
            ('cinemas.location.lon' in request.args  and 'cinemas.location.lat' in request.args)):
        lat, lon, theater_keys = do_location_search(request)
        theater_list = do_get_distance(lat, lon, theater_keys, request)
        filters = {'match': {'location.lat': lat, 'location.lon': lon}}
    else:
        filters, theater_list = query_model(models.Theater, 'theater',
                request, id_encoder=encoded_theater_id)

    return jsonify({'filters': filters, 'results': theater_list})

@api_ver4.route('/theaters/<uncoded_id>/')
def dummy_theater_info(uncoded_id):
    raise NotFoundException(details={'theater_id': uncoded_id})

@api_ver4.route('/theaters/<enc_org_uuid>~<int:theater_id>/')
def get_theater_info(enc_org_uuid, theater_id):
    org_uuid = decode_uuid(enc_org_uuid)

    return get_theater_info_common(str(org_uuid), enc_org_uuid, theater_id)


###############
#
# Movies Endpoints
#
###############

@api_ver4.route('/movies/')
def get_movies():
    filters, movie_list = query_model(models.Movie, 'movie', request)

    return jsonify({ 'filters': filters, 'results': movie_list })

@api_ver4.route('/movies/<movie_id>/')
def get_movie(movie_id):
    posters_args = {}
    posters_args['resolution'] = request.args.get('resolution', 'uploaded')
    posters_args['application_type'] = request.args.get('application_type', 'mobile-app')

    portrait_default_poster, landscape_default_poster = get_default_poster_url(posters_args=posters_args)
    movie = Key(models.Movie, movie_id).get()

    if not movie:
        raise NotFoundException(details={'movie_id': movie_id})

    return jsonify({'id': movie_id,
            'movie': movie.to_entity_movie_info(posters_args=posters_args,
                    portrait_default_poster=portrait_default_poster,
                    landscape_default_poster=landscape_default_poster)})

@api_ver4.route('/movies/<movie_id>/poster/')
def get_movie_poster(movie_id):
    posters_args = {}
    posters_args['resolution'] = request.args.get('resolution', 'uploaded')
    posters_args['application_type'] = request.args.get('application_type', 'mobile-app')

    orientation = request.args.get('orientation', 'landscape')
    movie_key = Key(models.Movie, movie_id)
    movie = movie_key.get()

    if not movie:
        raise NotFoundException(details={'movie_id': movie_id})

    portrait_default_poster, landscape_default_poster = get_default_poster_url(posters_args=posters_args)
    portrait_poster, landscape_poster = movie.get_poster_url(posters_args=posters_args)

    if orientation == 'landscape':
        if landscape_poster:
            poster = landscape_poster
        else:
            poster = landscape_default_poster
    else:
        if portrait_poster:
            poster = portrait_poster
        else:
            poster = portrait_default_poster

    if not poster:
        raise NotFoundException(details={'movie_id': movie_id,
                'orientation': orientation, 'resolution': posters_args['resolution'],
                'application_type': posters_args['application_type']})

    return redirect(poster)

@api_ver4.route('/movies/coming_soon/')
def get_movies_comingsoon():
    id_encoder = lambda x:x.id()
    queryset = models.Movie.query(models.Movie.is_showing == False,
            models.Movie.is_expired == False, models.Movie.is_inactive == False,
            models.Movie.is_published == True).order(models.Movie.release_date)
    coming_soon_movies = do_parse_comingsoon(queryset, id_encoder=id_encoder)

    return jsonify({'results': coming_soon_movies})

@api_ver4.route('/movies/now_showing_coming_soon/', methods=['GET'])
def get_movies_with_coming_soon():
    id_encoder = lambda x: x.id()
    today = date.today()

    posters_args = {}
    posters_args['resolution'] = request.args.get('resolution', 'uploaded')
    posters_args['application_type'] = request.args.get('application_type', 'mobile-app')
    portrait_default_poster, landscape_default_poster = get_default_poster_url(posters_args=posters_args)

    now_showing_query = models.Movie.query().filter(models.Movie.is_showing==True,
            models.Movie.is_expired==False, models.Movie.is_inactive==False).fetch()
    coming_soon_query = models.Movie.query().filter(models.Movie.release_date>today,
            models.Movie.is_expired==False, models.Movie.is_inactive==False).fetch()

    now_showing = [{'id': id_encoder(x.key),
            'movie': x.to_entity(posters_args=posters_args,
                    portrait_default_poster=portrait_default_poster,
                    landscape_default_poster=landscape_default_poster),
            'status': 'now_showing'} for x in now_showing_query]
    coming_soon = [{'id': id_encoder(x.key),
            'movie': x.to_entity(posters_args=posters_args,
                    portrait_default_poster=portrait_default_poster,
                    landscape_default_poster=landscape_default_poster),
            'status': 'coming_soon'} for x in coming_soon_query]

    movies = now_showing + coming_soon

    return jsonify({'results': movies})

@api_ver4.route('/movies/in_active/', methods=['GET'])
def get_in_active_movies():
    id_encoder = lambda x: x.id()
    query = models.Movie.query().filter(models.Movie.is_inactive==True).fetch()
    movies = [{'id': id_encoder(x.key), 'movie': x.to_entity()} for x in query]

    return jsonify({'results': movies})

@api_ver4.route('/movie_correlation/', methods=['GET'])
def get_movie_correlation():
    results = {}
    feed_id = request.args.get('feed_id', None)
    correlation_id = request.args.get('correlation_id', None)

    if feed_id is None:
        raise NotFoundException(details={'feed_id': feed_id})

    if correlation_id is None:
        raise NotFoundException(details={'correlation_id': correlation_id})

    feed_key = ndb.Key(models.Feed, feed_id)
    feed = feed_key.get()

    if not feed:
        raise NotFoundException(details={'feed_id': feed_id})

    if feed_id == str(orgs.ROCKWELL_MALLS):
        correlation_id = 'RW::%s' % correlation_id
    elif feed_id == str(orgs.GREENHILLS_MALLS):
        correlation_id = 'GH::%s' % correlation_id
    elif feed_id == str(orgs.SM_MALLS):
        correlation_id = 'SM::%s' % correlation_id
    elif feed_id == str(orgs.MEGAWORLD_MALLS):
        correlation_id = 'MGW::%s' % correlation_id

    movies = models.Movie.query(models.Movie.movie_correlation.movie_id==correlation_id, models.Movie.movie_correlation.org_key==feed_key).fetch()

    if len(movies) != 1:
        raise NotFoundException(details={'feed_id': feed_id, 'correlation_id': correlation_id})

    results['movie_id'] = movies[0].key.id()
    results['movie_title'] = movies[0].canonical_title

    return jsonify(results)


###############
#
# Schedules and Available Seats Endpoints
#
###############

@api_ver4.route('/schedules/')
def get_schedules():
    return do_get_schedules()

@api_ver4.route('/theaterorgs/<theaterorg_id>/theaters/<ignored>~<int:theater_id>/schedules/')
def get_theater_org_theater_schedules(theaterorg_id, ignored, theater_id):
    theaterorg_theater_safety_check(theaterorg_id, ignored, theater_id)

    return get_theater_schedules(ignored, theater_id)

@api_ver4.route('/theaters/<enc_org_uuid>~<int:theater_id>/schedules/')
def get_theater_schedules(enc_org_uuid, theater_id):
    log.debug("Got theater ID pair %s %s" % (enc_org_uuid, theater_id))

    return do_get_schedules(theater_pair=(enc_org_uuid,theater_id))

@api_ver4.route('/movies/<movie_id>/schedules/')
def get_movie_schedules(movie_id):
    return do_get_schedules(movie_id=movie_id)

# Requested endpoint for website.
# No way to determine if now_showing and coming_soon.
@api_ver4.route('/movies/<movie_id>/<theaterorg_id>/schedules/')
def get_movie_schedules_per_theater_org(movie_id, theaterorg_id):
    return do_get_schedules(movie_id=movie_id, theaterorg_id=theaterorg_id)

@api_ver4.route('/available_seats')
def translate_and_get_available_seats():
    if 'theater' not in request.args:
        raise BadValueException('theater', 'You must specify a theater')
    if 'schedule' not in request.args:
        raise BadValueException('schedule', 'You must specify a schedule')
    if 'time' not in request.args:
        raise BadValueException('time', 'You must specify a time')

    available = []
    remaining_seats = 0
    error_message = 'Unable to load the seats due to an unexpected error. Please try again later.'

    enc_theater_id = request.args['theater']
    sched_id_raw = request.args['schedule']
    sched_id = sched_id_raw.split(',')[0]
    sched_time = datetime.strptime(request.args['time'], models.TIME_FORMAT).time()
    theater_key = to_theater_key(enc_theater_id)
    theater = theater_key.get()

    if not theater:
        raise NotFoundException(details={'theater': theater_id})

    theater_code = theater.org_theater_code

    log.info("theater_name: %s" % theater.name)
    log.info("theater_code: %s, allow_reservation: %s" % (theater_code, theater.allow_reservation))

    if 'platform' in request.args:
        platform = request.args['platform'].lower()

        if platform == 'android' and theater.error_message_seatmaps_android:
            error_message = theater.error_message_seatmaps_android
        elif platform == 'ios' and theater.error_message_seatmaps_ios:
            error_message = theater.error_message_seatmaps_ios

    if not theater.allow_reservation:
        res = {'theater': enc_theater_id, 'schedule': sched_id,
                'available_seat_map': available, 'available_seats': remaining_seats,
                'error_message': error_message}

        return jsonify(res)

    schedule = Key(models.Schedule, sched_id, parent=theater.key).get()

    if not schedule:
        raise NotFoundException(details={'schedule': sched_id})

    sched_slot = None

    for sl in schedule.slots:
        if sl.start_time == sched_time:
            sched_slot = sl

            break

    if not sched_slot:
        raise NotFoundException(details={'time': request.args['time']})

    if schedule.show_date < date.today():
        raise BadValueException('schedule', 'Expired schedule.')

    sched_code = sched_slot.feed_code_schedule
    sched_slot_code = sched_slot.feed_code
    seating_type = sched_slot.seating_type

    log.info("Schedule feed_code: %s" % sched_code)
    log.info("translate_and_get_available_seats, sched_slot_code: %s..." % sched_slot_code)
    log.info("Schedule seating_type: %s" % seating_type)

    # Organization API lookup here; for now, hard-coded to use
    # Sureseats, Rockwell, Greenhills, and Megaworld - LCT API directly
    if str(theater_key.parent().id()) == str(orgs.AYALA_MALLS): # Get Available Seats for Sureseats
        log.info('Sureseats: get_available_seats...')

        status, available, remaining_seats = get_available_seats(theater_key.parent().id(), theater_code, sched_slot_code)
    elif str(theater_key.parent().id()) == str(orgs.ROCKWELL_MALLS): # Get Available Seats for Rockwell
        log.info('Rockwell: get_available_seats...')

        status, available, remaining_seats = get_available_seats(theater_key.parent().id(), sched_code, sched_slot_code)
    elif str(theater_key.parent().id()) == str(orgs.GREENHILLS_MALLS): # Get Available Seats for Greenhills
        log.info('Greenhills: get_available_seats...')

        schedslotcode_seatingtype = '%s::%s' % (sched_slot_code, seating_type)
        status, available, remaining_seats = get_available_seats(theater_key.parent().id(), sched_code, schedslotcode_seatingtype)
    elif str(theater_key.parent().id()) == str(orgs.SM_MALLS): # Get Available Seats for SM Malls
        log.info('SM Malls: get_available_seats...')

        status, available, remaining_seats = get_available_seats(theater_key.parent().id(), theater, sched_slot_code, arg4=True)
    elif str(theater_key.parent().id()) == str(orgs.MEGAWORLD_MALLS): # Get Available Seats for Megaworld
        log.info('Megaworld: get_available_seats...')

        if theater_code in MGI_THEATER_CODES:
            log.info('MGI: get_available_seats...')

            status, available, remaining_seats = get_available_seats(theater_key.parent().id(), theater, sched_slot_code, 'MGI')
        else:
            log.warn('ERROR!, missing third party available seats endpoint...')

            status = 'error'
    else:
        log.warn('ERROR!, missing third party available seats endpoint...')

        status = 'error'

    if status == 'success':
        error_message = ''

    available_seats = {'theater': enc_theater_id, 'schedule': sched_id,
            'available_seat_map': available, 'available_seats': remaining_seats,
            'error_message': error_message}

    return jsonify(available_seats)


###############
#
# Transactions Endpoints
#
###############

@api_ver4.route('/tx/', methods=['POST'])
def start_tx():
    log.info("Headers: {}".format(request.headers))

    device_id = request.headers[DEVICE_ID]

    try:
        log.info("request.json: {}".format(request.json))
        tx_info = request.json
    except Exception, e:
        log.warn("ERROR, malformed request.json...")
        log.error(e)
        log.info("request.data: {}".format(request.data))

        data = unicode(request.data, 'latin-1')
        tx_info = json.loads(data)

    log.info("tx_info: {}".format(tx_info))

    if not tx_info:
        log.warn("No tx_info...")

        raise BadValueException('body', 'You must supply transaction info')

    # VERY UGLY HACK: Restrict non-ipay88 payment options for ROCKWELL because of the inflexible iOS app
    payment_type = tx_info["payment"]["type"]
    encoded_theater_id = tx_info["reservations"][0]["theater"]
    (parent_org, theater_id) = decoded_theater_id(encoded_theater_id)
    log.info("Payment Type: {0} Theater Org: {1}".format(payment_type, str(parent_org)))

    if str(parent_org) == str(orgs.ROCKWELL_MALLS):
        log.info("Got Rockwell!!!")
        invalid_rockwell_payment_options = ["credit-card", "mpass", "client-initiated"]

        if payment_type in invalid_rockwell_payment_options:
            abort(400, error_code='PAYMENT_METHOD_NOT_ALLOWED', message='Invalid Payment Method.')

    if str(parent_org) == str(orgs.MEGAWORLD_MALLS):
        log.info("Got Megaworld!!!")
        theater_key = Key(models.TheaterOrganization, str(parent_org), models.Theater, theater_id)
        theater_code = theater_key.get().org_theater_code

        if theater_code =='LCT':
            invalid_lct_payment_options = ["credit-card", "mpass", "client-initiated", "promo-code"]

            if payment_type in invalid_lct_payment_options:
                abort(400, error_code='PAYMENT_METHOD_NOT_ALLOWED', message='Invalid Payment Method.')

            # check client id params
            if 'client_id' not in tx_info["payment"] or tx_info["payment"]["client_id"] is None:
                log.warn('Missing client id')
                raise BadValueException('client_id', 'Missing client id') 


    # restrict transactions for theaters with purchase-restricted status.
    # also serves as a filter for iOS apps with outdated purchased-enabled database 
    if not is_purchase_allowed(tx_info):
        log.info('Ticket purchase is not allowed, Aborting transaction')
        abort(400, error_code='RESERVATION_NOT_ALLOWED', message='Reservation for this cinema has been disabled.')
    else:
        log.info('Ticket purchase is allowed')
        transaction = _start_tx(device_id, tx_info)

        return jsonify({'id': transaction.key.id(), 'transaction': transaction.to_entity()})

@api_ver4.route('/tx/', methods=['GET'])
def get_device_txs():
    device_id = request.headers[DEVICE_ID]
    device_key = Key(models.Device, device_id)
    q = models.ReservationTransaction.query(ancestor=device_key)
    state_filter = None

    if 'states' in request.args:
        state_strs = request.args.getlist('states')
        states = filter(None, [getattr(tx.state, s) if hasattr(tx.state, s) else None for s in state_strs])
        log.debug("States to match: %s" % states)
        q = q.filter(models.ReservationTransaction.state.IN(states))
        state_filter = state_strs
    else:
        q = q.filter(models.ReservationTransaction.state != tx.state.TX_DONE)

    if 'sort' in request.args:
        sort_cols = [col if col[0] != '-' else col[1:] for col in request.args.getlist('sort')]
        check = filter(lambda tup: not tup[1], [(k, resolve_dotted_name(models.ReservationTransaction, k)) for k in sort_cols])

        if len(check) > 0:
            unknown_sort_args = [k for k,v in check]

            raise InvalidOrderPropertyException(unknown_sort_args)

        q = apply_sort(q, models.ReservationTransaction, request.args.getlist('sort'))

    txs = [k.id() for k in q.fetch(keys_only=True)]

    return jsonify({'states': state_filter, 'transactions': txs})

@api_ver4.route('/tx/<tx_id>/', methods=['GET'])
def get_tx_info(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = tx.query_info(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={'tx_id': tx_id})

    # (06/10/2014) Temporarily exclude barcode field from
    # response due to Android app not being able to handle 
    # long strings causing out of memory error
    trans = transaction.to_entity()
    trans['ticket']['barcode'] = ''
    trans['ticket']['qrcode'] = ''

    return jsonify({'id': tx_id, 'ticket_id': '', 'transaction': trans})

@api_ver4.route('/tx/<tx_id>/', methods=['PUT'])
def update_tx_info(tx_id):
    device_id = request.headers[DEVICE_ID]
    tx_info = request.json

    if tx_info == None or tx_info == {}:
        raise BadValueException('body', 'You must supply transaction info')

    transaction = tx.update(device_id, tx_id, tx_info)

    return jsonify({'id': tx_id, 'transaction': transaction.to_entity()})

@api_ver4.route('/tx/<tx_id>/', methods=['DELETE'])
def cancel_tx(tx_id):
    device_id = request.headers[DEVICE_ID]
    tx.cancel(device_id, tx_id)

    return jsonify({'id': tx_id, 'cancelled': True})

@api_ver4.route('/tx/<tx_id>/ticket_details/', methods=['GET'])
def get_tx_details(tx_id):
    device_id = request.headers[DEVICE_ID]
    state, __ = tx.query_state(device_id, tx_id)

    # transaction has no state yet
    if (state, __) == (None, None):
        raise NotFoundException(details={'tx_id': tx_id})

    # transaction is not done
    if state != tx.state.TX_DONE:
        raise NotFoundException(details={'tx_id': tx_id})

    q = tx.query_info(device_id, tx_id)
    transaction_detail = q.to_entity()

    ticket = {}
    ticket['title'] =  transaction_detail['ticket']['extra']['movie_canonical_title']
    ticket['theater_and_cinema_name'] = transaction_detail['ticket']['extra']['theater_and_cinema_name'] 
    ticket['reference_code'] = transaction_detail['ticket']['code']
    ticket['ticket_count'] = transaction_detail['ticket']['extra']['ticket_count']
    ticket['price'] = transaction_detail['ticket']['extra']['per_ticket_price']
    ticket['reservation_fee'] = transaction_detail['ticket']['extra']['reservation_fee']
    ticket['total_amount'] = transaction_detail['ticket']['extra']['total_amount']
    ticket['show_datetime'] = transaction_detail['ticket']['extra']['show_datetime']
    ticket['seats'] = transaction_detail['ticket']['extra']['seats']
    ticket['barcode'] = transaction_detail['ticket']['barcode']

    return jsonify(ticket)

@api_ver4.route('/tx/<tx_id>/state/', methods=['GET'])
def get_tx_state(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = tx.query_info(device_id, tx_id)
    state, msg = tx.query_state(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={'tx_id': tx_id})

    if (state, msg) == (None, None):
        raise NotFoundException(details={'tx_id': tx_id})

    tx_state = tx.to_state_str(state)

    if msg:
        log.info('State: {0} Message: {1}'.format(tx_state, msg))
        return jsonify({'state': tx_state, 'message': msg})
    else:
        log.info('State: {}'.format(tx_state))

        if transaction.payment_type == 'globe-promo':
            return jsonify({'state': tx_state, 'message': ''})

        return jsonify({'state': tx_state})

@api_ver4.route('/tx/<tx_id>/state/', methods=['PUT'])
def force_tx_state(tx_id):
    abort(501)

@api_ver4.route('/tx/<tx_id>/client-initiate/', methods=['GET'])
def client_initiate_payment(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = tx.query_info(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={'tx_id': tx_id})

    state, msg = tx.query_state(device_id, tx_id)

    if state != tx.state.TX_CLIENT_PAYMENT_HOLD:
        raise TransactionConflictException([tx.state.TX_CLIENT_PAYMENT_HOLD])

    log.info('Transaction Info: {}'.format(transaction))

    # FIXME: Leaked constant
    payment_info = transaction.workspace['payment::engine']
    reference = transaction.reservation_reference
    org_id = transaction.workspace['theaters.org_id'][0]

    log.info('Payment Info: {}'.format(payment_info))
    log.info('Reservation Reference: {}'.format(reference))

    # FIXME: Hard-coded reference
    if org_id == str(orgs.AYALA_MALLS):
        theater_code = transaction.workspace['theaters.org_theater_code'][0]
        reference = "%s-%s" % (theater_code, reference)

    # Override amount specified
    payment_info.form_parameters['amount'] = transaction.workspace['RSVP:totalamount']

    # FIXME: Hard-coded hash key and calculation
    payment_info.form_parameters['secureHash'] = payment_info.generate_hash(reference)

    log.info('Overriden Payment Info: {}'.format(payment_info.form_parameters))

    return render_template('client_initiated_payment.html', tx=transaction,
            reference=reference, payment_info=payment_info)

@api_ver4.route('/tx/<tx_id>/paynamics-initiate/', methods=['GET'])
def paynamics_initiate_payment(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = tx.query_info(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={'tx_id': tx_id})

    state, msg = tx.query_state(device_id, tx_id)

    if state != tx.state.TX_CLIENT_PAYMENT_HOLD:
        raise TransactionConflictException([tx.state.TX_CLIENT_PAYMENT_HOLD])

    client_ip = request.remote_addr
    payment_info = transaction.workspace['payment::engine']
    reservation_reference = transaction.reservation_reference

    payment_info.form_parameters['amount'] = transaction.workspace['RSVP:totalamount']
    payment_info.form_parameters['request_id'] = reservation_reference.split('~')[-1]
    payment_info.form_parameters['country'] = 'PH'
    payment_info.form_parameters['orders'] = payment_info.generate_orders_payload(transaction)
    payment_info.form_parameters['client_ip'] = client_ip
    payment_info.form_parameters['signature'] = payment_info.generate_signature()
    paymentrequest = payment_info.generate_paymentrequest()

    try:
        log.info("Paynamics parameter, client_ip: %s", client_ip)
        log.info("form_parameters: {}".format(payment_info.form_parameters))
        log.info("signature: {}".format(payment_info.form_parameters['signature']))
        log.info("paymentrequest: {}".format(paymentrequest))
    except Exception, e:
        log.error(e)
        log.warn("Failed to logs the paynamics-initiate details...")

    return render_template('paynamics_initiated_payment.html',
            payment_info=payment_info, paymentrequest=paymentrequest)

@api_ver4.route('/tx/<tx_id>/ipay88-payment-initiate', methods=['GET'])
def ipay88_initiate_payment(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = tx.query_info(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={'tx_id': tx_id})

    state, msg = tx.query_state(device_id, tx_id)

    if state != tx.state.TX_EXTERNAL_PAYMENT_HOLD:
        raise TransactionConflictException([tx.state.TX_EXTERNAL_PAYMENT_HOLD])

    payment_info = transaction.workspace['payment::engine']
    reservation_reference = transaction.reservation_reference
    signature = payment_info.generate_hash(reservation_reference, LCT_MERCHANT_KEY)
    payment_info.form_parameters["RefNo"] = reservation_reference
    payment_info.form_parameters["Signature"] = signature

    log.debug("form_parameters: {}".format(payment_info.form_parameters))
    log.debug("signature: {}".format(signature))

    return jsonify({"form_method": payment_info.form_method, "form_target":payment_info.form_target,
            "payment_info": payment_info.form_parameters})

@api_ver4.route('/tx/<tx_id>/tickets/', methods=['GET'])
def get_tx_tickets(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = tx.query_info(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={'tx_id': tx_id})

    queryset = models.TicketImageBlob.query(ancestor=transaction.key)
    queryset = apply_sort(queryset, models.TicketImageBlob, request.args.getlist('sort'))
    tx_ticket_ids = [ticket.id() for ticket in queryset.fetch(keys_only=True)]

    return jsonify({'id': tx_id, 'transactions': tx_ticket_ids})

@api_ver4.route('/tx/<tx_id>/tickets/<ticket_id>/', methods=['GET'])
def get_ticket_info(tx_id, ticket_id):
    device_id = request.headers[DEVICE_ID]
    transaction = tx.query_info(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={'tx_id': tx_id})

    tx_ticket_image = Key(models.TicketImageBlob, ticket_id, parent=transaction.key).get()
    ticket_image = tx_ticket_image.to_entity(transaction)

    return jsonify({'id': tx_id, 'ticket_id': ticket_id,
            'transaction': ticket_image})

@api_ver4.route('/tx/<tx_id>/min/', methods=['GET'])
def get_minimum_ticket_info(tx_id):
    device_id = request.headers[DEVICE_ID]
    transaction = tx.query_info(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={'tx_id': tx_id})

    trans = transaction.to_minimum_entity()

    return jsonify({'id' : tx_id, 'transaction': trans})

@api_ver4.route('/tx/<tx_id>/device/<device_id>/', methods=['GET'])
def get_tx_info_by_device_id(tx_id, device_id):
    transaction = tx.query_info(device_id, tx_id)

    if not transaction:
        raise NotFoundException(details={'tx_id': tx_id})

    return jsonify({'id' : tx_id, 'transaction': transaction.to_entity()})

@api_ver4.route('/tx/<tx_id>/device/<device_id>/ticket_details/', methods=['GET'])
def get_tx_details_by_device_id(tx_id, device_id):
    state, __ = tx.query_state(device_id, tx_id)

    # transaction has no state yet
    if (state, __) == (None, None):
        raise NotFoundException(details={'tx_id': tx_id})

    # transaction is not done
    if state != tx.state.TX_DONE:
        raise NotFoundException(details={'tx_id': tx_id})

    q = tx.query_info(device_id, tx_id)
    transaction_detail = q.to_entity()

    ticket = {}
    ticket['title'] =  transaction_detail['ticket']['extra']['movie_canonical_title']
    ticket['theater_and_cinema_name'] = transaction_detail['ticket']['extra']['theater_and_cinema_name']
    ticket['reference_code'] = transaction_detail['ticket']['code']
    ticket['ticket_count'] = transaction_detail['ticket']['extra']['ticket_count']
    ticket['price'] = transaction_detail['ticket']['extra']['per_ticket_price']
    ticket['reservation_fee'] = transaction_detail['ticket']['extra']['reservation_fee']
    ticket['total_amount'] = transaction_detail['ticket']['extra']['total_amount']
    ticket['show_datetime'] = transaction_detail['ticket']['extra']['show_datetime']
    ticket['seats'] = transaction_detail['ticket']['extra']['seats']
    ticket['barcode'] = transaction_detail['ticket']['barcode']

    return jsonify(ticket)

@api_ver4.route('/reports/tx/', methods=['GET'])
def get_txs_reports():
    results = []
    filters = {}
    cursor = ndb.Cursor(urlsafe=request.args.get('cursor'))
    query = models.ReservationTransaction.query(models.ReservationTransaction.state == tx.state.TX_DONE)

    if 'transaction_date' in request.args:
        filters['transaction_date'] = request.args.get('transaction_date')
        tx_date_from_str = request.args.get('transaction_date')
        tx_date_from = datetime.strptime(tx_date_from_str + ' 00:00:00', '%d %b %Y %H:%M:%S')
        tx_date_from = admin.convert_timezone(tx_date_from, 8, '-') # convert to UTC.
        tx_date_to_str = date.today().strftime('%d %b %Y')
        tx_date_to = datetime.strptime(tx_date_to_str + ' 23:59:59', '%d %b %Y %H:%M:%S')
        log.info("get_txs_reports, date_created range, >= tx_date_from %s, <= tx_date_to %s" % (tx_date_from, tx_date_to))
        query = query.filter(models.ReservationTransaction.date_created >= tx_date_from,
                models.ReservationTransaction.date_created <= tx_date_to)

    if 'cursor' in request.args:
        filters['cursor'] = request.args.get('cursor')

    query = query.order(-models.ReservationTransaction.date_created)
    transactions, next_cursor, more = query.fetch_page(100, start_cursor=cursor)
    next_cursor = next_cursor.urlsafe() if next_cursor else ''

    for transaction in transactions:
        tx_ent = transaction.to_entity_reports()
        tx_ent['merchant_name'] = admin.THEATER_ORGANIZATIONS[tx_ent['merchant_id']] if tx_ent['merchant_id'] in admin.THEATER_ORGANIZATIONS else ''

        results.append(tx_ent)

    return jsonify({'filters': filters, 'results': results, 'more': more, 'next_cursor': next_cursor})

@api_ver4.route('/reports/daily/tx/', methods=['GET'])
def get_txs_reports_daily():
    results = []
    filters = {}
    cursor = ndb.Cursor(urlsafe=request.args.get('cursor'))
    query = models.ReservationTransaction.query(models.ReservationTransaction.state == tx.state.TX_DONE)

    if 'transaction_date' in request.args:
        filters['transaction_date'] = request.args.get('transaction_date')
        tx_date_from_str = request.args.get('transaction_date')
        tx_date_from = datetime.strptime(tx_date_from_str + ' 00:00:00', '%d %b %Y %H:%M:%S')
        tx_date_from = admin.convert_timezone(tx_date_from, 8, '-') # convert to UTC.
        tx_date_to_str = tx_date_from_str
        tx_date_to = datetime.strptime(tx_date_to_str + ' 23:59:59', '%d %b %Y %H:%M:%S')
        tx_date_to = admin.convert_timezone(tx_date_to, 8, '-') # convert to UTC.
        log.info("get_txs_reports_daily, date_created range, >= tx_date_from %s, <= tx_date_to %s" % (tx_date_from, tx_date_to))
        query = query.filter(models.ReservationTransaction.date_created >= tx_date_from,
                models.ReservationTransaction.date_created <= tx_date_to)

    if 'cursor' in request.args:
        filters['cursor'] = request.args.get('cursor')

    query = query.order(-models.ReservationTransaction.date_created)
    transactions, next_cursor, more = query.fetch_page(100, start_cursor=cursor)
    next_cursor = next_cursor.urlsafe() if next_cursor else ''

    for transaction in transactions:
        tx_ent = transaction.to_entity_reports()
        tx_ent['merchant_name'] = admin.THEATER_ORGANIZATIONS[tx_ent['merchant_id']] if tx_ent['merchant_id'] in admin.THEATER_ORGANIZATIONS else ''

        results.append(tx_ent)

    return jsonify({'filters': filters, 'results': results, 'more': more, 'next_cursor': next_cursor})

###############
#
# Device Transactions Endpoints
#
###############

@api_ver4.route('/device/completed_transactions/', methods=['GET'])
def completed_device_transactions():
    device_id = request.headers[DEVICE_ID]
    device_key = Key(models.Device, device_id)
    reservations = models.ReservationTransaction.query(
            models.ReservationTransaction.state==tx.state.TX_DONE,
            ancestor=device_key).order(-models.ReservationTransaction.date_created).fetch()

    log.info("Reservations: {}".format(len(reservations)))

    def get_movie_details(m):
        if m is None:
            log.warn("No movie found")
            return None

        movie_key = Key(models.Movie, m.id())

        if movie_key is None:
            log.warn("No movie key")
            return None

        movie =  movie_key.get()

        if movie is None:
            return None

        return {'id': m.id(), 'title':movie.canonical_title}

    def get_scheds_movie(r):
        parent_org  = r.reservations[0].theater.parent().id()
        theater_id  = r.reservations[0].theater.id()
        schedule_id = r.reservations[0].schedule.id()
        theater_key = Key(models.TheaterOrganization, parent_org, models.Theater, theater_id) 
        schedule =  Key(models.Schedule, schedule_id, parent=theater_key ).get()
    
        if schedule is None:
            log.warn("No schedule data for {}".format(schedule_id))
            return None

        slots = schedule.slots # Extract slots.
        movie_keys = map(lambda x: x.movie, slots) # Get movie keys from slots.
        movie_keys_list = list(set(movie_keys)) # Remove duplicate keys.
        movie_details = get_movie_details(movie_keys_list[0])

        log.info("Movie Detail: {}".format(movie_details))

        if movie_details is not None:
            return {'tx_id': r.key.id(), 'date_created': datetime.strftime(r.date_created, DATETIME_FORMAT),
                    'movie': movie_details}
        else:
            return None

    purchased_movies = filter(None, map(get_scheds_movie, reservations))

    log.info("Movie Transactions: {}".format(purchased_movies))

    return jsonify({'device_id': device_id, 'details': purchased_movies})

@api_ver4.route('/device/<device_id>/tx/<tx_id>/completed_transactions/', methods=['GET'])
def completed_devices_transactions(device_id, tx_id):
    device_key = Key(models.Device, device_id)
    reservations = [Key(models.ReservationTransaction, tx_id, parent=device_key).get()]

    if not reservations:
        raise NotFoundException(details={'tx_id': tx_id})

    log.info("Reservations: {}".format(len(reservations)))

    def get_movie_details(m):
        if m is None:
            log.warn("No movie found")
            return None

        movie_key = Key(models.Movie, m.id())

        if movie_key is None:
            log.warn("No movie key")
            return None

        movie =  movie_key.get()

        if movie is None:
            return None

        return {'id': m.id(), 'title':movie.canonical_title}

    def get_scheds_movie(r):
        parent_org  = r.reservations[0].theater.parent().id()
        theater_id  = r.reservations[0].theater.id()
        schedule_id = r.reservations[0].schedule.id()
        theater_key = Key(models.TheaterOrganization, parent_org, models.Theater, theater_id) 
        schedule =  Key(models.Schedule, schedule_id, parent=theater_key ).get()
    
        if schedule is None:
            log.warn("No schedule data for {}".format(schedule_id))
            return None

        slots = schedule.slots # Extract slots.
        movie_keys = map(lambda x: x.movie, slots) # Get movie keys from slots.
        movie_keys_list = list(set(movie_keys)) # Remove duplicate keys.
        movie_details = get_movie_details(movie_keys_list[0])

        log.info("Movie Detail: {}".format(movie_details))

        if movie_details is not None:
            return {'tx_id': r.key.id() ,'date_created': datetime.strftime(r.date_created, DATETIME_FORMAT),
                    'movie': movie_details}
        else:
            return None
                
    purchased_movies = filter(None, map(get_scheds_movie, reservations))

    log.info("Trans Movies {}".format(purchased_movies))

    return jsonify({'device_id': device_id, 'details': purchased_movies})


###############
#
# CSRC Payment Endpoints
#
###############

@api_ver4.route('/csrc-payment/', methods=['POST'])
def start_csrc_payment():
    # Extract payment details and start recording process.
    deposit_info = request.json
    log.info("Bank Deposit: {}".format(deposit_info))
    device_id = request.headers[DEVICE_ID]
    payment_proc = models.CSRCPayment.start_process(device_id, deposit_info)

    log.info("Payment Process: {}".format(payment_proc))

    return jsonify({"id": payment_proc.key.id()})

@api_ver4.route('/csrc-payment/<proc_id>/state/', methods=['GET'])
def get_csrc_payment_state(proc_id):
    device_id = request.headers[DEVICE_ID]
    device_key = Key(models.Device, device_id)
    proc = models.CSRCPayment.get_by_id(proc_id, parent=device_key)

    return jsonify({"id": proc_id, "state": models.CSRCPayment.stringify_state(proc.state)})

@api_ver4.route('/csrc-payment/<proc_id>/form-data/', methods=['GET'])
def show_bank_deposit_form(proc_id):
    device_id = request.headers[DEVICE_ID]
    device_key = Key(models.Device, device_id)
    csrc_instance = models.CSRCPayment.get_by_id(proc_id, parent=device_key)
    form_data = csrc_instance.get_form_data()

    log.info("Bank Deposit Data: {}".format(form_data))
    log.debug("STARTED, bind listeners...")

    bind_listeners(csrc_instance) # Bind a listener for this process. OR REFACTOR THIS!

    log.debug("DONE, bind listeners...")

    return jsonify({"id": proc_id, "form_parameters": form_data})


###############
#
# Services Endpoints
#
###############

@api_ver4.route('/services/', methods=['GET'])
def get_services():
    check_filter_and_sort_args(models.AnalyticsService, request.args)
    platform = request.args.get('platform', '')
    queryset = models.AnalyticsService.get_services()
    services_list = [service.to_entity(platform=platform) for service in queryset]

    return jsonify({'filters': {}, 'results': services_list})

@api_ver4.route('/services/<service_code>/', methods=['GET'])
def get_service_status(service_code):
    results = {}
    platform = request.args.get('platform', '')
    service = models.AnalyticsService.get_service_status(str(service_code))

    if service:
        results['id'] = service[0].key.id()
        results['name'] = service[0].service_name
        results['code'] = service[0].service_code
        results['value'] = service[0].value

        if service[0].active:
            results['status'] = 'active'
        else:
            results['status'] = 'inactive'

        if platform not in service[0].platform:
            results['status'] = 'inactive'

    return jsonify({'service': results})


###############
#
# Additional Endpoints
#
###############

# Entity Images (e.g. posters, logos, etc.)
# This endpoint is inactive, because MultiResImageBlob still using the deprecated BlobStore.
# Need to update to Google Cloud Storage to be reusable again.
@api_ver4.route('/<entity_class>/<entity_id>/image')
def get_entity_image(entity_class, entity_id):
    if entity_class not in ENTITY_ATTACHMENT_PARENT_MODEL:
        raise NotFoundException()

    cls = ENTITY_ATTACHMENT_PARENT_MODEL[entity_class]
    ent_key = ENTITY_KEY_FUNC[entity_class](entity_id)

    assert isinstance(ent_key, Key), ent_key

    q = models.MultiResImageBlob.query(ancestor=ent_key)
    res = 'mdpi'

    if 'device_resolution' in request.args:
        res = request.args['device_resolution'].lower()

    q = q.filter(models.MultiResImageBlob.resolution == res)

    if q.count() != 1:
        raise NotFoundException(details={entity_class: entity_id, 'attachment': 'image'})

    multi_image = q.fetch(1)[0]

    return redirect(get_serving_url(multi_image.image, size=0, crop=False))

# Entity Versions
# This endpoint returns the latest timestamp of an entity.
# This is use by the app to check if there are updated entities.
@api_ver4.route('/entity~<entity_name>/version/')
def get_entity_version(entity_name):
    if entity_name == 'theater':
        queryset = models.Theater.query().order(-models.Theater.datetime_updated)
        theater = queryset.get()

        if theater:
            if theater.datetime_updated:
                dt = theater.datetime_updated
                ts = int(time.mktime(dt.timetuple()))
                version = str(ts)

                return jsonify({'version': version})

    return jsonify({'version': "0"})


###############
#
# MGi Integrated Cinemas Endpoints
# Megaworld - Lucky Chinatown
#
###############

# User registration for MGi Integrated Cinemas.
@api_ver4.route('/member/register/<theater_id>/', methods=['POST'])
def register_user(theater_id):
    """
    Headers:
    :: DEVICE_ID

    REST form_parameters
    :: theater_id

    Payload
    e.g.
    {"register": {
                  "first_name": "First Name",
                  "middle_name": "",
                  "last_name": "Last Name",
                  "address": "",
                  "city": "",
                  "bdate": "January 01, 2013",
                  "mobile_num": "09XXXXXXXXX",
                  "phone_num": "",
                  "email": "gmovies@localhost.com",
                  "password": "password"
                 }
    }
    """

    device_id = request.headers[DEVICE_ID]
    log.info("Device ID: {}".format(device_id))

    user_info = request.json
    log.info("USER INFO: {}".format(user_info))

    if user_info == None or 'register' not in user_info:
        log.warn("No USER_INFO")
        raise BadValueException('body', 'You must supply user info')

    org_id, _theater_id = decoded_theater_id(theater_id)
    theater_key = Key(models.TheaterOrganization, str(org_id), models.Theater, _theater_id)
    theater_code = theater_key.get().org_theater_code

    if not org_id:
        log.info("No TheaterOrganization ID")
        raise NotFoundException(details={'theaterorg_id': org_id})

    if not _theater_id:
        log.info("No Theater ID")
        raise NotFoundException(details={'theater_id': _theater_id})

    if theater_code not in REGISTRATION_ALLOWED_THEATER_CODES:
        log.info("Theater not allowed")
        raise InvalidActionException(message='This theater does not support this action')

    # User registration implementation here.
    user = WSUser(LCT_TRANSACTION_API)
    status, data = user.register(user_info['register'])

    if status == 'error':
        raise ThirdPartyServiceError('An error occurred while connecting to this theater', data)

    if status == 'success':
        return jsonify({'status': 0, 'registration_token': data})

    if status == 'fail':
        if data == 'Duplicate Email':
            data = 'Email Address already registered. Please try other email address'

        return jsonify({'status': 1, 'message': data})

# User login for MGi Integrated Cinemas.
@api_ver4.route('/member/login/<theater_id>/', methods=['POST'])
def login_user(theater_id):
    """
    Headers:
    :: DEVICE_ID

    REST form_parameters
    :: theater_id

    Payload
    :: email - registered user email
    :: password - you know this is

    e.g.
    { "login_info": {
      "email": "gmovies@localhost.com",
      "password": "password"
       }
    }
    """

    device_id = request.headers[DEVICE_ID]
    log.info("Device ID: {}".format(device_id))

    login_info = request.json
    log.info("LOGIN INFO: {}".format(login_info))

    if login_info == None or 'email' not in login_info['login_info'] or 'password' not in login_info['login_info']:
        log.warn("No login details")
        raise BadValueException('body', 'You must supply login info')

    org_id, _theater_id = decoded_theater_id(theater_id)
    theater_key = Key(models.TheaterOrganization, str(org_id), models.Theater, _theater_id)
    theater_code = theater_key.get().org_theater_code

    if not org_id:
        raise NotFoundException(details={'theaterorg_id': org_id})

    if not _theater_id:
        raise NotFoundException(details={'theater_id': _theater_id})

    if theater_code not in REGISTRATION_ALLOWED_THEATER_CODES:
        raise InvalidActionException(message='This theater does not support this action')

    # User login implementation here.
    user = WSUser(LCT_TRANSACTION_API)
    status, client_id, client_details, message = user.login(email=login_info['login_info']['email'],
            password=login_info['login_info']['password'])

    if status == 'error':
        raise ThirdPartyServiceError('An error occurred while connecting to this theater')

    if status == 'success':
        return jsonify({'status': 0, 'client_id': client_id, 'client_info': client_details})

    if status == 'fail':
        return jsonify({'status': 1, 'message': message})

# User registration verification for MGi Integrated Cinemas.
@api_ver4.route('/member/verify_registration/<theater_id>/', methods=['POST'])
def verify_registration(theater_id):
    """
    Headers:
    :: DEVICE_ID

    REST form_parameters
    :: theater_id

    Payload
    :: registration_token - generated after success registration
    e.g.
    { "registration_token": "randomtokencodehere"}
    """

    device_id = request.headers[DEVICE_ID]
    log.info("Device ID: {}".format(device_id))

    registration_info = request.json
    log.info("REGISTRATION_INFO: {}".format(registration_info))

    if registration_info == None or 'registration_token' not in registration_info:
        log.warn("No registration token")
        raise BadValueException('registration_token', 'Missing registration token')

    org_id, _theater_id = decoded_theater_id(theater_id)
    theater_key = Key(models.TheaterOrganization, str(org_id), models.Theater, _theater_id)
    theater_code = theater_key.get().org_theater_code

    if not org_id:
        raise NotFoundException(details={'theaterorg_id': org_id})

    if not _theater_id:
        raise NotFoundException(details={'theater_id': _theater_id})

    if theater_code not in REGISTRATION_ALLOWED_THEATER_CODES:
        raise InvalidActionException(message='This theater does not support this action')

    # User registration verification here.
    user = WSUser(LCT_TRANSACTION_API)
    status, data = user.verify(registration_info['registration_token'])

    if status == 'error':
        raise ThirdPartyServiceError('An error occurred while connecting to this theater. Failed to verify your email.', data)

    if status == 'success':
        return jsonify({'message': data})

# User reset password for MGi Integrated Cinemas.
@api_ver4.route('/member/reset_password/<org_theater_id>/', methods=['POST'])
def reset_password(org_theater_id):
    device_id = request.headers[DEVICE_ID]
    log.info("Device ID: {}".format(device_id))

    recovery_info = request.json
    log.info("USER_INFO: {}".format(recovery_info))

    if not recovery_info or 'email' not in recovery_info:
        log.warn("No email info")
        raise BadValueException('email', 'Email address is required')

    org_id, theater_id = decoded_theater_id(org_theater_id)
    theater_key = Key(models.TheaterOrganization, str(org_id), models.Theater, theater_id)
    theater = theater_key.get()

    if not org_id:
        raise NotFoundException(details={'theaterorg_id': org_id})

    if not theater_id:
        raise NotFoundException(details={'theater_id': theater_id})

    if theater.org_theater_code not in REGISTRATION_ALLOWED_THEATER_CODES:
        raise InvalidActionException(message='This theater does not support this action')

    queryset = models.MemberPasswordReset.query(
            models.MemberPasswordReset.email == recovery_info['email'],
            models.MemberPasswordReset.is_active == True)

    if queryset.count() == 1:
        existing_password_reset = queryset.get()
        existing_password_reset.is_active = False
        existing_password_reset.put()

    password_reset = models.MemberPasswordReset()
    password_reset.email = recovery_info['email']
    password_reset.theater = theater_key
    password_reset.put()

    password_reset_link = "%smember/password/reset/%s/" % (request.host_url, str(password_reset.key.id()))
    password_reset_request(user_email=recovery_info['email'],
            password_reset_link=password_reset_link, branch_account=theater.name)

    return jsonify({'message': 'Check your email for the next steps.'})

# User change password for MGi Integrated Cinemas.
@api_ver4.route('/member/update_password/<theater_id>/', methods=['POST'])
def update_password(theater_id):
    """
    Headers:
    :: DEVICE_ID

    REST form_parameters
    :: theater_id

    Payload
    :: email - email address used in registration
    :: new_password
    :: old_password
    e.g.
    {
        "email": "gmovies@localhost.com",
        "new_password": "gmoviespassword",
        "old_password": "password"
    }

    Response:
    ::msg -  password update message
    e.g:
    {
        "msg": "Change Password Successful!"
    }
    """

    device_id = request.headers[DEVICE_ID]
    log.info("Device ID: {}".format(device_id))

    update_info = request.json
    log.info("USER_INFO: {}".format(update_info))

    if update_info is None:
        log.warn("No data")
        raise BadValueException('email', 'Missing field data')

    if 'email' not in update_info or update_info['email'] == "":
        log.warn("No email info")
        raise BadValueException('email', 'Email address is required')

    if 'new_password' not in update_info or update_info['new_password'] == "":
        log.warn("Missing new password")
        raise BadValueException('new_password', 'New password is required')

    if 'old_password' not in update_info or update_info['old_password'] == "":
        log.warn("Missing old password")
        raise BadValueException('old_password', 'Old password is required')

    org_id, _theater_id = decoded_theater_id(theater_id)
    theater_key = Key(models.TheaterOrganization, str(org_id), models.Theater, _theater_id)
    theater_code = theater_key.get().org_theater_code

    if not org_id:
        raise NotFoundException(details={'theaterorg_id': org_id})

    if not _theater_id:
        raise NotFoundException(details={'theater_id': _theater_id})

    if theater_code not in REGISTRATION_ALLOWED_THEATER_CODES:
        raise InvalidActionException(message='This theater does not support this action')

    # User change password here.
    auth = WSAuth(LCT_SESSION_API)
    status, session_token = auth.get_session_and_authenticate(LCT_USERNAME, LCT_PASSWORD)

    if not session_token or status == 'error':
        log.warn('Error aquiring session token')
        raise ThirdPartyServiceError('An error occurred while connecting to this theater.')

    user = WSUser(LCT_TRANSACTION_API)
    status, data, _ = user.update_password(session_token=session_token,
            email=update_info['email'], new_password=update_info['new_password'],
            old_password=update_info['old_password'])

    if status == 'error':
        raise ThirdPartyServiceError('An error occurred while connecting to this theater. Failed to update your password.', data)

    if status == 'success':
        if data == 'Updating password Successful':
            data = 'Password Successfully Changed'

        return jsonify({'status': 0, 'message': data})

    if status == 'fail':
        return jsonify({'status': 1, 'message': data})