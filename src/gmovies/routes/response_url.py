import datetime
import base64
import logging
import sys

from flask import Blueprint, jsonify, redirect, render_template, request, url_for

from google.appengine.runtime.apiproxy_errors import OverQuotaError

from gmovies import models, orgs
from gmovies.admin import notify_admin
from gmovies.exceptions.api import *
from gmovies.tx.promo import state as promo_state
from gmovies.tx.util import translate_message_via_error_code
from gmovies.util.id_encoder import decoded_theater_id


log = logging.getLogger(__name__)
response_url = Blueprint('response_url', __name__, template_folder='response_url_templates')


@response_url.errorhandler(APIException)
def handle_api_error(e):
    res = jsonify(message=e.message, error_code=e.error_code, details=e.details)
    res.status_code = e.code

    return res

@response_url.errorhandler(OverQuotaError)
def handle_quota_error(e):
    res = jsonify(message='The backend has reached a resource quota. Please try again later.', 
            error_code='SYSTEM_OVER_QUOTA', details=e.message)
    res.status_code = 500
    notify_admin("Response URL", e, sys.exc_info())

    return res

@response_url.errorhandler(Exception)
def handle_system_error(e):
    res = jsonify(message='There has been a system error. Please try again later.',
            error_code='SYSTEM_GENERIC_ERROR', details=e.message)
    log.exception(e)
    res.status_code = 500
    notify_admin("Response URL", e, sys.exc_info())

    return res

@response_url.route('/response_url/')
def get_response_url():
    namespace = request.args.get('namespace', None)
    response_code = request.args.get('response_code', None)
    response_message = request.args.get('response_message', None)

    if namespace and response_code:
        response_message = translate_message_via_error_code(namespace, response_code, response_message)

    return render_template('response.html', response_code=response_code, response_message=response_message)

@response_url.route('/success_url/')
def get_success_url():
    return render_template('success.html')

@response_url.route('/failed_url/')
def get_failed_url():
    return render_template('failed.html')

@response_url.route('/cancel_url/')
def get_cancel_url():
    return render_template('cancel.html')

@response_url.route('/response_url/blocked_screening/')
def get_blocked_screening_response_url():
    is_sm_malls = False
    transaction = None

    if 'requestid' in request.args:
        requestid = base64.b64decode(request.args['requestid'])
        transaction = models.PromoReservationTransaction.query(
                models.PromoReservationTransaction.reservation_reference==requestid).get()

        if transaction:
            promo_key = transaction.reservations[0].promo
            promo = promo_key.get()

            if promo.theater_id:
                org_uuid, theater_id = decoded_theater_id(promo.theater_id)

                if org_uuid == orgs.SM_MALLS:
                    is_sm_malls = True

    return render_template('response_blocked_screening.html',
            transaction=transaction, is_sm_malls=is_sm_malls)

@response_url.route('/blocked_screening_tickets/validation/')
def tickets_validation_page_promos():
    message = ''
    reference_code = ''
    transaction = None

    if 'reference_code' in request.args:
        reference_code = request.args['reference_code']
        transaction = models.PromoReservationTransaction.query(
                models.PromoReservationTransaction.ticket.code==reference_code,
                models.PromoReservationTransaction.state==promo_state.TX_DONE).get()

        if 'validate_btn' in request.args:
            validate_btn = request.args['validate_btn']

            if not transaction:
                message = 'Sorry, but this reference code "%s" is invalid.' % reference_code

                return render_template('promo_tickets_validation.html',
                        transaction=transaction, reference_code=reference_code,
                        message=message)

            if validate_btn.lower() == 'verify':
                log.info("Verifying Reference Code, %s..." % reference_code)

                if transaction.is_verified and not transaction.is_claimed:
                    message = 'Reference code "%s" has already been verified.' % reference_code
                elif transaction.is_claimed:
                    message = 'Sorry, but this reference code "%s" has already been claimed.' % reference_code
                else:
                    message = 'Successfully verified reference code "%s".' % reference_code

                transaction.is_verified = True
                transaction.date_verified = datetime.datetime.today()
                transaction.put()
            elif validate_btn.lower() == 'claim':
                log.info("Claiming Reference Code, %s..." % reference_code)

                if transaction.is_claimed:
                    message = 'Sorry, but this reference code "%s" has already been claimed.' % reference_code
                else:
                    message = 'Thank you for claiming your tickets with reference code "%s".' % reference_code

                transaction.is_claimed = True
                transaction.date_claimed = datetime.datetime.today()
                transaction.put()

    return render_template('promo_tickets_validation.html',
            transaction=transaction, reference_code=reference_code,
            message=message)