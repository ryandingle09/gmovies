import base64
import datetime
import logging
import re
import sys
import time

from itertools import groupby
from operator import attrgetter

from google.appengine.api import taskqueue
from google.appengine.ext.ndb import Key, put_multi
from google.appengine.runtime.apiproxy_errors import OverQuotaError

from flask import Blueprint, jsonify, redirect, url_for, request, make_response, json, Response
from lxml import etree

from gmovies import models, tx, orgs, settings
from gmovies.admin import notify_admin
from gmovies.exceptions.api import *
from gmovies.util.id_encoder import decode_uuid


log = logging.getLogger(__name__)
paynamics_cb = Blueprint('paynamics', __name__, template_folder='callback_templates')
paynamics_cb_v1 = Blueprint('paynamics_v1', __name__, template_folder='callback_templates')
paynamics_cb_v2 = Blueprint('paynamics_v2', __name__, template_folder='callback_templates')


###############
#
# Paynamics Callback URL Version 0
#
###############

AUTH_THEATER_ORGANIZATION = 'X-GMovies-Auth-TheaterOrg'
AUTH_THEATER_CODE = 'X-GMovies-Auth-TheaterCode'


@paynamics_cb.errorhandler(APIException)
def handle_api_error(e):
    res = jsonify(message=e.message, error_code=e.error_code, details=e.details)
    res.status_code = e.code

    return res


@paynamics_cb.errorhandler(OverQuotaError)
def handle_quota_error(e):
    res = jsonify(message='The backend has reached a resource quota. Please try again later.',  error_code='SYSTEM_OVER_QUOTA', details=e.message)
    res.status_code = 500
    notify_admin("Paynamics Callback URL", e, sys.exc_info())

    return res


@paynamics_cb.errorhandler(Exception)
def handle_system_error(e):
    res = jsonify(message='There has been a system error. Please try again later.', error_code='SYSTEM_GENERIC_ERROR', details=e.message)
    log.exception(e)
    res.status_code = 500
    notify_admin("Paynamics Callback URL", e, sys.exc_info())

    return res


@paynamics_cb.before_request
def check_callback_auth():
    log.debug('Entered check_callback_auth()')

    if AUTH_THEATER_ORGANIZATION in request.headers and AUTH_THEATER_CODE in request.headers:
        theater_org = request.headers[AUTH_THEATER_ORGANIZATION]
        org = Key(models.TheaterOrganization, theater_org).get()

        log.debug("Auth params: TheaterOrg: %s", theater_org)

        if not org:
            log.error('TheaterOrg not found, callback not allowed')

            raise ClientAuthException('Failed auth check')
    else:
        log.error('No auth headers, callback not allowed')

        raise ClientAuthException('Theater organization authentication required')

@paynamics_cb.route('/<channel_name>', methods=['POST'])
def post_event(channel_name):
    log.info("Channel: %s", channel_name)
    log.info("Headers: {}".format(request.headers))

    event = {}
    body = request.values
    has_body = True if body else False

    log.info("Values: {}".format(body))
    log.info("Has Body:{}".format(has_body))

    if not body:
        log.info("Did not get body...")

        raise BadValueException('body', 'You must supply body info')

    log.info("Getting events...")

    if request.form["response_code"] == 'GR063' or request.form["response_code"] == 'GR033':
        log.info("response_code: %s, response_message: %s" % (request.form["response_code"], request.form["response_message"]))

        return make_response('RECEIVEOK', 200)

    org_uuid = request.headers[AUTH_THEATER_ORGANIZATION]
    theater_code = request.headers[AUTH_THEATER_CODE]
    org = Key(models.TheaterOrganization, org_uuid)
    timestamp_param = request.form["timestamp"]
    timestamp = parse_string_to_timestamp(timestamp_param.split('.')[0], '%Y-%m-%dT%H:%M:%S')

    event["merchant_id"] = request.form["merchantid"]
    event["request_id"] = request.form["request_id"]
    event["response_id"] = request.form["response_id"]
    event["timestamp"] = request.form["timestamp"]
    event["rebill_id"] = request.form["rebill_id"]
    event["signature"] = request.form["signature"]
    event["response_code"] = request.form["response_code"]
    event["response_message"] = request.form["response_message"]
    event["response_advise"] = request.form["response_advise"]
    event["theater_code"] = theater_code.upper()
    event["token_id"] = request.form["token_id"] if 'token_id' in request.form else ''
    event["token_info"] = request.form["token_info"] if 'token_info' in request.form else ''
    event["bin"] = request.form["bin"] if 'bin' in request.form else ''

    def to_event(e):
        ev = models.Event(parent=org)
        ev.event_channel = channel_name
        ev.org_id = request.form["request_id"]
        ev.payload = event

        if timestamp:
            ev.timestamp = timestamp

        return ev

    batch = [to_event(event)]
    put_multi(batch)

    log.info("Batch events: {}".format(batch))

    listeners = models.Listener.get_listeners(channel_name)
    listener_count = listeners.count()

    if listener_count != 0:
        version_key = attrgetter('version')
        by_version = groupby(sorted(listeners, key=version_key), version_key)

        log.debug("Triggering %d listeners", listener_count)

        for version,_ in by_version:
            log.info("Triggering callback listener for channel: {0} version {1}".format(channel_name, version))

            taskqueue.add(url=url_for('task.trigger_callback_listeners'),
                    queue_name='transactions', target=version,
                    params={'channel_name': channel_name})

    log.info("Got payment response. Responding with RECEIVEOK")

    return make_response('RECEIVEOK', 200)


###############
#
# Paynamics Callback URL Version 1
#
###############

@paynamics_cb_v1.errorhandler(APIException)
def handle_api_error_v1(e):
    res = jsonify(message=e.message, error_code=e.error_code, details=e.details)
    res.status_code = e.code

    return res


@paynamics_cb_v1.errorhandler(OverQuotaError)
def handle_quota_error_v1(e):
    res = jsonify(message='The backend has reached a resource quota. Please try again later.',  error_code='SYSTEM_OVER_QUOTA', details=e.message)
    res.status_code = 500
    notify_admin("Paynamics Callback URL v1", e, sys.exc_info())

    return res


@paynamics_cb_v1.errorhandler(Exception)
def handle_system_error_v1(e):
    res = jsonify(message='There has been a system error. Please try again later.', error_code='SYSTEM_GENERIC_ERROR', details=e.message)
    log.exception(e)
    res.status_code = 500
    notify_admin("Paynamics Callback URL v1", e, sys.exc_info())

    return res


# GMovies Blocked Screening Callback URL
# <channel_name>: blocked-paynamics-payment-complete
@paynamics_cb_v1.route('/<channel_name>', methods=['POST'])
def post_event_blocked(channel_name):
    log.info("BLOCKED SCREENING: Channel: %s", channel_name)

    events = {}
    timestamp = None
    body = request.values

    if not body:
        log.warn("BLOCKED SCREENING: Did not get body...")

        raise BadValueException('body', 'You must supply body info')

    events["merchantid"] = ''
    events["request_id"] = ''
    events["response_id"] = ''
    events["timestamp"] = ''
    events["rebill_id"] = ''
    events["signature"] = ''
    events["response_code"] = ''
    events["response_message"] = ''
    events["response_advise"] = ''

    log.info("BLOCKED SCREENING: Getting events...")

    if 'paymentresponse' in request.form:
        payment_response = request.form["paymentresponse"]

        if payment_response:
            decoded_response = base64.b64decode(str(payment_response), altchars=' .-_:')
            xml_data = etree.fromstring(decoded_response)

            for item in xml_data.getiterator():
                key_name = re.sub(r'\{.*?\}', '', item.tag)

                if key_name in events and item.text:
                    events[key_name] = item.text

            if events['timestamp']:
                timestamp = parse_string_to_timestamp(events['timestamp'].split('.')[0], '%Y-%m-%dT%H:%M:%S')
        else:
            log.warn("BLOCKED SCREENING: Missing paymentresponse value from Paynamics...")
            raise BadValueException('body', 'Missing paymentresponse value from Paynamics')
    else:
        log.warn("BLOCKED SCREENING: Missing paymentresponse from Paynamics...")
        raise BadValueException('body', 'Missing paymentresponse from Paynamics')

    if events["response_code"] == 'GR063' or events["response_code"] == 'GR033':
        log.warn("BLOCKED SCREENING: response_code: %s, response_message: %s" % (events["response_code"], events["response_message"]))
        return make_response('RECEIVEOK', 200)

    def to_event(e):
        ev = models.Event()
        ev.event_channel = channel_name
        ev.org_id = events["request_id"]
        ev.payload = events

        if timestamp:
            ev.timestamp = timestamp

        return ev

    batch = [to_event(events)]
    put_multi(batch)

    log.info("BLOCKED SCREENING: Batch events: {}".format(batch))

    listeners = models.Listener.get_listeners(channel_name)
    listener_count = listeners.count()

    if listener_count != 0:
        version_key = attrgetter('version')
        by_version = groupby(sorted(listeners, key=version_key), version_key)

        log.debug("BLOCKED SCREENING: Triggering %d listeners", listener_count)

        for version,_ in by_version:
            log.info("BLOCKED SCREENING: Triggering callback listener for channel: {0} version {1}".format(channel_name, version))

            taskqueue.add(url=url_for('task.trigger_callback_listeners'),
                    queue_name='transactionspromo', target=version,
                    params={'channel_name': channel_name})

    log.info("BLOCKED SCREENING: Got payment response. Responding with RECEIVEOK")

    return make_response('RECEIVEOK', 200)


###############
#
# Paynamics Callback URL Version 2
#
###############

@paynamics_cb_v2.errorhandler(APIException)
def handle_api_error_v1(e):
    res = jsonify(message=e.message, error_code=e.error_code, details=e.details)
    res.status_code = e.code

    return res

@paynamics_cb_v2.errorhandler(OverQuotaError)
def handle_quota_error_v1(e):
    res = jsonify(message='The backend has reached a resource quota. Please try again later.',  error_code='SYSTEM_OVER_QUOTA', details=e.message)
    res.status_code = 500
    notify_admin("Paynamics Callback URL v2", e, sys.exc_info())

    return res

@paynamics_cb_v2.errorhandler(Exception)
def handle_system_error_v1(e):
    res = jsonify(message='There has been a system error. Please try again later.', error_code='SYSTEM_GENERIC_ERROR', details=e.message)
    log.exception(e)
    res.status_code = 500
    notify_admin("Paynamics Callback URL v2", e, sys.exc_info())

    return res

@paynamics_cb_v2.route('/<channel_name>', methods=['POST'])
def post_event_v2(channel_name):
    log.info("post_event_v2, channel: %s...", channel_name)
    log.info("post_event_v2, headers: {}...".format(request.headers))

    body = request.values

    if not body:
        log.warn("ERROR!, post_event_v2, has no body...")

        raise BadValueException('body', 'You must supply body info.')

    if 'paymentresponse' not in request.form:
        log.warn("ERROR!, post_event_v2, missing parameter paymentresponse in request.form...")

        raise BadValueException('body', 'Missing paymentresponse from Paynamics.')

    payment_response = request.form["paymentresponse"]

    if not payment_response:
        log.warn("ERROR!, post_event_v2, not paymentresponse...")

        raise BadValueException('body', 'Missing paymentresponse value from Paynamics.')

    event = {}
    event['merchant_id'] = ''
    event['request_id'] = ''
    event['response_id'] = ''
    event['timestamp'] = ''
    event['rebill_id'] = ''
    event['signature'] = ''
    event['response_code'] = ''
    event['response_message'] = ''
    event['response_advise'] = ''
    event['theater_code'] = ''
    event['token_id'] = ''
    event['token_info'] = ''
    event['bin'] = ''

    log.debug("post_event_v2, getting events...")

    timestamp = None
    decoded_response = base64.b64decode(str(payment_response), altchars=' .-_:')
    xml_data = etree.fromstring(decoded_response)

    log.debug(decoded_response)
    items = []
    values = []

    for item in xml_data.getiterator():
        key_name = re.sub(r'\{.*?\}', '', item.tag)

        # to get token_id and token_info in metadata/subdata
        if key_name == 'item':
            items.append(item.text)
        if key_name == 'value':
            values.append(item.text)

        if key_name in event and item.text:
            event[key_name] = item.text

    if items and len(items) == len(values):
        for i in xrange(len(items)):
            if items[i] in event:
                event[items[i]] = values[i]

    if event['timestamp']:
        timestamp = parse_string_to_timestamp(event['timestamp'].split('.')[0], '%Y-%m-%dT%H:%M:%S')

    try:
        theater_code, reservation_reference = event['request_id'].split('~')
        theater = models.Theater.query(models.Theater.is_published==True, models.Theater.org_theater_code==theater_code).get()
        org_key = theater.key.parent()
    except Exception, e:
        log.warn("ERROR!, post_event_v2, something went wrong in getting theater and org_key...")
        log.error(e)

        raise NotFoundException(details={'request_id': event['request_id']})

    event['request_id'] = reservation_reference
    event['theater_code'] = theater_code

    if event['response_code'] == 'GR063' or event['response_code'] == 'GR033':
        log.warn("SKIP!, post_event_v2, response_code: %s, response_message: %s..." % (event['response_code'], event['response_message']))

        return make_response('RECEIVEOK', 200)

    def to_event(e):
        ev = models.Event(parent=org_key)
        ev.event_channel = channel_name
        ev.org_id = reservation_reference
        ev.payload = event

        if timestamp:
            ev.timestamp = timestamp

        return ev

    batch = [to_event(event)]
    put_multi(batch)

    log.info("post_event_v2, batch events: {}...".format(batch))

    listeners = models.Listener.get_listeners(channel_name)
    listener_count = listeners.count()

    if listener_count != 0:
        version_key = attrgetter('version')
        by_version = groupby(sorted(listeners, key=version_key), version_key)

        log.debug("post_event_v2, triggering %d listeners...", listener_count)

        for version,_ in by_version:
            log.info("post_event_v2, triggering callback listener for channel: %s, version: %s..." % (channel_name, version))

            taskqueue.add(url=url_for('task.trigger_callback_listeners'), queue_name='transactions', target=version, params={'channel_name': channel_name})

    log.debug("post_event_v2, got payment response. responding with RECEIVEOK...")

    return make_response('RECEIVEOK', 200)


def parse_string_to_timestamp(param, param_format='%Y-%m-%d %H:%M:%S'):
    try:
        datetime_format = datetime.datetime.strptime(param, param_format)
        timestamp = int(time.mktime(datetime_format.timetuple()))
    except Exception, e:
        log.warn('ERROR!, Failed to parse string(%s) to timestamp...' % param)
        log.error(e)

        timestamp = None

    return timestamp