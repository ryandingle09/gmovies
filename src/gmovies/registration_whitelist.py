# Theater codes where user registration is allowed.
# Usually handled by third-party system like MGI
# Append theater codes as necessary
REGISTRATION_ALLOWED_THEATER_CODES = ['LCT']