from google.appengine.api.app_identity import get_default_version_hostname
from google.appengine.ext import ndb
import datetime

class AccountSettings(ndb.Model):
    merchant_key = ndb.StringProperty(required=True)
    merchant_code = ndb.StringProperty(required=True)
    notification_addresses = ndb.StringProperty(repeated=True)
    globe_csr_addresses = ndb.StringProperty(repeated=True)
    last_updated = ndb.DateTimeProperty(auto_now_add=True)
    #donors_list = ndb.PickleProperty()
    #donors_last_10 = ndb.PickleProperty()
    last_notif = ndb.DateTimeProperty()

    @classmethod
    def get_settings(cls):
        settings = cls.get_by_id('csrc_settings')
        if not settings:
            settings = cls(id='csrc_settings')
            settings.notification_addresses = [ 'adelossantos@yondu.com','ltanalas@yondu.com', 'ajasmin@yondu.com' ]
            settings.globe_csr_addresses = [ 'ajasmin@yondu.com', 'adelossantos@yondu.com','ltanalas@yondu.com' ]
            settings.merchant_code = 'PH00123' # This is not a valid code. FOR TESTING PURPOSES ONLY!!!
            settings.merchant_key = 'abcd12345'  # This is not a valid code. FOR TESTING PURPOSES ONLY!!!
            settings.last_notif = datetime.datetime.now()
            settings.put()

        return settings

#IPAY88/GMOVIES Corporate Social Responsibility Campaign PAYMENT CALLBACK
#GMOVIES_CSR_PAYMENT_CALLBACK = 'https://globe-gmovies.appspot.com/ipay88/1/ipay88-bank-deposit-complete'

CSRC_SETTINGS = AccountSettings.get_settings()

GMOVIES_CSRC_PAYMENT_CALLBACK = 'https://' + get_default_version_hostname() + '/ipay88/2/ipay88-csrc-payment-complete'
GMOVIES_MERCHANT_KEY = CSRC_SETTINGS.merchant_key
GMOVIES_MERCHANT_CODE = CSRC_SETTINGS.merchant_code
GMOVIES_CSRC_NOTIFICATION_ADDRESSES = CSRC_SETTINGS.notification_addresses