from google.appengine.api import memcache
from google.appengine.ext import ndb
from google.appengine.api.app_identity import get_default_version_hostname

import orgs

from .models import TheaterOrganization


# MISC
class SystemSettings(ndb.Model):
    enforce_auth                  = ndb.BooleanProperty(default=False)
    enforce_auth_website          = ndb.BooleanProperty(default=False)
    notification_addresses        = ndb.StringProperty(repeated=True)
    cms_administrators            = ndb.StringProperty(repeated=True)
    notification_retry_threshold  = ndb.IntegerProperty(default=3)
    reap_timeout                  = ndb.IntegerProperty(default=60)
    client_initiated_reap_timeout = ndb.IntegerProperty(default=600) # 10 minutes
    rockwell_reap_timeout         = ndb.IntegerProperty(default=900) # 15 minutes
    rt_api_key                    = ndb.StringProperty(default='xcyuvytdbtzn6hqcg5dyksqd')
    api_versions_deprecated       = ndb.StringProperty(repeated=True)

    # lucky chinatown settings. service control.
    lct_reap_timeout = ndb.IntegerProperty(default=1200) # 20 minutes
    lct_username     = ndb.StringProperty(default='gmovies')
    lct_password     = ndb.StringProperty(default='tY4!guTVa9')

    @classmethod
    def get_settings(cls):
        settings = cls.get_by_id('settings')

        if not settings:
            settings                        = cls(id='settings')
            settings.notification_addresses = ['zrldingle@globe.com.ph', 'klim@yondu.com']
            settings.cms_administrators     = ['zrldingle@globe.com.ph', 'klim@yondu.com']
            settings.put()

        return settings


SETTINGS                      = SystemSettings.get_settings()
ENFORCE_AUTH                  = SETTINGS.enforce_auth
ENFORCE_AUTH_WEBSITE          = SETTINGS.enforce_auth_website
NOTIFICATION_ADDRESSES        = SETTINGS.notification_addresses
NOTIFICATION_RETRY_THRESHOLD  = SETTINGS.notification_retry_threshold
CMS_ADMINISTRATORS            = SETTINGS.cms_administrators
REAP_TIMEOUT                  = SETTINGS.reap_timeout
CLIENT_INITIATED_REAP_TIMEOUT = SETTINGS.client_initiated_reap_timeout
RWPP_REAP_TIMEOUT             = SETTINGS.rockwell_reap_timeout
ROTTEN_TOMATOES_API_KEY       = SETTINGS.rt_api_key
API_VERSIONS_DEPRECATED       = SETTINGS.api_versions_deprecated

# FIXME: Hardcoded Rockwell/IPay88 settings
# For Web-API, currently not applicable as we're mobile SDKs
IPAY88_SANDBOX_ENDPOINT    = "https://sandbox.ipay88.com.ph/epayment/entry.asp"
IPAY88_PRODUCTION_ENDPOINT = "https://payment.ipay88.com.ph/epayment/entry.asp"
IPAY88_PAYMENT_REQUERY_URL = "https://payment.ipay88.com.ph/epayment/enquiry.asp"
IPAY88_PAYMENT_ENDPOINT    = IPAY88_PRODUCTION_ENDPOINT

# CINEMA PARTNER/THIRD PARTY BASE URLs: STAGING / SIT
# ROCKWELL_ENDPOINT_PRIMARY_STAGING    = 'https://52.74.100.125/gmovies-webapp/api/v1/'
ROCKWELL_ENDPOINT_PRIMARY_STAGING    = 'https://52.77.98.38/gmovies-webapp/api/v1/'
ROCKWELL_STAGING_ACCESS_TOKEN        = '6aa93cb8-9f8b-46fa-8594-ff8294f0833a'
# GREENHILLS_ENDPOINT_PRIMARY_STAGING  = 'https://54.169.189.122:8443/api/v1/'
GREENHILLS_ENDPOINT_PRIMARY_STAGING  = 'https://greenhills.qaslave.digitalventures.ph/api/v1/'
GREENHILLS_STAGING_ACCESS_TOKEN      = '33c821a5-2ce3-4f87-bb32-e9352dedede9'
CINEMA76_ENDPOINT_PRIMARY_STAGING    = 'http://54.169.5.234/api/v1/'
CINEMA76_STAGING_ACCESS_TOKEN        = '35dd39bf-258e-4ad3-a830-bdd40aa3855a'
GLOBEEVENTS_ENDPOINT_PRIMARY_STAGING = 'http://13.250.236.181/api/v1/'
GLOBEEVENTS_STAGING_ACCESS_TOKEN     = 'c2cef78a-9f0b-4118-b77b-331b30dc0349'
CITYMALL_ENDPOINT_PRIMARY_STAGING    = 'https://13.228.140.205:8443/api/v1/'
CITYMALL_STAGING_ACCESS_TOKEN        = 'c2cef78a-9f0b-4118-b77b-331b30dc0349'

# CINEMA PARTNER/THIRD PARTY BASE URLs: PRODUCTION
# ROCKWELL_ENDPOINT_PRIMARY_PRODUCTION    = 'https://54.169.12.237:8443/gmovies-webapp/api/v1/'
# Rockwell enabled movie schedule listing ONLY - Dec 14, 2017
ROCKWELL_ENDPOINT_PRIMARY_PRODUCTION    = 'https://13.229.195.69/gmovies-webapp/api/v1/'
ROCKWELL_PRODUCTION_ACCESS_TOKEN        = 'fee67928-d823-4a7d-8654-dc9684fad283'
GREENHILLS_ENDPOINT_PRIMARY_PRODUCTION  = 'https://52.74.124.4:8443/api/v1/'
GREENHILLS_PRODUCTION_ACCESS_TOKEN      = '33c821a5-2ce3-4f87-bb32-e9352dedede9'
CINEMA76_ENDPOINT_PRIMARY_PRODUCTION    = 'http://cinema76.gmovies.ph/api/v1/'
CINEMA76_PRODUCTION_ACCESS_TOKEN        = '9824f11e-1668-4d63-9dd7-a1b53e806748'
GLOBEEVENTS_ENDPOINT_PRIMARY_PRODUCTION = 'http://13.228.162.111/api/v1/'
GLOBEEVENTS_PRODUCTION_ACCESS_TOKEN     = 'a8e31ac1-07e2-4c0b-8a00-d9bcfcbae327'
CITYMALL_ENDPOINT_PRIMARY_PRODUCTION    = 'https://13.229.165.31:8443/api/v1/'
CITYMALL_PRODUCTION_ACCESS_TOKEN        = 'c2cef78a-9f0b-4118-b77b-331b30dc0349'

# PROMOCODE SERVER URI
PROMOCODE_SERVER_STAGING            = 'https://gmovies-promo-staging.appspot.com'
PROMOCODE_SERVER_PRODUCTION         = 'https://gmovies-promo.appspot.com'

# CLAIM CODE GLOBAL SERVER URI
CLAIMCODE_GLOBAL_SERVER_STAGING     = 'http://core.qa.digitalventures.ph/api'
CLAIMCODE_GLOBAL_SERVER_PRODUCTION  = 'http://core.gmovies.ph/api'

# GMOVIES GLOBAL SERVER URI
GMOVIES_DV_GLOBAL_SERVER_STAGING    = 'http://gmovies.api.qa.digitalventures.ph/api'
GMOVIES_DV_GLOBAL_SERVER_PRODUCTION = 'https://api2.gmovies.ph/api'

# GCASH PROXY BASE URLs
GCASH_PROXY_BASE_URL_STAGING        = 'http://52.77.204.204/api' # will change this once the staging for gcash is up.
GCASH_PROXY_BASE_URL_PRODUCTION     = 'http://52.77.204.204/api'

# GREWARDS/WAKANDA PROXY BASE URLs/API URL
GREWARDS_PROXY_BASE_URL_STAGING        = 'https://gmov917api.globedv.com/api/v1' # will change this once the staging for Grewards/wakanda is up.
GREWARDS_PROXY_BASE_URL_PRODUCTION     = 'https://gmov917api.globedv.com/api/v1'

# GMOVIES DIGITAL VENTURES BASE URLs
GMOVIES_DIGITALVENTURES_V1_BASE_URLS_STAGING    = ['http://gmovies.api.qa.digitalventures.ph/api/v1', 'http://gmovies.api.dev.digitalventures.ph/api/v1']
GMOVIES_DIGITALVENTURES_V1_BASE_URLS_PRODUCTION = ['https://api2.gmovies.ph/api/v1']

# GOOGLE CLOUD STORAGE BUCKET NAME
GMOVIES_GCS_BUCKET_NAME_STAGING    = '/globe-gmovies-staging-posters/'
GMOVIES_GCS_BUCKET_NAME_PRODUCTION = '/globe-gmovies-images/'

# ADD NEEDED BPI CREDIT CARD BIN HERE.
BPI_BIN_STAGING         = ['443426', '525895']
BPI_BIN_PRODUCTION      = ['542458', '548809', '545512', '520365', '545512', '418898',
        '460888', '558769', '526491', '453756', '536214', '453756']
# ADD NEEDED CITIBANK CREDIT CARD BIN HERE.
CITIBANK_BIN_STAGING    = ['443426', '525895']
CITIBANK_BIN_PRODUCTION = ['434348', '453248', '453971', '453972', '524202', '537726',
        '540127', '542339', '549419', '552097', '403418', '403419']
# PNB CREDIT CARD BIN
PNB_BIN_STAGING         = ['443426', '525895']
PNB_BIN_PRODUCTION      = ['524046', '488907', '488908', '512634', '513208', '521387',
        '533639', '533653', '540076', '543761', '545143', '548509', '552280']
# BDO CREDIT CARD BIN
BDO_BIN_STAGING         = ['443426', '525895']
BDO_BIN_PRODUCTION      = ['545635', '541781', '512571', '526727', '545275', '545636',
        '541702', '531228', '524326', '524301', '523994', '518869', '519963',
        '492164', '492161', '418358', '492110', '492101', '488957', '464995', '418359']
# GCash MasterCard CREDIT CARD BIN
GCASH_BIN_STAGING       = ['443426', '525895']
GCASH_BIN_PRODUCTION    = ['536595', '549627']
#METRO BANK CREDIT CARD BIN
METROBANK_BIN_STAGING       = ['443426', '525895']
METROBANK_BIN_PRODUCTION    = ['546498', '546497', '534507', '521822', '545235', '405598', '405599', '556628']
#CHINA BANK CREDIT CARD BIN
CHINABANK_BIN_STAGING       = ['443426', '525895']
CHINABANK_BIN_PRODUCTION    = ['522644', '522604', '528557']
#UNION BANK CREDIT CARD BIN
UNIONBANK_BIN_STAGING       = ['443426', '525895']
UNIONBANK_BIN_PRODUCTION    = ['123456']
#ROBINSONS BANK CREDIT CARD BIN
ROBINSONSBANK_BIN_STAGING       = ['443426', '525895']
ROBINSONSBANK_BIN_PRODUCTION    = ['520315', '523091']

# E-PLUS ACCOUNT DETAILS - STAGING
EPLUS_MERCHANT_ID_STAGING      = 1
EPLUS_MERCHANT_CODE_STAGING    = 'GMOVIES'
# EPLUS_CARDNUMBER_STAGING       = '1409596943576357'
# EPLUS_PIN_STAGING              = '123456'
EPLUS_CARDNUMBER_STAGING       = '1409323616259017'
EPLUS_PIN_STAGING              = '953771'
EPLUS_SECRET_KEY_STAGING       = 'JaqQ8QDe3n'
# E-PLUS ACCOUNT DETAILS - PRODUCTION
EPLUS_MERCHANT_ID_PRODUCTION   = 1
EPLUS_MERCHANT_CODE_PRODUCTION = 'GMOVIES'
EPLUS_CARDNUMBER_PRODUCTION    = '1409972606213754'
EPLUS_PIN_PRODUCTION           = '123456'
EPLUS_SECRET_KEY_PRODUCTION    = 'JaqQ8QDe3n'

# MGi ACCOUNT DETAILS (USERNAME AND PASSWORD)
# SM MALLS
SM_USERNAME_MGI        = 'gmovies'
SM_PASSWORD_MGI        = 'tY4!guTVa9'
# ROBINSONS MALLS
ROBINSONS_USERNAME_MGI = 'gmovies'
ROBINSONS_PASSWORD_MGI = 'tY4!guTVa9'
# LUCKY CHINATOWN
LCT_USERNAME_MGI       = 'gmovies'
LCT_PASSWORD_MGI       = 'tY4!guTVa9'

# Refund Token for GMovies API
REFUND_TOKEN_STAGING    = '!GM0VI3S-!DEVELOPMENT-!REFUND-!ACCESS-!TOKEN'
REFUND_TOKEN_PRODUCTION = '!@Mc78veJ*QcwdE%bW^WcBqkF4yT7gQ38eWVH2Ws5VIIasldhfakljhf3kjwCT$3mjpRr%6QW2b6S2VZsS7QzuMcJPhz>F'

###############
#
# SET THIS TO PRODUCTION WHEN READY
#
###############

# Set this to false for Production deployment
IS_STAGING = True

if IS_STAGING:
    # STAGING SETTINGS
    ROCKWELL_ENDPOINT_PRIMARY            = ROCKWELL_ENDPOINT_PRIMARY_STAGING
    GREENHILLS_ENDPOINT_PRIMARY          = GREENHILLS_ENDPOINT_PRIMARY_STAGING
    CINEMA76_ENDPOINT_PRIMARY            = CINEMA76_ENDPOINT_PRIMARY_STAGING
    GLOBEEVENTS_ENDPOINT_PRIMARY         = GLOBEEVENTS_ENDPOINT_PRIMARY_STAGING
    CITYMALL_ENDPOINT_PRIMARY            = CITYMALL_ENDPOINT_PRIMARY_STAGING
    PROMOCODE_SERVER                     = PROMOCODE_SERVER_STAGING
    GMOVIES_GCS_BUCKET_NAME              = GMOVIES_GCS_BUCKET_NAME_STAGING
    CLAIMCODE_GLOBAL_SERVER              = CLAIMCODE_GLOBAL_SERVER_STAGING
    GMOVIES_DV_GLOBAL_SERVER             = GMOVIES_DV_GLOBAL_SERVER_STAGING
    GCASH_PROXY_BASE_URL                 = GCASH_PROXY_BASE_URL_STAGING
    GREWARDS_PROXY_BASE_URL              = GREWARDS_PROXY_BASE_URL_STAGING
    GMOVIES_DIGITALVENTURES_V1_BASE_URLS = GMOVIES_DIGITALVENTURES_V1_BASE_URLS_STAGING
    EPLUS_MERCHANT_ID                    = EPLUS_MERCHANT_ID_STAGING
    EPLUS_MERCHANT_CODE                  = EPLUS_MERCHANT_CODE_STAGING
    EPLUS_CARDNUMBER                     = EPLUS_CARDNUMBER_STAGING
    EPLUS_PIN                            = EPLUS_PIN_STAGING
    EPLUS_SECRET_KEY                     = EPLUS_SECRET_KEY_STAGING
    PAYNAMICS_CCP_BIN                    = BPI_BIN_STAGING
    PAYNAMICS_CCP_BIN_CITIBANK           = CITIBANK_BIN_STAGING
    PAYNAMICS_CCP_BIN_PNB                = PNB_BIN_STAGING
    PAYNAMICS_CCP_BIN_BDO                = BDO_BIN_STAGING
    PAYNAMICS_CCP_BIN_GCASH              = GCASH_BIN_STAGING
    PAYNAMICS_CCP_BIN_METROBANK          = METROBANK_BIN_STAGING
    PAYNAMICS_CCP_BIN_CHINABANK          = CHINABANK_BIN_STAGING
    PAYNAMICS_CCP_BIN_UNIONBANK          = UNIONBANK_BIN_STAGING
    PAYNAMICS_CCP_BIN_ROBINSONSBANK      = ROBINSONSBANK_BIN_STAGING
    REFUND_TOKEN                         = REFUND_TOKEN_STAGING
else:
    # PRODUCTION SETTINGS
    ROCKWELL_ENDPOINT_PRIMARY            = ROCKWELL_ENDPOINT_PRIMARY_PRODUCTION
    GREENHILLS_ENDPOINT_PRIMARY          = GREENHILLS_ENDPOINT_PRIMARY_PRODUCTION
    CINEMA76_ENDPOINT_PRIMARY            = CINEMA76_ENDPOINT_PRIMARY_PRODUCTION
    GLOBEEVENTS_ENDPOINT_PRIMARY         = GLOBEEVENTS_ENDPOINT_PRIMARY_PRODUCTION
    CITYMALL_ENDPOINT_PRIMARY            = CITYMALL_ENDPOINT_PRIMARY_PRODUCTION
    PROMOCODE_SERVER                     = PROMOCODE_SERVER_PRODUCTION
    GMOVIES_GCS_BUCKET_NAME              = GMOVIES_GCS_BUCKET_NAME_PRODUCTION
    CLAIMCODE_GLOBAL_SERVER              = CLAIMCODE_GLOBAL_SERVER_PRODUCTION
    GMOVIES_DV_GLOBAL_SERVER             = GMOVIES_DV_GLOBAL_SERVER_PRODUCTION
    GCASH_PROXY_BASE_URL                 = GCASH_PROXY_BASE_URL_PRODUCTION
    GREWARDS_PROXY_BASE_URL              = GREWARDS_PROXY_BASE_URL_PRODUCTION
    GMOVIES_DIGITALVENTURES_V1_BASE_URLS = GMOVIES_DIGITALVENTURES_V1_BASE_URLS_PRODUCTION
    EPLUS_MERCHANT_ID                    = EPLUS_MERCHANT_ID_PRODUCTION
    EPLUS_MERCHANT_CODE                  = EPLUS_MERCHANT_CODE_PRODUCTION
    EPLUS_CARDNUMBER                     = EPLUS_CARDNUMBER_PRODUCTION
    EPLUS_PIN                            = EPLUS_PIN_PRODUCTION
    EPLUS_SECRET_KEY                     = EPLUS_SECRET_KEY_PRODUCTION
    PAYNAMICS_CCP_BIN                    = BPI_BIN_PRODUCTION
    PAYNAMICS_CCP_BIN_CITIBANK           = CITIBANK_BIN_PRODUCTION
    PAYNAMICS_CCP_BIN_PNB                = PNB_BIN_PRODUCTION
    PAYNAMICS_CCP_BIN_BDO                = BDO_BIN_PRODUCTION
    PAYNAMICS_CCP_BIN_GCASH              = GCASH_BIN_PRODUCTION
    PAYNAMICS_CCP_BIN_METROBANK          = METROBANK_BIN_PRODUCTION
    PAYNAMICS_CCP_BIN_CHINABANK          = CHINABANK_BIN_PRODUCTION
    PAYNAMICS_CCP_BIN_UNIONBANK          = UNIONBANK_BIN_PRODUCTION
    PAYNAMICS_CCP_BIN_ROBINSONSBANK      = ROBINSONSBANK_BIN_PRODUCTION
    REFUND_TOKEN                         = REFUND_TOKEN_PRODUCTION

###############
#
# GMOVIES BLOCKED SCREENING SETTINGS
#
###############

BLOCKED_NOTIFICATION_URL = 'https://' + get_default_version_hostname() + '/paynamics/1/blocked-paynamics-payment-complete'
BLOCKED_RESPONSE_URL     = 'https://' + get_default_version_hostname() + '/gmovies/response_url/blocked_screening'
BLOCKED_CANCEL_URL       = 'https://' + get_default_version_hostname() + '/gmovies/cancel_url'

if IS_STAGING:
    # STAGING - BLOCKED SCREENING CREDIT-CARD
    BLOCKED_MERCHANT_ID               = '000000281015AD82DC8D'
    BLOCKED_MERCHANT_NAME             = 'GMOVIES_BLOCK_BPI_MIGS_PHP'
    BLOCKED_MERCHANT_KEY              = '6C39AB6F6F46FC93DD4F81DFF4095D5A'
    BLOCKED_PAYNAMICS_FORM_TARGET     = 'https://testpti.payserv.net/webpayment/erockwell/Default.aspx'
    BLOCKED_PAYNAMICS_REQUERY_URL     = 'https://testpti.payserv.net/paygate/ccservice.asmx?wsdl'
    BLOCKED_PAYNAMICS_TARGETNAMESPACE = 'http://paygate.paynamics.net/'
else:
    # PRODUCTION - BLOCKED SCREENING CREDIT-CARD
    BLOCKED_MERCHANT_ID               = '000000031115DBE5F5E8'
    BLOCKED_MERCHANT_NAME             = 'YONDU_GBLOCK_BPI_PHP'
    BLOCKED_MERCHANT_KEY              = 'CC0C37116A49617E783CCCD073EF390E'
    # BLOCKED_PAYNAMICS_FORM_TARGET     = 'https://apps.paynamics.net/webpayment/gblock/default.aspx' # paynamics old form target for GMovies blocked screening.
    # BLOCKED_PAYNAMICS_REQUERY_URL     = 'https://paygate.paynamics.net/ccservice/ccservice.asmx?wsdl' # paynamics old CCService API for GMovies blocked screening.
    BLOCKED_PAYNAMICS_FORM_TARGET     = 'https://ptiapps.paynamics.net/webpayment_v2/default.aspx' # paynamics new form target for GMovies blocked screening.
    BLOCKED_PAYNAMICS_REQUERY_URL     = 'https://ptipaygate.paynamics.net/ccservice/ccservice.asmx?wsdl' # paynamics new CCService API for GMovies blocked screening.
    BLOCKED_PAYNAMICS_TARGETNAMESPACE = 'http://paygate.paynamics.net/'


###############
#
# GET ACCESS TOKEN
#
###############

try:
    ROCKWELL_ACCESS_TOKEN    = TheaterOrganization.get_access_code(str(orgs.ROCKWELL_MALLS))
    GREENHILLS_ACCESS_TOKEN  = TheaterOrganization.get_access_code(str(orgs.GREENHILLS_MALLS))
    CINEMA76_ACCESS_TOKEN    = TheaterOrganization.get_access_code(str(orgs.CINEMA76_MALLS))
    GLOBEEVENTS_ACCESS_TOKEN = TheaterOrganization.get_access_code(str(orgs.GLOBE_EVENTS))
    CITYMALL_ACCESS_TOKEN    = TheaterOrganization.get_access_code(str(orgs.CITY_MALLS))
except:
    ROCKWELL_ACCESS_TOKEN    = ''
    GREENHILLS_ACCESS_TOKEN  = ''
    CINEMA76_ACCESS_TOKEN    = ''
    GLOBEEVENTS_ACCESS_TOKEN = ''
    CITYMALL_ACCESS_TOKEN    = ''


###############
#
# iPAY88 PAYMENT GATEWAY SETTINGS
#
###############

# GENERIC IPAY88 CALLBACK ENDPOINTS
GMOVIES_IPAY88_RECIEVEOK_RESPONSE_URL = 'https://' + get_default_version_hostname() + '/ipay88/1/backend-response'
GMOVIES_IPAY88_PAYMENT_COMPLETE_URL   = 'https://' + get_default_version_hostname() + '/ipay88/1/ipay88-payment-complete'

# ROCKWELL - POWER PLANT MALL
ROCKWELL_MERCHANT_KEY              = 'xffe5acG70' # don't change this unless needed
ROCKWELL_MERCHANT_CODE             = 'PH00235' # don't change this unless needed
RWPP_IPAY88_RECIEVEOK_RESPONSE_URL = GMOVIES_IPAY88_RECIEVEOK_RESPONSE_URL
RWPP_IPAY88_PAYMENT_COMPLETE_URL   = GMOVIES_IPAY88_PAYMENT_COMPLETE_URL

# MEGAWORLD - LUCKY CHINATOWN
LCT_MERCHANT_KEY                  = 'UnbAHgVmxv' # change these?
LCT_MERCHANT_CODE                 = 'PH00285' # change these?
LCT_TIMEOUT                       = SETTINGS.lct_reap_timeout
LCT_USERNAME                      = SETTINGS.lct_username # will remove this in the future for optimization.
LCT_PASSWORD                      = SETTINGS.lct_password # will remove this in the future for optimization.
LCT_IPAY88_RECIEVEOK_RESPONSE_URL = GMOVIES_IPAY88_RECIEVEOK_RESPONSE_URL
LCT_IPAY88_PAYMENT_COMPLETE_URL   = GMOVIES_IPAY88_PAYMENT_COMPLETE_URL
