import logging

from google.appengine.ext import deferred, ndb
from google.appengine.datastore.datastore_query import Cursor

from gmovies import models, orgs
from gmovies.heuristics.movies import get_searchable_words

BATCH_SIZE=100

# Update schema. One-off
def update_schema(cursor=None, num_updated=0, num_deleted=0, num_tx_affected=0):
    cur = Cursor(urlsafe=cursor)
    
    scheds, next_cur, more = models.Schedule.query().fetch_page(BATCH_SIZE, start_cursor=cur)

    batch_new = []
    affected_tx = []
    batch_del = []

    eligible_scheds = filter(lambda s: 'movie' in s._properties, scheds)

    for s in eligible_scheds:
        new_schema_sched = models.Schedule.convert_schedule(s)
        s_tx_list = models.ReservationTransaction.query(models.ReservationTransaction.reservations.schedule == s.key).fetch()
        for tx in s_tx_list:
            remap_schedules(tx, s.key, new_schema_sched.key)

        batch_new.append(new_schema_sched)
        affected_tx.extend(s_tx_list)
        batch_del.append(s.key)

    if batch_new:
        ndb.put_multi(affected_tx)
        ndb.put_multi(batch_new)
        ndb.delete_multi(batch_del)
        num_updated += len(batch_new)
        num_deleted += len(batch_del)
        num_tx_affected += len(affected_tx)

        logging.info("... tally: updated %d, deleted %d (affected tx: %d)", num_updated, num_deleted, num_tx_affected)

    if more:
        deferred.defer(update_schema, next_cur.urlsafe(), num_updated, num_deleted, num_tx_affected)
    else:
        logging.info("Schema migration completed, total new/updated %d, old schedules removed %d, tx affected %d", 
                     num_updated, num_deleted, num_tx_affected)


def remap_schedules(tx, s_key, new_schema_sched_key):
    for r in tx.reservations:
        if r.schedule == s_key:
            r.schedule = new_schema_sched_key


def update_schema_models(cls, cursor='', number_updated=0):
    cursor = ndb.Cursor(urlsafe=cursor)
    queryset = cls.query()
    queryset_batch, next_cursor, more = queryset.fetch_page(BATCH_SIZE, start_cursor=cursor)

    if cls == models.Movie:
        for ent in queryset_batch:
            ent.searchable_words = get_searchable_words(ent.canonical_title)

            if ent.rockwell_movie_ids:
                rockwell_feed_key = ndb.Key(models.Feed, str(orgs.ROCKWELL_MALLS))

                for correlation_id in ent.rockwell_movie_ids:
                    corr = models.FeedCorrelation(movie_id=str(correlation_id),
                            org_key=rockwell_feed_key, variant=None)

                    if corr not in ent.movie_correlation:
                        ent.movie_correlation.append(corr)
    elif cls == models.TicketTemplateImageBlob:
        for ent in queryset_batch:
            if not ent.template_type:
                ent.template_type = 'movie'

    ndb.put_multi(queryset_batch)
    number_updated += len(queryset_batch)

    logging.info('... tally, updated %s...' % str(number_updated))

    if more:
        deferred.defer(update_schema_models, cls, cursor=next_cursor.urlsafe(), number_updated=number_updated)
    else:
        logging.info('Update schema completed, total number updated %s...' % str(number_updated))
