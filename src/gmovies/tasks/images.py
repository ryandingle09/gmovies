import logging
from gmovies import models, orgs, exceptions
from gmovies.util import blobstore as blobstore_util
from google.appengine.ext import ndb
from uuid import uuid4 as uuid_gen
import StringIO, Image, math


density_resolution = {'mdpi': 1.0, 'hdpi': 1.5, 'hidpi': 2.0, 'xhdpi': 2.25}


# Deprecated. 05/25/2015
# Will remove in the future, because it uses the deprecated Files API.
# def transform_images(image_source, org_key, target_density):
#     theater_org = org_key.get()
#     if theater_org is None:
#         raise exceptions.EntityNotFoundError('Theater Organization does not exist')
#     else:
#         org_density = theater_org.image_density
#
#     scale_factor = compute_scale_factor(org_density, target_density)
#
#     (blob_info, original_image) = blobstore_util.get_image(image_source.image)
#     filename = blob_info.filename
#     original_image_type = blob_info.content_type
#
#     (width, height) = original_image.size
#     width = int(math.ceil(width * scale_factor))
#     height = int(math.ceil(height * scale_factor))
#
#     new_image = original_image.resize((width, height))
#
#     image_file=StringIO.StringIO()
#
#     logging.debug("Saving as %s (%s)" % (filename, original_image_type))
#
#     # HACK: We use the second half of the MIME type as the format
#     image_format = original_image_type.split('/')[1]
#
#     new_image.save(image_file, format=image_format)
#     blob_key = blobstore_util.write_to_blobstore(image_file.getvalue(), original_image_type)
#
#     multires_blob = models.MultiResImageBlob()
#     multires_blob.resolution = target_density
#     multires_blob.image = blob_key
#     multires_blob.key = ndb.Key(models.MultiResImageBlob, str(uuid_gen()), parent=image_source.key.parent())
#     multires_blob.put()
#
#     return multires_blob


def transform_poster_images(image_source, image_filename, image_density, target_density):
    scale_factor = compute_scale_factor(image_density, target_density)

    (gcs_info, original_image) = blobstore_util.get_image_gcs_info(image_filename, folder='posters/')
    filename = gcs_info.filename.split('/')[-1].split('~')[-1]
    original_image_type = gcs_info.content_type

    (width, height) = original_image.size
    width = int(math.ceil(width * scale_factor))
    height = int(math.ceil(height * scale_factor))

    new_image = original_image.resize((width, height))
    image_file = StringIO.StringIO()
    image_format = original_image_type.split('/')[1]

    new_image.save(image_file, format=image_format)

    logging.debug("Saving as %s (%s)" % (filename, original_image_type))

    if (image_source.is_default or not image_source.key.parent() or
            image_source.key.parent() is None):
        queryset = models.PosterImageBlob.query(
                models.PosterImageBlob.resolution == target_density,
                models.PosterImageBlob.orientation == image_source.orientation,
                models.PosterImageBlob.application_type == image_source.application_type,
                models.PosterImageBlob.is_default == image_source.is_default)
    else:
        queryset = models.PosterImageBlob.query(
                models.PosterImageBlob.resolution == target_density,
                models.PosterImageBlob.orientation == image_source.orientation,
                models.PosterImageBlob.application_type == image_source.application_type,
                models.PosterImageBlob.is_background == image_source.is_background,
                ancestor=image_source.key.parent())

    if queryset.count() != 0:
        image_blob = queryset.get()
    else:
        image_blob = models.PosterImageBlob()

        if (image_source.is_default or not image_source.key.parent() or
                image_source.key.parent() is None):
            image_blob.key = ndb.Key(models.PosterImageBlob, str(uuid_gen()))
        else:
            image_blob.key = ndb.Key(models.PosterImageBlob, str(uuid_gen()),
                    parent=image_source.key.parent())

        image_blob.resolution = target_density
        image_blob.orientation = image_source.orientation
        image_blob.application_type = image_source.application_type
        image_blob.is_background = image_source.is_background
        image_blob.is_default = image_source.is_default

    filename = image_blob.key.id() + '~' + filename

    blob_key = blobstore_util.write_to_blobstore_using_gcs(image_file.getvalue(),
            filename, original_image_type, folder='posters/')

    image_blob.image = blob_key

    image_blob.put()

    return image_blob


def compute_scale_factor(source, target):
    source = source.lower()
    target = target.lower()
    if source not in density_resolution or target not in density_resolution:
        raise ValueError
    scale_factor = density_resolution[target] / density_resolution[source]
    return scale_factor
