import logging
import pickle
log = logging.getLogger(__name__)

from datetime import datetime

from google.appengine.api import memcache
from google.appengine.ext import deferred, ndb
from google.appengine.datastore.datastore_query import Cursor

from gmovies import models, orgs
from gmovies.feeds.sureseats import read_schedules as read_sureseats_schedules
from gmovies.heuristics.schedules import get_movie_entity, query_theater_with_code, parse_screening

BATCH_SIZE=100
FSCK_CACHE_SCHEDULES_FEED='fsck_schedules::schedules_feed'
FSCK_CACHE_TX_SCHED_MAPPINGS='fsck_schedules::tx_sched_mappings'

def fsck_schedules_start():
    log.info('Fetching source feed data')
    sched_list_from_feed = read_sureseats_schedules()

    # Convert sched_list_from_feed to dict (key: id)
    sched_mappings = dict([ (str(sched['id']), sched) 
                            for sched in sched_list_from_feed ])

    memcache.set(FSCK_CACHE_SCHEDULES_FEED, pickle.dumps(sched_mappings))

    # Get oldest screening date
    oldest_screening = sorted([ parse_screening(sched['screening']) 
                                for sched in sched_list_from_feed ])[0].date()
    log.info('Deleting schedules older than %s with no related transactions', oldest_screening)
    q1 = models.Schedule.query(models.Schedule.show_date < oldest_screening)
    q2 = models.ReservationTransaction.query().fetch(projection=['reservations.schedule'])

    tx_with_scheds = [ tx for tx in q2 ]
    tx_scheds = set([ r.schedule for tx in tx_with_scheds for r in tx.reservations ])
    old_scheds = set(q1.fetch(keys_only=True))
    scheds_to_del = old_scheds - tx_scheds
    log.debug("scheds_to_del: %d schedules (%d old scheds - %d scheds w/tx)", len(scheds_to_del), len(old_scheds), len(tx_scheds))
    ndb.delete_multi(scheds_to_del)

    log.debug('Building reverse mapping (sched -> tx)')
    tx_sched_mappings = {}
    for tx in tx_with_scheds:
        for r in tx.reservations:
            tx_sched_mappings[r.schedule] = tx.key

    memcache.set(FSCK_CACHE_TX_SCHED_MAPPINGS, pickle.dumps(tx_sched_mappings))

    log.info('Starting consistency check for schedules...')
    deferred.defer(fsck_schedules)

def fsck_schedules(cursor=None,
                   counts=dict(schedules_scanned=0, schedules_created=0, schedule_deleted=0,
                               slots_scanned=0, slots_relocated=0,
                               slot_pruned_tx_schedule=0,
                               slots_corrected=0, slots_skipped=0)):

    sched_mappings = pickle.loads(memcache.get(FSCK_CACHE_SCHEDULES_FEED))
    tx_sched_mappings = pickle.loads(memcache.get(FSCK_CACHE_TX_SCHED_MAPPINGS))
    cur = Cursor(urlsafe=cursor)

    scheds, next_cur, more = models.Schedule.query().fetch_page(BATCH_SIZE, start_cursor=cur)

    batch_to_put = []
    batch_to_del = []
    for s in scheds:
        parent_theater = s.key.parent().get()
        slots_to_remove = []
        counts['schedules_scanned'] += 1
        tx_backref = None
        if s.key in tx_sched_mappings:
            tx_backref_key = tx_sched_mappings[s.key]
            tx_backref = tx_backref_key.get()
            tx_time_reservation = dict([ (r.time, r)
                                         for r in tx_backref.reservations ])

        for slot in s.slots:
            feed_code = slot.feed_code
            counts['slots_scanned'] += 1
            if feed_code in sched_mappings:
                feed_sched = sched_mappings[feed_code]

                movie = get_movie_entity(orgs.AYALA_MALLS, feed_sched['movie_title'])
                theater_code = feed_sched['theater_code']
                theater = query_theater_with_code(theater_code)

                show_date = parse_screening(feed_sched['screening']).date()
                if show_date != s.show_date:
                    log.warn("Mismatch between feed screening and schedule show date; skipping! (expected: %s, was: %s)", s.show_date, show_date)
                    continue

                if theater.key != parent_theater.key:
                    log.info("Slot %s in schedule %s misassigned to wrong parent theater schedule (expected: %s, was: %s); reassigning...", 
                             slot, s.key.id(), theater.key.id(), parent_theater.key.id())
                    is_new, new_sched_key = relocate_slot(batch_to_put, theater.key, s, slot)
                    counts['slots_relocated'] += 1
                    if is_new:
                        counts['schedules_created'] += 1
                    slots_to_remove.append(slot)
                else:
                    if not movie.key == slot.movie:
                        log.info("Mismatch movie for %s, updating", slot)
                        slot.movie = movie.key
                        counts['slots_corrected'] += 1
            else:
                if not tx_backref:
                    # log.warn("Skipping schedule %s slot %s with feed code %s, not in feed", s.key.id(), slot, feed_code)
                    counts['slots_skipped'] += 1
                else:
                    # Check that this slot is referenced by the transaction
                    if slot.start_time in tx_time_reservation:
                        reservation = tx_time_reservation[slot.start_time]
                        if reservation.theater != parent_theater.key:
                            log.info("Slot %s in schedule %s thought to be misassigned to wrong parent theater schedule (inferred from TX: %s, was: %s); reassigning...", 
                                     slot, s.key.id(), reservation.theater.id(), parent_theater.key.id())
                            is_new, new_sched_key = relocate_slot(batch_to_put, reservation.theater, s, slot)
                            counts['slots_relocated'] += 1
                            if is_new:
                                counts['schedules_created'] += 1
                            reservation.schedule = new_sched_key
                            if tx_backref not in batch_to_put:
                                batch_to_put.append(tx_backref)
                            slots_to_remove.append(slot)
                    else:
                        counts['slot_pruned_tx_schedule'] += 1
                        slots_to_remove.append(slot)

        if len(s.slots) == 0:
            counts['schedule_deleted'] += 1
            batch_to_del.append(s.key)
        elif slots_to_remove:
            for slot in slots_to_remove:
                s.slots.remove(slot)
            batch_to_put.append(s)

    ndb.put_multi(batch_to_put)
    ndb.delete_multi(batch_to_del)
    if more:
        log.debug("counts: %r", counts)
        deferred.defer(fsck_schedules, next_cur.urlsafe(), counts)
    else:
        log.info("Consistency check on schedules completed. Final counts: %r", counts)

def relocate_slot(batch_to_put, new_parent_theater_key, old_schedule, slot):
    new_schedule = models.Schedule.find_schedule(new_parent_theater_key, old_schedule.cinema, 
                                                 datetime.combine(old_schedule.show_date, slot.start_time))

    if new_schedule:
        assert new_schedule != old_schedule, 'Failed sanity check, got old schedule instance!'
        assert new_schedule.key != old_schedule.key, 'Failed sanity check, got a schedule instance of the same key!'
        is_new = False
    else:
        new_schedule = models.Schedule.create_schedule(cinema=old_schedule.cinema, show_date=old_schedule.show_date,
                                                       parent=new_parent_theater_key)
        logging.info('... creating new schedule for relocated slot')
        is_new = True

    new_schedule.slots.append(slot)
    batch_to_put.append(new_schedule)
    return is_new, new_schedule.key


__FSCK_MODEL_METHODS = {
    'schedule': fsck_schedules_start
}

def entity_fsck(model_name):
    if model_name in __FSCK_MODEL_METHODS:
        __FSCK_MODEL_METHODS[model_name]()
        return True

    return False


