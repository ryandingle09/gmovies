from __future__ import absolute_import

from datetime import datetime

from google.appengine.ext.ndb import Key
from gmovies.settings import ROTTEN_TOMATOES_API_KEY
from gmovies.models import Feed, Movie
from gmovies.orgs import ROTTEN_TOMATOES
from rottentomatoes import RT as rt_api

import logging
log = logging.getLogger(__name__)


RT = rt_api(ROTTEN_TOMATOES_API_KEY)
PAGE_LIMIT = 50

def get_movies():
    list_type = ['in_theaters','opening','upcoming']
    all_rt_movies = []
    for t in list_type:
        movie_list = get_movie_list(t)
        all_rt_movies.extend(movie_list)
    return all_rt_movies


def get_rt_movie_ratings():
    movies = Movie.query()
    rt_org_key = Key(Feed, str(ROTTEN_TOMATOES))
    movie_list = []
    for m in movies:
        movie_ids = [ corr.movie_id for corr in m.movie_correlation 
                      if corr.org_key == rt_org_key ]
        if len(movie_ids) == 1 and movie_ids[0].strip() != '':
            movie_id = movie_ids[0]
            ratings_dict = get_movie_ratings(movie_id)
            movie_dict = { 'id': movie_id,
                           'movie_title': m.canonical_title,
                           'ratings': ratings_dict }
            movie_list.append(movie_dict)

    return movie_list


def update_movie_ratings(movie_key, uuid):
    org_key = ndb.Key(Feed, str(uuid))

    movie_entity = movie_key.get()
    for correlation in movie_entity.movie_correlation:
        if correlation.org_key == org_key and uuid == orgs.ROTTEN_TOMATOES:
            ratings_dict = rottentomatoes.get_movie_ratings(correlation.movie_ids)
            build_ratings_property(movie_entity, ratings_dict, uuid)
            movie_entity.put()
        elif correlation.org_key == org_key and uuid == orgs.AYALA_MALLS:
            #get ratings from ayala
            pass

    return movie_entity
        

def get_movie_list(list_type):
    movie_list = []
    # query to retrieve the total entries from the feed
    movies_dict = RT.lists('movies', list_type, country='PH')
    if movies_dict:
        if 'total' in movies_dict:
            total = movies_dict['total']
        else:
            total = PAGE_LIMIT
        pages = compute_number_of_pages(total, PAGE_LIMIT)
        for i in range(pages):
            movies_dict = RT.lists('movies', list_type, page_limit=PAGE_LIMIT, page=(i+1))
            movies_dict_list = movies_dict['movies']
            movies_map_list = [ map_to_dictionary(m) for m in movies_dict_list ]
            movie_list.extend(movies_map_list)
    return movie_list


def get_movie_ratings(movie_id):
    critics_rating = None
    audience_rating = None
    critics_comment = None
    audience_comment = None
    movie_dict = RT.info(str(movie_id))
    if 'ratings' in movie_dict:
        critics_rating = movie_dict['ratings']['critics_score']
        audience_rating = movie_dict['ratings']['audience_score']
        critics_comment = movie_dict['ratings'].get('critics_rating', None)
        audience_comment = movie_dict['ratings'].get('audience_rating', None)
    return {'critics_rating': critics_rating, 'audience_rating': audience_rating, 'critics_comment': critics_comment, 'audience_comment': audience_comment }


def map_to_dictionary(movie):
    movie_dict = {}
    movie_dict['id'] = movie['id']
    movie_dict['movie_title'] = movie['title']
    movie_dict['cast'] = [ cast['name'] for cast in movie['abridged_cast'] ]
    movie_dict['synopsis'] = movie['synopsis']
    movie_dict['runtime'] = movie['runtime']
    movie_dict['image_url'] = movie['posters']['original']
    release_dates = movie['release_dates']
    if release_dates and 'theater' in release_dates:
        movie_dict['release_date'] = parse_date(release_dates['theater'])
    return movie_dict


# 50 is the max number of entries per page
def compute_number_of_pages(total, page_limit):
    return (total / page_limit) + ((total % page_limit) > 0)


def parse_date(date_str):
    return datetime.strptime(date_str, '%Y-%m-%d').date()


##########
#
# Fetching RT Details for Movie Title Bank (January 8, 2015)
#
##########

def get_movie_details():
    list_type = ['in_theaters','opening','upcoming']
    all_rt_movies = []

    for t in list_type:
        movie_list = get_movie_details_by_list_type(t)
        all_rt_movies.extend(movie_list)

    return all_rt_movies


def get_movie_details_by_list_type(list_type):
    movie_list = []
    movies_dict = RT.lists('movies', list_type, country='PH') # query to retrieve the total entries from the feed

    if movies_dict:
        if 'total' in movies_dict:
            total = movies_dict['total']
        else:
            total = PAGE_LIMIT

        pages = compute_number_of_pages(total, PAGE_LIMIT)

        for i in range(pages):
            movies_dict = RT.lists('movies', list_type, country='PH', page_limit=PAGE_LIMIT, page=(i+1))
            movies_dict_list = movies_dict['movies']
            movie_list.extend(movies_dict_list)

    return movie_list


def search_rottentomatoes_details(title):
    movie_list = []
    total = RT.search(title, 'total') # query to retrieve the total entries from the feed
    pages = compute_number_of_pages(total, PAGE_LIMIT)

    for i in range(pages):
        movies_dict = RT.search(title, page_limit=PAGE_LIMIT, page=(i+1))
        movie_list.extend(movies_dict)

    return movie_list