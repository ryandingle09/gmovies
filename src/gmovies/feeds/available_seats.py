import logging
import requests

from lxml import etree

from gmovies import orgs
from gmovies.exceptions import BadValueException
from gmovies.feeds import rockwell, greenhills, mgi, megaworld, cinema76, globeevents


SURESEATS_ENDPOINT_PRIMARY = 'http://api.sureseats.com/globe.asp'
MEGAWORLD_MGI = str(orgs.MEGAWORLD_MALLS) + '-MGI'
MGI_INTEGRATED_CINEMAS = [str(orgs.SM_MALLS), str(orgs.ROBINSONS_MALLS)]

log = logging.getLogger(__name__)


def sureseats_get_available_seats(theater_code, schedule_id): 
    log.info('Fetching Available Seats for Sureseats...')

    remaining_seats = 0

    try:
        r = requests.get(SURESEATS_ENDPOINT_PRIMARY,
                params={'action': 'AVAIL_SEAT', 'code': theater_code,
                        'id': schedule_id})

        if r.status_code != 200:
            return 'error', [], remaining_seats

        data = str(r.text)

        if not data.strip():
            return 'error', [], remaining_seats

        xml = etree.fromstring(data)
        avail_raw = xml.xpath('/Cinema/Seatmap/available/text()')

        if avail_raw:
            avail_raw = avail_raw[0].split(';')
            remaining_seats = len(avail_raw[:-1])

            return 'success', avail_raw[:-1], remaining_seats
        else:
            return 'success', [], remaining_seats
    except etree.XMLSyntaxError, e:
        log.warn("ERROR!, XMLSyntaxError...")
        log.error(e)

        return 'error', [], remaining_seats
    except Exception, e:
        log.warn("ERROR!, unexpected error...")
        log.error(e)

        return 'error', [], remaining_seats


AVAILABLE_SEATS_FEEDMAP = {str(orgs.AYALA_MALLS): sureseats_get_available_seats,
        str(orgs.ROCKWELL_MALLS): rockwell.get_available_seats_powerplantmall,
        str(orgs.GREENHILLS_MALLS): greenhills.get_available_seats_greenhills,
        str(orgs.CINEMA76_MALLS): cinema76.get_available_seats_cinema76,
        str(orgs.GLOBE_EVENTS): globeevents.get_available_seats_globeevents,
        str(orgs.SM_MALLS): mgi.get_available_seats,
        str(orgs.ROBINSONS_MALLS): mgi.get_available_seats,
        MEGAWORLD_MGI: mgi.get_available_seats}


# Sureseats: arg1: theater_code, arg2: schedule_id
# Rockwell: arg1: Rockwell's movie_schedule_id, arg2: Rockwell's showtime_id
# SM Malls: arg1: theater object, arg2: schedule_id, arg4: is_multiple_branch(use for mgi integrated cinemas)
# Megaworld MGI: arg1: theater object, arg2: schedule_id, arg3: third-party backend
def get_available_seats(org_id, arg1, arg2, arg3='', arg4=False):
    available_seats_feedmap_key = org_id

    if arg3 and arg3 is not None:
        available_seats_feedmap_key = available_seats_feedmap_key + '-' + arg3

    if available_seats_feedmap_key not in AVAILABLE_SEATS_FEEDMAP:
        raise BadValueException('theater', 'Theater Organization does not support querying available seats')

    if available_seats_feedmap_key in MGI_INTEGRATED_CINEMAS:
        return AVAILABLE_SEATS_FEEDMAP[available_seats_feedmap_key](arg1, arg2, arg4)

    return AVAILABLE_SEATS_FEEDMAP[available_seats_feedmap_key](arg1, arg2)
