import logging
import re

from lxml import etree
from requests import api

from google.appengine.ext import ndb

from gmovies import models, orgs
from gmovies.util import admin


TIMEOUT_DEADLINE = 45
SCHEDULE_URL = 'http://api.sureseats.com/globe.asp?action=SCHEDULE'

log = logging.getLogger(__name__)


def read_theaters():
    theater_list = []

    try:
        result = api.get(SCHEDULE_URL, timeout=TIMEOUT_DEADLINE)

        if result.status_code == 200:
            xml_data = etree.fromstring(result.content)

            for sched in xml_data.iter('Schedule'):
                theater_name = sched.find('theater').text
                theater_code = sched.find('theater_code').text
                cinema_name = sched.find('cinema').text
                theater = get_existing_theater(theater_list, theater_name, theater_code)

                if theater:
                    log.debug("read_theaters, found, theater_name: %s, theater_code: %s..." % (theater_name, theater_code))

                    if theater not in theater_list:
                        theater_list.append(theater)

                    create_cinema(theater, theater_code, cinema_name)
                else:
                    log.debug("read_theaters, not found, theater_name: %s, theater_code: %s..." % (theater_name, theater_code))

                    theater = create_new_theater(theater_name, theater_code)
                    cinema = create_cinema(theater, theater_code, cinema_name)
                    theater_list.append(theater)
    except (etree.XMLSyntaxError), e:
        log.warn("ERROR!, read_theaters, etree.XMLSyntaxError...")
        log.error(e)
    except Exception, e:
        log.warn("ERROR!, read_theaters...")
        log.error(e)

    if len(theater_list) != 0:
        ndb.put_multi(theater_list)

    return theater_list

def get_existing_theater(theater_list, theater_name, theater_code):
    theaters_pending = dict([(t.name, t) for t in theater_list])

    if theater_name in theaters_pending:
        return theaters_pending[theater_name]

    parent_key = ndb.Key(models.TheaterOrganization, str(orgs.AYALA_MALLS))
    q = models.Theater.query(ndb.OR(models.Theater.name==theater_name, models.Theater.org_theater_code==theater_code), ancestor=parent_key)

    if q.count() > 0:
        return q.fetch(1)[0]

    return None

def create_new_theater(theater_name, theater_code):
    theater = models.Theater(name=theater_name, org_theater_code=theater_code, parent=ndb.Key(models.TheaterOrganization, str(orgs.AYALA_MALLS)))

    return theater

def create_cinema(theater, theater_code, name):
    seat_count, seat_map = get_seat_count(theater_code, name)

    if name in [c.name for c in theater.cinemas]:
        cinema = admin.get_cinema_entity(theater, name)
        cinema.seat_count = str(seat_count)
        cinema.seat_map = seat_map

        log.debug("UPDATE!, via cinema_name with name: %s..." % name)
    else:
        cinema = models.Cinema()
        cinema.name = name
        cinema.seat_count = str(seat_count)
        cinema.seat_map = seat_map

        theater.cinemas.append(cinema)

        log.debug("CREATE!, cinema with name: %s..." % name)

def get_seat_count(code, cinema):
    seat_count = 0
    seat_map = []

    try:
        if code == 'ATC' and cinema in ['1']:
            log.warn("ATC!, get_seat_count, add suffix 1, update code, %s, cinema, %s..." % (code, cinema))
            code = '%s1' % code
        elif code == 'ATC' and cinema in ['2', '3', '4', '5']:
            log.warn("ATC!, get_seat_count, add suffix 2, update code, %s, cinema, %s..." % (code, cinema))
            code = '%s2' % code

        SEATS_URL = 'http://api.sureseats.com/globe.asp?action=SEATMAP&code=%s&id=%s' % (code, cinema)
        result = api.get(SEATS_URL, timeout=TIMEOUT_DEADLINE)

        if result.status_code == 200:
            xml_data = etree.fromstring(result.content)

            for seatmap in xml_data.iter('Seatmap'):
                column_count, rows = parse_rowmap(seatmap.find('rowmap').text, seatmap.find('rows').text)
                seat_count += column_count
                seat_map.append(rows)
    except etree.XMLSyntaxError, e:
        log.warn("ERROR!, get_seat_count, etree.XMLSyntaxError...")
        log.error(e)

        # error, reset the values for seat_count and seat_map.
        seat_count = 0
        seat_map = []
    except Exception, e:
        log.warn("ERROR!, get_seat_count...")
        log.error(e)

        # error, reset the values for seat_count and seat_map.
        seat_count = 0
        seat_map = []

    return seat_count, seat_map

def parse_rowmap(rowmap, row):
    results = map(lambda s: s.strip(), rowmap.split(';'))
    rowmap = [(row + i if i.isdigit() else i) for i in results if i != '']

    return len(rowmap), rowmap