import datetime
import json
import logging
import traceback

from google.appengine.api import urlfetch
from google.appengine.ext import ndb

from gmovies import models, orgs
from gmovies.settings import CINEMA76_ENDPOINT_PRIMARY
from gmovies.util import admin, parse_cast
from gmovies import heuristics
from gmovies.orgs import CINEMA76_MALLS
from .util import map_movies


log = logging.getLogger(__name__)

CINEMA76_MOVIES_FEED_URL = CINEMA76_ENDPOINT_PRIMARY + 'movies?movieCategoryId=%s'
CINEMA76_CINEMAS_FEED_URL = CINEMA76_ENDPOINT_PRIMARY + 'cinemas'
CINEMA76_SCHEDULES_FEED_URL = CINEMA76_ENDPOINT_PRIMARY + 'movie-schedules?upcoming=1&showingDate=%s'
CINEMA76_SEATS_STATUS_FEED_URL = CINEMA76_ENDPOINT_PRIMARY + 'movie-schedules/seats?movieScheduleId=%s&showtimeId=%s'
CINEMA76_SEATS_FREE_SEATING_FEED_URL = CINEMA76_ENDPOINT_PRIMARY + 'movie-schedules/seats-free-seating?movieScheduleId=%s&showtimeId=%s'

TIMEOUT_DEADLINE = 45
THEATER_MOVIE_CODE = 'C76'


def read_movies_feed_cinema76(feed_url, movie_category_id):
    movies = []

    try:
        feed_url = feed_url % str(movie_category_id)
        result = urlfetch.fetch(feed_url, deadline=TIMEOUT_DEADLINE)

        log.debug("read_movies_feed_cinema76, feed_url: %s..." % feed_url)

        if result.status_code != 200:
            log.warn("ERROR, read_movies_feed_cinema76, status_code, not equal to 200...")

            return movies

        json_data = json.loads(result.content)

        for movie in json_data:
            movie_dict = {}
            movie_dict['id'] = '%s::%s' % (THEATER_MOVIE_CODE, movie['id'])
            movie_dict['movie_title'] = movie['title']

            # modified for force schedule movie mappings
            api_title = movie['title']
            api_movie_id = '%s::%s' % (THEATER_MOVIE_CODE, movie['id'])
            map_movies(api_title, api_movie_id, str(orgs.CINEMA76_MALLS))

            if 'sysnopsis' in movie:
                movie_dict['synopsis'] = movie['sysnopsis']

            if 'casts' in movie:
                movie_dict['cast'] = parse_cast(movie['casts'])

            if 'tentativeDate' in movie:
                release_date = admin.parse_string_to_date(movie['tentativeDate'])
                movie_dict['release_date'] = release_date.date() if release_date else None

            if 'runningTime' in movie:
                movie_dict['runtime_mins'] = float(movie['runningTime']) if movie['runningTime'] else 0.0

            if 'rating' in movie:
                if 'rate' in movie['rating']:
                    movie_dict['advisory_rating'] = movie['rating']['rate']

            movies.append(movie_dict)
    except Exception, e:
        log.warn("ERROR!, read_movies_feed_cinema76...")
        log.error(traceback.format_exc(e))

    log.debug("movie_list: %s" % (movies))
    return movies

def read_movies_nowshowing_cinema76():
    log.debug("read_movies_nowshowing_cinema76, fetching now showing movies from %s..." % CINEMA76_MOVIES_FEED_URL)

    return read_movies_feed_cinema76(CINEMA76_MOVIES_FEED_URL, 2)

def read_movies_comingsoon_cinema76():
    log.debug("read_movies_comingsoon_cinema76, fetching coming soon movies from %s..." % CINEMA76_MOVIES_FEED_URL)

    return read_movies_feed_cinema76(CINEMA76_MOVIES_FEED_URL, 1)

def read_cinemas_feed_cinema76(theater_code):
    theater_list = []

    try:
        theaterorg_key = ndb.Key(models.TheaterOrganization, str(orgs.CINEMA76_MALLS))
        theaterorg = theaterorg_key.get()

        if not theaterorg:
            log.warn("ERROR, read_cinemas_feed_cinema76, theaterorg not found...")

            return theater_list

        theaters = models.Theater.query(models.Theater.org_theater_code==theater_code, ancestor=theaterorg_key).fetch()

        if len(theaters) != 1:
            log.warn("ERROR!, read_cinemas_feed_cinema76, len(theaters) conflicts, not equal to 1...")

            return theater_list

        result = urlfetch.fetch(CINEMA76_CINEMAS_FEED_URL, deadline=TIMEOUT_DEADLINE)

        log.debug("read_cinemas_feed_cinema76, feed_url: %s..." % CINEMA76_CINEMAS_FEED_URL)

        if result.status_code != 200:
            log.warn("ERROR!, read_cinemas_feed_cinema76, status_code, not equal to 200...")

            return theater_list

        theater = theaters[0]
        theater_name = theater.name.replace('Greenhills', '').strip()
        json_data = json.loads(result.content)

        for cinema in json_data:
            cinema_dict = {}
            # cinema_name = cinema['name'].strip().split(' ')[-1].strip()
            cinema_name = cinema['name'].replace(theater_name, '').strip()
            cinema_feed_code = str(cinema['id'])
            cinema_feed_name = cinema['name']
            seat_count, seat_map, cinema_feed_seatmap = create_seatmap_cinema76(cinema['cinemaSeats'])

            # if theater_name not in cinema_feed_name:
                # log.warn("SKIP!, read_cinemas_feed_cinema76, feed cinema_name not found...")
                # log.info("GAE theater_name: %s, FEED cinema_name: %s..." % (theater_name, cinema_feed_name))

                # continue

            if cinema_feed_code in [c.feed_code for c in theater.cinemas]:
                cinema = admin.get_cinema_entity_using_feed_code(theater, cinema_feed_code)
                cinema.name = cinema_name
                cinema.seat_count = str(seat_count)
                cinema.seat_map = seat_map
                cinema.feed_cinema_name = cinema_feed_name
                cinema.feed_seat_map = cinema_feed_seatmap

                log.debug("UPDATE!, via cinema_feed_code with cinema_name: %s, cinema_feed_code: %s..." % (cinema_name, cinema_feed_code))
            elif cinema_name in [c.name for c in theater.cinemas]:
                cinema = admin.get_cinema_entity(theater, cinema_name)
                cinema.seat_count = str(seat_count)
                cinema.seat_map = seat_map
                cinema.feed_code = cinema_feed_code
                cinema.feed_cinema_name = cinema_feed_name
                cinema.feed_seat_map = cinema_feed_seatmap

                log.debug("UPDATE!, via cinema_name with cinema_name: %s, cinema_feed_code: %s..." % (cinema_name, cinema_feed_code))
            else:
                cinema = models.Cinema()
                cinema.name = cinema_name
                cinema.seat_count = str(seat_count)
                cinema.seat_map = seat_map
                cinema.feed_code = cinema_feed_code
                cinema.feed_cinema_name = cinema_feed_name
                cinema.feed_seat_map = cinema_feed_seatmap
                theater.cinemas.append(cinema)

                log.debug("CREATE!, cinema with cinema_name: %s, cinema_feed_code: %s..." % (cinema_name, cinema_feed_code))

        theater.put()
        theater_list.append(theater)
    except Exception, e:
        log.warn("ERROR!, read_cinemas_feed_cinema76...")
        log.error(e)

    return theater_list

def create_seatmap_cinema76(seatmap):
    seat_count = 0
    seat_map = []
    seats_matrix = []

    for row in seatmap:
        seat_row = []
        seats_matrix_row = []
        row_name = None
        row_id = row['id']
        seats = row['seats']

        if 'rowName' in row:
            row_name = row['rowName']

        for column in seats:
            seat_column = {}
            seat_name = None
            column_name = None
            seat_id = column['id']
            column_number = column['columnNumber']
            for_seating = column['forSeating']

            if 'name' in column:
                column_name = column['name']

            if for_seating and row_name and column_name:
                seat_count += 1
                seat_name = row_name.strip() + column_name.strip()
                seat_row.append(seat_name)
            else:
                seat_row.append('b(%s)' % column['columnNumber'])

            seat_column['row_id'] = row_id
            seat_column['row_name'] = row_name
            seat_column['seat_id'] = seat_id
            seat_column['column_name'] = column_name
            seat_column['column_number'] = column_number
            seat_column['seat_name'] = seat_name
            seat_column['for_seating'] = for_seating
            seats_matrix_row.append(seat_column)

        seat_map.append(seat_row)
        seats_matrix.append(seats_matrix_row)

    return seat_count, seat_map, seats_matrix

def read_schedules_feed_cinema76():
    schedules = []

    try:
        theaterorg_key = ndb.Key(models.TheaterOrganization, str(orgs.CINEMA76_MALLS))
        theaterorg = theaterorg_key.get()

        if not theaterorg:
            log.warn("ERROR, read_schedules_feed_cinema76, theaterorg not found...")

            return theater_list

        theaters = models.Theater.query(ancestor=theaterorg_key).fetch()

        if len(theaters) == 0:
            log.warn("ERROR!, read_schedules_feed_cinema76, len(theaters) conflicts, no existing theaters...")

            return theater_list

        feed_url = CINEMA76_SCHEDULES_FEED_URL % str(datetime.datetime.now().date())
        result = urlfetch.fetch(feed_url, deadline=TIMEOUT_DEADLINE)

        log.debug("read_schedules_feed_cinema76, feed_url: %s..." % feed_url)

        if result.status_code != 200:
            log.warn("ERROR!, read_schedules_feed_cinema76, status_code, not equal to 200...")

            return schedules

        json_data = json.loads(result.content)

        for schedule in json_data:
            for show_time in schedule['showtimes']:
                schedule_dict = {}
                theater_code = None
                # theater_code = 'CFS'
                cinema_feed_name = schedule['cinema']['name']
                screening_datetime = '%s %s' % (schedule['showingDate'], show_time['screenTime'])

                for theater in theaters:
                    theater_name = theater.name.replace('Cinema 76', '').strip()

                    # if theater_name in cinema_feed_name:
                        # log.debug("read_schedules_feed_cinema76, theater_code found...")
                        # log.info("GAE theater_name: %s, FEED cinema_name: %s..." % (theater_name, cinema_feed_name))

                    theater_code = theater.org_theater_code

                schedule_dict['id'] = str(show_time['id'])
                schedule_dict['schedule_id'] = str(schedule['id'])
                schedule_dict['movie_id'] = '%s::%s' % (THEATER_MOVIE_CODE, schedule['movie']['id'])
                schedule_dict['movie_title'] = schedule['movie']['title']
                schedule_dict['cinema_id'] = str(schedule['cinema']['id'])
                schedule_dict['cinema_name'] = schedule['cinema']['name'].strip().split(' ')[-1].strip()
                schedule_dict['show_date'] = schedule['showingDate']
                schedule_dict['screening'] = admin.parse_datetime_to_string(admin.parse_string_to_date(screening_datetime, fdate='%Y-%m-%d %H:%M:%S'))
                schedule_dict['seat_type'] = rename_seating_type(show_time['seatingType'])
                schedule_dict['variant'] = schedule['displayTechnology']['name']
                schedule_dict['price'] = str(show_time['price'])
                schedule_dict['theater_code'] = theater_code
                schedule_dict['uuid'] = orgs.CINEMA76_MALLS
                schedules.append(schedule_dict)
    except Exception, e:
        log.warn("ERROR!, read_schedules_feed_cinema76...")
        log.error(e)

    return schedules

def get_available_seats_cinema76(movie_schedule_code, showtimeid_seatingtype):
    remaining_seats = 0
    available_seats = []
    showtime_id, seating_type = showtimeid_seatingtype.split('::')

    try:
        if seating_type in ['Free Seating', 'Guaranteed Seats']:
            feed_url = CINEMA76_SEATS_FREE_SEATING_FEED_URL % (movie_schedule_code, showtime_id)
            result = urlfetch.fetch(feed_url, deadline=TIMEOUT_DEADLINE)

            log.debug("get_available_seats_cinema76, FREE SEATING, feed_url: %s..." % feed_url)

            if result.status_code != 200:
                log.warn("ERROR!, get_available_seats_cinema76, FREE SEATING, status_code, not equal to 200...")

                return 'error', available_seats, remaining_seats

            if result.content:
                remaining_seats = int(result.content)
        else:
            feed_url = CINEMA76_SEATS_STATUS_FEED_URL % (movie_schedule_code, showtime_id)
            result = urlfetch.fetch(feed_url, deadline=TIMEOUT_DEADLINE)

            log.debug("get_available_seats_cinema76, RESERVED SEATING, feed_url: %s..." % feed_url)

            if result.status_code != 200:
                log.warn("ERROR!, get_available_seats_cinema76, RESERVED SEATING, status_code, not equal to 200...")

                return 'error', available_seats, remaining_seats

            row_name = None
            column_name = None
            json_data = json.loads(result.content)

            for row in json_data:
                row_name = row['rowName'] if 'rowName' in row else None

                for column in row['seats']:
                    for_seating = column['forSeating']
                    column_name = column['name'] if 'name' in column else None

                    if for_seating and row_name and column_name:
                        seat_name = row_name.strip() + column_name.strip()
                        availability = int(column['availability']) if 'availability' in column else None

                        if availability == 1:
                            available_seats.append(seat_name)

            remaining_seats = len(available_seats)

        return 'success', available_seats, remaining_seats
    except Exception, e:
        log.warn("ERROR!, get_available_seats_cinema76...")
        log.error(e)

    return 'error', available_seats, remaining_seats

def rename_seating_type(seating_type):
    log.debug("rename_seating_type, seating_type: %s..." % seating_type)

    if seating_type.upper() == 'SELECT':
        seating_type = 'Reserved Seating'
    elif seating_type.upper() == 'FREE':
        seating_type = 'Guaranteed Seats'

    return seating_type