
import traceback
import json
import logging
import urllib

from gmovies import heuristics
from gmovies import models
from google.appengine.ext import ndb

log = logging.getLogger(__name__)

def map_movies(real_movie_title, api_movie_id, uuid, variants=None):
    try:
        api_title = real_movie_title
        log.debug("movie_feed_map_id : %s" % (api_movie_id))
        log.debug("movie_title : %s" % (api_title))
        log.debug("movie_variant : %s" % (variants))

        searchable_words = heuristics.movies.get_searchable_words(api_title)
        log.debug("searchable_words : %s" % (searchable_words))
        log.debug("searchable_words_count : %s" % (len(searchable_words)))

        if len(searchable_words) is not 0:
            queryset = models.Movie.query(models.Movie.searchable_words.IN(searchable_words), models.Movie.is_inactive==False).order(models.Movie.key)

            for m in queryset:
                db_title = m.canonical_title.lower()
                api_title = str(api_title).lower()

                if db_title == api_title:
                    log.debug("matching nearest title in: db_title: %s and api_title: %s" % (db_title, api_title))

                    q = models.Movie.query()
                    q = q.filter(models.Movie.canonical_title==m.canonical_title)

                    if q.count() == 1:
                        data = q.fetch(1)[0]

                        log.debug("title : %s" % (data.canonical_title))
                        log.debug("old_movie_correlation : %s" % (data.movie_correlation))

                        check_movie = heuristics.schedules.get_movie_entity_by_feed_correlation(uuid, api_movie_id)

                        if not check_movie or check_movie is None:
                            log.warn("Movie ID is not yet mapped in this movie so force mapped it now.")

                            rt_key = ndb.Key(models.Feed, str(uuid))
                            data.movie_correlation.append(models.FeedCorrelation(org_key=rt_key, movie_id=api_movie_id, variant=variants))
                            data.put()

                            log.debug("new_movie_correlation : %s" % (data.movie_correlation))
                            
                        log.debug("DB_DATA: %s" % (q))

        else:
            log.warn("title_may_break_query: %s" % (api_title))
            log.warn("Cannot execute query for mapping schedules, searchable_words is empty")

    except Exception, e:
        log.warn("ERROR!, map_movies...")
        log.error(traceback.format_exc(e))