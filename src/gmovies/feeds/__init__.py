from __future__ import absolute_import

import json
import logging

from google.appengine.api import urlfetch


log = logging.getLogger(__name__)

TOTAL_REVIEWS_URL = 'http://gmovies.ph:8080/endpoints/reviews-ratings/total-reviews%s'
AVERAGE_RATING_URL = 'http://gmovies.ph:8080/endpoints/reviews-ratings/average-rating%s'

TIMEOUT_DEADLINE = 30

# Temporary, will remove this in the future.
def get_total_reviews(movie_ids=[]):
    total_reviews = {}

    try:
        params = '?'

        for movie_id in movie_ids:
            params += 'ids[]=%s&' % str(movie_id.id())

        feed_url = TOTAL_REVIEWS_URL % params
        result = urlfetch.fetch(feed_url, deadline=TIMEOUT_DEADLINE)

        log.info("Fetch total reviews from %s" % feed_url)

        if result.status_code == 200:
            json_data = json.loads(result.content)

            if 'result' in json_data:
                total_reviews = json_data['result']

        log.info("Total Reviews: {}".format(total_reviews))

        return total_reviews
    except Exception, e:
        log.error("ERROR!, Failed to fetch the total reviews. %s" % TOTAL_REVIEWS_URL)
        log.error(e)

        return total_reviews


# Temporary, will remove this in the future.
def get_average_rating(movie_ids=[]):
    average_rating = {}

    try:
        params = '?'

        for movie_id in movie_ids:
            params += 'ids[]=%s&' % str(movie_id.id())

        feed_url = AVERAGE_RATING_URL % params
        result = urlfetch.fetch(feed_url, deadline=TIMEOUT_DEADLINE)

        log.info("Fetch average rating from %s" % feed_url)

        if result.status_code == 200:
            json_data = json.loads(result.content)

            if 'result' in json_data:
                average_rating = json_data['result']

        log.info("Average Rating: {}".format(average_rating))

        return average_rating
    except Exception, e:
        log.error("ERROR!, Failed to fetch the average rating. %s" % AVERAGE_RATING_URL)
        log.error(e)

        return average_rating