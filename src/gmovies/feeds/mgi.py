import datetime
import logging
import traceback

from google.appengine.ext import ndb

from gmovies import models, orgs, settings
from gmovies.util import admin, parse_cast
from gmovies.ws.soap_connector import DataWSMGi, EPlusWSMGi, TransactionsWSMGi
from gmovies.ws.endpoints import (LCT_SCHEDULES_API, LCT_SESSION_API, LCT_TRANSACTION_API,
        SM_EPLUS_API, SM_SCHEDULES_API, SM_SESSION_API, SM_TRANSACTION_API,
        ROBINSONS_SCHEDULES_API, ROBINSONS_SESSION_API, ROBINSONS_TRANSACTION_API)


log = logging.getLogger(__name__)

MGW_LCT_DICTKEY = '%s::LCT' % str(orgs.MEGAWORLD_MALLS)
SCHEDULE_ENDPOINTS = {str(orgs.SM_MALLS): SM_SCHEDULES_API, str(orgs.ROBINSONS_MALLS): ROBINSONS_SCHEDULES_API, MGW_LCT_DICTKEY: LCT_SCHEDULES_API}
SESSION_ENDPOINTS = {str(orgs.SM_MALLS): SM_SESSION_API, str(orgs.ROBINSONS_MALLS): ROBINSONS_SESSION_API, MGW_LCT_DICTKEY: LCT_SESSION_API}
TRANSACTION_ENDPOINTS = {str(orgs.SM_MALLS): SM_TRANSACTION_API, str(orgs.ROBINSONS_MALLS): ROBINSONS_TRANSACTION_API, MGW_LCT_DICTKEY: LCT_TRANSACTION_API}
MGI_USERNAMES = {str(orgs.SM_MALLS): settings.SM_USERNAME_MGI, str(orgs.ROBINSONS_MALLS): settings.ROBINSONS_USERNAME_MGI, MGW_LCT_DICTKEY: settings.LCT_USERNAME_MGI}
MGI_PASSWORDS = {str(orgs.SM_MALLS): settings.SM_PASSWORD_MGI, str(orgs.ROBINSONS_MALLS): settings.ROBINSONS_PASSWORD_MGI, MGW_LCT_DICTKEY: settings.LCT_PASSWORD_MGI}
THEATER_MOVIE_CODE_CONCAT = {str(orgs.SM_MALLS): 'SM', str(orgs.ROBINSONS_MALLS): 'RMW', MGW_LCT_DICTKEY: 'MGW'}
FEED_CODES = {str(orgs.SM_MALLS): 'SM Malls', str(orgs.ROBINSONS_MALLS): 'Robinsons Malls', MGW_LCT_DICTKEY: 'Megaworld Malls'}

SCREENING_TYPE = {'1': 'Reserved Seating', '2': 'Guaranteed Seats', '3': 'Free Seating'}
SEAT_STATUS = {'0': 'AVAILABLE', '1': 'TAKEN', '2': 'RESERVED', '3': 'HOUSESEAT', '4': 'IGNORED'}
MOVIES_SHOWING_TYPE = {'ALL': 0, 'NOWSHOWING': 1, 'NEXTATTRACTION': 2, 'COMINGSOON': 3}


def read_movie_feed(showing_type, endpoint_dictkey, is_mutiple_branch=False):
    movies = []

    if endpoint_dictkey in SCHEDULE_ENDPOINTS:
        SCHEDULES_API = SCHEDULE_ENDPOINTS[endpoint_dictkey]
        THEATER_MOVIE_CODE = THEATER_MOVIE_CODE_CONCAT[endpoint_dictkey]
        FEED_CODE = FEED_CODES[endpoint_dictkey]
    else:
        log.warn("read_movie_feed, SKIP!, Theater Organization UUID ----- doesn't exist...")

        return movies

    try:
        mgi_data = DataWSMGi(SCHEDULES_API)

        if FEED_CODE in ['Robinsons Malls']:
            movies = mgi_data.get_movies_with_schedules2(showing_type, THEATER_MOVIE_CODE, days_delta=30)
        elif FEED_CODE in ['SM Malls']:
            movies = mgi_data.get_movies_with_schedules2(showing_type, THEATER_MOVIE_CODE, days_delta=90)
        elif FEED_CODE in ['Megaworld Malls']:
            movies = mgi_data.get_movies_by_showing_type(showing_type, THEATER_MOVIE_CODE)
    except Exception, e:
        log.warn('ERROR!, Fetching movies for ----- (MGI backend)...')
        log.error(e)

    return movies

def read_movies(showing_type, theaterorg_uuid, theater_code=None, is_mutiple_branch=False):
    ENDPOINT_DICTKEY = str(theaterorg_uuid)

    if not is_mutiple_branch and theater_code:
        ENDPOINT_DICTKEY = '%s::%s' % (ENDPOINT_DICTKEY, theater_code)

    return read_movie_feed(showing_type, ENDPOINT_DICTKEY, is_mutiple_branch=is_mutiple_branch)

def read_theaters(theaterorg_uuid, theater_code=None, is_mutiple_branch=False):
    theater_list = []
    theaterorg_key = ndb.Key(models.TheaterOrganization, str(theaterorg_uuid))
    theaterorg = theaterorg_key.get()
    ENDPOINT_DICTKEY = str(theaterorg_uuid)

    if not theaterorg:
        log.warn("SKIP!, Theater Organization ----- doesn't exist...")

        return theater_list

    if not is_mutiple_branch and theater_code:
        ENDPOINT_DICTKEY = '%s::%s' % (ENDPOINT_DICTKEY, theater_code)

    if ENDPOINT_DICTKEY in SCHEDULE_ENDPOINTS and ENDPOINT_DICTKEY in TRANSACTION_ENDPOINTS:
        MGI_USERNAME = MGI_USERNAMES[ENDPOINT_DICTKEY]
        MGI_PASSWORD = MGI_PASSWORDS[ENDPOINT_DICTKEY]
        SCHEDULES_API = SCHEDULE_ENDPOINTS[ENDPOINT_DICTKEY]
        SESSION_API = SESSION_ENDPOINTS[ENDPOINT_DICTKEY]
        TRANSACTION_API = TRANSACTION_ENDPOINTS[ENDPOINT_DICTKEY]
        FEED_CODE = FEED_CODES[ENDPOINT_DICTKEY]
    else:
        log.warn("read_theaters, SKIP!, Theater Organization UUID ----- doesn't exist...")

        return theater_list

    try:
        mgi_data = DataWSMGi(SCHEDULES_API)
        mgi_transactions = TransactionsWSMGi(TRANSACTION_API, SESSION_API, MGI_USERNAME, MGI_PASSWORD)

        if FEED_CODE in ['SM Malls', 'Robinsons Malls']:
            branches = mgi_data.get_branches()
        elif FEED_CODE in ['Megaworld Malls']:
            branches = mgi_data.get_branches_with_schedules()
        else:
            log.warn("SKIP!, Get Branches, FEED_CODE: %s doesn't exist..." % FEED_CODE)

            return theater_list

        if theater_code:
            branches = filter(lambda b: b['branch_code'] == theater_code, branches)

            if len(branches) != 1:
                log.warn("FIXME!, conflicts on branches with theater_code: %s, len(branches): %s..." % (theater_code, len(branches)))

                return theater_list

        for branch in branches:
            branch_key = branch['branch_key']
            branch_code = branch['branch_code']
            branch_name = '%s %s' % (branch['branch_company'], branch['branch_name'])
            branch_name = branch_name.strip()
            theater = models.Theater.query(models.Theater.org_theater_code==branch_code, ancestor=theaterorg_key).get()

            if not theater:
                log.warn("SKIP!, Theater with branch_code %s doesn't exist..." % branch_code)

                continue

            theater.branch_id = str(branch_key)

            if FEED_CODE in ['SM Malls', 'Robinsons Malls']:
                #force key 59 to change to key 3 for sm maison
                if branch_code == 'SMAI' or branch_key == '59':
                    branch_key = '3'
                    
                branch_cinemas = mgi_transactions.get_branch_cinemas(branch_key) # MGi's GetBranchCinemas are inside the Transaction API.
            elif FEED_CODE in ['Megaworld Malls']:
                branch_cinemas = mgi_transactions.get_cinema_lookup_key(branch_key) # MGi's GetCinemaLookUpKey are inside the Transaction API.
            else:
                log.warn("SKIP!, Get Branch Cinemas, FEED_CODE: %s doesn't exist..." % FEED_CODE)

                break

            for branch_cinema in branch_cinemas:
                cinema_key = branch_cinema['cinema_key']
                cinema_code = branch_cinema['cinema_code']
                cinema_name = branch_cinema['cinema_name']
                cinema_max_row = branch_cinema['max_row']
                cinema_max_column = branch_cinema['max_column']
                cinema_feed_code = '%s::%s' % (cinema_key, cinema_code)
                cinema_feed_name = branch_cinema['cinema_name']

                # override the cinema feed_code and cinema_name.
                if FEED_CODE in ['Megaworld Malls']:
                    cinema_feed_code = str(branch_cinema['cinema_code'])
                    cinema_name = cinema_name.replace('Cinema', '').strip()

                seat_count, seat_map, cinema_seat_map = create_cinema_seatmap(branch_key, cinema_key, cinema_max_row,
                        cinema_max_column, FEED_CODE, TRANSACTION_API, SESSION_API, MGI_USERNAME, MGI_PASSWORD)

                if cinema_feed_code in [c.feed_code for c in theater.cinemas]:
                    cinema = admin.get_cinema_entity_using_feed_code(theater, cinema_feed_code)
                    cinema.name = cinema_name
                    cinema.seat_count = str(seat_count)
                    cinema.seat_map = seat_map
                    cinema.feed_cinema_name = cinema_feed_name
                    cinema.feed_seat_map = cinema_seat_map

                    log.info("UPDATE!, via cinema_feed_code with branch_code: %s, cinema_feed_code: %s..." % (branch_code, cinema_feed_code))
                elif cinema_name in [c.name for c in theater.cinemas]:
                    cinema = admin.get_cinema_entity(theater, cinema_name)
                    cinema.seat_count = str(seat_count)
                    cinema.seat_map = seat_map
                    cinema.feed_code = cinema_feed_code
                    cinema.feed_cinema_name = cinema_feed_name
                    cinema.feed_seat_map = cinema_seat_map

                    log.info("UPDATE!, via cinema_name with branch_code: %s, cinema_feed_code: %s..." % (branch_code, cinema_feed_code))
                else:
                    cinema = models.Cinema()
                    cinema.name = cinema_name
                    cinema.seat_count = str(seat_count)
                    cinema.seat_map = seat_map
                    cinema.feed_code = cinema_feed_code
                    cinema.feed_cinema_name = cinema_feed_name
                    cinema.feed_seat_map = cinema_seat_map
                    theater.cinemas.append(cinema)

                    log.info("CREATE!, cinema with branch_code: %s, cinema_feed_code %s..." % (branch_code, cinema_feed_code))

            theater_list.append(theater)
    except Exception, e:
        log.warn('ERROR!, Fetching theater cinemas for ----- (MGI backend)...')
        log.error(e)

    ndb.put_multi(theater_list)

    return theater_list

def create_cinema_seatmap(branch_key, cinema_key, max_row, max_column, feed_code, feed_url, session_url, mgi_username, mgi_password):
    seat_count = 0
    seat_map = []
    seats_matrix = []

    try:
        mgi_transactions = TransactionsWSMGi(feed_url, session_url, mgi_username, mgi_password)

        if feed_code in ['SM Malls', 'Robinsons Malls']:
            seat_count, seats_matrix = mgi_transactions.get_seat_map2(branch_key, cinema_key,
                    max_row, max_column) # MGi's GetSeatMap2 are inside the Transaction API.
        elif feed_code in ['Megaworld Malls']:
            seat_count, seats_matrix = mgi_transactions.get_seat_map(branch_key, cinema_key,
                    max_row, max_column) # MGi's GetSeatMap are inside the Transaction API.
        else:
            log.warn("SKIP!, Get Seat Map, FEED_CODE: %s doesn't exist..." % feed_code)

            return seat_count, seat_map, seats_matrix

        for row in seats_matrix:
            seat_row = []

            for column in row:
                seat_name = column['seat_name']

                if not seat_name:
                    seat_name = 'b(%s)' % column['column']

                seat_row.append(seat_name)

            seat_map.append(seat_row)
    except Exception, e:
        log.warn('ERROR!, Fetching cinema seat map for ----- (MGI backend)...')
        log.error(e)

    return seat_count, seat_map, seats_matrix


def read_schedules(branch_key, theaterorg_uuid, theater_code, days_delta=1, is_mutiple_branch=False):
    schedules_list = []
    ENDPOINT_DICTKEY = str(theaterorg_uuid)

    log.debug("ROBINSONS_THEATER_DETAILS: %s, %s, %s" % (branch_key, theater_code, days_delta))

    if not is_mutiple_branch:
        ENDPOINT_DICTKEY = '%s::%s' % (ENDPOINT_DICTKEY, theater_code)

    if ENDPOINT_DICTKEY in SCHEDULE_ENDPOINTS:
        SCHEDULES_API = SCHEDULE_ENDPOINTS[ENDPOINT_DICTKEY]
        THEATER_MOVIE_CODE = THEATER_MOVIE_CODE_CONCAT[ENDPOINT_DICTKEY]
    else:
        log.warn("read_schedules, SKIP!, Theater Organization UUID ----- doesn't exist...")

        return schedules_list

    try:
        today = datetime.date.today()
        mgi_data = DataWSMGi(SCHEDULES_API)

        for i in range(days_delta):
            showing_date = today + datetime.timedelta(days=i)
            movies = mgi_data.get_movies_by_branch(branch_key, showing_date, THEATER_MOVIE_CODE)
            
            log.debug("ROBINSONS_MOVIE_LIST: %s" % (movies))

            schedules = mgi_data.get_schedules_by_branch(theaterorg_uuid, branch_key, theater_code, showing_date, movies)
            schedules_list.extend(schedules)
    except Exception, e:
        log.warn('ERROR!, Fetching schedules for ----- (MGI backend)...')
        log.error(traceback.format_exc(e))

    log.debug("ROBINSONS_SCHEDULE_LIST: %s" % (schedules_list))
    return schedules_list


def read_schedules2(branch_key, theaterorg_uuid, theater_code, days_delta=1, is_mutiple_branch=False):
    schedules_list = []
    ENDPOINT_DICTKEY = str(theaterorg_uuid)

    if not is_mutiple_branch:
        ENDPOINT_DICTKEY = '%s::%s' % (ENDPOINT_DICTKEY, theater_code)

    if ENDPOINT_DICTKEY in SCHEDULE_ENDPOINTS:
        SCHEDULES_API = SCHEDULE_ENDPOINTS[ENDPOINT_DICTKEY]
        THEATER_MOVIE_CODE = THEATER_MOVIE_CODE_CONCAT[ENDPOINT_DICTKEY]
    else:
        log.warn("read_schedules2, SKIP!, Theater Organization UUID ----- doesn't exist...")

        return schedules_list

    try:
        today = datetime.date.today()
        mgi_data = DataWSMGi(SCHEDULES_API)

        for i in range(days_delta):
            showing_date = today + datetime.timedelta(days=i)
            movies = mgi_data.get_movies_by_branch(branch_key, showing_date, THEATER_MOVIE_CODE)

            for movie in movies:
                schedules = mgi_data.get_schedules_by_branch_and_movie(theaterorg_uuid, branch_key, theater_code, showing_date, movie)
                schedules_list.extend(schedules)
    except Exception, e:
        log.warn('ERROR!, Fetching schedules2 for ----- (MGI backend)...')
        log.error(e)

    return schedules_list


def get_available_seats(theater, schedule_key, is_mutiple_branch=False):
    remaining_seats = 0
    available_seats = []
    theater_code = theater.org_theater_code
    ENDPOINT_DICTKEY = str(theater.key.parent().id())

    if not is_mutiple_branch:
        ENDPOINT_DICTKEY = '%s::%s' % (ENDPOINT_DICTKEY, theater_code)

    if ENDPOINT_DICTKEY in TRANSACTION_ENDPOINTS:
        MGI_USERNAME = MGI_USERNAMES[ENDPOINT_DICTKEY]
        MGI_PASSWORD = MGI_PASSWORDS[ENDPOINT_DICTKEY]
        SESSION_API = SESSION_ENDPOINTS[ENDPOINT_DICTKEY]
        TRANSACTION_API = TRANSACTION_ENDPOINTS[ENDPOINT_DICTKEY]
    else:
        log.warn("get_available_seats, SKIP!, Theater Organization UUID ----- doesn't exist...")

        return 'error', available_seats, remaining_seats

    try:
        branch_key = int(theater.branch_id)
        schedule_key = int(schedule_key)
        mgi_transactions = TransactionsWSMGi(TRANSACTION_API, SESSION_API, MGI_USERNAME, MGI_PASSWORD)
        status, schedule_seats = mgi_transactions.get_schedule_seats(branch_key, schedule_key) # MGi's GetScheduleSeats are inside the Transaction API.

        if schedule_seats:
            screening_type = getattr(schedule_seats, 'ScreeningType', None)
            remaining_seats = getattr(schedule_seats, 'Available', 0)

            if screening_type is None:
                log.warn("SKIP!, ScreeningType attribute doesn't exist...")

                return 'error', available_seats, remaining_seats

            seating_type = SCREENING_TYPE[str(screening_type)]

            if seating_type == 'Reserved Seating':
                schedule_seats_seats = getattr(schedule_seats, 'Seats', None)

                if schedule_seats_seats is None and remaining_seats > 0:
                    log.warn("SKIP!, Seats attribute doesn't exist...")

                    return 'error', available_seats, remaining_seats

                seats_list = getattr(schedule_seats_seats, 'Seat', [])
                available = filter(lambda s: SEAT_STATUS[str(s.Status)] == 'AVAILABLE', seats_list)

                for seat in available:
                    available_seats.append(str(seat.Name))

        return status, available_seats, remaining_seats
    except Exception, e:
        log.warn('ERROR!, Fetching available seats for ----- (MGI backend)...')
        log.error(traceback.format_exc(e))

    return 'error', available_seats, remaining_seats


# get GMovies EPlus account balance for SM Cinemas via GCash.
def get_eplus_balance():
    results = []

    try:
        card_numbers = [settings.EPLUS_CARDNUMBER]
        eplus_connection = EPlusWSMGi(SM_EPLUS_API)

        for card_number in card_numbers:
            eplus_balance = eplus_connection.do_online_wallet_member_check_balance(SM_SESSION_API,
                    settings.SM_USERNAME_MGI, settings.SM_PASSWORD_MGI, card_number)

            if eplus_balance['status'] == 'success':
                account_balance = '%s: PHP %s' % (card_number, eplus_balance['message'])

                results.append(account_balance)
    except Exception, e:
        log.warn("ERROR! feeds(mgi.py), check_balance_eplus...")
        log.error(e)

    return results