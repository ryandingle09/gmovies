import json
import logging

from google.appengine.api import urlfetch
from google.appengine.ext import ndb

from gmovies import models, orgs


log = logging.getLogger(__name__)

HEROKU_SCHEDULE_URL = 'http://reelscrape.herokuapp.com/fetch/%s/origin'
# HEROKU_CTC_SCHEDULE_URL = 'http://reelscrape.herokuapp.com/fetch/%s/CTC'
HEROKU_CTC_SCHEDULE_URL = 'http://52.76.151.143:4000/fetch/%s/CTC' # moved scraper to aws instance
TIMEOUT_DEADLINE = 45


def scraper_read_schedules(org_uuid):
    all_schedules = []

    try:
        log.info("org_key: %s..." % str(org_uuid))

        parent_key = ndb.Key(models.TheaterOrganization, str(org_uuid))
        theaters = models.Theater.query(ancestor=parent_key).fetch(projection=['name', 'org_theater_code'])

        for theater in theaters:
            if theater.org_theater_code == 'LCT':
                continue

            feed_url = HEROKU_CTC_SCHEDULE_URL % theater.org_theater_code

            log.info("feed_url: %s..." % feed_url)

            result = urlfetch.fetch(feed_url, deadline=TIMEOUT_DEADLINE)

            if result.status_code == 200:
                json_data = json.loads(result.content)
                all_schedules += json_data

        return all_schedules
    except Exception, e:
        log.error(e)

        return all_schedules