import Image
import logging
import requests
import StringIO

from uuid import uuid4 as uuid_gen

from google.appengine.api import taskqueue
from google.appengine.ext import ndb

from gmovies import models
from gmovies.util import blobstore as blobstore_util
from gmovies.util.task import is_from_flixster


log = logging.getLogger(__name__)

ALLOWED_IMAGE_CONTENT_TYPE = ['image/png', 'image/jpg', 'image/jpeg', 'image/gif']


def save_poster_image(image_file, form, parent_key=None, resolution='uploaded', is_background=False, is_default=False):
    image_content_type = image_file.content_type
    application_type = form.application_type.data
    image_data = image_file.read()

    if is_default or not parent_key or parent_key is None:
        orientation = form.orientation.data
        queryset = models.PosterImageBlob.query(models.PosterImageBlob.resolution==resolution, models.PosterImageBlob.orientation==orientation,
                models.PosterImageBlob.application_type==application_type, models.PosterImageBlob.is_default==is_default)
    elif is_background:
        queryset = models.PosterImageBlob.query(models.PosterImageBlob.application_type==application_type,
                models.PosterImageBlob.is_background==is_background, ancestor=parent_key)
    else:
        orientation = form.orientation.data
        queryset = models.PosterImageBlob.query(models.PosterImageBlob.resolution==resolution, models.PosterImageBlob.orientation==orientation,
                models.PosterImageBlob.application_type==application_type, models.PosterImageBlob.is_background==False, ancestor=parent_key)

    if queryset.count() != 0:
        image_blob = queryset.get()
    else:
        image_blob = models.PosterImageBlob()

        if is_default or not parent_key or parent_key is None:
            image_blob.key = ndb.Key(models.PosterImageBlob, str(uuid_gen()))
        else:
            image_blob.key = ndb.Key(models.PosterImageBlob, str(uuid_gen()), parent=parent_key)

        if is_background:
            image_blob.orientation = 'landscape'
            image_blob.is_background = is_background
        else:
            image_blob.orientation = form.orientation.data

        image_blob.resolution = resolution
        image_blob.application_type = form.application_type.data
        image_blob.is_default = is_default

    image_filename = image_blob.key.id() + '~' + image_file.filename
    blob_key = blobstore_util.write_to_blobstore_using_gcs(image_data, image_filename, image_content_type, folder='posters/')
    image_blob.image = blob_key
    image_blob.put()

    if not is_background and application_type == 'mobile-app':
        for density in ['mdpi', 'hdpi', 'hidpi', 'xhdpi']:
            params = {'source_key': image_blob.key.urlsafe(), 'image_filename': image_filename, 'image_density': 'xhdpi', 'target_density': density}

            try:
                taskqueue.add(queue_name='highcpu', url='/tasks/fetch/poster_image/scaler/', method='POST', params=params)
            except (taskqueue.TaskAlreadyExistsError, taskqueue.TombstonedTaskError), e:
                log.warn("ERROR!, save_poster_image, scaling, taskqueue.TaskAlreadyExistsError or taskqueue.TombstonedTaskError...")
                log.error(e)
            except Exception, e:
                log.warn("ERROR!, save_poster_image, scaling...")
                log.error(e)

    return image_blob

def save_assets_image(image_file, assets_image_name, assets_group=None):
    queryset = models.AssetsImageBlob.query(models.AssetsImageBlob.name==assets_image_name, models.AssetsImageBlob.assets_group==assets_group)
    image_content_type = image_file.content_type
    image_data = image_file.read()

    if queryset.count() != 0:
        image_blob = queryset.get()
    else:
        image_blob = models.AssetsImageBlob()
        image_blob.key = ndb.Key(models.AssetsImageBlob, str(uuid_gen()))
        image_blob.name = assets_image_name
        image_blob.assets_group = assets_group

    image_filename = image_blob.key.id() + '~' + image_file.filename
    blob_key = blobstore_util.write_to_blobstore_using_gcs(image_data, image_filename, image_content_type, folder='assets/')
    image_blob.image = blob_key
    image_blob.put()

    return image_blob

def get_image_blob_key(entity, image_file, folder='assets/'):
    image_content_type = image_file.content_type
    image_data = image_file.read()
    image_filename = entity.key.id() + '~' + image_file.filename
    blob_key = blobstore_util.write_to_blobstore_using_gcs(image_data, image_filename, image_content_type, folder=folder)

    return blob_key