import logging

from datetime import datetime
from lxml import etree

from google.appengine.api import urlfetch

from gmovies.heuristics.movies import variant_from_title # XXX Bad form here; feeds shouldn't pull in from heuristics
from gmovies.orgs import AYALA_MALLS
from gmovies.util import parse_cast
from .util import map_movies

TIMEOUT_DEADLINE = 45
SCHEDULE_URL = 'http://api.sureseats.com/globe.asp?action=SCHEDULE'
NOW_SHOWING_URL = 'http://api.sureseats.com/globe.asp?action=NOWSHOWING'
COMING_SOON_URL = 'http://api.sureseats.com/globe.asp?action=COMINGSOON'

log = logging.getLogger(__name__)

urlfetch.set_default_fetch_deadline(15) # set urlfetch deadline limit to 15 seconds


# reads schedules from the Ayala's SureSeats API endpoint.
def read_schedules():
    all_schedules = []

    try:
        result = urlfetch.fetch(SCHEDULE_URL, deadline=TIMEOUT_DEADLINE)

        if result.status_code == 200:
            xml_data = etree.fromstring(result.content)

            for ia in xml_data.iter('Schedule'):
                schedule_dict = {}

                for elem in ia:
                    schedule_dict[elem.tag] = elem.text

                schedule_dict['cinema_code'] = schedule_dict['cinema']
                schedule_dict['cinema_name'] = schedule_dict['cinema']
                schedule_dict['price'] = parse_price(schedule_dict['price'])
                schedule_dict['variant'] = variant_from_title(schedule_dict['movie_title'])
                schedule_dict['uuid'] = AYALA_MALLS
                all_schedules.append(schedule_dict)
    except (etree.XMLSyntaxError, urlfetch.ResponseTooLargeError), e:
        log.warn("ERROR!, read_schedules, etree.XMLSyntaxError or urlfetch.ResponseTooLargeError...")
        log.error(e)
    except Exception, e:
        log.warn("ERROR!, read_schedules...")
        log.error(e)

    return all_schedules


def read_movie_feed(feed_url, root):
    movies = []

    try:
        result = urlfetch.fetch(feed_url, deadline=TIMEOUT_DEADLINE)

        if result.status_code == 200:
            xml_data = etree.fromstring(result.content)

            for ia in xml_data.iter(root):
                movie_dict = {}

                for elem in ia:
                    if elem.tag == 'picture':
                        movie_dict['image_url'] = elem.text
                    elif elem.tag == 'cast':
                        movie_dict['cast'] = parse_cast(elem.text)
                    elif elem.tag == 'tentative':
                        movie_dict['release_date'] = parse_date(elem.text)
                    else:
                        movie_dict[elem.tag] = elem.text

                movies.append(movie_dict)
    except (etree.XMLSyntaxError, urlfetch.ResponseTooLargeError), e:
        log.warn("ERROR!, read_movie_feed, etree.XMLSyntaxError or urlfetch.ResponseTooLargeError...")
        log.error(e)
    except Exception, e:
        log.warn("ERROR!, read_movie_feed...")
        log.error(e)

    # modified for force schedule movie mappings
    for movie in movies:
        api_title = movie['movie_title']

        log.debug("sureseats_movie_title : %s" % (api_title))
        
        variant = ''

        if '(2D/4DX)' in api_title:
            variant = '2D/4DX'
        elif '(3D/4DX)' in api_title:
            variant = '3D/4DX'
        elif '(ATMOS)' in api_title:
            variant = 'ATMOS'

        api_title = api_title.replace("(2D/4DX) ", "")
        api_title = api_title.replace("(3D/4DX) ", "")
        api_title = api_title.replace("(ATMOS) ", "")
        api_movie_id = movie['id']

        map_movies(api_title, api_movie_id, str(AYALA_MALLS), variant)
    # end

    log.debug('movie_list: %s' % (movies))
    return movies

def read_movies_now_showing():
    return read_movie_feed(NOW_SHOWING_URL, 'Now_Showing')

def read_movies_coming_soon():
    return read_movie_feed(COMING_SOON_URL, 'Coming_Soon')

def read_advisory_ratings_for_movies():
    scheds = read_schedules()
    movies_and_ratings = set([(s['movie_title'], s['rating']) for s in scheds])

    return [dict(movie_title=s[0], advisory_rating=s[1]) for s in movies_and_ratings]

def parse_date(date_str):
    return datetime.strptime(date_str, '%m/%d/%Y').date()

def parse_price(price):
    return price[4:]
    
