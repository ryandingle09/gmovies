################################################################################
#
# Deprecated (10/21/2016). Remove or delete this file in the future.
# Use the mgi.py for the cinema partners under MGi integration.
#
################################################################################


import datetime
import logging
import sys, traceback

from google.appengine.ext import ndb

from gmovies import models, orgs
from gmovies.util import admin, parse_cast
from gmovies.ws.connector import WSData, WSSeatInfo, WSTheaterInfo
from gmovies.ws.endpoints import LCT_SCHEDULES_API, LCT_TRANSACTION_API
from .util import map_movies


log = logging.getLogger(__name__)

###############
#
# Parsers for MGI backend
#
###############

def read_movies_mgi(movie_list, showing_type, branch_code):
    movies = []

    try:
        for movie in movie_list:
            movie_dict = {}
            movie_dict['id'] = movie['movie_code']
            movie_dict['movie_title'] = movie['movie_title']

            # modified for force schedule movie mappings
            api_title = movie['movie_title']
            api_movie_id = '%s::%s' % (theater_code, movie['movie_code'])
            map_movies(api_title, api_movie_id, str(orgs.MEGAWORLD_MALLS))

            if 'casts' in movie:
                movie_dict['cast'] = parse_cast(movie['casts'])

            if 'running_time' in movie:
                movie_dict['runtime_mins'] = float(movie['running_time'])

            if 'mtrcb_rating' in movie:
                movie_dict['advisory_rating'] = movie['mtrcb_rating']

            if 'synopsis' in movie:
                movie_dict['synopsis'] = movie['synopsis']

            movies.append(movie_dict)
    except Exception, e:
        log.error('ERROR!, Fetching Megaworld - %s (MGI backend) movies...' % branch_code)
        log.error(e)


    log.debug('movie_list: %s' % (movies))
    return movies

def read_movies_now_showing_mgi(branch_code):
    log.info('Fetching Megaworld - %s (MGI backend) now showing movies...' % branch_code)

    movie_list = []

    try:
        lct_data = WSData(LCT_SCHEDULES_API)

        if branch_code == 'LCT':
            movie_list = lct_data.get_movies_nowshowing()
    except Exception, e:
        log.error(e)
        log.error("ERROR!, Fetching Megaworld - %s (MGI backend) now showing movies..." % branch_code)

    return read_movies_mgi(movie_list, 'now_showing', branch_code)


def read_movies_coming_soon_mgi(branch_code):
    log.info('Fetching Megaworld - %s (MGI backend) coming soon movies...' % branch_code)

    movie_list = []

    try:
        lct_data = WSData(LCT_SCHEDULES_API)

        if branch_code == 'LCT':
            movie_list = lct_data.get_movies_comingsoon()
    except Exception, e:
        log.error(e)
        log.error("ERROR!, Fetching Megaworld - %s (MGI backend) coming movies..." % branch_code)

    return read_movies_mgi(movie_list, 'coming_soon', branch_code)


def read_theater_cinemas_mgi(branch_code):
    cinemas = []

    try:
        status = None
        cinema_list = []
        lct_data = WSData(LCT_SCHEDULES_API)
        lct_theater_info = WSTheaterInfo(LCT_TRANSACTION_API)
        branches = lct_data.get_branch_theaters(branch_code)
        branch_info = filter(lambda t: t['branch_code'] == branch_code, branches)
        theaterorg_key = ndb.Key(models.TheaterOrganization, str(orgs.MEGAWORLD_MALLS))

        if theaterorg_key.get():
            theater = models.Theater.query(
                    models.Theater.org_theater_code == branch_code,
                    ancestor=theaterorg_key).get()

            if theater is None:
                log.info("SKIP!, Theater with branch_code %s doesn't exist..." % branch_code)

                return cinemas
        else:
            log.info("SKIP!, Theater Organization Megaworld doesn't exist...")

            return cinemas

        if branch_code == 'LCT':
            status, cinema_list = lct_theater_info.get_cinemas(branch_info[0]['branch_key'])

        if status == 'success':
            theater.branch_id = str(branch_info[0]['branch_key'])

            for cinema_dict in cinema_list:
                seat_count, seat_map, feed_seat_map = create_cinema_seatmap_mgi(
                        branch_code, branch_info[0]['branch_key'],
                        cinema_dict['cinema_key'])
                cinema_id = cinema_dict['cinema_key']
                cinema_name = cinema_dict['cinema_name']
                cinema_code = cinema_dict['cinema_code']

                if cinema_id not in [c.feed_code for c in theater.cinemas]:
                    if cinema_code not in [c.name for c in theater.cinemas]:
                        cinema = models.Cinema(name=cinema_code,
                                seat_count=str(seat_count), seat_map=seat_map,
                                feed_code=cinema_id, feed_cinema_name=cinema_name,
                                feed_seat_map=feed_seat_map)

                        theater.cinemas.append(cinema)

                        log.info("CREATE!, %s - Cinema %s..." % (branch_code, cinema.name))
                    else:
                        cinema = admin.get_cinema_entity(theater, cinema_code)
                        cinema.seat_count = str(seat_count)
                        cinema.seat_map = seat_map
                        cinema.feed_code = cinema_id
                        cinema.feed_cinema_name = cinema_name
                        cinema.feed_seat_map = feed_seat_map

                        log.info("UPDATE!, %s - Cinema %s..." % (branch_code, cinema.name))
                else:
                    cinema = admin.get_cinema_entity_using_feed_code(theater, cinema_id)
                    cinema.seat_count = str(seat_count)
                    cinema.seat_map = seat_map
                    cinema.feed_cinema_name = cinema_name
                    cinema.feed_seat_map = feed_seat_map

                    log.info("UPDATE!, %s - Cinema %s..." % (branch_code, cinema.name))

            theater.put()
    except Exception, e:
        errtype, errinst, tb = sys.exc_info()
        traceback_desc = traceback.format_exception(errtype, errinst, tb)

        log.error('ERROR!, Fetching Megaworld - %s (MGI backend) cinemas...' % branch_code)
        log.info('line# %s: %s' % (str(sys.exc_traceback.tb_lineno), e))

    return cinemas

def create_cinema_seatmap_mgi(branch_code, branch_id, cinema_id):
    seat_map = []
    feed_seat_map = []
    seat_count = 0

    try:
        lct_theater_info = WSTheaterInfo(LCT_TRANSACTION_API)

        if branch_code == 'LCT':
            seat_plan_info = lct_theater_info.get_seatplan(branch_id, cinema_id)
            seats = seat_plan_info['seats']
            seat_count = seat_plan_info['seat_count']
        else:
            seats = []

        for row in seats:
            seat_row = []

            for column in row['row_seats']:
                seat_row.append(column['seat_name'])

            seat_map.append(seat_row)
            feed_seat_map.append(row['row_seats'])
    except Exception, e:
        log.error('ERROR!, Fetching Megaworld - %s (MGI backend) seatmaps...')
        log.error(e)
    return seat_count, seat_map, feed_seat_map

def read_schedules_mgi(branch_code, days_delta=1):
    schedules = []

    try:
        lct_data = WSData(LCT_SCHEDULES_API)

        if branch_code == 'LCT':
            schedule_info_list = lct_data.get_schedules(branch_code, days_delta)
        else:
            schedule_info_list = []

        for schedule_info in schedule_info_list:
            schedule_list = filter(lambda s: s['allow_purchase'] == True, schedule_info['schedules'])

            for schedule in schedule_list:
                schedule_dict = {}
                schedule_dict['id'] = str(schedule['schedule_id'])
                schedule_dict['seat_type'] = schedule['screening_type']
                schedule_dict['variant'] = schedule_info['variant']
                schedule_dict['screening'] = admin.parse_datetime_to_string(schedule['start_time'])
                schedule_dict['movie_id'] = schedule_info['movie_code']
                schedule_dict['movie_title'] = schedule_info['movie_name']
                schedule_dict['theater_code'] = branch_code
                schedule_dict['cinema_code'] = schedule['cinema_code']
                schedule_dict['cinema_name'] = schedule['cinema_name']
                schedule_dict['price'] = str(schedule['price'])
                schedule_dict['uuid'] = orgs.MEGAWORLD_MALLS

                schedules.append(schedule_dict)
    except Exception, e:
        log.error('ERROR!, Fetching Megaworld - %s (MGI backend) schedules...' % branch_code)
        log.error(e)

    return schedules


def megaworld_get_available_seats_mgi(theater, schedule_id):
    remaining_seats = 0
    available_seats = []
    theater_code = theater.org_theater_code

    try:
        schedule_info = {}
        seats_info = []
        branch_id = int(theater.branch_id)
        schedule_id = int(schedule_id)
        lct_seats_info = WSSeatInfo(LCT_TRANSACTION_API)

        if theater_code == 'LCT':
            status, schedule_info, seats_info = lct_seats_info.get_scheduleseats_info(
                    branch_id, schedule_id)
        else:
            log.warn('ERROR!, missing third party available seats endpoint...')

            status = 'error'

        available = filter(lambda x : x['status'] == 'AVAILABLE', seats_info)
        remaining_seats = schedule_info.get('seats_available', 0)

        for seat in available:
            available_seats.append(seat['seat_name'])

        return status, available_seats, remaining_seats
    except Exception, e:
        log.error('ERROR!, Fetching megaworld - %s (MGI backend) available seats...' % theater_code)
        log.error(e)

        return 'error', available_seats, remaining_seats
