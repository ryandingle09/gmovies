import datetime

from flask.ext.wtf import Form

from wtforms import (widgets, BooleanField, TextField, DecimalField, HiddenField, IntegerField,
        SelectMultipleField, DateField, TextAreaField, DateTimeField, ValidationError, SelectField,
        FieldList, FileField, FloatField, Field)
from wtforms.ext.appengine.ndb import model_form
from wtforms.validators import Required, Optional

from gmovies import models
from gmovies.bank_transfer import AccountSettings
from gmovies.settings import SystemSettings
from gmovies.util.admin import convert_string_to_time


MOVIE_FIELDS = [('movie_title', 'Movie Title'), ('cast', 'Cast'),
        ('genre', 'Genre'), ('variant', 'Variant'), ('runtime_mins', 'Runtime'),
        ('advisory_rating', 'Advisory Rating/Classification'),
        ('ratings', 'Ratings'), ('release_date', 'Release Date'),
        ('synopsis', 'Synopsis'), ('image_url', 'Image URL')]
MTRCB_ADVISORY_RATINGS = ['Rating not yet available', 'G', 'PG', 'R-13', 'R-16', 'R-18']
PAYMENT_OPTIONS = [('pesopay','PesoPay'), ('paynamics', 'Paynamics'), ('migs', 'MIGS'), ('ipay88','iPay88'),
        ('claim_code','Claim Code'), ('mpass','MPass'), ('eplus', 'EPlus'), ('gcash', 'GCash'), ('g_cash_app', 'GCash App'), ('cash','Cash'),
        ('globe_operator_billing', 'Operator Billing (Globe Subscribers)'), ('globe_my_rewards', 'Globe My Rewards'),
        ('grewards', 'GRewards'),
        ('bank_payment', 'Bank Payment'), ('hey_kuya', 'Hey Kuya'),
        ('bpi_cardholder_promo_pesopay', 'BPI Cardholder Promo (PesoPay)'),
        ('bpi_cardholder_promo_paynamics', 'BPI Cardholder Promo (Paynamics)'),
        ('citibank_cardholder_promo_pesopay', 'CitiBank Cardholder Promo (PesoPay)'),
        ('citibank_cardholder_promo_paynamics', 'CitiBank Cardholder Promo (Paynamics)'),
        ('pnb_cardholder_promo_pesopay', 'PNB Cardholder Promo (PesoPay)'),
        ('pnb_cardholder_promo_paynamics', 'PNB Cardholder Promo (Paynamics)'),
        ('bdo_cardholder_promo_pesopay', 'BDO Cardholder Promo (PesoPay)'),
        ('bdo_cardholder_promo_paynamics', 'BDO Cardholder Promo (Paynamics)'),
        ('g_cash_cardholder_promo_pesopay', 'GCash Cardholder Promo (PesoPay)'),
        ('g_cash_cardholder_promo_paynamics', 'GCash Cardholder Promo (Paynamics)'),
        ('metrobank_cardholder_promo_pesopay', 'Metrobank Cardholder Promo (PesoPay)'),
        ('metrobank_cardholder_promo_paynamics', 'Metrobank Cardholder Promo (Paynamics)'),
        ('chinabank_cardholder_promo_pesopay', 'Chinabank Cardholder Promo (PesoPay)'),
        ('chinabank_cardholder_promo_paynamics', 'Chinabank Cardholder Promo (Paynamics)'),
        ('unionbank_cardholder_promo_pesopay', 'Unionbank Cardholder Promo (PesoPay)'),
        ('unionbank_cardholder_promo_paynamics', 'Unionbank Cardholder Promo (Paynamics)'),
        ('robinsonsbank_cardholder_promo_pesopay', 'Robinsons Bank Cardholder Promo (PesoPay)'),
        ('robinsonsbank_cardholder_promo_paynamics', 'Robinsons Bank Cardholder Promo (Paynamics)')]
BLOCKED_PAYMENT_OPTIONS = [('blocked_screening_paynamics', 'Paynamics'), ('blocked_screening_claim_code', 'Claim Code'), ('promo_code', 'Promo Code')]
IMAGE_ORIENTATION = [('portrait', 'Portrait'), ('landscape', 'Landscape')]
IMAGE_APPLICATION_TYPE = [('mobile-app', 'Mobile Application'), ('website', 'Website')]
COMING_SOON_ASSETS_MONTHS = [('1', 'January'), ('2', 'February'), ('3', 'March'),
        ('4', 'April'), ('5', 'May'), ('6', 'June'), ('7', 'July'), ('8', 'August'),
        ('9', 'September'), ('10', 'October'), ('11', 'November'), ('12', 'December')]
PLATFORM_CHOICES = [('android', 'Android'), ('ios', 'iOS'), ('website', 'Website')]


class TimeField(Field):
    widget = widgets.TextInput()

    def _value(self):
        if self.data:
            return self.data.strftime('%H:%M')
        else:
            return ''

    def process_formdata(self, valuelist):
        if valuelist:
            self.data = [datetime.datetime.strptime(v, '%H:%M').time() for v in valuelist]

            if len(self.data) == 1:
                self.data = self.data[0]
        else:
            self.data = []

def whitelist_blacklist_widget(field, **kwargs):
    html = ['<label class="checkbox">%s%s</label>' % (subfield(**kwargs), subfield.label.text) for subfield in field]

    return widgets.HTMLString(''.join(html))

def payment_options_widget(field, **kwargs):
    html = ['<label class="checkbox">%s%s</label>' % (subfield(**kwargs), subfield.label.text) for subfield in field]

    return widgets.HTMLString(''.join(html))


TheaterForm = model_form(models.Theater, base_class=Form, exclude=['allow_reservation_platform',
        'branch_id', 'location', 'payment_options', 'payment_options_android', 'payment_options_website',
        'error_message_seatmaps_android', 'error_message_seatmaps_ios'])
TheaterForm.longitude = DecimalField('Longitude', places=7, validators=[Optional()])
TheaterForm.latitude = DecimalField('Latitude', places=7, validators=[Optional()])
TheaterForm.allow_reservation_platform = SelectMultipleField('Allow Reservation (Platform)',
        option_widget=widgets.CheckboxInput(), widget=payment_options_widget, choices=PLATFORM_CHOICES)
TheaterForm.payment_options = SelectMultipleField('Payment Options(iOS)',
        option_widget=widgets.CheckboxInput(), widget=payment_options_widget, choices=PAYMENT_OPTIONS)
TheaterForm.payment_options_android = SelectMultipleField('Payment Options(Android)',
        option_widget=widgets.CheckboxInput(), widget=payment_options_widget, choices=PAYMENT_OPTIONS)
TheaterForm.payment_options_website = SelectMultipleField('Payment Options(Website)',
        option_widget=widgets.CheckboxInput(), widget=payment_options_widget, choices=PAYMENT_OPTIONS)
TheaterForm.error_message_seatmaps_android = TextAreaField('Seat Maps Error Message(Android)')
TheaterForm.error_message_seatmaps_ios = TextAreaField('Seat Maps Error Message(iOS)')
TheaterForm.theater_image_file = FileField('Theater Image')

CinemaForm = model_form(models.Cinema, base_class=Form, exclude=['location', 'seat_map',
        'rockwell_cinema_id', 'feed_code', 'feed_cinema_name', 'feed_seat_map'])
CinemaForm.longitude = DecimalField('Longitude', places=7, validators=[Optional()])
CinemaForm.latitude = DecimalField('Latitude', places=7, validators=[Optional()])
CinemaForm.seat_map = SelectMultipleField('Seat Rows',option_widget=widgets.CheckboxInput(),widget=widgets.ListWidget(prefix_label=False))
CinemaForm.addseats = TextField('Additional Seat Row')

MovieForm = model_form(models.Movie, base_class=Form, exclude=['cast', 'advisory_rating', 'release_date',
        'is_showing', 'runtime_mins', 'slottime_mins', 'rockwell_movie_ids', 'alternate_posters', 'searchable_words'])
MovieForm.is_expired = BooleanField('Expired')
MovieForm.cast_list = TextField('Cast')
MovieForm.advisory_rating = SelectField('Advisory Rating/Classification', choices=zip(MTRCB_ADVISORY_RATINGS, MTRCB_ADVISORY_RATINGS))
MovieForm.release_date = DateField('Release Date', format='%m/%d/%Y')
MovieForm.runtime_mins = FloatField('Runtime', validators=[Optional()])
MovieForm.rt_mapping = TextField('Rotten Tomatoes ID')
MovieForm.ticket_template_image_file = FileField('Ticket Template Image (768 x 1024)')
MovieForm.critics_score = TextField('Critics Score (0-100)')
MovieForm.audience_score = TextField('Audience Score (0-100)')
MovieForm.critics_rating = TextField('Critics Rating (e.g Certified Fresh)')
MovieForm.audience_rating = TextField('Audience Score (e.g Upright)')

ScheduleForm = model_form(models.Schedule, base_class=Form, exclude=['cinema', 'show_date'])
ScheduleForm.cinema = SelectField('Cinema')
ScheduleForm.show_date = DateField('Show Date', format='%m/%d/%Y')

ScheduleSlotForm = model_form(models.ScheduleTimeSlot, base_class=Form, exclude=[ 'feed_code',
        'feed_code_schedule', 'start_time', 'movie', 'price', 'rockwell_movie_schedule_id', 'popcorn_price' ])
ScheduleSlotForm.start_time = TimeField('Start Time')
ScheduleSlotForm.movie_title = SelectField('Movie')
ScheduleSlotForm.price = DecimalField('Price')
ScheduleSlotForm.popcorn_price = DecimalField('Popcorn Price')

TheaterOrgForm = model_form(models.TheaterOrganization, base_class=Form, exclude=['template', 'template_image',
        'movie_ticket_template', 'ticket_remarks', 'access_code', 'theaterorg_ticket_template'])
TheaterOrgForm.template = TextAreaField('Generic Ticket Template Definition')
TheaterOrgForm.movie_ticket_template = TextAreaField('Movie Ticket Template Definition')
TheaterOrgForm.template_image_file = FileField('Generic Ticket Template Image')
TheaterOrgForm.ticket_remarks = TextAreaField('Ticket Remarks')
TheaterOrgForm.access_code = TextField('Access Token (optional)')
TheaterOrgForm.theaterorg_ticket_template = TextAreaField('Theater Organization Ticket Template Definition')
TheaterOrgForm.theaterorg_template_image_file = FileField('Theater Organization Ticket Template Image')

ClientForm = model_form(models.Client, base_class=Form)
ClientForm.id = TextField('ID')


def date_format_check(form, field):
    try:
        date_string = datetime.datetime.strptime(field.data, '%m/%d/%Y %H:%M')
    except ValueError:
        raise ValidationError('Date format should be month/date/year hours:minutes (24-hour style)')

def time_format_check(form, field):
    try:
        date_string = datetime.datetime.strptime(field.data, '%H:%M')
    except ValueError:
        raise ValidationError('Time format should be hours:minutes (24-hour style)')

def check_white_list(form, field):
    whitelist_data = field.data

    for data in whitelist_data:
        if data in form.movie_fields_blacklist.data:
            raise ValidationError('Fields in whitelist should not be in the blacklist')

def check_black_list(form, field):
    blacklist_data = field.data

    for data in blacklist_data:
        if data in form.movie_fields_whitelist.data:
            raise ValidationError('Fields in blacklist should not be in the whitelist')


class FileUploadForm(Form):
    file_uploaded = FileField('File')


class FeedForm(Form):
    name = TextField('name', validators=[Required()])
    image_density = SelectField('Image Density', choices=[('mdpi', 'MDPI'),('hdpi','HDPI'),('hidpi','HiDPI'),('xhdpi','XHDPI')])
    movie_fields_whitelist = SelectMultipleField('Whitelist', option_widget=widgets.CheckboxInput(),
            widget=whitelist_blacklist_widget, choices=MOVIE_FIELDS, validators=[check_white_list])
    movie_fields_blacklist = SelectMultipleField('Blacklist', option_widget=widgets.CheckboxInput(),
            widget=whitelist_blacklist_widget, choices=MOVIE_FIELDS, validators=[check_black_list])
    allow_movie_creation = BooleanField('Allow Movie Creation')
    heuristics_priority = IntegerField('Heuristics Priority')


class EditReservationForm(Form):
    theater = SelectField('Theaters')
    schedule = SelectField('Schedule', coerce=int)
    time = SelectField('Start Times', coerce=convert_string_to_time)
    cinema = TextField('Cinema')
    seat_id = TextField('Seat ID', validators=[Required()])
    reservation_index = HiddenField('reservation_index')


class PaymentSettingsForm(Form):
    theater = SelectField('Theater', validators=[Optional()], description='Theater associated with payment setting. Note: This cannot be changed once created to prevent duplicates.')
    payment_identifier = TextField('Payment Indentifier', default='pesopay', validators=[Required()], description='Default value is pesopay')
    currency_code = TextField('Currency Code', default='608', validators=[Required()], description='Default value is 608')
    pay_type = TextField('Payment Type', default='N', validators=[Required()], description='Default value is N')
    merchand_id = TextField('Merchant ID', validators=[Required()])
    hash_secret = TextField('Hash Secret', validators=[Required()])
    default_form_target = TextField('Default Form Target', default='https://www.pesopay.com/b2c2/eng/payment/payForm.jsp',
            validators=[Required()], description='Default value is https://www.pesopay.com/b2c2/eng/payment/payForm.jsp')

class PesoPayPaymentSettingsCinemaForm(Form):
    cinema = TextField('Cinema', validators=[Required()])
    currency_code = TextField('Currency Code', default='608', validators=[Required()], description='Default value is 608')
    pay_type = TextField('Payment Type', default='N', validators=[Required()], description='Default value is N')
    merchand_id = TextField('Merchant ID', validators=[Required()])
    hash_secret = TextField('Hash Secret', validators=[Required()])
    default_form_target = TextField('Default Form Target', default='https://www.pesopay.com/b2c2/eng/payment/payForm.jsp',
            validators=[Required()], description='Default value is https://www.pesopay.com/b2c2/eng/payment/payForm.jsp')

class PaynamicsPaymentSettingsForm(Form):
    theater = SelectField('Theater', validators=[Optional()], description='Theater associated with payment setting. Note: This cannot be changed once created to prevent duplicates.')
    merchant_id = TextField('Merchant ID', validators=[Required()])
    merchant_name = TextField('Merchant Name', validators=[Required()])
    merchant_key = TextField('Merchant Key', validators=[Required()])
    moto_merchant_id = TextField('MOTO Merchant ID', description='Merchant ID for tokenization')
    moto_merchant_name = TextField('MOTO Merchant Name', description='Merchant Name for tokenization')
    moto_merchant_key = TextField('MOTO Merchant Key', description='Merchant Key for tokenization')
    tokenized_3d_merchant_id = TextField('Tokenized 3d Merchant ID', description='Merchant ID for 3d tokenization')
    tokenized_3d_merchant_name = TextField('Tokenized 3d Merchant Name', description='Merchant Name for 3d tokenization')
    tokenized_3d_merchant_key = TextField('Tokenized 3d Merchant Key', description='Merchant Key for 3d tokenization')
    ip_address = TextField('IP Address', validators=[Required()])
    notification_url = TextField('Notification URL', validators=[Required()])
    response_url = TextField('Response URL', validators=[Required()])
    cancel_url = TextField('Cancel URL', validators=[Required()])
    mtac_url = TextField('mtac URL')
    currency = TextField('Currency', validators=[Required()])
    pmethod = TextField('Payment Method', validators=[Required()])
    trxtype = TextField('trxtype', validators=[Required()])
    mlogo_url = TextField('mlogo URL')
    secure3d = TextField('Secure 3D')
    descriptor_note = TextField('Descriptor Note', validators=[Required()])
    default_form_target = TextField('Default Form Target', validators=[Required()])


class MIGSPaymentSettingsForm(Form):
    theater = SelectField('Theater', validators=[Optional()], description='Theater associated with payment setting. Note: This cannot be changed once created to prevent duplicates.')
    merchant_id = TextField('Merchant ID', validators=[Required()])
    access_code = TextField('Access Code', validators=[Required()])
    hash_secret = TextField('Hash Secret', validators=[Required()])
    username = TextField('Username', validators=[Required()])
    password = TextField('Password', validators=[Required()])
    title = TextField('Title', validators=[Required()])
    vpc_version = TextField('VPC Version', validators=[Required()])
    vpc_command = TextField('VPC Command', validators=[Required()])
    vpc_locale = TextField('VPC Locale', validators=[Required()])
    vpc_currency = TextField('VPC Currency', validators=[Required()])
    vpc_return_url = TextField('VPC Return URL', validators=[Required()])
    default_form_target = TextField('Default Form Target', validators=[Required()])
    requery_url = TextField('ReQuery URL', validators=[Required()])


class IPay88PaymentSettingsForm(Form):
    theater = SelectField('Theater', validators=[Optional()], description='Theater associated with payment setting. Note: This cannot be changed once created to prevent duplicates.')
    merchant_code = TextField('Merchant Code', validators=[Required()])
    merchant_key = TextField('Merchant Key', validators=[Required()])
    payment_id = TextField('Payment ID', validators=[Required()])
    currency = TextField('Currency', validators=[Required()])
    prod_desc = TextField('ProdDesc', validators=[Required()])
    remark = TextField('Remark')
    lang = TextField('Lang', validators=[Required()])
    backend_url = TextField('Backend URL', validators=[Required()])
    default_form_target = TextField('Default Form Target', validators=[Required()])


class SecondaryMovieTitlesLookupForm(Form):
    secondary_title_lookup = SelectField('Secondary Title Lookup')


class MovieTitlesLookupForm(Form):
    movie_title_lookup = SelectField('Movie Title Lookup')
    rt_id = TextField('Rotten Tomatoes ID')


class AlternatePostersForm(Form):
    resolution = TextField('Name', validators=[Required()])
    source_url = TextField('Poster URL', validators=[Required()])
    orientation = SelectField('Orientation', choices=IMAGE_ORIENTATION)
    application_type = SelectField('Application Type', choices=IMAGE_APPLICATION_TYPE)


class MultiResImageBlobForm(Form):
    source_url = TextField('Source URL')


class ConvenienceFeeSettingsForm(Form):
    theaterorgs = SelectField('Theater Organizations', validators=[Optional()])
    credit_card_convenience_fee = TextField('Convenience Fee (Credit Card)')
    credit_card_convenience_fee_message = TextAreaField('Convenience Fee Message (Credit Card)')
    bank_payment_convenience_fee = TextField('Convenience Fee (Bank Payment)')
    bank_payment_convenience_fee_message = TextAreaField('Convenience Fee Message (Bank Payment)')
    gcash_convenience_fee = TextField('Convenience Fee (GCash)')
    gcash_convenience_fee_message = TextAreaField('Convenience Fee Message (GCash)')


class AllowDiscountCodesWhitelistForm(Form):
    theaters_whitelist = SelectMultipleField('Allow Discount Codes Whitelist', option_widget=widgets.CheckboxInput(), widget=whitelist_blacklist_widget)


PaymentOptionSettingsForm = model_form(models.PaymentOptionSettings, base_class=Form, exclude=['allow_discount_codes_whitelist'])
PaymentOptionSettingsForm.payment_option_identifier = TextField('Payment Option Identifier', validators=[Required()])
PaymentOptionSettingsForm.paymentoption_image_file = FileField('Payment Option Image')
PaymentOptionSettingsForm.paymentoption_header_file = FileField('Payment Option Header')

SystemSettingsForm = model_form(SystemSettings, base_class=Form, exclude=['lct_reap_timeout', 'lct_username', 'lct_password'])
SystemSettingsForm.lct_username = TextField('LCT Username')
SystemSettingsForm.lct_password = TextField('LCT Password')
SystemSettingsForm.lct_reap_timeout = IntegerField('LCT Reap / Cancellation Timeout')

CSRCSettingForm = model_form(AccountSettings, base_class=Form, exclude=[ 'merchant_key', 'merchant_code', 'last_updated'])
CSRCSettingForm.merchant_key = TextField('GMovies Merchant Key (provided by Ipay88)', validators=[Required()])
CSRCSettingForm.merchant_code = TextField('GMovies Merchant Code (provided by Ipay88)', validators=[Required()])

AnalyticsServiceForm = model_form(models.AnalyticsService, base_class=Form, exclude=['service_name', 'service_code', 'previous_status', 'description'])
AnalyticsServiceForm.service_name = TextField('Service Name', validators=[Required()])
AnalyticsServiceForm.service_code = TextField('Service Code', validators=[Required()])
AnalyticsServiceForm.description = TextAreaField('Description')
AnalyticsServiceForm.platform = SelectMultipleField('Platform', option_widget=widgets.CheckboxInput(),
        widget=payment_options_widget, choices=PLATFORM_CHOICES)

MovieTitlesForm = model_form(models.MovieTitles, base_class=Form, exclude=['correlation_titles',
        'rt_details', 'secondary_titles', 'searchable_title', 'searchable_words'])
MovieTitlesForm.rt_id = TextField('Rotten Tomatoes ID')

SecondaryMovieTitlesForm = model_form(models.SecondaryMovieTitles, base_class=Form, exclude=['correlation_title', 'date_created', 'datetime_created'])


###############
#
# Forms for Uploading Posters and Assets Image
#
###############

PosterImageForm = model_form(models.PosterImageBlob, base_class=Form, exclude=['resolution', 'image', 'orientation', 'application_type', 'is_default'])
PosterImageForm.orientation = SelectField('Orientation', choices=IMAGE_ORIENTATION)
PosterImageForm.application_type = SelectField('Application Type', choices=IMAGE_APPLICATION_TYPE)
PosterImageForm.image_file = FileField('Poster Image')

BackgroundPosterImageForm = model_form(models.PosterImageBlob, base_class=Form, exclude=['resolution', 'image', 'orientation', 'application_type', 'is_default'])
BackgroundPosterImageForm.application_type = SelectField('Application Type', choices=IMAGE_APPLICATION_TYPE)
BackgroundPosterImageForm.image_file = FileField('Background Poster Image')

ComingSoonAssetsForm = model_form(models.AssetsImageBlob, base_class=Form, exclude=['name', 'image', 'assets_group'])
ComingSoonAssetsForm.month = SelectField('Month', choices=COMING_SOON_ASSETS_MONTHS)
ComingSoonAssetsForm.image_file = FileField('Tab Image')


###############
#
# GMovies Promo Form
#
###############

BlockedScreeningAssetsForm = model_form(models.AssetsImageBlob, base_class=Form, exclude=['assets_group', 'image', 'is_active', 'platform'])
BlockedScreeningAssetsForm.image_file = FileField('Tab Image')
BlockedScreeningAssetsForm.is_active = BooleanField('Is Active')
BlockedScreeningAssetsForm.platform = SelectMultipleField('Platform', option_widget=widgets.CheckboxInput(),
        widget=payment_options_widget, choices=PLATFORM_CHOICES)

PromoNewForm = model_form(models.Promo, base_class=Form,
        exclude=['available_date', 'movie', 'start_time', 'end_time', 'price',
                'convenience_fees', 'convenience_fees_message', 'theater_id',
                'seat_map', 'seat_count', 'seats_whitelist', 'seats_blacklist',
                'seats_locked', 'seats_purchased', 'payment_options_ios',
                'payment_options_android', 'payment_options_website',
                'allowed_seats', 'ticket_template', 'is_qrcode', 'is_featured',
                'is_active', 'is_block_screening', 'activate_requery', 'platform'])
PromoNewForm.available_date = DateField('Available Date', format='%m/%d/%Y', description='Date Format (ex. mm/dd/YYYY)')
PromoNewForm.start_time_timefield = TimeField('Start Time', validators=[Optional()])
PromoNewForm.end_time_timefield = TimeField('End Time', validators=[Optional()])
PromoNewForm.movie_title = SelectField('Movie', validators=[Optional()])
PromoNewForm.price = DecimalField('Price', validators=[Optional()])
PromoNewForm.convenience_fees_text = TextAreaField('Convenience Fees')
PromoNewForm.convenience_fees_message_text = TextAreaField('Convenience Fees Messages')
PromoNewForm.theater_id = TextField('Theater ID')
PromoNewForm.theater_logo_image_file = FileField('Theater Logo Image')

PromoEditForm = model_form(models.Promo, base_class=Form,
        exclude=['available_date', 'movie', 'start_time', 'end_time', 'price',
                'convenience_fees', 'convenience_fees_message', 'theater_id',
                'seat_map', 'seat_count', 'seats_whitelist', 'seats_blacklist',
                'seats_locked', 'seats_purchased' 'payment_options_ios',
                'payment_options_android', 'payment_options_website',
                'allowed_seats', 'ticket_template', 'is_qrcode', 'is_featured',
                'is_active', 'is_block_screening', 'activate_requery', 'platform'])
PromoEditForm.available_date = DateField('Available Date', format='%m/%d/%Y', description='Date Format (ex. mm/dd/YYYY)')
PromoEditForm.start_time_timefield = TimeField('Start Time', validators=[Optional()])
PromoEditForm.end_time_timefield = TimeField('End Time', validators=[Optional()])
PromoEditForm.movie_title = SelectField('Movie', validators=[Optional()])
PromoEditForm.price = DecimalField('Price', validators=[Optional()])
PromoEditForm.convenience_fees_text = TextAreaField('Convenience Fees')
PromoEditForm.convenience_fees_message_text = TextAreaField('Convenience Fees Messages')
PromoEditForm.theater_id = TextField('Theater ID')
PromoEditForm.ticket_template = TextAreaField('Ticket Template Definition')
PromoEditForm.ticket_template_image_file = FileField('Ticket Template Image')
PromoEditForm.theater_logo_image_file = FileField('Theater Logo Image')
PromoEditForm.addseats = TextField('Additional Seat Row')
PromoEditForm.is_qrcode = BooleanField('Is QRCode')
PromoEditForm.is_block_screening = BooleanField('Is Block Screening')
PromoEditForm.is_featured = BooleanField('Is Featured')
PromoEditForm.is_active = BooleanField('Is Active')
PromoEditForm.activate_requery = BooleanField('Activate Re-Query')
PromoEditForm.platform = SelectMultipleField('Platform', option_widget=widgets.CheckboxInput(),
        widget=payment_options_widget, choices=PLATFORM_CHOICES)
PromoEditForm.seat_map = SelectMultipleField('Seat Rows', option_widget=widgets.CheckboxInput(),
        widget=widgets.ListWidget(prefix_label=False))
PromoEditForm.payment_options_ios = SelectMultipleField('iOS', option_widget=widgets.CheckboxInput(),
        widget=payment_options_widget, choices=BLOCKED_PAYMENT_OPTIONS)
PromoEditForm.payment_options_android = SelectMultipleField('Android', option_widget=widgets.CheckboxInput(),
        widget=payment_options_widget, choices=BLOCKED_PAYMENT_OPTIONS)
PromoEditForm.payment_options_website = SelectMultipleField('Website', option_widget=widgets.CheckboxInput(),
        widget=payment_options_widget, choices=BLOCKED_PAYMENT_OPTIONS)

class PromoSeatsStatusForm(Form):
    seats_blacklist = SelectMultipleField('Seats Blacklist', option_widget=widgets.CheckboxInput(), widget=widgets.ListWidget(prefix_label=False))
    seats_locked = SelectMultipleField('Seats Locked', option_widget=widgets.CheckboxInput(), widget=widgets.ListWidget(prefix_label=False))
    seats_purchased = SelectMultipleField('Seats Purchased', option_widget=widgets.CheckboxInput(), widget=widgets.ListWidget(prefix_label=False))
