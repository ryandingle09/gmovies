import traceback
import inspect
import pprint
import logging

from gmovies import orgs
from gmovies.util.logging import get_current_context_log
from gmovies.settings import NOTIFICATION_ADDRESSES, CMS_ADMINISTRATORS, IS_STAGING
from gmovies.bank_transfer import GMOVIES_CSRC_NOTIFICATION_ADDRESSES

from flask import request

from google.appengine.api import mail
from google.appengine.ext import deferred

from google.appengine.api.app_identity import get_application_id

log = logging.getLogger(__name__)

if IS_STAGING:
    TX_REPORT_MAIL_TO     = "Ryan Dingle <zrldingle@globe.com.ph>, Kimberly Lim <klim@yondu.com>"
    TX_REPORT_MAIL_BCC    = "Ryan Dingle <zrldingle@globe.com.ph>, Kimberly Lim <klim@yondu.com>"
    TX_REPORT_MAIL_DAILY_ROB_TO  = "Ryan Dingle <zrldingle@globe.com.ph>, Kimberly Lim <klim@yondu.com>"
    TX_REPORT_MAIL_DAILY_ROB_BCC = "Ryan Dingle <zrldingle@globe.com.ph>, Kimberly Lim <klim@yondu.com>"
    TX_REPORT_MAIL_DAILY_AYL_TO  = "Ryan Dingle <zrldingle@globe.com.ph>, Kimberly Lim <klim@yondu.com>"
    TX_REPORT_MAIL_DAILY_AYL_BCC = "Ryan Dingle <zrldingle@globe.com.ph>, Kimberly Lim <klim@yondu.com>"
    # SUPPORT_MAIL_BCC      = "Ryan Dingle <zrldingle@globe.com.ph>"
    SUPPORT_MAIL_TO       = "Jose Marcelo Angeles <jangeles@yondu.com>, Ryan Dingle <zrldingle@globe.com.ph>, Kimberly Lim <klim@yondu.com>"
    SUPPORT_MAIL_TO_TE    = "Ronnie Maala <rmaala@yondu.com>, Ryan Dingle <zrldingle@globe.com.ph>, Kimberly Lim <klim@yondu.com>"
    TE_SUPPORT_EMAIL      = "Ronnie Maala <rmaala@yondu.com>"
    REFUND_PAYMENT_GW_TO_PAYNAMICS_SM   = "Ryan Dingle <zrldingle@globe.com.ph>, Kimberly Lim <klim@yondu.com>"
    REFUND_PAYMENT_GW_TO_PAYNAMICS_GH   = "Ryan Dingle <zrldingle@globe.com.ph>, Kimberly Lim <klim@yondu.com>"
    REFUND_PAYMENT_GW_TO_PAYNAMICS_C76  = "Ryan Dingle <zrldingle@globe.com.ph>, Kimberly Lim <klim@yondu.com>"
    REFUND_PAYMENT_GW_TO_PAYNAMICS_GE   = "Ryan Dingle <zrldingle@globe.com.ph>, Kimberly Lim <klim@yondu.com>"
    REFUND_PAYMENT_GW_TO_MIGS       = "Ryan Dingle <zrldingle@globe.com.ph>, Kimberly Lim <klim@yondu.com>"
    REFUND_PAYMENT_GW_TO_IPAY88     = "Ryan Dingle <zrldingle@globe.com.ph>, Kimberly Lim <klim@yondu.com>"
    REFUND_PAYMENT_GW_BCC = "Jose Marcelo Angeles <jangeles@yondu.com>, Ryan Dingle <zrldingle@globe.com.ph>, Kimberly Lim <klim@yondu.com>"
    REFUND_USER_BCC       = REFUND_PAYMENT_GW_BCC
else:
    TX_REPORT_MAIL_TO     = "Francesca De Guzman <zmtdeguzman@globe.com.ph>"
    TX_REPORT_MAIL_BCC    = "Josh Lemuel Torio <jtorio@yondu.com>, Ryan Dingle <zrldingle@globe.com.ph>, Kimberly Lim <klim@yondu.com>"
    TX_REPORT_MAIL_DAILY_ROB_TO  = "Francesca De Guzman <zmtdeguzman@globe.com.ph>, Ma. Socorro Alvarez <Socorro.Alvarez@robinsonsland.com>, Mary Grace Cataluna - Fontiveros <MaryGrace.Cataluna-Fontiveros@robinsonsland.com>, Adrian Rubio <adrian.rubio@robinsonsland.com>, Ma. Carmela Vanessa Sembrero <vanessa.sembrero@robinsonsland.com>, Rosalinda Kabigting <Rosalinda.Kabigting@robinsonsland.com>, Michelle Araullo <Michelle.Araullo@robinsonsland.com>, Ana M. Palenzuela <Ana.Palenzuela@robinsonsland.com>"
    TX_REPORT_MAIL_DAILY_ROB_BCC = "Josh Lemuel Torio <zjgtorio@globe.com.ph>, Ryan Dingle <zrldingle@globe.com.ph>, Kimberly Lim <klim@yondu.com>"
    TX_REPORT_MAIL_DAILY_AYL_TO  = "Francesca De Guzman <zmtdeguzman@globe.com.ph>, Diego Xavier Garcia <dxcgarcia@globe.com.ph>, jl.leonado@mynt.xyz"
    TX_REPORT_MAIL_DAILY_AYL_BCC = "Josh Lemuel Torio <zjgtorio@globe.com.ph>, Ryan Dingle <zrldingle@globe.com.ph>, Kimberly Lim <klim@yondu.com>"
    # SUPPORT_MAIL_BCC      = "Josh Lemuel Torio <jtorio@yondu.com>"
    SUPPORT_MAIL_TO       = "Josh Lemuel Torio <jtorio@yondu.com>, Rhonnel Francisco <rfrancisco@yondu.com>, Jane Sheila Joson <zjsjoson@globe.com.ph>, Cannie Eduardo <zcceduardo@globe.com.ph>, Ryan Dingle <zrldingle@globe.com.ph>, Kimberly Lim <klim@yondu.com>, Jose Marcelo Angeles <jangeles@yondu.com>, Genica Garcia <ggarcia@yondu.com>, Myra Jane Cleofe Burgos <zmvburgos@globe.com.ph>, Joe Tuclaud <zjvtuclaud@globe.com.ph>, Elaine Iturralde <eiturralde@yondu.com>"
    SUPPORT_MAIL_TO_TE    = "TE Support <tesupport@yondu.com>, Ryan Dingle <zrldingle@globe.com.ph>, Kimberly Lim <klim@yondu.com>"
    TE_SUPPORT_EMAIL      = "TE Support <tesupport@yondu.com>"
    REFUND_PAYMENT_GW_TO_PAYNAMICS_SM   = "cs@paynamics.net, technical@paynamics.net, renato.martinez@paynamics.net, mylene.chua@paynamics.net, ariel.macarayan@paynamics.net"
    REFUND_PAYMENT_GW_TO_PAYNAMICS_GH   = "ann.musicmuseum@gmail.com, inquiries@greenhillscinemas.com.ph, technical@paynamics.net, tesupport@yondu.com, cs@paynamics.net"
    REFUND_PAYMENT_GW_TO_PAYNAMICS_C76  = "cs@paynamics.net, technical@paynamics.net, renato.martinez@paynamics.net, mylene.chua@paynamics.net, ariel.macarayan@paynamics.net"
    REFUND_PAYMENT_GW_TO_PAYNAMICS_GE   = "cs@paynamics.net, technical@paynamics.net, renato.martinez@paynamics.net, mylene.chua@paynamics.net, ariel.macarayan@paynamics.net"
    REFUND_PAYMENT_GW_TO_MIGS       = "adrian.rubio@robinsonsland.com, janireireinon.ancheta@robinsonsland.com"
    REFUND_PAYMENT_GW_TO_IPAY88     = "cpfandino@megaworld-lifestyle.com, ailomibao@megaworld-lifestyle.com, rollie.pamulaya@mobilegroupinc.com, luckychinatowncinemas@gmail.com, bvvaldenarro@megaworld-lifestyle.com, ftcastigador@megaworld-lifestyle.com, kagozon@megaworld-lifestyle.com, maermino@megaworld-lifestyle.com, fcmallanao@megaworld-lifestyle.com, ej.comiso@mobilegroupinc.com, bvvaldenarro@megaworld-lifestyle.com, jvvaldenarro@megaworld-lifestyle.com, dcflororita@megaworld-lifestyle.com, BongDeVera@megaworld-lifestyle.com, emedina@megaworld-lifestyle.com, pcortiz@megaworld-lifestyle.com"
    REFUND_PAYMENT_GW_BCC = "Josh Lemuel Torio <jtorio@yondu.com>, GMovies Support <support@gmovies.ph>, Ryan Dingle <zrldingle@globe.com.ph>, Kimberly Lim <klim@yondu.com>"
    REFUND_USER_BCC       = REFUND_PAYMENT_GW_BCC

DEVOPS = "Josh Lemuel Torio <jtorio@yondu.com>, Ryan Dingle <zrldingle@globe.com.ph>, Kimberly Lim <klim@yondu.com>, Jose Marcelo Angeles <jangeles@yondu.com>, Genica Garcia <ggarcia@yondu.com>, Elaine Iturralde <eiturralde@yondu.com>"

PATH_TO_EMAIL_TEMPLATE = 'gmovies/static/gmovies-email-template.html'

MAIL_FROM_TEMPLATE = "%%s@%s.appspotmail.com" % get_application_id()
BLAST_MAIL_FROM_TEMPLATE = "GMovies - e-Ticket <%%s@%s.appspotmail.com>" % get_application_id()

TX_REPORT_MAIL_FROM    = "GMovies Reports <gmovies-reports-noreply@%s.appspotmail.com>" % get_application_id()
TX_REPORT_MAIL_SUBJECT = "[GMovies] Transaction Reports"

SUPPORT_MAIL_FROM      = "GMovies Errors <gmovies-errors-noreply@%s.appspotmail.com>" % get_application_id()
SUPPORT_MAIL_FROM_INFO = "GMovies Info <gmovies-info-noreply@%s.appspotmail.com>" % get_application_id()
SUPPORT_MAIL_SUBJECT = "[GMovies] - Transaction Error Notification"
SUPPORT_MAIL_SUBJECT_TE = "[GMovies] - Transaction Success Error Notification"
SUPPORT_MAIL_SUBJECT_INFO = "[GMovies] - Transaction Notification"

REFUND_PAYMENT_GW_MAIL_FROM    = "GMovies Refunds <gmovies-refunds-noreply@%s.appspotmail.com>" % get_application_id()
REFUND_PAYMENT_GW_MAIL_SUBJECT = "GMovies Request for Refund"

REFUND_USER_MAIL_FROM    = REFUND_PAYMENT_GW_MAIL_FROM
REFUND_USER_MAIL_SUBJECT = REFUND_PAYMENT_GW_MAIL_SUBJECT

AYALA_CALLBAK_AUTH_FROM = SUPPORT_MAIL_FROM
AYALA_CALLBAK_AUTH_TO = SUPPORT_MAIL_TO
AYALA_CALLBACK_AUTH_SUBJECT = "Ayala Transaction Notification"

AYALA_CALLBACK_AUTH_BODY = """
    <p>This is to inform you that there's a transaction without callback as of <b>{freshtime}</b> from Ayala theaters.</p>
"""

SUPPORT_MAIL_BODY    = """
Transaction Error Notification

Details:
    Email: {email_address}
    Mobile Number: {mobnum}
    Movie : {movie}
    Cinema : {theater_and_cinema_name}
    Show Date and Time : {show_date_time}
    Seat(s): {seats}
    Dashboard Link: https://{app_id}.appspot.com/admin/transactions/{device_id}~{tx_id}/

    Error Encountered:
    {error_msg}

* For 'Could not find movie screening. Select another time.' and 'Movie schedule not found. Please try a different schedule.' errors,
double check with theater/cinema if there is a duplicate or schedule was removed/changed.
* For 'do_finalize_transaction' errors, this can be subjected for refund.
"""

SUPPORT_MAIL_BODY_TE = """
Transaction Error Notification

Details:
    Email: {email_address}
    Mobile Number: {mobnum}
    Movie : {movie}
    Cinema : {theater_and_cinema_name}
    Show Date and Time : {show_date_time}
    Seat(s): {seats}
    Dashboard Link: https://{app_id}.appspot.com/admin/transactions/{device_id}~{tx_id}/

    Error Encountered:
    {error_msg}

* Transaction charged, error on SUCCESS api. Check if transaction is for refund.
"""

SUPPORT_MAIL_BODY_INFO = """
Transaction Notification

Details:
    Email: {email_address}
    Mobile Number: {mobnum}
    Movie : {movie}
    Cinema : {theater_and_cinema_name}
    Show Date and Time : {show_date_time}
    Seat(s): {seats}
    Dashboard Link: https://{app_id}.appspot.com/admin/transactions/{device_id}~{tx_id}/

    Notificaction:
    {error_msg}
"""

EXCEPTION_REPORT_TEXT = """
Exception report:

Request   :
  Method  :\t {req_method}
  Path    :\t {req_path}
  Headers :
{req_headers}
  Query   :
{req_query}

Client    :\t {client_id}
Device    :\t {device_id}
User-Agent:\t {req_ua}


Exception occured during request: {e_val}

Log
==================================================
{log_messages}
==================================================

See attached dump.
"""

SCHEDULE_CHANGED_TEXT = """
<p>{status}</p>
<br>
Ciname Partner: <b>{cinema_partner}</b><br>
Cinema name: <b>{ciname_name}</b><br>
Showing Date: <b>{showing_date}</b>
<br>
Old Schedule:<br>
<p>
    start time: <b>{old_start_time}</b> , 
    feed code: <b>{old_feed_code}</b> , 
    Movie: <b>{old_movie_name}</b> , 
    seating type: <b>{old_seating_type}</b> , 
    seating type: <b>{old_variant}</b> , 
    price: <b>{old_price}</b>
</p>
<br>
Updated Schedule:<br>
<p>
    start time: <b>{updated_start_time}</b> , 
    feed code: <b>{updated_feed_code}</b> , 
    Movie: <b>{updated_movie_name}</b> , 
    seating type: <b>{updated_seating_type}</b> , 
    seating type: <b>{updated_variant}</b> ,
    price: <b>{updated_price}</b>
</p>
<br>
Dashboard Link: https://{app_id}.appspot.com/{url}
"""

SCHEDULE_DELETED_TEXT = """
<p>{status}</p>
<br>
Ciname Partner: <b>{cinema_partner}</b><br>
Cinema name: <b>{ciname_name}</b><br>
Showing Date: <b>{showing_date}</b>
<br>
Deleted Schedule:<br>
<p>
    start time: <b>{old_start_time}</b> , 
    feed code: <b>{old_feed_code}</b> , 
    Movie: <b>{old_movie_name}</b> , 
    seating type: <b>{old_seating_type}</b> , 
    seating type: <b>{old_variant}</b> , 
    price: <b>{old_price}</b>
</p>
<br>
Dashboard Link: https://{app_id}.appspot.com/{url}
"""

MOVIE_HEURISTICS_REPORT_TEXT = """
Movies updated.

New movies added:                       {new_movies}
Existing movies
    From cache:                         {existing_from_cache}
    From datastore:                     {existing_from_datastore}
Datastore entries
    correlated by title:                {correlated_by_title}
    correlated by TheaterOrg/movie ID:  {correlated_by_id}
Cache matched
    by movie ID:                        {cached_by_id}
    by title:                           {cached_by_title}
Feed does not allow creation:           {movies_not_created}
Multiple correlations error:            {multi_correlations}

Movies Added:
{movies_added}

No Correlation. Failed to Add Movies:
{movies_not_match_by_correlation_id_or_title}

FIXME: Multiple Correlation. Failed to Add Movies:
{movies_with_multiple_correlation}



Movie Title Bank:
Existing titles
    From movie title bank:              {existing_from_movie_title_bank}

Not in movie title bank:
{titles_not_in_movie_title_bank}
"""

SCHEDULE_HEURISTICS_REPORT_TEXT = """
Schedules updated.

New schedules added:                    {new_schedules}
Existing schedules
    From cache:                         {existing_from_cache}
    From datastore:                     {existing_from_datastore}
    From datastore with variants:       {existing_from_datastore_variants}
Cache matched
    by code:                            {cached_by_code}
Datastore matched
    by code:                            {fetch_by_code}
    with variants:                      {fetch_variants}
    without variants:                   {fetch_no_variants}
Too many matches:                       {multi_errors}

"""

TICKET_NOTICE_TEXT = """
Thank you for purchasing tickets via GMovies.

Reference Number:           {reference_number}
Reference Code:             {reference_code}
Showing Date:               {show_date}
Movie Title:                {movie_title}
Theater - Cinema:           {theater_and_cinema}
Seats:                      {seats}
Total Amount:               {ticket_amount}
"""

BLOCKED_SCREENING_TICKET_NOTICE_TEXT = """
<html>
<head></head>
<body style="margin: 0; padding: 30px 0px 0px 0px; font-family: Arial, Helvetica, sans-serif;">
  <table align="center"cellpadding="0" cellspacing="0" width="100%">
    <tr>
      <td>
        <table align="center" width="700" style="border: 2px solid #00768F; border-collapse: collapse;">
          <tr>
            <td>
              <img src="https://globe-gmovies.appspot.com/static/images/smemailheader.png" width="100%" />
            </td>
          </tr>
          <tr>
            <td style="padding: 15px 10px 0px 10px;">
              <p align="center">
                <font size="4">This entitles the bearer a STAR WARS:</font><br />
                <b><font size="8" style="bold">THE FORCE AWAKENS</font></b><br />
                <b><font size="5">Advanced Screening Premium Pass.</font><b><br/>
              </p>
            </td>
          </tr>
          <tr>
            <td style="padding: 22px 0px 22px 0px;" width="100%">
              <hr style="border-style: dashed; border-color: #A3A3A3;" />
            </td>
          </tr>
          <tr>
            <td style="padding: 0px 10px 0px 10px;">
              <table width="100%">
                <tr>
                  <td colspan="2" style="padding: 0px 10px 0px 10px;" width="100%">
                    <b><font size="5">{movie_title}</font></b>&nbsp;&nbsp;
                    <font size="4">
                        <span style="background-color: #00BFFF; color: #FFFFFF; border-radius: 4px; padding: 2px 2px 2px 2px;">PG</span>
                    </font>
                  </td>
                </tr>
                <tr>
                  <td style="padding: 0px 0px 0px 10px;" width="60%">
                    <font size="3">{theater_name} {cinema_name}</font>
                  </td>
                  <td style="padding: 0px 10px 0px 0px;" width="40%">
                    <font size="3">{show_date}</font>
                  </td>
                </tr>
                <tr>
                  <td style="padding: 0px 0px 0px 10px;" width="60%">
                    <font size="3">Seat No. {seats}</font>
                  </td>
                  <td style="padding: 0px 10px 0px 0px;" width="40%">
                    <font size="3">{start_time}</font>
                  </td>
                </tr>
                <tr>
                  <td style="padding: 0px 0px 25px 10px" width="60%">
                    <b><font size="3">Reference No. {reference_code}</font></b>
                  </td>
                </tr>
                <tr>
                  <td bgcolor="#EAEAEA" colspan="2" style="padding: 15px 0px 15px 0px" width="100%">
                    <center><font size="2">Surrender this coupon at the theatre entrance.</font></center>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="padding: 0px 0px 22px 0px;"  width="100%">
              <hr style="border-style: dashed; border-color: #A3A3A3;" />
            </td>
          </tr>
          <tr>
            <td style="padding: 0px 10px 40px 10px;">
              <table width="100%">
                <tr>
                  <td style="padding: 0px 0px 0px 10px;">
                    <b><font size="5">Cup with Topper</font></b>
                  </td>
                </tr>
                <tr>
                  <td style="padding: 10px 0px 30px 10px;">
                    <b><font size="4">Reference No. {reference_code}</font></b>
                  </td>
                </tr>
                <tr>
                  <td bgcolor="#EAEAEA" colspan="2" style="padding: 15px 0px 15px 0px" width="100%">
                    <center><font size="2">Claiming Period is from December 16, 2015 to February 16, 2016 at the Snack Time Counter.</font></center>
                  </td>
                </tr>
             </table>
            </td>
          </tr>
          <tr>
            <td bgcolor="#00768F" style="padding: 8px 0px 8px 0px">
              <center><span style="color:#FFF; font-size: 14px;">Landline: 63 2 211 9792 | Mobile: 63 917 543 4594</span></center>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>
"""

def notify_changed_schedules(kwargs):
    email = mail.EmailMessage()
    email.sender = MAIL_FROM_TEMPLATE % 'cron-noreply'
    email.to = "Ryan Dingle <rdingle@yondu.com>" #SUPPORT_MAIL_TO
    email.html = SCHEDULE_CHANGED_TEXT.format(**kwargs)
    email.subject = "GMovies: Schedule Notification"

    try:
        email.send()
    except:
        pass

def notify_deleted_schedules(kwargs):
    email = mail.EmailMessage()
    email.sender = MAIL_FROM_TEMPLATE % 'cron-noreply'
    email.to = "Ryan Dingle <rdingle@yondu.com>" #SUPPORT_MAIL_TO
    email.html = SCHEDULE_DELETED_TEXT.format(**kwargs)
    email.subject = "GMovies: Deleted Schedule Notification"

    try:
        email.send()
    except:
        pass

def notify_admin(route_class, e, e_bundle):
    e_typ, e_val, e_trace = e_bundle

    frames = _get_tb_frames(e_trace)
    format_trace = ''.join([ _format_tb_frame(fr) for fr in frames ])
    log_dump = get_current_context_log()

    req_method = request.method
    req_path = request.script_root + request.path
    req_headers = format_request_dict(request.headers)
    req_query = format_request_dict(request.values)

    client_id = '(none)'
    if 'X-GMovies-Auth-ClientId' in request.headers:
        client_id = request.headers['X-GMovies-Auth-ClientId']
    device_id = '(none)'
    if 'X-GMovies-DeviceId' in request.headers:
        device_id = request.headers['X-GMovies-DeviceId']
    ua = request.headers['User-Agent']

    email = mail.EmailMessage()
    email.sender = MAIL_FROM_TEMPLATE % 'errors-noreply'
    email.to = NOTIFICATION_ADDRESSES
    email.body = EXCEPTION_REPORT_TEXT.format(req_method=req_method,
                                              req_path=req_path,
                                              req_headers=req_headers,
                                              req_query=req_query,
                                              client_id=client_id,
                                              device_id=device_id,
                                              req_ua=ua,
                                              log_messages=log_dump,
                                              e_val=e_val)
    email.subject = "Exception in %s: %s" % (route_class, e_typ.__name__)
    email.attachments = [("dump.txt", format_trace)]

    try:
        email.send()
    except:
        pass

def movie_heuristics_notification(**kwargs):
    email = mail.EmailMessage()
    email.sender = MAIL_FROM_TEMPLATE % 'cron-noreply'
    email.to = NOTIFICATION_ADDRESSES
    email.body = MOVIE_HEURISTICS_REPORT_TEXT.format(**kwargs)
    email.subject = "GMovies API: Updated Movies"

    try:
        email.send()
    except:
        pass

def schedule_heuristics_notification(**kwargs):
    email = mail.EmailMessage()
    email.sender = MAIL_FROM_TEMPLATE % 'cron-noreply'
    email.to = NOTIFICATION_ADDRESSES
    email.body = SCHEDULE_HEURISTICS_REPORT_TEXT.format(**kwargs)
    email.subject = "GMovies API: Updated Schedules"

    try:
        email.send()
    except:
        pass

def send_ticket_details(email_address, ticket):
    movie_title = ''
    theater_and_cinema = ''
    seats = ''
    show_date = ticket.date.strftime('%d %b %Y')

    if 'movie_title' in ticket.extra:
        movie_title = ticket.extra['movie_title']

    if 'theater_and_cinema_name' in ticket.extra:
        if ticket.extra['theater_and_cinema_name']:
            if type(ticket.extra['theater_and_cinema_name']) != list:
                theater_and_cinema = ticket.extra['theater_and_cinema_name']
            else:
                theater_and_cinema = ticket.extra['theater_and_cinema_name'][0]

    if 'seats' in ticket.extra:
        seats = ticket.extra['seats']

    if 'show_datetime' in ticket.extra:
        show_date = ticket.extra['show_datetime']

    email = mail.EmailMessage()
    email.sender = MAIL_FROM_TEMPLATE % 'ticketing-noreply'
    email.to = email_address
    email.body = TICKET_NOTICE_TEXT.format(reference_number=ticket.ref,
            reference_code=ticket.code, show_date=show_date,
            movie_title=movie_title, theater_and_cinema=theater_and_cinema,
            seats=seats, ticket_amount=ticket.extra['total_amount'])
    email.subject = "[GMovies] Ticket Details"

    try:
        email.send()
    except:
        log.error("Could not send out email, out of quota?")

def send_blocked_screening_ticket_details(email_address, ticket):
    log.info("BLOCKED SCREENING: Sending email, ticket details...")

    try:
        body_template = BLOCKED_SCREENING_TICKET_NOTICE_TEXT.format(
                movie_title=ticket.extra['movie_title'],
                theater_name=ticket.extra['theater_name'],
                cinema_name=ticket.extra['cinema_name'],
                reference_number=ticket.ref, reference_code=ticket.code,
                show_date=ticket.date.strftime('%B, %d %Y'),
                start_time=ticket.extra['start_time'], seats=ticket.extra['seats'],
                seat_price=ticket.extra['price_per_seat'],
                convenience_fee=ticket.extra['reservation_fee'],
                total_amount=ticket.extra['total_amount'])

        email = mail.EmailMessage()
        email.sender = MAIL_FROM_TEMPLATE % 'ticketing-noreply'
        email.to = email_address
        email.html = body_template
        email.subject = "[GMovies] Blocked Screening Ticket Details"
        email.send()
    except Exception, e:
        log.warn("Could not send out email, out of quota?")
        log.error(e)

def format_value_list(val_list):
    if len(val_list) > 1:
        val_list_quot = [ "'%s'" % v for v in val_list ]
        return "[ %s ]" % ','.join(val_list_quot)
    else:
        return "'%s'" % val_list[0]

def format_request_dict(values):
     kv_format = "           \t '%s' : %s"

     kv_pairs_str = [ kv_format % (k, format_value_list(values.getlist(k)))
                      for k in values.keys() ]
     return '\n'.join(kv_pairs_str)


def _get_tb_frames(tb):
    frames = []
    while tb is not None:
        filename = tb.tb_frame.f_code.co_filename
        function = tb.tb_frame.f_code.co_name
        lineno = tb.tb_lineno - 1
        loader = tb.tb_frame.f_globals.get('__loader__')
        module_name = tb.tb_frame.f_globals.get('__name__') or ''
        frames.append({
            'tb': tb,
            'type': 'gmovies' if module_name.startswith('gmovies.') else 'sys',
            'filename': filename,
            'function': function,
            'lineno': lineno + 1,
            'vars': tb.tb_frame.f_locals.items(),
            'id': id(tb),
        })
        tb = tb.tb_next

    return frames

def _format_tb_frame(frame):
    FRAME_FORMAT = "{id}\t{type}\t{filename}:{lineno} {function}\n{vars_str}\n\n"
    VAR_FORMAT =   " {name: >15} : {value}"

    vars_str_list = [ VAR_FORMAT.format(name=name, value=pprint.saferepr(value))
                      for name,value in frame['vars'] ]
    vars_str = '\n'.join(vars_str_list)
    return FRAME_FORMAT.format(vars_str=vars_str, **frame)


################
#
# Rockwell Email Notifications
#
################

def rockwell_schedule_heuristics_notification(**kwargs):
    email = mail.EmailMessage()
    email.sender = MAIL_FROM_TEMPLATE % 'cron-noreply'
    email.to = CMS_ADMINISTRATORS
    email.body = ROCKWELL_SCHEDULE_HEURISTICS_REPORT_TEXT.format(**kwargs)
    email.subject = "GMovies API: Updated Rockwell Schedules"

    try:
        email.send()
    except:
        pass


ROCKWELL_SCHEDULE_HEURISTICS_REPORT_TEXT = """
Rockwell schedules updated.

New schedules added:                    {rockwell_new_schedules}

Existing schedules
    From cache:                         {existing_from_cache}
    From datastore:                     {existing_from_datastore}

Cache matched
    by code:                            {cached_by_code}

Datastore matched
    by code:                            {fetch_by_code}

"""


def send_csr_campaign_payment_notification(**kwargs):
    email = mail.EmailMessage()
    email.sender = MAIL_FROM_TEMPLATE % 'csr-campaign-payment'
    email.to = GMOVIES_CSRC_NOTIFICATION_ADDRESSES
    email.body = GMOVIES_CSR_CAMPAIGN_REPORT.format(**kwargs)
    email.subject = "GMovies: CSR Campaign Payment Received Notification"
    try:
        email.send()
    except:
        log.error("Could not send out email, out of quota?")
        raise


GMOVIES_CSR_CAMPAIGN_REPORT = """
GMovies CSR Campaign Payment Notification

Campaign: {campaign}
Name: {username}
Email Address: {email_address}
Contact Number: {contact_number}
Amount: {amount}
Reference Code: {reference_code}
Payment Transaction ID: {trans_id} 
Authentication Code: {auth_code}
"""

def donor_payment_notification(**kwargs):
    email_address = kwargs.get('email_address', None)
    if email_address is None:
        pass

    email = mail.EmailMessage()
    email.sender = MAIL_FROM_TEMPLATE % 'noreply'
    email.to = email_address
    email.body = GMOVIES_DONOR_NOTIFICATION.format(**kwargs)
    email.subject = "GMovies: Payment Received Notification"
    try:
        email.send()
    except:
        log.error("Could not send out email, out of quota?")
        raise



GMOVIES_DONOR_NOTIFICATION = """
Thank you very much for participating in {campaign}.

For your reference:
Amount: {amount}
Reference Code: {reference_code}
Bank Payment Transaction ID: {trans_id} 
"""


def notify_globe_csr(donors_list, csr_email_address, is_monthly=False):
    if is_monthly:
        log.info("Monthly Notification")
        HEADER = """"Donors from the last thirty days"""
    else:
        HEADER = """Latest 10 Donors"""
        log.info("Wave of 10")
    if donors_list is not None:
        log.info("Donors Count: {}".format(len(donors_list)))
        BODY = ""
        for d in donors_list:
            if BODY is not None:
                BODY = BODY + CSR_NOTIFICATION_TEMPLATE.format(**d)
            else:
                BODY = CSR_NOTIFICATION_TEMPLATE.format(**d)

    email = mail.EmailMessage()
    email.sender = MAIL_FROM_TEMPLATE % 'noreply'
    email.to = csr_email_address
    email.body = BODY
    email.subject = "GMovies: Donors List"
    try:
        email.send()
    except:
        log.error("Could not send out email, out of quota?")
        raise

    
CSR_NOTIFICATION_TEMPLATE ="""
=================================
Campaign: {campaign}
Name: {username}
Email Address: {email_address}
Contact Number: {contact_number}
Amount: {amount}
Transaction Date: {trans_date}
Reference Code: {reference_code}
Payment Transaction ID: {trans_id} 
"""


def movie_multiple_correlation_notification(**kwargs):
    email = mail.EmailMessage()
    email.sender = MAIL_FROM_TEMPLATE % 'cron-noreply'
    email.to = CMS_ADMINISTRATORS
    email.html = MOVIES_MULTIPLE_CORRELATION_REPORT_TEXT.format(**kwargs)
    email.subject = "GMovies API: Multiple Correlation Movies"

    try:
        email.send()
    except:
        pass


MOVIES_MULTIPLE_CORRELATION_REPORT_TEXT = """
FIXME: Multiple Correlation. Failed to Add Movies
{movies_with_multiple_correlation}
"""

def movie_multiple_correlation_notification_for_cinema76(**kwargs):
    email = mail.EmailMessage()
    email.sender = MAIL_FROM_TEMPLATE % 'cron-noreply'
    email.to = DEVOPS
    email.html = MOVIES_MULTIPLE_CORRELATION_REPORT_TEXT_C76.format(**kwargs)
    email.subject = "GMovies GAE: Notification for Adding Movie Title"

    try:
        email.send()
    except:
        pass


MOVIES_MULTIPLE_CORRELATION_REPORT_TEXT_C76 = """
System detected movie title already exist.<br>
Schedules for this movie were successfully updated.<br><br>
{movies_with_multiple_correlation}
"""


def movie_title_bank_heuristics_notification(**kwargs):
    email = mail.EmailMessage()
    email.sender = MAIL_FROM_TEMPLATE % 'cron-noreply'
    email.to = NOTIFICATION_ADDRESSES
    email.body = MOVIE_TITLE_BANK_HEURISTICS_REPORT_TEXT.format(**kwargs)
    email.subject = "GMovies API: Updated Movie Title Bank"

    try:
        email.send()
    except:
        pass


MOVIE_TITLE_BANK_HEURISTICS_REPORT_TEXT = """
Movie Title Bank Updated.

New movie titles added:                       {new_movie_titles}
Existing movie titles
    From cache:                         {existing_from_cache}
    From datastore:                     {existing_from_datastore}
Datastore entries
    correlated by title:                {correlated_by_title}
    correlated by TheaterOrg/movie ID:  {correlated_by_id}
Cache matched
    by movie ID:                        {cached_by_id}
    by title:                           {cached_by_title}
Multiple correlations error:            {multi_correlations}


FIXME: Multiple Correlation. Failed to Add Movies:
{movie_titles_with_multiple_correlation}
"""


def movie_title_bank_multiple_correlation_notification(**kwargs):
    email = mail.EmailMessage()
    email.sender = MAIL_FROM_TEMPLATE % 'cron-noreply'
    email.to = CMS_ADMINISTRATORS
    email.body = MOVIE_TITLES_MULTIPLE_CORRELATION_REPORT_TEXT.format(**kwargs)
    email.subject = "GMovies API: Multiple Correlation in Movie Title Bank"

    try:
        email.send()
    except:
        pass


MOVIE_TITLES_MULTIPLE_CORRELATION_REPORT_TEXT = """
FIXME: Multiple Correlation. Failed to Add Movie Titles in Movie Title Bank
{movie_titles_with_multiple_correlation}
"""


def password_reset_request(**kwargs):
    email = mail.EmailMessage()
    email.sender = MAIL_FROM_TEMPLATE % 'no-reply'
    email.to = kwargs['user_email']
    email.body = PASSWORD_RESET_REQUEST_TEXT.format(**kwargs)
    email.subject = "[GMovies] Password Reset Request"

    try:
        email.send()
    except:
        pass


def password_reset_confirmation(**kwargs):
    email = mail.EmailMessage()
    email.sender = MAIL_FROM_TEMPLATE % 'no-reply'
    email.to = kwargs['user_email']
    email.body = PASSWORD_RESET_CONFIRMATION_TEXT.format(**kwargs)
    email.subject = "[GMovies] Password Reset Confirmation"

    try:
        email.send()
    except:
        pass


PASSWORD_RESET_REQUEST_TEXT = """
You're receiving this e-mail because you requested a password reset for your account at {branch_account} via GMovies
Click the link below to reset your password:

{password_reset_link}

If you don't wish to reset your password, disregard this e-mail and no action will be taken.

-GMovies Team-
"""

PASSWORD_RESET_CONFIRMATION_TEXT = """
You have successfully reset your password. Below is your new password.

E-Mail Address: {user_email}
New Password: {new_password}

-GMovies Team-
"""


def send_sales_reports(date_from, date_to, attachments):
    email = mail.EmailMessage()
    email.sender = MAIL_FROM_TEMPLATE % 'reports-noreply'
    email.to = 'zrldingle@globe.com.ph'
    email.bcc = 'zrldingle@globe.com.ph'
    email.body = 'This is an auto generated email sent by the system. No need to reply.'
    email.attachments = attachments
    email.subject = "[GMovies] Sales Reports %s - %s" % (str(date_from), str(date_to))

    try:
        email.send()
    except:
        log.error("Could not send out email, out of quota?")

def send_transaction_reports(year, month, attachments, payment_type=None, report_type=None, th_code=None):
    email = mail.EmailMessage()
    email.sender = TX_REPORT_MAIL_FROM
    email_to = TX_REPORT_MAIL_TO
    email_bcc = TX_REPORT_MAIL_BCC
    if 'daily' == report_type:
        email_to = TX_REPORT_MAIL_DAILY_ROB_TO
        email_bcc = TX_REPORT_MAIL_DAILY_ROB_BCC
        if th_code:
            if 'AYL' == th_code:
                email_to = TX_REPORT_MAIL_DAILY_AYL_TO
                email_bcc = TX_REPORT_MAIL_DAILY_AYL_BCC
    email.to = email_to
    email.bcc = email_bcc
    email.body = 'This is an auto generated email sent by the system. No need to reply.'
    email.attachments = attachments
    email_subject = TX_REPORT_MAIL_SUBJECT + (" %s - %s" % (year, month))
    if payment_type:
        email_subject = email_subject + (" (%s)" % payment_type.upper())
    email.subject = email_subject

    try:
        email.send()
    except:
        log.error("Could not send out email, out of quota?")

EMAIL_BLAST_TEXT = """
Hi {first_name}

Thank you for booking via GMovies.

Skip the lines and go straight to the cinema door to present your e-ticket!
No need to print or queue at the ticket counter.

{email_msg}

        Movie : {movie}
        Cinema : {theater_and_cinema_name}
        Date : {show_date}
        Showtime: {show_time}
        Seat(s): {seats}

        Ticket Price: {ticket_price}{popcorn_price}
        Convenience Fee: {convenience_fee}
        Subtotal: {subtotal}
        Promo Discount: {promo_discount}
        Total: {total}

For any concerns, you may reach us via email at support@gmovies.ph or call us at (02) 211 9792 or (0917) 543 4594.

Get inside, not in line!

- GMovies Team
"""

EB_TEXT_WEB = """
Your e-ticket is automatically sent to the email address you provided.
"""

EB_TEXT_APP = """
Your e-ticket is automatically saved in the app under the "Ticket" tab in the "My Account" section and on your phone's gallery. Present your GMovies e-ticket at the cinema door and enjoy the movie!
"""

def send_email_blasts_txdone(email_address, first_name, platform, ticket):
    details = {}
    details['first_name'] = first_name
    details['email_msg'] = EB_TEXT_WEB if 'website' == platform else EB_TEXT_APP

    details['movie'] = ticket.extra.get('movie_canonical_title', "")
    if not details['movie']:
        details['movie'] = ticket.extra.get('movie_title', "")

    details['theater_and_cinema_name'] = ""
    if 'theater_and_cinema_name' in ticket.extra and ticket.extra['theater_and_cinema_name']:
        if list == type(ticket.extra['theater_and_cinema_name']):
            details['theater_and_cinema_name'] = ticket.extra['theater_and_cinema_name'][0]
        else:
            details['theater_and_cinema_name'] = ticket.extra['theater_and_cinema_name']

    details['show_date'] = ""
    details['show_time'] = ""
    if 'show_datetime' in ticket.extra and ticket.extra['show_datetime']:
        details['show_date'], details['show_time'] = ticket.extra['show_datetime'].split(" ", 1)

    details['seats'] = ticket.extra.get('seats', "")
    details['ticket_price'] = ticket.extra.get('per_ticket_price', '0.00')

    pp = ticket.extra.get('popcorn_price', '0.00')
    details['popcorn_price'] = '' if ('0.00' == pp or pp is None or 'None' == pp ) else "\n        Premier Seat: %s" % (pp)

    details['convenience_fee'] = ticket.extra.get('reservation_fee', '0.00')
    details['subtotal'] = ticket.extra.get('original_total_amount', '0.00')
    details['promo_discount'] = ticket.extra.get('total_discount', '0.00')
    details['total'] = ticket.extra.get('total_amount', '0.00')

    email = mail.EmailMessage()
    email.sender = BLAST_MAIL_FROM_TEMPLATE % 'gmovies-noreply'
    email.to = email_address
    email.body = EMAIL_BLAST_TEXT.format(**details)
    email.subject = "GMovies - Transaction Confirmation"

    try:
        email.send()
    except:
        log.error("Could not send out email, out of quota?")


EPLUS_BALANCE_INQUIRY_TEXT = """
This is an auto generated email sent by the system. No need to reply.

EPlus Card Number and Remaining Balance.
{eplus_balance}
"""

def eplus_balance_notification(**kwargs):
    email = mail.EmailMessage()
    email.sender = MAIL_FROM_TEMPLATE % 'cron-noreply'
    email.to = CMS_ADMINISTRATORS
    email.body = EPLUS_BALANCE_INQUIRY_TEXT.format(**kwargs)
    email.subject = "EPlus Balance Inquiry"

    try:
        email.send()
    except Exception, e:
        log.warn("ERROR!, eplus_balance_notification, failed to send email...")
        log.error(e)

REFUND_CLAIMCODE_TRANSACTION_NOTICE_TEXT = """
<p style="font-size:14px;font-weight:500;padding:10px;color:#222">
<b>Hi {full_name},</b>
<br><br>
Thank you for patiently waiting for our update.
<br>
We have successfully voided your transaction, and your claim code has been reactivated.
<br><br>
CLAIM CODE: {claim_code}
<br><br>
Terms and conditions still apply. For any concerns, you may reach us via email at support@gmovies.ph or call us at (02) 211 9792 or (0917) 543 4594.
<br><br>
Get inside, not in line!
<br><br>
- GMovies
</p>
"""

def send_refund_claimcodetx_notification(email_address, full_name, claim_code):
    log.debug("send_refund_claimcodetx_notification, refund claim-code transaction, sending email notification...")

    try:
        # get content of html email template
        content = ''
        with open(PATH_TO_EMAIL_TEMPLATE, 'r') as content_file:
            content = content_file.read()

        refund_base_message = REFUND_CLAIMCODE_TRANSACTION_NOTICE_TEXT.format(full_name=full_name, claim_code=claim_code)
        log.info(refund_base_message)

        email_message = content.replace('email_message', refund_base_message)

        email = mail.EmailMessage()
        email.sender = 'GMovies <support@gmovies.ph>'
        email.to = email_address
        email.bcc = REFUND_USER_BCC
        email.html = email_message
        email.subject = "[GMovies] Refund Notification"
        email.send()
    except Exception, e:
        log.warn("Could not send out email, out of quota?")
        log.error(e)

def send_email_support(tx, error_msg, support_type=None):
    details = {}
    details['email_address']           = tx.payment_info['email']
    details['mobnum']                  = ''

    if 'mobile' in tx.payment_info and tx.payment_info['mobile']:
        details['mobnum'] = tx.payment_info['mobile']
    elif tx.user_info and 'mobile' in tx.user_info and tx.user_info['mobile']:
        details['mobnum'] = tx.user_info['mobile']

    if hasattr(tx, 'ticket') and hasattr(tx.ticket, 'extra'):
        details['movie']                   = tx.ticket.extra['movie_canonical_title']
        if list == type(tx.ticket.extra['theater_and_cinema_name']):
            details['theater_and_cinema_name'] = tx.ticket.extra['theater_and_cinema_name'][0]
        else:
            details['theater_and_cinema_name'] = tx.ticket.extra['theater_and_cinema_name']
        details['show_date_time']          = tx.ticket.extra['show_datetime']
        details['seats']                   = tx.ticket.extra['seats']
    elif tx.workspace:
        details['movie']                   = tx.workspace['schedules.movie'][0].canonical_title
        if list == type(tx.workspace['theaters.theater_cinema_name']):
            details['theater_and_cinema_name'] = tx.workspace['theaters.theater_cinema_name'][0]
        else:
            details['theater_and_cinema_name'] = tx.workspace['theaters.theater_cinema_name']
        details['show_date_time']          = tx.workspace['schedules.show_datetime'][0].strftime('%m/%d/%Y %I:%M %p')
        details['seats']                   = tx.workspace['seats::imploded']
    details['app_id']                  = get_application_id()
    details['device_id']               = tx.key.parent().id()
    details['tx_id']                   = tx.key.id()
    details['error_msg']               = error_msg if error_msg else 'Check logs for this transaction.'

    email_to      = SUPPORT_MAIL_TO
    if 'Greenhills' in details['theater_and_cinema_name']:
        email_to = email_to + ", " + TE_SUPPORT_EMAIL
    email_body    = SUPPORT_MAIL_BODY.format(**details)
    email_subject = SUPPORT_MAIL_SUBJECT
    email_sender  = SUPPORT_MAIL_FROM

    if support_type:
        if 'TE' == support_type:
            email_to      = SUPPORT_MAIL_TO_TE
            email_body    = SUPPORT_MAIL_BODY_TE.format(**details)
            email_subject = SUPPORT_MAIL_SUBJECT_TE
        elif 'INFO' == support_type:
            email_body    = SUPPORT_MAIL_BODY_INFO.format(**details)
            email_subject = SUPPORT_MAIL_SUBJECT_INFO
            email_sender  = SUPPORT_MAIL_FROM_INFO
    log.info(email_body)

    email         = mail.EmailMessage()
    email.sender  = email_sender
    email.to      = email_to
    # email.bcc     = SUPPORT_MAIL_BCC
    email.body    = email_body
    email.subject = email_subject

    try:
        email.send()
    except Exception, e:
        log.warn("ERROR!, send_email_support, Could not send out email, out of quota?")
        log.error(e)

EMAIL_REFUND_PAYMENT_GW_TEXT = """
<p style="font-size:14px;font-weight:500;padding:10px;color:#222">
Hi {payment_gw},
<br><br>
Please refund the transaction below.<br>
This has been approved by {theater_name}.
<br><br>
Customer : {customer_name} <br>
Email Address : {email} <br>
Request ID : {request_id} <br>
Total Price : {total_amount} <br>
<br>
Please confirm with us once done, and send a confirmation email to customer at {email}.
<br><br>
Thank you very much.
<br><br>
Regards,
<br>
GMovies
</p>
"""

EMAIL_REFUND_PAYMENT_GW_NOTIF_TEXT = """
<p style="font-size:14px;font-weight:500;padding:10px;color:#222">
Hi {payment_gw},
<br><br>
The transaction below has been refunded.
<br><br>
Customer : {customer_name} <br>
Email Address : {email} <br>
Request ID : {request_id} <br>
Total Price : {total_amount} <br>
<br>
Thank you very much.
<br><br>
Regards,
<br>
GMovies
</p>
"""

def send_email_refund_payment_gateway(tx, org_uuid, payment_gateway='paynamics', theater_code='SM'):
    # get content of html email template
    content = ''
    with open(PATH_TO_EMAIL_TEMPLATE, 'r') as content_file:
        content = content_file.read()

    payment_gateway_mapping = {'paynamics':'Paynamics Team', 'migs':'Migs Team', 'ipay88':'Ipay88 Team'}
    details = {}
    details['theater_name'] = tx.ticket.extra['theater_name']
    details['customer_name'] = ''
    details['payment_gw'] = payment_gateway_mapping[payment_gateway]
    if 'fname' in tx.payment_info:
        details['customer_name'] += tx.payment_info['fname']
    if 'lname' in tx.payment_info:
        details['customer_name'] += ' ' + tx.payment_info['lname']
    details['request_id'] = tx.reservation_reference.split('~')[-1] if str(org_uuid) not in [str(orgs.SM_MALLS), str(orgs.ROBINSONS_MALLS), str(orgs.CINEMA76_MALLS), str(orgs.GLOBE_EVENTS)] else tx.reservation_reference
    details['total_amount'] = tx.ticket.extra['total_amount']
    details['email']        = tx.payment_info['email']
    refund_base_message = EMAIL_REFUND_PAYMENT_GW_TEXT.format(**details)

    email_message = content.replace('email_message', refund_base_message)

    email         = mail.EmailMessage()
    email.sender  = REFUND_PAYMENT_GW_MAIL_FROM
    email_to_str  = "REFUND_PAYMENT_GW_TO_" + payment_gateway.upper()
    if 'paynamics' == payment_gateway :
        email_to_str += "_" + theater_code
        refund_base_message = EMAIL_REFUND_PAYMENT_GW_NOTIF_TEXT.format(**details)
        email_message = content.replace('email_message', refund_base_message)
    log.info(refund_base_message)
    email.to      = eval(email_to_str)
    email.bcc     = REFUND_PAYMENT_GW_BCC
    email.html    = email_message
    email.subject = REFUND_PAYMENT_GW_MAIL_SUBJECT

    try:
        email.send()
        return 'success', "Email to Paynamics sent."
    except Exception, e:
        log.warn("ERROR!, send_email_refund_payment_gateway, Could not send out email, out of quota?")
        log.error(e)
        return 'error', "Email to Paynamics not sent."

EMAIL_REFUND_USER_TEXT = """
<p style="font-size:14px;font-weight:500;padding:10px;color:#222">
Hi {first_name},
<br><br>
{message_body}
<br><br>
Movie : {movie} <br>
Cinema : {theater_and_cinema_name} <br>
Date : {show_date} <br>
Showtime: {show_time} <br>
Seat(s): {seats} <br>
Ticket Code: {ticket_code} <br>
Total Amount: {total}
<br><br>
{additional_message}
Terms and conditions still apply. For any concerns, you may reach us via email at support@gmovies.ph or call us at (02) 211 9792 or (0917) 543 4594.
<br><br>
Get inside, not in line!
<br><br>
- GMovies
</p>
"""

EMAIL_REFUND_USER_TEXT_REFUNDED    = "We have successfully refunded your transaction."
EMAIL_REFUND_USER_TEXT_REFUNDED_ADDTL_GCASH = """
Please be advised that you will receive the refunded amount within 24 hours.
<br>
If there is a delay, please contact GCash Support at 2882.
<br><br>
"""
EMAIL_REFUND_USER_TEXT_REFUNDED_ADDTL_PESOPAY = """
Please be advised that it takes 7 to 10 banking days for the amount to reflect on your account.
<br>
"""

EMAIL_REFUND_USER_TEXT_REFUNDED_ADDTL_PAYNAMICS = EMAIL_REFUND_USER_TEXT_REFUNDED_ADDTL_PESOPAY

EMAIL_REFUND_USER_TEXT_NOTREFUNDED = """
Greetings from GMovies!
<br><br>
Thank you for reaching out to us about your concern.
<br>
Your transaction below is currently being processed for refund.
"""

EMAIL_REFUND_USER_TEXT_NOTREFUNDED_ADDTL = """
Kindly wait for an update once refund has been inititated.
<br>
Please be advised that it takes 7 to 10 banking days for the amount to reflect on your account.
<br><br>
"""

def send_email_refund_user(tx, payment_gateway, is_refunded=False):
    # get content of html email template
    content = ''
    with open(PATH_TO_EMAIL_TEMPLATE, 'r') as content_file:
        content = content_file.read()

    details = {}
    details['first_name'] = tx.user_info.get('first_name', "")
    details['message_body'] = EMAIL_REFUND_USER_TEXT_REFUNDED if is_refunded else EMAIL_REFUND_USER_TEXT_NOTREFUNDED

    ticket = tx.ticket
    details['movie'] = ticket.extra.get('movie_canonical_title', "")
    if not details['movie']:
        details['movie'] = ticket.extra.get('movie_title', "")

    details['theater_and_cinema_name'] = ""
    if 'theater_and_cinema_name' in ticket.extra and ticket.extra['theater_and_cinema_name']:
        if list == type(ticket.extra['theater_and_cinema_name']):
            details['theater_and_cinema_name'] = ticket.extra['theater_and_cinema_name'][0]
        else:
            details['theater_and_cinema_name'] = ticket.extra['theater_and_cinema_name']

    details['show_date'] = ""
    details['show_time'] = ""
    if 'show_datetime' in ticket.extra and ticket.extra['show_datetime']:
        details['show_date'], details['show_time'] = ticket.extra['show_datetime'].split(" ", 1)

    details['seats']        = ticket.extra.get('seats', "")
    details['ticket_code']  = ticket.code
    details['total'] = ticket.extra.get('total_amount', '0.00')
    details['additional_message'] = EMAIL_REFUND_USER_TEXT_NOTREFUNDED_ADDTL
    if is_refunded:
        details['additional_message'] = ''
        if payment_gateway in ['gcash', 'pesopay','paynamics']:
            details['additional_message'] = eval("EMAIL_REFUND_USER_TEXT_REFUNDED_ADDTL_" + payment_gateway.upper())

    refund_base_message = EMAIL_REFUND_USER_TEXT.format(**details)
    log.info(refund_base_message)

    email_message = content.replace('email_message', refund_base_message)

    email         = mail.EmailMessage()
    email.sender  = REFUND_USER_MAIL_FROM
    email.to      = tx.payment_info['email']
    email.bcc     = REFUND_USER_BCC
    email.html    = email_message
    email.subject = REFUND_USER_MAIL_SUBJECT

    try:
        email.send()
        return 'success', "Email to Customer sent."
    except Exception, e:
        log.warn("ERROR!, send_email_refund_user, Could not send out email, out of quota?")
        log.error(e)
        return 'error', "Email to Customer not sent."

def send_ayala_failed_callback_notification(freshtime):

    details = {}
    details['freshtime'] = freshtime

    log.info("INFO_TIME:, %s" % (freshtime))
    log.info("INFO_DETAILS:, %s" % (details))

    email_body = AYALA_CALLBACK_AUTH_BODY.format(**details)

    email         = mail.EmailMessage()
    email.sender  = AYALA_CALLBAK_AUTH_FROM
    email.to      = AYALA_CALLBAK_AUTH_TO
    email.html    = email_body
    email.subject = AYALA_CALLBACK_AUTH_SUBJECT

    log.info("INFO_BODY, %s" % (email_body))

    try:
        email.send()

        log.info("INFO!, send_ayala_failed_callback_notification, Email to Support sent.")

        return 'success', "Email to Support sent."
    except Exception, e:

        log.warn("ERROR!, send_ayala_failed_callback_notification, Could not send out email, out of quota?")
        log.error(e)

        return 'error', "Email to Support not sent."