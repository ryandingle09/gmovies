import logging
import string
import re

from uuid import uuid4 as uuid_gen

from google.appengine.api import taskqueue, memcache
from google.appengine.ext import ndb

from gmovies.admin.email import movie_title_bank_heuristics_notification, movie_title_bank_multiple_correlation_notification
from gmovies.heuristics.movies import get_all_correlation_titles, get_canonical_title, get_searchable_words, get_searchable_title
from gmovies.models import MovieTitles, RottenTomatoesDetails
from gmovies.util import roman_to_numeric


log = logging.getLogger(__name__)

CORRELATION_VERSION = 4
BATCH_SIZE = 50


def rottentomatoes_transform_movies(movie_dictionary_list):
    movie_batch = []
    movie_list = []

    try:
        correlator = MovieTitleCorrelation()

        for movie_dictionary in movie_dictionary_list:
            if 'title' in movie_dictionary:
                movie_dictionary['title'] = movie_dictionary['title'].replace(u"\u2019", "'").replace(u"\u2018", "'")
                log.info("Title Bank: Transforming Rotten Tomatoes Details of {} ".format(movie_dictionary['title']))

                movie_title, correlation_titles = correlator.correlate(movie_dictionary)
                rt_details = get_rt_details(movie_dictionary) if 'id' in movie_dictionary else None

                if not movie_title:
                    continue

                if rt_details:
                    movie_title.rt_details = rt_details
                else:
                    log.info("SKIPPING {} , Couldn't find Rotten Tomatoes ID from feeds.".format(movie_dictionary['title']))

                if movie_title.is_title_locked:
                    log.info("SKIPPING {} , Title is locked.".format(movie_dictionary['title']))
                else:
                    movie_title.title = get_canonical_title(movie_dictionary['title'])
                    movie_title.correlation_title = correlation_titles
                    movie_title.searchable_words = get_searchable_words(movie_dictionary['title'])
                    movie_title.searchable_title = get_searchable_title(movie_dictionary['title'])

                movie_batch.append(movie_title)
                movie_list.append(movie_title)

                if len(movie_batch) == BATCH_SIZE:
                    ndb.put_multi(movie_batch)
                    movie_batch = []

        ndb.put_multi(movie_batch)

        log.info("Transformation movie titles for Title Bank completed...")
        log.info("Total Updates: %d" % len(movie_list))
    except Exception, e:
        log.warn('Exception while processing movie title for Title Bank, flushing existing movie_batch and movie_list...')
        log.error("ERROR!, Transforming movie titles for Title Bank...")
        log.error(e)
        log.info("Total Updates: %d" % len(movie_list))
        ndb.put_multi(movie_batch)

    movie_title_bank_heuristics_notification(new_movie_titles=correlator.ADDED_NEW_MOVIE_TITLE,
            existing_from_cache=correlator.EXISTING_MOVIE_TITLES_FROM_CACHE,
            existing_from_datastore=correlator.EXISTING_MOVIE_TITLES_FROM_DATASTORE,
            correlated_by_title=correlator.DS_CORRELATED_BY_TITLE,
            correlated_by_id=correlator.DS_CORRELATED_BY_RT_ID,
            cached_by_id=correlator.CACHE_MATCH_BY_ID,
            cached_by_title=correlator.CACHE_MATCH_BY_TITLE,
            multi_correlations=correlator.MULTIPLE_CORRELATION,
            movie_titles_with_multiple_correlation='\n'.join(correlator.MOVIE_TITLES_WITH_MULTIPLE_CORRELATION))

    return movie_list


def get_rt_details(movie_dictionary):
    rt_details = RottenTomatoesDetails(alternate_ids={}, casts={},
            release_dates={}, ratings={}, posters={})

    if 'title' in movie_dictionary:
        rt_details.rt_title = movie_dictionary['title']

    if 'id' in movie_dictionary:
        rt_details.rt_id = str(movie_dictionary['id'])

    if 'alternate_ids' in movie_dictionary:
        rt_details.alternate_ids = movie_dictionary['alternate_ids']

    if 'year' in movie_dictionary:
        if movie_dictionary['year'] and type(movie_dictionary['year']) == int:
            rt_details.year = movie_dictionary['year']

    if 'abridged_cast' in movie_dictionary:
        rt_details.casts = movie_dictionary['abridged_cast']

    if 'release_dates' in movie_dictionary:
        rt_details.release_dates = movie_dictionary['release_dates']

    if 'synopsis' in movie_dictionary:
        rt_details.synopsis = movie_dictionary['synopsis']

    if 'runtime' in movie_dictionary:
        if movie_dictionary['runtime'] and type(movie_dictionary['runtime']) == int:
            rt_details.runtime = movie_dictionary['runtime']

    if 'ratings' in movie_dictionary:
        rt_details.ratings = movie_dictionary['ratings']

    if 'mpaa_rating' in movie_dictionary:
        rt_details.mpaa_rating = movie_dictionary['mpaa_rating']

    if 'posters' in movie_dictionary:
        rt_details.posters = movie_dictionary['posters']

    return rt_details


class MovieTitleCorrelation:
    def __init__(self):
        self.ADDED_NEW_MOVIE_TITLE = 0
        self.EXISTING_MOVIE_TITLES_FROM_CACHE = 0
        self.EXISTING_MOVIE_TITLES_FROM_DATASTORE = 0
        self.DS_CORRELATED_BY_RT_ID = 0
        self.DS_CORRELATED_BY_TITLE = 0
        self.CACHE_MATCH_BY_ID = 0
        self.CACHE_MATCH_BY_TITLE = 0
        self.MULTIPLE_CORRELATION = 0

        self.MOVIE_TITLES_WITH_MULTIPLE_CORRELATION = []

        self.movie_title_cache = {}

    def cached(self, key):
        if key not in self.movie_title_cache:
            self.movie_title_cache[key] = key.get()

        return self.movie_title_cache[key]

    def correlate(self, movie_dictionary):
        rt_id = str(movie_dictionary['id']) if 'id' in movie_dictionary else None
        title = movie_dictionary['title']

        if 'correlation_title' in movie_dictionary:
            correlation_titles = movie_dictionary['correlation_title']
        else:
            correlation_titles = get_all_correlation_titles(movie_dictionary['title'])

        cached_rt_id = memcache.get("movie_title::rottentomatoes_id::%s" % rt_id)
        cached_titles = filter(None, [memcache.get("movie_title::correlation_title::%s" % t) for t in correlation_titles])

        if cached_rt_id or cached_titles:
            self.EXISTING_MOVIE_TITLES_FROM_CACHE += 1

            if cached_rt_id:
                log.debug("Matched against cached feed rottentomatoes ID %s" % rt_id)

                movie_title = self.cached(ndb.Key(urlsafe=cached_rt_id))
                self.CACHE_MATCH_BY_ID += 1
            else:
                log.info("Matched against cached correlation title(s) %s" % correlation_titles)

                movie_title = self.cached(ndb.Key(urlsafe=cached_titles[0]))
                self.CACHE_MATCH_BY_TITLE += 1

            log.debug("Found correlated item in cache: %s" % movie_title.key.id())

            return movie_title, correlation_titles

        if rt_id:
            queryset = MovieTitles.query(ndb.OR(
                            MovieTitles.rt_details.rt_id == rt_id,
                            MovieTitles.correlation_title.IN(correlation_titles),
                            MovieTitles.secondary_titles.correlation_title_version_one.IN(correlation_titles),
                            MovieTitles.secondary_titles.correlation_title_version_two.IN(correlation_titles),
                            MovieTitles.secondary_titles.correlation_title_version_three.IN(correlation_titles),
                            MovieTitles.secondary_titles.correlation_title_version_four.IN(correlation_titles)),
                    MovieTitles.is_active == True)

            if queryset.count() == 1:
                self.DS_CORRELATED_BY_RT_ID += 1
        else:
            queryset = MovieTitles.query(ndb.OR(
                            MovieTitles.correlation_title.IN(correlation_titles),
                            MovieTitles.secondary_titles.correlation_title_version_one.IN(correlation_titles),
                            MovieTitles.secondary_titles.correlation_title_version_two.IN(correlation_titles),
                            MovieTitles.secondary_titles.correlation_title_version_three.IN(correlation_titles),
                            MovieTitles.secondary_titles.correlation_title_version_four.IN(correlation_titles)),
                    MovieTitles.is_active == True)

            if queryset.count() == 1:
                self.DS_CORRELATED_BY_TITLE += 1

        if queryset.count() == 1:
            log.debug("Found movie title in Title Bank with movie title: %s or rt_id: %s..." % (movie_dictionary['title'], rt_id))
            self.EXISTING_MOVIE_TITLES_FROM_DATASTORE += 1

            movie_title = queryset.get()

            if rt_id and rt_id is not None:
                # We were correlated by rottentomatoes ID, cache the rottentomatoes ID
                memcache.set("movie_title::rottentomatoes_id::%s" % rt_id, movie_title.key.urlsafe())

            # Cache the correlation_title as well for this entry
            for t in movie_title.correlation_title:
                memcache.set("movie_title::correlation_title::%s" % t, movie_title.key.urlsafe())
        elif queryset.count() == 0:
            log.debug("Couldn't find movie title in TItle Bank with movie title: %s or rt_id: %s..." % (movie_dictionary['title'], rt_id))
            self.ADDED_NEW_MOVIE_TITLE += 1

            movie_title = MovieTitles(key=ndb.Key(MovieTitles, str(uuid_gen())))
            movie_title.is_active = True

            # Cache the correlation_title as well for this entry
            for t in correlation_titles:
                memcache.set("movie_title::correlation_title::%s" % t, movie_title.key.urlsafe())

            self.movie_title_cache[movie_title.key] = movie_title
        else:
            log.warn("FIXME!, Found multiple movie title in Title Bank with movie title: %s or rt_id: %s..." % (movie_dictionary['title'], rt_id))
            self.MULTIPLE_CORRELATION += 1

            self.MOVIE_TITLES_WITH_MULTIPLE_CORRELATION.append('movie_title: %s' % movie_dictionary['title'])

            movie_title_bank_multiple_correlation_notification(movie_titles_with_multiple_correlation='\n'.join(self.MOVIE_TITLES_WITH_MULTIPLE_CORRELATION))

            raise ValueError("Multiple correlated movie titles for %s" % movie_dictionary)

        return movie_title, correlation_titles