################################################################################
#
# Deprecated (10/21/2016). Remove or delete this file in the future.
# No need for separate Movies and Schedules Heuristics for different cinema partners.
#
################################################################################


import datetime
import json
import logging

from decimal import Decimal
from uuid import uuid4 as uuid_gen

from google.appengine.ext import ndb

from gmovies import models, orgs
from gmovies.admin.email import rockwell_schedule_heuristics_notification
from gmovies.heuristics import movies
from gmovies.heuristics.rottentomatoes import get_all_correlation_titles


log = logging.getLogger(__name__)


def rockwell_transform_schedules(rockwell_schedules):
    schedule_list = []

    try:
        log.info('Transforming Rockwell Schedules...')
        correlator = RockwellScheduleCorrelation()

        for rockwell_schedule in rockwell_schedules:
            schedule = correlator.convert_schedule(rockwell_schedule)
            schedule_list.append(schedule)

        ndb.put_multi(filter(None, schedule_list))

        rockwell_schedule_heuristics_notification(
                    rockwell_new_schedules=correlator.ROCKWELL_NEW_SCHEDULE_ADDED,
                    existing_from_cache=correlator.EXISTING_ROCKWELL_SCHEDULE_FROM_CACHE,
                    existing_from_datastore=correlator.EXISTING_ROCKWELL_SCHEDULE_FROM_DATASTORE,
                    cached_by_code=correlator.CACHE_MATCH_BY_CODE,
                    fetch_by_code=correlator.DS_MATCH_BY_CODE)

        log.info('correlator.ROCKWELL_NEW_SCHEDULE_ADDED: %s' % str(correlator.ROCKWELL_NEW_SCHEDULE_ADDED))
        log.info('correlator.EXISTING_ROCKWELL_SCHEDULE_FROM_DATASTORE: %s' % str(correlator.EXISTING_ROCKWELL_SCHEDULE_FROM_DATASTORE))

        return schedule_list
    except Exception, e:
        log.info('ERROR!, Transforming Rockwell Schedules...')
        log.warn('Exception while processing schedules, flushing existing schedule_list...')
        log.info(e)
        ndb.put_multi(filter(None, schedule_list))

        return schedule_list


class RockwellScheduleCorrelation:
    def __init__(self):
        self.ROCKWELL_NEW_SCHEDULE_ADDED = 0
        self.EXISTING_ROCKWELL_SCHEDULE_FROM_CACHE = 0
        self.EXISTING_ROCKWELL_SCHEDULE_FROM_DATASTORE = 0
        self.CACHE_MATCH_BY_CODE = 0
        self.DS_MATCH_BY_CODE = 0
        self.ROCKWELL_SHOWTIMES_ID = []

    def convert_schedule(self, schedule_dict):
        schedule_id = schedule_dict['schedule_id']
        movie_id = schedule_dict['movie_id']
        cinema_id = schedule_dict['cinema_id']
        cinema_name = schedule_dict['cinema_name']
        rockwell_cinema_name = schedule_dict['rockwell_cinema_name']
        variant = schedule_dict['variant']
        seating_type = schedule_dict['seating_type']
        price = Decimal(schedule_dict['price'])
        showtimes = schedule_dict['showtimes']
        show_date = parse_show_date(schedule_dict['show_date'])
        uuid = schedule_dict['uuid']

        rockwell_theaterorg_key = ndb.Key(models.TheaterOrganization, str(uuid))
        feed_key = ndb.Key(models.Feed, str(uuid))
        theater = models.Theater.query(models.Theater.org_theater_code=='RWPP', ancestor=rockwell_theaterorg_key).get()

        movie = models.Movie.query(models.Movie.movie_correlation.org_key == feed_key,
                models.Movie.movie_correlation.movie_id == str(movie_id),
                models.Movie.is_inactive == False).get()

        if show_date is None:
            log.info('SKIP!, Wrong Show Date Format: %s...' % schedule_dict['show_date'])

            return None

        if movie is None:
            log.info("SKIP!, we can't match movie...")

            return None

        if theater is None:
            log.info("SKIP!, we can't match theater...")

            return None

        if int(cinema_id) not in [c.rockwell_cinema_id for c in theater.cinemas]:
            log.info("SKIP!, we can't match cinema...")

            return None

        movie_key = movie.key
        theater_key = theater.key
        cinema = get_rockwell_cinema_entity(theater, int(cinema_id))
        schedule = models.Schedule.find_schedule(theater_key, cinema.name, show_date)

        if not schedule:
            log.info("CREATE!, Schedule. Movie: %s, Cinema: %s, ShowDate: %s..." % (movie.canonical_title, cinema.name, show_date))
            schedule = models.Schedule.create_schedule(
                                    cinema=cinema.name, parent=theater_key,
                                    show_date=show_date)
            self.ROCKWELL_NEW_SCHEDULE_ADDED += 1

        if schedule_id not in schedule.rockwell_movie_schedule_ids:
            schedule.rockwell_movie_schedule_ids.append(schedule_id)

        schedule, rockwell_showtimes_ids = create_schedule_slots(schedule, schedule_id, schedule_dict['show_date'], showtimes,
                movie_key, variant, seating_type, price)

        self.ROCKWELL_SHOWTIMES_ID.extend(rockwell_showtimes_ids)

        schedule = delete_nonexisting_rockwell_showtimes(schedule, schedule_id, self.ROCKWELL_SHOWTIMES_ID)

        schedule.put()

        self.EXISTING_ROCKWELL_SCHEDULE_FROM_DATASTORE += 1

        return schedule


def create_schedule_slots(schedule, rockwell_schedule_id, show_date, showtimes, movie_key, variant, seating_type, price):
    rockwell_showtimes_ids = []

    for showtime in showtimes:
        show_datetime = parse_screening(show_date, showtime['screenTime'])
        screening_time = show_datetime.time()
        showtime_id = str(showtime['id'])
        movie = movie_key.get()

        if screening_time is None:
            log.info('SKIP!, Wrong Screening Time Format: %s %s...' % (show_date, showtime['screenTime']))
            continue

        if showtime_id not in schedule._feed_codes():
            log.info("CREATE!, ScheduleSlot. FeedCode: %s, Movie: %s, ScreenTime: %s..." % (showtime_id, movie.canonical_title, screening_time))
            slot = models.ScheduleTimeSlot()
            slot.feed_code = showtime_id
            slot.start_time = screening_time
            slot.movie = movie_key
            slot.variant = variant
            slot.seating_type = seating_type.title()
            slot.price = price
            slot.rockwell_movie_schedule_id = rockwell_schedule_id
            schedule.slots.append(slot)
        else:
            log.info("UPDATE!, ScheduleSlot. FeedCode: %s, Movie: %s, ScreenTime: %s..." % (showtime_id, movie.canonical_title, screening_time))
            slot = schedule.get_feed_code(showtime_id)
            slot.start_time = screening_time
            slot.movie = movie_key
            slot.variant = variant
            slot.seating_type = seating_type.title()
            slot.price = price
            slot.rockwell_movie_schedule_id = rockwell_schedule_id

        rockwell_showtimes_ids.append(showtime_id)

    return schedule, rockwell_showtimes_ids


def delete_nonexisting_rockwell_showtimes(schedule, rockwell_schedule_id, rockwell_showtimes_ids):
    slots = [slot for slot in schedule.slots]

    for index, slot in enumerate(slots):
        movie = slot.movie.get()

        if slot.feed_code not in rockwell_showtimes_ids or slot.rockwell_movie_schedule_id not in schedule.rockwell_movie_schedule_ids:
            log.info("DELETE!, ScheduleSlot. FeedCode: %s, Movie: %s, Screentime: %s..." % (slot.feed_code, movie.canonical_title, slot.start_time))
            schedule.slots.remove(slot)

    return schedule


def get_rockwell_cinema_entity(theater, rockwell_cinema_id):
    cinemas = theater.cinemas
    for c in cinemas:
        if rockwell_cinema_id == c.rockwell_cinema_id:
            return c


def parse_show_date(show_date):
    try:
        show_date = datetime.datetime.strptime(show_date, '%Y-%m-%d')
    except ValueError:
        show_date = None

    return show_date


def parse_screening(show_date, showtime):
    screening_datetime = show_date.strip() + ' ' + showtime.strip()
    try:
        screening = datetime.datetime.strptime(screening_datetime, '%Y-%m-%d %H:%M:%S')
    except ValueError:
        try:
            screening = datetime.datetime.strptime(screening_datetime, '%Y-%m-%d %H:%M')
        except ValueError:
            screening = None

    return screening
