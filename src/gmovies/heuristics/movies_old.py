import logging
log = logging.getLogger(__name__)

import string, re
from operator import attrgetter

from flask import url_for

from gmovies.feeds import rottentomatoes
from gmovies.models import Movie, FeedCorrelation, Feed
from uuid import uuid4 as uuid_gen
from google.appengine.ext import ndb
from google.appengine.api import taskqueue, memcache
from gmovies import orgs
from gmovies.util import roman_to_numeric
from gmovies.admin import movie_heuristics_notification

from gmovies.util.task import is_from_flixster

CORRELATION_VERSION = 2
BATCH_SIZE = 20

def build_whitelist_blacklist_check(blacklist, whitelist):
    log.debug("Blacklist: %s, whitelist: %s" % (blacklist, whitelist))
    def check_field(m, field):
        return field in m and (field not in blacklist) and (field in whitelist or not whitelist)

    return check_field

def transform(per_org_payloads):
    feed_ids = per_org_payloads.keys()
    feeds = ndb.get_multi([ ndb.Key(Feed, feed_id) for feed_id in feed_ids ])

    prioritized_feeds = reversed(sorted(feeds, key=attrgetter('heuristics_priority')))

    payload_movie_matches = {}
    key_to_movie = {}
    correlator = MovieCorrelation()

    for f in prioritized_feeds:
        feed_id = f.key.id()
        field_blacklist = f.movie_fields_blacklist
        field_whitelist = f.movie_fields_whitelist
        check_field = build_whitelist_blacklist_check(field_blacklist, field_whitelist)

        movie_payload = per_org_payloads[feed_id]
        for m in movie_payload:
            movie = correlator.correlate(f.key, m, f)
            if not movie:
                continue

            movie_key = movie.key.id()
            movie.canonical_title = get_canonical_title(m['movie_title'])

            if movie_key not in key_to_movie:
                key_to_movie[movie_key] = movie

            variant = get_variant(m)

            if not check_field(m, 'synopsis'):
                m.pop('synopsis', None)
            if not check_field(m, 'cast'):
                m.pop('cast', None)
            if not check_field(m, 'release_date'):
                m.pop('release_date', None)
            if not check_field(m, 'genre'):
                m.pop('genre', None)
            if not check_field(m, 'runtime_mins'):
                m.pop('runtime_mins', None)

            if not check_field(m, 'advisory_rating'):
                m.pop('advisory_rating', None)

            if check_field(m, 'ratings'):
                build_ratings_property(movie, m, feed_id)
            movie.bind_org_title(feed_id, variant, m['movie_title'])
            
            if movie_key not in payload_movie_matches:
                payload_movie_matches[movie_key] = m
            else:
                payload_movie_matches[movie_key].update(m)

            if 'image_url' in m:
                image_url =  is_from_flixster(m['image_url'])
                payload_movie_matches[movie_key]['image_params'] = {
                    'org_uuid' : f.key.id(),
                    'image_url': image_url,
                    'parent': movie.key.urlsafe()
                }

    movie_heuristics_notification(new_movies=correlator.NEW_MOVIES_ADDED,
                                  existing_from_cache=correlator.EXISTING_MOVIES_FROM_CACHE,
                                  existing_from_datastore=correlator.EXISTING_MOVIES_FROM_DATASTORE,
                                  correlated_by_title=correlator.DS_CORRELATED_BY_TITLE,
                                  correlated_by_id=correlator.DS_CORRELATED_BY_ORG_AND_ID,
                                  cached_by_id=correlator.CACHE_MATCH_BY_ID,
                                  cached_by_title=correlator.CACHE_MATCH_BY_TITLE,
                                  movies_not_created=correlator.FEED_NO_CREATION,
                                  multi_correlations=correlator.MULTIPLE_CORRELATION)

    return transform_merged_payloads(map(lambda (m_key, m): (key_to_movie[m_key], m),
                                         payload_movie_matches.items()))

    
def transform_merged_payloads(raw_movies):
    movie_batch = []
    movie_list = []

    for movie,m in raw_movies:
        if 'synopsis' in m:
            movie.synopsis = m['synopsis']
        if 'cast' in m:
            movie.cast = m['cast']
        if 'release_date' in m:
            movie.release_date = m['release_date']
        if 'genre' in m:
            movie.genre = m['genre']
        if 'runtime_mins' in m:
            movie.runtime_mins = m['runtime_mins']
        if 'advisory_rating' in m:
            movie.advisory_rating = m['advisory_rating']

        movie_batch.append(movie)
        movie_list.append(movie)
        if len(movie_batch) == BATCH_SIZE:
            ndb.put_multi(movie_batch)
            movie_batch = []

        if 'image_params' in m:
            taskqueue.add(url=url_for('task.fetch_image'), params=m['image_params'])

    ndb.put_multi(movie_batch)

    return movie_list

def get_variant(movie_dict):
    if 'variant' in movie_dict:
        return movie_dict['variant']
    else:
        return variant_from_title(movie_dict['movie_title'])

#some basic heuristics for the canonical title
def get_canonical_title(title):
    canon = title.strip()
    canon = strip_suffix_variance(canon)
    canon = strip_prefix_variance(canon)
    canon = strip_in_suffix(canon)
    canon = canon.title()
    canon = canon.replace("'S ", "'s ")

    canon = canon.strip()
    return canon

FILTER_WORDS = [ 'the', 'a', 'an', 'or', 'and', 'is', 'in' ]

def strip_punct(w):
    return filter(lambda c: c not in string.punctuation, w)

ROMAN_NUMERAL_MATCH='^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$'
def convert_roman_numeral(w):
    if re.search(ROMAN_NUMERAL_MATCH, w):
        return str(roman_to_numeric(w))
    else:
        return w

def strip_part_specifier_before_romans(words):
    new_words = []
    for w in words:
        if re.search(ROMAN_NUMERAL_MATCH, w) and new_words and new_words[-1].lower() == 'part':
            new_words.pop()

        new_words.append(w)

    return new_words


SUFFIX_VARIANCE_MATCH=r'\(([23][dD])\)$'
PREFIX_VARIANCE_MATCH=r'^\(([23][dD])\)'
IN_SUFFIX_VARIANCE_MATCH=r'\(?in\s+([23][dD])\)?'

def strip_suffix_variance(title):
    return re.sub(SUFFIX_VARIANCE_MATCH,'',title)

def strip_prefix_variance(title):
    return re.sub(PREFIX_VARIANCE_MATCH,'',title)

def strip_in_suffix(title):
    return re.sub(IN_SUFFIX_VARIANCE_MATCH,'',title)

def variant_from_title(title):
    match = re.search(SUFFIX_VARIANCE_MATCH, title)
    if match:
        return match.group(1).upper()

    match = re.search(PREFIX_VARIANCE_MATCH, title)
    if match:
        return match.group(1).upper()

    match = re.search(IN_SUFFIX_VARIANCE_MATCH, title)
    if match:
        return match.group(1).upper()

    return None

def get_all_correlation_titles(title):
    return [ get_correlation_title(v, title) 
             for v in range(1, CORRELATION_VERSION + 1) ]

def get_correlation_title(v, title):
    if title.strip() == '':
        return title

    title = title.strip()

    # Bunch of transforms for the title
    title = strip_prefix_variance(title)
    title = strip_suffix_variance(title)
    title = strip_in_suffix(title)

    title_words = title.split(' ')
    title_words = filter(None, [ strip_punct(w) for w in title_words ])
    if v == 2:
        title_words = strip_part_specifier_before_romans(title_words)
        title_words = [ convert_roman_numeral(w) for w in title_words ]
    title_words = [ w.lower() for w in title_words ]


    title_words = filter(lambda w: w != '',
                         title_words)
    title_words = filter(lambda w: w not in FILTER_WORDS,
                         title_words)

    return "%s:%s" % (v, '_'.join(title_words))

def build_ratings_property(movie, movie_dict, feed_id):
    ratings = get_ratings(movie_dict)
    if movie.ratings == None:
        movie.ratings = {}
    movie.ratings[feed_id] = ratings

# all of the ratings should provide a rating and a comment for critics and audiences
def get_ratings(movie_dict):
    ratings = movie_dict['ratings'] if 'ratings' in movie_dict else {}
    if 'critics_rating' in ratings and 'audience_rating' in ratings and 'critics_comment' in ratings and 'audience_comment' in ratings:
        return { 'critics' : ratings['critics_rating'],
                 'audience': ratings['audience_rating'],
                 'critics_comment': ratings['critics_comment'],
                 'audience_comment': ratings['audience_comment']
               }
    return None

class MovieCorrelation:
    def __init__(self):
        self.NEW_MOVIES_ADDED = 0
        self.EXISTING_MOVIES_FROM_CACHE = 0
        self.EXISTING_MOVIES_FROM_DATASTORE = 0
        self.DS_CORRELATED_BY_TITLE = 0
        self.DS_CORRELATED_BY_ORG_AND_ID = 0
        self.CACHE_MATCH_BY_ID = 0
        self.CACHE_MATCH_BY_TITLE = 0
        self.FEED_NO_CREATION = 0
        self.MULTIPLE_CORRELATION = 0

        self.movie_cache = {}

    def cached(self, key):
        if key not in self.movie_cache:
            self.movie_cache[key] = key.get()

        return self.movie_cache[key]


    def correlate(self, org_key, movie_dict, feed=None):
        if not feed:
            feed = org_key.get()
            log.debug("Getting feed from org_key %s: %s" % (org_key, feed))

        correlation_id = str(movie_dict['id']) if 'id' in movie_dict else None
        titles = get_all_correlation_titles(movie_dict['movie_title'])
        variant = get_variant(movie_dict)

        if correlation_id:
            corr = FeedCorrelation(movie_id=correlation_id, org_key=org_key,
                                   variant=variant)
        else:
            corr = None

        log.debug("Correlation params: id %s title %s" % (correlation_id, titles))
        cached_movie_id = memcache.get("movie::movie_correlation::org_key/movie_id[%s,%s]" % (org_key.id(), correlation_id))
        cached_titles = filter(None, [ memcache.get("movie::correlation_title::%s" % t) for t in titles ])

        if cached_movie_id or cached_titles:
            self.EXISTING_MOVIES_FROM_CACHE += 1
            if cached_movie_id:
                log.debug("Matched against cached feed movie ID %s" % correlation_id)
                movie = self.cached(ndb.Key(urlsafe=cached_movie_id))
                self.CACHE_MATCH_BY_ID += 1
            else: # cached_title
                # We don't care *which* title; we assume that the
                # different versions all point to the same entity
                log.debug("Matched against cached correlation title(s) %s" % titles)
                movie = self.cached(ndb.Key(urlsafe=cached_titles[0]))
                self.CACHE_MATCH_BY_TITLE += 1

            log.debug("Found correlated item in cache: %s" % movie.key.id())
            if variant:
                if movie.variants and variant not in movie.variants:
                    movie.variants.append(variant)
                elif not movie.variants:
                    movie.variants = [ variant ]
            return movie

        if correlation_id:
            q = Movie.query(ndb.OR(ndb.AND(Movie.movie_correlation.org_key == org_key,
                                           Movie.movie_correlation.movie_id == correlation_id),
                               Movie.correlation_title.IN(titles)))
            if q.count == 1:
                self.DS_CORRELATED_BY_ORG_AND_ID += 1
        else:
            q = Movie.query(Movie.correlation_title.IN(titles))
            if q.count == 1:
                self.DS_CORRELATED_BY_TITLE += 1

        q_count = q.count()

        if q_count == 1:
            self.EXISTING_MOVIES_FROM_DATASTORE += 1
            movie = q.fetch(1)[0]
            log.debug("Found correlated item: %s" % movie.key.id())
            if corr and corr not in movie.movie_correlation:
                # We were correlated by title, cache the correlation title
                movie.movie_correlation.append(corr)
                for t in movie.correlation_title:
                    memcache.set("movie::correlation_title::%s" % t,
                                 movie.key.urlsafe())
            elif corr:
                # We were correlated by ID, cache the correlation ID
                memcache.set("movie::movie_correlation::org_key/movie_id[%s,%s]" % (org_key.id(), correlation_id), movie.key.urlsafe())
                # Cache the correlation_title as well for this entry
                for t in movie.correlation_title:
                    memcache.set("movie::correlation_title::%s" % t, 
                                 movie.key.urlsafe())

            # HACK: Update correlation title whenever we match
            movie.correlation_title = titles

            # HACK: Ensure variant specified is in our variants list
            if variant:
                if movie.variants and variant not in movie.variants:
                    movie.variants.append(variant)
                elif not movie.variants:
                    movie.variants = [ variant ]

        elif q_count == 0 and feed.allow_movie_creation:
            self.NEW_MOVIES_ADDED += 1
            log.debug("Couldn't find correlated item for %s given [%s,%s], will run additional checks\n\tCorrelation titles: %r"
                          % (movie_dict, org_key, correlation_id, titles))

            movie = Movie(key=ndb.Key(Movie, str(uuid_gen())),
                          per_org_titlemap={},
                          correlation_title=titles)

            if corr:
                movie.movie_correlation = [ corr ]

            if variant:
                movie.variants = [ variant ]

            for t in titles:
                memcache.set("movie::correlation_title::%s" % t, 
                             movie.key.urlsafe())

            self.movie_cache[movie.key] = movie
        elif not feed.allow_movie_creation:
            self.FEED_NO_CREATION += 1
            log.info("Skipping '%s', this feed is not allowed to create new movies" % titles)
            return None
        else:
            self.MULTIPLE_CORRELATION += 1
            log.warn("FIXME: Found multiple matching movies with the same correlation ID! Running additional correlations here")
            raise ValueError("Multiple correlated movies for %s" % movie_dict)

        return movie

DEFAULT_MOVIE_CORRELATION = MovieCorrelation()
correlate_dict_to_movie_entity = DEFAULT_MOVIE_CORRELATION.correlate
