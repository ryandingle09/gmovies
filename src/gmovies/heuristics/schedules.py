from __future__ import absolute_import

import datetime
import logging
import traceback

from decimal import Decimal
from operator import attrgetter
from uuid import uuid4 as uuid_gen

from google.appengine.api import memcache
from google.appengine.ext import deferred, ndb

from gmovies import models, orgs
from gmovies.admin import schedule_heuristics_notification
from gmovies.heuristics import movies

log = logging.getLogger(__name__)

BATCH_SIZE = 100


def transform(schedule_dictionary_list):
    schedule_batch = []
    schedule_list = []

    if len(schedule_dictionary_list) > 0:
        try:
            correlator = ScheduleCorrelation()

            for schedule_dictionary in schedule_dictionary_list:
                schedule = correlator.convert_schedule(schedule_dictionary)

                schedule_batch.append(schedule)
                schedule_list.append(schedule)

                if len(schedule_batch) == BATCH_SIZE:
                    log.info("SAVED!, schedule_batch...")
                    ndb.put_multi(filter(None, schedule_batch))
                    schedule_batch = []

            ndb.put_multi(filter(None, schedule_batch))

            schedule_heuristics_notification(
                    new_schedules=correlator.NEW_SCHEDULE_ADDED,
                    existing_from_cache=correlator.EXISTING_SCHEDULE_FROM_CACHE,
                    existing_from_datastore=correlator.EXISTING_SCHEDULE_FROM_DATASTORE,
                    existing_from_datastore_variants=correlator.EXISTING_SCHEDULES_FROM_DATASTORE_WITH_VARIANTS,
                    cached_by_code=correlator.CACHE_MATCH_BY_CODE,
                    fetch_by_code=correlator.DS_MATCH_BY_CODE,
                    fetch_variants=correlator.DS_MATCH_SCHEDULE_WITH_VARIANT,
                    fetch_no_variants=correlator.DS_MATCH_SCHEDULE_WITHOUT_VARIANT,
                    multi_errors=correlator.TOO_MANY_MATCHES)

            return schedule_list
        except Exception, e:
            if len(schedule_batch) > 0:
                log.warn('Exception while processing schedules, flushing existing schedule_list')
                log.info(e)
                ndb.put_multi(filter(None, schedule_batch))

            return schedule_list

def transform_schedules_by_theaterorg(schedule_dictionary_list):
    schedules = []
    schedule_batch = []
    correlator = ScheduleCorrelation()

    try:
        for schedule_dictionary in schedule_dictionary_list:
            schedule = correlator.convert_schedule_by_theaterorg(schedule_dictionary)

            schedule_batch.append(schedule)
            schedules.append(schedule)

            if len(schedule_batch) == BATCH_SIZE:
                ndb.put_multi(filter(None, schedule_batch))
                schedule_batch = []

        ndb.put_multi(filter(None, schedule_batch))
        deferred.defer(delete_nonexistent_scheduleslot_feed_code, schedules, correlator.NEW_SCHEDULETIMESLOT_FEED_CODES)

        schedule_heuristics_notification(
                new_schedules=correlator.NEW_SCHEDULE_ADDED,
                existing_from_cache=correlator.EXISTING_SCHEDULE_FROM_CACHE,
                existing_from_datastore=correlator.EXISTING_SCHEDULE_FROM_DATASTORE,
                existing_from_datastore_variants=correlator.EXISTING_SCHEDULES_FROM_DATASTORE_WITH_VARIANTS,
                cached_by_code=correlator.CACHE_MATCH_BY_CODE,
                fetch_by_code=correlator.DS_MATCH_BY_CODE,
                fetch_variants=correlator.DS_MATCH_SCHEDULE_WITH_VARIANT,
                fetch_no_variants=correlator.DS_MATCH_SCHEDULE_WITHOUT_VARIANT,
                multi_errors=correlator.TOO_MANY_MATCHES)
    except Exception, e:
        log.warn("ERROR!, transform_schedules_by_theaterorg...")
        log.error(traceback.format_exc(e))

        if len(schedule_batch) > 0:
            ndb.put_multi(filter(None, schedule_batch))

        deferred.defer(delete_nonexistent_scheduleslot_feed_code, schedules, correlator.NEW_SCHEDULETIMESLOT_FEED_CODES)

    return schedules

def transform_schedules_taskgroup(per_org_payloads):
    schedules = []
    schedules_batch = []
    correlator = ScheduleCorrelation()
    theaterorg_ids = per_org_payloads.keys()
    theaterorgs = ndb.get_multi([ndb.Key(models.TheaterOrganization, theaterorg_id) for theaterorg_id in theaterorg_ids])
    prioritized_theaterorg = reversed(sorted(theaterorgs, key=attrgetter('sort_priority')))

    try:
        for theaterorg in prioritized_theaterorg:
            schedules_payloads = per_org_payloads[theaterorg.key.id()]

            for schedule_dictionary in schedules_payloads:
                schedule = correlator.convert_schedule_by_theaterorg(schedule_dictionary)

                schedules.append(schedule)
                schedules_batch.append(schedule)

                if len(schedules_batch) == BATCH_SIZE:
                    log.debug("transform2, reach the batch size, execute put_multi by batch...")

                    ndb.put_multi(filter(None, schedules_batch))
                    deferred.defer(delete_nonexistent_scheduleslot_feed_code, schedules_batch, correlator.NEW_SCHEDULETIMESLOT_FEED_CODES)

                    schedules_batch = []

        schedule_heuristics_notification(new_schedules=correlator.NEW_SCHEDULE_ADDED,
                existing_from_cache=correlator.EXISTING_SCHEDULE_FROM_CACHE,
                existing_from_datastore=correlator.EXISTING_SCHEDULE_FROM_DATASTORE,
                existing_from_datastore_variants=correlator.EXISTING_SCHEDULES_FROM_DATASTORE_WITH_VARIANTS,
                cached_by_code=correlator.CACHE_MATCH_BY_CODE,
                fetch_by_code=correlator.DS_MATCH_BY_CODE,
                fetch_variants=correlator.DS_MATCH_SCHEDULE_WITH_VARIANT,
                fetch_no_variants=correlator.DS_MATCH_SCHEDULE_WITHOUT_VARIANT,
                multi_errors=correlator.TOO_MANY_MATCHES)
    except Exception, e:
        log.warn("ERROR!, transform2...")
        log.error(traceback.format_exc(e))

    if len(schedules_batch) > 0:
        ndb.put_multi(filter(None, schedules_batch))
        deferred.defer(delete_nonexistent_scheduleslot_feed_code, schedules_batch, correlator.NEW_SCHEDULETIMESLOT_FEED_CODES)

    # deferred.defer(delete_nonexistent_scheduleslot_feed_code, schedules, correlator.NEW_SCHEDULETIMESLOT_FEED_CODES)

    return schedules

def get_movie_entity(uuid, movie_title):
    movie = query_movie_with_title(movie_title)

    if not movie:
        # Fallback: Create the movie entry
        log.warn("Creating entity for '%s'; couldn't find match in datastore" % movie_title)

        movie_dict = {'movie_title': movie_title}
        org_key = ndb.Key(models.Feed, str(uuid))
        movie, correlation_titles = movies.correlate_dict_to_movie_entity(org_key, movie_dict)

        if movie and movie is not None:
            movie.put()
            cache_movie_title(movie)

    return movie

def get_movie_entity_by_feed_correlation(uuid, movie_id):
    feed_key = ndb.Key(models.Feed, str(uuid))
    queryset = models.Movie.query(models.Movie.movie_correlation.org_key==feed_key, models.Movie.movie_correlation.movie_id==movie_id)
    movie = queryset.get()

    return movie

def parse_screening(screening_date_time):
    screening_date_time = screening_date_time.replace('MN', 'AM').replace('NN', 'PM')

    try:
        screening = datetime.datetime.strptime(screening_date_time, '%m/%d/%Y %I:%M:%S %p')
    except ValueError:
        try:
            screening = datetime.datetime.strptime(screening_date_time, '%m/%d/%Y %I:%M %p')
        except ValueError:
            screening = None

    return screening

def to_cache_key(*tup):
    matcher_string = ':'.join([unicode(o) for o in tup])

    return "schedule::matcher(%d)[%s]" % (len(tup), matcher_string)

def query_movie_with_title(title):
    correlation_titles = movies.get_all_correlation_titles(title)
    cached_titles = filter(None, [memcache.get("movie::correlation_title::%s" % t) for t in correlation_titles]) # check cache against correlation title.

    if cached_titles:
        return ndb.Key(urlsafe=cached_titles[0]).get()

    # hit datastore, query against correlation title.
    queryset = models.Movie.query(models.Movie.correlation_title.IN(correlation_titles), models.Movie.is_inactive==False)

    if queryset.count() == 1:
        movie = queryset.get()

        for t in correlation_titles:
            memcache.set("movie::correlation_title::%s" % t, movie.key.urlsafe())

        return movie

    return None

def cache_movie_title(movie):
    memcache.set("movie::correlation_title::%s" % movie.correlation_title, movie.key.urlsafe())

def fix_schedule_slot_price(schedule, movie_key, price, variant):
    log.info("Theater: %s - Cinema: %s - Schedule: %s - Updated Price: %s" % (theater.name, schedule.cinema, schedule.show_date, price))

    theater = schedule.key.parent().get()

    for slot in schedule.slots:
        if slot.movie == movie_key and slot.variant == variant and slot.price != price:
            slot.price = price

    return schedule

def delete_nonexistent_scheduleslot_feed_code(schedules, feed_codes):
    try:
        for schedule in filter(None, schedules):
            slots = [slot for slot in schedule.slots]

            for slot in slots:
                if slot.feed_code not in feed_codes:
                    log.warn("DELETE!, ScheduleSlot, feed_code: %s, movie_id: %s, start_time: %s..." % (slot.feed_code, slot.movie.id(), slot.start_time))

                    schedule.slots.remove(slot)

        ndb.put_multi(filter(None, schedules))
    except Exception, e:
        log.warn("ERROR!, delete_nonexistent_scheduleslot_feed_code, something went wrong...")
        log.error(traceback.format_exc(e))

def rename_seating_type(seat_type, theater, uuid):
    if str(uuid) == str(orgs.AYALA_MALLS):
        if seat_type == 'Guaranteed Seats':
            seat_type = 'Reserved Seating'

    return seat_type


def match_movies(real_movie_title):
    try:
        r_title = real_movie_title
        api_title = real_movie_title
        log.debug("movie_title : %s" % (api_title))

        searchable_words = movies.get_searchable_words(api_title)
        log.debug("searchable_words : %s" % (searchable_words))
        log.debug("searchable_words_count : %s" % (len(searchable_words)))

        if len(searchable_words) is not 0:
            queryset = models.Movie.query(models.Movie.searchable_words.IN(searchable_words), models.Movie.is_inactive==False).order(models.Movie.key)

            for m in queryset:
                db_title = m.canonical_title.lower()
                api_title = str(api_title).lower()

                if db_title == api_title:
                    log.debug("matching nearest title in: db_title: %s and api_title: %s" % (db_title, api_title))

                    r_title = m.canonical_title

        log.debug("return_updated_movie_title : %s" % (r_title))
        return r_title

    except Exception, e:
        log.warn("ERROR!, match_movies...")
        log.error(traceback.format_exc(e))


class ScheduleCorrelation:
    def __init__(self):
        self.NEW_SCHEDULE_ADDED = 0
        self.EXISTING_SCHEDULE_FROM_CACHE = 0
        self.CACHE_MATCH_BY_CODE = 0
        self.EXISTING_SCHEDULE_FROM_DATASTORE = 0
        self.EXISTING_SCHEDULES_FROM_DATASTORE_WITH_VARIANTS = 0
        self.DS_MATCH_BY_CODE = 0
        self.DS_MATCH_SCHEDULE_WITH_VARIANT = 0
        self.DS_MATCH_SCHEDULE_WITHOUT_VARIANT = 0
        self.TOO_MANY_MATCHES = 0

        self.NOT_FOUND_MOVIES = []
        self.NEW_SCHEDULETIMESLOT_FEED_CODES = []

    def convert_schedule(self, s):
        schedules = []
        schedule_code = s['id']
        show_date_time = parse_screening(s['screening'])
        movie_title = s['movie_title']
        theater_code = s['theater_code']
        cinema_code = s['cinema']
        price = Decimal(s['price'])
        seat_type = s['seat_type'] if ('seat_type' in s) else None
        variant = s['variant'] if ('variant' in s) else None
        uuid = s['uuid']
        popcorn_price = Decimal(s['popcorn_price']) if 'popcorn_price' in s else None

        movie_title = match_movies(movie_title)

        if show_date_time is None:
            log.debug('SKIP!, schedule_code: %s - theater_code: %s - cinema: %s - movie: %s' % (s['id'], s['theater_code'], s['cinema'], s['movie_title']))
            log.error('ERROR!, Wrong Screening Time Format: %s' % s['screening'])

            return None

        if movie_title in self.NOT_FOUND_MOVIES:
            log.debug("SKIP!, movie %s is not found in the previous schedule convertion..." % movie_title)

            return None

        theaterorg_key = ndb.Key(models.TheaterOrganization, str(uuid))
        theaterorg = theaterorg_key.get()

        if not theaterorg or theaterorg is None:
            log.debug("SKIP!, we can't match theaterorg...")

            return None

        movie = get_movie_entity(uuid, movie_title)
        theater = query_theater_with_code(theater_code, theaterorg=theaterorg)

        if not movie or movie is None:
            self.NOT_FOUND_MOVIES.append(movie_title)
            log.debug("SKIP!, we can't match movie...")

            return None

        if not theater or theater is None:
            log.debug("SKIP!, we can't match theater...")

            return None

        has_cinema = query_cinema_in_theater(theater, cinema_code)

        if not has_cinema or has_cinema is None:
            log.debug("SKIP!, we can't match cinema...")

            return None

        seat_type = rename_seating_type(seat_type, theater, uuid)
        correlation_titles = movies.get_all_correlation_titles(movie_title)
        show_date = show_date_time.date()
        show_time = show_date_time.time()
        theater_key = theater.key
        movie_key = movie.key

        log.debug("Looking for (%s, %s, %s, %s, %s, %s, %s, %s, %s)" % (schedule_code, show_date, show_time, correlation_titles, theaterorg.name, theater_code, cinema_code, variant, seat_type))

        cached_sched_key_tuples = filter(None, [memcache.get(to_cache_key(show_date, t, variant,
                theaterorg.name, theater_code, cinema_code, seat_type)) for t in correlation_titles])

        if cached_sched_key_tuples:
            log.debug("Schedule cache hit (%s, %s, %s, %s, %s, %s, %s, %s, %s)" % (schedule_code, show_date, show_time, correlation_titles, theaterorg.name, theater_code, cinema_code, variant, seat_type))

            self.EXISTING_SCHEDULE_FROM_CACHE += 1
            schedule = ndb.Key(urlsafe=cached_sched_key_tuples[0]).get()

            if show_time not in schedule._start_times():
                schedule.slots.append(models.ScheduleTimeSlot(start_time=show_time, movie=movie_key,
                        variant=variant, feed_code=schedule_code, seating_type=seat_type, price=price, popcorn_price=popcorn_price))
            else:
                slot = schedule.get_timeslot(show_time)
                slot.movie = movie_key
                slot.variant = variant
                slot.feed_code = schedule_code
                slot.seating_type = seat_type
                slot.price = price
                slot.popcorn_price = popcorn_price

            log.debug("Cached Schedule: %r", schedule)
        else:
            schedule = models.Schedule.find_schedule(theater_key, cinema_code, show_date)

            if not schedule:
                self.NEW_SCHEDULE_ADDED += 1

                schedule = models.Schedule.create_schedule(cinema=cinema_code, parent=theater_key, show_date=show_date)
                schedule.slots = [models.ScheduleTimeSlot(start_time=show_time, movie=movie_key,
                        variant=variant,feed_code=schedule_code, seating_type=seat_type, price=price, popcorn_price=popcorn_price)]
                schedule.put()

                log.debug("Caching (%s, %s, %s, %s, %s, %s, %s, %s, %s)" % (schedule_code, show_date, show_time, correlation_titles, theaterorg.name, theater_code, cinema_code, variant, seat_type))

                for t in correlation_titles:
                    memcache.set(to_cache_key(show_date, t, variant, theaterorg.name, theater_code, cinema_code, seat_type), schedule.key.urlsafe())
            else:
                self.EXISTING_SCHEDULE_FROM_DATASTORE += 1

                if show_time not in schedule._start_times():
                    schedule.slots.append(models.ScheduleTimeSlot(start_time=show_time, movie=movie_key,
                            variant=variant, feed_code=schedule_code, seating_type=seat_type, price=price, popcorn_price=popcorn_price))
                else:
                    slot = schedule.get_timeslot(show_time)
                    slot.movie = movie_key
                    slot.variant = variant
                    slot.feed_code = schedule_code
                    slot.seating_type = seat_type
                    slot.price = price
                    slot.popcorn_price = popcorn_price

        return schedule

    def convert_schedule_by_theaterorg(self, s):
        schedule_code = str(s['id'])
        movie_schedule_code = str(s['schedule_id']) if 'schedule_id' in s else None
        show_date_time = parse_screening(s['screening'])
        movie_id = s.get('movie_id', None)
        movie_title = s['movie_title']
        theater_code = s.get('theater_code', None)
        cinema_id = s.get('cinema_id', None)
        cinema_code = s.get('cinema_code', None)
        cinema_name = s['cinema_name']
        price = Decimal(s['price'])
        seat_type = s.get('seat_type', None)
        variant = s.get('variant', None)
        uuid = s['uuid']
        popcorn_price = Decimal(s['popcorn_price']) if 'popcorn_price' in s else None

        movie_title = match_movies(movie_title)

        if show_date_time is None:
            log.debug('SKIP!, schedule_code: %s - theater_code: %s - cinema: %s - movie: %s' % (schedule_code, theater_code, cinema_name, movie_title))
            log.error('ERROR!, Wrong Screening Time Format: %s' % s['screening'])

            return None

        if movie_title in self.NOT_FOUND_MOVIES:
            log.debug("SKIP!, theater_code %s with movie %s is not found in the previous schedule convertion..." % (theater_code, movie_title))

            return None

        theaterorg_key = ndb.Key(models.TheaterOrganization, str(uuid))
        theaterorg = theaterorg_key.get()

        if not theaterorg or theaterorg is None:
            log.debug("SKIP!, we can't match theaterorg...")

            return None

        movie = get_movie_entity_by_feed_correlation(uuid, movie_id) if movie_id else get_movie_entity(uuid, movie_title)
        theater = query_theater_with_code(theater_code, theaterorg=theaterorg)

        if not movie or movie is None:
            log.debug("SKIP!, we can't match movie...")

            self.NOT_FOUND_MOVIES.append(movie_title)

            return None

        if not theater or theater is None:
            log.debug("SKIP!, we can't match theater...")

            return None

        has_cinema = query_cinema_in_theater(theater, cinema_code, cinema_id=cinema_id)

        if not has_cinema or has_cinema is None:
            log.debug("SKIP!, we can't match cinema...")

            return None

        if not cinema_code:
            cinema = get_cinema_entity_using_feed_code(theater, cinema_id)
            cinema_code = cinema.name

        show_date = show_date_time.date()
        show_time = show_date_time.time()
        seat_type = rename_seating_type(seat_type, theater, uuid)

        log.debug("Looking for (%s, %s, %s, %s, %s, %s, %s, %s, %s)" % (schedule_code, show_date, show_time, movie_id, theaterorg.name, theater_code, cinema_code, variant, seat_type))

        cached_sched_key_tuple = memcache.get(to_cache_key(show_date, movie_id, variant, theaterorg.name, theater_code, cinema_code, seat_type))

        if cached_sched_key_tuple:
            log.debug("Schedule cache hit (%s, %s, %s, %s, %s, %s, %s, %s, %s)" % (schedule_code, show_date, show_time, movie_id, theaterorg.name, theater_code, cinema_code, variant, seat_type))

            self.EXISTING_SCHEDULE_FROM_CACHE += 1

            schedule = ndb.Key(urlsafe=cached_sched_key_tuple).get()

            if schedule_code not in schedule._feed_codes():
                schedule.slots.append(models.ScheduleTimeSlot(feed_code=schedule_code, feed_code_schedule=movie_schedule_code,
                        start_time=show_time, movie=movie.key, variant=variant, seating_type=seat_type, price=price, popcorn_price=popcorn_price))
            else:
                if movie_schedule_code and movie_schedule_code not in schedule.feed_codes:
                    schedule.feed_codes.append(movie_schedule_code)

                slot = schedule.get_feed_code(schedule_code)
                slot.feed_code = schedule_code
                slot.feed_code_schedule = movie_schedule_code
                slot.start_time = show_time
                slot.movie = movie.key
                slot.variant = variant
                slot.seating_type = seat_type
                slot.price = price
                slot.popcorn_price = popcorn_price

            log.debug("Cached Schedule: %r", schedule)
        else:
            schedule = models.Schedule.find_schedule(theater.key, cinema_code, show_date)

            if not schedule:
                self.NEW_SCHEDULE_ADDED += 1

                schedule = models.Schedule.create_schedule(cinema=cinema_code, parent=theater.key, show_date=show_date)
                schedule.feed_codes = [movie_schedule_code] if movie_schedule_code else []
                schedule.slots = [models.ScheduleTimeSlot(feed_code=schedule_code, feed_code_schedule=movie_schedule_code,
                        start_time=show_time, movie=movie.key, variant=variant, seating_type=seat_type, price=price, popcorn_price=popcorn_price)]
                schedule.put()

                log.debug("Caching (%s, %s, %s, %s, %s, %s, %s, %s, %s)" % (schedule_code, show_date, show_time, movie_id, theaterorg.name, theater_code, cinema_code, variant, seat_type))

                memcache.set(to_cache_key(show_date, movie_id, variant, theaterorg.name, theater_code, cinema_code, seat_type), schedule.key.urlsafe())
            else:
                self.EXISTING_SCHEDULE_FROM_DATASTORE += 1

                if schedule_code not in schedule._feed_codes():
                    schedule.slots.append(models.ScheduleTimeSlot(feed_code=schedule_code, feed_code_schedule=movie_schedule_code,
                            start_time=show_time, movie=movie.key, variant=variant, seating_type=seat_type, price=price, popcorn_price=popcorn_price))
                else:
                    if movie_schedule_code and movie_schedule_code not in schedule.feed_codes:
                        schedule.feed_codes.append(movie_schedule_code)

                    slot = schedule.get_feed_code(schedule_code)
                    slot.feed_code = schedule_code
                    slot.feed_code_schedule = movie_schedule_code
                    slot.start_time = show_time
                    slot.movie = movie.key
                    slot.variant = variant
                    slot.seating_type = seat_type
                    slot.price = price
                    slot.popcorn_price = popcorn_price

        self.NEW_SCHEDULETIMESLOT_FEED_CODES.append(schedule_code)

        return schedule

    def query_for_schedule_correlation(self, theater_key, movie_key, show_date, variant, theater_code, cinema_code, seat_type):
        return models.Schedule.find_schedule(theater_key, cinema_code, show_date)

    def query_theater_with_code(self, theater_code, theaterorg=None):
        if theaterorg and theaterorg is not None:
            cache_key = "theaterorg::%s::theater::theater_code::%s" % (theaterorg.key.id(), theater_code)
        else:
            cache_key = "theater::theater_code::%s" % theater_code

        theater_key = memcache.get(cache_key)

        if theater_key:
            self.CACHE_MATCH_BY_CODE += 1

            return ndb.Key(urlsafe=theater_key).get()

        log.debug("Matching against theater with code %s" % theater_code)

        if theaterorg and theaterorg is not None:
            queryset = models.Theater.query(models.Theater.org_theater_code==theater_code, ancestor=theaterorg.key)
        else:
            queryset = models.Theater.query(models.Theater.org_theater_code==theater_code)

        if queryset.count() == 0:
            return None

        theater = queryset.get()
        self.DS_MATCH_BY_CODE += 1

        memcache.set(cache_key, theater.key.urlsafe())

        return theater

    def query_cinema_in_theater(self, theater, cinema, cinema_id=None):
        if cinema_id:
            cinemas = theater.cinemas
            
            if cinema_id not in [c.feed_code for c in theater.cinemas]:
                return False
        else:
            if cinema not in [c.name for c in theater.cinemas]:
                return False

        return True

    def get_cinema_entity_using_feed_code(self, theater, feed_code):
        cinema = None
        cinemas = theater.cinemas

        for c in cinemas:
            if feed_code == c.feed_code:
                cinema = c

        return cinema


DEFAULT_SCHEDULE_CORRELATION = ScheduleCorrelation()
convert_schedule = DEFAULT_SCHEDULE_CORRELATION.convert_schedule
query_theater_with_code = DEFAULT_SCHEDULE_CORRELATION.query_theater_with_code
query_cinema_in_theater = DEFAULT_SCHEDULE_CORRELATION.query_cinema_in_theater
get_cinema_entity_using_feed_code = DEFAULT_SCHEDULE_CORRELATION.get_cinema_entity_using_feed_code