import datetime
import json
import logging

from decimal import Decimal

from google.appengine.api import memcache
from google.appengine.ext import ndb

from gmovies import models, orgs
from gmovies.admin import schedule_heuristics_notification
from gmovies.heuristics import movies
from gmovies.heuristics.schedules import get_movie_entity


log = logging.getLogger(__name__)
BATCH_SIZE = 100


def scraper_transform_schedules(scraper_schedules, is_file_upload=False):
    schedule_batch = []
    schedule_list = []

    if scraper_schedules and len(scraper_schedules) > 0:
        try:
            log.info('Transforming Scraper Schedules...')

            correlator = ScraperScheduleCorrelation()

            for scraper_schedule in scraper_schedules:
                if is_file_upload:
                    schedule_dict = scraper_schedule
                else:
                    schedule_dict = {}
                    schedule_dict['id'] = ''
                    schedule_dict['theater_code'] = scraper_schedule[1]
                    schedule_dict['cinema'] = scraper_schedule[2]
                    schedule_dict['movie_title'] = scraper_schedule[0]
                    schedule_dict['rating'] = ''
                    schedule_dict['variant'] = scraper_schedule[6]
                    schedule_dict['price'] = scraper_schedule[5]
                    schedule_dict['screening'] = scraper_schedule[3] + ' ' + scraper_schedule[4]
                    schedule_dict['seat_type'] = ''
                    schedule_dict['uuid'] = orgs.GLOBE

                schedules = correlator.convert_schedule(schedule_dict)

                for schedule in schedules:
                    schedule_batch.append(schedule)
                    schedule_list.append(schedule)

                    if len(schedule_batch) == BATCH_SIZE:
                        log.info("SAVED!, schedule_batch...")
                        ndb.put_multi(filter(None, schedule_batch))
                        schedule_batch = []

            ndb.put_multi(filter(None, schedule_batch))

            schedule_heuristics_notification(new_schedules=correlator.NEW_SCHEDULE_ADDED, 
                                             existing_from_cache=correlator.EXISTING_SCHEDULE_FROM_CACHE,
                                             existing_from_datastore=correlator.EXISTING_SCHEDULE_FROM_DATASTORE,
                                             existing_from_datastore_variants=correlator.EXISTING_SCHEDULES_FROM_DATASTORE_WITH_VARIANTS,
                                             cached_by_code=correlator.CACHE_MATCH_BY_CODE,
                                             fetch_by_code=correlator.DS_MATCH_BY_CODE,
                                             fetch_variants=correlator.DS_MATCH_SCHEDULE_WITH_VARIANT,
                                             fetch_no_variants=correlator.DS_MATCH_SCHEDULE_WITHOUT_VARIANT,
                                             multi_errors=correlator.TOO_MANY_MATCHES)

            return schedule_list
        except Exception, e:
            if len(schedule_batch) > 0:
                log.error('ERROR!, Transforming SCRAPER Schedules...')
                log.warn('Exception while processing schedules, flushing existing schedule_list...')
                log.error(e)

                ndb.put_multi(filter(None, schedule_batch))

            return schedule_list


def parse_screening(screening_date_time):
    screening_date_time = screening_date_time.replace('MN', 'AM').replace('NN', 'PM')
    try:
        screening = datetime.datetime.strptime(screening_date_time, '%m/%d/%Y %I:%M:%S %p')
    except ValueError:
        try:
            screening = datetime.datetime.strptime(screening_date_time, '%m/%d/%Y %I:%M %p')
        except ValueError:
            screening = None

    return screening


def to_cache_key(*tup):
    matcher_string = ':'.join([ unicode(o) for o in tup ])
    return "schedule::matcher(%d)[%s]" % (len(tup), matcher_string)


def fix_schedule_slot_price(schedule, movie_key, price, variant):
    theater = schedule.key.parent().get()

    for slot in schedule.slots:
        if slot.movie == movie_key and slot.variant == variant and slot.price != price:
            slot.price = price

    return schedule


class ScraperScheduleCorrelation:
    def __init__(self):
        self.NEW_SCHEDULE_ADDED = 0
        self.EXISTING_SCHEDULE_FROM_CACHE = 0
        self.CACHE_MATCH_BY_CODE = 0
        self.EXISTING_SCHEDULE_FROM_DATASTORE = 0
        self.EXISTING_SCHEDULES_FROM_DATASTORE_WITH_VARIANTS = 0
        self.DS_MATCH_BY_CODE = 0
        self.DS_MATCH_SCHEDULE_WITH_VARIANT = 0
        self.DS_MATCH_SCHEDULE_WITHOUT_VARIANT = 0
        self.TOO_MANY_MATCHES = 0

        self.NOT_FOUND_MOVIES = []

    def convert_schedule(self, s):
        log.info('Converting schedules from scraper...')
        schedules = []
        schedule_code = s['id']
        show_date_time = parse_screening(s['screening'])

        if show_date_time is None:
            log.debug('Skip!, theater_code: %s - cinema: %s - movie: %s' % (s['theater_code'], s['cinema'], s['movie_title']))
            log.error('ERROR!, Wrong Screening Time Format: %s' % s['screening'])

            return schedules

        show_date = show_date_time.date()
        show_time = show_date_time.time()

        price = Decimal(s['price'])
        uuid = s['uuid']
        movie_title = s['movie_title']
        variant = s['variant']

        theater_code = s['theater_code']
        cinema_code = s['cinema']
        seat_type = s['seat_type'] if ('seat_type' in s) else None

        if movie_title in self.NOT_FOUND_MOVIES:
            log.debug("SKIP!, movie %s is not found in the previous schedule convertion..." % movie_title)

            return schedules

        movie = get_movie_entity(uuid, movie_title)
        correlation_titles = movies.get_all_correlation_titles(movie_title)
        theaters = query_theater_with_code(theater_code)

        if not movie or movie is None:
            self.NOT_FOUND_MOVIES.append(movie_title)
            log.debug("SKIP!, we can't match movie_title: %s ..." % movie_title)

            return schedules

        if not theaters or theaters is None:
            log.debug("SKIP!, we can't match theater with theater_code: %s..." % theater_code)

            return schedules

        for index, theater in enumerate(theaters):
            has_cinema = query_cinema_in_theater(theater, cinema_code)

            if not has_cinema or has_cinema is None:
                log.debug("SKIP!, we can't match cinema: %s..." % cinema_code)

                schedules.append(None)

                if len(theaters) == index+1:
                    return schedules
                else:
                    continue

            theater_key = theater.key
            movie_key = movie.key
            theaterorg = theater.key.parent().get().name

            log.debug("Looking for (%s, %s, %s, %s, %s, %s, %s, %s, %s)..." %
                        (schedule_code, show_date, show_time,
                                correlation_titles, theaterorg,
                                theater_code, cinema_code,
                                variant, seat_type))

            cached_sched_key_tuples = filter(None,
                    [memcache.get(to_cache_key(show_date, t, variant,
                            theaterorg, theater_code, cinema_code, seat_type))
                            for t in correlation_titles])

            if cached_sched_key_tuples:
                log.debug("Schedule cache hit (%s, %s, %s, %s, %s, %s, %s, %s, %s)..." %
                                (schedule_code, show_date, show_time,
                                        correlation_titles, theaterorg,
                                        theater_code, cinema_code, variant,
                                        seat_type))

                self.EXISTING_SCHEDULE_FROM_CACHE += 1
                schedule = ndb.Key(urlsafe=cached_sched_key_tuples[0]).get()

                if schedule:
                    schedule = fix_schedule_slot_price(schedule, movie_key,
                                        price, variant)

                if show_time not in schedule._start_times():
                    schedule.slots.append(
                            models.ScheduleTimeSlot(start_time=show_time,
                                    movie=movie_key, variant=variant,
                                    feed_code=schedule_code,
                                    seating_type=seat_type, price=price))
                else:
                    # Replacement of slot info
                    slot = schedule.get_timeslot(show_time)
                    slot.movie = movie_key
                    slot.variant = variant
                    slot.feed_code = schedule_code
                    slot.seating_type = seat_type
                    slot.price = price

                log.debug("Cached Schedule: %r...", schedule)
            else:
                schedule = models.Schedule.find_schedule(theater_key,
                                    cinema_code, show_date)

                if not schedule:
                    self.NEW_SCHEDULE_ADDED += 1
                    schedule = models.Schedule.create_schedule(
                                    cinema=cinema_code, parent=theater_key,
                                    show_date=show_date)
                    schedule.slots = [models.ScheduleTimeSlot(
                                        start_time=show_time,
                                        movie=movie_key, variant=variant,
                                        feed_code=schedule_code,
                                        seating_type=seat_type, price=price)]
                    schedule.put()

                    log.debug("Caching (%s, %s, %s, %s, %s, %s, %s, %s, %s)..." %
                            (schedule_code, show_date, show_time,
                            correlation_titles, theaterorg, theater_code,
                            cinema_code, variant, seat_type))

                    for t in correlation_titles:
                        memcache.set(to_cache_key(show_date, t, variant,
                                theaterorg, theater_code, cinema_code, seat_type),
                                    schedule.key.urlsafe())
                else:
                    schedule = fix_schedule_slot_price(schedule, movie_key,
                                    price, variant)

                    if show_time not in schedule._start_times():
                        schedule.slots.append(
                                models.ScheduleTimeSlot(start_time=show_time,
                                        movie=movie_key,
                                        variant=variant,
                                        feed_code=schedule_code,
                                        seating_type=seat_type,
                                        price=price))
                    else:
                        # Replacement of slot info
                        slot = schedule.get_timeslot(show_time)
                        slot.movie = movie_key
                        slot.variant = variant
                        slot.feed_code = schedule_code
                        slot.seating_type = seat_type
                        slot.price = price

                    self.EXISTING_SCHEDULE_FROM_DATASTORE += 1

            schedules.append(schedule)

        return schedules
            
    def query_theater_with_code(self, theater_code):
        cached_theater_keys = memcache.get("theater::theater_code::%s" % theater_code)
        theater_keys = models.Theater.query(
                    models.Theater.org_theater_code == theater_code,
                    models.Theater.is_published == True).fetch(keys_only=True)

        if cached_theater_keys and len(cached_theater_keys) == len(theater_keys):
            theaters = []
            for theater_key in cached_theater_keys:
                theaters.append(ndb.Key(urlsafe=theater_key).get())
                self.CACHE_MATCH_BY_CODE += 1
        else:
            log.debug("Matching against theater with code %s" % theater_code)

            theater_keys = []
            theaters = models.Theater.query(
                            models.Theater.org_theater_code == theater_code,
                            models.Theater.is_published == True)

            if theaters.count() == 0:
                return None

            theaters = theaters.fetch()

            for theater in theaters:
                theater_keys.append(theater.key.urlsafe())
                self.DS_MATCH_BY_CODE += 1

            memcache.set("theater::theater_code::%s" % theater_code, theater_keys)

        return theaters

    def query_cinema_in_theater(self, theater, cinema):
        if cinema not in [ c.name for c in theater.cinemas ]:
            return None

        return cinema


DEFAULT_SCHEDULE_CORRELATION = ScraperScheduleCorrelation()
convert_schedule = DEFAULT_SCHEDULE_CORRELATION.convert_schedule
query_theater_with_code = DEFAULT_SCHEDULE_CORRELATION.query_theater_with_code
query_cinema_in_theater = DEFAULT_SCHEDULE_CORRELATION.query_cinema_in_theater