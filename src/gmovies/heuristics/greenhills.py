################################################################################
#
# Deprecated (10/21/2016). Remove or delete this file in the future.
# No need for separate Movies and Schedules Heuristics for different cinema partners.
#
################################################################################


import datetime
import json
import logging

from decimal import Decimal
from uuid import uuid4 as uuid_gen

from google.appengine.ext import ndb

from gmovies import models, orgs
from gmovies.admin.email import rockwell_schedule_heuristics_notification
from gmovies.heuristics import movies
from gmovies.heuristics.rottentomatoes import get_all_correlation_titles

log = logging.getLogger(__name__)
