import logging
import re
import string

from operator import attrgetter
from uuid import uuid4 as uuid_gen

from flask import url_for

from google.appengine.api import taskqueue, memcache
from google.appengine.ext import ndb

from gmovies import orgs
from gmovies.admin import movie_heuristics_notification, movie_multiple_correlation_notification, movie_multiple_correlation_notification_for_cinema76
from gmovies.feeds import rottentomatoes
from gmovies.models import Movie, MovieTitles, MovieTitlesLookup, FeedCorrelation, Feed
from gmovies.util import cache_movie_runtime, roman_to_numeric
from gmovies.util.task import is_from_flixster


log = logging.getLogger(__name__)

CORRELATION_VERSION = 4
BATCH_SIZE = 50

FILTER_WORDS = ['the', 'a', 'an', 'or', 'and', 'is', 'in']
SEARCHABLE_WORDS_FILTER = ['the', 'a', 'an', 'or', 'and', 'is', 'in', 'at', 'of']
ROMAN_NUMERAL_MATCH = '^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$'

variants = ['3D-HFR', '2D', '3D', '4DX', 'ATMOS', '35MM', 'HFR', 'IMAX']
suffix_single_pattern = '(' + '|'.join(variant for variant in variants) + ')'
suffix_multiple_pattern = '(' + '|'.join(variant for variant in variants) + ')\s*/\s*(' + '|'.join(variant for variant in variants) + ')'

SUFFIX_VARIANCE_MATCH = r'\((?i)(' + suffix_multiple_pattern + '|' + suffix_single_pattern + ')\)$'
PREFIX_VARIANCE_MATCH = r'^\((?i)(' + suffix_multiple_pattern + '|' + suffix_single_pattern + ')\)'
IN_SUFFIX_VARIANCE_MATCH = r'\((?i)?in\s+(' + suffix_multiple_pattern + '|' + suffix_single_pattern + ')\)?'
SUFFIX_YEAR_MATCH = r'\((?i)(\d{4})\)$'


def build_whitelist_blacklist_check(blacklist, whitelist):
    log.debug("Blacklist: %s, whitelist: %s" % (blacklist, whitelist))

    def check_field(m, field):
        return field in m and (field not in blacklist) and (field in whitelist or not whitelist)

    return check_field

def transform(per_org_payloads):
    key_to_movie = {}
    payload_movie_matches = {}
    feed_ids = per_org_payloads.keys()
    feeds = ndb.get_multi([ndb.Key(Feed, feed_id) for feed_id in feed_ids])
    prioritized_feeds = reversed(sorted(feeds, key=attrgetter('heuristics_priority')))
    correlator = MovieCorrelation()

    for f in prioritized_feeds:
        feed_id = f.key.id()
        field_blacklist = f.movie_fields_blacklist
        field_whitelist = f.movie_fields_whitelist
        check_field = build_whitelist_blacklist_check(field_blacklist, field_whitelist)
        movie_payload = per_org_payloads[feed_id]

        for m in movie_payload:
            movie, correlation_titles = correlator.correlate(f.key, m, f)

            if not movie:
                log.warn("SKIP!, heuristics, movies, transform, movie not found...")

                continue

            movie_key = movie.key.id()

            if check_field(m, 'movie_title'):
                movie.canonical_title = get_canonical_title(m['movie_title'])
                movie.correlation_title = correlation_titles
                movie.searchable_words = get_searchable_words(m['movie_title'])

            if movie_key not in key_to_movie:
                key_to_movie[movie_key] = movie

            variant = get_variant(m)

            if not check_field(m, 'synopsis'):
                m.pop('synopsis', None)

            if not check_field(m, 'cast'):
                m.pop('cast', None)

            if not check_field(m, 'release_date'):
                m.pop('release_date', None)

            if not check_field(m, 'genre'):
                m.pop('genre', None)

            if not check_field(m, 'runtime_mins'):
                m.pop('runtime_mins', None)

            if not check_field(m, 'advisory_rating'):
                m.pop('advisory_rating', None)

            if check_field(m, 'is_event'):
                movie.is_event = m['is_event']

            if check_field(m, 'ratings'):
                build_ratings_property(movie, m, feed_id)

            movie.bind_org_title(feed_id, variant, m['movie_title'])

            if movie_key not in payload_movie_matches:
                payload_movie_matches[movie_key] = m
            else:
                payload_movie_matches[movie_key].update(m)

            rt_key = ndb.Key(Feed, str(orgs.ROTTEN_TOMATOES))
            rt_corr = filter(lambda corr: corr.org_key == rt_key, movie.movie_correlation)

            if rt_corr:
                title_bank_queryset = MovieTitles.query(MovieTitles.rt_details.rt_id==rt_corr[0].movie_id, MovieTitles.is_active==True)
                rt_ratings_dict = {}
                rt_ratings_dict['ratings'] = get_rottentomatoes_ratings(title_bank_queryset)

                build_ratings_property(movie, rt_ratings_dict, rt_key.id())

            if 'image_url' in m:
                image_url =  m['image_url']
                payload_movie_matches[movie_key]['image_params'] = {'org_uuid' : f.key.id(), 'image_url': image_url, 'parent': movie.key.urlsafe()}

    try:
        movie_heuristics_notification(new_movies=correlator.NEW_MOVIES_ADDED,
                existing_from_cache=correlator.EXISTING_MOVIES_FROM_CACHE,
                existing_from_datastore=correlator.EXISTING_MOVIES_FROM_DATASTORE,
                correlated_by_title=correlator.DS_CORRELATED_BY_TITLE,
                correlated_by_id=correlator.DS_CORRELATED_BY_ORG_AND_ID,
                cached_by_id=correlator.CACHE_MATCH_BY_ID,
                cached_by_title=correlator.CACHE_MATCH_BY_TITLE,
                movies_not_created=correlator.FEED_NO_CREATION,
                multi_correlations=correlator.MULTIPLE_CORRELATION,
                existing_from_movie_title_bank=correlator.EXISTING_FROM_MOVIE_TITLE_BANK,
                movies_added='\n'.join(correlator.MOVIES_ADDED).encode('utf-8'),
                titles_not_in_movie_title_bank='\n'.join(correlator.TITLES_NOT_IN_MOVIE_TITLE_BANK).encode('utf-8'),
                movies_not_match_by_correlation_id_or_title='\n'.join(correlator.MOVIES_NOT_MATCH_BY_CORRELATION_ID_OR_TITLE).encode('utf-8'),
                movies_with_multiple_correlation='\n'.join(correlator.MOVIES_WITH_MULTIPLE_CORRELATION).encode('utf-8'))
    except Exception, e:
        log.warn("ERROR!, heuristics, movies, transform, failed to send email notification...")
        log.error(e)

    return transform_merged_payloads(map(lambda (m_key, m): (key_to_movie[m_key], m), payload_movie_matches.items()))

def transform_merged_payloads(raw_movies):
    movie_list = []
    movie_batch = []

    for movie, m in raw_movies:
        try:
            if movie.is_details_locked:
                log.warn("SKIP!, heuristics, movies, transform_merged_payloads, %s is locked..." % movie.canonical_title)
            else:
                movie.synopsis = m['synopsis'] if 'synopsis' in m else ''
                movie.genre = m['genre'] if 'genre' in m else ''
                movie.cast = m['cast'] if 'cast' in m else ''
                movie.advisory_rating = m['advisory_rating'] if 'advisory_rating' in m else ''
                movie.release_date = m['release_date'] if 'release_date' in m else None
                movie.runtime_mins = m['runtime_mins'] if 'runtime_mins' in m else 0.0

                cache_movie_runtime(movie.key.id(), movie.runtime_mins)
        except AttributeError, e:
            log.warn("ERROR!, heuristics, movies, transform_merged_payloads, AttributeError...")
            log.error(e)
        except Exception, e:
            log.warn("ERROR!, heuristics, movies, transform_merged_payloads...")
            log.error(e)

        movie_batch.append(movie)
        movie_list.append(movie)

        if len(movie_batch) == BATCH_SIZE:
            ndb.put_multi(movie_batch)
            movie_batch = []

    ndb.put_multi(movie_batch)

    return movie_list

def get_variant(movie_dict):
    if 'variant' in movie_dict:
        return movie_dict['variant']

    return variant_from_title(movie_dict['movie_title'])

# strip variance in canonical_title
def get_canonical_title(title):
    canon = title.strip()
    canon = strip_suffix_variance(canon)
    canon = strip_prefix_variance(canon)
    canon = strip_in_suffix(canon)
    # canon = canon.title()
    # canon = canon.replace("'S ", "'s ")
    canon = canon.strip()

    return canon

def get_searchable_words(title):
    if title.strip() == '':
        return []

    title = title.strip()
    title = title.lower()
    words = re.split("[\s\.,:;()?&']", title)

    words = filter(None, [strip_punct(w) for w in words])
    words = filter(lambda w: w != '', words)
    words = filter(lambda w: w not in SEARCHABLE_WORDS_FILTER, words)
    words = list(set(words))

    return words

def get_searchable_title(title):
    if title.strip() == '':
        return title

    title = title.strip()
    title = title.lower()

    words = title.split(' ')
    words = filter(lambda w: w != '', words)

    return '_'.join(words)

def strip_punct(w):
    return filter(lambda c: c not in string.punctuation, w)

def convert_roman_numeral(w):
    if re.search(ROMAN_NUMERAL_MATCH, w):
        return str(roman_to_numeric(w))

    return w

def strip_part_specifier_before_romans(words):
    new_words = []

    for w in words:
        if re.search(ROMAN_NUMERAL_MATCH, w) and new_words and new_words[-1].lower() == 'part':
            new_words.pop()

        new_words.append(w)

    return new_words

def strip_suffix_variance(title):
    return re.sub(SUFFIX_VARIANCE_MATCH, '', title)

def strip_prefix_variance(title):
    return re.sub(PREFIX_VARIANCE_MATCH, '', title)

def strip_in_suffix(title):
    return re.sub(IN_SUFFIX_VARIANCE_MATCH, '', title)

def strip_suffix_year(title):
    return re.sub(SUFFIX_YEAR_MATCH, '', title)

def variant_from_title(title):
    match = re.search(SUFFIX_VARIANCE_MATCH, title)

    if match:
        return match.group(1).upper()

    match = re.search(PREFIX_VARIANCE_MATCH, title)

    if match:
        return match.group(1).upper()

    match = re.search(IN_SUFFIX_VARIANCE_MATCH, title)

    if match:
        return match.group(1).upper()

    return None

def get_all_correlation_titles(title):
    return [get_correlation_title(v, title) for v in range(1, CORRELATION_VERSION + 1)]

def get_correlation_title(v, title):
    if title.strip() == '':
        return title

    title = title.strip()
    title = strip_prefix_variance(title)
    title = strip_suffix_variance(title)
    title = strip_in_suffix(title)
    title = strip_suffix_year(title)

    title_words = title.split(' ')
    title_words = filter(None, [strip_punct(w) for w in title_words])

    if v == 2 or v == 4:
        title_words = strip_part_specifier_before_romans(title_words)
        title_words = [convert_roman_numeral(w) for w in title_words]

    title_words = [w.lower() for w in title_words]
    title_words = filter(lambda w: w != '', title_words)
    title_words = filter(lambda w: w not in FILTER_WORDS, title_words)

    if v <= 2:
        correlation_title = "%s:%s" % (v, '_'.join(title_words))
    else:
        correlation_title = "%s:%s" % (v, ''.join(title_words))

    return correlation_title

def build_ratings_property(movie, movie_dict, feed_id):
    ratings = get_ratings(movie_dict)

    if movie.ratings == None:
        movie.ratings = {}

    movie.ratings[feed_id] = ratings

# all of the ratings should provide a rating and a comment for critics and audiences
def get_ratings(movie_dict):
    ratings = movie_dict['ratings'] if 'ratings' in movie_dict else {}

    if 'critics_rating' in ratings and 'audience_rating' in ratings and 'critics_comment' in ratings and 'audience_comment' in ratings:
        return {'critics' : ratings['critics_rating'], 'audience': ratings['audience_rating'],
                'critics_comment': ratings['critics_comment'], 'audience_comment': ratings['audience_comment']}

    return None

def get_rottentomatoes_image_url(queryset):
    if queryset.count() == 1:
        rt_details = queryset.get().rt_details

        if 'thumbnail' in rt_details.posters:
            image_url = rt_details.posters['thumbnail']
        elif 'profile' in rt_details.posters:
            image_url = rt_details.posters['profile']
        elif 'detailed' in rt_details.posters:
            image_url = rt_details.posters['detailed']
        elif 'original' in rt_details.posters:
            image_url = rt_details.posters['original']
        else:
            return None

        return image_url

    return None

def get_rottentomatoes_ratings(queryset):
    critics_rating = None
    audience_rating = None
    critics_comment = None
    audience_comment = None

    if queryset.count() == 1:
        rt_details = queryset.get().rt_details

        if rt_details.ratings:
            critics_rating = rt_details.ratings.get('critics_score', None)
            audience_rating = rt_details.ratings.get('audience_score', None)
            critics_comment = rt_details.ratings.get('critics_rating', None)
            audience_comment = rt_details.ratings.get('audience_rating', None)

    return {'critics_rating': critics_rating, 'audience_rating': audience_rating,
            'critics_comment': critics_comment, 'audience_comment': audience_comment}

def query_movie_title_bank(correlation_titles):
    queryset = MovieTitles.query(ndb.OR(
                    MovieTitles.correlation_title.IN(correlation_titles),
                    MovieTitles.secondary_titles.correlation_title_version_one.IN(correlation_titles),
                    MovieTitles.secondary_titles.correlation_title_version_two.IN(correlation_titles),
                    MovieTitles.secondary_titles.correlation_title_version_three.IN(correlation_titles),
                    MovieTitles.secondary_titles.correlation_title_version_four.IN(correlation_titles)),
            MovieTitles.is_active == True)

    return queryset


class MovieCorrelation:
    def __init__(self):
        self.NEW_MOVIES_ADDED = 0
        self.EXISTING_MOVIES_FROM_CACHE = 0
        self.EXISTING_MOVIES_FROM_DATASTORE = 0
        self.EXISTING_FROM_MOVIE_TITLE_BANK = 0
        self.DS_CORRELATED_BY_TITLE = 0
        self.DS_CORRELATED_BY_ORG_AND_ID = 0
        self.CACHE_MATCH_BY_ID = 0
        self.CACHE_MATCH_BY_TITLE = 0
        self.FEED_NO_CREATION = 0
        self.MULTIPLE_CORRELATION = 0

        self.MOVIES_ADDED = []
        self.TITLES_NOT_IN_MOVIE_TITLE_BANK = []
        self.MOVIES_NOT_MATCH_BY_CORRELATION_ID_OR_TITLE = []
        self.MOVIES_WITH_MULTIPLE_CORRELATION = []

        self.movie_cache = {}

    def cached(self, key):
        if key not in self.movie_cache:
            self.movie_cache[key] = key.get()

        return self.movie_cache[key]

    def create_movie_title_lookup(self, movie_title_lookup_queryset, movie_title, correlation_titles):
        if movie_title_lookup_queryset.count() == 0:
            log.debug("CREATE!, movie_title: %s, correlation_title: %s in MovieTitlesLookup..." % (movie_title, correlation_titles))
            movie_title_lookup = MovieTitlesLookup()
            movie_title_lookup.movie_title = movie_title
            movie_title_lookup.correlation_title = correlation_titles
            movie_title_lookup.put()
        else:
            log.debug("EXISTING!, movie_title: %s, correlation_title: %s in MovieTitlesLookup..." % (movie_title, correlation_titles))

    def add_movie_variant(self, movie, variant):
        if movie.variants and variant not in movie.variants:
            movie.variants.append(variant)
        elif not movie.variants:
            movie.variants = [variant]

        return movie

    def add_rottentomatoes_id(self, movie, title_bank):
        rt_key = ndb.Key(Feed, str(orgs.ROTTEN_TOMATOES))
        rt_corr = filter(lambda corr: corr.org_key == rt_key, movie.movie_correlation)

        if not rt_corr:
            if title_bank.rt_details.rt_id:
                rt_corr = FeedCorrelation(org_key=rt_key,
                        movie_id=title_bank.rt_details.rt_id)
            else:
                rt_corr = None

            if rt_corr and rt_corr not in movie.movie_correlation:
                if movie.correlation_title:
                    movie.movie_correlation.append(rt_corr)
                else:
                    movie.movie_correlation = [rt_corr]

        return movie

    def correlate(self, org_key, movie_dict, feed=None):
        if not feed:
            log.debug("Getting feed from org_key %s: %s" % (org_key, feed))
            feed = org_key.get()

        log.info("Feed name %s!..." % feed.name)

        try:
            correlation_id = str(movie_dict['id']) if 'id' in movie_dict else None
        except UnicodeEncodeError, e:
            log.warn("ERROR!, heuristics, movies, correlate, correlation_id UnicodeEncodeError...")
            log.error(e)

            correlation_id = unicode(movie_dict['id']) if 'id' in movie_dict else None
        except Exception, e:
            log.warn("ERROR!, heuristics, movies, correlate, correlation_id...")
            log.error(e)

            correlation_id = None

        correlation_titles = get_all_correlation_titles(movie_dict['movie_title'])
        variant = get_variant(movie_dict)

        if correlation_id:
            corr = FeedCorrelation(movie_id=correlation_id, org_key=org_key, variant=variant)
        else:
            corr = None

        log.info("Correlation params: id %s correlation_titles %s" % (correlation_id, correlation_titles))
        cached_movie_id = memcache.get("movie::movie_correlation::org_key/movie_id[%s,%s]" % (org_key.id(), correlation_id))
        cached_titles = filter(None, [memcache.get("movie::correlation_title::%s" % t) for t in correlation_titles])

        if cached_movie_id or cached_titles:
            self.EXISTING_MOVIES_FROM_CACHE += 1

            if cached_movie_id:
                log.debug("Matched against cached feed movie ID %s" % correlation_id)

                movie = self.cached(ndb.Key(urlsafe=cached_movie_id))
                self.CACHE_MATCH_BY_ID += 1
            else: # cached_title
                log.debug("Matched against cached correlation title(s) %s" % correlation_titles)

                # We don't care *which* title; we assume that the
                # different versions all point to the same entity
                movie = self.cached(ndb.Key(urlsafe=cached_titles[0]))
                self.CACHE_MATCH_BY_TITLE += 1

            if movie is not None:
                log.debug("Found correlated item in cache: %s" % movie.key.id())

                if corr and corr not in movie.movie_correlation:
                    # We were correlated by title, cache the correlation title
                    movie.movie_correlation.append(corr)

            if variant: # HACK: Ensure variant specified is in our variants list
                movie = self.add_movie_variant(movie, variant)

            return movie, correlation_titles

        if correlation_id:
            movie_queryset = Movie.query(ndb.OR(
                            ndb.AND(Movie.movie_correlation.org_key == org_key,
                            Movie.movie_correlation.movie_id == correlation_id),
                            Movie.correlation_title.IN(correlation_titles)),
                    Movie.is_inactive == False)

            if movie_queryset.count() == 1:
                self.DS_CORRELATED_BY_ORG_AND_ID += 1
        else:
            movie_queryset = Movie.query(Movie.correlation_title.IN(correlation_titles),
                    Movie.is_inactive == False)

            if movie_queryset.count() == 1:
                self.DS_CORRELATED_BY_TITLE += 1

        movie = movie_queryset.get()

        if movie_queryset.count() == 1:
            title_bank_queryset = query_movie_title_bank(movie.correlation_title)
        else:
            title_bank_queryset = query_movie_title_bank(correlation_titles)

        title_bank = title_bank_queryset.get()

        if movie_queryset.count() == 1 and title_bank_queryset.count() == 1:
            log.debug("1. Movies: Found correlated item: %s!..." % movie.key.id())
            log.debug("1. Movie Title Bank: Found correlated item: %s!..." % title_bank.key.id())

            self.EXISTING_MOVIES_FROM_DATASTORE += 1
            self.EXISTING_FROM_MOVIE_TITLE_BANK += 1

            if corr and corr not in movie.movie_correlation:
                movie.movie_correlation.append(corr)

                for t in movie.correlation_title:
                    memcache.set("movie::correlation_title::%s" % t,
                            movie.key.urlsafe())
            elif corr:
                # We were correlated by ID, cache the correlation ID
                memcache.set("movie::movie_correlation::org_key/movie_id[%s,%s]" % (org_key.id(), correlation_id), movie.key.urlsafe())

                # Cache the correlation_title as well for this entry
                for t in movie.correlation_title:
                    memcache.set("movie::correlation_title::%s" % t, 
                            movie.key.urlsafe())

            if variant: # HACK: Ensure variant specified is in our variants list
                movie = self.add_movie_variant(movie, variant)

            if title_bank.rt_details:
                movie = self.add_rottentomatoes_id(movie, title_bank)
        elif movie_queryset.count() == 1 and title_bank_queryset.count() == 0:
            log.debug("1. Movies: Found correlated item: %s!..." % movie.key.id())
            log.debug("1. Movie Title Bank: Couldn't find correlation_titles: %s!..." % correlation_titles)

            self.EXISTING_MOVIES_FROM_DATASTORE += 1
            self.TITLES_NOT_IN_MOVIE_TITLE_BANK.append(movie_dict['movie_title'])

            movie_title_lookup_queryset = MovieTitlesLookup.query(
                    MovieTitlesLookup.correlation_title.IN(correlation_titles))

            # Create Movie Title in MovieTitlesLookup Model
            self.create_movie_title_lookup(movie_title_lookup_queryset, movie_dict['movie_title'], correlation_titles)

            if corr and corr not in movie.movie_correlation:
                movie.movie_correlation.append(corr)

                for t in movie.correlation_title:
                    memcache.set("movie::correlation_title::%s" % t,
                            movie.key.urlsafe())
            elif corr:
                # We were correlated by ID, cache the correlation ID
                memcache.set("movie::movie_correlation::org_key/movie_id[%s,%s]" % (org_key.id(), correlation_id), movie.key.urlsafe())

                # Cache the correlation_title as well for this entry
                for t in movie.correlation_title:
                    memcache.set("movie::correlation_title::%s" % t, 
                            movie.key.urlsafe())

            if variant: # HACK: Ensure variant specified is in our variants list
                movie = self.add_movie_variant(movie, variant)
        elif movie_queryset.count() == 0 and title_bank_queryset.count() == 1:
            log.debug("1. Movies: Couldn't find correlation_titles: %s!..." % correlation_titles)
            log.debug("1. Movie Title Bank: Found correlated item: %s!..." % title_bank.key.id())

            self.EXISTING_FROM_MOVIE_TITLE_BANK += 1

            movie_queryset = Movie.query(Movie.correlation_title.IN(title_bank.correlation_title),
                    Movie.is_inactive == False)

            if movie_queryset.count() == 1:
                self.EXISTING_MOVIES_FROM_DATASTORE += 1
                movie = movie_queryset.get()

                log.debug("2. Movies: Found correlated item: %s!..." % movie.key.id())

                if corr and corr not in movie.movie_correlation:
                    movie.movie_correlation.append(corr)

                    for t in movie.correlation_title:
                        memcache.set("movie::correlation_title::%s" % t,
                                movie.key.urlsafe())
                elif corr:
                    # We were correlated by ID, cache the correlation ID
                    memcache.set("movie::movie_correlation::org_key/movie_id[%s,%s]" % (org_key.id(), correlation_id), movie.key.urlsafe())

                    # Cache the correlation_title as well for this entry
                    for t in movie.correlation_title:
                        memcache.set("movie::correlation_title::%s" % t,
                                movie.key.urlsafe())

                if variant: # HACK: Ensure variant specified is in our variants list
                    movie = self.add_movie_variant(movie, variant)

                if title_bank.rt_details:
                    movie = self.add_rottentomatoes_id(movie, title_bank)
            elif movie_queryset.count() == 0:
                log.debug("2. Movies: Couldn't find correlation_titles: %s!..." % correlation_titles)

                for secondary_title in title_bank.secondary_titles:
                    movie_queryset = Movie.query(ndb.OR(
                                    Movie.correlation_title.IN([secondary_title.correlation_title_version_one]),
                                    Movie.correlation_title.IN([secondary_title.correlation_title_version_two]),
                                    Movie.correlation_title.IN([secondary_title.correlation_title_version_three]),
                                    Movie.correlation_title.IN([secondary_title.correlation_title_version_four])),
                            Movie.is_inactive == False)

                    if movie_queryset.count() > 0:
                        break

                if movie_queryset.count() == 1:
                    self.EXISTING_MOVIES_FROM_DATASTORE += 1
                    movie = movie_queryset.get()

                    log.debug("3. Movies: Found correlated item: %s using secondary title!..." % correlation_titles)
                    log.debug("3. Movies: Found correlated item: %s!..." % movie.key.id())

                    if corr and corr not in movie.movie_correlation:
                        movie.movie_correlation.append(corr)

                        for t in movie.correlation_title:
                            memcache.set("movie::correlation_title::%s" % t,
                                    movie.key.urlsafe())
                    elif corr:
                        # We were correlated by ID, cache the correlation ID
                        memcache.set("movie::movie_correlation::org_key/movie_id[%s,%s]" % (org_key.id(), correlation_id), movie.key.urlsafe())

                        # Cache the correlation_title as well for this entry
                        for t in movie.correlation_title:
                            memcache.set("movie::correlation_title::%s" % t,
                                    movie.key.urlsafe())

                    if variant: # HACK: Ensure variant specified is in our variants list
                        movie = self.add_movie_variant(movie, variant)

                    if title_bank.rt_details:
                        movie = self.add_rottentomatoes_id(movie, title_bank)
                elif movie_queryset.count() == 0:
                    log.debug("3. Movies: Couldn't find correlation_titles: %s using secondary title!..." % correlation_titles)

                    if feed.allow_movie_creation:
                        log.debug("CREATE!, Movie for movie_id: %s, movie_title: %s, correlation_title: %s!..." % (title_bank.key.id(), title_bank.title, title_bank.correlation_title))

                        self.NEW_MOVIES_ADDED += 1

                        movie = Movie(key=ndb.Key(Movie, str(uuid_gen())),
                                correlation_title=title_bank.correlation_title,
                                per_org_titlemap={}, alternate_posters={})

                        movie.canonical_title = get_canonical_title(title_bank.title)
                        movie.searchable_words = get_searchable_words(title_bank.title)

                        if corr:
                            movie.movie_correlation = [corr]

                        if variant:
                            movie.variants = [variant]

                        if title_bank.rt_details:
                            movie = self.add_rottentomatoes_id(movie, title_bank)

                        for t in correlation_titles:
                            memcache.set("movie::correlation_title::%s" % t, 
                                    movie.key.urlsafe())

                        self.movie_cache[movie.key] = movie

                        self.MOVIES_ADDED.append('feed: %s, movie_title: %s, correlation_id: %s' % (feed.name, movie_dict['movie_title'], correlation_id))
                    else:
                        log.debug("Skipping '%s', this feed is not allowed to create new movies" % correlation_titles)

                        self.FEED_NO_CREATION += 1
                        self.MOVIES_NOT_MATCH_BY_CORRELATION_ID_OR_TITLE.append('feed: %s, movie_title: %s, correlation_id: %s' % (feed.name, movie_dict['movie_title'], correlation_id))

                        return None, correlation_titles
                else:
                    log.warn("3. FIXME!, Movies: Found multiple matching correlation_titles: %s using secondary title!..." % correlation_titles)

                    self.MULTIPLE_CORRELATION += 1
                    self.MOVIES_WITH_MULTIPLE_CORRELATION.append('feed: %s, movie_title: %s' % (feed.name, movie_dict['movie_title']))

                    movie_multiple_correlation_notification(movies_with_multiple_correlation='\n'.join(self.MOVIES_WITH_MULTIPLE_CORRELATION).encode('utf-8'))

                    raise ValueError("Multiple correlated Movies for %s" % movie_dict)
            else:
                log.warn("2. FIXME!, Movies: Found multiple matching correlation_titles: %s!..." % correlation_titles)

                self.MULTIPLE_CORRELATION += 1
                self.MOVIES_WITH_MULTIPLE_CORRELATION.append('feed: %s, movie_title: %s' % (feed.name, movie_dict['movie_title']))

                movie_multiple_correlation_notification(movies_with_multiple_correlation='\n'.join(self.MOVIES_WITH_MULTIPLE_CORRELATION).encode('utf-8'))

                raise ValueError("Multiple correlated Movies for %s" % movie_dict)

            return movie, correlation_titles
        elif movie_queryset.count() == 0 and title_bank_queryset.count() == 0:
            log.debug("1. Movies: Couldn't find correlation_titles: %s!..." % correlation_titles)
            log.debug("1. Movie Title Bank: Couldn't find correlation_titles: %s!..." % correlation_titles)

            self.FEED_NO_CREATION += 1
            self.TITLES_NOT_IN_MOVIE_TITLE_BANK.append(movie_dict['movie_title'])
            self.MOVIES_NOT_MATCH_BY_CORRELATION_ID_OR_TITLE.append('feed: %s, movie_title: %s, correlation_id: %s' % (feed.name, movie_dict['movie_title'], correlation_id))

            movie_title_lookup_queryset = MovieTitlesLookup.query(
                    MovieTitlesLookup.correlation_title.IN(correlation_titles))

            # Create Movie Title in MovieTitlesLookup Model
            self.create_movie_title_lookup(movie_title_lookup_queryset, movie_dict['movie_title'], correlation_titles)

            return None, correlation_titles
        else:
            log.warn("1. FIXME!, Movies or Movie Title Bank: Found multiple matching correlation_titles: %s!..." % correlation_titles)

            # if 'CityMall' not in str(feed.name):
            #     self.MULTIPLE_CORRELATION += 1
            #     self.MOVIES_WITH_MULTIPLE_CORRELATION.append('Feed: <strong>%s</strong>, movie_title: <strong>%s</strong>' % (feed.name, movie_dict['movie_title']))

            #     if 'Cinema 76' in str(feed.name):
            #         movie_multiple_correlation_notification_for_cinema76(movies_with_multiple_correlation='<br />'.join(self.MOVIES_WITH_MULTIPLE_CORRELATION).encode('utf-8'))

            #         log.warn("1. Multiple correlated Movies or Movie Title Bank for %s" % movie_dict)
                
            #     else:
            #         movie_multiple_correlation_notification(movies_with_multiple_correlation='<br />'.join(self.MOVIES_WITH_MULTIPLE_CORRELATION).encode('utf-8'))

            #         raise ValueError("1. Multiple correlated Movies or Movie Title Bank for %s" % movie_dict)

        return movie, correlation_titles


DEFAULT_MOVIE_CORRELATION = MovieCorrelation()
correlate_dict_to_movie_entity = DEFAULT_MOVIE_CORRELATION.correlate
