$(document).ready(function() {
    var today = new Date();
    var transactions_from_date = $("#transactions_from").val(),
        transactions_from_year = parseInt(transactions_from_date.split('-')[0]),
        transactions_from_month = parseInt(transactions_from_date.split('-')[1]) - 1,
        transactions_from_day = parseInt(transactions_from_date.split('-')[2])

    $("#transactions_from").addClass("datepicker").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        maxDate: today,
        onSelect: function(selectedDate) {
            var option = $(this).id == "transactions_to" ? "minDate": "minDate",
            instance = $(this).data("datepicker"),
            date = $.datepicker.parseDate(
                instance.settings.dateFormat ||
                $.datepicker._defaults.dateFormat,
                selectedDate, instance.settings);
            transactions_to.not(this).datepicker("option", option, date);
        }
    });

    var transactions_to = $("#transactions_to").addClass("datepicker").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        maxDate: today,
        minDate: new Date(transactions_from_year, transactions_from_month, transactions_from_day)
    });

    $("#theaterorg_id").change(function() {
        var filter_by = 'theater';
        var theaterorg_id = $("#theaterorg_id").val();

        $.isLoading({text: "Loading..."});

        $.ajax({
            url: "/admin/transactions/filters/",
            type: "GET",
            data: {filter_by: filter_by, theaterorg_id: theaterorg_id},
            success: function(data) {
                var option = $("<option value=''></option>");

                $("#theater_id").empty();
                $("#theater_id").append(option);

                for(var theater_id in data) {
                    var new_option = $("<option value=" + data[theater_id] + ">" + theater_id + "</option>");

                    $("#theater_id").append(new_option);
                    $("#theater_id").trigger("chosen:updated");
                }

                $.isLoading("hide");
            }
        });
    });

    /* $(document).on("change", "input[name='org_uuids']", function(e) {
        var org_uuids = []
        var filter_by = 'theater-v2';

        $.isLoading({text: "Loading..."});
        $('#checkbox-org_uuids :checked').each(function() {
            org_uuids.push($(this).val());
        });

        $.ajax({
            url: "/admin/transactions/filters/",
            type: "GET",
            data: {filter_by: filter_by, org_uuids: org_uuids.join()},
            success: function(data) {
                var option = $("<option value=''></option>");

                $("#theater_id").empty();
                $("#theater_id").append(option);

                for(var theater_id in data) {
                    var new_option = $("<option value=" + data[theater_id] + ">" + theater_id + "</option>");

                    $("#theater_id").append(new_option);
                    $("#theater_id").trigger("chosen:updated");
                }

                $.isLoading("hide");
            }
        });
    }); */

    $("#search_btn").click(function() {
        var form = $("#transactions_sales_reports_form").attr("method", "get");

        form.submit();
    });

    $("#export_btn").click(function() {
        var form = $("#transactions_sales_reports_form").attr("method", "post");

        form.submit();
    });

    $("#purchase_reports_search_btn").click(function() {
        var form = $("#transactions_purchase_reports_form").attr("method", "get");

        form.submit();
    });

    $("#purchase_reports_export_btn").click(function() {
        var form = $("#transactions_purchase_reports_form").attr("method", "post");

        form.submit();
    });

    $("#report_search_btn").click(function() {
        var form = $("#promo_done_report_form").attr("method", "get");

        form.submit();
    });

    $("#report_export_btn").click(function() {
        var form = $("#promo_done_report_form").attr("method", "post");

        form.submit();
    });
});