$( document ).ready(function() {
	$("#theater").change(function() {
		theater_id = $(this).val();
		$.ajax({
			url: '/api/0/theaters/'+theater_id+'/schedules?sort=show_date',
			headers: {'x-gmovies-deviceid': 'test@test'}
		}).done(function(data) {
			var results = data.results;
			var schedule = $("#schedule");
			var optionValues = '<select>';
			var movie_name = ''
			for (var i=0; i < results.length; i++) {
				$.ajax({
					url: '/api/0/movies/'+results[i].schedule.movie+'/',
					async: false,
					headers: {'x-gmovies-deviceid': 'test@test'},
					success: function(data) {
						movie_name = data.movie.canonical_title;
					}
				});
				optionValues += '<option value="' + results[i].id + '">' + results[i].schedule.show_date + ' - ' + movie_name + ' - Cinema' + results[i].schedule.cinema;
				if(results[i].schedule.variant != null) {
					optionValues += ' - ' + results[i].schedule.variant;
				}
				optionValues += '</option>'
			};
			optionValues += '</select>'
			schedule.html(optionValues);
			load_time();

			$('#cinema').val('');
		});
	});

	$('#schedule').change(load_time);
});

function load_time() {
	theater_key = $('#theater').val();
	schedule_key = $('#schedule').val();
	time = $('#time')
	$.ajax({
		url: '/api/0/theaters/'+theater_key+'/schedules/'+schedule_key+'/',
		headers: {'x-gmovies-deviceid': 'test@test'}
	}).done(function(data) {
		var optionValues = '<select>';
		var results = data.schedule
		for (var i=0; i < results.start_times.length; i++) {
			optionValues += '<option value="' + results.start_times[i]+':00">' + results.start_times[i] + '</option>';
		};
		optionValues += '</select>';
		time.html(optionValues);

		$('#cinema').val(data.schedule.cinema);
	});
}