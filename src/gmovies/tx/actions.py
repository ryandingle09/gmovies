import logging
import datetime

from google.appengine.api import memcache, users
from google.appengine.ext.ndb import Key, transactional
from google.appengine.api import taskqueue

from .state_machine import state, to_state_str, transition_state
from .query import query_info, query_otp, query_otp_number, update_otp, query_cf
from .scheduler import reschedule, cancel_reaping
from .payment import prepare_payment, cancel_payment, do_cancel_transaction, do_refund_transaction
from .reservation import prepare_reservation, cancel_reservation
from .util import trigger_send_sms_notification, trigger_tag_refunded, SMS_TX_FAILED_NOTIFICATION

from gmovies.models import ReservationTransaction
from gmovies.exceptions.api import TransactionConflictException, AlreadyCancelledException, NotFoundException
from gmovies.tx.sureseats import create_sureseats_request
import requests
from lxml import etree

log = logging.getLogger(__name__)

MAX_QUEUES=10
SURESEATS_ENDPOINT_PRIMARY = 'http://api.sureseats.com/globe.asp'
SURESEATS_ENDPOINT_STAGING = 'http://api.sureseats.com/globe_test.asp'
STATUS_ACTION = 'STATUS'
SOURCE_ID = '9'
IS_STAGING = True
TIMEOUT_DEADLINE = 45

def _incr_queue_id(client):
    while True:
        queue_id = client.gets('tx_queue_id')

        if queue_id == None:
            client.set('tx_queue_id', 1)

            return 1

        new_queue_id = (queue_id % MAX_QUEUES) + 1 # Cycle from 1 to 4

        if client.cas('tx_queue_id', new_queue_id):
            return new_queue_id


@transactional
def start(device_id, tx_info):
    tx = ReservationTransaction.create_transaction(device_id, tx_info)
    tx_id = tx.key.id()
    client = memcache.Client()
    state_key = "%s::s" % tx_id
    client.set(state_key, state.TX_START)
    tx.state = state.TX_START
    queue_id = _incr_queue_id(client)
    tx.workspace['queue_id'] = queue_id
    tx.put()

    return tx


@transactional
def start_stopgap(device_id, tx_info):
    tx = ReservationTransaction.create_transaction_raw(device_id, tx_info)
    tx_id = tx.key.id()
    client = memcache.Client()
    state_key = "%s::s" % tx_id
    client.set(state_key, state.TX_START)
    tx.state = state.TX_START
    tx.put()

    return tx


state_after_update = {
    state.TX_PAYMENT_ERROR: state.TX_RESERVATION_OK,
    state.TX_RESERVATION_ERROR: state.TX_STARTED,
    state.TX_PREPARE_ERROR: state.TX_START
}


@transactional
def update(device_id, tx_id, tx_info={}):
    tx = query_info(device_id, tx_id)

    if not tx:
        raise NotFoundException(details={ 'tx_id': tx_id })

    if tx.state == state.TX_CANCELLED:
        raise AlreadyCancelledException()

    if tx.state not in state_after_update:
        raise TransactionConflictException(state_after_update.keys())

    cancel_reaping(tx)

    if tx_info:
        payment_info = dict(tx_info['payment'])
        payment_info.pop('type', None)
        same_payment_info = (payment_info == tx.payment_info)

        if 'reservations' in tx_info and tx.state == state.TX_PAYMENT_ERROR:
            raise TransactionConflictException([ to_state_str(state.TX_RESERVATION_ERROR) ])

        old_reservations = tx.reservations
        tx.copy_api_entity(tx_info)
        same_reservation_info = (old_reservations == tx.reservations)

        log.debug("Same payment info? %s", same_payment_info)
        log.debug("Same reservation info? %s", same_reservation_info)
        if not same_payment_info or not same_reservation_info:
            log.debug('Triggering update callbacks')
            tx.trigger_update(same_reservation=same_reservation_info,
                              same_payment=same_payment_info)

        # HACK: Sneak in prepare step (even though we're technically not in TX_PREPARE)
        #
        # We need to do this because we depend particularly on the
        # state of the workspace being set in the prepare_* methods,
        # which in turn depends on the passed in transaction info. If
        # the transaction info changes, we need to re-run the
        # preparation
        r, msg = 'success', None
        if tx.state in [ state.TX_RESERVATION_ERROR ] \
           and not same_reservation_info:
            r, msg = prepare_reservation(tx)
        # HACK: Sneak in error message into tx.workspace['msg'] and don't reschedule
        if r != 'success':
            tx.workspace['msg'] = msg
            tx.put()
            return tx

        if tx.state in [ state.TX_PAYMENT_ERROR, state.TX_RESERVATION_ERROR ] \
           and not same_payment_info:
            r, msg = prepare_payment(tx)
        # HACK: Sneak in error message into tx.workspace['msg'] and don't reschedule
        if r != 'success':
            tx.workspace['msg'] = msg
            tx.put()
            return tx
        #
        # NB: Setting the error message during transaction update with
        # the pre-existing error state (either TX_PAYMENT_ERROR or
        # TX_RESERVATION_ERROR) is semantically wrong because the
        # error message is related to transaction preparation (for
        # instance, invalid MPASS username) and could be unexpected by
        # the client. This should be fixed in a newer public API.

    new_tx_state = state_after_update[tx.state]

    # Clear msg from workspace
    tx.workspace['msg'] = None

    transition_state(tx, tx.state, new_tx_state)
    reschedule(tx)

    return tx


@transactional
def cancel(device_id, tx_id):
    tx = query_info(device_id, tx_id)

    if not tx:
        raise NotFoundException(details={ 'tx_id': tx_id })

    if tx.state == state.TX_CANCELLED:
        raise AlreadyCancelledException()

    allowed_cancellation_states = [state.TX_STARTED, state.TX_PREPARE_ERROR,
            state.TX_CLIENT_PAYMENT_HOLD, state.TX_EXTERNAL_PAYMENT_HOLD,
            state.TX_PAYMENT_ERROR, state.TX_RESERVATION_ERROR]

    if tx.state not in allowed_cancellation_states:
        raise TransactionConflictException([to_state_str(s) for s in allowed_cancellation_states])

    tx.trigger_cancellation()
    tx.previous_state = tx.state
    tx.state = state.TX_CANCELLED
    log.debug('Triggering do_cancel_transaction for: %s~%s' % (device_id, tx_id))
    do_cancel_transaction(tx)
    # Excluded payment codes
    # GR015 - Original Trx ID missing or invalid - Original Trx ID is required or the Original Trx ID that was provided was not found.  This can happen with a query transaction in which the merchant request might not have reached Paygate. 
    # GR053 - Transaction cancelled by user - The transaction was cancelled by the user.
    # GR123 - Incomplete Transaction - Transactions is Incomplete
    # A - Transaction Aborted.
    # C - Transaction Cancelled.
    '''
    skip_sms = False
    if (tx.error_info and 'response_code' in tx.error_info
            and tx.error_info['response_code'] in ['A', 'C', 'GR015', 'GR053', 'GR123']):
       log.debug('Skipping sending of SMS failed notification for: %s~%s, payment_code: %s' % (device_id, tx_id, tx.error_info['response_code']))
        skip_sms = True

    if not skip_sms:
        log.debug('Triggering sending of SMS failed notification for: %s~%s' % (device_id, tx_id))
        trigger_send_sms_notification(tx, SMS_TX_FAILED_NOTIFICATION)
    '''
    tx.put()

    return tx

@transactional
def refund(device_id, tx_id, refund_reason=None, refunded_by = None, source=None):
    tx = query_info(device_id, tx_id)

    if not tx:
        raise NotFoundException(details={ 'tx_id': tx_id })

    if tx.state in [state.TX_CANCELLED, state.TX_REFUNDED]:
        log.warn("ERROR!, transaction already cancelled/refunded...")
        raise AlreadyCancelledException()

    allowed_refund_states = [state.TX_DONE]

    if tx.state not in allowed_refund_states:
        raise TransactionConflictException([to_state_str(s) for s in allowed_refund_states])

    status = 'success'
    result = ''
    payment_gateway = ''
    # if source is gmovies-dashboard no need to process, just tag as refunded
    # else normal processing then
    if source != 'gmovies-dashboard':
        # Add things to do on cancellation
        # call api for refund, GH
        # call api for refund on payment (gcash, pesopay, paynamics)
        # if paynamics, send email to paynamics for refund
        # send email to customer
        log.debug('Triggering do_refund_transaction for: %s~%s' % (device_id, tx_id))
        status, result, payment_gateway = do_refund_transaction(tx, refund_reason)
        log.debug('do_refund_transaction status: %s' % (status))
        log.debug('do_refund_transaction result: %s' % (result))

    if 'success' == status:
        log.debug('Triggering update state to refund for: %s~%s, state:%s' % (device_id, tx_id, tx.state))
        if payment_gateway in ['cash']:
            tx.state = state.TX_CANCELLED_CASH
        else:
            tx.state = state.TX_REFUNDED
        tx.date_cancelled = datetime.datetime.now()
        tx.refund_reason = refund_reason
        tx.refunded_by   = refunded_by if refunded_by else users.get_current_user().email()
        tx.put()

        # call gmovies api to tag as refunded on their end
        if source != 'gmovies-dashboard':
            tag_status, tag_result = trigger_tag_refunded(tx)

    return status, result

def update_pin(pin):
    otp = update_otp()
    otp.code = int(pin)
    otp.put()
    return pin

def get_mobile():
    m = query_otp_number()
    return m.number

def update_state(device_id, tx_id, code):
    tx = query_info(device_id, tx_id)
    otp = query_otp(code)
    otpnumber = query_otp_number()

    status = 'success'
    result = ''

    log.debug("TX_INFO : %s" % (tx))
    log.debug("OTP_INFO : %s" % (otp.code))
    log.debug("OTP_FORM_PIN : %s" % (code))
    log.debug("OTP_NUM_INFO : %s" % (otpnumber.number))

    if otp is not None and int(otp.code) == int(code):
        log.warn("Code successfully verified.")
    else:
        result = 'Invalid Verification Code.'
        log.debug("Code does not match ..")
        return status, result

    if not tx:
        raise NotFoundException(details={ 'tx_id': tx_id })

    if tx.state  == state.TX_CANCELLED:
        log.debug("State detected as TX_CANCELLED ..")
        tx.state = state.TX_DONE
        tx.put()
        result = 'Transaction State successfully updated to TX_DONE.'
        log.debug("State changed to TX_DONE ..")
    else:
        log.debug("State detected not equal to TX_CANCELLED ..")
        tx.state = state.TX_CANCELLED
        tx.put()
        result = 'Transaction State successfully updated to TX_CANCELLED.'
        log.debug("State changed to TX_CANCELLED ..")

    return status, result

def update_status_by_qrcode(device_id, tx_id):
    status = ''
    result = ''

    tx = query_info(device_id, tx_id)

    if not tx:
        raise NotFoundException(details={ 'tx_id': tx_id })

        status = 'error'
        result = 'Transaction not found.'
        
        log.debug("update_status_by_qrcode Error Transaction not found")
    else:
        log.info("Payment type: %s" % str(tx.payment_type))
        if tx.payment_type in ['cash']:
            if IS_STAGING:
                endpoint = SURESEATS_ENDPOINT_STAGING
            else:
                endpoint = SURESEATS_ENDPOINT_PRIMARY
            code = (tx.ticket).transaction_code_list

            status_payload = {'src': SOURCE_ID, 'action': STATUS_ACTION, 'claim_code': str(code[0])}
            log.info("payload: %s" % str(status_payload))
            req = create_sureseats_request(endpoint, status_payload)

            log.info("######################### START ############################")
            log.info(req.url)
            log.info("########################## END #############################")
            s = requests.Session()
            r = s.send(req, timeout=TIMEOUT_DEADLINE)

            if r.status_code != 200:
                log.warn("ERROR!, update_status_by_qrcode, cannot perform status retrieval, there was an error calling SureSeats...")
                return jsonify(status='skipped')
            
            log.info("update_status_by_qrcode, got response: %s..." % str(r.text))

            xml = etree.fromstring(str(r.text))
            
            try:
                xml_status = str(xml.xpath('//TransStatus/Status/text()')[0]).lower()
            except IndexError:
                xml_status = ''
            try:
                error_message = str(xml.xpath('//TransStatus/Error/text()')[0])
            except IndexError:
                error_message = ''

            if xml_status and not error_message:
                if xml_status == 'claimed':
                    tx.status = 'claimed over-the-counter'
                    tx.put()
                    status = 'success'
                    result = 'Transaction Status successfully updated to claimed.'
                    log.debug("update_status_by_qrcode Transaction changed status to claimed over-the-counter..")
                elif xml_status == 'forfeited':
                    tx.status = xml_status
                    tx.put()
                    status = 'success'
                    result = 'Transaction was not claimed before scheduled screening therefore forfeited.'
                    log.debug("update_status_by_qrcode Transaction changed status to forfeited..")
                else:
                    status = 'error'
                    result = ''.join(['Transaction status is ', xml_status, '.'])
                    log.debug("update_status_by_qrcode Transaction status unchanged, Sureseat status is %s.." % str(xml_status))
                    if xml_status == 'cancelled':
                        tx.status = xml_status
                        tx.put()
            else:
                status = 'error'
                result = error_message
                
        else:
            tx.status = 'claimed'
            tx.put()

            status = 'success'
            result = 'Transaction Status successfully updated to claimed.'
            
            log.debug("update_status_by_qrcode Transaction changed status to Claimed ..")

    return status, result

def get_convenience_fee(org_id):
    status = ''
    result = ''

    cf = query_cf(org_id)

    if not cf:
        status = 'error'
        result = 'Org not found.'
        
        log.debug("get_convenience_fee Error Org not found")
        
    else:
        status = 'success'
        result = cf
        
        log.debug("get_convenience_fee found in Org id :%s .." % (org_id))

    return status, result