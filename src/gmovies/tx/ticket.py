# coding: utf-8
import Image
import ImageDraw
import ImageFont
import logging
import yaml
import traceback

from datetime import datetime
from StringIO import StringIO
from uuid import uuid4 as uuid_gen

from google.appengine.ext.ndb import Key, non_transactional
from google.appengine.api import images

from .barcode import generate_ticket_barcode, generate_ticket_barcode_custom, generate_sample_ticket_barcode
from .qrcode import generate_ticket_qrcode, generate_sample_ticket_qrcode

from gmovies import orgs
from gmovies.models import Theater, TheaterOrganization, TicketData, TicketImageBlob, TicketTemplateImageBlob, FreeTicketData, DEFAULT_RUNTIME_MINS
from gmovies.util.admin import convert_timezone


log = logging.getLogger(__name__)

DEFAULT_ERROR_MESSAGE = 'An error occurred during your transaction. Kindly contact support@gmovies.ph to address your issue.'
TICKET_DATETIME_FORMAT = '%m/%d/%Y %I:%M %p'


def do_ticket(tx):
    log.debug("do_ticket, start...")

    if "RSVP:claimcode" not in tx.workspace:
        log.warn("ERROR!, do_ticket, missing key RSVP:claimcode...")

        return "error", DEFAULT_ERROR_MESSAGE

    theater_name, is_multiple_tickets = __get_is_multiple_tickets(tx.workspace['theaters.key'][0])
    ref = tx.reservation_reference
    code = tx.workspace['RSVP:claimcode']
    date = tx.workspace['RSVP:claimdate']
    amount = tx.workspace['RSVP:totalamount']
    prices = tx.workspace['schedules.price'] or ['0.00']
    popcorn_prices = ['0.00']
    original_seat_price = str(tx.total_seat_price(tx.workspace['RSVP:fee'], True))
    original_total_amount = str(tx.total_amount(tx.workspace['RSVP:fee'], True))
    total_seat_price = str(tx.total_seat_price(tx.workspace['RSVP:fee'], True))
    total_discount = '0.00'
    total_seat_discount = '0.00'
    total_discounted_seats = '0'
    discount_type = ''

    if 'schedules.popcorn_price' in tx.workspace:
        popcorn_prices = tx.workspace['schedules.popcorn_price'] or ['0.00']

    if 'RSVP:originalseatprice' in tx.workspace:
        original_seat_price = tx.workspace['RSVP:originalseatprice']

    if 'RSVP:originaltotalamount' in tx.workspace:
        original_total_amount = tx.workspace['RSVP:originaltotalamount']

    if 'RSVP:totalseatprice' in tx.workspace:
        total_seat_price = tx.workspace['RSVP:totalseatprice']

    if 'RSVP:totaldiscount' in tx.workspace:
        total_discount = tx.workspace['RSVP:totaldiscount']

    if 'RSVP:totalseatdiscount' in tx.workspace:
        total_seat_discount = tx.workspace['RSVP:totalseatdiscount']

    if 'RSVP:totaldiscountedseats' in tx.workspace:
        total_discounted_seats = str(tx.workspace['RSVP:totaldiscountedseats'])

    if 'RSVP:discounttype' in tx.workspace:
        discount_type = tx.workspace['RSVP:discounttype']

    ticket = TicketData(ref=ref, code=code, date=date, extra={}, seat_transaction_codes={})
    ticket.extra['platform'] = tx.platform
    ticket.extra['movie_id'] = tx.workspace['schedules.movie'][0].key.id()
    ticket.extra['movie_title'] = tx.workspace['RSVP:movie']
    ticket.extra['movie_canonical_title'] = tx.workspace['schedules.movie'][0].canonical_title
    ticket.extra['movie_rating'] = tx.workspace['schedules.movie'][0].advisory_rating if tx.workspace['schedules.movie'][0].advisory_rating else ''
    ticket.extra['movie_genre'] = tx.workspace['schedules.movie'][0].genre if tx.workspace['schedules.movie'][0].genre else ''
    ticket.extra['runtime'] = str(tx.workspace['schedules.movie'][0].runtime_mins) if tx.workspace['schedules.movie'][0].runtime_mins else DEFAULT_RUNTIME_MINS
    ticket.extra['theater_name'] = tx.workspace['theaters.theater_name'] if 'theaters.theater_name' in tx.workspace else ''
    ticket.extra['cinema_name'] = tx.workspace['theaters.cinemas.cinema_name'] if 'theaters.cinemas.cinema_name' in tx.workspace else ''
    ticket.extra['theater_and_cinema_name'] = tx.workspace['theaters.theater_cinema_name']
    ticket.extra['show_datetime'] = tx.workspace['schedules.show_datetime'][0].strftime(TICKET_DATETIME_FORMAT)
    ticket.extra['seating_type'] = tx.workspace['seating_type']
    ticket.extra['variant'] = tx.workspace['schedules.variant'][0]
    ticket.extra['per_ticket_price'] = str(prices[0])
    popcorn_price = str(popcorn_prices[0])
    if not ('0.00' == popcorn_price or popcorn_price is None or 'None' == popcorn_price):
        ticket.extra['popcorn_price'] = str(popcorn_prices[0])
        ticket.extra['total_popcorn_price'] = str(popcorn_prices[0] * tx.ticket_count())
    ticket.extra['reservation_fee'] = tx.workspace['RSVP:fee']
    ticket.extra['seats'] = tx.workspace['seats::imploded']
    ticket.extra['ticket_count'] = tx.ticket_count()
    ticket.extra['total_amount'] = currency_format(amount)
    ticket.extra['discount_type'] = discount_type
    ticket.extra['original_seat_price'] = original_seat_price
    ticket.extra['original_total_amount'] = original_total_amount
    ticket.extra['total_seat_price'] = total_seat_price
    ticket.extra['total_discount'] = total_discount
    ticket.extra['total_seat_discount'] = total_seat_discount
    ticket.extra['total_discounted_seats'] = total_discounted_seats
    ticket.extra['is_discounted'] = tx.workspace['discount::is_discounted']
    ticket.extra['is_multiple_tickets'] = False

    if __get_is_qrcode_ticket(tx.workspace['theaters.org_id'][0]):
        log.debug("do_ticket, generate ticket UI version 2...")

        ticket.extra['ticket_ui_version'] = '2'
    else:
        log.debug("do_ticket, generate ticket UI version 1...")

        ticket.extra['ticket_ui_version'] = '1'

    tx.ticket = ticket

    if is_multiple_tickets and 'RSVP:seat_txs_dict' in tx.workspace:
        log.debug("do_ticket, generating multiple tickets, is_multiple_tickets...")

        ticket.extra['is_multiple_tickets'] = True

        count = 0

        for seat_tx_tup in tx.workspace['RSVP:seat_txs_dict']:
            count += 1

            log.debug("count_multiple_ticket_on_loop_before_generate_ticket: %s" % (count))

            generate_ticket_barcode_custom(tx, font_size=20, text_distance=2, dpi=230, seat_tx_code=seat_tx_tup[1])
            generate_ticket_qrcode(tx, seat_tx_code=seat_tx_tup[1])

            ticket.seat_transaction_codes[seat_tx_tup[0]] = seat_tx_tup[1]
            ticket.transaction_code_list.append(seat_tx_tup[1])
            tx.put()

            # only generate a ticket image if we aren't a raw reservation.
            if 'is_raw' not in tx.workspace:
                try:
                    log.debug("do_ticket, is_multiple_tickets, start generate_ticket_image...")

                    generate_ticket_image(tx, seat_name=seat_tx_tup[0], seat_tx_code=seat_tx_tup[1], count=count)
                except Exception, e:
                    log.warn("ERROR!, do_ticket, generate_ticket_image, is_multiple_tickets...")
                    log.error(traceback.format_exc(e))
    else:
        log.debug("do_ticket, generating single ticket, not is_multiple_tickets...")

        generate_ticket_barcode_custom(tx, font_size=20, text_distance=2, dpi=230)
        generate_ticket_qrcode(tx)

        ticket.transaction_code_list.append(code)
        tx.put()

        # only generate a ticket image if we aren't a raw reservation.
        if 'is_raw' not in tx.workspace:
            try:
                log.debug("do_ticket, not is_multiple_tickets, start generate_ticket_image...")

                generate_ticket_image(tx)
            except Exception, e:
                log.warn("ERROR!, do_ticket, generate_ticket_image, not is_multiple_tickets...")
                log.error(traceback.format_exc(e))

    # free tickets are only for special and rush-rewards for now
    if discount_type in ['special', 'rush-rewards']:
        # loop through seats, get actual discounted seats
        if 'seat_ids' in tx.workspace:
            seat_ids = tx.workspace['seat_ids']
        else:
            seat_ids = tx.workspace['seats']
        # reverse the order; assigned free tickets starts at last part of the list
        seat_ids = seat_ids[::-1]
        discounted_seat_count = tx.discount_info['actual_discounted_seats']
        log.debug("do_ticket, saving %s free tickets for discount_type:%s..." % (discounted_seat_count, discount_type))

        # For every free ticket in transaction, save FreeTicketData entry
        ctr = 0
        while ctr < discounted_seat_count:
            log.debug("do_ticket, saving free ticket ctr:%s..." % (ctr))
            free_ticket = FreeTicketData(parent=tx.key.parent())
            free_ticket.transaction = tx.key
            free_ticket.transaction_date = tx.date_created
            free_ticket.reservation_reference = ref
            free_ticket.payment_reference = code
            free_ticket.movie = ticket.extra['movie_canonical_title']
            free_ticket.email = tx.payment_info['email']
            if 'mobile' in tx.payment_info:
                free_ticket.mobile = tx.payment_info['mobile']
            else:
                free_ticket.mobile = ''
                if 'mobile' in tx.user_info:
                    free_ticket.mobile = tx.user_info['mobile']
            free_ticket.show_datetime = convert_timezone(tx.workspace['schedules.show_datetime'][0], 8, '-') # convert to UTC
            free_ticket.discount_type = discount_type
            free_ticket.platform = tx.platform
            free_ticket.theater = tx.workspace['theaters.key'][0]
            free_ticket.cinema = ticket.extra['cinema_name']
            free_ticket.seat_id = str(seat_ids[ctr])
            seat_name = ' '
            if 'seat_label_id_mapping' in tx.workspace:
                seat_name = str(tx.workspace['seat_label_id_mapping'][seat_ids[ctr]])
            free_ticket.seat_name = seat_name

            free_ticket.put()
            ctr+=1

    log.debug("do_ticket, process done...")

@non_transactional
def currency_format(amount, amount_format='{:.2f}'):
    amount = float(amount)

    return amount_format.format(amount)

@non_transactional
def get_ticket_template(org_id, template_name):
    org_key = Key(TheaterOrganization, org_id)
    template_key = Key(TicketImageTemplate, template_name, parent=org_key)

    return template_key.get()

@non_transactional
def __get_is_qrcode_ticket(org_id):
    org = Key(TheaterOrganization, org_id).get()

    return org.is_qrcode_ticket

@non_transactional
def __get_is_old_ticket_template(org_id):
    org = Key(TheaterOrganization, org_id).get()

    return org.is_old_ticket_template

@non_transactional
def __get_is_theaterorg_ticket_template(workspace):
    org_id = workspace['theaters.org_id'][0]
    org_key = Key(TheaterOrganization, org_id)
    org = org_key.get()

    if 'is_theaterorg_ticket_template' in workspace:
        return workspace['is_theaterorg_ticket_template']

    return org.is_theaterorg_ticket_template

@non_transactional
def __get_is_multiple_tickets(theater_key):
    is_multiple_tickets = False
    theater_name = None

    if theater_key and theater_key is not None:
        theater = theater_key.get()
        theater_name = theater.name

        if theater.is_multiple_tickets:
            is_multiple_tickets = theater.is_multiple_tickets

    return theater_name, is_multiple_tickets

@non_transactional
def __get_org_template_details(org_id):
    org = Key(TheaterOrganization, org_id).get()

    return org.template_image, org.template, org.ticket_remarks

@non_transactional
def __get_movie_ticket_template_details(movie, org_id):
    org = Key(TheaterOrganization, org_id).get()
    ticket_template_image = TicketTemplateImageBlob.query(TicketTemplateImageBlob.template_type=='movie', ancestor=movie.key).get()
    template_image = org.template_image
    template = org.template

    if ticket_template_image:
        if ticket_template_image.image:
            template_image = ticket_template_image.image
            template = org.movie_ticket_template

    return template_image, template

@non_transactional
def __get_theaterorg_ticket_template_details(org_id):
    org_key = Key(TheaterOrganization, org_id)
    org = org_key.get()
    ticket_template_image = TicketTemplateImageBlob.query(TicketTemplateImageBlob.template_type=='theaterorg', ancestor=org_key).get()
    template_image = org.template_image
    template = org.template

    if ticket_template_image:
        if ticket_template_image.image and org.theaterorg_ticket_template:
            template_image = ticket_template_image.image
            template = org.theaterorg_ticket_template

    return template_image, template

def generate_ticket_image(tx, seat_name=None, seat_tx_code=None, count=None):
    remarks = ""
    variant = tx.workspace['schedules.variant'][0]
    org_id = tx.workspace['theaters.org_id'][0]
    theater_key = tx.workspace['theaters.key'][0]

    # default ticket template details.
    is_old_ticket_template = False
    template_img, template, ticket_remarks = __get_org_template_details(org_id)
    is_qrcode = __get_is_qrcode_ticket(org_id)
    theater_name, is_multiple_tickets = __get_is_multiple_tickets(theater_key)

    # ticket details.
    transaction_code = tx.ticket.code
    ticket_count = tx.ticket_count()
    movies = tx.workspace['schedules.movie']
    show_datetimes = tx.workspace['schedules.show_datetime']
    prices = tx.workspace['schedules.price'] or ['0.00']
    seats = tx.workspace['seats::imploded']
    theater_and_cinema_name = tx.workspace['theaters.theater_cinema_name']
    total_price = tx.ticket.extra['total_amount']
    total_discount = tx.ticket.extra['total_discount']
    barcode_data = tx.ticket.barcode
    qrcode_data = tx.ticket.qrcode
    claim_code = None

    if tx.discount_info:
        if 'claim_code' in tx.discount_info:
            claim_code = tx.discount_info['claim_code']

    if ticket_remarks:
        log.debug("generate_ticket_image, get ticket remarks...")

        remarks = ticket_remarks[tx.payment_type]
        log.debug("generate_ticket_image, remarks: %s" % remarks)

    if remarks:
        remarks = remarks.split('\n')

    # replace default ticket template definition and ticket template image.
    if __get_is_theaterorg_ticket_template(tx.workspace) and has_theaterorg_ticket_template(org_id):
        is_old_ticket_template = __get_is_old_ticket_template(org_id)
        template_img, template = __get_theaterorg_ticket_template_details(org_id)

        if is_old_ticket_template:
            log.debug("generate_ticket_image, generate ticket UI version 0...")

            tx.ticket.extra['ticket_ui_version'] = '0'
            tx.put()
    elif has_movie_ticket_template(movies[0], org_id):
        template_img, template = __get_movie_ticket_template_details(movies[0], org_id)

    if is_qrcode:
        barcode_data = qrcode_data

    template = template_yaml_to_metrics(template)

    if is_multiple_tickets:
        log.debug("generate_ticket_image, additional details for multiple tickets...")

        ticket_count = 1
        total_price = tx.ticket.extra['original_seat_price']

        if 'discount::is_discounted' in tx.workspace and tx.workspace['discount::is_discounted']:
            total_discount = tx.workspace['discount::multiple_tickets::total_seat_discount']
            total_price = tx.workspace['discount::multiple_tickets::total_seat_price']

        if seat_name and seat_name is not None:
            seats = seat_name

        if seat_tx_code and seat_tx_code is not None:
            transaction_code = seat_tx_code

    log.debug("count_multiple_ticket_on_loop_before_draw_ticket: %s" % (count))

    ticket_img = draw_ticket(template_img, template, barcode_data, {
            'movie_title': movies[0].canonical_title.upper(),
            'show_datetime': show_datetimes[0].strftime(TICKET_DATETIME_FORMAT),
            'theater_name': theater_and_cinema_name[0],
            'ticket_count': ticket_count,
            'total_ticket_count': tx.ticket_count(),
            'per_ticket_price': str(prices[0]),
            'total_price': total_price,
            'total_discount': total_discount,
            'reservation_fee': tx.workspace['RSVP:fee'],
            'seats_imploded': seats,
            'code': transaction_code,
            'variant': variant,
            'seat_type': tx.workspace['seating_type'],
            'remarks': remarks,
            'claim_code': claim_code
    }, is_qrcode=is_qrcode, is_old_ticket_template=is_old_ticket_template, count=count)

    png_data = ticket_img.getvalue()
    save_ticket_image(png_data, tx.key, seat_name, seat_tx_code)

# generate sample ticket in GMovies GAE CMS. will deprecate and remove generate_sample_ticket_image() in the future.
def generate_sample_ticket_image(movie, workspace={}):
    # FIXME: Assumes only one reservation for now
    remarks = ""
    variant = '2D'
    org_id = str(orgs.AYALA_MALLS)

    # Default Ticket Template Details
    is_old_ticket_template = False
    template_img, template, ticket_remarks = __get_org_template_details(org_id)
    is_qrcode = __get_is_qrcode_ticket(org_id)

    # Ticket details
    show_datetimes = 'MM/DD/YYYY HH:MM AM' # sample show_date and show_time
    prices = '0.00' # sample price
    theater_and_cinema_name = 'Theater - Cinema' # sample theater and cinema
    ticket_count = '0' # sample ticket count
    total_price = '0.00' # sample total price
    total_discount = '0.00' # sample total discount
    reservation_fee = '0.00' # sample reservation fee
    seats_imploded = 'M35,M34,M33,M32,M31,M30,M29,M28,M27,M26' # sample seats_imploded
    ticket_code = 'SAMPLETICKETCODE0001' # sample ticket code

    barcode_data = generate_sample_ticket_barcode(ticket_code,
                font_size=20, text_distance=2, dpi=230)
    qrcode_data = generate_sample_ticket_qrcode(ticket_code)

    if ticket_remarks:
        log.info('Get Remarks...')
        remarks = ticket_remarks['globe-promo']

    if not remarks:
        remarks = ""
    else:
        remarks = remarks.split('\n')

    if is_qrcode:
        log.info('Generate Ticket UI Version 2')
    else:
        log.info('Generate Ticket UI Version 1')

    # Replace Default Ticket Template Definition and Ticket Template Image
    if __get_is_theaterorg_ticket_template(workspace) and has_theaterorg_ticket_template(org_id):
        is_old_ticket_template = __get_is_old_ticket_template(org_id)
        template_img, template = __get_theaterorg_ticket_template_details(org_id)

        if is_old_ticket_template:
            log.info('Generate Ticket UI Version 0')
    elif has_movie_ticket_template(movie, org_id):
        is_old_ticket_template = False
        template_img, template = __get_movie_ticket_template_details(movie, org_id)

    if is_qrcode:
        barcode_data = qrcode_data

    template = template_yaml_to_metrics(template)

    ticket_img = draw_ticket(template_img, template, barcode_data, {
        'movie_title': movie.canonical_title.upper(),
        'show_datetime': show_datetimes,
        'theater_name': theater_and_cinema_name,
        'ticket_count': ticket_count,
        'per_ticket_price': str(prices),
        'total_price': total_price,
        'total_discount': total_discount,
        'reservation_fee': reservation_fee,
        'seats_imploded': seats_imploded,
        'code': ticket_code,
        'variant': variant,
        'seat_type': 'Free Seating',
        'remarks': remarks
    }, is_qrcode=is_qrcode, is_old_ticket_template=is_old_ticket_template)

    png_data = ticket_img.getvalue()

    return png_data

def draw_ticket(template_img, template, barcode, ticket_data, is_qrcode=False, is_old_ticket_template=False, count=None):
    log.debug("draw_ticket, start...")

    if is_qrcode:
        if not is_old_ticket_template:
            log.debug("draw_ticket, new generic ticket with qrcode...")

            template['seats_imploded_label'].y = 660
            template['seats_imploded_label'].text_align = 'left'
            template['seats_imploded_label'].width = 100
            template['seats_imploded'].y = 645
            template['seats_imploded'].text_align = 'left'
            template['seats_imploded'].width = 430
    else:
        if not is_old_ticket_template:
            log.debug("draw_ticket, barcode...")

            ticket_data.pop('code', None)
            template.pop('code', None)

    log.debug("draw_ticket, extracting ticket data...")

    seats_imploded = ticket_data['seats_imploded'].split(',')
    template, seats_imploded = new_line_seats_imploded(template, seats_imploded, is_old_ticket_template)
    template = new_line_movie_title(template, ticket_data['movie_title'], is_old_ticket_template)

    if is_old_ticket_template:
        price_text = "Purchased %s @ Php %s each\nReservation Fee: Php %s\nTotal Price: Php %s" % (ticket_data['ticket_count'], ticket_data['per_ticket_price'], ticket_data['reservation_fee'], ticket_data['total_price'])
        barcode_loc = template['barcode']
        ticket_data['seats_imploded_label'] = ''
    else:
        if ticket_data['claim_code'] is not None and count is not None:
            log.debug("detected_claim_code_discount")
            log.debug("total_discount: %s" % (ticket_data['total_discount']))

            price_text = "Purchased %s @ Php %s each\nReservation Fee: Php %s\nDiscounts: Php %s" % (ticket_data['ticket_count'], ticket_data['per_ticket_price'], ticket_data['reservation_fee'], ticket_data['total_discount'])

            if str(ticket_data['claim_code']) in ['REEL50','REEL75']:
                log.debug("detected_claim_code_discount: %s" % (ticket_data['claim_code']))

                if count == ticket_data['total_ticket_count']:
                    price_text = "Purchased %s @ Php %s each\nReservation Fee: Php %s\nDiscounts: Php %s" % (ticket_data['ticket_count'], ticket_data['per_ticket_price'], ticket_data['reservation_fee'], ticket_data['total_discount'])

            elif str(ticket_data['claim_code']) in ['REEL100']:
                log.debug("detected_claim_code_discount: %s" % (ticket_data['claim_code']))
                
                counter = int(ticket_data['total_ticket_count']) - 2
                if count > counter:
                    price_text = "Purchased %s @ Php %s each\nReservation Fee: Php %s\nDiscounts: Php %s" % (ticket_data['ticket_count'], ticket_data['per_ticket_price'], ticket_data['reservation_fee'], ticket_data['total_discount'])

            elif str(ticket_data['claim_code']) in ['REEL150']:
                log.debug("detected_claim_code_discount: %s" % (ticket_data['claim_code']))

                if '2D' in ticket_data['variant']:
                    log.debug("variant: %s" % (ticket_data['variant']))

                    counter = int(ticket_data['total_ticket_count']) - 3
                    if count > counter:
                        price_text = "Purchased %s @ Php %s each\nReservation Fee: Php %s\nDiscounts: Php %s" % (ticket_data['ticket_count'], ticket_data['per_ticket_price'], ticket_data['reservation_fee'], ticket_data['total_discount'])
                
                if '3D' in ticket_data['variant'] or '4D' in ticket_data['variant'] or 'IMAX' in ticket_data['variant']:
                    log.debug("variant: %s" % (ticket_data['variant']))
                    
                    counter = int(ticket_data['total_ticket_count']) - 2
                    if count > counter:
                        price_text = "Purchased %s @ Php %s each\nReservation Fee: Php %s\nDiscounts: Php %s" % (ticket_data['ticket_count'], ticket_data['per_ticket_price'], ticket_data['reservation_fee'], ticket_data['total_discount'])

        else:
            price_text = "Purchased %s @ Php %s each\nReservation Fee: Php %s\nDiscounts: Php 0.00" % (ticket_data['ticket_count'], ticket_data['per_ticket_price'], ticket_data['reservation_fee'])

        if ticket_data['variant']:
            theater_show_datetime_text = "%s, %s\n%s" % (ticket_data['theater_name'], ticket_data['variant'], ticket_data['show_datetime'])
        else:
            theater_show_datetime_text = "%s\n%s" % (ticket_data['theater_name'], ticket_data['show_datetime'])

        theater_show_datetime_text = theater_show_datetime_text.split('\n')
        ticket_data['theater_show_datetime'] = theater_show_datetime_text

        log.debug("count_multiple_ticket_on_loop: %s" % (count))

        if ticket_data['claim_code'] is not None and count is not None:

            total = float(ticket_data['per_ticket_price']) * float(ticket_data['ticket_count']) + float(ticket_data['reservation_fee']) - float(ticket_data['total_discount'])
            total_text = 'Total Price: Php %s.00' % int(total)

            if str(ticket_data['claim_code']) in ['REEL50','REEL75']:
                if count == ticket_data['total_ticket_count']:
                    total_text = 'Total Price: Php %s' % str(ticket_data['total_price'])
                
            elif str(ticket_data['claim_code']) in ['REEL100']:
                counter = int(ticket_data['total_ticket_count']) - 2
                if count > counter:
                    total_text = 'Total Price: Php %s' % str(ticket_data['total_price'])
            
            elif str(ticket_data['claim_code']) in ['REEL150']:
                if '2D' in ticket_data['variant']:
                    counter = int(ticket_data['total_ticket_count']) - 3
                    if count > counter:
                        total_text = 'Total Price: Php %s' % str(ticket_data['total_price'])
                    
                if '3D' in ticket_data['variant'] or '4D' in ticket_data['variant'] or 'IMAX' in ticket_data['variant']:
                    counter = int(ticket_data['total_ticket_count']) - 2
                    if count > counter:
                        total_text = 'Total Price: Php %s' % str(ticket_data['total_price'])
            
            ticket_data['total_price'] = total_text

        else:
            ticket_data['total_price'] = 'Total Price: Php %s' % str(ticket_data['total_price'])

        if ticket_data['seat_type'] in ['Free Seating', 'Guaranteed Seats']:
            ticket_data['seats_imploded_label'] = ''
            seats_imploded = ticket_data['seat_type']
        else:
            ticket_data['seats_imploded_label'] = 'Seat No.'

    if is_qrcode:
        if is_old_ticket_template:
            barcode_loc = center_qrcode_on_ticket(template, template_img, barcode)
        else:
            barcode_loc = template['qrcode']
    else:
        barcode_loc = center_barcode_on_ticket(template, template_img, barcode)

    price_text = price_text.split('\n')
    ticket_data['price'] = price_text
    ticket_data['seats_imploded'] = seats_imploded
    base_canvas = StringIO(template_img)
    text_canvas = ImageText(base_canvas, background=(255, 255, 255, 255), supersample=True)

    log.debug("draw_ticket, plotting ticket data...")

    for key, metrics in template.items():
        if key != 'barcode' and key != 'qrcode':
            text = ticket_data[key]
            draw_text_on_ticket(text_canvas, text, metrics)

    ticket = StringIO()

    log.debug("draw_ticket, creating canvas...")

    # paste barcode.
    base_image = text_canvas.merge_with_base()
    barcode_f = StringIO(barcode)
    barcode_img = Image.open(barcode_f)

    log.debug("draw_ticket, masking barcode...")

    # we just need a bitmask of the pixels that are non-white, so we can get a transparent background from the barcode.
    barcode_mask = barcode_img.copy()
    barcode_mask = Image.eval(barcode_mask.convert('1'), lambda x: not x)

    log.debug("debug, pasting barcode to canvas...")

    base_image.paste(barcode_img, barcode_loc, barcode_mask)

    log.debug("draw_ticket, saving ticket...")

    base_image.save(ticket, 'PNG')

    log.debug("draw_ticket, end draw ticket...")

    return ticket

def draw_text_on_ticket(text_canvas, text, metrics):
    if metrics.font_color:
        color = tuple([int(num) for num in metrics.font_color.split(',')])
    else:
        color = (0, 0, 0)

    if not isinstance(text, list):
        text_canvas.write_text_box(metrics.location(), text, box_width=metrics.width, font_name=metrics.font_name,
                font_size=metrics.font_size, color=color, place=metrics.text_align)
    else:
        loc = metrics.location()

        for line in text:
            loc = (loc[0], loc[1] + metrics.font_size + 4)
            text_canvas.write_text_box(loc, line, box_width=metrics.width, font_name=metrics.font_name,
                    font_size=metrics.font_size, color=color, place=metrics.text_align)

def save_ticket_image(image_data, parent, seat=None, tx_code=None):
    ticket_image_blob = TicketImageBlob()
    ticket_image_blob.key = Key(TicketImageBlob, str(uuid_gen()), parent=parent)
    ticket_image_blob.image = image_data
    ticket_image_blob.seat = seat
    ticket_image_blob.transaction_code = tx_code
    ticket_image_blob.put()

class TemplateTextBoxMetrics():
    def __init__(self, font_name, font_size, font_color, x, y, width, height, text_align):
        self.font_name = font_name
        self.font_size = font_size
        self.font_color = font_color
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.text_align = text_align

    def location(self):
        return (self.x, self.y)

def template_yaml_to_metrics(yaml_text):
    text_stream = StringIO(yaml_text)
    metrics_yaml = yaml.load(text_stream)
    metrics = {}
    defaults = {}

    if 'defaults' in metrics_yaml:
        defaults = metrics_yaml['defaults']

    with_default = lambda k,d,v: v.get(k, d.get(k, None))

    for k,v in metrics_yaml['parts'].items():
        metrics[k] = TemplateTextBoxMetrics(font_name=with_default('font_name', defaults, v), font_size=with_default('font_size', defaults, v),
                font_color=with_default('font_color', defaults, v), x=with_default('x', defaults, v), y=with_default('y', defaults, v),
                width=with_default('width', defaults, v), height=with_default('height', defaults, v), text_align=with_default('text_align', defaults, v))

    barcode_loc = metrics_yaml['barcode']
    metrics['barcode'] = (barcode_loc['x'], barcode_loc['y'])

    qrcode_loc = metrics_yaml['qrcode']
    metrics['qrcode'] = (qrcode_loc['x'], qrcode_loc['y'])

    return metrics

def center_barcode_on_ticket(template, template_img, barcode):
    default_barcode_loc = template['barcode']
    base_canvas = StringIO(template_img)
    barcode_f = StringIO(barcode)
    base_canvas_img = Image.open(base_canvas)
    barcode_img = Image.open(barcode_f)

    base_canvas_size = base_canvas_img.size # get the width and height of base canvas
    barcode_size = barcode_img.size # get the width and height of barcode

    x_position = (base_canvas_size[0] - barcode_size[0]) / 2 # center the barcode on base canvas
    barcode_loc = (x_position, default_barcode_loc[1])

    return barcode_loc

def center_qrcode_on_ticket(template, template_img, barcode):
    default_barcode_loc = template['qrcode']
    base_canvas = StringIO(template_img)
    barcode_f = StringIO(barcode)
    base_canvas_img = Image.open(base_canvas)
    barcode_img = Image.open(barcode_f)

    base_canvas_size = base_canvas_img.size # get the width and height of base canvas
    barcode_size = barcode_img.size # get the width and height of barcode

    x_position = (base_canvas_size[0] - barcode_size[0]) / 2 # center the qrcode on base canvas
    barcode_loc = (x_position, default_barcode_loc[1])

    return barcode_loc

@non_transactional
def has_movie_ticket_template(movie, org_id):
    org = Key(TheaterOrganization, org_id).get()
    ticket_template_image = TicketTemplateImageBlob.query(TicketTemplateImageBlob.template_type=='movie', ancestor=movie.key).get()

    if ticket_template_image:
        if ticket_template_image.image and movie.is_active_ticket_template and org.movie_ticket_template:
            return True

    return False

@non_transactional
def has_theaterorg_ticket_template(org_id):
    org_key = Key(TheaterOrganization, org_id)
    org = org_key.get()
    ticket_template_image = TicketTemplateImageBlob.query(TicketTemplateImageBlob.template_type=='theaterorg', ancestor=org_key).get()

    if ticket_template_image:
        if ticket_template_image.image and org.theaterorg_ticket_template:
            return True

    return False

@non_transactional
def new_line_movie_title(template, movie_title, is_old_ticket_template):
    if len(movie_title) > 30:
        height = template['movie_title'].height

        if is_old_ticket_template:
            barcode_loc = (template['barcode'][0], template['barcode'][1] + height)
            template['barcode'] = barcode_loc
            template['qrcode'] = barcode_loc
            template['movie_title'].height = template['movie_title'].height * 2
            template['theater_name'].y = template['theater_name'].y + height
            template['show_datetime'].y = template['show_datetime'].y + height
            template['price'].y = template['price'].y + height
            template['code'].y = template['code'].y + height
            template['seats_imploded'].y = template['seats_imploded'].y + height
            template['remarks'].y = template['remarks'].y + height
        else:
            template['movie_title'].height = template['movie_title'].height * 2
            template['movie_title'].y = template['movie_title'].y - 10
            template['theater_show_datetime'].y = template['theater_show_datetime'].y-10 + height
            template['price'].y = template['price'].y-10 + height
            template['total_price'].y = template['total_price'].y-10 + height

    return template

@non_transactional
def new_line_seats_imploded(template, seats_imploded, is_old_ticket_template):
    if len(seats_imploded) > 5:
        seats_imploded = [', '.join(seats_imploded[:5]).strip(), ', '.join(seats_imploded[5:]).strip()]
        seats_imploded_height = template['seats_imploded'].height

        if is_old_ticket_template:
            barcode_loc = (template['barcode'][0], template['barcode'][1] + int(seats_imploded_height))
            template['barcode'] = barcode_loc
            template['qrcode'] = barcode_loc
            template['remarks'].y = template['remarks'].y + int(seats_imploded_height)
            template['seats_imploded'].y = template['seats_imploded'].y - int(seats_imploded_height)
        else:
            template['seats_imploded'].y = template['seats_imploded'].y - (int(seats_imploded_height) + 2) * 2
            template['seats_imploded_label'].y = template['seats_imploded_label'].y - int(seats_imploded_height)
    else:
        seats_imploded = ', '.join(seats_imploded)

    return template, seats_imploded


# ImageText
# From: https://gist.github.com/turicas/1455973
# Copyright 2011 Álvaro Justen [alvarojusten at gmail dot com]
# License: GPL <http://www.gnu.org/copyleft/gpl.html>

class ImageText():
    def __init__(self, filename_or_size, mode='RGBA', background=(0, 0, 0, 0),
                 encoding='utf8', supersample=False):

        self.supersample = supersample
        if isinstance(filename_or_size, (list, tuple)):
            if not supersample:
                self.size = filename_or_size
            else:
                self.size = (filename_or_size[0] * 2, filename_or_size[1] * 2)
            self.image = Image.new(mode, self.size, color=background)
            self.filename = None
        else:
            self.filename = filename_or_size
            if not supersample:
                self.image = Image.open(self.filename)
                self.size = self.image.size
            else:
                self.base_image = Image.open(self.filename)
                self.size = (self.base_image.size[0] * 2, self.base_image.size[1] * 2)
                self.image = Image.new(mode, self.size, color=(255, 255, 255, 0)) # Used default mode:'RGBA'
                # self.image = Image.new(self.base_image.mode, self.size, color=(255, 255, 255, 0))

        self.draw = ImageDraw.Draw(self.image)
        self.encoding = encoding

    def save(self, filename=None):
        if not self.supersample:
            self.image.save(filename or self.filename, 'PNG')
        else:
            text_layer = self.image.resize(self.base_image.size, Image.ANTIALIAS)
            self.base_image.paste(text_layer, (0,0), text_layer)
            self.base_image.save(filename or self.filename, 'PNG')

    def merge_with_base(self):
        if not self.supersample:
            return self.image
        else:
            text_layer = self.image.resize(self.base_image.size, Image.ANTIALIAS)
            self.base_image.paste(text_layer, (0,0), text_layer)
            return self.base_image

    def get_font_size(self, text, font, max_width=None, max_height=None):
        if max_width is None and max_height is None:
            raise ValueError('You need to pass max_width or max_height')
        font_size = 1
        text_size = self.get_text_size(font, font_size, text)
        if (max_width is not None and text_size[0] > max_width) or \
           (max_height is not None and text_size[1] > max_height):
            raise ValueError("Text can't be filled in only (%dpx, %dpx)" % \
                    text_size)
        while True:
            if (max_width is not None and text_size[0] >= max_width) or \
               (max_height is not None and text_size[1] >= max_height):
                return font_size - 1
            font_size += 1
            text_size = self.get_text_size(font, font_size, text)

    def write_text(self, (x, y), text, font_filename, font_size=11,
                   color=(0, 0, 0), max_width=None, max_height=None):

        if isinstance(text, str):
            text = text.decode(self.encoding)
        if font_size == 'fill' and \
           (max_width is not None or max_height is not None):
            font_size = self.get_font_size(text, font_filename, max_width,
                                           max_height)
        text_size = self.get_text_size(font_filename, font_size, text)
        font = ImageFont.truetype(font_filename, font_size)
        if x == 'center':
            x = (self.size[0] - text_size[0]) / 2
        if y == 'center':
            y = (self.size[1] - text_size[1]) / 2
        self.draw.text((x, y), text, font=font, fill=color)
        return text_size

    def get_text_size(self, font_filename, font_size, text):
        font = ImageFont.truetype(font_filename, font_size)
        return font.getsize(text)

    def write_text_box(self, (x, y), text, box_width, font_name,
                       font_size=11, color=(0, 0, 0), place='left',
                       justify_last_line=False):

        font_filename = "res/fonts/%s.ttf" % font_name

        if self.supersample:
            x, y = (x * 2, y * 2)
            box_width = box_width * 2
            font_size = font_size * 2

        lines = []
        line = []
        words = text.split(' ')
        for word in words:
            new_line = ' '.join(line + [word])
            size = self.get_text_size(font_filename, font_size, new_line)
            text_height = size[1]
            if size[0] <= box_width:
                line.append(word)
            else:
                lines.append(line)
                line = [word]
        if line:
            lines.append(line)
        lines = [' '.join(line) for line in lines if line]
        height = y
        for index, line in enumerate(lines):
            height += text_height
            if place == 'left':
                self.write_text((x, height), line, font_filename, font_size,
                                color)
            elif place == 'right':
                total_size = self.get_text_size(font_filename, font_size, line)
                x_left = x + box_width - total_size[0]
                self.write_text((x_left, height), line, font_filename,
                                font_size, color)
            elif place == 'center':
                total_size = self.get_text_size(font_filename, font_size, line)
                x_left = int(x + ((box_width - total_size[0]) / 2))
                self.write_text((x_left, height), line, font_filename,
                                font_size, color)
            elif place == 'justify':
                words = line.split()
                if (index == len(lines) - 1 and not justify_last_line) or \
                   len(words) == 1:
                    self.write_text((x, height), line, font_filename, font_size,
                                    color)
                    continue
                line_without_spaces = ''.join(words)
                total_size = self.get_text_size(font_filename, font_size,
                                                line_without_spaces)
                space_width = (box_width - total_size[0]) / (len(words) - 1.0)
                start_x = x
                for word in words[:-1]:
                    self.write_text((start_x, height), word, font_filename,
                                    font_size, color)
                    word_size = self.get_text_size(font_filename, font_size,
                                                    word)
                    start_x += word_size[0] + space_width
                last_word_size = self.get_text_size(font_filename, font_size,
                                                    words[-1])
                last_word_x = x + box_width - last_word_size[0]
                self.write_text((last_word_x, height), words[-1], font_filename,
                                font_size, color)
        return (box_width, height - y)    

