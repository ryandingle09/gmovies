from __future__ import absolute_import

import base64
import datetime
import logging
import hashlib
import os
import re
import requests
import suds
import time
import yaml

import Image
import ImageFont

from StringIO import StringIO

from barcode import get_barcode_class
from barcode.writer import mm2px, FONT, ImageWriter
from lxml import etree
from qrcode import constants, QRCode

from flask import url_for

from google.appengine.api import memcache, taskqueue
from google.appengine.ext import deferred
from google.appengine.ext.ndb import get_multi, non_transactional, transactional, Key

from .ticket import ImageText
from .util import call_once, translate_message_via_error_code, DEFAULT_ERROR_MESSAGE

from gmovies import orgs
from gmovies.admin.email import send_ticket_details, send_blocked_screening_ticket_details
from gmovies.exceptions.api import AlreadyCancelledException, TransactionConflictException, NotFoundException
from gmovies.models import (Device, Listener, PaymentOptionSettings, Promo, PromoReservationTransaction, PromoTicketData,
        TheaterOrganization, TicketTemplateImageBlob)
from gmovies.settings import (BLOCKED_MERCHANT_ID, BLOCKED_MERCHANT_NAME, BLOCKED_MERCHANT_KEY, BLOCKED_PAYNAMICS_FORM_TARGET,
        BLOCKED_PAYNAMICS_REQUERY_URL, BLOCKED_PAYNAMICS_TARGETNAMESPACE, BLOCKED_NOTIFICATION_URL, BLOCKED_RESPONSE_URL,
        BLOCKED_CANCEL_URL, REAP_TIMEOUT, PROMOCODE_SERVER)
from gmovies.util import make_enum
from gmovies.util.id_encoder import decoded_theater_id


log = logging.getLogger(__name__)

MAX_QUEUES = 2
TX_TASK_MAX_RETRY = 10
WS_PAYMENT_ENGINE = 'payment::engine'
TICKET_DATE_FORMAT = '%m/%d/%Y'
TICKET_TIME_FORMAT = '%I:%M %p'
TICKET_BARCODE_TYPE = 'code39'
TICKET_BARCODE_GENERATOR = get_barcode_class(TICKET_BARCODE_TYPE)

PROMOCODE_ENDPOINT = PROMOCODE_SERVER + '/admin/0/promos/redeem'
PROMOCODE_RESET_ENDPOINT = PROMOCODE_SERVER + '/admin/0/promos/reset'
PROMOCODE_VERIFY_ENDPOINT = PROMOCODE_SERVER + '/admin/0/promos/verify'

BLOCKED_CHANNEL_NAME ='blocked-paynamics-payment-complete'
BLOCKED_LISTENER_ID ='$1$blocked_paynamics_payment_completion_listener'
BLOCKED_TIMEOUT = 1320 # 22 minutes
BLOCKED_TIMEOUT_REQUERY = 1200 # 20 minutes

state = make_enum('state', TX_START=0, TX_PREPARE=1, TX_STARTED=2,
        TX_RESERVATION_HOLD=3, TX_RESERVATION_OK=4, TX_PAYMENT_HOLD=5,
        TX_GENERATE_TICKET=6, TX_PAYNAMICS_PAYMENT_HOLD=7, TX_FINALIZE=8,
        TX_FINALIZE_HOLD=9, TX_RESERVATION_ERROR=11, TX_PAYMENT_ERROR=12,
        TX_PREPARE_ERROR=13, TX_DONE=100, TX_CANCELLED=101, TX_STATE_ERROR=-1)

state_after_update = {
        state.TX_PAYMENT_ERROR: state.TX_RESERVATION_OK,
        state.TX_RESERVATION_ERROR: state.TX_STARTED,
        state.TX_PREPARE_ERROR: state.TX_START
}

PAYMENTTYPE_EQUIVALENTTO_PAYMENTOPTIONSIDENTIFIER = {
        PromoReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT: 'blocked_screening_paynamics',
        PromoReservationTransaction.allowed_payment_types.PROMO_CODE: 'blocked_screening_claim_code',
}


###############
#
# Reservation related methods/functions.
#
###############

def cancel_reservation(tx):
    pass

def prepare_reservation(tx):
    log.debug("PROMOS or BLOCKEDSCREENINGS, prepare_reservation, preparing transaction...")

    try:
        promo_keys, seats = zip(*[(r.promo, r.seat_id) for r in tx.reservations])
        seats = list(set(seats))
        promos = non_transactional_get_multi(promo_keys)
        payment_options_identifiers = _get_payment_options_identifiers(promos[0], tx.platform)
        is_payment_option_enabled = payment_options_handler(tx.payment_type, 'paynamics', payment_options_identifiers)

        tx.workspace['promos'] = promos
        tx.workspace['promos.key'] = promo_keys
        tx.workspace['promos.promo_id'] = [str(promo.key.id()) for promo in promos]

        if not promos:
            log.warn("ERROR!, PROMOS or BLOCKEDSCREENINGS, prepare_reservation, not promos...")

            return 'error', DEFAULT_ERROR_MESSAGE

        if not promos[0].is_active:
            log.warn("ERROR!, PROMOS or BLOCKEDSCREENINGS, prepare_reservation, not is_active...")

            return 'error', DEFAULT_ERROR_MESSAGE

        if tx.platform not in promos[0].platform:
            log.warn("ERROR!, PROMOS or BLOCKEDSCREENINGS, prepare_reservation, platform not found...")

            return 'error', DEFAULT_ERROR_MESSAGE

        if not is_payment_option_enabled:
            log.warn("ERROR!, PROMOS or BLOCKEDSCREENINGS, prepare_reservation, payment_type not in payment_options...")

            return 'error', DEFAULT_ERROR_MESSAGE

        movie = _do_get_movie(promos[0].movie) if promos[0].movie else None

        tx.workspace['promos.name'] = promos[0].name
        tx.workspace['promos.available_date'] = promos[0].available_date
        tx.workspace['promos.is_block_screening'] = promos[0].is_block_screening
        tx.workspace['promos.activate_requery'] = promos[0].activate_requery
        tx.workspace['promos.theater_id'] = promos[0].theater_id if promos[0].theater_id else ''
        tx.workspace['promos.theater'] = promos[0].theater if promos[0].theater else ''
        tx.workspace['promos.theater_code'] = promos[0].theater_code if promos[0].theater_code else ''
        tx.workspace['promos.cinema'] = promos[0].cinema if promos[0].cinema else ''
        tx.workspace['promos.start_time'] = promos[0].start_time.strftime(TICKET_TIME_FORMAT) if promos[0].start_time else ''
        tx.workspace['promos.end_time'] = promos[0].end_time.strftime(TICKET_TIME_FORMAT) if promos[0].end_time else ''
        tx.workspace['promos.price_per_seat'] = str(promos[0].price) if promos[0].price else ''
        tx.workspace['promos.prices'] = [promo.price for promo in promos]
        tx.workspace['promos.payment_options_identifiers'] = payment_options_identifiers
        tx.workspace['promos.payment_options_settings'] = _get_payment_options_settings(tx.payment_type)
        tx.workspace['seats'] = seats
        tx.workspace['seats::locked'] = []
        tx.workspace['seats::imploded'] = ','.join(seats)
        tx.workspace['movies.movie_id'] = movie.key.id() if movie else ''
        tx.workspace['movies.canonical_title'] = movie.canonical_title if movie else ''
        tx.workspace['movies.advisory_rating'] = movie.advisory_rating if movie and movie.advisory_rating else ''
        tx.workspace['movies.runtime_mins'] = movie.runtime_mins if movie and movie.runtime_mins else 0

        tx.put()

        return 'success', None
    except Exception, e:
        log.warn("ERROR!, PROMOS or BLOCKEDSCREENINGS, prepare_reservation...")
        log.error(e)

    return 'error', DEFAULT_ERROR_MESSAGE

def do_reservation(tx):
    log.debug("PROMOS or BLOCKEDSCREENINGS, do_reservation, start transaction...")

    try:
        r, msg = tx._do_lock_seats()
        tx.reservation_reference = _do_get_reservation_reference(tx)
        tx.workspace['RSVP:fee'] = '0.00'

        if tx.payment_type in [PromoReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT]:
            tx.workspace['RSVP:fee'] = _do_get_convenience_fee(tx, 'credit_card')

        tx.workspace['RSVP:claimcode'] = tx.reservation_reference
        tx.workspace['RSVP:claimdate'] = tx.workspace['promos.available_date']
        tx.workspace['RSVP:originaltotalamount'] = str(tx.total_amount(tx.workspace['RSVP:fee'], True))
        tx.workspace['RSVP:originalseatprice'] = str(tx.total_seat_price(tx.workspace['RSVP:fee'], True))
        tx.workspace['RSVP:totalamount'] = str(tx.total_amount(tx.workspace['RSVP:fee'], True))
        tx.workspace['RSVP:totalseatprice'] = str(tx.total_seat_price(tx.workspace['RSVP:fee'], True))
        tx.workspace['RSVP:totaldiscount'] = '0.00'
        tx.workspace['RSVP:totalseatdiscount'] = '0.00'
        tx.workspace['RSVP:isdiscounted'] = False
        tx.workspace['RSVP:seats'] = [res.seat_id for res in tx.reservations]

        tx.bind_cancellation_callback(cancel_reservation)
        tx.put()

        return r, msg
    except Exception, e:
        log.warn("ERROR!, PROMOS or BLOCKEDSCREENINGS, do_reservation...")
        log.error(e)

    return 'error', DEFAULT_ERROR_MESSAGE

@non_transactional
def non_transactional_get_multi(keys):
    return get_multi(keys)

@non_transactional
def _do_get_movie(movie_key):
    return movie_key.get()

@non_transactional
def _do_get_reservation_reference(tx):
    today = datetime.datetime.today()
    timestamp = str(int(time.mktime(today.timetuple())))
    reference_number = PromoReservationTransaction.generate_reservation_reference(timestamp)

    if 'promos.theater_code' in tx.workspace and tx.workspace['promos.theater_code']:
        reference_number = tx.workspace['promos.theater_code'][0] + reference_number

    return reference_number

@non_transactional
def _do_get_convenience_fee(tx, payment_options_identifier):
    convenience_fee = '0.00'
    promo_key = Key(Promo, tx.workspace['promos.promo_id'][0])
    promo = promo_key.get()

    if payment_options_identifier == 'credit_card':
        log.debug("_do_get_convenience_fee, PROMOS or BLOCKEDSCREENINGS, default convenience fee credit_card...")

        convenience_fee = '20.00'

    if promo and payment_options_identifier in promo.convenience_fees:
        log.debug("_do_get_convenience_fee, PROMOS or BLOCKEDSCREENINGS, get convenience fee...")
        log.info("payment_options_identifier: %s..." % payment_options_identifier)
        log.info("convenience_fee: %s..." % promo.convenience_fees[payment_options_identifier])

        convenience_fee = promo.convenience_fees[payment_options_identifier]

    return convenience_fee

@non_transactional
def payment_options_handler(payment_type, payment_gateway, payment_options_identifiers):
    is_enabled = False
    payment_options_identifier = None

    if payment_type in PAYMENTTYPE_EQUIVALENTTO_PAYMENTOPTIONSIDENTIFIER:
        payment_options_identifier = PAYMENTTYPE_EQUIVALENTTO_PAYMENTOPTIONSIDENTIFIER[payment_type]

    if payment_options_identifier in payment_options_identifiers:
        is_enabled = True

    return is_enabled

@non_transactional
def _get_payment_options_identifiers(promo, platform):
    payment_options_identifiers = []

    if platform == 'ios':
        payment_options_identifiers = promo.payment_options_ios
    elif platform == 'android':
        payment_options_identifiers = promo.payment_options_android
    elif platform == 'website':
        payment_options_identifiers = promo.payment_options_website

    return payment_options_identifiers

@non_transactional
def _get_payment_options_settings(payment_type, payment_gateway=None):
    payment_options_identifier = None
    payment_options_settings = None

    if payment_type in PAYMENTTYPE_EQUIVALENTTO_PAYMENTOPTIONSIDENTIFIER:
        payment_options_identifier = PAYMENTTYPE_EQUIVALENTTO_PAYMENTOPTIONSIDENTIFIER[payment_type]

    if payment_options_identifier:
        payment_options_settings_key = Key(PaymentOptionSettings, payment_options_identifier)
        payment_options_settings = payment_options_settings_key.get()

    return payment_options_settings


###############
#
# Payment related methods/functions.
#
###############

def cancel_payment(tx):
    if tx.payment_type == PromoReservationTransaction.allowed_payment_types.PROMO_CODE:
        engine = tx.workspace[WS_PAYMENT_ENGINE]
        tx.bind_cancellation_callback(engine._cancellation_callback)

def prepare_payment(tx):
    try:
        if tx.payment_type == PromoReservationTransaction.allowed_payment_types.PROMO_CODE:
            engine = DummyPromoCodePayment(tx.payment_info)
        elif tx.payment_type == PromoReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT:
            engine = BlockedPaynamicsPayment(tx.payment_info)
        else:
            log.warn("ERROR!, PROMOS or BLOCKEDSCREENINGS, prepare_payment, payment_type not supported...")

            return 'error', DEFAULT_ERROR_MESSAGE

        tx.workspace[WS_PAYMENT_ENGINE] = engine
        status, message = engine.do_prepare(tx)
        tx.put()

        return status, message
    except KeyError, e:
        log.warn("ERROR!, PROMOS or BLOCKEDSCREENINGS, prepare_payment, missing key...")
        log.error(e)
    except Exception, e:
        log.warn("ERROR!, PROMOS or BLOCKEDSCREENINGS, prepare_payment...")
        log.error(e)

    return 'error', DEFAULT_ERROR_MESSAGE

def do_payment(tx):
    engine = tx.workspace[WS_PAYMENT_ENGINE]
    retval = engine.do_payment_tx(tx)
    tx.bind_cancellation_callback(cancel_payment)
    tx.put()

    return retval

class DummyPromoCodePayment():
    def __init__(self, payment_info):
        self.err_message = DEFAULT_ERROR_MESSAGE
        self.promo_code = payment_info['promo_code']
        self.promo_id = ''
        self.seat_count = ''

    def _do_promocode_check(self):
        log.debug("DummyPromoCodePayment, PROMOS or BLOCKEDSCREENINGS, _do_promocode_check, checking promo_code from %s..." % PROMOCODE_ENDPOINT)

        r = requests.get(PROMOCODE_ENDPOINT, params={'promo_code': self.promo_code, 'promo_id': self.promo_id, 'seat_count': self.seat_count})

        log.debug("DummyPromoCodePayment, PROMOS or BLOCKEDSCREENINGS, _do_promocode_check, status_code: {0}, response: {1}...".format(r.status_code, r.text))

        return r.status_code, r.headers['content-type'], r.text

    def _do_promocode_verify(self):
        log.debug("DummyPromoCodePayment, PROMOS or BLOCKEDSCREENINGS, _do_promocode_verify, verifying promo_code from %s..." % PROMOCODE_VERIFY_ENDPOINT)

        r = requests.get(PROMOCODE_VERIFY_ENDPOINT, params={'promo_code': self.promo_code, 'promo_id': self.promo_id, 'seat_count': self.seat_count})

        log.debug("DummyPromoCodePayment, PROMOS or BLOCKEDSCREENINGS, _do_promocode_verify, status_code: {0}, response: {1}...".format(r.status_code, r.text))

        return r.status_code, r.headers['content-type'], r.text

    def _do_promocode_reset(self):
        log.debug("DummyPromoCodePayment, PROMOS or BLOCKEDSCREENINGS, _do_promocode_reset, reseting promo_code from %s..." % PROMOCODE_RESET_ENDPOINT)

        r = requests.get(PROMOCODE_RESET_ENDPOINT, params={'promo_code': self.promo_code})

        log.debug("DummyPromoCodePayment, PROMOS or BLOCKEDSCREENINGS, _do_promocode_reset, status_code: %s..." % r.status_code)

        return r.status_code

    def _cancellation_callback(self, tx):
        call_once(self._do_promocode_reset, tx)

    def _update_callback(self, tx, same_reservation, same_payment):
        if not same_payment:
            call_once(self._do_promocode_reset, tx)

    def do_prepare(self, tx):
        self.seat_count = tx.reservation_count()
        self.promo_id = tx.workspace['promos.promo_id'][0]
        r_status_code, r_content_type, r_text = call_once(self._do_promocode_verify, tx)

        if r_status_code != 200:
            if r_content_type == 'text/plain':
                namespace = 'claim-code-gae'
                response_code = 'INVALIDCODES' # default response code to translate the error messages.
                response_message = r_text

                if r_text == 'Error in parameters.':
                    response_code = 'ERRORPARAMS'

                return 'error', translate_message_via_error_code(namespace, response_code, response_message)

            return 'error', self.err_message

        return 'success', None

    def do_payment_tx(self, tx):
        self.seat_count = tx.reservation_count()
        self.promo_id = tx.workspace['promos.promo_id'][0]
        r_status_code, r_content_type, r_text = call_once(self._do_promocode_check, tx)

        if r_status_code != 200:
            if r_content_type == 'text/plain':
                namespace = 'claim-code-gae'
                response_code = 'INVALIDCODES' # default response code to translate the error messages.
                response_message = r_text

                if r_text == 'Error in parameters.':
                    response_code = 'ERRORPARAMS'

                return 'error', translate_message_via_error_code(namespace, response_code, response_message)

            return 'error', self.err_message

        if r_content_type == 'text/plain':
            tx.workspace['msg'] = r_text

        tx._do_reserve_seats() # reserve/finalize the locked seats.

        return 'success', r_text

class BlockedPaynamicsPayment():
    def __init__(self, payment_info):
        self.err_message = DEFAULT_ERROR_MESSAGE
        self.form_parameters = {}
        self.form_method = 'POST'
        self.form_target = BLOCKED_PAYNAMICS_FORM_TARGET

        # Default form parameters
        self.form_parameters['mid'] = BLOCKED_MERCHANT_ID
        self.form_parameters['merchantname'] = BLOCKED_MERCHANT_NAME
        self.form_parameters['merchantkey'] = BLOCKED_MERCHANT_KEY
        self.form_parameters['notification_url'] = BLOCKED_NOTIFICATION_URL
        self.form_parameters['response_url'] = BLOCKED_RESPONSE_URL
        self.form_parameters['cancel_url'] = BLOCKED_CANCEL_URL
        self.form_parameters['ip_address'] = '173.194.72.141'
        self.form_parameters['mtac_url'] = ''
        self.form_parameters['currency'] = 'PHP'
        self.form_parameters['pmethod'] = 'CC'
        self.form_parameters['trxtype'] = 'sale'
        self.form_parameters['mlogo_url'] = ''
        self.form_parameters['secure3d'] = 'try3d'
        self.form_parameters['descriptor_note'] = 'GMovies Blocked Screening'

        # User input form parameters
        self.form_parameters['email'] = payment_info['email'].strip()
        self.form_parameters['fname'] = payment_info['fname'].strip()
        self.form_parameters['lname'] = payment_info['lname'].strip()
        self.form_parameters['mname'] = payment_info['mname']
        self.form_parameters['address1'] = payment_info['address1'].strip()
        self.form_parameters['address2'] = payment_info['address2']
        self.form_parameters['city'] = payment_info['city']
        self.form_parameters['state'] = payment_info['state']
        self.form_parameters['country'] = payment_info['country']
        self.form_parameters['zip'] = payment_info['zip']
        self.form_parameters['phone'] = payment_info['phone']
        self.form_parameters['mobile'] = payment_info['mobile'].strip()

    def do_prepare(self, tx):
        return 'success', None

    def do_payment_tx(self, tx):
        if 'signed_xml_response' in tx.workspace:
            if not tx.workspace['signed_xml_response']:
                log.warn("ERROR!, BlockedPaynamicsPayment, PROMOS or BLOCKEDSCREENINGS, invalid response signature...")
                tx._do_free_seats() # free the locked seats.

                return 'error', 'Invalid response signature.'

        if 'response_code' in tx.workspace and 'response_message' in tx.workspace:
            if tx.workspace["response_code"] != 'GR001' and tx.workspace["response_code"] != 'GR002':
                log.warn("ERROR!, BlockedPaynamicsPayment, PROMOS or BLOCKEDSCREENINGS, response_code: %s" % tx.workspace["response_code"])
                log.warn("ERROR!, BlockedPaynamicsPayment, PROMOS or BLOCKEDSCREENINGS, response_message: %s" % tx.workspace["response_message"])

                tx._do_free_seats() # free the locked seats.

                return 'error', tx.workspace["response_message"]

        tx._do_reserve_seats() # reserve/finalize the locked seats.

        return 'success', None

    def generate_orders_payload(self, tx):
        orders_payload = {}
        orders_list = []
        total_amount = str(tx.total_amount(tx.workspace['RSVP:fee'], True))
        seat_price = str(tx.total_seat_price(tx.workspace['RSVP:fee'], True))

        try:
            promo_name = tx.workspace['promos.name']
            seats = tx.workspace['seats']

            for seat in seats:
                orders_dict = {}
                orders_dict['itemname'] = 'GMovies Blocked Screening (%s - %s)' % (promo_name, seat)
                orders_dict['quantity'] = '1'
                orders_dict['amount'] = seat_price

                orders_list.append(orders_dict)
        except Exception, e:
            log.warn("ERROR!, BlockedPaynamicsPayment, PROMOS or BLOCKEDSCREENINGS, failed to generate orders payload...")
            log.error(e)

            item_name = 'GMovies Blocked Screening E-Ticket'
            amount = total_amount
            quantity = '1'

            orders_list.append({'itemname': item_name, 'quantity': quantity, 'amount': amount})

        orders_payload['items'] = orders_list

        return orders_payload

    def generate_xml_string(self):
        root = etree.Element('Request')

        for key, value in self.form_parameters.iteritems():
            xml_tag = etree.Element(key)

            if key == 'orders':
                for orders_key, orders_value in value.iteritems():
                    orders_xml_tag = etree.Element(orders_key)

                    for items in  orders_value:
                        root_items = etree.Element('Items')

                        for items_key, items_value in items.iteritems():
                            items_xml_tag = etree.Element(items_key)
                            items_xml_tag.text = items_value

                            root_items.append(items_xml_tag)

                        orders_xml_tag.append(root_items)

                xml_tag.append(orders_xml_tag)
            elif key == 'merchantname':
                continue
            elif key == 'merchantkey':
                continue
            else:
                xml_tag.text = value

            root.append(xml_tag)

        xml_str = etree.tostring(root)

        return xml_str

    def generate_signature(self):
        mid = self.form_parameters['mid']
        request_id = self.form_parameters['request_id']
        ip_address = self.form_parameters['ip_address']
        notification_url = self.form_parameters['notification_url']
        response_url = self.form_parameters['response_url']
        fname = self.form_parameters['fname']
        lname = self.form_parameters['lname']
        mname = self.form_parameters['mname']
        address1 = self.form_parameters['address1']
        address2 = self.form_parameters['address2']
        city = self.form_parameters['city']
        state = self.form_parameters['state']
        country = self.form_parameters['country']
        zip_code = self.form_parameters['zip']
        email = self.form_parameters['email']
        phone = self.form_parameters['phone']
        client_ip = self.form_parameters['client_ip']
        amount = self.form_parameters['amount']
        currency = self.form_parameters['currency']
        secure3d = self.form_parameters['secure3d']
        merchant_key = self.form_parameters['merchantkey']
        secure3d_merchantkey = secure3d + merchant_key

        seed = [mid, request_id, ip_address, notification_url, response_url,
                fname, lname, mname, address1, address2, city, state, country,
                zip_code, email, phone, client_ip, amount, currency,
                secure3d_merchantkey]

        seed_str = ''.join(seed)
        signature = hashlib.sha512(seed_str.encode('utf-8')).hexdigest()

        return signature

    def generate_paymentrequest(self):
        xml_str = self.generate_xml_string()
        encoded_xml_str = xml_str.encode('base64')

        return encoded_xml_str


###############
#
# Action related methods/functions.
#
###############

def _incr_queue_id(client):
    while True:
        queue_id = client.gets('tx_promo_queue_id')

        if queue_id == None:
            client.set('tx_promo_queue_id', 1)

            return 1

        new_queue_id = (queue_id % MAX_QUEUES) + 1 # cycle from 1 to 2.

        if client.cas('tx_promo_queue_id', new_queue_id):
            return new_queue_id

@transactional
def start(device_id, tx_info):
    tx = PromoReservationTransaction.create_transaction(device_id, tx_info)
    tx_id = tx.key.id()
    client = memcache.Client()
    state_key = "txpromo::%s::s" % tx_id
    client.set(state_key, state.TX_START)
    tx.state = state.TX_START

    queue_id = _incr_queue_id(client)
    tx.workspace['queue_id'] = queue_id
    tx.put()

    return tx

@transactional
def update(device_id, tx_id, tx_info={}):
    tx = query_info(device_id, tx_id)

    if not tx:
        log.warn("ERROR!, action, update, PROMOS or BLOCKEDSCREENINGS, transaction not found...")

        raise NotFoundException(details={'tx_id': tx_id})

    if tx.state == state.TX_CANCELLED:
        log.warn("ERROR!, action, update, PROMOS or BLOCKEDSCREENINGS, transaction already cancelled...")

        raise AlreadyCancelledException()

    if tx.state not in state_after_update:
        log.warn("ERROR!, action, update, PROMOS or BLOCKEDSCREENINGS, transaction not allowed for update...")

        raise TransactionConflictException(state_after_update.keys())

    cancel_reaping(tx)

    if tx_info:
        payment_info = dict(tx_info['payment'])
        payment_info.pop('type', None)
        same_payment_info = (payment_info == tx.payment_info)

        if 'reservations' in tx_info and tx.state == state.TX_PAYMENT_ERROR:
            log.warn("ERROR!, action, update, PROMOS or BLOCKEDSCREENINGS, TX_PAYMENT_ERROR...")

            raise TransactionConflictException([to_state_str_promo(state.TX_RESERVATION_ERROR)])

        old_reservations = tx.reservations
        tx.copy_api_entity(tx_info)
        same_reservation_info = (old_reservations == tx.reservations)

        log.debug("action, update, PROMOS or BLOCKEDSCREENINGS, same payment info? %s...", same_payment_info)
        log.debug("action, update, PROMOS or BLOCKEDSCREENINGS, same reservation info? %s...", same_reservation_info)

        if not same_payment_info or not same_reservation_info:
            log.debug("action, update, PROMOS or BLOCKEDSCREENINGS, triggering update callbacks...")

            tx.trigger_update(same_reservation=same_reservation_info, same_payment=same_payment_info)

        # HACK: Sneak in prepare step (even though we're technically not in TX_PREPARE)
        #
        # We need to do this because we depend particularly on the
        # state of the workspace being set in the prepare_* methods,
        # which in turn depends on the passed in transaction info. If
        # the transaction info changes, we need to re-run the
        # preparation

        r, msg = 'success', None

        if tx.state in [state.TX_RESERVATION_ERROR] and not same_reservation_info:
            r, msg = prepare_reservation(tx)

        # HACK: Sneak in error message into tx.workspace['msg'] and don't reschedule
        if r != 'success':
            tx.workspace['msg'] = msg
            tx.put()

            return tx

        if tx.state in [state.TX_PAYMENT_ERROR, state.TX_RESERVATION_ERROR] and not same_payment_info:
            r, msg = prepare_payment(tx)

        # HACK: Sneak in error message into tx.workspace['msg'] and don't reschedule
        if r != 'success':
            tx.workspace['msg'] = msg
            tx.put()

            return tx

        # NB: Setting the error message during transaction update with
        # the pre-existing error state (either TX_PAYMENT_ERROR or
        # TX_RESERVATION_ERROR) is semantically wrong because the
        # error message is related to transaction preparation (for
        # instance, invalid MPASS username) and could be unexpected by
        # the client. This should be fixed in a newer public API.

    new_tx_state = state_after_update[tx.state]
    tx.workspace['msg'] = None # clear msg from workspace.
    transition_state(tx, tx.state, new_tx_state)
    reschedule(tx)

    return tx

@transactional
def cancel(device_id, tx_id):
    tx = query_info(device_id, tx_id)

    if not tx:
        log.warn("ERROR!, action, cancel, PROMOS or BLOCKEDSCREENINGS, transaction not found...")

        raise NotFoundException(details={'tx_id': tx_id})

    if tx.state == state.TX_CANCELLED:
        log.warn("ERROR!, action, cancel, PROMOS or BLOCKEDSCREENINGS, transaction already cancelled...")

        raise AlreadyCancelledException()

    allowed_cancellation_states = [state.TX_STARTED, state.TX_PAYNAMICS_PAYMENT_HOLD,
            state.TX_PREPARE_ERROR, state.TX_PAYMENT_ERROR, state.TX_RESERVATION_ERROR]

    if tx.state not in allowed_cancellation_states:
        log.warn("ERROR!, action, cancel, PROMOS or BLOCKEDSCREENINGS, transaction not allowed for cancellation...")

        raise TransactionConflictException([to_state_str_promo(s) for s in allowed_cancellation_states])

    tx._do_free_seats() # free the locked seats.
    tx.trigger_cancellation()
    tx.state = state.TX_CANCELLED
    tx.put()

    return tx


###############
#
# Query related methods/functions.
#
###############

def query_info(device_id, tx_id):
    log.debug("query, query_info, PROMOS or BLOCKEDSCREENINGS, querying for TX entity %s..." % tx_id)

    tx = PromoReservationTransaction.get_by_id(tx_id, parent=Key(Device, device_id))

    return tx

def query_state(device_id, tx_id):
    log.debug("query, query_state, PROMOS or BLOCKEDSCREENINGS, querying for TX state %s..." % tx_id)

    msg = None
    tx = PromoReservationTransaction.get_by_id(tx_id, parent=Key(Device, device_id))

    if not tx:
        return None, None

    if 'msg' in tx.workspace:
        msg = tx.workspace['msg']

    return tx.state, msg

def query_reservation_reference(reference):
    log.debug("query, query_reservation_reference, PROMOS or BLOCKEDSCREENINGS, querying for reservation_reference %s..." % reference)

    q = PromoReservationTransaction.query(PromoReservationTransaction.reservation_reference==reference)

    if q.count() == 1:
        log.debug("query, query_reservation_reference, PROMOS or BLOCKEDSCREENINGS, 1 TX entity found...")

        return q.get()
    elif q.count() > 1:
        log.warn("ERROR!, query, query_reservation_reference, PROMOS or BLOCKEDSCREENINGS, duplicate reservation_reference...")
    else:
        log.warn("ERROR!, query, query_reservation_reference, PROMOS or BLOCKEDSCREENINGS, something went wrong...")

    return None


###############
#
# Scheduler related methods/functions.
#
###############

def reschedule(tx, **kwargs):
    tx_id = tx.key.id()
    device_id = tx.key.parent().id()
    params = dict(tx_id=tx_id, device_id=device_id, tx_ver=str(tx.ver), **kwargs)
    version = os.environ['CURRENT_VERSION_ID'].split('.')
    major_version = version[0]

    log.debug("scheduler, reschedule, PROMOS or BLOCKEDSCREENINGS, rescheduling transaction with params: {}...".format(params))

    taskqueue.add(url=url_for('task.process_tx_promo'), target=major_version, transactional=True, queue_name=tx.get_queue(), params=params)

def schedule_for_reaping(tx, timeout=REAP_TIMEOUT):
    tx_id = tx.key.id()
    device_id = tx.key.parent().id()
    params = {'tx_id': tx_id, 'device_id': device_id}
    task = taskqueue.add(url=url_for('task.reap_tx_promo'), params=params, queue_name=tx.get_queue(), transactional=True, countdown=timeout)

    log.debug("scheduler, schedule_for_reaping, PROMOS or BLOCKEDSCREENINGS, scheduled for reaping, TX: {0}, DEVICE: {1}, COUNTDOWN: {2}...".format(tx_id, device_id, timeout))

    tx.workspace['reap_task'] = task
    tx.put()

    return task

def schedule_for_requery(tx, timeout=600):
    log.debug("scheduler, schedule_for_requery, PROMOS or BLOCKEDSCREENINGS, scheduling paynamics requery...")
    log.debug("scheduler, schedule_for_requery, PROMOS or BLOCKEDSCREENINGS, requery url, %s..." % BLOCKED_PAYNAMICS_REQUERY_URL)

    tx_id = tx.key.id()
    device_id = tx.key.parent().id()
    params = {'tx_id': tx_id, 'device_id': device_id}
    task = taskqueue.add(url=url_for('task.requery_tx_promo'), params=params,
            queue_name=tx.get_requery_queue(), transactional=True, countdown=timeout)
    tx.workspace['requery_task'] = task
    tx.put()

    log.debug("scheduler, schedule_for_requery, PROMOS or BLOCKEDSCREENINGS, do paynamics requery, TX: {0}, DEVICE: {1}, COUNTDOWN: {2}...".format(tx_id, device_id, timeout))

    return task

def cancel_reaping(tx):
    log.debug("scheduler, cancel_reaping, PROMOS or BLOCKEDSCREENINGS, reaping cancelled for tx, %s..." % tx.key.id())

    if 'reap_task' not in tx.workspace:
        log.warn("ERROR!, scheduler, cancel_reaping, PROMOS or BLOCKEDSCREENINGS, reap_task not in tx.workspace...")

        return

    task = tx.workspace['reap_task']

    if task:
        queue = taskqueue.Queue(name=tx.get_queue())
        queue.delete_tasks(task)

    del tx.workspace['reap_task']

    tx.put()

def cancel_requery(tx):
    log.debug("scheduler, cancel_requery, PROMOS or BLOCKEDSCREENINGS, requery cancelled for tx, %s..." % tx.key.id())

    if 'requery_task' not in tx.workspace:
        log.warn("ERROR!, scheduler, cancel_requery, PROMOS or BLOCKEDSCREENINGS, requery_task not in tx.workspace...")

        return

    task = tx.workspace['requery_task']

    if task:
        queue = taskqueue.Queue(name=tx.get_requery_queue())
        queue.delete_tasks(task)

    del tx.workspace['requery_task']

    tx.put()


###############
#
# Reaper related methods/functions.
#
###############

@transactional
def reap_stale_tx(device_id, tx_id):
    tx = query_info(device_id, tx_id)
    tx.workspace['reap_task'] = None

    try:
        cancel(device_id, tx_id)
    except TransactionConflictException, ignored:
        log.warn("ERROR!, reaper, reap_stale_tx, reap task fired on a transaction in the wrong state; may have been retried?")
    except AlreadyCancelledException, ignored:
        log.warn("ERROR!, reaper, reap_stale_tx, reap task fired on an already cancelled transaction, will ignore...")
    except Exception, e:
        log.warn("ERROR!, reaper, reap_stale_tx, something went wrong...")
        log.error(e)

    tx.was_reaped = True
    tx.put()


###############
#
# Paynamics Requery related methods/functions.
#
###############

class WSPaynamicsRequery():
    def __init__(self, wsdl):
        schema_url = 'http://www.w3.org/2001/XMLSchema'
        schema_import = suds.xsd.doctor.Import(schema_url)
        schema_import.filter.add(BLOCKED_PAYNAMICS_TARGETNAMESPACE)
        schema_doctor = suds.xsd.doctor.ImportDoctor(schema_import)
        self.ws_client = suds.client.Client(url=wsdl, doctor=schema_doctor)

    def requery(self, merchant_id, request_id, org_trxid, org_trxid2, signature):
        log.debug("WSPaynamicsRequery, requery, start paynamics requery...")

        try:
            log.info("WSPaynamicsRequery, requery, merchant_id, %s..." % merchant_id)
            log.info("WSPaynamicsRequery, requery, request_id, %s..." % request_id)
            log.info("WSPaynamicsRequery, requery, org_trxid, %s..." % org_trxid)
            log.info("WSPaynamicsRequery, requery, org_trxid2, %s..." % org_trxid2)
            log.info("WSPaynamicsRequery, requery, signature, %s..." % signature)

            requery_response = self.ws_client.service.query(merchantid=merchant_id,
                request_id=request_id, org_trxid=org_trxid, org_trxid2=org_trxid2, signature=signature)

            return 'success', requery_response
        except suds.WebFault, e:
            log.warn("WSPaynamicsRequery, requery, failed paynamics requery...")
            log.error(e)
        except Exception, e:
            log.warn("WSPaynamicsRequery, requery, something went wrong...")
            log.error(e)

        return 'error', None

@transactional
def paynamics_requery_tx(device_id, tx_id):
    tx = query_info(device_id, tx_id)

    try:
        tx.workspace['requery_task'] = None
        merchant_id = BLOCKED_MERCHANT_ID
        merchant_key = BLOCKED_MERCHANT_KEY
        request_id = 'RQ' + tx.reservation_reference
        org_trxid = ''
        org_trxid2 = tx.reservation_reference
        seed = [merchant_id, request_id, org_trxid, org_trxid2, merchant_key]
        seed_str = ''.join(seed)
        signature = hashlib.sha512(seed_str).hexdigest()

        log.debug("paynamics_requery_tx, sending signature %s to paynamics..." % signature)

        paynamics_requery = WSPaynamicsRequery(BLOCKED_PAYNAMICS_REQUERY_URL)
        status, requery_response = paynamics_requery.requery(merchant_id, request_id, org_trxid, org_trxid2, signature)

        log.debug("paynamics_requery_tx, received response from paynamics, status: {0}, resposne: {1}...".format(status, requery_response))

        if status == 'success':
            txns_counter = 0
            tx.workspace['response::requery'] = {}
            requery_application = requery_response.application
            requery_status = requery_response.responseStatus

            log.debug("paynamics_requery_tx, response, requery_response.application, {}...".format(requery_application))
            log.debug("paynamics_requery_tx, response, requery_response.responseStatus, {}...".format(requery_status))

            # paynamics requery response outside txns.
            tx.workspace['response::requery']['rebill_id'] = ''
            tx.workspace['response::requery']['merchantid'] = requery_application.merchantid
            tx.workspace['response::requery']['request_id'] = requery_application.request_id
            tx.workspace['response::requery']['response_id'] = requery_application.response_id
            tx.workspace['response::requery']['timestamp'] = requery_application.timestamp
            tx.workspace['response::requery']['signature'] = requery_application.signature
            tx.workspace['response::requery']['response_code'] = requery_status.response_code
            tx.workspace['response::requery']['response_message'] = requery_status.response_message
            tx.workspace['response::requery']['response_advise'] = requery_status.response_advise

            log.debug("paynamics_requery_tx, tx.workspace['response::requery']: {}...".format(tx.workspace['response::requery']))

            # paynamics requery response inside txns.
            if requery_response.txns:
                for txns in requery_response.txns.ServiceResponse:
                    txns_workspace_key = 'response::requery::txns::%d' % txns_counter
                    txns_application = txns.application
                    txns_status = txns.responseStatus

                    log.debug("paynamics_requery_tx, txns_application, {}...".format(txns_application))
                    log.debug("paynamics_requery_tx, txns_status, {}...".format(txns_status))

                    tx.workspace[txns_workspace_key] = {}
                    tx.workspace[txns_workspace_key]['merchantid'] = txns_application.merchantid
                    tx.workspace[txns_workspace_key]['request_id'] = txns_application.request_id
                    tx.workspace[txns_workspace_key]['response_id'] = txns_application.response_id
                    tx.workspace[txns_workspace_key]['timestamp'] = txns_application.timestamp
                    tx.workspace[txns_workspace_key]['response_code'] = txns_status.response_code
                    tx.workspace[txns_workspace_key]['response_message'] = txns_status.response_message
                    tx.workspace[txns_workspace_key]['response_advise'] = txns_status.response_advise

                    txns_counter += 1

            if 'response::requery::txns::0' in tx.workspace:
                tx.workspace["response_code"] = tx.workspace['response::requery::txns::0']["response_code"]
                tx.workspace["response_message"] = tx.workspace['response::requery::txns::0']["response_message"]
                tx.payment_reference = tx.workspace['response::requery::txns::0']['response_id']

                log.debug("paynamics_requery_tx, tx.workspace['response::requery::txns::0']: {}...".format(tx.workspace['response::requery::txns::0']))
                log.debug("paynamics_requery_tx, completing: %s...", tx.key.id())

                requery_trigger_completion(tx)

                log.debug("paynamics_requery_tx,  triggered completion...")
            else:
                log.warn("ERROR!, paynamics_requery_tx, response, missing txns details...")
        else:
            log.warn("ERROR!, paynamics_requery_tx, something went wrong...")
    except Exception, e:
        log.warn("ERROR!, paynamics_requery_tx, Exception, something went wrong...")
        log.error(e)

    if tx:
        log.debug("paynamics_requery_tx, performed requery and tagged transaction %s via device %s..." % (tx_id, device_id))

        tx.was_requery = True
        tx.put()

@transactional
def requery_trigger_completion(tx):
    if tx.state != state.TX_PAYNAMICS_PAYMENT_HOLD:
        log.warn("ERROR!, requery_trigger_completion, attempt to complete TX %s, which is not in TX_PAYNAMICS_PAYMENT_HOLD; callback in error? - tx.state: %s" % (tx.key.id(), tx.state))

        return

    cancel_reaping(tx)
    transition_state(tx, state.TX_PAYNAMICS_PAYMENT_HOLD, state.TX_FINALIZE)
    reschedule(tx)


###############
#
# Mutex related methods/functions.
#
###############

def mutex_acquire(tx_id):
    while not memcache.add("LOCK::TXPROMO::%s" % tx_id, 1):
        pass

    return True

def mutex_release(tx_id):
    memcache.delete("LOCK::TXPROMO::%s" % tx_id)


###############
#
# State Machine related methods/functions.
#
###############

def to_state_str_promo(s):
    if s not in state.reverse_mapping:
        return 'TX_UNKNOWN'

    return state.reverse_mapping[s]

def on_start(tx, params):
    log.debug("state_machine, on_start, PROMOS or BLOCKEDSCREENINGS, TX %s\tstate TX_START\t\t Preparing transaction..." % tx.key.id())

    tx_id = tx.key.id()
    transition_state(tx, state.TX_START, state.TX_PREPARE)
    r, msg = prepare_transaction(tx)
    reschedule(tx, r=r, msg=msg)

def on_started(tx, params):
    log.debug("state_machine, on_started, PROMOS or BLOCKEDSCREENINGS, entering ON_STARTED state...")
    log.debug("state_machine, on_started, PROMOS or BLOCKEDSCREENINGS, TX %s\tstate TX_STARTED\t\t Running reservation..." % tx.key.id())

    tx_id = tx.key.id()
    transition_state(tx, state.TX_STARTED, state.TX_RESERVATION_HOLD)
    r, msg = do_reservation(tx)

    log.debug("state_machine, on_started, PROMOS or BLOCKEDSCREENINGS, do_reservation, STATUS: {0}, MSG: {1}...".format(r, msg))
    log.debug("state_machine, on_started, PROMOS or BLOCKEDSCREENINGS, leaving ON_STARTED state...")

    reschedule(tx, r=r, msg=msg)

def on_reservation_ok(tx, params):
    log.debug("state_machine, on_reservation_ok, PROMOS or BLOCKEDSCREENINGS, entering ON_RESERVATION_OK state...")
    log.debug("state_machine, on_reservation_ok, PROMOS or BLOCKEDSCREENINGS, TX %s\tstate TX_RESERVATION_OK\t\t Running payment" % tx.key.id())

    tx_id = tx.key.id()

    if tx.payment_type == PromoReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT:
        log.debug("state_machine, on_reservation_ok, PROMOS or BLOCKEDSCREENINGS, TX %s\t\t\tTransaction flagged for paynamics payment..." % tx_id)

        transition_state(tx, state.TX_RESERVATION_OK, state.TX_PAYNAMICS_PAYMENT_HOLD)
        blocked_bind_listeners(tx)
        schedule_for_reaping(tx, BLOCKED_TIMEOUT)
        schedule_for_requery(tx, BLOCKED_TIMEOUT_REQUERY)

        log.debug("state_machine, on_reservation_ok, PROMOS or BLOCKEDSCREENINGS, scheduled for reaping, %d seconds..." % BLOCKED_TIMEOUT)
        log.debug("state_machine, on_reservation_ok, PROMOS or BLOCKEDSCREENINGS, scheduled for requery, %d seconds..." % BLOCKED_TIMEOUT_REQUERY)
        log.debug("state_machine, on_reservation_ok, PROMOS or BLOCKEDSCREENINGS, leaving ON_RESERVATION_OK state...")

        return

    transition_state(tx, state.TX_RESERVATION_OK, state.TX_PAYMENT_HOLD)
    r, msg = do_payment(tx)

    log.debug("state_machine, on_reservation_ok, PROMOS or BLOCKEDSCREENINGS, leaving ON_RESERVATION_OK state...")

    reschedule(tx, r=r, msg=msg)

def on_finalize(tx, params):
    log.debug("state_machine, on_finalize, PROMOS or BLOCKEDSCREENINGS, entering ON_FINALIZE state...")
    log.debug("state_machine, on_finalize, PROMOS or BLOCKEDSCREENINGS, TX %s\tstate TX_FINALIZE\t\t Finalizing transaction..." % tx.key.id())

    tx_id = tx.key.id()

    if tx.payment_type == PromoReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT:
        log.debug("state_machine, on_finalize, PROMOS or BLOCKEDSCREENINGS, TX %s\t\t\tTransaction flagged for external transaction finalization..." % tx_id)

        transition_state(tx, state.TX_FINALIZE, state.TX_FINALIZE_HOLD)
        r, msg = do_payment(tx)

        log.debug("state_machine, on_finalize, PROMOS or BLOCKEDSCREENINGS, leaving ON_FINALIZE state...")

        reschedule(tx, r=r, msg=msg)

def on_finalize_hold(tx, params):
    log.debug("state_machine, on_finalize_hold, PROMOS or BLOCKEDSCREENINGS, entering ON_FINALIZE_HOLD state...")
    log.debug("state_machine, on_finalize_hold, PROMOS or BLOCKEDSCREENINGS, TX %s\tstate TX_FINALIZE_HOLD\t\t Finalization hold..." % tx.key.id())

    tx_id = tx.key.id()

    if tx.payment_type == PromoReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT:
        log.debug("state_machine, on_finalize_hold, PROMOS or BLOCKEDSCREENINGS, TX %s\t\t\tTransaction held for external transaction finalization..." % tx_id)

        transition_state(tx, state.TX_FINALIZE_HOLD, state.TX_GENERATE_TICKET)

        log.debug("state_machine, on_finalize_hold, PROMOS or BLOCKEDSCREENINGS, leaving ON_FINALIZE_HOLD state...")

        reschedule(tx)

def on_ticket_generate(tx, params):
    log.debug("state_machine, on_ticket_generate, PROMOS or BLOCKEDSCREENINGS, entering ON_TICKET_GENERATE state...")
    log.debug("state_machine, on_ticket_generate, PROMOS or BLOCKEDSCREENINGS, TX %s\tstate TX_GENERATE_TICKET\t\t Generating ticket..." % tx.key.id())

    tx_id = tx.key.id()
    do_ticket(tx)
    transition_state(tx, state.TX_GENERATE_TICKET, state.TX_DONE)

    log.debug("state_machine, on_ticket_generate, PROMOS or BLOCKEDSCREENINGS, leaving ON_TICKET_GENERATE state...")

    reschedule(tx)

def on_done(tx, params):
    log.debug("state_machine, on_done, PROMOS or BLOCKEDSCREENINGS, entering ON_DONE state...")

    tx_id = tx.key.id()
    org_uuid = None
    message = ''

    if tx.workspace['promos.theater_id']:
        org_uuid, theater_id = decoded_theater_id(tx.workspace['promos.theater_id'])

    if 'msg' in tx.workspace:
        message = tx.workspace['msg']

    log.debug("state_machine, on_done, PROMOS or BLOCKEDSCREENINGS, TX %s\tstate TX_DONE\t\t Deleting workspace..." % tx_id)

    tx.workspace = {}
    tx.workspace['msg'] = message
    tx.put()

    # Sending ticket details via email.
    if 'email' in tx.payment_info:
        send_ticket_details(tx.payment_info['email'], tx.ticket)

    log.debug("state_machine, on_done, PROMOS or BLOCKEDSCREENINGS, leaving ON_DONE state...")

HOLD_NEXT_STATES = {
    state.TX_PREPARE: (state.TX_STARTED, state.TX_PREPARE_ERROR),
    state.TX_RESERVATION_HOLD: (state.TX_RESERVATION_OK, state.TX_RESERVATION_ERROR),
    state.TX_PAYMENT_HOLD: (state.TX_GENERATE_TICKET, state.TX_PAYMENT_ERROR),
    state.TX_FINALIZE: (state.TX_FINALIZE_HOLD, state.TX_PAYMENT_ERROR),
    state.TX_FINALIZE_HOLD: (state.TX_GENERATE_TICKET, state.TX_PAYMENT_ERROR)
}

def bind_on_hold(which):
    new_success_state, new_error_state = HOLD_NEXT_STATES[which]

    def on_hold(tx, params):
        tx_id = tx.key.id()
        ret = params['r']

        if ret == 'success':
            log.debug("state_machine, bind_on_hold, PROMOS or BLOCKEDSCREENINGS, TX %s\tstate %s\t\t Success..." % (tx_id, which))

            transition_state(tx, which, new_success_state)
            reschedule(tx)
        else:
            msg = params['msg']

            log.debug("state_machine, bind_on_hold, PROMOS or BLOCKEDSCREENINGS, TX %s\tstate %s\t\t Error %s..." % (tx_id, which, msg))

            tx.workspace['msg'] = msg
            transition_state(tx, which, new_error_state)
            schedule_for_reaping(tx)

    return on_hold

state_handlers = {
    state.TX_START: on_start,
    state.TX_PREPARE: bind_on_hold(state.TX_PREPARE),
    state.TX_STARTED: on_started,
    state.TX_PAYMENT_HOLD: bind_on_hold(state.TX_PAYMENT_HOLD),
    state.TX_RESERVATION_OK: on_reservation_ok,
    state.TX_RESERVATION_HOLD: bind_on_hold(state.TX_RESERVATION_HOLD),
    state.TX_FINALIZE: on_finalize,
    state.TX_FINALIZE_HOLD: bind_on_hold(state.TX_FINALIZE_HOLD),
    state.TX_GENERATE_TICKET: on_ticket_generate,
    state.TX_DONE: on_done
}

def current_state(tx):
    return tx.state

def transition_state(tx, current_state, new_state):
    tx_id = tx.key.id()

    if tx.state == current_state or not current_state:
        tx.state = new_state
        tx.ver += 1
        tx.put()
    else:
        log.warn("state_machine, transition_state, PROMOS or BLOCKEDSCREENINGS, transitioning to an older state; did a transaction fail? (current: %s, expected: %s)..." % (to_state_str_promo(tx.state), to_state_str_promo(current_state)))

def next_state(params):
    device_id = params['device_id']
    tx_id = params['tx_id']
    mutex_acquire(tx_id)

    try:
        state = __in_transaction_next_state(device_id, tx_id, params)
    finally:
        mutex_release(tx_id)

    log.debug("state_machine, transition_state, PROMOS or BLOCKEDSCREENINGS, next_state on Device %s -> TX %s => %s...", device_id, tx_id, to_state_str_promo(state))

    return state

@transactional(retries=0)
def __in_transaction_next_state(device_id, tx_id, params):
    retry = int(params.get('retry', '0'))
    tx_ver = int(params['tx_ver'])

    if retry >= TX_TASK_MAX_RETRY:
        log.warn("ERROR!, state_machine, __in_transaction_next_state, PROMOS or BLOCKEDSCREENINGS, TX %s on device %s reached max retry; bailing...", tx_id, device_id)

        return None

    tx = query_info(device_id, tx_id)
    current_tx_state = current_state(tx)

    if current_tx_state == state.TX_CANCELLED:
        log.debug("state_machine, __in_transaction_next_state, PROMOS or BLOCKEDSCREENINGS, TX %s\tstate %s\t\t Transaction was cancelled..." % (tx_id, to_state_str_promo(current_tx_state)))

        return current_tx_state

    if tx_ver < tx.ver:
        log.warn("ERROR!, state_machine, __in_transaction_next_state, PROMOS or BLOCKEDSCREENINGS, TX %s\tstate %s\t\t Retried for spurious transaction failure; exiting...", tx_id, to_state_str_promo(current_tx_state))

        return current_tx_state

    if current_tx_state not in state_handlers:
        if tx_ver > tx.ver:
            log.warn("state_machine, __in_transaction_next_state, PROMOS or BLOCKEDSCREENINGS, TX %s\tstate %s\t Transaction was scheduled! Shouldn't be at this state..." % (tx_id, to_state_str_promo(current_tx_state)))
            log.warn("state_machine, __in_transaction_next_state, PROMOS or BLOCKEDSCREENINGS, might be due to datastore contention; intentionally rescheduling transaction (incrementing retry)...")

            reschedule(tx, retry=str(retry+1))
        else:
            log.fatal("ERROR!, state_machine, __in_transaction_next_state, PROMOS or BLOCKEDSCREENINGS, TX %s: Should not have been scheduled in this state: %s...", tx_id, to_state_str_promo(current_tx_state))

            assert current_tx_state in state_handlers, 'Unexpected state'
    else:
        try:
            state_handlers[current_tx_state](tx, params)
        except Exception, e:
            log.warn("ERROR!, state_machine, __in_transaction_next_state, PROMOS or BLOCKEDSCREENINGS, exception occurred in state_handler, rescheduling transaction tasklet...")
            log.exception(e)
            # Reset state to current_tx_state (we need to do this,
            # also because we cache state in memcache, which doesn't
            # participate in the transaction)
            transition_state(tx, None, current_tx_state)

            raise e

    return current_tx_state

def prepare_transaction(tx):
    r, msg = prepare_reservation(tx)

    if r == 'success':
        log.debug("state_machine, __in_transaction_next_state, PROMOS or BLOCKEDSCREENINGS, Reservation Preparation Success --> Preparing Payment...")

        r, msg = prepare_payment(tx)

    return r, msg


###############
#
# Paynamics Payment Initiated related methods/functions.
#
###############

def blocked_listener_bind():
    version = os.environ['CURRENT_VERSION_ID'].split('.')
    major_version = version[0]
    listener_queryset = Listener.query(Listener.event_channel==BLOCKED_CHANNEL_NAME,
            Listener.version==major_version, Listener.listener_id==BLOCKED_LISTENER_ID)

    if listener_queryset.count() == 0:
        l = Listener()
        l.event_channel = BLOCKED_CHANNEL_NAME
        l.version = major_version
        l.listener_id = BLOCKED_LISTENER_ID
        l.callback = blocked_payment_completion_listener

        log.debug("state_machine, blocked_listener_bind, PROMOS or BLOCKEDSCREENINGS, bound listener for channel: %s...", BLOCKED_CHANNEL_NAME)

        l.put()

def blocked_bind_listeners(tx):
    log.debug("state_machine, blocked_bind_listeners, PROMOS or BLOCKEDSCREENINGS, (deferred) Binding listener for TX %s...", tx.key.id())

    deferred.defer(blocked_listener_bind)

def blocked_payment_completion_listener(outstanding):
    for e in outstanding:
        log.debug("state_machine, blocked_payment_completion_listener, PROMOS or BLOCKEDSCREENINGS, processing: %s...", e)

        if e.payload:
            if 'request_id' not in e.payload:
                log.debug("state_machine, blocked_payment_completion_listener, PROMOS or BLOCKEDSCREENINGS, cannot find Reference Number in callback response...")

                continue

            reservation_reference = e.payload['request_id']
            paynamics_payment_reference = e.payload["response_id"]
            signed_xml_response = generate_response_signature(e.payload)
            tx = query_reservation_reference(reservation_reference)

            log.debug("state_machine, blocked_payment_completion_listener, PROMOS or BLOCKEDSCREENINGS, reference_number, %s..." % reservation_reference)
            log.debug("state_machine, blocked_payment_completion_listener, PROMOS or BLOCKEDSCREENINGS, payment_reference, %s..." % paynamics_payment_reference)

            if tx:
                if 'signature' in e.payload:
                    log.debug("state_machine, blocked_payment_completion_listener, PROMOS or BLOCKEDSCREENINGS, callback, received signature {} from paynamics...".format(e.payload['signature']))
                    log.debug("state_machine, blocked_payment_completion_listener, PROMOS or BLOCKEDSCREENINGS, callback, generated signature {} from paynamics response...".format(signed_xml_response))

                    tx.workspace["signed_xml_response"] = False

                    if signed_xml_response == e.payload["signature"]:
                        tx.workspace["signed_xml_response"] = True

                if 'response_code' in e.payload and 'response_message' in e.payload:
                    tx.workspace["response_code"] = e.payload["response_code"]
                    tx.workspace["response_message"] = e.payload["response_message"]

                tx.payment_reference = paynamics_payment_reference
                tx.put()

                log.debug("state_machine, blocked_payment_completion_listener, PROMOS or BLOCKEDSCREENINGS, completing: %s...", tx.key.id())

                blocked_trigger_completion(tx)
                e.read = True

                log.debug("state_machine, blocked_payment_completion_listener, PROMOS or BLOCKEDSCREENINGS, triggered completion...")
            else:
                log.warn("state_machine, blocked_payment_completion_listener, PROMOS or BLOCKEDSCREENINGS, cannot find reservation_reference: {}...".format(reservation_reference))

@transactional
def blocked_trigger_completion(tx):
    if tx.state != state.TX_PAYNAMICS_PAYMENT_HOLD:
        log.warn("ERROR!, state_machine, blocked_payment_completion_listener, PROMOS or BLOCKEDSCREENINGS, attempt to complete TX %s, which is not in TX_PAYNAMICS_PAYMENT_HOLD; callback in error? - tx.state: %s" % (tx.key.id(), tx.state))

        return

    cancel_reaping(tx)
    cancel_requery(tx)
    transition_state(tx, state.TX_PAYNAMICS_PAYMENT_HOLD, state.TX_FINALIZE)
    reschedule(tx)

@non_transactional
def generate_response_signature(params):
    merchant_id = BLOCKED_MERCHANT_ID
    merchant_key = BLOCKED_MERCHANT_KEY
    request_id = params["request_id"]
    response_id = params["response_id"]
    timestamp = params["timestamp"]
    rebill_id = params["rebill_id"]
    response_code = params["response_code"]
    response_message = params["response_message"]
    response_advise = params["response_advise"]
    seed = [merchant_id, request_id, response_id, response_code, response_message, response_advise, timestamp, rebill_id, merchant_key]
    seed_str = ''.join(seed)
    signature = hashlib.sha512(seed_str).hexdigest()

    return signature


###############
#
# Ticket related methods/functions.
#
###############

@non_transactional
def __get_is_qrcode_ticket(promo_id):
    promo = Key(Promo, promo_id).get()

    return promo.is_qrcode

@non_transactional
def __get_ticket_template_details(promo_id):
    promo_key = Key(Promo, promo_id)
    promo = promo_key.get()
    ticket_template_image = TicketTemplateImageBlob.query(TicketTemplateImageBlob.template_type=='promo', ancestor=promo.key).get()
    template = promo.ticket_template
    template_image = ticket_template_image.image

    return template_image, template

@non_transactional
def new_line_promo_name(template, promo_name):
    if len(promo_name) > 32:
        height = template['promo_name'].height
        template['promo_name'].height = template['promo_name'].height * 2
        template['theater_schedule'].y = template['theater_schedule'].y + height
        template['price'].y = template['price'].y + height
        template['total_price'].y = template['total_price'].y + height

    return template

@non_transactional
def new_line_seats_imploded(template, seats_imploded):
    if len(seats_imploded) > 5:
        seats_imploded = [', '.join(seats_imploded[:5]).strip(), ', '.join(seats_imploded[5:]).strip()]
        seats_imploded_height = template['seats_imploded'].height
        template['seats_imploded'].y = template['seats_imploded'].y - (int(seats_imploded_height) + 2) * 2
        template['seats_imploded_label'].y = template['seats_imploded_label'].y - int(seats_imploded_height)
    else:
        seats_imploded = ', '.join(seats_imploded)

    return template, seats_imploded

def do_ticket(tx):
    ref = tx.reservation_reference
    code = tx.workspace['RSVP:claimcode']
    date = tx.workspace['RSVP:claimdate']
    theater_and_cinema_name = '%s %s' % (tx.workspace['promos.theater'], tx.workspace['promos.cinema'])
    show_datetime = '%s %s' % (tx.workspace['promos.available_date'].strftime(TICKET_DATE_FORMAT), tx.workspace['promos.start_time'])

    ticket = PromoTicketData(ref=ref, code=code, date=date, extra={})
    ticket.extra['promo_id'] = tx.workspace['promos.promo_id'][0] + '@promo'
    ticket.extra['promo_name'] = tx.workspace['promos.name']
    ticket.extra['movie_id'] = tx.workspace['movies.movie_id']
    ticket.extra['movie_title'] = tx.workspace['movies.canonical_title']
    ticket.extra['movie_canonical_title'] = tx.workspace['movies.canonical_title']
    ticket.extra['movie_rating'] = tx.workspace['movies.advisory_rating']
    ticket.extra['movie_advisory_rating'] = tx.workspace['movies.advisory_rating']
    ticket.extra['movies_runtime_mins'] = tx.workspace['movies.runtime_mins']
    ticket.extra['variant'] = ''
    ticket.extra['theater_name'] = tx.workspace['promos.theater']
    ticket.extra['cinema_name'] = tx.workspace['promos.cinema']
    ticket.extra['theater_and_cinema_name'] = theater_and_cinema_name.strip()
    ticket.extra['available_date'] = tx.workspace['promos.available_date'].strftime(TICKET_DATE_FORMAT)
    ticket.extra['start_time'] = tx.workspace['promos.start_time']
    ticket.extra['end_time'] = tx.workspace['promos.end_time']
    ticket.extra['show_datetime'] = show_datetime.strip()
    ticket.extra['seats'] = tx.workspace['seats::imploded']
    ticket.extra['ticket_count'] = tx.reservation_count()
    ticket.extra['original_total_amount'] = tx.workspace['RSVP:originaltotalamount']
    ticket.extra['original_seat_price'] = tx.workspace['RSVP:originalseatprice']
    ticket.extra['total_amount'] = tx.workspace['RSVP:totalamount']
    ticket.extra['total_seat_price'] = tx.workspace['RSVP:totalseatprice']
    ticket.extra['price_per_seat'] = tx.workspace['promos.price_per_seat']
    ticket.extra['reservation_fee'] = tx.workspace['RSVP:fee']
    ticket.extra['total_discount'] = tx.workspace['RSVP:totaldiscount']
    ticket.extra['total_seat_discount'] = tx.workspace['RSVP:totalseatdiscount']
    ticket.extra['is_blocked_screening'] = tx.workspace['promos.is_block_screening']
    ticket.extra['is_multiple_tickets'] = False

    tx.ticket = ticket

    if __get_is_qrcode_ticket(tx.workspace['promos.promo_id'][0]):
        ticket.extra['ticket_ui_version'] = '2'
    else:
        ticket.extra['ticket_ui_version'] = '1'

    try:
        generate_ticket_barcode(tx, font_size=20, text_distance=2, dpi=230)
        generate_ticket_qrcode(tx)
        generate_ticket_image(tx)
    except Exception, e:
        log.warn("ERROR!, ticket, do_ticket, PROMOS or BLOCKEDSCREENINGS, failed to generate images...")
        log.error(e)

    tx.put()

def generate_ticket_image(tx):
    promo_id = tx.workspace['promos.promo_id'][0]

    template_img, template = __get_ticket_template_details(promo_id)
    is_qrcode = __get_is_qrcode_ticket(promo_id)
    template = template_yaml_to_metrics(template)

    barcode_data = tx.ticket.barcode
    qrcode_data = tx.ticket.qrcode

    if is_qrcode:
        barcode_data = qrcode_data

    ticket_img = draw_ticket(template_img, template, barcode_data, {
        'promo_name': tx.workspace['promos.name'].upper(),
        'movie_title': tx.ticket.extra['movie_canonical_title'].upper(),
        'theater_name': tx.workspace['promos.theater'],
        'cinema_name': tx.workspace['promos.cinema'],
        'available_date': tx.workspace['promos.available_date'].strftime(TICKET_DATE_FORMAT),
        'start_time': tx.workspace['promos.start_time'],
        'end_time': tx.workspace['promos.end_time'],
        'seats_imploded': tx.workspace['seats::imploded'],
        'ticket_count': tx.reservation_count(),
        'total_price': tx.ticket.extra['total_amount'],
        'per_ticket_price': tx.ticket.extra['price_per_seat'],
        'reservation_fee': tx.ticket.extra['reservation_fee'],
        'is_blocked_screening': tx.ticket.extra['is_blocked_screening'],
        'code': tx.ticket.code
    }, is_qrcode=is_qrcode)

    png_data = ticket_img.getvalue()
    tx.ticket.image = png_data

def draw_ticket(template_img, template, barcode, ticket_data, is_qrcode=False):
    if is_qrcode:
        template['seats_imploded_label'].y = 634
        template['seats_imploded_label'].text_align = 'right'
        template['seats_imploded_label'].width = 668
        template['seats_imploded'].y = 650
        template['seats_imploded'].text_align = 'right'
        template['seats_imploded'].width = 668
    else:
        ticket_data.pop('code', None)
        template.pop('code', None)

    theater_and_cinema_text = ''
    start_time_text = ''
    end_time_text = ''
    price_text = ''
    total_price_text = ''
    seats_imploded = ticket_data['seats_imploded'].split(',')
    template, seats_imploded = new_line_seats_imploded(template, seats_imploded)
    template = new_line_promo_name(template, ticket_data['promo_name'])

    if ticket_data['theater_name'] and ticket_data['cinema_name']:
        theater_and_cinema_text = '%s - %s\n' % (ticket_data['theater_name'], ticket_data['cinema_name'])

    if ticket_data['start_time']:
        start_time_text = ' %s' % ticket_data['start_time']

    if ticket_data['end_time']:
        end_time_text = ' - %s' % ticket_data['end_time']

    if ticket_data['is_blocked_screening']:
        price_text = 'Purchased %s @ Php %s each\nReservation Fee: Php %s' % (ticket_data['ticket_count'], ticket_data['per_ticket_price'], ticket_data['reservation_fee'])
        total_price_text = 'Total Price: Php %s' % ticket_data['total_price']

    schedule_text = ticket_data['available_date'] + start_time_text + end_time_text
    theater_schedule_text = theater_and_cinema_text + schedule_text
    theater_schedule_text = theater_schedule_text.split('\n')
    price_text = price_text.split('\n')

    if is_qrcode:
        barcode_loc = template['qrcode']
    else:
        barcode_loc = center_barcode_on_ticket(template, template_img, barcode)

    ticket_data['price'] = price_text
    ticket_data['total_price'] = total_price_text
    ticket_data['theater_schedule'] = theater_schedule_text
    ticket_data['seats_imploded_label'] = 'Seat No.'
    ticket_data['seats_imploded'] = seats_imploded

    base_canvas = StringIO(template_img)
    text_canvas = ImageText(base_canvas, background=(255, 255, 255, 255), supersample=True)

    for key, metrics in template.items():
        if key != 'barcode' and key != 'qrcode':
            text = ticket_data[key]
            draw_text_on_ticket(text_canvas, text, metrics)

    ticket = StringIO()
    base_image = text_canvas.merge_with_base()
    barcode_f = StringIO(barcode)
    barcode_img = Image.open(barcode_f)
    barcode_mask = barcode_img.copy()
    barcode_mask = Image.eval(barcode_mask.convert('1'), lambda x: not x)
    base_image.paste(barcode_img, barcode_loc, barcode_mask)
    base_image.save(ticket, 'PNG')

    return ticket

def draw_text_on_ticket(text_canvas, text, metrics):
    if metrics.font_color:
        color = tuple([int(num) for num in metrics.font_color.split(',')])
    else:
        color = (0, 0, 0)

    if not isinstance(text, list):
        text_canvas.write_text_box(metrics.location(), text, box_width=metrics.width, font_name=metrics.font_name,
                font_size=metrics.font_size, color=color, place=metrics.text_align)
    else:
        loc = metrics.location()

        for line in text:
            loc = (loc[0], loc[1] + metrics.font_size + 4)
            text_canvas.write_text_box(loc, line, box_width=metrics.width, font_name=metrics.font_name,
                    font_size=metrics.font_size, color=color, place=metrics.text_align)

def center_barcode_on_ticket(template, template_img, barcode):
    default_barcode_loc = template['barcode']
    base_canvas = StringIO(template_img)
    barcode_f = StringIO(barcode)
    base_canvas_img = Image.open(base_canvas)
    barcode_img = Image.open(barcode_f)
    base_canvas_size = base_canvas_img.size # get the width and height of base canvas
    barcode_size = barcode_img.size # get the width and height of barcode
    x_position = (base_canvas_size[0] - barcode_size[0]) / 2 # center the barcode on base canvas
    barcode_loc = (x_position, default_barcode_loc[1])

    return barcode_loc

def template_yaml_to_metrics(yaml_text):
    text_stream = StringIO(yaml_text)
    metrics_yaml = yaml.load(text_stream)
    metrics = {}
    defaults = {}

    if 'defaults' in metrics_yaml:
        defaults = metrics_yaml['defaults']

    with_default = lambda k,d,v: v.get(k, d.get(k, None))

    for k, v in metrics_yaml['parts'].items():
        metrics[k] = TemplateTextBoxMetrics(font_name=with_default('font_name', defaults, v),
                font_size=with_default('font_size', defaults, v), font_color=with_default('font_color', defaults, v),
                x=with_default('x', defaults, v), y=with_default('y', defaults, v), width=with_default('width', defaults, v),
                height=with_default('height', defaults, v), text_align=with_default('text_align', defaults, v))

    barcode_loc = metrics_yaml['barcode']
    metrics['barcode'] = (barcode_loc['x'], barcode_loc['y'])

    qrcode_loc = metrics_yaml['qrcode']
    metrics['qrcode'] = (qrcode_loc['x'], qrcode_loc['y'])

    return metrics

def generate_ticket_barcode(tx, font_size=10, text_distance=5, dpi=300):
    ticket_barcode = StringIO()
    ticket_code = tx.ticket.code
    barcode_gen = TICKET_BARCODE_GENERATOR(code=ticket_code, writer=CustomImageWriter(), add_checksum=False)
    barcode_gen.write(ticket_barcode, options={'font_size': font_size, 'text_distance': text_distance, 'dpi': dpi})
    tx.ticket.barcode = ticket_barcode.getvalue()

def generate_ticket_qrcode(tx, version=1, box_size=10, border=1):
    ticket_qrcode = StringIO()
    ticket_code = tx.ticket.code
    qrcode_gen = QRCode(version=version, error_correction=constants.ERROR_CORRECT_L, box_size=box_size, border=border)
    qrcode_gen.add_data(ticket_code)
    qrcode_gen.make(fit=True)
    qrcode_image = qrcode_gen.make_image()
    qrcode_image.save(ticket_qrcode)
    tx.ticket.qrcode = ticket_qrcode.getvalue()

class CustomImageWriter(ImageWriter):
    def _paint_text(self, xpos, ypos):
        xpos = ((xpos + self.font_size) / 2) - self.quiet_zone
        pos = (mm2px(xpos, self.dpi), mm2px(ypos, self.dpi))
        font = ImageFont.truetype(FONT, self.font_size)
        self._draw.text(pos, self.text, font=font, fill=self.foreground)

class TemplateTextBoxMetrics():
    def __init__(self, font_name, font_size, font_color, x, y, width, height, text_align):
        self.font_name = font_name
        self.font_size = font_size
        self.font_color = font_color
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.text_align = text_align

    def location(self):
        return (self.x, self.y)