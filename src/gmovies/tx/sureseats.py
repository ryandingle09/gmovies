# Sureseats-related methods

import logging
log = logging.getLogger(__name__)

import requests
from Crypto.Cipher import AES

SURESEATS_AES_KEY='0123456789abcdef'
SURESEATS_AES_IV='fedcba9876543210'

BS=16
def pkcs5_padding(s):
    padding_length = (BS - len(s) % BS)
    return s + padding_length * chr(padding_length)

DUMMY='http://example.com/'

def create_sureseats_request(endpoint, params):
    pass1_params = dict(**params)
    del pass1_params['action']

    log.info('Pass1 Params: {}'.format(pass1_params))
    pass1 = requests.Request('POST', url=endpoint, data=pass1_params).prepare()
    param_string = pass1.body
    log.info('Param String: {}'.format(param_string))

    encrypted_value = sureseats_encrypt_params(param_string)

    req_params = { 'action': params['action'], 'en': encrypted_value }
    log.info('Request Params for Sureseats: {}'.format(req_params))
    req = requests.Request('POST', url=endpoint, params=req_params).prepare()
    return req

def sureseats_encrypt_params(string):
    aes = AES.new(SURESEATS_AES_KEY, AES.MODE_CBC, SURESEATS_AES_IV)

    log.debug("Encrypting: |%s| %s" % (string, len(string)))

    enc = pkcs5_padding(string + '\0')
    log.debug("Padded    : (len %s)" % len(enc))

    enc = aes.encrypt(enc)
    enc = enc.encode("hex")
    return enc
