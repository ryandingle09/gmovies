import base64
import hashlib
import hmac
import json
import logging
import operator
import re
import requests
import binascii
import traceback

from datetime import datetime, date
from lxml import etree
from uuid import uuid4 as uuid_gen
from collections import OrderedDict

from Crypto.Hash import SHA

from google.appengine.ext.ndb import Key, non_transactional
from .mpass_sureseats_login import create_sureseats_request_login
from .sureseats import sureseats_encrypt_params, create_sureseats_request
from .util import call_once, translate_message_via_error_code, DEFAULT_ERROR_MESSAGE, trigger_refund, seat_cancellation
from .rockwell import create_rockwell_request
from .greenhills import create_greenhills_request
from .cinema76 import create_cinema76_request
from .globeevents import create_globeevents_request
from .mgi import (check_balance_eplus, create_transaction_eplus, finalize_transaction_mgi, check_transaction_finalized,
        rebill_with_token, rebill_with_token_3d, reversal, refund, ALLOWED_MGI_RESERVATION_BRANCH_CODES)

from gmovies.models import ClientInitiatedPaymentSettings, ReservationTransaction, Theater, TheaterOrganization
from gmovies.orgs import (AYALA_MALLS, ROCKWELL_MALLS, GREENHILLS_MALLS, SM_MALLS, ROBINSONS_MALLS, MEGAWORLD_MALLS,
        CINEMA76_MALLS, GLOBE_EVENTS)
from gmovies.settings import (IPAY88_PAYMENT_ENDPOINT, ROCKWELL_ENDPOINT_PRIMARY, GREENHILLS_ENDPOINT_PRIMARY,
        CINEMA76_ENDPOINT_PRIMARY, GLOBEEVENTS_ENDPOINT_PRIMARY,
        PROMOCODE_SERVER, CLAIMCODE_GLOBAL_SERVER, GCASH_PROXY_BASE_URL, GREWARDS_PROXY_BASE_URL,
        LCT_MERCHANT_KEY, LCT_MERCHANT_CODE, LCT_IPAY88_PAYMENT_COMPLETE_URL,
        EPLUS_MERCHANT_ID, EPLUS_MERCHANT_CODE, EPLUS_CARDNUMBER, EPLUS_PIN,
        PAYNAMICS_CCP_BIN, PAYNAMICS_CCP_BIN_CITIBANK, PAYNAMICS_CCP_BIN_PNB,
        PAYNAMICS_CCP_BIN_BDO, PAYNAMICS_CCP_BIN_GCASH, PAYNAMICS_CCP_BIN_METROBANK,  
        PAYNAMICS_CCP_BIN_CHINABANK, PAYNAMICS_CCP_BIN_ROBINSONSBANK)
from gmovies.util import id_encoder
from gmovies.admin.email import send_email_support, send_email_refund_payment_gateway, send_email_refund_user, send_refund_claimcodetx_notification

log = logging.getLogger(__name__)

TIMEOUT_DEADLINE = 45
CREDIT_CARD_PAYMENT_ACTION = 'PAYCC'
MPASS_LOGIN = 'LOGIN'
WS_PAYMENT_ENGINE = 'payment::engine'

# SURESEATS ENDPOINTS
SURESEATS_ENDPOINT_PRIMARY = 'http://api.sureseats.com/globe.asp'
SURESEATS_ENDPOINT_PRIMARY_GCASH = 'http://api.sureseats.com/globe.asp'
SURESEATS_ENDPOINT_SECONDARY = 'http://api.sureseats.com/index_globe.asp'
# ROCKWELL POWER PLANT MALL ENDPOINTS
ROCKWELL_ENDPOINT_SUCCESS = ROCKWELL_ENDPOINT_PRIMARY + 'book-movie/success'
# GREENHILLS ENDPOINTS
GREENHILLS_ENDPOINT_SUCCESS = GREENHILLS_ENDPOINT_PRIMARY + 'book-movie/success'
GREENHILLS_ENDPOINT_CANCEL = GREENHILLS_ENDPOINT_PRIMARY + 'book-movie/cancel'
GREENHILLS_ENDPOINT_VOID = GREENHILLS_ENDPOINT_PRIMARY + 'book-movie/void'
# CINEMA76 ENDPOINTS
CINEMA76_ENDPOINT_SUCCESS = CINEMA76_ENDPOINT_PRIMARY + 'book-movie/success'
CINEMA76_ENDPOINT_CANCEL = CINEMA76_ENDPOINT_PRIMARY + 'book-movie/cancel'
CINEMA76_ENDPOINT_VOID = CINEMA76_ENDPOINT_PRIMARY + 'book-movie/void'
# GLOBE EVENTS ENDPOINTS
GLOBEEVENTS_ENDPOINT_SUCCESS = GLOBEEVENTS_ENDPOINT_PRIMARY + 'book-movie/success'
GLOBEEVENTS_ENDPOINT_CANCEL = GLOBEEVENTS_ENDPOINT_PRIMARY + 'book-movie/cancel'
GLOBEEVENTS_ENDPOINT_VOID = GLOBEEVENTS_ENDPOINT_PRIMARY + 'book-movie/void'
# CLAIM CODES ENDPOINTS
PROMOCODE_ENDPOINT = PROMOCODE_SERVER + '/redeem'
PROMOCODE_RESET_ENDPOINT = PROMOCODE_SERVER + '/reset'
PROMOCODE_VERIFY_ENDPOINT = PROMOCODE_SERVER + '/verify'
PROMOCODE_CHECK_ENDPOINT = PROMOCODE_SERVER + '/check_code'
PROMOCODE_MPASS_ENDPOINT = PROMOCODE_SERVER + '/mpass'
# CLAIM CODES GLOBAL ENDPOINTS
CLAIM_CODES_VERIFY_ENDPOINT = '%s/gmovies/claim-code/promo-verify' % CLAIMCODE_GLOBAL_SERVER
CLAIM_CODES_REDEEM_ENDPOINT = '%s/gmovies/claim-code/promo-redeem' % CLAIMCODE_GLOBAL_SERVER

# GCASH ENDPOINTS
GCASH_OTP_ENDPOINT = '%s/gmovies/gcash/pin/verify' % CLAIMCODE_GLOBAL_SERVER
GCASH_PAYMENT_ENDPOINT = '%s/payment' % GCASH_PROXY_BASE_URL
GCASH_PAYMENT_ENDPOINT_AYALA = '%s/n/payment' % GCASH_PROXY_BASE_URL
GCASH_PAYMENT_ENDPOINT_ROBINSONS = '%s/n/payment' % GCASH_PROXY_BASE_URL
GCASH_REFUND_ENDPOINT = '%s/refund' % GCASH_PROXY_BASE_URL
GCASH_REFUND_ENDPOINT_AYALA = '%s/n/refund' % GCASH_PROXY_BASE_URL

# GREWARDS ENDPOINTS
GREWARDS_OTP_ENDPOINT = '%s/gmovies/gcash/pin/verify' % CLAIMCODE_GLOBAL_SERVER
GREWARDS_PAYMENT_ENDPOINT = '%s/getReward' % GREWARDS_PROXY_BASE_URL
GREWARDS_PAYMENT_ENDPOINT_AYALA = '%s/getReward' % GREWARDS_PROXY_BASE_URL
GREWARDS_PAYMENT_ENDPOINT_ROBINSONS = '%s/getReward' % GREWARDS_PROXY_BASE_URL
GREWARDS_REFUND_ENDPOINT = '%s/getReward' % GREWARDS_PROXY_BASE_URL
GREWARDS_REFUND_ENDPOINT_AYALA = '%s/getReward' % GREWARDS_PROXY_BASE_URL

# GMOVIES MPASS ACCOUNT DETAILS
PROMOCODE_USERNAME = 'gmovies_promo'
# PROMOCODE_PASSWORD = 'Greg20091'
PROMOCODE_PASSWORD = 'gmovies123'
# GMOVIES E-PLUS ACCOUNT DETAILS AND TRANSACTION TYPE
EPLUS_CARDNUMBER = EPLUS_CARDNUMBER
EPLUS_PIN = EPLUS_PIN
EPLUS_TRASACTION_TYPE_CREDIT = 1
EPLUS_TRASACTION_TYPE_DEBIT = 2

# SURESEATS API PARAMETERS
RESERVATION_ACTION = 'TRANSACT2'
RESERVATION_TYPE   = 'RESERVE'
BUY_TYPE           = 'BUY'
SOURCE_ID          = '9'
PTYPE_GCASH        = 'G-Cash'
PTYPE_GREWARDS      = 'G-Rewards'

# SURESEATS - PESOPAY
WS_PAYMENT_CLIENT_INITIATED = 'payment::is-client-initiated'
WS_PAYMENT_AYALA_GCASH      = 'payment::is-ayala-gcash'
WS_PAYMENT_AYALA_GREWARDS   = 'payment::is-ayala-grewards'
# ROCKWELL (POWERPLANT MALL) - PAYNAMICS / GCASH / PROMOCODE
WS_PAYMENT_POWERPLANTMALL_PAYNAMICS = 'payment::is-powerplantmall-paynamics'
WS_PAYMENT_POWERPLANTMALL_GCASH = 'payment::is-powerplantmall-gcash'
WS_PAYMENT_POWERPLANTMALL_PROMOCODE = 'payment::is-powerplantmall-promocode'
# GREENHILLS - PAYNAMICS / GCASH / PROMOCODE
WS_PAYMENT_GREENHILLS_PAYNAMICS = 'payment::is-greenhills-paynamics'
WS_PAYMENT_GREENHILLS_GCASH = 'payment::is-greenhills-gcash'
WS_PAYMENT_GREENHILLS_PROMOCODE = 'payment::is-greenhills-promocode'
# CINEMA76 - PAYNAMICS / GCASH / PROMOCODE
WS_PAYMENT_CINEMA76_PAYNAMICS = 'payment::is-cinema76-paynamics'
WS_PAYMENT_CINEMA76_GCASH = 'payment::is-cinema76-gcash'
WS_PAYMENT_CINEMA76_PROMOCODE = 'payment::is-cinema76-promocode'
# GLOBE EVENTS - PAYNAMICS / GCASH / PROMOCODE
WS_PAYMENT_GLOBEEVENTS_PAYNAMICS = 'payment::is-globeevents-paynamics'
WS_PAYMENT_GLOBEEVENTS_GCASH = 'payment::is-globeevents-gcash'
WS_PAYMENT_GLOBEEVENTS_PROMOCODE = 'payment::is-globeevents-promocode'
# SM MALLS - PAYNAMICS / GCASH / PROMOCODE
WS_PAYMENT_SMMALLS_PAYNAMICS = 'payment::is-sm-malls-paynamics'
WS_PAYMENT_SMMALLS_GCASH = 'payment::is-sm-malls-gcash'
WS_PAYMENT_SMMALLS_PROMOCODE = 'payment::is-sm-malls-promocode'
# ROBINSONS MALLS - PAYNAMICS / PROMOCIDE / MIGS
WS_PAYMENT_ROBINSONS_PAYNAMICS = 'payment::is-robinsons-paynamics'
WS_PAYMENT_ROBINSONS_PROMOCODE = 'payment::is-robinsons-promocode'
WS_PAYMENT_ROBINSONSMALLS_MIGS = 'payment::is-robinsons-malls-migs'
WS_PAYMENT_ROBINSONS_GCASH = 'payment::is-robinsons-gcash'
# MEGAWORLD - IPAY88
WS_PAYMENT_MGW_IPAY88_PAYMENT = 'payment::is-megaworld-ipay88-payment'

# PAYNAMICS - WITH TOKEN
WS_PAYMENT_TOKEN_PAYNAMICS = 'payment::is-token-paynamics'
WS_PAYMENT_3D_TOKEN_PAYNAMICS = 'payment::is-3d-token-paynamics'

# PAYMENT GATEWAY WITH INTEGRATED THEATER ORGANIZATIONS
PAYNAMICS_THEATERORGS = [str(ROCKWELL_MALLS), str(GREENHILLS_MALLS), str(SM_MALLS), str(ROBINSONS_MALLS),
    str(CINEMA76_MALLS), str(GLOBE_EVENTS)]
PESOPAY_THEATERORGS = [str(AYALA_MALLS)]
MIGS_THEATERORGS = [str(ROBINSONS_MALLS)]
IPAY88_THEATERORGS = [str(MEGAWORLD_MALLS)]

# list of bins per cardholder promo
CARDHOLDER_PROMOS_BINS = {
        ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO: PAYNAMICS_CCP_BIN,
        ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO: PAYNAMICS_CCP_BIN_CITIBANK,
        ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO: PAYNAMICS_CCP_BIN_PNB,
        ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO: PAYNAMICS_CCP_BIN_BDO,
        ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO: PAYNAMICS_CCP_BIN_GCASH,
        ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO: PAYNAMICS_CCP_BIN_METROBANK,
        ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO: PAYNAMICS_CCP_BIN_CHINABANK,
        ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO: PAYNAMICS_CCP_BIN_ROBINSONSBANK,
}

CARDHOLDER_PROMOS = [ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO,
    ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO,
    ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO,
    ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO,
    ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO,
    ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO,
    ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO,
    ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO]

# TOKEN FOR CLAIM CODES GLOBAL SERVER
LBAPITOKEN = 'Mc78beJQcwdEbWWcBqkF4yT7gQ38eWVH2Ws5VeUJT6jEpuGYmYDj4jYAwCT3mjpRr6QW2b6S2VZsS7QzuMcJPhzF'
# TOKEN FOR GCASH PROXY SERVER
GMOVIESGCASHTOKEN = ':eT"{>U3qmX83g)n3^MxQ{+c]<7XY]9"TL[N`GC!4&8pB+jW~n#=q=T5\daWqxSa}w^~~5J\cu(~S"2jrSrAWM]/'
GMOVIESGREWARDSTOKEN = ':eT"{>U3qmX83g)n3^MxQ{+c]<7XY]9"TL[N`GC!4&8pB+jW~n#=q=T5\daWqxSa}w^~~5J\cu(~S"2jrSrAWM]/'

RUSH_REWARDS_CODE_TYPE = 'rush-rewards'

def prepare_payment(tx):
    try:
        theater_key = tx.workspace['theaters.key'][0]
        theaterorg_id = tx.workspace['theaters.org_id'][0]
        theater_code = tx.workspace['theaters.org_theater_code'][0]
        convenience_fee = tx.workspace['convenience_fee'] if 'convenience_fee' in tx.workspace else '20.00'

        # initializing payment handler to be use for transaction.
        if tx.payment_type == ReservationTransaction.allowed_payment_types.CREDIT_CARD:
            log.debug("prepare_payment, initializing CreditCardPayment handler...")

            engine = CreditCardPayment(tx.payment_info)
        elif tx.payment_type == ReservationTransaction.allowed_payment_types.MPASS:
            log.debug("prepare_payment, initializing MPassPayment handler...")

            engine = MPassPayment(tx.payment_info)
        elif tx.payment_type in [ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO]:
            log.debug("prepare_payment, initializing DiscountedCreditCardPayment handler for %s..." % tx.payment_type.replace('-','_').upper())

            engine = DiscountedCreditCardPayment(tx.payment_info, tx.discount_info)
        elif tx.payment_type == ReservationTransaction.allowed_payment_types.PROMO_CODE:
            if 'promo_code' not in tx.payment_info:
                log.warn("ERROR!, prepare_payment, missing key promo_code in tx.payment_info...")
                tx.error_info['error_msg'] = "missing key promo_code in tx.payment_info"
                return 'error', DEFAULT_ERROR_MESSAGE

            # Add checking if PROMO_CODE is bank deposit, else not eligible for discount
            status_code, code_details = check_promo_code(tx.payment_info['promo_code'])
            if status_code != 200:
                log.warn("ERROR!, prepare_payment, check_promo_code, status_code not equal to 200: %s..." % (status_code))
                tx.error_info['error_msg'] = json.dumps(code_details)
                if 400 == status_code:
                    return 'error', code_details
                return 'error', DEFAULT_ERROR_MESSAGE
            if 'is_bank_deposit' not in code_details['return']['claim_code_details']:
                log.warn("ERROR!, prepare_payment, missing key 'is_bank_deposit' in json response...")
                tx.error_info['error_msg'] = "missing key 'is_bank_deposit' in json response"
                return 'error', DEFAULT_ERROR_MESSAGE

            tx.workspace['is_bank_deposit'] = code_details['return']['claim_code_details']['is_bank_deposit']
            # if not tx.workspace['is_bank_deposit']:
            #     tx.workspace['convenience_fee'] = '0.00'
            log.debug("prepare_payment, tx.workspace['is_bank_deposit']: {}".format(tx.workspace['is_bank_deposit']))

            if tx.discount_info:
                log.debug("prepare_payment, initializing GlobalClaimCodePayment/RewardsPayment handler for PROMO_CODE...")

                if 'claim_code' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key claim_code in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key claim_code in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if 'user_id' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key user_id in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key user_id in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if 'mobile_number' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key mobile_number in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key mobile_number in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if 'claim_code_type' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key claim_code_type in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key claim_code_type in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if 'payment_gateway' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key payment_gateway in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key payment_gateway in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if tx.workspace['is_bank_deposit']:
                    if RUSH_REWARDS_CODE_TYPE == tx.discount_info['claim_code_type']:
                        log.debug("prepare_payment, initializing RewardsPayment handler for PROMO_CODE...")
                        engine = RewardsPayment(tx.payment_info, tx.discount_info)
                    else:
                        log.debug("prepare_payment, initializing GlobalClaimCodePayment handler for PROMO_CODE...")
                        engine = GlobalClaimCodePayment(tx.payment_info, tx.discount_info)
                else:
                    log.warn("ERROR!, prepare_payment, PROMO_CODE (not from bank deposit) can not be used with GlobalClaimCodePayment/RewardsPayment...")

                    namespace = 'claim-code-gae'
                    response_code = 'CLAIMCODEINVALIDPROMO'
                    response_message = 'Promo code is not applicable for Claim Code payment method. Please choose another payment method.'
                    tx.error_info['error_msg'] = response_message
                    return 'error', translate_message_via_error_code(namespace, response_code, response_message)
            else:
                log.debug("prepare_payment, initializing PromoCodePayment handler...")

                engine = PromoCodePayment(tx.payment_info)

                if theaterorg_id == str(SM_MALLS):
                    log.debug("prepare_payment, SM Malls settings...")

                    tx.workspace[WS_PAYMENT_SMMALLS_PROMOCODE] = True
                elif theaterorg_id == str(ROCKWELL_MALLS):
                    log.debug("prepare_payment, Rockwell (Power PLant Mall) settings...")

                    tx.workspace[WS_PAYMENT_POWERPLANTMALL_PROMOCODE] = True
                elif theaterorg_id == str(GREENHILLS_MALLS):
                    log.debug("prepare_payment, Greenhills Malls settings...")

                    tx.workspace[WS_PAYMENT_GREENHILLS_PROMOCODE] = True
                elif theaterorg_id == str(CINEMA76_MALLS):
                    log.debug("prepare_payment, Cinema 76 Malls settings...")

                    tx.workspace[WS_PAYMENT_CINEMA76_PROMOCODE] = True
                elif theaterorg_id == str(GLOBE_EVENTS):
                    log.debug("prepare_payment, Globe Events settings...")

                    tx.workspace[WS_PAYMENT_GLOBEEVENTS_PROMOCODE] = True
                elif theaterorg_id == str(ROBINSONS_MALLS):
                    log.debug("prepare_payment, Robinsons Malls settings...")

                    tx.workspace[WS_PAYMENT_ROBINSONS_PROMOCODE] = True
        elif tx.payment_type in [ReservationTransaction.allowed_payment_types.GCASH, ReservationTransaction.allowed_payment_types.GCASH_APP]:
            if tx.discount_info:
                log.debug("prepare_payment, initializing GlobalClaimCodePayment/RewardsPayment handler for GCash/GCash App...")

                if 'claim_code' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key claim_code in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key claim_code in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if 'user_id' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key user_id in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key user_id in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if 'mobile_number' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key mobile_number in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key mobile_number in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if 'claim_code_type' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key claim_code_type in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key claim_code_type in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if 'payment_gateway' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key payment_gateway in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key payment_gateway in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if RUSH_REWARDS_CODE_TYPE == tx.discount_info['claim_code_type']:
                    log.debug("prepare_payment, initializing RewardsPayment handler for GCash...")
                    engine = RewardsPayment(tx.payment_info, tx.discount_info)
                else:
                    log.debug("prepare_payment, initializing GlobalClaimCodePayment handler for GCash...")
                    engine = GlobalClaimCodePayment(tx.payment_info, tx.discount_info)
            else:
                log.debug("prepare_payment, initializing GCashPayment handler...")

                engine = GCashPayment(tx.payment_info)

                if theaterorg_id == str(SM_MALLS):
                    log.debug("prepare_payment, SM Malls settings...")

                    tx.workspace[WS_PAYMENT_SMMALLS_GCASH] = True
                elif theaterorg_id == str(ROCKWELL_MALLS):
                    log.debug("prepare_payment, Rockwell (Power Plant Mall) settings...")

                    tx.workspace[WS_PAYMENT_POWERPLANTMALL_GCASH] = True
                elif theaterorg_id == str(GREENHILLS_MALLS):
                    log.debug("prepare_payment, Greenhills Malls settings...")

                    tx.workspace[WS_PAYMENT_GREENHILLS_GCASH] = True
                elif theaterorg_id == str(CINEMA76_MALLS):
                    log.debug("prepare_payment, Cinema 76 Malls settings...")

                    tx.workspace[WS_PAYMENT_CINEMA76_GCASH] = True
                elif theaterorg_id == str(GLOBE_EVENTS):
                    log.debug("prepare_payment, Globe Events settings...")

                    tx.workspace[WS_PAYMENT_GLOBEEVENTS_GCASH] = True
                elif theaterorg_id == str(AYALA_MALLS):
                    log.debug("prepare_payment, Ayala Malls settings...")

                    tx.workspace[WS_PAYMENT_AYALA_GCASH] = True
                elif theaterorg_id == str(ROBINSONS_MALLS):
                    log.debug("prepare_payment, Robinsons settings...")

                    tx.workspace[WS_PAYMENT_ROBINSONS_GCASH] = True
        elif tx.payment_type == ReservationTransaction.allowed_payment_types.GREWARDS:
            if tx.discount_info:
                log.debug("prepare_payment, initializing GlobalClaimCodePayment/RewardsPayment handler for GRewards App...")

                if 'claim_code' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key claim_code in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key claim_code in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if 'user_id' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key user_id in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key user_id in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if 'mobile_number' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key mobile_number in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key mobile_number in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if 'claim_code_type' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key claim_code_type in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key claim_code_type in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if 'payment_gateway' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key payment_gateway in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key payment_gateway in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if RUSH_REWARDS_CODE_TYPE == tx.discount_info['claim_code_type']:
                    log.debug("prepare_payment, initializing RewardsPayment handler for GRewards...")
                    engine = RewardsPayment(tx.payment_info, tx.discount_info)
                else:
                    log.debug("prepare_payment, initializing GlobalClaimCodePayment handler for GRewards...")
                    engine = GlobalClaimCodePayment(tx.payment_info, tx.discount_info)
            else:
                log.debug("prepare_payment, initializing GRewardsPayment handler...")

                engine = GRewardsPayment(tx.payment_info)

                if theaterorg_id == str(AYALA_MALLS):
                    log.debug("prepare_payment, Ayala Malls settings...")

                    tx.workspace[WS_PAYMENT_AYALA_GREWARDS] = True
        elif tx.payment_type == ReservationTransaction.allowed_payment_types.CLIENT_INITIATED:
            payment_gateway = 'pesopay'

            if tx.discount_info:
                log.debug("prepare_payment, initializing GlobalClaimCodePayment/RewardsPayment handler for CLIENT_INITIATED...")

                if 'claim_code' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key claim_code in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key claim_code in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if 'user_id' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key user_id in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key user_id in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if 'mobile_number' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key mobile_number in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key mobile_number in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if 'claim_code_type' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key claim_code_type in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key claim_code_type in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if 'payment_gateway' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key payment_gateway in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key payment_gateway in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if RUSH_REWARDS_CODE_TYPE == tx.discount_info['claim_code_type']:
                    log.debug("prepare_payment, initializing RewardsPayment handler for CLIENT_INITIATED...")
                    engine = RewardsPayment(tx.payment_info, tx.discount_info)
                else:
                    log.debug("prepare_payment, initializing GlobalClaimCodePayment handler for CLIENT_INITIATED...")
                    engine = GlobalClaimCodePayment(tx.payment_info, tx.discount_info)
            else:
                log.debug("prepare_payment, initializing ClientInitiatedPayment handler...")

                # for raw reservations, use AYALA_MALLS instead.
                if 'is_raw' in tx.workspace and tx.workspace['is_raw']:
                    theater_key = Key(TheaterOrganization, str(AYALA_MALLS))

                if 'payment-gateway' in tx.payment_info:
                    payment_gateway = tx.payment_info.pop('payment-gateway')

                settings = _get_client_initiated_payment_settings(theater_key, payment_gateway, tx.workspace['theaters.cinemas.cinema_name'])
                engine = ClientInitiatedPayment(settings, tx.payment_info)
                tx.workspace[WS_PAYMENT_CLIENT_INITIATED] = True
        elif tx.payment_type == ReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT:
            payment_gateway = 'paynamics'

            if tx.discount_info:
                log.debug("prepare_payment, initializing GlobalClaimCodePayment/RewardsPayment handler for PAYNAMICS_PAYMENT...")

                if 'claim_code' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key claim_code in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key claim_code in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if 'user_id' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key user_id in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key user_id in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if 'mobile_number' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key mobile_number in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key mobile_number in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if 'claim_code_type' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key claim_code_type in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key claim_code_type in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if 'payment_gateway' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key payment_gateway in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key payment_gateway in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if RUSH_REWARDS_CODE_TYPE == tx.discount_info['claim_code_type']:
                    log.debug("prepare_payment, initializing RewardsPayment handler for PAYNAMICS_PAYMENT...")
                    engine = RewardsPayment(tx.payment_info, tx.discount_info)
                else:
                    log.debug("prepare_payment, initializing GlobalClaimCodePayment handler for PAYNAMICS_PAYMENT...")
                    engine = GlobalClaimCodePayment(tx.payment_info, tx.discount_info)
            else:
                log.debug("prepare_payment, initializing PaynamicsPayment handler...")

                if 'payment-gateway' in tx.payment_info:
                    payment_gateway = tx.payment_info.pop('payment-gateway')

                settings = _get_client_initiated_payment_settings(theater_key, payment_gateway)
                if 'token' in tx.payment_info and tx.payment_info['token']:
                    if theaterorg_id in [str(CINEMA76_MALLS), str(SM_MALLS)]:
                        log.debug("prepare_payment, initializing PaynamicsPaymentWithToken3D handler...")
                        engine = PaynamicsPaymentWithToken3D(settings, tx.payment_info)
                        tx.workspace[WS_PAYMENT_3D_TOKEN_PAYNAMICS] = True
                    else:
                        log.debug("prepare_payment, initializing PaynamicsPaymentWithToken handler...")
                        engine = PaynamicsPaymentWithToken(settings, tx.payment_info)
                        tx.workspace[WS_PAYMENT_TOKEN_PAYNAMICS] = True
                else:
                    engine = PaynamicsPayment(settings, tx.payment_info, tx.workspace['theaters.org_id'][0])

                if theaterorg_id == str(ROCKWELL_MALLS):
                    log.debug("prepare_payment, Rockwell (Power Plant Mall) settings...")

                    tx.workspace[WS_PAYMENT_POWERPLANTMALL_PAYNAMICS] = True
                elif theaterorg_id == str(GREENHILLS_MALLS):
                    log.debug("prepare_payment, Greenhills Malls settings...")

                    tx.workspace[WS_PAYMENT_GREENHILLS_PAYNAMICS] = True
                elif theaterorg_id == str(CINEMA76_MALLS):
                    log.debug("prepare_payment, Cinema 76 Malls settings...")

                    tx.workspace[WS_PAYMENT_CINEMA76_PAYNAMICS] = True
                elif theaterorg_id == str(GLOBE_EVENTS):
                    log.debug("prepare_payment, Globe Events settings...")

                    tx.workspace[WS_PAYMENT_GLOBEEVENTS_PAYNAMICS] = True
                elif theaterorg_id == str(SM_MALLS):
                    log.debug("prepare_payment, SM Malls settings...")

                    tx.workspace[WS_PAYMENT_SMMALLS_PAYNAMICS] = True
                elif theaterorg_id == str(ROBINSONS_MALLS):
                    log.debug("prepare_payment, Robinsons Malls settings...")

                    tx.workspace[WS_PAYMENT_ROBINSONS_PAYNAMICS] = True
                else:
                    log.warn("ERROR!, prepare_payment, theater is not supported...")

                    tx.error_info['error_msg'] = "theater is not supported"
                    return 'error', DEFAULT_ERROR_MESSAGE
        elif tx.payment_type == ReservationTransaction.allowed_payment_types.MIGS_PAYMENT:
            payment_gateway = 'migs'

            if tx.discount_info:
                log.debug("prepare_payment, initializing GlobalClaimCodePayment/RewardsPayment handler for MIGS_PAYMENT...")

                if 'claim_code' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key claim_code in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key claim_code in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if 'user_id' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key user_id in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key user_id in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if 'mobile_number' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key mobile_number in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key mobile_number in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if 'claim_code_type' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key claim_code_type in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key claim_code_type in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if 'payment_gateway' not in tx.discount_info:
                    log.warn("ERROR!, prepare_payment, missing key payment_gateway in tx.discount_info...")

                    tx.error_info['error_msg'] = "missing key payment_gateway in tx.discount_info"
                    return 'error', DEFAULT_ERROR_MESSAGE

                if RUSH_REWARDS_CODE_TYPE == tx.discount_info['claim_code_type']:
                    log.debug("prepare_payment, initializing RewardsPayment handler for MIGS_PAYMENT...")
                    engine = RewardsPayment(tx.payment_info, tx.discount_info)
                else:
                    log.debug("prepare_payment, initializing GlobalClaimCodePayment handler for MIGS_PAYMENT...")
                    engine = GlobalClaimCodePayment(tx.payment_info, tx.discount_info)
            else:
                log.debug("prepare_payment, initializing MIGSPayment handler...")

                if 'payment-gateway' in tx.payment_info:
                    payment_gateway = tx.payment_info.pop('payment-gateway')

                settings = _get_client_initiated_payment_settings(theater_key, payment_gateway)
                engine = MIGSPayment(settings, tx.payment_info)

                if theaterorg_id == str(ROBINSONS_MALLS):
                    log.debug("prepare_payment, Robinsons Malls settings...")

                    tx.workspace[WS_PAYMENT_ROBINSONSMALLS_MIGS] = True
                else:
                    log.warn("ERROR!, prepare_payment, theater is not supported...")

                    tx.error_info['error_msg'] = "theater is not supported"
                    return 'error', DEFAULT_ERROR_MESSAGE
        elif tx.payment_type == ReservationTransaction.allowed_payment_types.IPAY88_PAYMENT:
            log.debug("prepare_payment, initializing IPay88Payment handler...")

            if theater_code in ALLOWED_MGI_RESERVATION_BRANCH_CODES:
                remark = 'GMovies (Lucky Chinatown Cinemas)'
                settings = _get_ipay88_payment_settings(tx, LCT_MERCHANT_CODE, remark, LCT_IPAY88_PAYMENT_COMPLETE_URL)
                settings['UserEmail'] = 'luckychinatownonline@gmail.com' # override UserEmail with luckychinatownonline@gmail.com
                settings['Amount'] = str(tx.total_amount(convenience_fee, True))
                settings['MerchantKey'] = LCT_MERCHANT_KEY
                engine = IPay88Payment(settings, tx.payment_info)
                tx.workspace[WS_PAYMENT_MGW_IPAY88_PAYMENT] = True
            else:
                log.warn("ERROR!, prepare_payment, theater_code is not supported...")

                tx.error_info['error_msg'] = "theater_code is not supported"
                return 'error', DEFAULT_ERROR_MESSAGE

        elif tx.payment_type == ReservationTransaction.allowed_payment_types.RESERVE:
            log.debug("prepare_payment, initializing CashPayment handler...")
            
            engine = CashPayment(tx.payment_info)
        else:
            log.warn("ERROR!, prepare_payment, payment_type is not supported...")

            tx.error_info['error_msg'] = "payment_type is not supported"
            return 'error', DEFAULT_ERROR_MESSAGE

        tx.workspace[WS_PAYMENT_ENGINE] = engine
        status, message = engine.do_prepare(tx)
        tx.put()

        return status, message
    except KeyError, e:
        log.warn("ERROR!, prepare_payment, missing key...")
        log.error(str(traceback.format_exc(e)))
        tx.error_info['error_msg'] = str(e)
    except Exception, e:
        log.warn("ERROR!, prepare_payment...")
        log.error(str(traceback.format_exc(e)))
        tx.error_info['error_msg'] = str(e)

    return 'error', DEFAULT_ERROR_MESSAGE

def do_payment(tx):
    engine = tx.workspace[WS_PAYMENT_ENGINE]
    retval = engine.do_payment_tx(tx)
    tx.bind_cancellation_callback(cancel_payment)

    try:
        # GCash, auto-refund if transaction encountered an error in this state.
        if tx.payment_type in [ReservationTransaction.allowed_payment_types.GCASH,
                ReservationTransaction.allowed_payment_types.GCASH_APP]:
            if 'refund::is_for_refund' in tx.workspace and tx.workspace['refund::is_for_refund']:
                engine.do_refund_tx(tx)
    except Exception, e:
        log.warn("ERROR!, do_payment...")
        log.error(e)

    tx.put()

    return retval

def cancel_payment(tx):
    pass

def is_client_initiated(tx):
    return WS_PAYMENT_CLIENT_INITIATED in tx.workspace and tx.workspace[WS_PAYMENT_CLIENT_INITIATED]

def is_ayala_gcash(tx):
    return WS_PAYMENT_AYALA_GCASH in tx.workspace and tx.workspace[WS_PAYMENT_AYALA_GCASH]

def is_ayala_grewards(tx):
    return WS_PAYMENT_AYALA_GREWARDS in tx.workspace and tx.workspace[WS_PAYMENT_AYALA_GREWARDS]

def is_powerplantmall_paynamics(tx):
    return WS_PAYMENT_POWERPLANTMALL_PAYNAMICS in tx.workspace and tx.workspace[WS_PAYMENT_POWERPLANTMALL_PAYNAMICS]

def is_powerplantmall_gcash(tx):
    return WS_PAYMENT_POWERPLANTMALL_GCASH in tx.workspace and tx.workspace[WS_PAYMENT_POWERPLANTMALL_GCASH]

def is_powerplantmall_promocode(tx):
    return WS_PAYMENT_POWERPLANTMALL_PROMOCODE in tx.workspace and tx.workspace[WS_PAYMENT_POWERPLANTMALL_PROMOCODE]

def is_greenhills_paynamics(tx):
    return WS_PAYMENT_GREENHILLS_PAYNAMICS in tx.workspace and tx.workspace[WS_PAYMENT_GREENHILLS_PAYNAMICS]

def is_greenhills_gcash(tx):
    return WS_PAYMENT_GREENHILLS_GCASH in tx.workspace and tx.workspace[WS_PAYMENT_GREENHILLS_GCASH]

def is_greenhills_promocode(tx):
    return WS_PAYMENT_GREENHILLS_PROMOCODE in tx.workspace and tx.workspace[WS_PAYMENT_GREENHILLS_PROMOCODE]

def is_cinema76_paynamics(tx):
    return WS_PAYMENT_CINEMA76_PAYNAMICS in tx.workspace and tx.workspace[WS_PAYMENT_CINEMA76_PAYNAMICS]

def is_cinema76_gcash(tx):
    return WS_PAYMENT_CINEMA76_GCASH in tx.workspace and tx.workspace[WS_PAYMENT_CINEMA76_GCASH]

def is_cinema76_promocode(tx):
    return WS_PAYMENT_CINEMA76_PROMOCODE in tx.workspace and tx.workspace[WS_PAYMENT_CINEMA76_PROMOCODE]

def is_globeevents_paynamics(tx):
    return WS_PAYMENT_GLOBEEVENTS_PAYNAMICS in tx.workspace and tx.workspace[WS_PAYMENT_GLOBEEVENTS_PAYNAMICS]

def is_globeevents_gcash(tx):
    return WS_PAYMENT_GLOBEEVENTS_GCASH in tx.workspace and tx.workspace[WS_PAYMENT_GLOBEEVENTS_GCASH]

def is_globeevents_promocode(tx):
    return WS_PAYMENT_GLOBEEVENTS_PROMOCODE in tx.workspace and tx.workspace[WS_PAYMENT_GLOBEEVENTS_PROMOCODE]

def is_smmalls_paynamics(tx):
    return WS_PAYMENT_SMMALLS_PAYNAMICS in tx.workspace and tx.workspace[WS_PAYMENT_SMMALLS_PAYNAMICS]

def is_smmalls_gcash(tx):
    return WS_PAYMENT_SMMALLS_GCASH in tx.workspace and tx.workspace[WS_PAYMENT_SMMALLS_GCASH]

def is_smmalls_promocode(tx):
    return WS_PAYMENT_SMMALLS_PROMOCODE in tx.workspace and tx.workspace[WS_PAYMENT_SMMALLS_PROMOCODE]

def is_robinsons_paynamics(tx):
    return WS_PAYMENT_ROBINSONS_PAYNAMICS in tx.workspace and tx.workspace[WS_PAYMENT_ROBINSONS_PAYNAMICS]

def is_robinsons_promocode(tx):
    return WS_PAYMENT_ROBINSONS_PROMOCODE in tx.workspace and tx.workspace[WS_PAYMENT_ROBINSONS_PROMOCODE]

def is_robinsonsmalls_migs(tx):
    return WS_PAYMENT_ROBINSONSMALLS_MIGS in tx.workspace and tx.workspace[WS_PAYMENT_ROBINSONSMALLS_MIGS]

def is_robinsons_gcash(tx):
    return WS_PAYMENT_ROBINSONS_GCASH in tx.workspace and tx.workspace[WS_PAYMENT_ROBINSONS_GCASH]

def is_mgw_ipay88_payment(tx):
    return WS_PAYMENT_MGW_IPAY88_PAYMENT in tx.workspace and tx.workspace[WS_PAYMENT_MGW_IPAY88_PAYMENT]

def is_token_paynamics(tx):
    return WS_PAYMENT_TOKEN_PAYNAMICS in tx.workspace and tx.workspace[WS_PAYMENT_TOKEN_PAYNAMICS]

def is_3d_token_paynamics(tx):
    return WS_PAYMENT_3D_TOKEN_PAYNAMICS in tx.workspace and tx.workspace[WS_PAYMENT_3D_TOKEN_PAYNAMICS]

# dynamic settings for PesoPay and Paynamics payment gateway.
@non_transactional
def _get_client_initiated_payment_settings(theater_key, payment_gateway, cinema_name=None):
    return ClientInitiatedPaymentSettings.get_settings(theater_key, payment_gateway, cinema_name)

# hardcoded settings for iPay88 payment gateway.
# FIXME!, do dynamic settings like PesoPay and Paynamics payment gateway.
@non_transactional
def _get_ipay88_payment_settings(tx, merchant_code, remark, backend_url):
    form_parameters = {}
    form_parameters['MerchantCode'] = merchant_code # Mandatory: Will be provided by iPay88
    form_parameters['PaymentId'] = '1' # Optional: Credit Card Payment only
    form_parameters['RefNo'] = '' # Mandatory: Reference Number
    form_parameters['Amount'] = '' # Mandatory: Total Amount of purchased ticket. Amount with two decimals and thousand symbols. Example: 1,278.99
    form_parameters['Currency'] = 'PHP' # Mandatory: Currency(PHP)
    form_parameters['ProdDesc'] = 'Movie Ticket' # Mandatory: Product Description.
    form_parameters['UserName'] = tx.payment_info['first_name'] + ' ' + tx.payment_info['last_name'] # Mandatory: Customer Name
    form_parameters['UserEmail'] = tx.payment_info['email'] # Mandatory: Customer Email Address
    form_parameters['UserContact'] = tx.payment_info['contact_number'] # Mandatory: Customer Contact Number
    form_parameters['Remark'] = remark # Optional: Merchant Remark
    form_parameters['Lang'] = 'UTF-8' # Optional: Language Encoding
    form_parameters['Signature'] = '' # Mandatory: Signature
    form_parameters['BackendURL'] = backend_url # Mandatory: Callback URL

    return form_parameters

@non_transactional
def map_seats_label_to_tx(seat_label_id, seat_id_tx):
    log.debug("map_seats_label_to_tx...")

    return [(idl, idx['transactionCode']) for idi, idl in seat_label_id.iteritems() for idx in seat_id_tx if idi == idx['seatId']]

# Method added for checking key 'is_bank_deposit' of Promo Code
def check_promo_code(promo_code, theaterorg_id=None, theater_id=None):
    params = {}
    log.debug("check_promo_code, promo_code: %s..." % (promo_code))
    params['promo_code'] = promo_code
    params['theaterorg_id'] = theaterorg_id
    params['theater_id'] = theater_id
    log.debug("check_promo_code, params: {0}...".format(params))
    r = requests.get(PROMOCODE_CHECK_ENDPOINT, params=params)

    log.debug("check_promo_code, status_code: %s..." % (r.status_code))
    log.debug("check_promo_code, response: %s..." % (r.text))

    if r.status_code != 200:
        log.warn("ERROR!, check_promo_code, status_code not equal to 200: %s..." % (r.status_code))
        if 400 == r.status_code:
            namespace = 'claim-code-gae'
            response_code = 'ERRORPARAMS'
            response_message = 'Claim code is not valid. Please try again or contact support.'
            return r.status_code, translate_message_via_error_code(namespace, response_code, response_message)
        return 'error', DEFAULT_ERROR_MESSAGE

    d_json = {}
    try:
        d_json = json.loads(r.text)
    except ValueError, e:
        log.warn("ERROR!, check_promo_code, response not in json format...")
        return 'error', DEFAULT_ERROR_MESSAGE
    else:
        if 'return' not in d_json:
            log.warn("ERROR!, check_promo_code, missing key 'return' in json response...")
            return 'error', DEFAULT_ERROR_MESSAGE
        if 'claim_code_details' not in d_json['return']:
            log.warn("ERROR!, check_promo_code, missing key 'claim_code_details' in json response...")
            return 'error', DEFAULT_ERROR_MESSAGE
    return r.status_code, d_json

class CreditCardPayment():
    def __init__(self, payment_info):
        card_number = payment_info['card_number']
        exp_month = payment_info['expiration_month']
        self.cardholder = payment_info['cardholder']
        self.address = payment_info['address']
        self.credit_card = card_number.replace('-', '')
        self.expiration_month = exp_month[-1] if exp_month[0] == '0' else exp_month
        self.expiration_year = payment_info['expiration_year']
        self.cvv = payment_info['cvv']
        self.address = payment_info['address']
        self.email = payment_info['email']

    def do_prepare(self, tx):
        return 'success', None

    def do_payment_tx(self, tx):
        def __send_payment_req(s, req):
            r = s.send(req, timeout=TIMEOUT_DEADLINE)

            return r.status_code, r.text

        param_str = "id=%s&trid=%s&cname=%s&cno=%s&xm=%s&xy=%s&csc=%s"
        param_str = param_str % (tx.workspace['RSVP:customerid'], tx.workspace['RSVP:ref'],
                self.cardholder, self.credit_card, self.expiration_month, self.expiration_year, self.cvv)
        encrypted_value = sureseats_encrypt_params(param_str)

        s = requests.Session()
        req = requests.Request('POST', url=SURESEATS_ENDPOINT_PRIMARY, params={'action': CREDIT_CARD_PAYMENT_ACTION, 'en': encrypted_value}).prepare()
        r_status_code, r_text = call_once(__send_payment_req, tx, s, req)

        if r_status_code != 200:
            log.warn("ERROR! CreditCardPayment, do_payment_tx, %s..." % r_text)

            tx.error_info['error_msg'] = "Cannot perform payment, there was an error calling Sureseats"
            return 'error', 'Cannot perform payment, there was an error calling Sureseats'

        log.debug("Got response: %s" % str(r_text))

        try:
            xml = etree.fromstring(str(r_text))
            status = xml.xpath('//Transaction/Status/text()')[0]

            if status.upper() == 'APPROVED':
                tx.workspace['RSVP:claimcode'] = xml.xpath('//Transaction/ClaimCode/text()')
                tx.workspace['RSVP:claimdate'] = self.convert_date(xml.xpath('//Transaction/ClaimDate/text()'))
                return 'success', None
            else:
                tx.error_info['error_msg'] = status
                return 'error', status

        except etree.XMLSyntaxError, e:
            tx.error_info['error_msg'] = str(e)
            return 'error', 'Please retry'

    def do_refund_tx(self, tx):
        log.warn("SKIP!, No refund process for CreditCardPayment...")

    def convert_date(self, d):
        return datetime.strptime(d, '%m/%d/%Y %I:%M:%S %p')


class MPassPayment():
    def __init__(self, payment_info):
        self.err_message = DEFAULT_ERROR_MESSAGE
        self.username = payment_info['username']
        self.password = payment_info['password']

    def _do_mpass_request(self):
        
        params = {
            "action": "LOGIN", 
            "un": self.username.encode('ascii','ignore'), 
            "pw": self.password.encode('ascii','ignore')
        }
        
        log.debug("app_params :%s" % (params))
        
        r = create_sureseats_request_login(SURESEATS_ENDPOINT_PRIMARY, params)
        
        log.debug("sureseats_endpoint_response :%s" % (r.content))
        
        return r.status_code, r.text

    def do_prepare(self, tx):
        r_status_code, r_text = call_once(self._do_mpass_request, tx)

        if r_status_code != 200:
            log.warn("ERROR!, MPassPayment, do_prepare, MPASS login failed, status_code: %s..." % r_status_code)

            tx.error_info['error_msg'] = "MPASS login failed: username: %s" % self.username
            return 'error', 'Sorry, failed to log-in into M-Pass. Please try again.'

        xml = etree.fromstring(str(r_text))
        status = xml.xpath('/CheckLogin/Status/text()')[0]

        if status != 'SUCCESS':
            log.warn("ERROR!, MPassPayment, do_prepare, MPASS transaction failed, status: %s..." % status)

            namespace = 'mpass'
            response_code = 'FAILED' # default response code to translate the error messages.
            response_message = status

            if status == 'INCORRECT USERNAME OR PASSWORD':
                response_code = 'INCORRECTUSERNAMEPASSWORD'

            tx.error_info['error_msg'] = status
            return 'error', translate_message_via_error_code(namespace, response_code, response_message)

        cust = xml.xpath('/CheckLogin/CustID/text()')[0]
        tx.workspace['MPASS:id'] = cust

        log.debug("MPassPayment, do_prepare, MPASS login, login ID: %s..." % cust)

        return 'success', None

    # empty, because MPass payments are one-step through Sureseats
    def do_payment_tx(self, tx):
        return 'success', None

    def do_refund_tx(self, tx):
        log.warn("SKIP!, No refund process for MPassPayment...")


class ClientInitiatedPayment():
    def __init__(self, settings, payment_info):
        self.err_message = DEFAULT_ERROR_MESSAGE
        self.settings = settings
        self.reference_parameter_key = None
        self.form_method = 'POST'
        self.form_target = settings.settings.get('default-form-target')
        self.form_parameters = dict(settings.default_params)
        merchant_id = settings.settings.get('pesopay::merchant-id')

        if merchant_id:
            self.form_parameters['merchantId'] = merchant_id

        if 'reference-parameter' in payment_info:
            self.reference_parameter_key = payment_info['reference-parameter']

        if 'form-method' in payment_info:
            self.form_method = payment_info['form-method']

        if 'form-target' in payment_info:
            self.form_target = payment_info['form-target']

        if 'form-parameters' in payment_info:
            self.form_parameters = dict(self.form_parameters.items() + payment_info['form-parameters'].items())

    def do_prepare(self, tx):
        return 'success', None

    # default error, since we should be in a different state in the first place.
    # Internal error: Client-initiated payment, but entered TX_PAYMENT_HOLD
    def do_payment_tx(self, tx):
        return 'error', self.err_message

    def do_refund_tx(self, tx):
        log.warn("SKIP!, No refund process for ClientInitiatedPayment...")

    def generate_hash(self, reference):
        merchant_id = self.settings.settings['pesopay::merchant-id']
        hash_secret = self.settings.settings['pesopay::merchant-hash-secret']
        currency_code = self.form_parameters['currCode']
        amount = self.form_parameters['amount']
        payment_type = self.form_parameters['payType']
        seed = [merchant_id, reference, currency_code, amount, payment_type, hash_secret]
        seed_str = '|'.join(seed)
        hash = SHA.new(seed_str).digest()

        return hash.encode('hex')


class PaynamicsPayment():
    def __init__(self, settings, payment_info, theaterorg_id=None):
        self.err_message = DEFAULT_ERROR_MESSAGE
        self.settings = settings
        self.form_method = 'POST'
        self.form_target = settings.settings.get('default-form-target')
        self.form_parameters = dict(settings.default_params)
        self.theaterorg_id = theaterorg_id
        self.merchant_id = settings.settings.get('paynamics::merchant-id')
        self.merchant_name = settings.settings.get('paynamics::merchant-name')
        self.merchant_key = settings.settings.get('paynamics::merchant-key')

        if self.merchant_id:
            self.form_parameters['mid'] = self.merchant_id

        if self.merchant_name:
            self.form_parameters['merchantname'] = self.merchant_name

        if self.merchant_key:
            self.form_parameters['merchantkey'] = self.merchant_key

        if 'form-method' in payment_info:
            self.form_method = payment_info['form-method']

        if 'form-target' in payment_info:
            self.form_target = payment_info['form-target']

        if 'email' in payment_info:
            self.form_parameters['email'] = payment_info['email']

        if 'fname' in payment_info:
            self.form_parameters['fname'] = payment_info['fname']

        if 'lname' in payment_info:
            self.form_parameters['lname'] = payment_info['lname']

        if 'mname' in payment_info:
            self.form_parameters['mname'] = payment_info['mname']

        if 'address1' in payment_info:
            self.form_parameters['address1'] = payment_info['address1']

        if 'address2' in payment_info:
            self.form_parameters['address2'] = payment_info['address2']

        if 'city' in payment_info:
            self.form_parameters['city'] = payment_info['city']

        if 'state' in payment_info:
            self.form_parameters['state'] = payment_info['state']

        if 'country' in payment_info:
            self.form_parameters['country'] = payment_info['country']

        if 'zip' in payment_info:
            self.form_parameters['zip'] = payment_info['zip']

        if 'phone' in payment_info:
            self.form_parameters['phone'] = payment_info['phone']

        if 'mobile' in payment_info:
            self.form_parameters['mobile'] = payment_info['mobile']

        if 'form-parameters' in payment_info:
            self.form_parameters = dict(self.form_parameters.items() + payment_info['form-parameters'].items())

        # if token is not in payment info, add 'tokenize' to save cc info
        if 'token' not in payment_info:
            self.form_parameters['metadata1'] = 'tokenize'

    def do_prepare(self, tx):
        self.theaterorg_id = tx.workspace['theaters.org_id'][0]
        theater_code = tx.workspace['theaters.org_theater_code'][0]

        return 'success', None

    # NOTE: no payment processing here, done externally.
    def do_payment_tx(self, tx):
        theaterorg_id = tx.workspace['theaters.org_id'][0]
        theater_code = tx.workspace['theaters.org_theater_code'][0]

        if 'response_code' not in tx.workspace:
            log.warn("ERROR!, PaynamicsPayment, do_payment_tx, missing key response_code in tx.workspace...")

            tx.error_info['error_msg'] = "missing key response_code in tx.workspace"
            return 'error', self.err_message

        if tx.workspace["response_code"] != 'GR001' and tx.workspace["response_code"] != 'GR002':
            log.warn("ERROR, PaynamicsPayment, do_payment_tx, response_code not GR001 or GR002...")

            namespace = 'paynamics'
            response_code = tx.workspace['response_code']
            response_message = tx.workspace['response_message'] if 'response_message' in tx.workspace else self.err_message

            # Check if Bank Time Out, Trigger reversal
            if tx.workspace["response_code"] == 'GR013':
                log.debug("PaynamicsPayment, do_payment_tx, do_reversal_tx...")
                result, response = self.do_reversal_tx(tx)

            tx.error_info['error_msg'] = response_message
            return 'error', translate_message_via_error_code(namespace, response_code, response_message)

        # do finilize transaction on different cinema partners.
        if theaterorg_id == str(ROCKWELL_MALLS):
            log.debug("PaynamicsPayment, do_payment_tx, Rockwell (Power Plant Mall) finalizing transaction...")

            return do_finalize_transaction(tx)
        elif theaterorg_id == str(GREENHILLS_MALLS):
            log.debug("PaynamicsPayment, do_payment_tx, Greenhills Malls finalizing transaction...")

            return 'success', None
        elif theaterorg_id == str(CINEMA76_MALLS):
            log.debug("PaynamicsPayment, do_payment_tx, Cinema 76 Malls finalizing transaction...")

            return do_finalize_transaction(tx)
        elif theaterorg_id == str(GLOBE_EVENTS):
            log.debug("PaynamicsPayment, do_payment_tx, Globe Events finalizing transaction...")

            return do_finalize_transaction(tx)
        elif theaterorg_id == str(ROBINSONS_MALLS):
            log.debug("PaynamicsPayment, do_payment_tx, Robinsons Malls finalizing transaction...")

            return do_finalize_transaction(tx)
        elif theaterorg_id == str(SM_MALLS):
            log.debug("PaynamicsPayment, do_payment_tx, SM Malls...")

            return do_finalize_transaction(tx)
        else:
            log.warn("ERROR!, PaynamicsPayment, do_payment_tx, theaterorg_id is not supported...")

        tx.error_info['error_msg'] = "PaynamicsPayment, do_payment_tx, theaterorg_id is not supported"
        return 'error', self.err_message

    def do_refund_tx(self, tx):
        log.warn("SKIP!, No refund process for PaynamicsPayment...")

    def do_reversal_tx(self, tx):
        def __send_reversal_req(merchant_id, request_id, ip_address, response_id,
                amount, notification_url, response_url, signature):

            status, r_data = reversal(merchant_id, request_id, ip_address, response_id,
                amount, notification_url, response_url, signature)

            return status, r_data

        # Check tx date if eligible for reversal
        log.debug("PaynamicsPayment, do_reversal_tx, date_created: %s, date_reversal: %s" % (tx.date_created, datetime.now()))
        '%m/%d/%Y'
        dt_created = date.strftime(tx.date_created, '%m/%d/%Y')
        dt_today = date.strftime(date.today(), '%m/%d/%Y')
        if dt_created != dt_today:
            error_message = "Not eligible for auto reversal. Transaction date not equal to reversal date."
            send_email_support(tx, "ERROR in PaynamicsPayment Auto Reversal: " + error_message)
            tx.error_info['reversal_error_msg'] = error_message
            return 'error', error_message

        theaterorg_id = tx.workspace['theaters.org_id'][0]
        amount = tx.workspace['RSVP:totalamount']
        request_id = tx.reservation_reference.split('~')[-1] if str(theaterorg_id) not in [str(SM_MALLS), str(ROBINSONS_MALLS), str(CINEMA76_MALLS), str(GLOBE_EVENTS)] else tx.reservation_reference

        # make request_id unique
        request_id = request_id + "REV"
        # generate signature
        seed = [self.merchant_id, request_id, tx.payment_reference, self.form_parameters['ip_address'],
                self.form_parameters['notification_url'], self.form_parameters['response_url'], amount, self.merchant_key]

        seed_str = ''.join(seed)
        signature = hashlib.sha512(seed_str.encode('utf-8')).hexdigest()

        reversal_status, reversal_response = call_once(__send_reversal_req, tx, self.merchant_id,
                request_id, self.form_parameters['ip_address'], tx.payment_reference,
                amount, self.form_parameters['notification_url'], self.form_parameters['response_url'], signature)

        log.debug("reversal_response")
        log.debug(reversal_response)
        tx.workspace['reversal_response_code'] = reversal_response['response_code'] if 'response_code' in reversal_response else ''
        tx.workspace['reversal_response_message'] = reversal_response['response_message'] if 'response_message' in reversal_response else ''
        tx.workspace['reversal_response_id'] = reversal_response['response_id']

        if reversal_status == 'error':
            log.warn("ERROR!, PaynamicsPayment, do_reversal_tx: %s" % reversal_response['message'])

            # send email to GMovies support if exception errors are encountered
            if 'exception_msg' in reversal_response:
                send_email_support(tx, "ERROR in PaynamicsPayment Auto Reversal: " + reversal_response['exception_msg'])

            tx.error_info['reversal_error_msg'] = reversal_response['exception_msg'] if 'exception_msg' in reversal_response else reversal_response['message']
            return 'error', self.err_message

        # Log response for error details
        tx.error_info['reversal_response_code'] = tx.workspace['reversal_response_code'] if 'reversal_response_code' in tx.workspace else ''
        tx.error_info['reversal_response_message'] = tx.workspace['reversal_response_message'] if 'reversal_response_message' in tx.workspace else ''
        tx.error_info['reversal_response_id'] = tx.workspace['reversal_response_id'] if 'reversal_response_id' in tx.workspace else ''
        tx.put()

        # Send email to support whatever is the result of auto reversal
        send_email_support(tx, "PaynamicsPayment Auto Reversal result: " +
                reversal_response['response_code'] + " " + reversal_response['response_message'], 'INFO')
        return 'success', None

    def generate_orders_payload(self, tx):
        orders_payload = {}
        orders_list = []
        total_amount = tx.workspace['RSVP:totalamount']
        total_seat_price = str(tx.total_seat_price(tx.workspace['RSVP:fee'], True))

        try:
            movie = tx.workspace['schedules.movie'][0]
            theater_cinema_name = tx.workspace['theaters.theater_cinema_name'][0]
            seats = tx.workspace['seats']

            for seat in seats:
                orders_dict = {}
                item_name = '%s (%s, %s)' % (movie.canonical_title, theater_cinema_name, seat)
                orders_dict['itemname'] = item_name
                orders_dict['quantity'] = '1'
                orders_dict['amount'] = total_seat_price
                orders_list.append(orders_dict)

            if tx.workspace['discount::is_discounted']:
                log.debug("PaynamicsPayment, generate_orders_payload, create discount item...")

                orders_discount_dict = {}
                orders_discount_dict['itemname'] = 'Discount'
                orders_discount_dict['quantity'] = '1'
                orders_discount_dict['amount'] = '-%s' % tx.workspace['RSVP:totaldiscount']
                orders_list.append(orders_discount_dict)
        except Exception, e:
            log.warn("ERROR!, PaynamicsPayment, generate_orders_payload, failed to generate orders payload...")
            log.error(e)

            item_name = 'GMovies E-Ticket'
            amount = total_amount
            quantity = '1'

            orders_list.append({'itemname': item_name, 'quantity': quantity, 'amount': amount})

        orders_payload['items'] = orders_list

        return orders_payload

    def generate_xml_string(self):
        root = etree.Element('Request')

        for key, value in self.form_parameters.iteritems():
            xml_tag = etree.Element(key)

            if key == 'orders':
                for orders_key, orders_value in value.iteritems():
                    orders_xml_tag = etree.Element(orders_key)

                    for items in  orders_value:
                        root_items = etree.Element('Items')

                        for items_key, items_value in items.iteritems():
                            items_xml_tag = etree.Element(items_key)
                            items_xml_tag.text = items_value

                            root_items.append(items_xml_tag)

                        orders_xml_tag.append(root_items)

                xml_tag.append(orders_xml_tag)
            elif key == 'merchantname':
                continue
            elif key == 'merchantkey':
                continue
            else:
                xml_tag.text = value

            root.append(xml_tag)

        xml_str = etree.tostring(root)

        log.debug("==========================================================")
        log.debug(xml_str)
        log.debug("==========================================================")

        return xml_str

    def generate_signature(self):
        mid = self.form_parameters['mid']
        request_id = self.form_parameters['request_id']
        ip_address = self.form_parameters['ip_address']
        notification_url = self.form_parameters['notification_url']
        response_url = self.form_parameters['response_url']
        fname = self.form_parameters['fname']
        lname = self.form_parameters['lname']
        mname = self.form_parameters['mname']
        address1 = self.form_parameters['address1']
        address2 = self.form_parameters['address2']
        city = self.form_parameters['city']
        state = self.form_parameters['state']
        country = self.form_parameters['country']
        zip_code = self.form_parameters['zip']
        email = self.form_parameters['email']
        phone = self.form_parameters['phone']
        client_ip = self.form_parameters['client_ip']
        amount = self.form_parameters['amount']
        currency = self.form_parameters['currency']
        secure3d = self.form_parameters['secure3d']
        merchant_key = self.form_parameters['merchantkey']
        secure3d_merchantkey = secure3d + merchant_key

        seed = [mid, request_id, ip_address, notification_url, response_url,
                fname, lname, mname, address1, address2, city, state, country,
                zip_code, email, phone, client_ip, amount, currency,
                secure3d_merchantkey]

        seed_str = ''.join(seed)
        signature = hashlib.sha512(seed_str.encode('utf-8')).hexdigest()

        return signature

    def generate_paymentrequest(self):
        xml_str = self.generate_xml_string()
        encoded_xml_str = xml_str.encode('base64')

        return encoded_xml_str

class PaynamicsPaymentWithToken():
    def __init__(self, settings, payment_info):
        self.err_message = DEFAULT_ERROR_MESSAGE
        self.token_id = payment_info['token'] if 'token' in payment_info else ''
        self.response_id = payment_info['response_id'] if 'response_id' in payment_info else ''

        self.merchant_id = settings.settings.get('paynamics::moto-merchant-id')
        self.merchant_key = settings.settings.get('paynamics::moto-merchant-key')
        self.ip_address = settings.default_params['ip_address']
        self.trx_type = settings.default_params['trxtype']
        self.notification_url = settings.default_params['notification_url']
        self.response_url = settings.default_params['response_url']

        # populate for requery
        self.form_parameters = {}
        if self.merchant_id:
            self.form_parameters['mid'] = self.merchant_id
        if self.merchant_key:
            self.form_parameters['merchantkey'] = self.merchant_key

    def do_prepare(self, tx):
        theaterorg_id = tx.workspace['theaters.org_id'][0]
        theater_code = tx.workspace['theaters.org_theater_code'][0]

        return 'success', None

    def do_payment_tx(self, tx):
        def __send_rebill_with_token_req(merchant_id, request_id, ip_address, response_id, token_id, trx_type,
                amount, notification_url, response_url, signature):

            status, r_data = rebill_with_token(merchant_id, request_id, ip_address, response_id,
                token_id, trx_type, amount, notification_url, response_url, signature)

            return status, r_data

        theaterorg_id = tx.workspace['theaters.org_id'][0]
        theater_code = tx.workspace['theaters.org_theater_code'][0]

        amount = tx.workspace['RSVP:totalamount']
        request_id = tx.reservation_reference.split('~')[-1] if str(theaterorg_id) not in [str(SM_MALLS), str(ROBINSONS_MALLS), str(CINEMA76_MALLS), str(GLOBE_EVENTS)] else tx.reservation_reference

        # generate signature
        seed = [self.merchant_id, request_id, self.response_id, self.token_id, self.ip_address,
                self.notification_url, self.response_url, amount, self.merchant_key]

        seed_str = ''.join(seed)
        signature = hashlib.sha512(seed_str.encode('utf-8')).hexdigest()

        rebill_token_status, rebill_token_response = call_once(__send_rebill_with_token_req, tx, self.merchant_id,
                request_id, self.ip_address, self.response_id, self.token_id, self.trx_type,
                amount, self.notification_url, self.response_url, signature)

        if rebill_token_status == 'error':
            log.warn("ERROR!, PaynamicsPaymentWithToken, do_payment_tx: %s" % rebill_token_response['message'])

            # send email to GMovies support if exception errors are encountered
            if 'exception_msg' in rebill_token_response:
                send_email_support(tx, "ERROR in PaynamicsPaymentWithToken: " + rebill_token_response['exception_msg'])

            tx.error_info['error_msg'] = rebill_token_response['exception_msg'] if 'exception_msg' in rebill_token_response else rebill_token_response['message']
            return 'error', self.err_message

        tx.workspace['response_code'] = rebill_token_response['response_code'] if 'response_code' in rebill_token_response else ''
        tx.workspace['response_message'] = rebill_token_response['response_message'] if 'response_message' in rebill_token_response else ''

        if 'response_code' not in tx.workspace:
            log.warn("ERROR!, PaynamicsPaymentWithToken, do_payment_tx, missing key response_code in tx.workspace...")

            tx.error_info['error_msg'] = "missing key response_code in tx.workspace"
            return 'error', self.err_message

        # Log response for error details
        tx.error_info['response_code'] = tx.workspace['response_code'] if 'response_code' in tx.workspace else ''
        tx.error_info['response_message'] = tx.workspace['response_message'] if 'response_message' in tx.workspace else ''

        tx.payment_reference = rebill_token_response['response_id']

        if tx.workspace["response_code"] != 'GR001' and tx.workspace["response_code"] != 'GR002':
            log.warn("ERROR, PaynamicsPaymentWithToken, do_payment_tx, response_code not GR001 or GR002...")

            namespace = 'paynamics'
            response_code = tx.workspace['response_code']
            response_message = tx.workspace['response_message'] if 'response_message' in tx.workspace else self.err_message

            # Check if Bank Time Out, Trigger reversal
            if tx.workspace["response_code"] == 'GR013':
                log.debug("PaynamicsPaymentWithToken, do_payment_tx, do_reversal_tx...")
                result, response = self.do_reversal_tx(tx)

            tx.error_info['error_msg'] = response_message
            return 'error', translate_message_via_error_code(namespace, response_code, response_message)

        # do finilize transaction on different cinema partners.
        if theaterorg_id == str(ROCKWELL_MALLS):
            log.debug("PaynamicsPaymentWithToken, do_payment_tx, Rockwell (Power Plant Mall) finalizing transaction...")

            return do_finalize_transaction(tx)
        elif theaterorg_id == str(GREENHILLS_MALLS):
            log.debug("PaynamicsPaymentWithToken, do_payment_tx, Greenhills Malls finalizing transaction...")

            return do_finalize_transaction(tx)
        elif theaterorg_id == str(CINEMA76_MALLS):
            log.debug("PaynamicsPaymentWithToken, do_payment_tx, Cinema 76 Malls finalizing transaction...")

            return do_finalize_transaction(tx)
        elif theaterorg_id == str(GLOBE_EVENTS):
            log.debug("PaynamicsPaymentWithToken, do_payment_tx, Globe Events finalizing transaction...")

            return do_finalize_transaction(tx)
        elif theaterorg_id == str(ROBINSONS_MALLS):
            log.debug("PaynamicsPaymentWithToken, do_payment_tx, Robinsons Malls finalizing transaction...")

            return do_finalize_transaction(tx)
        elif theaterorg_id == str(SM_MALLS):
            log.debug("PaynamicsPaymentWithToken, do_payment_tx, SM Malls...")

            return do_finalize_transaction(tx)
        else:
            log.warn("ERROR!, PaynamicsPaymentWithToken, do_payment_tx, theaterorg_id is not supported...")

        tx.error_info['error_msg'] = "PaynamicsPaymentWithToken, do_payment_tx, theaterorg_id is not supported"
        return 'error', self.err_message

    def do_reversal_tx(self, tx):
        def __send_reversal_req(merchant_id, request_id, ip_address, response_id,
                amount, notification_url, response_url, signature):

            status, r_data = reversal(merchant_id, request_id, ip_address, response_id,
                amount, notification_url, response_url, signature)

            return status, r_data

        # Check tx date if eligible for reversal
        log.debug("PaynamicsPaymentWithToken, do_reversal_tx, date_created: %s, date_reversal: %s" % (tx.date_created, datetime.now()))
        '%m/%d/%Y'
        dt_created = date.strftime(tx.date_created, '%m/%d/%Y')
        dt_today = date.strftime(date.today(), '%m/%d/%Y')
        if dt_created != dt_today:
            error_message = "Not eligible for auto reversal. Transaction date not equal to reversal date."
            send_email_support(tx, "ERROR in PaynamicsPaymentWithToken Auto Reversal: " + error_message)
            tx.error_info['reversal_error_msg'] = error_message
            return 'error', error_message

        theaterorg_id = tx.workspace['theaters.org_id'][0]
        amount = tx.workspace['RSVP:totalamount']
        request_id = tx.reservation_reference.split('~')[-1] if str(theaterorg_id) not in [str(SM_MALLS), str(ROBINSONS_MALLS), str(CINEMA76_MALLS), str(GLOBE_EVENTS)] else tx.reservation_reference

        # make request_id unique
        request_id = request_id + "REV"
        # generate signature
        seed = [self.merchant_id, request_id, self.response_id, self.ip_address,
                self.notification_url, self.response_url, amount, self.merchant_key]

        seed_str = ''.join(seed)
        signature = hashlib.sha512(seed_str.encode('utf-8')).hexdigest()

        reversal_status, reversal_response = call_once(__send_reversal_req, tx, self.merchant_id,
                request_id, self.ip_address, self.response_id,
                amount, self.notification_url, self.response_url, signature)

        tx.workspace['reversal_response_code'] = reversal_response['response_code'] if 'response_code' in reversal_response else ''
        tx.workspace['reversal_response_message'] = reversal_response['response_message'] if 'response_message' in reversal_response else ''
        tx.workspace['reversal_response_id'] = reversal_response['response_id']

        if reversal_status == 'error':
            log.warn("ERROR!, PaynamicsPaymentWithToken, do_reversal_tx: %s" % reversal_response['message'])

            # send email to GMovies support if exception errors are encountered
            if 'exception_msg' in reversal_response:
                send_email_support(tx, "ERROR in PaynamicsPaymentWithToken Auto Reversal: " + reversal_response['exception_msg'])

            tx.error_info['reversal_error_msg'] = reversal_response['exception_msg'] if 'exception_msg' in reversal_response else reversal_response['message']
            return 'error', self.err_message

        # Log response for error details
        tx.error_info['reversal_response_code'] = tx.workspace['reversal_response_code'] if 'reversal_response_code' in tx.workspace else ''
        tx.error_info['reversal_response_message'] = tx.workspace['reversal_response_message'] if 'reversal_response_message' in tx.workspace else ''
        tx.error_info['reversal_response_id'] = tx.workspace['reversal_response_id'] if 'reversal_response_id' in tx.workspace else ''
        tx.put()

        # Send email to support whatever is the result of auto reversal
        send_email_support(tx, "PaynamicsPaymentWithToken Auto Reversal result: " +
                reversal_response['response_code'] + " " + reversal_response['response_message'], 'INFO')
        return 'success', None

class PaynamicsPaymentWithToken3D():
    def __init__(self, settings, payment_info):
        self.err_message = DEFAULT_ERROR_MESSAGE
        self.token_id = payment_info['token'] if 'token' in payment_info else ''
        self.response_id = payment_info['response_id'] if 'response_id' in payment_info else ''
        self.secure3d_policy = 'ON' # possible values ON/OFF/CUSTOM

        self.merchant_id = settings.settings.get('paynamics::3d-token-merchant-id')
        self.merchant_key = settings.settings.get('paynamics::3d-token-merchant-key')
        self.ip_address = settings.default_params['ip_address']
        self.trx_type = settings.default_params['trxtype']
        self.notification_url = settings.default_params['notification_url']
        self.response_url = settings.default_params['response_url']
        self.theaterorg_id = ''
        self.theater_code = ''
        self.form_target = ''
        self.form_method = 'POST'

        # populate for requery
        self.form_parameters = {}
        if self.merchant_id:
            self.form_parameters['mid'] = self.merchant_id
        if self.merchant_key:
            self.form_parameters['merchantkey'] = self.merchant_key

    def do_prepare(self, tx):
        self.theaterorg_id = tx.workspace['theaters.org_id'][0]
        self.theater_code = tx.workspace['theaters.org_theater_code'][0]

        # send first request rebill_with_token_3d
        # wait for the response
        # response should be GR033
        # if GR033 redirect to secure page
            # wait for final callback
        # else cancel requery

        return 'success', None

    # NOTE: no payment processing here, done externally.
    def do_payment_tx(self, tx):
        if 'response_code' not in tx.workspace:
            log.warn("ERROR!, PaynamicsPaymentWithToken3D, do_payment_tx, missing key response_code in tx.workspace...")

            tx.error_info['error_msg'] = "missing key response_code in tx.workspace"
            return 'error', self.err_message

        if tx.workspace["response_code"] != 'GR001' and tx.workspace["response_code"] != 'GR002':
            log.warn("ERROR, PaynamicsPaymentWithToken3D, do_payment_tx, response_code not GR001 or GR002...")

            namespace = 'paynamics'
            response_code = tx.workspace['response_code']
            response_message = tx.workspace['response_message'] if 'response_message' in tx.workspace else self.err_message

            # Check if Bank Time Out, Trigger reversal
            if tx.workspace["response_code"] == 'GR013':
                log.debug("PaynamicsPaymentWithToken3D, do_payment_tx, do_reversal_tx...")
                result, response = self.do_reversal_tx(tx)

            tx.error_info['error_msg'] = response_message
            return 'error', translate_message_via_error_code(namespace, response_code, response_message)

        # do finilize transaction on different cinema partners.
        if self.theaterorg_id == str(ROCKWELL_MALLS):
            log.debug("PaynamicsPaymentWithToken3D, do_payment_tx, Rockwell (Power Plant Mall) finalizing transaction...")

            return do_finalize_transaction(tx)
        elif self.theaterorg_id == str(GREENHILLS_MALLS):
            log.debug("PaynamicsPaymentWithToken3D, do_payment_tx, Greenhills Malls finalizing transaction...")

            return 'success', None
        elif self.theaterorg_id == str(CINEMA76_MALLS):
            log.debug("PaynamicsPaymentWithToken3D, do_payment_tx, Cinema 76 Malls finalizing transaction...")

            return do_finalize_transaction(tx)
        elif self.theaterorg_id == str(GLOBE_EVENTS):
            log.debug("PaynamicsPaymentWithToken3D, do_payment_tx, Globe Events finalizing transaction...")

            return do_finalize_transaction(tx)
        elif self.theaterorg_id == str(ROBINSONS_MALLS):
            log.debug("PaynamicsPaymentWithToken3D, do_payment_tx, Robinsons Malls finalizing transaction...")

            return do_finalize_transaction(tx)
        elif self.theaterorg_id == str(SM_MALLS):
            log.debug("PaynamicsPaymentWithToken3D, do_payment_tx, SM Malls...")

            return do_finalize_transaction(tx)
        else:
            log.warn("ERROR!, PaynamicsPaymentWithToken3D, do_payment_tx, theaterorg_id is not supported...")

        tx.error_info['error_msg'] = "PaynamicsPaymentWithToken3D, do_payment_tx, theaterorg_id is not supported"
        return 'error', self.err_message

    # initial SOAP call for 3d tokenization processing
    def prepare_paynamics_tokenization_3d(self, tx):
        log.debug("PaynamicsPaymentWithToken3D, prepare_paynamics_tokenization_3d ...")

        def __send_rebill_with_token_3d_req(merchant_id, request_id, ip_address, response_id, token_id, trx_type,
                amount, notification_url, response_url, secure3d_policy, signature):
            status, r_data = rebill_with_token_3d(merchant_id, request_id, ip_address, response_id,
                token_id, trx_type, amount, notification_url, response_url, secure3d_policy, signature)
            return status, r_data

        amount = tx.workspace['RSVP:totalamount']
        request_id = tx.reservation_reference.split('~')[-1] if str(self.theaterorg_id) not in [str(SM_MALLS), str(ROBINSONS_MALLS), str(CINEMA76_MALLS), str(GLOBE_EVENTS)] else tx.reservation_reference

        log.debug("PaynamicsPaymentWithToken3D, prepare_paynamics_tokenization_3d, seed...")
        log.debug("%s, %s, %s, %s, %s, %s, %s, %s, %s, %s" % (self.merchant_id, request_id, self.response_id,
                self.token_id, self.ip_address, self.notification_url, self.response_url,
                amount, self.secure3d_policy, self.merchant_key))
        # generate signature
        seed = [self.merchant_id, request_id, self.response_id, self.token_id, self.ip_address,
                self.notification_url, self.response_url, amount, self.secure3d_policy, self.merchant_key]

        seed_str = ''.join(seed)
        signature = hashlib.sha512(seed_str.encode('utf-8')).hexdigest()
        log.debug("PaynamicsPaymentWithToken3D, signature: %s..." % (signature))

        rebill_token_status, rebill_token_response = call_once(__send_rebill_with_token_3d_req, tx, self.merchant_id,
                request_id, self.ip_address, self.response_id, self.token_id, self.trx_type,
                amount, self.notification_url, self.response_url, self.secure3d_policy, signature)

        if rebill_token_status == 'error':
            log.warn("ERROR!, PaynamicsPaymentWithToken3D, prepare_paynamics_tokenization_3d: %s" % rebill_token_response['message'])

            # send email to GMovies support if exception errors are encountered
            if 'exception_msg' in rebill_token_response:
                send_email_support(tx, "ERROR in PaynamicsPaymentWithToken3D, prepare_paynamics_tokenization_3d: " + rebill_token_response['exception_msg'])

            tx.error_info['error_msg'] = rebill_token_response['exception_msg'] if 'exception_msg' in rebill_token_response else rebill_token_response['message']
            return 'error', self.err_message

        tx.workspace['response_code'] = rebill_token_response['response_code'] if 'response_code' in rebill_token_response else ''
        tx.workspace['response_message'] = rebill_token_response['response_message'] if 'response_message' in rebill_token_response else ''

        if 'response_code' not in tx.workspace:
            log.warn("ERROR!, PaynamicsPaymentWithToken3D, prepare_paynamics_tokenization_3d, missing key response_code in tx.workspace...")

            tx.error_info['error_msg'] = "missing key response_code in tx.workspace"
            return 'error', self.err_message

        # Log response for error details
        tx.error_info['response_code'] = tx.workspace['response_code'] if 'response_code' in tx.workspace else ''
        tx.error_info['response_message'] = tx.workspace['response_message'] if 'response_message' in tx.workspace else ''

        # For 3D tokenization, initial response should be GR033 and with securePageRedirect parameter
        tx.payment_reference = rebill_token_response['response_id']
        if tx.workspace["response_code"] != 'GR033':
            log.warn("ERROR, PaynamicsPaymentWithToken3D, prepare_paynamics_tokenization_3d, response_code not GR033...")

            namespace = 'paynamics'
            response_code = tx.workspace['response_code']
            response_message = tx.workspace['response_message'] if 'response_message' in tx.workspace else self.err_message

            tx.error_info['error_msg'] = response_message
            return 'error', translate_message_via_error_code(namespace, response_code, response_message)

        if 'secure_page_redirect' not in rebill_token_response:
            log.warn("ERROR, PaynamicsPaymentWithToken3D, prepare_paynamics_tokenization_3d, secure_page_redirect not in response...")

            namespace = 'paynamics'
            response_code = tx.workspace['response_code']
            response_message = tx.workspace['response_message'] if 'response_message' in tx.workspace else self.err_message

            tx.error_info['error_msg'] = response_message
            return 'error', translate_message_via_error_code(namespace, response_code, response_message)

        tx.workspace['secure_page_redirect'] = rebill_token_response['secure_page_redirect']
        self.form_target = tx.workspace['secure_page_redirect']
        log.debug("PaynamicsPaymentWithToken3D, prepare_paynamics_tokenization_3d, form_target: %s ..." % (self.form_target))
        return 'success', None

    def do_reversal_tx(self, tx):
        def __send_reversal_req(merchant_id, request_id, ip_address, response_id,
                amount, notification_url, response_url, signature):

            status, r_data = reversal(merchant_id, request_id, ip_address, response_id,
                amount, notification_url, response_url, signature)

            return status, r_data

        # Check tx date if eligible for reversal
        log.debug("PaynamicsPaymentWithToken3D, do_reversal_tx, date_created: %s, date_reversal: %s" % (tx.date_created, datetime.now()))
        '%m/%d/%Y'
        dt_created = date.strftime(tx.date_created, '%m/%d/%Y')
        dt_today = date.strftime(date.today(), '%m/%d/%Y')
        if dt_created != dt_today:
            error_message = "Not eligible for auto reversal. Transaction date not equal to reversal date."
            send_email_support(tx, "ERROR in PaynamicsPaymentWithToken3D Auto Reversal: " + error_message)
            tx.error_info['reversal_error_msg'] = error_message
            return 'error', error_message

        theaterorg_id = tx.workspace['theaters.org_id'][0]
        amount = tx.workspace['RSVP:totalamount']
        request_id = tx.reservation_reference.split('~')[-1] if str(theaterorg_id) not in [str(SM_MALLS), str(ROBINSONS_MALLS), str(CINEMA76_MALLS), str(GLOBE_EVENTS)] else tx.reservation_reference

        # make request_id unique
        request_id = request_id + "REV"
        # generate signature
        seed = [self.merchant_id, request_id, self.response_id, self.ip_address,
                self.notification_url, self.response_url, amount, self.merchant_key]

        seed_str = ''.join(seed)
        signature = hashlib.sha512(seed_str.encode('utf-8')).hexdigest()

        reversal_status, reversal_response = call_once(__send_reversal_req, tx, self.merchant_id,
                request_id, self.ip_address, self.response_id,
                amount, self.notification_url, self.response_url, signature)

        tx.workspace['reversal_response_code'] = reversal_response['response_code'] if 'response_code' in reversal_response else ''
        tx.workspace['reversal_response_message'] = reversal_response['response_message'] if 'response_message' in reversal_response else ''
        tx.workspace['reversal_response_id'] = reversal_response['response_id']

        if reversal_status == 'error':
            log.warn("ERROR!, PaynamicsPaymentWithToken3D, do_reversal_tx: %s" % reversal_response['message'])

            # send email to GMovies support if exception errors are encountered
            if 'exception_msg' in reversal_response:
                send_email_support(tx, "ERROR in PaynamicsPaymentWithToken3D Auto Reversal: " + reversal_response['exception_msg'])

            tx.error_info['reversal_error_msg'] = reversal_response['exception_msg'] if 'exception_msg' in reversal_response else reversal_response['message']
            return 'error', self.err_message

        # Log response for error details
        tx.error_info['reversal_response_code'] = tx.workspace['reversal_response_code'] if 'reversal_response_code' in tx.workspace else ''
        tx.error_info['reversal_response_message'] = tx.workspace['reversal_response_message'] if 'reversal_response_message' in tx.workspace else ''
        tx.error_info['reversal_response_id'] = tx.workspace['reversal_response_id'] if 'reversal_response_id' in tx.workspace else ''
        tx.put()

        # Send email to support whatever is the result of auto reversal
        send_email_support(tx, "PaynamicsPaymentWithToken3D Auto Reversal result: " +
                reversal_response['response_code'] + " " + reversal_response['response_message'], 'INFO')
        return 'success', None

class PromoCodePayment():
    def __init__(self, payment_info):

        self.err_message = DEFAULT_ERROR_MESSAGE
        self.promo_code = payment_info['promo_code']
        self.mobile = payment_info['mobile']
        self.platform = ''
        self.seat_count = ''
        self.seat_price = ''
        self.total_amount = ''
        self.movie = ''
        self.movie_id = ''
        self.theaterorg_id = ''
        self.theater_id = ''
        self.cinema = ''
        self.schedule = ''
        self.seats = ''
        self.discounted_amount = ''
        self.is_discounted = False
        self.discounted_seats = ''
        self.is_postpaid = payment_info['is_postpaid'] if 'is_postpaid' in payment_info else False

    def _do_promocode_check(self):
        log.debug("PromoCodePayment, _do_promocode_check, checking promo_code from %s..." % PROMOCODE_ENDPOINT)

        params = {}
        params['promo_code'] = self.promo_code
        params['seat_count'] = self.seat_count
        params['seat_price'] = self.seat_price
        params['total_amount'] = self.discounted_amount if self.is_discounted else self.total_amount
        params['theaterorg_id'] = self.theaterorg_id
        log.debug("PromoCodePayment, _do_promocode_check, params: {0}...".format(params))

        r = requests.get(PROMOCODE_ENDPOINT, params=params)

        log.debug("PromoCodePayment, _do_promocode_check, status_code: {0}, response: {1}...".format(r.status_code, r.text))

        return r.status_code, r.headers['content-type'], r.text

    def _do_promocode_redeem_prepare(self):
        log.debug("PromoCodePayment, _do_promocode_redeem_prepare, redeeming promo_code from %s..." % PROMOCODE_ENDPOINT)

        payload = {}
        payload['promo_code'] = self.promo_code
        payload['seat_count'] = self.seat_count
        payload['seat_price'] = self.seat_price
        payload['total_amount'] = self.discounted_amount if self.is_discounted else self.total_amount
        payload['movie'] = self.movie
        payload['movie_id'] = self.movie_id
        payload['theaterorg_id'] = self.theaterorg_id
        payload['theater_id'] = self.theater_id
        payload['cinema'] = self.cinema
        payload['schedule'] = self.schedule
        payload['seats'] = self.seats
        payload['mobnum'] = self.mobile
        payload['platform'] = self.platform
        payload['is_postpaid'] = self.is_postpaid
        log.debug("PromoCodePayment, _do_promocode_redeem_prepare, payload: {0}...".format(payload))


        str_payload = json.dumps(payload)
        headers = {'Content-type': 'application/json'}
        req = requests.Request('POST', url=PROMOCODE_ENDPOINT, headers=headers, data=str_payload).prepare()

        return req

    def _do_promocode_redeem(self, s, req):
        r = s.send(req, timeout=TIMEOUT_DEADLINE)

        return r.status_code, r.headers['content-type'], r.text

    def _do_promocode_verify(self):
        log.debug("PromoCodePayment, _do_promocode_verify, verifying promo_code from %s..." % PROMOCODE_VERIFY_ENDPOINT)

        params = {}
        params['promo_code'] = self.promo_code
        params['seat_count'] = self.seat_count
        params['total_amount'] = self.discounted_amount if self.is_discounted else self.total_amount
        params['movie_id'] = self.movie_id
        params['theaterorg_id'] = self.theaterorg_id
        params['theater_id'] = self.theater_id
        params['mobnum'] = self.mobile
        params['platform'] = self.platform
        params['is_postpaid'] = self.is_postpaid
        
        log.debug("PromoCodePayment, _do_promocode_verify, payload: {0}...".format(params))

        r = requests.get(PROMOCODE_VERIFY_ENDPOINT, params=params)

        log.debug("PromoCodePayment, _do_promocode_verify, status_code: {0}, response: {1}...".format(r.status_code, r.text))

        return r.status_code, r.headers['content-type'], r.text

    def _cancellation_callback(self, tx):
        call_once(self._do_promocode_reset, tx)

    def _update_callback(self, tx, same_reservation, same_payment):
        if not same_payment:
            call_once(self._do_promocode_reset, tx)

    def _do_promocode_reset(self):
        log.debug("PromoCodePayment, _do_promocode_reset, resetting promo_code %s with mobnum %s, from %s..." % (self.promo_code, self.mobile, PROMOCODE_RESET_ENDPOINT))

        r = requests.get(PROMOCODE_RESET_ENDPOINT, params={'promo_code': self.promo_code, 'mobnum': self.mobile})

        log.debug("PromoCodePayment, _do_promocode_reset, status_code: %s..." % r.status_code)

        return r.status_code

    def do_prepare(self, tx):
        self.seat_price = str(tx.workspace['schedules.price'][0])
        self.total_amount = str(tx.total_amount('0.00', True))
        self.movie = tx.workspace['schedules.movie'][0].canonical_title
        self.movie_id = tx.workspace['schedules.movie'][0].key.id()
        self.theaterorg_id = str(tx.workspace['theaters.org_id'][0])
        self.theater_id = str(tx.workspace['theaters.key'][0].id())
        self.cinema = tx.workspace['theaters.theater_cinema_name'][0]
        self.schedule = tx.workspace['schedules.show_datetime'][0].strftime("%Y-%m-%dT%H:%M:%S")
        self.seats = tx.workspace['seats::imploded']
        self.is_discounted = tx.workspace['discount::is_discounted']
        self.discounted_amount = tx.workspace['discount::total_amount'] if self.is_discounted else 0
        self.discounted_seats = tx.workspace['discount::actual_discounted_seats'] if self.is_discounted else 0
        orig_seat_count = tx.reservation_count()
        self.seat_count =  (orig_seat_count - self.discounted_seats) if self.is_discounted else orig_seat_count
        self.platform = tx.platform

        rv_status_code, rv_content_type, rv_text = call_once(self._do_promocode_verify, tx)

        tx.workspace['response_code'] = rv_status_code
        tx.workspace['response_message'] = rv_text
        if rv_status_code != 200:
            if rv_content_type == 'text/plain':
                namespace = 'claim-code-gae'
                response_code = 'INVALIDCODES' # default response code to translate the error messages.
                response_message = rv_text

                if rv_text == 'Error in parameters.':
                    response_code = 'ERRORPARAMS'

                tx.workspace['response_code'] = response_code
                tx.workspace['response_message'] = response_message

                resp_msg = translate_message_via_error_code(namespace, response_code, response_message)

                # check return of verify
                d_json = {}
                try:
                    d_json = json.loads(rv_text)
                except ValueError, e:
                    log.warn("PromoCodePayment, do_prepare, _do_promocode_verify, response not in json format...")
                    tx.error_info['error_msg'] = "_do_promocode_verify, response not in json format"
                    return 'error', DEFAULT_ERROR_MESSAGE
                else:
                    if 'response' not in d_json:
                        log.warn("PromoCodePayment, do_prepare, _do_promocode_verify, missing key 'response' in json response...")
                        tx.error_info['error_msg'] = "_do_promocode_verify, missing key 'response' in json response"
                        return 'error', DEFAULT_ERROR_MESSAGE
                    if 'message' not in d_json['response']:
                        log.warn("PromoCodePayment, do_prepare, _do_promocode_verify, missing key 'message' in json response...")
                        tx.error_info['error_msg'] = "_do_promocode_verify, missing key 'message' in json response"
                        return 'error', DEFAULT_ERROR_MESSAGE
                    response_message = d_json['response']['message']
                    response_code = d_json['response']['message_code'] if 'message_code' in d_json['response'] else ''
                    tx.workspace['response_code'] = response_code
                    tx.workspace['response_message'] = response_message
                    resp_msg = response_message

                tx.error_info['error_msg'] = response_message
                return 'error', resp_msg

            tx.error_info['error_msg'] = rv_text
            return 'error', self.err_message

        s = requests.Session()
        req = self._do_promocode_redeem_prepare()
        r_status_code, r_content_type, r_text = call_once(self._do_promocode_redeem, tx, s, req)

        
        log.info("PromoCodePayment, do_prepare, convenience fee: {}".format(str(tx.workspace['convenience_fee'])))

        tx.workspace['response_code'] = r_status_code
        tx.workspace['response_message'] = r_text
        if r_status_code != 200:
            if r_content_type == 'text/plain':
                namespace = 'claim-code-gae'
                response_code = 'INVALIDCODES' # default response code to translate the error messages.
                response_message = r_text

                if r_text == 'Error in parameters.':
                    response_code = 'ERRORPARAMS'

                tx.workspace['response_code'] = response_code
                tx.workspace['response_message'] = response_message

                tx.error_info['error_msg'] = response_message
                return 'error', translate_message_via_error_code(namespace, response_code, response_message)

            tx.error_info['error_msg'] = r_text
            return 'error', self.err_message

        tx.bind_cancellation_callback(self._cancellation_callback)
        tx.bind_update_callback(self._update_callback)

        if r_content_type == 'text/plain':
            tx.workspace['msg'] = r_text

        if self.theaterorg_id == str(AYALA_MALLS):

            p_username = PROMOCODE_USERNAME
            p_password = PROMOCODE_PASSWORD

            log.debug("PromoCodePayment, do_prepare, Ayala Malls...")
            log.debug("PromocodePayment, check claim code mpass wallet account endpoint... %s" % (PROMOCODE_MPASS_ENDPOINT))
            
            req = requests.get(PROMOCODE_MPASS_ENDPOINT, headers={'CLAIMCODE-MPASS': self.promo_code})
            
            wallet = req.content
            wallet = wallet.replace("'",'"')
            wallet = wallet.replace('u"','"')

            log.debug("api req: %s" % (wallet))

            wallet = json.loads(wallet)

            log.debug("api req decoded: %s" % (wallet))

            if req.status_code is 200:

                log.debug("api status code response: %s" % (req.status_code))
                
                if wallet['status'] is not 0:

                    log.debug("username: %s" % (wallet['data']['username']))
                    log.debug("password: %s" % (wallet['data']['password']))

                    p_username = str(wallet['data']['username'])
                    p_password = str(wallet['data']['password'])

            else:
                log.debug("PromocodePayment, Endpoint %s is unreachable, response_code: %s" % (PROMOCODE_MPASS_ENDPOINT, req.status_code))

            self.payment_engine = MPassPayment({'username' : p_username, 'password' : p_password})
        elif self.theaterorg_id == str(SM_MALLS):
            log.debug("PromoCodePayment, do_prepare, SM Malls...")

            self.payment_engine = EPlusPayment({'card_number': EPLUS_CARDNUMBER, 'pin': EPLUS_PIN})
        elif self.theaterorg_id == str(ROCKWELL_MALLS):
            log.debug("PromoCodePayment, do_prepare, Rockwell (Power Plant Mall)...")

            return 'success', None
        elif self.theaterorg_id == str(GREENHILLS_MALLS):
            log.debug("PromoCodePayment, do_prepare, Greenhills Malls...")

            return 'success', None
        elif self.theaterorg_id == str(CINEMA76_MALLS):
            log.debug("PromoCodePayment, do_prepare, Cinema 76 Malls...")

            return 'success', None
        elif self.theaterorg_id == str(GLOBE_EVENTS):
            log.debug("PromoCodePayment, do_prepare, Globe Events...")

            return 'success', None
        elif self.theaterorg_id == str(ROBINSONS_MALLS):
            log.debug("PromoCodePayment, do_prepare, Robinsons Malls...")

            return 'success', None
        else:
            log.warn("ERROR!, PromoCodePayment, do_prepare, theaterorg_id is not supported...")

            tx.error_info['error_msg'] = "PromoCodePayment, do_prepare, theaterorg_id is not supported"
            return 'error', self.err_message

        return self.payment_engine.do_prepare(tx)

    def do_payment_tx(self, tx):
        if self.theaterorg_id == str(AYALA_MALLS):
            log.debug("PromoCodePayment, do_payment_tx, Ayala Malls...")

            return self.payment_engine.do_payment_tx(tx)
        elif self.theaterorg_id == str(SM_MALLS):
            log.debug("PromoCodePayment, do_payment_tx, SM Malls...")

            return do_finalize_transaction(tx)
        elif self.theaterorg_id == str(ROCKWELL_MALLS):
            log.debug("PromoCodePayment, do_payment_tx, Rockwell (Power Plant Mall)...")

            return do_finalize_transaction(tx)
        elif self.theaterorg_id == str(GREENHILLS_MALLS):
            log.debug("PromoCodePayment, do_payment_tx, Greenhills Malls...")

            return do_finalize_transaction(tx)
        elif self.theaterorg_id == str(CINEMA76_MALLS):
            log.debug("PromoCodePayment, do_payment_tx, Cinema 76 Malls...")

            return do_finalize_transaction(tx)
        elif self.theaterorg_id == str(GLOBE_EVENTS):
            log.debug("PromoCodePayment, do_payment_tx, Globe Events...")

            return do_finalize_transaction(tx)
        elif self.theaterorg_id == str(ROBINSONS_MALLS):
            log.debug("PromoCodePayment, do_payment_tx, Robinsons Malls...")

            return do_finalize_transaction(tx)
        else:
            log.warn("ERROR!, PromoCodePayment, do_payment_tx, theaterorg_id is not supported...")
            tx.error_info['error_msg'] = "PromoCodePayment, do_payment_tx, theaterorg_id is not supported"

        return 'error', self.err_message

    def do_refund_tx(self, tx):
        log.warn("SKIP!, No refund process for PromoCodePayment...")


# NOTE: this handler will be used for credit-card holder promos.
class DiscountedCreditCardPayment():
    def __init__(self, payment_info, discount_info):
        self.err_message = DEFAULT_ERROR_MESSAGE
        self.device_type = ''
        self.payment_type = ''
        self.org_uuid = ''
        self.theater_id = ''
        self.seat_count = ''
        self.movie_id = ''
        self.claim_code = discount_info['claim_code']
        self.claim_code_type = discount_info['claim_code_type']
        self.payment_gateway = discount_info['payment_gateway']
        self.mobile_number = discount_info['mobile_number']
        self.user_id = discount_info['user_id']
        self.rush_id = ''
        self.promo_type = ''
        self.bank_type = ''
        self.CARDHOLDER_PROMOS = [ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO]

        # initialize variables for payment gateway handlers.
        self.reference_parameter_key = None # used by PesoPay payment gateway.
        self.form_method = None
        self.form_target = None
        self.form_parameters = None

    def _do_claimcode_verify_prepare(self, verify_endpoint):
        log.debug("DiscountedCreditCardPayment, _do_claimcode_verify_prepare, verifying GLOBAL CLAIM CODE from %s..." % verify_endpoint)

        headers = {}
        form_data = {}

        headers['LBAPIToken'] = LBAPITOKEN
        form_data['user_id'] = self.user_id
        form_data['seat_count'] = self.seat_count
        form_data['code'] = self.claim_code
        form_data['mobile'] = self.mobile_number
        form_data['theater'] = self.theater_id
        form_data['device_type'] = self.device_type
        form_data['movie'] = self.movie_id
        form_data['rush_id'] = self.rush_id
        form_data['api_type'] = "new" # Fixed value to support old build

        req = requests.Request('POST', url=verify_endpoint, headers=headers, data=form_data).prepare()

        return req

    def _do_claimcode_redeem_prepare(self, transaction_id, total_amount, total_discount, redeem_endpoint):
        log.debug("DiscountedCreditCardPayment, _do_claimcode_redeem_prepare, redeeming GLOBAL CLAIM CODE from %s..." % redeem_endpoint)

        headers = {}
        form_data = {}

        headers['LBAPIToken'] = LBAPITOKEN
        form_data['user_id'] = self.user_id
        form_data['seat_count'] = self.seat_count
        form_data['code'] = self.claim_code
        form_data['mobile'] = self.mobile_number
        form_data['theater'] = self.theater_id
        form_data['device_type'] = self.device_type
        form_data['discount'] = total_discount
        form_data['total_amount'] = total_amount
        form_data['transaction_id'] = transaction_id
        form_data['payment_method'] = self.payment_gateway
        form_data['movie'] = self.movie_id
        form_data['rush_id'] = self.rush_id
        form_data['promo_type'] = self.promo_type
        form_data['bank_type'] = self.bank_type

        req = requests.Request('POST', url=redeem_endpoint, headers=headers, data=form_data).prepare()

        return req

    def _do_claimcode_verify(self, s, req):
        r = s.send(req, timeout=TIMEOUT_DEADLINE)

        return r.status_code, r.text

    def _do_claimcode_redeem(self, s, req):
        r = s.send(req, timeout=TIMEOUT_DEADLINE)

        return r.status_code, r.text

    def _do_parse_float_to_string(self, float_parameter, string_format='{:.2f}'):
        return string_format.format(float_parameter)

    # FIXME!, discounted_seat_price is used for multiple tickets transaction,
    # assuming that the discount will reflect/display only on the last generated ticket.
    def _do_discount_computation(self, total_amount, total_seat_price, ticket_count, discount_value, discount_type, convenience_fee=None, discounted_seats=0):
        log.debug("DiscountedCreditCardPayment, _do_discount_computation, computing discounts...")

        total_discount = 0.0
        actual_discounted_seats = ticket_count
        discounted_seats = int(discounted_seats)

        if discount_type == 'amount':
            total_discount = float(discount_value)
        elif discount_type == 'percent':
            total_discount = float(total_amount) * float(discount_value)/100

        # computation for waived convenience fee
        elif discount_type == 'convenience_fee' and convenience_fee:
            if ticket_count > discounted_seats:
                actual_discounted_seats = discounted_seats

            total_discount = float(convenience_fee) * actual_discounted_seats

        # computation for bulk purchase incentive (free tickets)
        elif discount_type == 'special':
            if ticket_count > discounted_seats:
                actual_discounted_seats = discounted_seats

            # total discount
            total_discount = float(total_seat_price) * actual_discounted_seats

        # discount per seat
        total_seat_discount = float(total_discount) / actual_discounted_seats
        # amount after discounts
        discounted_amount = float(total_amount) - float(total_discount)
        # amount after discount per seat
        discounted_seat_price = float(total_seat_price) - float(total_seat_discount)

        if discounted_amount < 0.0:
            discounted_amount = 0.0

        if discounted_seat_price < 0.0:
            discounted_seat_price = 0.0

        total_discount = self._do_parse_float_to_string(total_discount)
        total_seat_discount = self._do_parse_float_to_string(total_seat_discount)
        discounted_amount = self._do_parse_float_to_string(discounted_amount)
        discounted_seat_price = self._do_parse_float_to_string(discounted_seat_price)

        return total_discount, total_seat_discount, discounted_amount, discounted_seat_price, actual_discounted_seats

    def do_prepare(self, tx):
        log.debug("DiscountedCreditCardPayment, do_prepare, start...")

        if tx.payment_type in [ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO]:

            log.debug("DiscountedCreditCardPayment, do_prepare, payment_type, %s..." % (tx.payment_type))
            verify_endpoint = CLAIM_CODES_VERIFY_ENDPOINT
        else:
            log.warn("ERROR!, DiscountedCreditCardPayment, do_prepare, cardholder promo not supported, %s..." % (tx.payment_type))
            tx.error_info['error_msg'] = "cardholder promo not supported"
            return 'error', self.err_message

        discount_value = '0.00'
        discount_type = 'amount'
        convenience_fee = tx.workspace['convenience_fee']
        seat_price = str(tx.workspace['schedules.price'][0])
        total_amount = str(tx.total_amount(convenience_fee, True))
        total_seat_price = str(tx.total_seat_price(convenience_fee, True))
        ticket_count = int(tx.ticket_count())
        theater_key = tx.workspace['theaters.key'][0]
        theater_code = tx.workspace['theaters.org_theater_code'][0]
        payment_options_settings = tx.workspace['theaters.payment_options_settings']
        discounted_seats = 0

        self.org_uuid = tx.workspace['theaters.org_id'][0]
        self.theater_id = id_encoder.encoded_theater_id(theater_key)
        self.device_type = tx.platform if tx.platform else ''
        self.seat_count = str(tx.reservation_count())
        self.payment_type = tx.payment_type
        self.movie_id = tx.workspace['schedules.movie'][0].key.id()
        self.rush_id = tx.user_info['rush_id']

        # verify whitedlisted theaters for discount codes.
        if payment_options_settings:
            whitelist_theaters = payment_options_settings.allow_discount_codes_whitelist

            if not whitelist_theaters:
                log.warn("ERROR!, DiscountedCreditCardPayment, do_prepare, not whitelist_theaters...")

                tx.error_info['error_msg'] = "DiscountedCreditCardPayment, do_prepare, not whitelist_theaters"
                return 'error', self.err_message

            if self.org_uuid not in whitelist_theaters:
                log.warn("ERROR!, DiscountedCreditCardPayment, do_prepare, org_uuid not in whitelist_theaters...")

                tx.error_info['error_msg'] = "DiscountedCreditCardPayment, do_prepare, org_uuid not in whitelist_theaters"
                return 'error', self.err_message

            if self.theater_id not in whitelist_theaters[self.org_uuid]:
                log.warn("ERROR!, DiscountedCreditCardPayment, do_prepare, theater_id not in whitelist_theaters...")

                tx.error_info['error_msg'] = "DiscountedCreditCardPayment, do_prepare, theater_id not in whitelist_theaters"
                return 'error', self.err_message

        s = requests.Session()
        req = self._do_claimcode_verify_prepare(verify_endpoint)
        r_status_code, r_json = call_once(self._do_claimcode_verify, tx, s, req)

        log.debug("DiscountedCreditCardPayment, do_prepare, GLOBAL CLAIM CODE verify response: {}...".format(r_json))

        if r_status_code != 200:
            log.warn("ERROR!, DiscountedCreditCardPayment, do_prepare, GLOBAL CLAIM CODE request status_code error...")

            tx.error_info['error_msg'] = "GLOBAL CLAIM CODE request status_code error: %s" % r_status_code
            return 'error', self.err_message

        d_json = json.loads(r_json)

        if 'response' not in d_json:
            log.warn("ERROR!, DiscountedCreditCardPayment, do_prepare, missing key response in json response...")

            tx.error_info['error_msg'] = "missing key response in json response"
            return 'error', self.err_message

        if 'status' not in d_json['response']:
            log.warn("ERROR!, DiscountedCreditCardPayment, do_prepare, missing key status in json response...")

            tx.error_info['error_msg'] = "missing key status in json response"
            return 'error', self.err_message

        if d_json['response']['status'] != 1:
            log.warn("ERROR!, DiscountedCreditCardPayment, do_prepare, status is not equal to 1...")

            if 'details' in d_json['response']:
                if 'msg' in d_json['response']['details']:
                    tx.error_info['error_msg'] = d_json['response']['details']['msg']
                    return 'error', d_json['response']['details']['msg']

            tx.error_info['error_msg'] = "DiscountedCreditCardPayment, do_prepare, status is not equal to 1: %s" % d_json['response']['status']
            return 'error', self.err_message

        if 'details' not in d_json['response']:
            log.warn("ERROR!, DiscountedCreditCardPayment, do_prepare, missing key details in json response...")

            tx.error_info['error_msg'] = "missing key details in json response"
            return 'error', self.err_message

        if 'discount_value' not in d_json['response']['details']:
            log.warn("ERROR!, DiscountedCreditCardPayment, do_prepare, missing key discount_value in json response...")

            tx.error_info['error_msg'] = "missing key discount_value in json response"
            return 'error', self.err_message

        if 'discount_type' not in d_json['response']['details']:
            log.warn("ERROR!, DiscountedCreditCardPayment, do_prepare, missing key discount_type in json response...")

            tx.error_info['error_msg'] = "missing key discount_type in json response"
            return 'error', self.err_message

        if 'promo_type' not in d_json['response']['details']:
            log.warn("ERROR!, DiscountedCreditCardPayment, do_prepare, missing key promo_type in json response...")

            tx.error_info['error_msg'] = "missing key promo_type in json response"
            return 'error', self.err_message

        discount_value = d_json['response']['details']['discount_value']
        discount_type = d_json['response']['details']['discount_type']
        self.promo_type = d_json['response']['details']['promo_type']
        if 'bank_type' in d_json['response']['details']:
            self.bank_type = d_json['response']['details']['bank_type']

        # Accept discount type amount and percent for now, will remove this condition once the discount type special is ready.
        if discount_type not in ['amount', 'percent', 'convenience_fee', 'special']:
            log.warn("ERROR!, DiscountedCreditCardPayment, do_prepare, discount_type is not equal to ['amount', 'percent', 'convenience_fee', 'special'], discount_type : %s..." % discount_type)

            tx.error_info['error_msg'] = "discount_type not supported: %s" % (discount_type)
            return 'error', self.err_message

        # Check if discount_type 'convenience_fee' has discounted_seats.
        # Needed for computation of discount.
        if 'convenience_fee' == discount_type:
            if 'discounted_seats' not in d_json['response']['details']:
                log.warn("ERROR!, DiscountedCreditCardPayment, do_prepare, missing key discounted_seats in json response...")
                tx.error_info['error_msg'] = "missing key discounted_seats in json response"
                return 'error', self.err_message
            else:
                discounted_seats = d_json['response']['details']['discounted_seats']

        # if discount_type 'special' parse discount_value to get discounted_seats
        # discount_value (ex. 'free;2')
        if 'special' == discount_type:
            discount_desc, discounted_seats = discount_value.split(';')

            if 'free' != discount_desc:
                log.warn("ERROR!, DiscountedCreditCardPayment, do_prepare, discount_value is not 'free' in json response, discount_value : %s ..." % discount_value)
                tx.error_info['error_msg'] = "discount_value is not 'free' in json response: %s" % discount_value
                return 'error', self.err_message
            else:
                # so discount_value will just be 'free' instead of 'free;2'
                discount_value = discount_desc

            if not discounted_seats.isdigit():
                log.warn("ERROR!, DiscountedCreditCardPayment, do_prepare, discount_value does not contain free ticket count in json response, discount_value : %s ..." % discount_value)
                tx.error_info['error_msg'] = "discount_value does not contain free ticket count in json response: %s" % discount_value
                return 'error', self.err_message

        total_discount, total_seat_discount, discounted_amount, discounted_seat_price, actual_discounted_seats = self._do_discount_computation(
                total_amount, total_seat_price, ticket_count, discount_value, discount_type, convenience_fee, discounted_seats)

        tx.workspace['discount::is_discounted'] = True
        tx.workspace['discount::total_discount'] = total_discount
        tx.workspace['discount::multiple_tickets::total_seat_discount'] = total_seat_discount
        tx.workspace['discount::total_amount'] = discounted_amount
        tx.workspace['discount::multiple_tickets::total_seat_price'] = discounted_seat_price
        tx.workspace['discount::multiple_tickets::actual_discounted_seats'] = actual_discounted_seats
        tx.workspace['discount::discount_value'] = str(total_discount) if 'free' == discount_value else str(discount_value)
        tx.workspace['discount::discount_type'] = discount_type
        tx.workspace['discount::actual_discounted_seats'] = actual_discounted_seats

        tx.is_discounted = True
        tx.discount_info['original_total_amount'] = total_amount
        tx.discount_info['total_discount'] = total_discount
        tx.discount_info['total_amount'] = discounted_amount
        tx.discount_info['discount_value'] = str(total_discount) if 'free' == discount_value else str(discount_value)
        tx.discount_info['discount_type'] = discount_type
        tx.discount_info['actual_discounted_seats'] = actual_discounted_seats

        log.debug("DiscountedCreditCardPayment, do_prepare, preparing payment handler...")

        # prepare payment gateway.
        if self.payment_type in self.CARDHOLDER_PROMOS and self.org_uuid in PESOPAY_THEATERORGS and self.payment_gateway == 'pesopay':
            log.debug("DiscountedCreditCardPayment, do_prepare, initializing ClientInitiatedPayment handler...")

            settings = _get_client_initiated_payment_settings(theater_key, self.payment_gateway, tx.workspace['theaters.cinemas.cinema_name'])
            self.payment_engine = ClientInitiatedPayment(settings, tx.payment_info)

            self.reference_parameter_key = self.payment_engine.reference_parameter_key
            self.form_method = self.payment_engine.form_method
            self.form_target = self.payment_engine.form_target
            self.form_parameters = self.payment_engine.form_parameters

            # additional parameters in form_parameters for Cardholder Promos.
            if tx.payment_type == ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO:
                self.form_parameters['promotion'] = 'T'
                self.form_parameters['promotionCode'] = 'gmovies1'
                self.form_parameters['promotionRuleCode'] = 'gmovies1'
            elif tx.payment_type == ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO:
                self.form_parameters['promotion'] = 'T'
                self.form_parameters['promotionCode'] = 'citibank'
                self.form_parameters['promotionRuleCode'] = 'citibank'
            elif tx.payment_type == ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO:
                self.form_parameters['promotion'] = 'T'
                self.form_parameters['promotionCode'] = 'pnbpromo'
                self.form_parameters['promotionRuleCode'] = 'pnbpromo'
            elif tx.payment_type == ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO:
                self.form_parameters['promotion'] = 'T'
                self.form_parameters['promotionCode'] = 'bdopromo'
                self.form_parameters['promotionRuleCode'] = 'bdopromo'
            elif tx.payment_type == ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO:
                self.form_parameters['promotion'] = 'T'
                self.form_parameters['promotionCode'] = 'gcapromo'
                self.form_parameters['promotionRuleCode'] = 'gcapromo'
            elif tx.payment_type == ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO:
                self.form_parameters['promotion'] = 'T'
                self.form_parameters['promotionCode'] = 'metrobnk'
                self.form_parameters['promotionRuleCode'] = 'metrobnk'
            elif tx.payment_type == ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO:
                self.form_parameters['promotion'] = 'T'
                self.form_parameters['promotionCode'] = 'chinabnk'
                self.form_parameters['promotionRuleCode'] = 'chinabnk'
            elif tx.payment_type == ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO:
                self.form_parameters['promotion'] = 'T'
                self.form_parameters['promotionCode'] = 'rbsbnk'
                self.form_parameters['promotionRuleCode'] = 'rbsbnk'

            # tagging of payment type in transaction workspace.
            tx.workspace[WS_PAYMENT_CLIENT_INITIATED] = True
        elif self.payment_type in self.CARDHOLDER_PROMOS and self.org_uuid in PAYNAMICS_THEATERORGS and self.payment_gateway == 'paynamics':
            log.debug("DiscountedCreditCardPayment, do_prepare, initializing PaynamicsPayment handler...")

            settings = _get_client_initiated_payment_settings(theater_key, self.payment_gateway)
            if 'token' in tx.payment_info and tx.payment_info['token']:
                if 'bin' in tx.payment_info and tx.payment_info['bin']:
                    # validate if bin is included in list
                    if tx.payment_type not in CARDHOLDER_PROMOS_BINS:
                        log.warn("ERROR!, DiscountedCreditCardPayment, do_prepare, cardholder promo not supported...")
                        tx.error_info['error_msg'] = "cardholder promo not supported: %s" % tx.payment_type
                        return 'error', self.err_message

                    if tx.payment_info['bin'] not in CARDHOLDER_PROMOS_BINS[tx.payment_type]:
                        log.warn("ERROR!, DiscountedCreditCardPayment, do_prepare, card bin not included...")
                        tx.error_info['error_msg'] = "cardholder promo, bin not included: %s - %s" % (tx.payment_type, tx.payment_info['bin'])
                        return 'error', self.err_message

                    if self.org_uuid in [str(CINEMA76_MALLS), str(SM_MALLS)]:
                        log.debug("DiscountedCreditCardPayment, do_prepare, initializing PaynamicsPaymentWithToken3D handler...")
                        self.payment_engine = PaynamicsPaymentWithToken3D(settings, tx.payment_info)
                        tx.workspace[WS_PAYMENT_3D_TOKEN_PAYNAMICS] = True
                    else:
                        log.debug("DiscountedCreditCardPayment, do_prepare, initializing PaynamicsPaymentWithToken handler...")
                        self.payment_engine = PaynamicsPaymentWithToken(settings, tx.payment_info)
                        tx.workspace[WS_PAYMENT_TOKEN_PAYNAMICS] = True
                else:
                    log.warn("ERROR!, DiscountedCreditCardPayment, do_prepare, missing key bin in tx.payment_info...")
                    tx.error_info['error_msg'] = "missing key bin in tx.payment_info"
                    return 'error', self.err_message
            else:
                self.payment_engine = PaynamicsPayment(settings, tx.payment_info, tx.workspace['theaters.org_id'][0])
                self.form_method = self.payment_engine.form_method
                self.form_target = self.payment_engine.form_target
                self.form_parameters = self.payment_engine.form_parameters

                # additional parameters in form_parameters for Cardholder Promos.
                if tx.payment_type == ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO:
                    self.form_parameters['pmethod'] = 'CCP'
                    self.form_parameters['ccp_bin'] = ','.join(PAYNAMICS_CCP_BIN)
                elif tx.payment_type == ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO:
                    self.form_parameters['pmethod'] = 'CCP'
                    self.form_parameters['ccp_bin'] = ','.join(PAYNAMICS_CCP_BIN_CITIBANK)
                elif tx.payment_type == ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO:
                    self.form_parameters['pmethod'] = 'CCP'
                    self.form_parameters['ccp_bin'] = ','.join(PAYNAMICS_CCP_BIN_PNB)
                elif tx.payment_type == ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO:
                    self.form_parameters['pmethod'] = 'CCP'
                    self.form_parameters['ccp_bin'] = ','.join(PAYNAMICS_CCP_BIN_BDO)
                elif tx.payment_type == ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO:
                    self.form_parameters['pmethod'] = 'CCP'
                    self.form_parameters['ccp_bin'] = ','.join(PAYNAMICS_CCP_BIN_GCASH)
                elif tx.payment_type == ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO:
                    self.form_parameters['pmethod'] = 'CCP'
                    self.form_parameters['ccp_bin'] = ','.join(PAYNAMICS_CCP_BIN_METROBANK)
                elif tx.payment_type == ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO:
                    self.form_parameters['pmethod'] = 'CCP'
                    self.form_parameters['ccp_bin'] = ','.join(PAYNAMICS_CCP_BIN_CHINABANK)
                elif tx.payment_type == ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO:
                    self.form_parameters['pmethod'] = 'CCP'
                    self.form_parameters['ccp_bin'] = ','.join(PAYNAMICS_CCP_BIN_ROBINSONSBANK)

            # tagging of payment type in transaction workspace.
            if self.org_uuid == str(ROCKWELL_MALLS):
                log.debug("DiscountedCreditCardPayment, do_prepare, Rockwell (Power Plant Mall) settings...")

                tx.workspace[WS_PAYMENT_POWERPLANTMALL_PAYNAMICS] = True
            elif self.org_uuid == str(GREENHILLS_MALLS):
                log.debug("DiscountedCreditCardPayment, do_prepare, Greenhills Malls settings...")

                tx.workspace[WS_PAYMENT_GREENHILLS_PAYNAMICS] = True
            elif self.org_uuid == str(CINEMA76_MALLS):
                log.debug("DiscountedCreditCardPayment, do_prepare, Cinema 76 Malls settings...")

                tx.workspace[WS_PAYMENT_CINEMA76_PAYNAMICS] = True
            elif self.org_uuid == str(GLOBE_EVENTS):
                log.debug("DiscountedCreditCardPayment, do_prepare, Globe Events settings...")

                tx.workspace[WS_PAYMENT_GLOBEEVENTS_PAYNAMICS] = True
            elif self.org_uuid == str(SM_MALLS):
                log.debug("DiscountedCreditCardPayment, do_prepare, SM Malls settings...")

                tx.workspace[WS_PAYMENT_SMMALLS_PAYNAMICS] = True
            elif self.org_uuid == str(ROBINSONS_MALLS):
                log.debug("DiscountedCreditCardPayment, do_prepare, Robinsons Malls settings...")

                tx.workspace[WS_PAYMENT_ROBINSONS_PAYNAMICS] = True
            else:
                log.warn("ERROR, DiscountedCreditCardPayment, do_prepare, theaterorg_id not supported...")

                tx.error_info['error_msg'] = "theaterorg_id not supported"
                return 'error', self.err_message
        elif self.payment_type in self.CARDHOLDER_PROMOS and self.org_uuid in IPAY88_THEATERORGS and self.payment_gateway == 'ipay88':
            log.debug("DiscountedCreditCardPayment, do_prepare, initializing IPay88Payment2 handler...")

            tx.error_info['error_msg'] = "DiscountedCreditCardPayment ipay88 not supported"
            return 'error', self.err_message
        else:
            log.warn("ERROR!, DiscountedCreditCardPayment, do_prepare, payment_type not supported...")

            tx.error_info['error_msg'] = "DiscountedCreditCardPayment payment_type not supported"
            return 'error', self.err_message

        log.debug("DiscountedCreditCardPayment, do_prepare, finalized payment handler...")

        return self.payment_engine.do_prepare(tx)

    def do_payment_tx(self, tx):
        log.debug("DiscountedCreditCardPayment, do_payment_tx, start...")

        if tx.payment_type in [ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO]:

            log.debug("DiscountedCreditCardPayment, do_prepare, payment_type, %s..." % (tx.payment_type))
            verify_endpoint = CLAIM_CODES_VERIFY_ENDPOINT
        else:
            log.warn("ERROR!, DiscountedCreditCardPayment, do_prepare, cardholder promo not supported...")
            tx.error_info['error_msg'] = "cardholder promo not supported: %s" % tx.payment_type
            return 'error', self.err_message

        if tx.payment_type in [ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO]:
            log.debug("DiscountedCreditCardPayment, do_payment_tx, payment_type, %s..." % (tx.payment_type))

            redeem_endpoint = CLAIM_CODES_REDEEM_ENDPOINT
        else:
            log.warn("ERROR! DiscountedCreditCardPayment, do_payment_tx, cardholder promo not supported, %s..." % (tx.payment_type))

            tx.error_info['error_msg'] = "cardholder promo not supported: %s" % tx.payment_type
            return 'error', self.err_message

        if self.payment_type in self.CARDHOLDER_PROMOS and self.org_uuid in PAYNAMICS_THEATERORGS and self.payment_gateway == 'paynamics':
            log.debug("DiscountedCreditCardPayment, do_payment_tx, verify PaynamicsPayment handler...")

            # check only if not using token; no response yet on this part if using token
            if not is_token_paynamics(tx):
                if 'response_code' not in tx.workspace:
                    log.warn("ERROR!, DiscountedCreditCardPayment, do_payment_tx, missing key response_code in tx.workspace...")

                    tx.error_info['error_msg'] = "missing key response_code in tx.workspace"
                    return 'error', self.err_message

                if tx.workspace["response_code"] != 'GR001' and tx.workspace["response_code"] != 'GR002':
                    log.warn("ERROR, DiscountedCreditCardPayment, do_payment_tx, response_code not GR001 or GR002...")

                    namespace = 'paynamics'
                    response_code = tx.workspace['response_code']
                    response_message = tx.workspace['response_message'] if 'response_message' in tx.workspace else ''

                    tx.error_info['error_msg'] = response_message
                    return 'error', translate_message_via_error_code(namespace, response_code, response_message)

        try:
            transaction_id = '%s~%s' % (tx.key.parent().id(), tx.key.id())
            convenience_fee = tx.workspace['convenience_fee']
            total_amount = str(tx.total_amount(convenience_fee, True))
            total_discount = tx.workspace['discount::total_discount']

            s = requests.Session()
            req = self._do_claimcode_redeem_prepare(transaction_id, total_amount, total_discount, redeem_endpoint)
            r_status_code, r_json = call_once(self._do_claimcode_redeem, tx, s, req)

            log.debug("DiscountedCreditCardPayment, do_payment_tx, CARDHOLDERPROMO CLAIM CODE redeemed response: {}...".format(r_json))
        except Exception, e:
            log.warn("ERROR!, DiscountedCreditCardPayment, do_payment_tx, CARDHOLDERPROMO CLAIM CODE connection/response issue...")
            log.error(e)
            tx.error_info['error_msg'] = str(e)

        if self.payment_type in self.CARDHOLDER_PROMOS and self.org_uuid in PESOPAY_THEATERORGS and self.payment_gateway == 'pesopay':
            log.warn("DiscountedCreditCardPayment, do_payment_tx, bypass ClientInitiatedPayment handler...")

            return 'success', None

        return self.payment_engine.do_payment_tx(tx)

    def do_refund_tx(self, tx):
        log.debug("DiscountedCreditCardPayment, do_refund_tx, start...")

        has_do_refund_tx = hasattr(self.payment_engine, 'do_refund_tx')

        if has_do_refund_tx:
            log.debug("DiscountedCreditCardPayment, do_refund_tx, initialized payment handler has do_refund_tx...")

            return self.payment_engine.do_refund_tx(tx)
        else:
            log.warn("SKIP!, DiscountedCreditCardPayment, do_refund_tx, initialized payment handler has no attribute do_refund_tx...")

    # used by PesoPay and iPay88 payment gateway.
    def generate_hash(self, reference):
        log.debug("DiscountedCreditCardPayment, generate_hash, start...")

        signature = ''
        has_generate_hash = hasattr(self.payment_engine, 'generate_hash')

        if has_generate_hash:
            signature = self.payment_engine.generate_hash(reference)

        return signature

    # used by Paynamics payment gateway.
    def generate_orders_payload(self, tx):
        log.debug("DiscountedCreditCardPayment, generate_orders_payload, start...")

        orders_payload = {}
        has_generate_orders_payload = hasattr(self.payment_engine, 'generate_orders_payload')

        if has_generate_orders_payload:
            orders_payload = self.payment_engine.generate_orders_payload(tx)

        return orders_payload

    # used by Paynamics payment gateway.
    def generate_signature(self):
        log.debug("DiscountedCreditCardPayment, generate_signature, start...")

        signature = ''
        has_generate_signature = hasattr(self.payment_engine, 'generate_signature')

        if has_generate_signature:
            signature = self.payment_engine.generate_signature()

        return signature

    # used by Paynamics payment gateway.
    def generate_paymentrequest(self):
        log.debug("DiscountedCreditCardPayment, generate_paymentrequest, start...")

        payment_request = ''
        has_generate_paymentrequest = hasattr(self.payment_engine, 'generate_paymentrequest')

        if has_generate_paymentrequest:
            payment_request = self.payment_engine.generate_paymentrequest()

        return payment_request

# NOTE: this handler will be used for coretool claim codes.
class GlobalClaimCodePayment():
    def __init__(self, payment_info, discount_info):
        self.err_message = DEFAULT_ERROR_MESSAGE
        self.device_type = ''
        self.payment_type = ''
        self.org_uuid = ''
        self.theater_id = ''
        self.seat_count = ''
        self.movie_id = ''
        self.claim_code = discount_info['claim_code']
        self.claim_code_type = discount_info['claim_code_type']
        self.payment_gateway = discount_info['payment_gateway']
        self.mobile_number = discount_info['mobile_number']
        self.user_id = discount_info['user_id']
        self.rush_id = ''
        self.promo_type = ''
        self.bank_type = ''
        self.variant = ''
        self.show_date = ''

        # initialize variables for payment gateway handlers.
        self.reference_parameter_key = None # used by PesoPay payment gateway.
        self.form_method = None
        self.form_target = None
        self.form_parameters = None

    def _do_claimcode_verify_prepare(self):
        log.debug("GlobalClaimCodePayment, _do_claimcode_verify_prepare, verifying GLOBAL CLAIM CODE from %s..." % CLAIM_CODES_VERIFY_ENDPOINT)

        headers = {}
        form_data = {}

        headers['LBAPIToken'] = LBAPITOKEN
        form_data['user_id'] = self.user_id
        form_data['seat_count'] = self.seat_count
        form_data['code'] = self.claim_code
        form_data['mobile'] = self.mobile_number
        form_data['theater'] = self.theater_id
        form_data['device_type'] = self.device_type
        form_data['movie'] = self.movie_id
        form_data['rush_id'] = self.rush_id
        form_data['api_type'] = "new" # Fixed value to support old build
        form_data['variant'] = self.variant
        form_data['show_date'] = self.show_date
        log.debug("GlobalClaimCodePayment, _do_claimcode_verify_prepare, params: {0}...".format(form_data))

        req = requests.Request('POST', url=CLAIM_CODES_VERIFY_ENDPOINT, headers=headers, data=form_data).prepare()

        return req

    def _do_claimcode_redeem_prepare(self, transaction_id, total_amount, total_discount):
        log.debug("GlobalClaimCodePayment, _do_claimcode_redeem_prepare, redeeming GLOBAL CLAIM CODE from %s..." % CLAIM_CODES_REDEEM_ENDPOINT)

        headers = {}
        form_data = {}

        headers['LBAPIToken'] = LBAPITOKEN
        form_data['user_id'] = self.user_id
        form_data['seat_count'] = self.seat_count
        form_data['code'] = self.claim_code
        form_data['mobile'] = self.mobile_number
        form_data['theater'] = self.theater_id
        form_data['device_type'] = self.device_type
        form_data['discount'] = total_discount
        form_data['total_amount'] = total_amount
        form_data['transaction_id'] = transaction_id
        form_data['payment_method'] = self.payment_gateway
        form_data['movie'] = self.movie_id
        form_data['rush_id'] = self.rush_id
        form_data['promo_type'] = self.promo_type
        form_data['bank_type'] = self.bank_type
        form_data['variant'] = self.variant
        form_data['show_date'] = self.show_date
        log.debug("GlobalClaimCodePayment, _do_claimcode_redeem_prepare, params: {0}...".format(form_data))

        req = requests.Request('POST', url=CLAIM_CODES_REDEEM_ENDPOINT, headers=headers, data=form_data).prepare()

        return req

    def _do_claimcode_verify(self, s, req):
        r = s.send(req, timeout=TIMEOUT_DEADLINE)

        return r.status_code, r.text

    def _do_claimcode_redeem(self, s, req):
        r = s.send(req, timeout=TIMEOUT_DEADLINE)

        return r.status_code, r.text

    def _do_parse_float_to_string(self, float_parameter, string_format='{:.2f}'):
        return string_format.format(float_parameter)

    # FIXME!, discounted_seat_price is used for multiple tickets transaction,
    # assuming that the discount will reflect/display only on the last generated ticket.
    def _do_discount_computation(self, total_amount, total_seat_price, ticket_count, discount_value, discount_type, convenience_fee=None, discounted_seats=0):
        log.debug("GlobalClaimCodePayment, _do_discount_computation, computing discounts...")

        total_discount = 0.0
        actual_discounted_seats = ticket_count
        discounted_seats = int(discounted_seats)

        if discount_type == 'amount':
            total_discount = float(discount_value)
        elif discount_type == 'percent':
            total_discount = float(total_amount) * float(discount_value)/100

        # computation for waived convenience fee
        elif discount_type == 'convenience_fee' and convenience_fee:
            if ticket_count > discounted_seats:
                actual_discounted_seats = discounted_seats

            total_discount = float(convenience_fee) * actual_discounted_seats

        # computation for bulk purchase incentive (free tickets)
        elif discount_type == 'special':
            if ticket_count > discounted_seats:
                actual_discounted_seats = discounted_seats

            # total discount
            total_discount = float(total_seat_price) * actual_discounted_seats

        # discount per seat
        total_seat_discount = float(total_discount) / actual_discounted_seats
        # amount after discounts
        discounted_amount = float(total_amount) - float(total_discount)
        # amount after discount per seat
        discounted_seat_price = float(total_seat_price) - float(total_seat_discount)

        if discounted_amount < 0.0:
            discounted_amount = 0.0

        if discounted_seat_price < 0.0:
            discounted_seat_price = 0.0

        total_discount = self._do_parse_float_to_string(total_discount)
        total_seat_discount = self._do_parse_float_to_string(total_seat_discount)
        discounted_amount = self._do_parse_float_to_string(discounted_amount)
        discounted_seat_price = self._do_parse_float_to_string(discounted_seat_price)

        return total_discount, total_seat_discount, discounted_amount, discounted_seat_price, actual_discounted_seats

    def do_prepare(self, tx):
        log.debug("GlobalClaimCodePayment, do_prepare, start...")

        discount_value = '0.00'
        discount_type = 'amount'
        convenience_fee = tx.workspace['convenience_fee']
        seat_price = str(tx.workspace['schedules.price'][0])
        total_amount = str(tx.total_amount(convenience_fee, True))
        total_seat_price = str(tx.total_seat_price(convenience_fee, True))
        ticket_count = int(tx.ticket_count())
        theater_key = tx.workspace['theaters.key'][0]
        theater_code = tx.workspace['theaters.org_theater_code'][0]
        payment_options_settings = tx.workspace['theaters.payment_options_settings']
        discounted_seats = 0

        self.org_uuid = tx.workspace['theaters.org_id'][0]
        self.theater_id = id_encoder.encoded_theater_id(theater_key)
        self.device_type = tx.platform if tx.platform else ''
        self.seat_count = str(tx.reservation_count())
        self.payment_type = tx.payment_type
        self.movie_id = tx.workspace['schedules.movie'][0].key.id()
        self.rush_id = tx.user_info['rush_id']
        self.variant = tx.workspace['schedules.variant']
        self.show_date = tx.workspace['schedules.show_datetime'][0].strftime('%Y-%m-%d')

        # verify whitedlisted theaters for discount codes.
        if payment_options_settings:
            whitelist_theaters = payment_options_settings.allow_discount_codes_whitelist

            if not whitelist_theaters:
                log.warn("ERROR!, GlobalClaimCodePayment, do_prepare, not whitelist_theaters...")

                tx.error_info['error_msg'] = "GlobalClaimCodePayment not whitelist_theaters"
                return 'error', self.err_message

            if self.org_uuid not in whitelist_theaters:
                log.warn("ERROR!, GlobalClaimCodePayment, do_prepare, org_uuid not in whitelist_theaters...")

                tx.error_info['error_msg'] = "GlobalClaimCodePayment org_uuid not in whitelist_theaters"
                return 'error', self.err_message

            if self.theater_id not in whitelist_theaters[self.org_uuid]:
                log.warn("ERROR!, GlobalClaimCodePayment, do_prepare, theater_id not in whitelist_theaters...")

                tx.error_info['error_msg'] = "GlobalClaimCodePayment theater_id not in whitelist_theaters"
                return 'error', self.err_message

        s = requests.Session()
        req = self._do_claimcode_verify_prepare()
        r_status_code, r_json = call_once(self._do_claimcode_verify, tx, s, req)

        log.debug("GlobalClaimCodePayment, do_prepare, GLOBAL CLAIM CODE verify response: {}...".format(r_json))

        if r_status_code != 200:
            log.warn("ERROR!, GlobalClaimCodePayment, do_prepare, GLOBAL CLAIM CODE request status_code error...")

            tx.error_info['error_msg'] = "GLOBAL CLAIM CODE request status_code error: %s" % r_status_code
            return 'error', self.err_message

        d_json = json.loads(r_json)

        if 'response' not in d_json:
            log.warn("ERROR!, GlobalClaimCodePayment, do_prepare, missing key response in json response...")

            tx.error_info['error_msg'] = "missing key response in json response"
            return 'error', self.err_message

        if 'status' not in d_json['response']:
            log.warn("ERROR!, GlobalClaimCodePayment, do_prepare, missing key status in json response...")

            tx.error_info['error_msg'] = "missing key status in json response"
            return 'error', self.err_message

        if d_json['response']['status'] != 1:
            log.warn("ERROR!, GlobalClaimCodePayment, do_prepare, status is not equal to 1...")

            if 'details' in d_json['response']:
                if 'msg' in d_json['response']['details']:
                    tx.error_info['error_msg'] = d_json['response']['details']['msg']
                    return 'error', d_json['response']['details']['msg']

            tx.error_info['error_msg'] = "GlobalClaimCodePayment, do_prepare, status is not equal to 1: %s" % d_json['response']['status']
            return 'error', self.err_message

        if 'details' not in d_json['response']:
            log.warn("ERROR!, GlobalClaimCodePayment, do_prepare, missing key details in json response...")

            tx.error_info['error_msg'] = "missing key details in json response"
            return 'error', self.err_message

        if 'discount_value' not in d_json['response']['details']:
            log.warn("ERROR!, GlobalClaimCodePayment, do_prepare, missing key discount_value in json response...")

            tx.error_info['error_msg'] = "missing key discount_value in json response"
            return 'error', self.err_message

        if 'discount_type' not in d_json['response']['details']:
            log.warn("ERROR!, GlobalClaimCodePayment, do_prepare, missing key discount_type in json response...")

            tx.error_info['error_msg'] = "missing key discount_type in json response"
            return 'error', self.err_message

        if 'promo_type' not in d_json['response']['details']:
            log.warn("ERROR!, GlobalClaimCodePayment, do_prepare, missing key promo_type in json response...")

            tx.error_info['error_msg'] = "missing key promo_type in json response"
            return 'error', self.err_message

        discount_value = d_json['response']['details']['discount_value']
        discount_type = d_json['response']['details']['discount_type']
        self.promo_type = d_json['response']['details']['promo_type']
        if 'bank_type' in d_json['response']['details']:
            self.bank_type = d_json['response']['details']['bank_type']

        # Accept discount type amount and percent for now, will remove this condition once the discount type special is ready.
        if discount_type not in ['amount', 'percent', 'convenience_fee', 'special']:
            log.warn("ERROR!, GlobalClaimCodePayment, do_prepare, discount_type is not equal to ['amount', 'percent', 'convenience_fee', 'special'], discount_type : %s..." % discount_type)

            tx.error_info['error_msg'] = "discount_type not supported: %s" % (discount_type)
            return 'error', self.err_message

        # Check if discount_type 'convenience_fee' has discounted_seats.
        # Needed for computation of discount.
        if 'convenience_fee' == discount_type:
            if 'discounted_seats' not in d_json['response']['details']:
                log.warn("ERROR!, GlobalClaimCodePayment, do_prepare, missing key discounted_seats in json response...")
                tx.error_info['error_msg'] = "missing key discounted_seats in json response"
                return 'error', self.err_message
            else:
                discounted_seats = d_json['response']['details']['discounted_seats']

        # if discount_type 'special' parse discount_value to get discounted_seats
        # discount_value (ex. 'free;2')
        if 'special' == discount_type:
            discount_desc, discounted_seats = discount_value.split(';')

            if 'free' != discount_desc:
                log.warn("ERROR!, GlobalClaimCodePayment, do_prepare, discount_value is not 'free' in json response, discount_value : %s ..." % discount_value)
                tx.error_info['error_msg'] = "discount_value is not 'free' in json response: %s" % discount_value
                return 'error', self.err_message
            else:
                # so discount_value will just be 'free' instead of 'free;2'
                discount_value = discount_desc

            if not discounted_seats.isdigit():
                log.warn("ERROR!, GlobalClaimCodePayment, do_prepare, discount_value does not contain free ticket count in json response, discount_value : %s ..." % discount_value)
                tx.error_info['error_msg'] = "discount_value does not contain free ticket count in json response: %s" % discount_value
                return 'error', self.err_message

        total_discount, total_seat_discount, discounted_amount, discounted_seat_price, actual_discounted_seats = self._do_discount_computation(
                total_amount, total_seat_price, ticket_count, discount_value, discount_type, convenience_fee, discounted_seats)

        tx.workspace['discount::is_discounted'] = True
        tx.workspace['discount::total_discount'] = total_discount
        tx.workspace['discount::multiple_tickets::total_seat_discount'] = total_seat_discount
        tx.workspace['discount::total_amount'] = discounted_amount
        tx.workspace['discount::multiple_tickets::total_seat_price'] = discounted_seat_price
        tx.workspace['discount::multiple_tickets::actual_discounted_seats'] = actual_discounted_seats
        tx.workspace['discount::discount_value'] = str(total_discount) if 'free' == discount_value else str(discount_value)
        tx.workspace['discount::discount_type'] = discount_type
        tx.workspace['discount::actual_discounted_seats'] = actual_discounted_seats

        tx.is_discounted = True
        tx.discount_info['original_total_amount'] = total_amount
        tx.discount_info['total_discount'] = total_discount
        tx.discount_info['total_amount'] = discounted_amount
        tx.discount_info['discount_value'] = str(total_discount) if 'free' == discount_value else str(discount_value)
        tx.discount_info['discount_type'] = discount_type
        tx.discount_info['actual_discounted_seats'] = actual_discounted_seats

        log.debug("GlobalClaimCodePayment, do_prepare, preparing payment handler...")

        # prepare payment handler.
        if self.payment_type in [ReservationTransaction.allowed_payment_types.GCASH, ReservationTransaction.allowed_payment_types.GCASH_APP]:
            log.debug("GlobalClaimCodePayment, do_prepare, initializing GCashPayment handler...")

            # initialize GCashPayment handler.
            self.payment_engine = GCashPayment(tx.payment_info)

            if self.org_uuid == str(SM_MALLS):
                log.debug("GlobalClaimCodePayment, do_prepare, SM Malls settings...")

                tx.workspace[WS_PAYMENT_SMMALLS_GCASH] = True
            elif self.org_uuid == str(ROCKWELL_MALLS):
                log.debug("GlobalClaimCodePayment, do_prepare, Rockwell (Power Plant Mall) settings...")

                tx.workspace[WS_PAYMENT_POWERPLANTMALL_GCASH] = True
            elif self.org_uuid == str(GREENHILLS_MALLS):
                log.debug("GlobalClaimCodePayment, do_prepare, Greenhills Malls settings...")

                tx.workspace[WS_PAYMENT_GREENHILLS_GCASH] = True
            elif self.org_uuid == str(CINEMA76_MALLS):
                log.debug("GlobalClaimCodePayment, do_prepare, Cinema 76 Malls settings...")

                tx.workspace[WS_PAYMENT_CINEMA76_GCASH] = True
            elif self.org_uuid == str(GLOBE_EVENTS):
                log.debug("GlobalClaimCodePayment, do_prepare, Globe Events settings...")

                tx.workspace[WS_PAYMENT_GLOBEEVENTS_GCASH] = True
            elif self.org_uuid == str(AYALA_MALLS):
                log.debug("GlobalClaimCodePayment, do_prepare, Ayala Malls settings...")

                tx.workspace[WS_PAYMENT_AYALA_GCASH] = True
            elif self.org_uuid == str(ROBINSONS_MALLS):
                log.debug("GlobalClaimCodePayment, do_prepare, Robinsons settings...")

                tx.workspace[WS_PAYMENT_ROBINSONS_GCASH] = True
            else:
                log.warn("ERROR, GlobalClaimCodePayment, do_prepare, theaterorg_id not supported...")

                tx.error_info['error_msg'] = "theaterorg_id not supported"
                return 'error', self.err_message
        elif self.payment_type in [ReservationTransaction.allowed_payment_types.GREWARDS]:
            log.debug("GlobalClaimCodePayment, do_prepare, initializing GRewardsPayment handler...")

            # initialize GRewardsPayment handler.
            self.payment_engine = GRewardsPayment(tx.payment_info)

            if self.org_uuid == str(AYALA_MALLS):
                log.debug("GlobalClaimCodePayment, do_prepare, Ayala Malls settings...")

                tx.workspace[WS_PAYMENT_AYALA_GREWARDS] = True
            else:
                log.warn("ERROR, GlobalClaimCodePayment, do_prepare, theaterorg_id not supported...")

                tx.error_info['error_msg'] = "theaterorg_id not supported"
                return 'error', self.err_message
        elif self.payment_type == ReservationTransaction.allowed_payment_types.CLIENT_INITIATED:
            log.debug("GlobalClaimCodePayment, do_prepare, initializing ClientInitiatedPayment handler...")

            # initialize ClientInitiatedPayment handler.
            settings = _get_client_initiated_payment_settings(theater_key, self.payment_gateway, tx.workspace['theaters.cinemas.cinema_name'])
            self.payment_engine = ClientInitiatedPayment(settings, tx.payment_info)

            self.reference_parameter_key = self.payment_engine.reference_parameter_key
            self.form_method = self.payment_engine.form_method
            self.form_target = self.payment_engine.form_target
            self.form_parameters = self.payment_engine.form_parameters

            tx.workspace[WS_PAYMENT_CLIENT_INITIATED] = True
        elif self.payment_type == ReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT:
            log.debug("GlobalClaimCodePayment, do_prepare, initializing PaynamicsPayment handler...")

            # initialize PaynamicsPayment handler.
            settings = _get_client_initiated_payment_settings(theater_key, self.payment_gateway)

            if 'token' in tx.payment_info and tx.payment_info['token']:
                if self.org_uuid in [str(CINEMA76_MALLS), str(SM_MALLS)]:
                    log.debug("GlobalClaimCodePayment, do_prepare, initializing PaynamicsPaymentWithToken3D handler...")
                    self.payment_engine = PaynamicsPaymentWithToken3D(settings, tx.payment_info)
                    tx.workspace[WS_PAYMENT_3D_TOKEN_PAYNAMICS] = True
                else:
                    log.debug("GlobalClaimCodePayment, do_prepare, initializing PaynamicsPaymentWithToken handler...")
                    self.payment_engine = PaynamicsPaymentWithToken(settings, tx.payment_info)
                    tx.workspace[WS_PAYMENT_TOKEN_PAYNAMICS] = True
            else:
                self.payment_engine = PaynamicsPayment(settings, tx.payment_info, tx.workspace['theaters.org_id'][0])
                self.form_method = self.payment_engine.form_method
                self.form_target = self.payment_engine.form_target
                self.form_parameters = self.payment_engine.form_parameters

            if self.org_uuid == str(ROCKWELL_MALLS):
                log.debug("GlobalClaimCodePayment, do_prepare, Rockwell (Power Plant Mall) settings...")

                tx.workspace[WS_PAYMENT_POWERPLANTMALL_PAYNAMICS] = True
            elif self.org_uuid == str(GREENHILLS_MALLS):
                log.debug("GlobalClaimCodePayment, do_prepare, Greenhills Malls settings...")

                tx.workspace[WS_PAYMENT_GREENHILLS_PAYNAMICS] = True
            elif self.org_uuid == str(CINEMA76_MALLS):
                log.debug("GlobalClaimCodePayment, do_prepare, Cinema 76 Malls settings...")

                tx.workspace[WS_PAYMENT_CINEMA76_PAYNAMICS] = True
            elif self.org_uuid == str(GLOBE_EVENTS):
                log.debug("GlobalClaimCodePayment, do_prepare, Globe Events settings...")

                tx.workspace[WS_PAYMENT_GLOBEEVENTS_PAYNAMICS] = True
            elif self.org_uuid == str(SM_MALLS):
                log.debug("GlobalClaimCodePayment, do_prepare, SM Malls settings...")

                tx.workspace[WS_PAYMENT_SMMALLS_PAYNAMICS] = True
            elif self.org_uuid == str(ROBINSONS_MALLS):
                log.debug("GlobalClaimCodePayment, do_prepare, Robinsons Malls settings...")

                tx.workspace[WS_PAYMENT_ROBINSONS_PAYNAMICS] = True
            else:
                log.warn("ERROR, GlobalClaimCodePayment, do_prepare, theaterorg_id not supported...")

                tx.error_info['error_msg'] = "theaterorg_id not supported"
                return 'error', self.err_message
        elif self.payment_type == ReservationTransaction.allowed_payment_types.MIGS_PAYMENT:
            log.debug("GlobalClaimCodePayment, do_prepare, initializing MIGSPayment handler...")

            # initialize MIGSPayment handler.
            settings = _get_client_initiated_payment_settings(theater_key, self.payment_gateway)
            self.payment_engine = MIGSPayment(settings, tx.payment_info)

            self.form_method = self.payment_engine.form_method
            self.form_target = self.payment_engine.form_target
            self.form_parameters = self.payment_engine.form_parameters

            if self.org_uuid == str(ROBINSONS_MALLS):
                log.debug("GlobalClaimCodePayment, do_prepare, Robinsons Malls settings...")

                tx.workspace[WS_PAYMENT_ROBINSONSMALLS_MIGS] = True
            else:
                log.warn("ERROR, GlobalClaimCodePayment, do_prepare, theaterorg_id not supported...")

                tx.error_info['error_msg'] = "theaterorg_id not supported"
                return 'error', self.err_message
        elif self.payment_type == ReservationTransaction.allowed_payment_types.PROMO_CODE:
            log.debug("GlobalClaimCodePayment, do_prepare, initializing PromoCodePayment handler...")
            self.payment_engine = PromoCodePayment(tx.payment_info)

            if self.org_uuid == str(SM_MALLS):
                log.debug("GlobalClaimCodePayment, do_prepare, SM Malls settings...")

                tx.workspace[WS_PAYMENT_SMMALLS_PROMOCODE] = True
            elif self.org_uuid == str(ROCKWELL_MALLS):
                log.debug("GlobalClaimCodePayment, do_prepare, Rockwell (Power PLant Mall) settings...")

                tx.workspace[WS_PAYMENT_POWERPLANTMALL_PROMOCODE] = True
            elif self.org_uuid == str(GREENHILLS_MALLS):
                log.debug("GlobalClaimCodePayment, do_prepare, Greenhills Malls settings...")

                tx.workspace[WS_PAYMENT_GREENHILLS_PROMOCODE] = True
            elif self.org_uuid == str(CINEMA76_MALLS):
                log.debug("GlobalClaimCodePayment, do_prepare, Cinema 76 Malls settings...")

                tx.workspace[WS_PAYMENT_CINEMA76_PROMOCODE] = True
            elif self.org_uuid == str(GLOBE_EVENTS):
                log.debug("GlobalClaimCodePayment, do_prepare, Globe Events settings...")

                tx.workspace[WS_PAYMENT_GLOBEEVENTS_PROMOCODE] = True
            elif self.org_uuid == str(ROBINSONS_MALLS):
                log.debug("GlobalClaimCodePayment, do_prepare, Robinsons Malls settings...")

                tx.workspace[WS_PAYMENT_ROBINSONS_PROMOCODE] = True
        else:
            log.warn("ERROR!, GlobalClaimCodePayment, do_prepare, payment_type not supported...")

            tx.error_info['error_msg'] = "payment_type not supported: %s" % self.payment_type
            return 'error', self.err_message

        log.debug("GlobalClaimCodePayment, do_prepare, finalized payment handler...")

        return self.payment_engine.do_prepare(tx)

    def do_payment_tx(self, tx):
        log.debug("GlobalClaimCodePayment, do_payment_tx, start...")

        if self.payment_type == ReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT:
            log.debug("GlobalClaimCodePayment, do_payment_tx, verify PaynamicsPayment handler...")

            # check only if not using token; no response yet on this part if using token
            if not is_token_paynamics(tx):
                namespace = 'paynamics'
                response_code = tx.workspace['response_code']
                response_message = tx.workspace['response_message']

                if response_code != 'GR001' and response_code != 'GR002':
                    log.warn("ERROR, GlobalClaimCodePayment, do_payment_tx, response_code not GR001 or GR002...")

                    tx.error_info['error_msg'] = response_message
                    return 'error', translate_message_via_error_code(namespace, response_code, response_message)
            else:
                dp_result, dp_message = self.payment_engine.do_payment_tx(tx)
                if 'error' == dp_result:
                    log.debug("GlobalClaimCodePayment, do_payment_tx, PaynamicsPaymentWithToken.do_payment_tx error: %s - %s, won't proceed to _do_claimcode_redeem.." % (dp_result, dp_message))
                    return dp_result, dp_message

        elif self.payment_type == ReservationTransaction.allowed_payment_types.MIGS_PAYMENT:
            log.debug("GlobalClaimCodePayment, do_payment_tx, verify MIGSPayment handler...")

            namespace = 'migs'
            response_code = str(tx.workspace['response_code'])
            response_message = tx.workspace['response_message']
            secure_hash_migsresponse = unicode(tx.workspace['secure_hash'])
            secure_hash_gmoviesbackend = unicode(self.generate_signature(tx.workspace['response_parameters']))

            if secure_hash_migsresponse.strip() != secure_hash_gmoviesbackend.strip():
                log.warn("ERROR!, GlobalClaimCodePayment, MIGS, do_payment_tx, secure_hash not matched...")

                tx.error_info['error_msg'] = "secure_hash not matched"
                return 'error', self.err_message

            if response_code != '0':
                log.warn("ERROR!, GlobalClaimCodePayment, do_payment_tx, response_code not 0...")

                tx.error_info['error_msg'] = response_message
                return 'error', translate_message_via_error_code(namespace, response_code, response_message)
        elif self.payment_type in [ReservationTransaction.allowed_payment_types.GCASH, ReservationTransaction.allowed_payment_types.GCASH_APP]:
            log.debug("GlobalClaimCodePayment, do_payment_tx, call GCASH.do_payment_tx first before _do_claimcode_redeem..")

            dp_result, dp_message = self.payment_engine.do_payment_tx(tx)
            if 'error' == dp_result:
                log.debug("GlobalClaimCodePayment, do_payment_tx, GCASH.do_payment_tx error: %s - %s, won't proceed to _do_claimcode_redeem.." % (dp_result, dp_message))
                return dp_result, dp_message
        elif self.payment_type in [ReservationTransaction.allowed_payment_types.GREWARDS]:
            log.debug("GlobalClaimCodePayment, do_payment_tx, call GREWARDS.do_payment_tx first before _do_claimcode_redeem..")

            dp_result, dp_message = self.payment_engine.do_payment_tx(tx)
            if 'error' == dp_result:
                log.debug("GlobalClaimCodePayment, do_payment_tx, GREWARDS.do_payment_tx error: %s - %s, won't proceed to _do_claimcode_redeem.." % (dp_result, dp_message))
                return dp_result, dp_message
       
        try:
            transaction_id = '%s~%s' % (tx.key.parent().id(), tx.key.id())
            convenience_fee = tx.workspace['convenience_fee']
            total_amount = str(tx.total_amount(convenience_fee, True))
            total_discount = tx.workspace['discount::total_discount']

            s = requests.Session()
            req = self._do_claimcode_redeem_prepare(transaction_id, total_amount, total_discount)
            r_status_code, r_json = call_once(self._do_claimcode_redeem, tx, s, req)

            log.debug("GlobalClaimCodePayment, do_payment_tx, GLOBAL CLAIM CODE redeemed response: {}...".format(r_json))
        except Exception, e:
            log.warn("ERROR!, GlobalClaimCodePayment, do_payment_tx, GLOBAL CLAIM CODE connection/response issue...")
            log.error(e)
            tx.error_info['error_msg'] = str(e)

        if self.payment_type == ReservationTransaction.allowed_payment_types.CLIENT_INITIATED:
            log.warn("GlobalClaimCodePayment, do_payment_tx, bypass ClientInitiatedPayment handler...")

            return 'success', None

        if ((self.payment_type in [ReservationTransaction.allowed_payment_types.GCASH, ReservationTransaction.allowed_payment_types.GCASH_APP])
                or (self.payment_type == ReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT and is_token_paynamics(tx))):
            log.debug("GlobalClaimCodePayment, do_payment_tx, GCASH/PaynamicsPaymentWithToken.do_payment_tx success: %s - %s.." % (dp_result, dp_message))
            return dp_result, dp_message
        elif ((self.payment_type in [ReservationTransaction.allowed_payment_types.GREWARDS])
                or (self.payment_type == ReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT and is_token_paynamics(tx))):
            log.debug("GlobalClaimCodePayment, do_payment_tx, GREWARDS/PaynamicsPaymentWithToken.do_payment_tx success: %s - %s.." % (dp_result, dp_message))
            return dp_result, dp_message
        else:
            return self.payment_engine.do_payment_tx(tx)

    def do_refund_tx(self, tx):
        log.debug("GlobalClaimCodePayment, do_refund_tx, start...")

        has_do_refund_tx = hasattr(self.payment_engine, 'do_refund_tx')

        if has_do_refund_tx:
            log.debug("GlobalClaimCodePayment, do_refund_tx, initialized payment handler has do_refund_tx...")

            return self.payment_engine.do_refund_tx(tx)
        else:
            log.warn("SKIP!, GlobalClaimCodePayment, do_refund_tx, initialized payment handler has no attribute do_refund_tx...")

    # used by PesoPay and iPay88 payment gateway.
    def generate_hash(self, reference):
        log.debug("GlobalClaimCodePayment, generate_hash, start...")

        signature = ''
        has_generate_hash = hasattr(self.payment_engine, 'generate_hash')

        if has_generate_hash:
            signature = self.payment_engine.generate_hash(reference)

        return signature

    # used by Paynamics payment gateway.
    def generate_orders_payload(self, tx):
        log.debug("GlobalClaimCodePayment, generate_orders_payload, start...")

        orders_payload = {}
        has_generate_orders_payload = hasattr(self.payment_engine, 'generate_orders_payload')

        if has_generate_orders_payload:
            orders_payload = self.payment_engine.generate_orders_payload(tx)

        return orders_payload

    # used by MIGS payment gateway.
    def generate_order_information(self, tx):
        log.debug("GlobalClaimCodePayment, generate_order_information, start...")

        order_information = ''
        has_generate_order_information = hasattr(self.payment_engine, 'generate_order_information')

        if has_generate_order_information:
            order_information = self.payment_engine.generate_order_information(tx)

        return order_information

    # used by Paynamics and MIGS payment gateway.
    def generate_signature(self, parameters=None):
        log.debug("GlobalClaimCodePayment, generate_signature, start...")

        signature = ''
        has_generate_signature = hasattr(self.payment_engine, 'generate_signature')

        if has_generate_signature and not parameters and self.payment_type == ReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT:
            signature = self.payment_engine.generate_signature()
        elif has_generate_signature and parameters:
            signature = self.payment_engine.generate_signature(parameters)

        return signature

    # used by Paynamics payment gateway.
    def generate_paymentrequest(self):
        log.debug("GlobalClaimCodePayment, generate_paymentrequest, start...")

        payment_request = ''
        has_generate_paymentrequest = hasattr(self.payment_engine, 'generate_paymentrequest')

        if has_generate_paymentrequest:
            payment_request = self.payment_engine.generate_paymentrequest()

        return payment_request

# NOTE: this handler will be used for Rush Rewards codes.
class RewardsPayment():
    def __init__(self, payment_info, discount_info):
        self.err_message = DEFAULT_ERROR_MESSAGE
        self.device_type = ''
        self.payment_type = ''
        self.org_uuid = ''
        self.theater_id = ''
        self.seat_count = ''
        self.movie_id = ''
        self.claim_code = discount_info['claim_code']
        self.claim_code_type = discount_info['claim_code_type']
        self.payment_gateway = discount_info['payment_gateway']
        self.mobile_number = discount_info['mobile_number']
        self.user_id = discount_info['user_id']
        self.rush_id = ''
        self.variant = ''
        self.ticket_price = 0
        self.promo_type = ''
        self.bank_type = ''
        self.show_date = ''

        # initialize variables for payment gateway handlers.
        self.reference_parameter_key = None # used by PesoPay payment gateway.
        self.form_method = None
        self.form_target = None
        self.form_parameters = None

    def _do_claimcode_verify_prepare(self):
        log.debug("RewardsPayment, _do_claimcode_verify_prepare, verifying GLOBAL CLAIM CODE from %s..." % CLAIM_CODES_VERIFY_ENDPOINT)

        headers = {}
        form_data = {}

        headers['LBAPIToken'] = LBAPITOKEN
        form_data['user_id'] = self.user_id
        form_data['seat_count'] = self.seat_count
        form_data['code'] = self.claim_code
        form_data['mobile'] = self.mobile_number
        form_data['theater'] = self.theater_id
        form_data['device_type'] = self.device_type
        form_data['movie'] = self.movie_id
        form_data['rush_id'] = self.rush_id
        form_data['ticket_price'] = self.ticket_price
        form_data['variant'] = self.variant
        form_data['api_type'] = "new" # Fixed value to support old build
        form_data['show_date'] = self.show_date
        log.debug("RewardsPayment, _do_claimcode_verify_prepare, params: {0}...".format(form_data))

        req = requests.Request('POST', url=CLAIM_CODES_VERIFY_ENDPOINT, headers=headers, data=form_data).prepare()

        return req

    def _do_claimcode_redeem_prepare(self, transaction_id, total_amount, total_discount):
        log.debug("RewardsPayment, _do_claimcode_redeem_prepare, redeeming GLOBAL CLAIM CODE from %s..." % CLAIM_CODES_REDEEM_ENDPOINT)

        headers = {}
        form_data = {}

        headers['LBAPIToken'] = LBAPITOKEN
        form_data['user_id'] = self.user_id
        form_data['seat_count'] = self.seat_count
        form_data['code'] = self.claim_code
        form_data['mobile'] = self.mobile_number
        form_data['theater'] = self.theater_id
        form_data['device_type'] = self.device_type
        form_data['discount'] = total_discount
        form_data['total_amount'] = total_amount
        form_data['transaction_id'] = transaction_id
        form_data['payment_method'] = self.payment_gateway
        form_data['movie'] = self.movie_id
        form_data['rush_id'] = self.rush_id
        form_data['promo_type'] = self.promo_type
        form_data['bank_type'] = self.bank_type
        form_data['variant'] = self.variant
        form_data['show_date'] = self.show_date
        log.debug("RewardsPayment, _do_claimcode_redeem_prepare, params: {0}...".format(form_data))

        req = requests.Request('POST', url=CLAIM_CODES_REDEEM_ENDPOINT, headers=headers, data=form_data).prepare()

        return req

    def _do_claimcode_verify(self, s, req):
        r = s.send(req, timeout=TIMEOUT_DEADLINE)

        return r.status_code, r.text

    def _do_claimcode_redeem(self, s, req):
        r = s.send(req, timeout=TIMEOUT_DEADLINE)

        return r.status_code, r.text

    def _do_parse_float_to_string(self, float_parameter, string_format='{:.2f}'):
        return string_format.format(float_parameter)

    # FIXME!, discounted_seat_price is used for multiple tickets transaction,
    # assuming that the discount will reflect/display only on the last generated ticket.
    def _do_discount_computation(self, total_amount, total_seat_price, ticket_count, discount_value, discount_type, discounted_seats=0):
        log.debug("RewardsPayment, _do_discount_computation, computing discounts...")

        total_discount = 0.0
        actual_discounted_seats = ticket_count
        discounted_seats = int(discounted_seats)

        # computation for rush-rewards (same as free tickets)
        if discount_type == 'rush-rewards':
            if ticket_count > discounted_seats:
                actual_discounted_seats = discounted_seats

            # total discount
            total_discount = float(total_seat_price) * actual_discounted_seats

        # discount per seat
        total_seat_discount = float(total_discount) / actual_discounted_seats
        # amount after discounts
        discounted_amount = float(total_amount) - float(total_discount)
        # amount after discount per seat
        discounted_seat_price = float(total_seat_price) - float(total_seat_discount)

        if discounted_amount < 0.0:
            discounted_amount = 0.0

        if discounted_seat_price < 0.0:
            discounted_seat_price = 0.0

        total_discount = self._do_parse_float_to_string(total_discount)
        total_seat_discount = self._do_parse_float_to_string(total_seat_discount)
        discounted_amount = self._do_parse_float_to_string(discounted_amount)
        discounted_seat_price = self._do_parse_float_to_string(discounted_seat_price)

        return total_discount, total_seat_discount, discounted_amount, discounted_seat_price, actual_discounted_seats

    def do_prepare(self, tx):
        log.debug("RewardsPayment, do_prepare, start...")

        discount_value = '0.00'
        discount_type = 'rush-rewards'
        convenience_fee = tx.workspace['convenience_fee']
        seat_price = str(tx.workspace['schedules.price'][0])
        total_amount = str(tx.total_amount(convenience_fee, True))
        total_seat_price = str(tx.total_seat_price(convenience_fee, True))
        ticket_count = int(tx.ticket_count())
        theater_key = tx.workspace['theaters.key'][0]
        theater_code = tx.workspace['theaters.org_theater_code'][0]
        payment_options_settings = tx.workspace['theaters.payment_options_settings']
        discounted_seats = 0

        self.org_uuid = tx.workspace['theaters.org_id'][0]
        self.theater_id = id_encoder.encoded_theater_id(theater_key)
        self.device_type = tx.platform if tx.platform else ''
        self.seat_count = str(tx.reservation_count())
        self.payment_type = tx.payment_type
        self.movie_id = tx.workspace['schedules.movie'][0].key.id()
        self.rush_id = tx.user_info['rush_id']
        self.variant = tx.workspace['schedules.variant']
        self.ticket_price = tx.workspace['schedules.price']
        self.show_date = tx.workspace['schedules.show_datetime'][0].strftime('%Y-%m-%d')

        # verify whitedlisted theaters for discount codes.
        if payment_options_settings:
            whitelist_theaters = payment_options_settings.allow_discount_codes_whitelist

            if not whitelist_theaters:
                log.warn("ERROR!, RewardsPayment, do_prepare, not whitelist_theaters...")

                tx.error_info['error_msg'] = "not whitelist_theaters"
                return 'error', self.err_message

            if self.org_uuid not in whitelist_theaters:
                log.warn("ERROR!, RewardsPayment, do_prepare, org_uuid not in whitelist_theaters...")

                tx.error_info['error_msg'] = "org_uuid not in whitelist_theaters"
                return 'error', self.err_message

            if self.theater_id not in whitelist_theaters[self.org_uuid]:
                log.warn("ERROR!, RewardsPayment, do_prepare, theater_id not in whitelist_theaters...")

                tx.error_info['error_msg'] = "theater_id not in whitelist_theaters"
                return 'error', self.err_message

        s = requests.Session()
        req = self._do_claimcode_verify_prepare()
        r_status_code, r_json = call_once(self._do_claimcode_verify, tx, s, req)

        log.debug("RewardsPayment, do_prepare, GLOBAL CLAIM CODE verify response: {}...".format(r_json))

        if r_status_code != 200:
            log.warn("ERROR!, RewardsPayment, do_prepare, GLOBAL CLAIM CODE request status_code error...")

            tx.error_info['error_msg'] = "GLOBAL CLAIM CODE request status_code error: %s" % r_status_code
            return 'error', self.err_message

        d_json = json.loads(r_json)

        if 'response' not in d_json:
            log.warn("ERROR!, RewardsPayment, do_prepare, missing key response in json response...")

            tx.error_info['error_msg'] = "missing key response in json response"
            return 'error', self.err_message

        if 'status' not in d_json['response']:
            log.warn("ERROR!, RewardsPayment, do_prepare, missing key status in json response...")

            tx.error_info['error_msg'] = "missing key status in json response"
            return 'error', self.err_message

        if d_json['response']['status'] != 1:
            log.warn("ERROR!, RewardsPayment, do_prepare, status is not equal to 1...")

            if 'details' in d_json['response']:
                if 'msg' in d_json['response']['details']:
                    tx.error_info['error_msg'] = d_json['response']['details']['msg']
                    return 'error', d_json['response']['details']['msg']

            tx.error_info['error_msg'] = "RewardsPayment, do_prepare, status is not equal to 1: %s" % d_json['response']['status']
            return 'error', self.err_message

        if 'details' not in d_json['response']:
            log.warn("ERROR!, RewardsPayment, do_prepare, missing key details in json response...")

            tx.error_info['error_msg'] = "missing key details in json response"
            return 'error', self.err_message

        if 'discount_value' not in d_json['response']['details']:
            log.warn("ERROR!, RewardsPayment, do_prepare, missing key discount_value in json response...")

            tx.error_info['error_msg'] = "missing key discount_value in json response"
            return 'error', self.err_message

        if 'discount_type' not in d_json['response']['details']:
            log.warn("ERROR!, RewardsPayment, do_prepare, missing key discount_type in json response...")

            tx.error_info['error_msg'] = "missing key discount_type in json response"
            return 'error', self.err_message

        if 'promo_type' not in d_json['response']['details']:
            log.warn("ERROR!, RewardsPayment, do_prepare, missing key promo_type in json response...")

            tx.error_info['error_msg'] = "missing key promo_type in json response"
            return 'error', self.err_message

        discount_value = d_json['response']['details']['discount_value']
        discount_type = d_json['response']['details']['discount_type']
        self.promo_type = d_json['response']['details']['promo_type']
        if 'bank_type' in d_json['response']['details']:
            self.bank_type = d_json['response']['details']['bank_type']

        if discount_type not in ['rush-rewards']:
            log.warn("ERROR!, RewardsPayment, do_prepare, discount_type is not equal to ['rush-rewards'], discount_type : %s..." % discount_type)

            tx.error_info['error_msg'] = "discount_type not supported: %s" % (discount_type)
            return 'error', self.err_message

        # Check if discount_type 'rush-rewards' has discounted_seats.
        # Needed for computation of discount.
        if 'discounted_seats' not in d_json['response']['details']:
            log.warn("ERROR!, RewardsPayment, do_prepare, missing key discounted_seats in json response...")
            tx.error_info['error_msg'] = "missing key discounted_seats in json response"
            return 'error', self.err_message
        else:
            discounted_seats = d_json['response']['details']['discounted_seats']

        total_discount, total_seat_discount, discounted_amount, discounted_seat_price, actual_discounted_seats = self._do_discount_computation(
                total_amount, total_seat_price, ticket_count, discount_value, discount_type, discounted_seats)

        tx.workspace['discount::is_discounted'] = True
        tx.workspace['discount::total_discount'] = total_discount
        tx.workspace['discount::multiple_tickets::total_seat_discount'] = total_seat_discount
        tx.workspace['discount::total_amount'] = discounted_amount
        tx.workspace['discount::multiple_tickets::total_seat_price'] = discounted_seat_price
        tx.workspace['discount::multiple_tickets::actual_discounted_seats'] = actual_discounted_seats
        tx.workspace['discount::discount_value'] = str(total_discount) # discount_value for rush is in points
        tx.workspace['discount::discount_type'] = discount_type
        tx.workspace['discount::actual_discounted_seats'] = actual_discounted_seats

        tx.is_discounted = True
        tx.discount_info['original_total_amount'] = total_amount
        tx.discount_info['total_discount'] = total_discount
        tx.discount_info['total_amount'] = discounted_amount
        tx.discount_info['discount_value'] = str(total_discount) # discount_value for rush is in points
        tx.discount_info['discount_type'] = discount_type
        tx.discount_info['actual_discounted_seats'] = actual_discounted_seats

        log.debug("RewardsPayment, do_prepare, preparing payment handler...")

        # prepare payment handler.
        if self.payment_type in [ReservationTransaction.allowed_payment_types.GCASH, ReservationTransaction.allowed_payment_types.GCASH_APP]:
            log.debug("RewardsPayment, do_prepare, initializing GCashPayment handler...")

            # initialize GCashPayment handler.
            self.payment_engine = GCashPayment(tx.payment_info)

            if self.org_uuid == str(SM_MALLS):
                log.debug("RewardsPayment, do_prepare, SM Malls settings...")

                tx.workspace[WS_PAYMENT_SMMALLS_GCASH] = True
            elif self.org_uuid == str(ROCKWELL_MALLS):
                log.debug("RewardsPayment, do_prepare, Rockwell (Power Plant Mall) settings...")

                tx.workspace[WS_PAYMENT_POWERPLANTMALL_GCASH] = True
            elif self.org_uuid == str(GREENHILLS_MALLS):
                log.debug("RewardsPayment, do_prepare, Greenhills Malls settings...")

                tx.workspace[WS_PAYMENT_GREENHILLS_GCASH] = True
            elif self.org_uuid == str(CINEMA76_MALLS):
                log.debug("RewardsPayment, do_prepare, Cinema 76 Malls settings...")

                tx.workspace[WS_PAYMENT_CINEMA76_GCASH] = True
            elif self.org_uuid == str(GLOBE_EVENTS):
                log.debug("RewardsPayment, do_prepare, Globe Events settings...")

                tx.workspace[WS_PAYMENT_GLOBEEVENTS_GCASH] = True
            elif self.org_uuid == str(AYALA_MALLS):
                log.debug("RewardsPayment, do_prepare, Ayala Malls settings...")

                tx.workspace[WS_PAYMENT_AYALA_GCASH] = True
            elif self.org_uuid == str(ROBINSONS_MALLS):
                log.debug("RewardsPayment, do_prepare, Robinsons settings...")

                tx.workspace[WS_PAYMENT_ROBINSONS_GCASH] = True
            else:
                log.warn("ERROR, RewardsPayment, do_prepare, theaterorg_id not supported...")

                tx.error_info['error_msg'] = "theaterorg_id not supported"
                return 'error', self.err_message 
        elif self.payment_type in [ReservationTransaction.allowed_payment_types.GREWARDS]:
            log.debug("RewardsPayment, do_prepare, initializing GRewardsPayment handler...")

            # initialize GRewardsPayment handler.
            self.payment_engine = GRewardsPayment(tx.payment_info)

            if self.org_uuid == str(AYALA_MALLS):
                log.debug("GRewardsPayment, do_prepare, Ayala Malls settings...")

                tx.workspace[WS_PAYMENT_AYALA_GREWARDS] = True
            else:
                log.warn("ERROR, GRewardsPayment, do_prepare, theaterorg_id not supported...")

                tx.error_info['error_msg'] = "theaterorg_id not supported"
                return 'error', self.err_message
        elif self.payment_type == ReservationTransaction.allowed_payment_types.CLIENT_INITIATED:
            log.debug("RewardsPayment, do_prepare, initializing ClientInitiatedPayment handler...")

            # initialize ClientInitiatedPayment handler.
            settings = _get_client_initiated_payment_settings(theater_key, self.payment_gateway, tx.workspace['theaters.cinemas.cinema_name'])
            self.payment_engine = ClientInitiatedPayment(settings, tx.payment_info)

            self.reference_parameter_key = self.payment_engine.reference_parameter_key
            self.form_method = self.payment_engine.form_method
            self.form_target = self.payment_engine.form_target
            self.form_parameters = self.payment_engine.form_parameters

            tx.workspace[WS_PAYMENT_CLIENT_INITIATED] = True
        elif self.payment_type == ReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT:
            log.debug("RewardsPayment, do_prepare, initializing PaynamicsPayment handler...")

            # initialize PaynamicsPayment handler.
            settings = _get_client_initiated_payment_settings(theater_key, self.payment_gateway)

            if 'token' in tx.payment_info and tx.payment_info['token']:
                if self.org_uuid in [str(CINEMA76_MALLS), str(SM_MALLS)]:
                    log.debug("RewardsPayment, do_prepare, initializing PaynamicsPaymentWithToken3D handler...")
                    self.payment_engine = PaynamicsPaymentWithToken3D(settings, tx.payment_info)
                    tx.workspace[WS_PAYMENT_3D_TOKEN_PAYNAMICS] = True
                else:
                    log.debug("RewardsPayment, do_prepare, initializing PaynamicsPaymentWithToken handler...")
                    self.payment_engine = PaynamicsPaymentWithToken(settings, tx.payment_info)
                    tx.workspace[WS_PAYMENT_TOKEN_PAYNAMICS] = True
            else:
                self.payment_engine = PaynamicsPayment(settings, tx.payment_info, tx.workspace['theaters.org_id'][0])
                self.form_method = self.payment_engine.form_method
                self.form_target = self.payment_engine.form_target
                self.form_parameters = self.payment_engine.form_parameters

            if self.org_uuid == str(ROCKWELL_MALLS):
                log.debug("RewardsPayment, do_prepare, Rockwell (Power Plant Mall) settings...")

                tx.workspace[WS_PAYMENT_POWERPLANTMALL_PAYNAMICS] = True
            elif self.org_uuid == str(GREENHILLS_MALLS):
                log.debug("RewardsPayment, do_prepare, Greenhills Malls settings...")

                tx.workspace[WS_PAYMENT_GREENHILLS_PAYNAMICS] = True
            elif self.org_uuid == str(CINEMA76_MALLS):
                log.debug("RewardsPayment, do_prepare, Cinema 76 Malls settings...")

                tx.workspace[WS_PAYMENT_CINEMA76_PAYNAMICS] = True
            elif self.org_uuid == str(GLOBE_EVENTS):
                log.debug("RewardsPayment, do_prepare, Globe Events settings...")

                tx.workspace[WS_PAYMENT_GLOBEEVENTS_PAYNAMICS] = True
            elif self.org_uuid == str(SM_MALLS):
                log.debug("RewardsPayment, do_prepare, SM Malls settings...")

                tx.workspace[WS_PAYMENT_SMMALLS_PAYNAMICS] = True
            elif self.org_uuid == str(ROBINSONS_MALLS):
                log.debug("RewardsPayment, do_prepare, Robinsons Malls settings...")

                tx.workspace[WS_PAYMENT_ROBINSONS_PAYNAMICS] = True
            else:
                log.warn("ERROR, RewardsPayment, do_prepare, theaterorg_id not supported...")

                tx.error_info['error_msg'] = "theaterorg_id not supported"
                return 'error', self.err_message
        elif self.payment_type == ReservationTransaction.allowed_payment_types.MIGS_PAYMENT:
            log.debug("RewardsPayment, do_prepare, initializing MIGSPayment handler...")

            # initialize MIGSPayment handler.
            settings = _get_client_initiated_payment_settings(theater_key, self.payment_gateway)
            self.payment_engine = MIGSPayment(settings, tx.payment_info)

            self.form_method = self.payment_engine.form_method
            self.form_target = self.payment_engine.form_target
            self.form_parameters = self.payment_engine.form_parameters

            if self.org_uuid == str(ROBINSONS_MALLS):
                log.debug("RewardsPayment, do_prepare, Robinsons Malls settings...")

                tx.workspace[WS_PAYMENT_ROBINSONSMALLS_MIGS] = True
            else:
                log.warn("ERROR, RewardsPayment, do_prepare, theaterorg_id not supported...")

                tx.error_info['error_msg'] = "theaterorg_id not supported"
                return 'error', self.err_message
        elif self.payment_type == ReservationTransaction.allowed_payment_types.PROMO_CODE:
            log.debug("RewardsPayment, do_prepare, initializing PromoCodePayment handler...")
            self.payment_engine = PromoCodePayment(tx.payment_info)

            if self.org_uuid == str(SM_MALLS):
                log.debug("RewardsPayment, do_prepare, SM Malls settings...")

                tx.workspace[WS_PAYMENT_SMMALLS_PROMOCODE] = True
            elif self.org_uuid == str(ROCKWELL_MALLS):
                log.debug("RewardsPayment, do_prepare, Rockwell (Power PLant Mall) settings...")

                tx.workspace[WS_PAYMENT_POWERPLANTMALL_PROMOCODE] = True
            elif self.org_uuid == str(GREENHILLS_MALLS):
                log.debug("RewardsPayment, do_prepare, Greenhills Malls settings...")

                tx.workspace[WS_PAYMENT_GREENHILLS_PROMOCODE] = True
            elif self.org_uuid == str(CINEMA76_MALLS):
                log.debug("RewardsPayment, do_prepare, Cinema 76 Malls settings...")

                tx.workspace[WS_PAYMENT_CINEMA76_PROMOCODE] = True
            elif self.org_uuid == str(GLOBE_EVENTS):
                log.debug("RewardsPayment, do_prepare, Globe Events settings...")

                tx.workspace[WS_PAYMENT_GLOBEEVENTS_PROMOCODE] = True
            elif self.org_uuid == str(ROBINSONS_MALLS):
                log.debug("RewardsPayment, do_prepare, Robinsons Malls settings...")

                tx.workspace[WS_PAYMENT_ROBINSONS_PROMOCODE] = True
        else:
            log.warn("ERROR!, RewardsPayment, do_prepare, payment_type not supported...")

            tx.error_info['error_msg'] = "payment_type not supported: %s" % self.payment_type
            return 'error', self.err_message

        log.debug("RewardsPayment, do_prepare, finalized payment handler...")

        return self.payment_engine.do_prepare(tx)

    def do_payment_tx(self, tx):
        log.debug("RewardsPayment, do_payment_tx, start...")

        if self.payment_type == ReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT:
            log.debug("RewardsPayment, do_payment_tx, verify PaynamicsPayment handler...")

            # check only if not using token; no response yet on this part if using token
            if not is_token_paynamics(tx):
                namespace = 'paynamics'
                response_code = tx.workspace['response_code']
                response_message = tx.workspace['response_message']

                if response_code != 'GR001' and response_code != 'GR002':
                    log.warn("ERROR, RewardsPayment, do_payment_tx, response_code not GR001 or GR002...")

                    tx.error_info['error_msg'] = response_message
                    return 'error', translate_message_via_error_code(namespace, response_code, response_message)
        elif self.payment_type == ReservationTransaction.allowed_payment_types.MIGS_PAYMENT:
            log.debug("RewardsPayment, do_payment_tx, verify MIGSPayment handler...")

            namespace = 'migs'
            response_code = str(tx.workspace['response_code'])
            response_message = tx.workspace['response_message']
            secure_hash_migsresponse = unicode(tx.workspace['secure_hash'])
            secure_hash_gmoviesbackend = unicode(self.generate_signature(tx.workspace['response_parameters']))

            if secure_hash_migsresponse.strip() != secure_hash_gmoviesbackend.strip():
                log.warn("ERROR!, RewardsPayment, MIGS, do_payment_tx, secure_hash not matched...")

                tx.error_info['error_msg'] = "secure_hash not matched"
                return 'error', self.err_message

            if response_code != '0':
                log.warn("ERROR!, RewardsPayment, do_payment_tx, response_code not 0...")

                tx.error_info['error_msg'] = response_message
                return 'error', translate_message_via_error_code(namespace, response_code, response_message)

        try:
            transaction_id = '%s~%s' % (tx.key.parent().id(), tx.key.id())
            convenience_fee = tx.workspace['convenience_fee']
            total_amount = str(tx.total_amount(convenience_fee, True))
            total_discount = tx.workspace['discount::total_discount']

            s = requests.Session()
            req = self._do_claimcode_redeem_prepare(transaction_id, total_amount, total_discount)
            r_status_code, r_json = call_once(self._do_claimcode_redeem, tx, s, req)

            log.debug("RewardsPayment, do_payment_tx, GLOBAL CLAIM CODE redeemed response: {}...".format(r_json))
        except Exception, e:
            log.warn("ERROR!, RewardsPayment, do_payment_tx, GLOBAL CLAIM CODE connection/response issue...")
            log.error(e)
            tx.error_info['error_msg'] = str(e)

        if self.payment_type == ReservationTransaction.allowed_payment_types.CLIENT_INITIATED:
            log.warn("RewardsPayment, do_payment_tx, bypass ClientInitiatedPayment handler...")

            return 'success', None

        return self.payment_engine.do_payment_tx(tx)

    def do_refund_tx(self, tx):
        log.debug("RewardsPayment, do_refund_tx, start...")

        has_do_refund_tx = hasattr(self.payment_engine, 'do_refund_tx')

        if has_do_refund_tx:
            log.debug("RewardsPayment, do_refund_tx, initialized payment handler has do_refund_tx...")

            return self.payment_engine.do_refund_tx(tx)
        else:
            log.warn("SKIP!, RewardsPayment, do_refund_tx, initialized payment handler has no attribute do_refund_tx...")

    # used by PesoPay and iPay88 payment gateway.
    def generate_hash(self, reference):
        log.debug("RewardsPayment, generate_hash, start...")

        signature = ''
        has_generate_hash = hasattr(self.payment_engine, 'generate_hash')

        if has_generate_hash:
            signature = self.payment_engine.generate_hash(reference)

        return signature

    # used by Paynamics payment gateway.
    def generate_orders_payload(self, tx):
        log.debug("RewardsPayment, generate_orders_payload, start...")

        orders_payload = {}
        has_generate_orders_payload = hasattr(self.payment_engine, 'generate_orders_payload')

        if has_generate_orders_payload:
            orders_payload = self.payment_engine.generate_orders_payload(tx)

        return orders_payload

    # used by MIGS payment gateway.
    def generate_order_information(self, tx):
        log.debug("RewardsPayment, generate_order_information, start...")

        order_information = ''
        has_generate_order_information = hasattr(self.payment_engine, 'generate_order_information')

        if has_generate_order_information:
            order_information = self.payment_engine.generate_order_information(tx)

        return order_information

    # used by Paynamics and MIGS payment gateway.
    def generate_signature(self, parameters=None):
        log.debug("RewardsPayment, generate_signature, start...")

        signature = ''
        has_generate_signature = hasattr(self.payment_engine, 'generate_signature')

        if has_generate_signature and not parameters and self.payment_type == ReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT:
            signature = self.payment_engine.generate_signature()
        elif has_generate_signature and parameters:
            signature = self.payment_engine.generate_signature(parameters)

        return signature

    # used by Paynamics payment gateway.
    def generate_paymentrequest(self):
        log.debug("RewardsPayment, generate_paymentrequest, start...")

        payment_request = ''
        has_generate_paymentrequest = hasattr(self.payment_engine, 'generate_paymentrequest')

        if has_generate_paymentrequest:
            payment_request = self.payment_engine.generate_paymentrequest()

        return payment_request

class GCashPayment():
    def __init__(self, payment_info):
        self.err_message = DEFAULT_ERROR_MESSAGE
        self.gcash_account = payment_info['gcash_account']
        self.otp = payment_info['otp']
        self.device_type = ''

    def _do_gcash_otp_verify_prepare(self):
        log.debug("GCashPayment, _do_gcash_otp_verify_prepare, preparing OTP verify from %s..." % GCASH_OTP_ENDPOINT)

        headers = {'LBAPIToken': LBAPITOKEN}
        form_data = {}
        form_data['mobile'] = self.gcash_account
        form_data['pin'] = self.otp
        form_data['device_type'] = self.device_type
        req = requests.Request('POST', url=GCASH_OTP_ENDPOINT, headers=headers, data=form_data).prepare()

        return req

    def _do_gcash_payment_prepare(self, params, theaterorg_id):
        headers = {'GMOVIESTOKEN': GMOVIESGCASHTOKEN}
        form_data = {}
        form_data['mobile'] = params['mobile']
        form_data['amount'] = params['amount']
        form_data['transaction_id'] = params['transaction_id']

        if theaterorg_id == str(AYALA_MALLS):
            form_data['theater_id'] = params['theater_id']
            url_endpoint            = GCASH_PAYMENT_ENDPOINT_AYALA
        elif theaterorg_id == str(ROBINSONS_MALLS):
            form_data['theater_id'] = params['theater_id']
            url_endpoint            = GCASH_PAYMENT_ENDPOINT_ROBINSONS
        else:
            url_endpoint            = GCASH_PAYMENT_ENDPOINT

        log.debug("GCashPayment, _do_gcash_payment_prepare, preparing GCash Sell Command for theaterorg_id: %s..." % theaterorg_id)
        log.debug("GCashPayment, _do_gcash_payment_prepare, preparing GCash Sell Command from %s..." % url_endpoint)
        log.debug("GCashPayment, _do_gcash_payment_prepare, payload: {}...".format(form_data))

        req = requests.Request('POST', url=url_endpoint, headers=headers, data=form_data).prepare()

        return req

    def _do_gcash_refund_prepare(self, transaction_id, theaterorg_id):
        refund_endpoint = GCASH_REFUND_ENDPOINT_AYALA

        if theaterorg_id in [str(AYALA_MALLS), str(ROBINSONS_MALLS)]:
            refund_endpoint = GCASH_REFUND_ENDPOINT_AYALA
        # FOR ROB GCASH: Double check if endpoint for refund in Robinsons GCASH will be the same as in Ayala
        # If they are the same just add it in the condition above
        # as in "if theaterorg_id in [str(AYALA_MALLS), str(ROBINSONS_MALLS)]"
        else:
            refund_endpoint = GCASH_REFUND_ENDPOINT

        log.debug("GCashPayment, _do_gcash_refund_prepare, preparing GCash Transfer Command from %s, with txn_id: %s..." % (refund_endpoint, transaction_id))

        headers = {'GMOVIESTOKEN': GMOVIESGCASHTOKEN}
        refund_url = refund_endpoint + '/' + transaction_id
        req = requests.Request('GET', url=refund_url, headers=headers).prepare()

        return req

    def _do_gcash_otp_verify(self, s, req):
        r = s.send(req, timeout=TIMEOUT_DEADLINE)

        return r.status_code, r.text

    def _do_gcash_payment(self, s, req):
        r = s.send(req, timeout=TIMEOUT_DEADLINE)

        return r.status_code, r.text

    def _do_gcash_refund(self, s, req):
        r = s.send(req, timeout=TIMEOUT_DEADLINE)

        return r.status_code, r.text

    def do_prepare(self, tx):
        self.device_type = tx.platform if tx.platform else ''
        theaterorg_id = tx.workspace['theaters.org_id'][0]

        s = requests.Session()
        req = self._do_gcash_otp_verify_prepare()
        r_status_code, r_json = call_once(self._do_gcash_otp_verify, tx, s, req)

        log.debug("GCashPayment, OTP, verify response: {}...".format(r_json))

        if r_status_code != 200:
            tx.error_info['error_msg'] = "GCashPayment, OTP verify status_code not 200: %s" % r_status_code
            return 'error', self.err_message

        d_json = json.loads(r_json)

        if 'response' not in d_json:
            tx.error_info['error_msg'] = "missing key response in json response"
            return 'error', self.err_message

        if 'status' not in d_json['response']:
            tx.error_info['error_msg'] = "missing key status in json response"
            return 'error', self.err_message

        if d_json['response']['status'] == 0:
            if 'msg' in d_json['response']:
                tx.error_info['error_msg'] = d_json['response']['msg']
                return 'error', d_json['response']['msg']

            tx.error_info['error_msg'] = "GCashPayment, OTP verify status is 0 (error)"
            return 'error', self.err_message

        # do_prepare for other payment engine if necessary.
        if theaterorg_id == str(SM_MALLS):
            log.debug("GCashPayment, do_prepare, SM Malls...")

            self.payment_engine = EPlusPayment({'card_number': EPLUS_CARDNUMBER, 'pin': EPLUS_PIN})
        elif theaterorg_id == str(AYALA_MALLS):
            log.debug("GCashPayment, do_prepare, Ayala Malls...")

            return 'success', None
        elif theaterorg_id == str(ROCKWELL_MALLS):
            log.debug("GCashPayment, do_prepare, Rockwell (Power Plant Mall)...")

            return 'success', None
        elif theaterorg_id == str(GREENHILLS_MALLS):
            log.debug("GCashPayment, do_prepare, Greenhills Malls...")

            return 'success', None
        elif theaterorg_id == str(CINEMA76_MALLS):
            log.debug("GCashPayment, do_prepare, Cinema 76 Malls...")

            return 'success', None
        elif theaterorg_id == str(GLOBE_EVENTS):
            log.debug("GCashPayment, do_prepare, Globe Events...")

        elif theaterorg_id == str(ROBINSONS_MALLS):
            log.debug("GCashPayment, do_prepare, Robinsons...")

            return 'success', None
        else:
            log.warn("ERROR!, GCashPayment, do_prepare, theaterorg_id is not supported...")

            tx.error_info['error_msg'] = "theaterorg_id is not supported"
            return 'error', self.err_message

        return self.payment_engine.do_prepare(tx)

    def do_payment_tx(self, tx):
        theaterorg_id = tx.workspace['theaters.org_id'][0]

        try:
            gcash_payment_payload = {}
            gcash_payment_payload['transaction_id'] = '%s~%s' % (tx.key.parent().id(), tx.key.id())
            gcash_payment_payload['mobile'] = self.gcash_account
            gcash_payment_payload['amount'] = tx.workspace['RSVP:totalamount']
            theater_key = tx.workspace['theaters.key'][0]
            gcash_payment_payload['theater_id'] = id_encoder.encoded_theater_id(theater_key)
            if 'theaters.org_theater_code' in tx.workspace and 'ATC' == tx.workspace['theaters.org_theater_code'][0]:
                suffix = '-2'
                if 'theaters.cinemas.cinema_name' in tx.workspace and '1' == tx.workspace['theaters.cinemas.cinema_name']:
                    suffix = '-1'
                gcash_payment_payload['theater_id'] = gcash_payment_payload['theater_id'] + suffix

            s = requests.Session()
            req = self._do_gcash_payment_prepare(gcash_payment_payload, theaterorg_id)
            r_status_code, r_json = call_once(self._do_gcash_payment, tx, s, req)

            log.debug("GCashPayment, gcash payment results: {}...".format(r_json))

            if r_status_code != 200:
                tx.error_info['error_msg'] = "GCashPayment, payment status_code not 200: %s" % r_status_code
                return 'error', self.err_message

            d_json = json.loads(r_json)

            if 'response' not in d_json:
                tx.error_info['error_msg'] = "missing key response in json response"
                return 'error', self.err_message

            if 'status' not in d_json['response']:
                tx.error_info['error_msg'] = "missing key status in json response"
                return 'error', self.err_message

            tx.workspace['response_code'] = d_json['response']['status']
            tx.workspace['response_message'] = d_json['response']['message'] if 'message' in d_json['response'] else ''
            if d_json['response']['status'] == 0:
                if 'message' in d_json['response']:
                    namespace = 'gcash'
                    response_code = 'FAILED' # default response code to translate the error messages.
                    response_message = d_json['response']['message']

                    if 'Account not yet registered on GCash' == d_json['response']['message']:
                        response_code = 'NOTREGISTERED'
                    elif 'Your mobile number is not yet registered on GCash. Please select another payment method.' == d_json['response']['message']:
                        response_code = 'NOTREGISTERED'
                    elif 'Insufficient funds' == d_json['response']['message']:
                        response_code = 'INSUFFICIENTFUNDS'
                    elif 'You have insufficient GCash funds. Please cash-in or select another payment method.' == d_json['response']['message']:
                        response_code = 'INSUFFICIENTFUNDS'
                    tx.error_info['error_msg'] = d_json['response']['message']
                    return 'error', translate_message_via_error_code(namespace, response_code, response_message)

                tx.error_info['error_msg'] = "GCashPayment, payment status is 0 (error)"
                return 'error', self.err_message
        except Exception, e:
            log.warn("ERROR!, GCashPayment. do_payment_tx...")
            log.error(e)

            tx.error_info['error_msg'] = str(e)
            return 'error', self.err_message

        # do_payment_tx for other payment engine if necessary.
        if theaterorg_id == str(SM_MALLS):
            log.debug("GCashPayment, do_payment_tx, SM Malls...")

            return 'success', None
        elif theaterorg_id == str(AYALA_MALLS):
            log.debug("GCashPayment, do_payment_tx, Ayala Malls...")

            return do_finalize_transaction(tx)
        elif theaterorg_id == str(ROCKWELL_MALLS):
            log.debug("GCashPayment, do_payment_tx, Rockwell (Power Plant Mall)...")

            return do_finalize_transaction(tx)
        elif theaterorg_id == str(GREENHILLS_MALLS):
            log.debug("GCashPayment, do_payment_tx, Greenhills Malls...")

            return 'success', None
        elif theaterorg_id == str(CINEMA76_MALLS):
            log.debug("GCashPayment, do_payment_tx, Cinema 76 Malls...")

            return do_finalize_transaction(tx)
        elif theaterorg_id == str(GLOBE_EVENTS):
            log.debug("GCashPayment, do_payment_tx, Globe Events...")

            return do_finalize_transaction(tx)
        elif theaterorg_id == str(ROBINSONS_MALLS):
            log.debug("GCashPayment, do_payment_tx, Robinsons...")

            return do_finalize_transaction(tx)
        else:
            log.warn("ERROR!, GCashPayment, do_payment_tx, theaterorg_id is not supported...")

        tx.error_info['error_msg'] = "theaterorg_id is not supported"
        return 'error', self.err_message

    def do_refund_tx(self, tx):
        theaterorg_id = tx.workspace['theaters.org_id'][0]
        log.warn("START!, Refund process GCashPayment, theaterorg_id: %s..." % theaterorg_id)

        try:
            transaction_id = '%s~%s' % (tx.key.parent().id(), tx.key.id())
            s = requests.Session()
            req = self._do_gcash_refund_prepare(transaction_id, theaterorg_id)
            r_status_code, r_json = call_once(self._do_gcash_refund, tx, s, req)

            log.debug("GCashPayment, refund status_code: %s" % r_status_code)
            log.debug("GCashPayment, refund results: {}...".format(r_json))
        except Exception, e:
            log.warn("ERROR!, GCashPayment. do_refund_tx...")
            log.error(e)

# NOTE: this handler will be use for globe rewards 917 promo using mpass engine
class GRewardsPayment():
    def __init__(self, payment_info):
        self.err_message = DEFAULT_ERROR_MESSAGE
        self.grewards_account = payment_info['grewards_account']
        self.otp = payment_info['otp']
        self.device_type = ''

    def _do_grewards_otp_verify_prepare(self):
        log.debug("GRewardsPayment, _do_grewards_otp_verify_prepare, preparing OTP verify from %s..." % GREWARDS_OTP_ENDPOINT)

        headers = {'LBAPIToken': LBAPITOKEN}
        form_data = {}
        form_data['mobile'] = self.grewards_account
        form_data['pin'] = self.otp
        form_data['device_type'] = self.device_type
        req = requests.Request('POST', url=GREWARDS_OTP_ENDPOINT, headers=headers, data=form_data).prepare()

        return req

    def _do_grewards_payment_prepare(self, params, theaterorg_id):
        headers = {'GMOVIESTOKEN': GMOVIESGREWARDSTOKEN}
        form_data = {}
        form_data['mobile'] = params['mobile']
        form_data['amount'] = params['amount']
        form_data['transaction_id'] = params['transaction_id']

        if theaterorg_id == str(AYALA_MALLS):
            form_data['theater_id'] = params['theater_id']
            url_endpoint            = GREWARDS_PAYMENT_ENDPOINT_AYALA
        else:
            url_endpoint            = GREWARDS_PAYMENT_ENDPOINT

        log.debug("GRewardsPayment, _do_grewards_payment_prepare, preparing GRewards Sell Command for theaterorg_id: %s..." % theaterorg_id)
        log.debug("GRewardsPayment, _do_grewards_payment_prepare, preparing GRewards Sell Command from %s..." % url_endpoint)
        log.debug("GRewardsPayment, _do_grewards_payment_prepare, payload: {}...".format(form_data))

        req = requests.Request('POST', url=url_endpoint, headers=headers, data=form_data).prepare()

        return req

    def _do_grewards_refund_prepare(self, transaction_id, theaterorg_id):
        refund_endpoint = GREWARDS_REFUND_ENDPOINT_AYALA

        if theaterorg_id in [str(AYALA_MALLS)]:
            refund_endpoint = GREWARDS_REFUND_ENDPOINT_AYALA
        # FOR ROB GREWARDS AND OTHERS CINEMA PARTNERS: Double check if endpoint for refund in Robinsons GREWARDS will be the same as in Ayala
        # If they are the same just add it in the condition above
        # as in "if theaterorg_id in [str(AYALA_MALLS), str(ROBINSONS_MALLS)]"
        else:
            refund_endpoint = GREWARDS_REFUND_ENDPOINT

        log.debug("GRewardsPayment, _do_grewards_refund_prepare, preparing GRewards Transfer Command from %s, with txn_id: %s..." % (refund_endpoint, transaction_id))

        headers = {'GMOVIESTOKEN': GMOVIESGREWARDSTOKEN}
        refund_url = refund_endpoint + '/' + transaction_id
        req = requests.Request('GET', url=refund_url, headers=headers).prepare()

        return req

    def _do_grewards_otp_verify(self, s, req):
        r = s.send(req, timeout=TIMEOUT_DEADLINE)

        return r.status_code, r.text

    def _do_grewards_payment(self, s, req):
        r = s.send(req, timeout=TIMEOUT_DEADLINE)

        return r.status_code, r.text

    def _do_grewards_refund(self, s, req):
        r = s.send(req, timeout=TIMEOUT_DEADLINE)

        return r.status_code, r.text

    def do_prepare(self, tx):
        self.device_type = tx.platform if tx.platform else ''
        theaterorg_id = tx.workspace['theaters.org_id'][0]

        s = requests.Session()
        req = self._do_grewards_otp_verify_prepare()
        r_status_code, r_json = call_once(self._do_grewards_otp_verify, tx, s, req)

        log.debug("GRewardsPayment, OTP, verify response: {}...".format(r_json))

        if r_status_code != 200:
            tx.error_info['error_msg'] = "GRewardsPayment, OTP verify status_code not 200: %s" % r_status_code
            return 'error', self.err_message

        d_json = json.loads(r_json)

        if 'response' not in d_json:
            tx.error_info['error_msg'] = "missing key response in json response"
            return 'error', self.err_message

        if 'status' not in d_json['response']:
            tx.error_info['error_msg'] = "missing key status in json response"
            return 'error', self.err_message

        if d_json['response']['status'] == 0:
            if 'msg' in d_json['response']:
                tx.error_info['error_msg'] = d_json['response']['msg']
                return 'error', d_json['response']['msg']

            tx.error_info['error_msg'] = "GRewardsPayment, OTP verify status is 0 (error)"
            return 'error', self.err_message

        # do_prepare for other payment engine if necessary.
        if theaterorg_id == str(AYALA_MALLS):
            log.debug("GRewardsPayment, do_prepare, Ayala Malls...")
            
            self.payment_engine = MPassPayment({'username' : PROMOCODE_USERNAME, 'password' : PROMOCODE_PASSWORD})
        else:
            log.warn("ERROR!, GRewardsPayment, do_prepare, theaterorg_id is not supported...")

            tx.error_info['error_msg'] = "theaterorg_id is not supported"
            return 'error', self.err_message

        return self.payment_engine.do_prepare(tx)

    def do_payment_tx(self, tx):
        theaterorg_id = tx.workspace['theaters.org_id'][0]

        try:
            grewards_payment_payload = {}
            grewards_payment_payload['transaction_id'] = '%s~%s' % (tx.key.parent().id(), tx.key.id())
            grewards_payment_payload['mobile'] = self.grewards_account
            grewards_payment_payload['amount'] = tx.workspace['RSVP:totalamount']
            theater_key = tx.workspace['theaters.key'][0]
            grewards_payment_payload['theater_id'] = id_encoder.encoded_theater_id(theater_key)
            if 'theaters.org_theater_code' in tx.workspace and 'ATC' == tx.workspace['theaters.org_theater_code'][0]:
                suffix = '-2'
                if 'theaters.cinemas.cinema_name' in tx.workspace and '1' == tx.workspace['theaters.cinemas.cinema_name']:
                    suffix = '-1'
                grewards_payment_payload['theater_id'] = grewards_payment_payload['theater_id'] + suffix

            s = requests.Session()
            req = self._do_grewards_payment_prepare(grewards_payment_payload, theaterorg_id)
            r_status_code, r_json = call_once(self._do_grewards_payment, tx, s, req)

            log.debug("GRewardsPayment, grewards payment results: {}...".format(r_json))

            if r_status_code != 200:
                tx.error_info['error_msg'] = "GRewardsPayment, payment status_code not 200: %s" % r_status_code
                return 'error', self.err_message

            d_json = json.loads(r_json)

            if 'status' not in d_json:
                tx.error_info['error_msg'] = "missing key status in json response"
                return 'error', self.err_message
            
            tx.workspace['response_code'] = r_status_code
            tx.workspace['response_message'] = d_json['data'] if 'data' in d_json else ''
            if d_json['status'] is not 201:
                log.warn("Redeem Status : %s Response: %s" % (d_json['status'], d_json['data']))
                log.warn("Redeem Status : %s Response: %s" % (d_json['status'], d_json['data']))

                tx.error_info['error_msg'] = "GRewardsPayment, payment status is %s (error): %s" % (d_json['status'], d_json['data']['resultDescription'])
                return 'error', self.err_message

        except Exception, e:
            log.warn("ERROR!, GRewardsPayment. do_payment_tx...")
            log.error(e)

            tx.error_info['error_msg'] = str(e)
            return 'error', self.err_message

        # do_payment_tx for other payment engine if necessary.
        if theaterorg_id == str(AYALA_MALLS):
            log.debug("GRewardsPayment, do_payment_tx, Ayala Malls...")

            return do_finalize_transaction(tx)
        else:
            log.warn("ERROR!, GRewardsPayment, do_payment_tx, theaterorg_id is not supported...")

        tx.error_info['error_msg'] = "theaterorg_id is not supported"
        return 'error', self.err_message

    def do_refund_tx(self, tx):
        theaterorg_id = tx.workspace['theaters.org_id'][0]
        log.warn("START!, Refund process GRewardsPayment, theaterorg_id: %s..." % theaterorg_id)

        try:
            transaction_id = '%s~%s' % (tx.key.parent().id(), tx.key.id())
            s = requests.Session()
            req = self._do_grewards_refund_prepare(transaction_id, theaterorg_id)
            r_status_code, r_json = call_once(self._do_grewards_refund, tx, s, req)

            log.debug("GRewardsPayment, refund status_code: %s" % r_status_code)
            log.debug("GRewardsPayment, refund results: {}...".format(r_json))
        except Exception, e:
            log.warn("ERROR!, GRewardsPayment. do_refund_tx...")
            log.error(e)


# TODO: Operator Billing...
class OperatorBillingPayment():
    def __init__(self, payment_info):
        self.err_message = DEFAULT_ERROR_MESSAGE

    def do_prepare(tx):
        return 'error', self.err_message

    def do_payment_tx(tx):
        return 'error', self.err_message

    def do_refund_tx(self, tx):
        log.warn("SKIP!, No refund process for OperatorBillingPayment...")


class EPlusPayment():
    def __init__(self, payment_info):
        self.err_message = DEFAULT_ERROR_MESSAGE
        self.card_number = payment_info['card_number']
        self.pin = payment_info['pin']

    def do_prepare(self, tx):
        def __send_eplus_check_balance_req(card_number, org_uuid, theater_code=None, is_multiple_branch=True):
            status, r_data = check_balance_eplus(card_number, org_uuid, theater_code=theater_code, is_multiple_branch=is_multiple_branch)

            return status, r_data

        try:
            theaterorg_id = tx.workspace['theaters.org_id'][0]
            theater_code = tx.workspace['theaters.org_theater_code'][0]
            convenience_fee = tx.workspace['convenience_fee']
            total_amount = str(tx.total_amount(convenience_fee, True))

            if theaterorg_id == str(SM_MALLS):
                status, r_data = call_once(__send_eplus_check_balance_req, tx, self.card_number, theaterorg_id, theater_code, True)

                if status == 'error':
                    log.warn("ERROR!, EPlusPayment, do_prepare. %s..." % r_data['message'])

                    tx.error_info['error_msg'] = r_data['message']
                    return 'error', self.err_message

                if float(total_amount) > float(r_data['message']):
                    log.warn("ERROR!, EPlusPayment, do_prepare. Insufficient balance...")
                    log.debug("total_amount: %s, remaining_balance: %s..." % (total_amount, r_data['message']))

                    tx.error_info['error_msg'] = "EPlus - Insufficient balance, card number: %s" % self.card_number
                    return 'error', self.err_message

                return 'success', None
        except Exception, e:
            log.warn("ERROR!, EPlusPayment. do_prepare...")
            log.error(e)
            tx.error_info['error_msg'] = str(e)

        return 'error', self.err_message

    def do_payment_tx(self, tx):
        def __send_eplus_payment_req(card_number, pin, payload):
            status, r_data = create_transaction_eplus(card_number, pin, payload)

            return status, r_data

        try:
            theaterorg_id = tx.workspace['theaters.org_id'][0]
            theater_code = tx.workspace['theaters.org_theater_code'][0]

            if theaterorg_id == str(SM_MALLS):
                eplus_debit_amount = tx.workspace['create::amount']

                if 'discount::is_discounted' in tx.workspace and tx.workspace['discount::is_discounted']:
                    log.warn("EPlusPayment, do_payment_tx, SM Malls transaction via eplus is discounted...")
                    log.info("original_amount: %s, discounted_amount: %s..." % (tx.workspace['create::amount'], tx.workspace['RSVP:totalamount']))

                    eplus_debit_amount = tx.workspace['RSVP:totalamount']

                eplus_payload = {}
                eplus_payload['reference_number'] = tx.reservation_reference
                eplus_payload['merchant_reference'] = tx.workspace['create::reference_number']
                eplus_payload['amount'] = float(eplus_debit_amount)
                eplus_payload['transaction_type'] = EPLUS_TRASACTION_TYPE_DEBIT
                eplus_payload['merchant_id'] = EPLUS_MERCHANT_ID
                eplus_payload['merchant_code'] = EPLUS_MERCHANT_CODE
                eplus_payload['currency'] = 'PHP'
                eplus_payload['description'] = 'GMovies (SM Cinemas)'

                eplus_status, eplus_r_data = call_once(__send_eplus_payment_req, tx, self.card_number, self.pin, eplus_payload)

                if eplus_status == 'error':
                    log.warn("ERROR!, do_payment_tx. %s..." % eplus_r_data['message'])

                    if tx.payment_type in [ReservationTransaction.allowed_payment_types.GCASH,
                            ReservationTransaction.allowed_payment_types.GCASH_APP,
                            ReservationTransaction.allowed_payment_types.GREWARDS]:
                        tx.workspace['refund::is_for_refund'] = True

                    tx.error_info['error_msg'] = eplus_r_data['message']
                    return 'error', self.err_message

                log.debug("EPlusPayment, create eplus transaction results: {}...".format(eplus_r_data))

                tx.workspace['eplus::reference_number'] = eplus_r_data['reference_number']
                tx.workspace['eplus::remaining_balance'] = eplus_r_data['remaining_balance']

                return do_finalize_transaction(tx)
        except Exception, e:
            log.warn("ERROR!, EPlusPayment. do_payment_tx...")
            log.error(e)
            tx.error_info['error_msg'] = str(e)

            if tx.payment_type in [ReservationTransaction.allowed_payment_types.GCASH,
                    ReservationTransaction.allowed_payment_types.GCASH_APP,
                    ReservationTransaction.allowed_payment_types.GREWARDS]:
                tx.workspace['refund::is_for_refund'] = True

        return 'error', self.err_message

    def do_refund_tx(self, tx):
        log.warn("SKIP!, No refund process for EPlusPayment...")


class MIGSPayment():
    def __init__(self, settings, payment_info):
        self.err_message = DEFAULT_ERROR_MESSAGE
        self.settings = settings
        self.form_method = 'POST'
        self.form_parameters = dict(settings.default_params)
        self.form_target = settings.settings.get('default-form-target')
        merchant_id = settings.settings.get('migs::merchant-id')
        access_code = settings.settings.get('migs::access-code')
        title = settings.settings.get('migs::title')

        if merchant_id:
            self.form_parameters['vpc_Merchant'] = merchant_id

        if access_code:
            self.form_parameters['vpc_AccessCode'] = access_code

        if title:
            self.form_parameters['Title'] = title

        if 'form-method' in payment_info:
            self.form_method = payment_info['form-method']

        if 'form-target' in payment_info:
            self.form_target = payment_info['form-target']

        if 'form-parameters' in payment_info:
            self.form_parameters = dict(self.form_parameters.items() + payment_info['form-parameters'].items())

        self.form_parameters.pop('vpc_Currency', None)

    def _do_querydr_prepare(self, reservation_reference):
        log.debug("_do_migs_querydr_prepare, MIGS, queryDR prepare...")

        params = {}
        params['vpc_AccessCode'] = self.settings.settings.get('migs::access-code')
        params['vpc_Command'] = 'queryDR'
        params['vpc_MerchTxnRef'] = reservation_reference
        params['vpc_Merchant'] = self.settings.settings.get('migs::merchant-id')
        params['vpc_Password'] = self.settings.settings.get('migs::password')
        params['vpc_User'] = self.settings.settings.get('migs::username')
        params['vpc_Version'] = self.settings.default_params.get('vpc_Version')

        requery_url = self.settings.settings.get('requery-url')
        req = requests.Request('POST', url=requery_url, params=params).prepare()

        return req

    def _do_querydr_send(self, s, req):
        r = s.send(req, timeout=TIMEOUT_DEADLINE)

        return r.status_code, r.text

    def do_prepare(self, tx):
        return 'success', None

    # NOTE: no payment processing here, done externally.
    def do_payment_tx(self, tx):
        theaterorg_id = tx.workspace['theaters.org_id'][0]
        theater_code = tx.workspace['theaters.org_theater_code'][0]

        secure_hash_migsresponse = unicode(tx.workspace['secure_hash'])
        secure_hash_gmoviesbackend = unicode(self.generate_signature(tx.workspace['response_parameters']))
        response_code_migsresponse = str(tx.workspace['response_code'])
        response_message_migsresponse = tx.workspace['response_message']
        vpc_command_migsresponse = tx.workspace['vpc_command']

        if not tx.was_requery and vpc_command_migsresponse != 'queryDR':
            if secure_hash_migsresponse.strip() != secure_hash_gmoviesbackend.strip():
                log.warn("ERROR!, MIGS, do_payment_tx, secure_hash not matched...")

                tx.error_info['error_msg'] = "secure_hash not matched"
                return 'error', self.err_message

        if not tx.was_requery and response_code_migsresponse != '0':
            log.warn("ERROR!, MIGSPayment, do_payment_tx, response_code not 0...")

            namespace = 'migs'
            response_code = response_code_migsresponse
            response_message = response_message_migsresponse

            tx.error_info['error_msg'] = response_message
            return 'error', translate_message_via_error_code(namespace, response_code, response_message)

        # do finalize transaction on different cinema partners.
        if theaterorg_id == str(ROBINSONS_MALLS):
            if tx.was_requery:
                log.debug("MIGS, do_payment_tx, Robinsons Malls was_requery, skipping finalize...")
                if 0 == tx.workspace['response_code']:
                    return 'success', None
                tx.error_info['error_msg'] = response_message_migsresponse
                return 'error', self.err_message

            else:
                log.debug("MIGS, do_payment_tx, Robinsons Malls finalizing transaction...")
                return do_finalize_transaction(tx)
        else:
            log.warn("ERROR!, MIGS, do_payment_tx, theaterorg_id is not supported...")
            tx.error_info['error_msg'] = "theaterorg_id is not supported"

        return 'error', self.err_message

    def do_refund_tx(self, tx):
        log.warn("SKIP!, No refund process for MIGSPayment...")

    def generate_order_information(self, tx):
        return tx.reservation_reference

    def generate_signature(self, parameters):
        prefix = ['vpc', 'user']
        excluded = ['vpc_Currency', 'vpc_SecureHash', 'vpc_SecureHashType', 'Title', 'AgainLink']

        hash_secret = binascii.unhexlify(str(self.settings.settings.get('migs::merchant-hash-secret')))
        sorted_params = sorted(parameters.items(), key=operator.itemgetter(0))
        seed_str = '&'.join(['='.join(p) for p in sorted_params if p[0] not in excluded and p[0].split('_')[0] in prefix])
        signature = hmac.new(hash_secret, msg=seed_str, digestmod=hashlib.sha256).hexdigest().upper()

        return signature


class IPay88Payment():
    def __init__(self, settings, payment_info):
        self.err_message = DEFAULT_ERROR_MESSAGE
        self.form_method = 'POST'
        self.form_target = IPAY88_PAYMENT_ENDPOINT
        self.form_parameters = dict(settings)

    def do_prepare(self, tx):
        return 'success', None

    # No payment processing here. This is where we finalize seat reservation after payment completion done externally.
    def do_payment_tx(self, tx):
        # def __send_finalize_req_mgi(payload, org_uuid, theater_code=None, is_multiple_branch=False):
        #     status, r_data = finalize_transaction_mgi(payload, org_uuid, theater_code=theater_code, is_multiple_branch=is_multiple_branch)
        #
        #     return status, r_data

        try:
            theaterorg_id = tx.workspace['theaters.org_id'][0]
            theater_code = tx.workspace['theaters.org_theater_code'][0]

            if int(tx.workspace["payment_status"]) == 0:
                log.warn("ERROR!, IPay88Payment (do_payment_tx). %s..." % tx.workspace["payment_error_desc"])

                tx.error_info['error_msg'] = tx.workspace["payment_error_desc"]
                return 'error', tx.workspace["payment_error_desc"]

            return do_finalize_transaction(tx)

            # if theaterorg_id in [str(MEGAWORLD_MALLS)] and theater_code in ALLOWED_MGI_RESERVATION_BRANCH_CODES:
            #     rsvp_payload = {}
            #     rsvp_payload['reference_number'] = tx.workspace['create::reference_number']
            #     rsvp_payload['amount'] = float(tx.workspace['create::amount'])
            #     rsvp_payload['payment_type'] = tx.workspace['create::payment_type']
            #     rsvp_payload['payment_reference'] = str(tx.workspace['create::payment_type'])
            #     rsvp_payload['pin'] = tx.workspace['create::token::pin']
            #     rsvp_payload['receipt_id'] = tx.payment_reference
            #     rsvp_payload['email'] = tx.payment_info['email'] if 'email' in tx.payment_info else ''
            #     rsvp_payload['partner_name'] = 'Megaworld Malls'
            #
            #     status, r_data = call_once(__send_finalize_req_mgi, tx, rsvp_payload, theaterorg_id, theater_code)
            #
            #     if status == 'error':
            #         log.warn("ERROR!, IPay88Payment (do_payment_tx). %s..." % r_data['message'])
            #
            #         return 'error', r_data['message']
            #
            #     log.debug("IPay88Payment, Megaworld ({0}), finalize reservation transaction results: {1}...".format(theater_code, r_data))
            #
            #     tx.workspace['finalize::payment_reference'] = r_data['payment_reference']
            #     tx.workspace['finalize::token:pin'] = r_data['token']
            #     tx.workspace['finalize::booking_id'] = r_data['booking_id']
            #     tx.workspace['RSVP:claimcode'] = r_data['booking_id']
            #
            #     return 'success', None
            # else:
            #     log.warn("ERROR!, IPay88Payment (do_payment_tx), %s is not supported..." % theater_code)
        except Exception, e:
            log.warn("ERROR!, IPay88Payment (do_payment_tx)...")
            log.error(e)
            tx.error_info['error_msg'] = str(e)

        return 'error', self.err_message

    def do_refund_tx(self, tx):
        log.warn("SKIP!, No refund process for IPay88Payment...")

    def generate_hash(self, transaction_code, merchant_key):
        merchant_code = self.form_parameters['MerchantCode']
        currency_code = self.form_parameters['Currency']
        reference_number = transaction_code
        amount = re.sub('[,.]', '', str(self.form_parameters['Amount'])) # Remove commas and periods from string.
        seed = [merchant_key, merchant_code, reference_number, amount, currency_code] # Make a list out of string.
        seed_str = ''.join(seed)
        seed_hash = SHA.new(seed_str).digest() # Generate Hash

        return base64.b64encode(seed_hash)


class IPay88Payment2():
    def __init__(self, settings, payment_info):
        self.err_message = DEFAULT_ERROR_MESSAGE
        self.settings = settings
        self.form_method = 'POST'
        self.form_target = settings.settings.get('default-form-target')
        self.form_parameters = dict(settings.default_params)

        merchant_code = settings.settings.get('ipay88::merchant-code')
        merchant_key = settings.settings.get('ipay88::merchant-key')
        full_name = '%s %s' % (payment_info['first_name'], payment_info['last_name'])

        self.form_parameters['MerchantCode'] = merchant_code
        self.form_parameters['MerchantKey'] = merchant_key
        self.form_parameters['UserName'] = full_name.strip()
        self.form_parameters['UserEmail'] = payment_info['email']
        self.form_parameters['UserContact'] = payment_info['contact_number']

        if 'form-method' in payment_info:
            self.form_method = payment_info['form-method']

        if 'form-target' in payment_info:
            self.form_target = payment_info['form-target']

        if 'form-parameters' in payment_info:
            self.form_parameters = dict(self.form_parameters.items() + payment_info['form-parameters'].items())

    def do_prepare(self, tx):
        return 'success', None

    def do_payment_tx(self, tx):
        theaterorg_id = tx.workspace['theaters.org_id'][0]
        theater_code = tx.workspace['theaters.org_theater_code'][0]

        try:
            if int(tx.workspace["payment_status"]) == 0:
                log.warn("ERROR!, IPay88Payment (do_payment_tx). %s..." % tx.workspace["payment_error_desc"])

                tx.error_info['error_msg'] = tx.workspace["payment_error_desc"]
                return 'error', tx.workspace["payment_error_desc"]
        except Exception, e:
            log.warn("ERROR!, IPay88Payment2 (do_payment_tx)...")
            log.error(e)
            tx.error_info['error_msg'] = str(e)

        return 'error', self.err_message

    def do_refund_tx(self, tx):
        log.warn("SKIP!, No refund process for IPay88Payment2...")

    def generate_hash(self, reference):
        merchant_code = self.form_parameters['MerchantCode']
        merchant_key = self.form_parameters['MerchantKey']
        currency = self.form_parameters['Currency']
        amount = self.form_parameters['Amount']

        seed = [merchant_key, merchant_code, reference, amount, currency]
        seed_str = ''.join(seed)
        seed_hash = SHA.new(seed_str).digest()
        signature = base64.b64encode(seed_hash)

        return signature

class CashPayment():
    def __init__(self, payment_info):
        self.err_message = DEFAULT_ERROR_MESSAGE

    def do_prepare(self, tx):
        return 'success', None

    # empty, because Reservation types payments are over the counter
    def do_payment_tx(self, tx):
        return 'success', None

    def do_refund_tx(self, tx):
        log.warn("SKIP!, No refund process for CashPayment...")

# this where we finalize the reservation in core/third-party systems after payment completion.
def do_finalize_transaction(tx):
    def __send_finalize_req_theater_enablement(s, req):
        r = s.send(req, timeout=TIMEOUT_DEADLINE)

        return r.status_code, r.text

    def __send_finalize_req_mgi(payload, org_uuid, theater_code=None, is_multiple_branch=False):
        status, r_data = finalize_transaction_mgi(payload, org_uuid, theater_code=theater_code, is_multiple_branch=is_multiple_branch)

        return status, r_data

    def __send_reservation_req(s, req):
        log.info("######################### START ############################")
        log.info(req.method)
        log.info(req.url)
        log.info("########################## END #############################")

        r = s.send(req, timeout=TIMEOUT_DEADLINE)

        return r.status_code, r.text

    rsvp_payload = {}
    theaterorg_id = tx.workspace['theaters.org_id'][0]
    theater_code = tx.workspace['theaters.org_theater_code'][0]

    if is_powerplantmall_paynamics(tx) or is_powerplantmall_gcash(tx) or is_powerplantmall_promocode(tx):
        log.debug("do_finalize_transaction, Rockwell (Power Plant Mall) via PaynamicsPayment, GCashPayment, and PromoCodePayment...")

        reservation_id = tx.workspace['RESERVATION_ID']
        rsvp_payload['id'] = reservation_id

        log.debug("do_finalize_transaction, endpoint: {0}, payload: {1}...".format(ROCKWELL_ENDPOINT_SUCCESS, rsvp_payload))

        s = requests.Session()
        req = create_rockwell_request(ROCKWELL_ENDPOINT_SUCCESS, rsvp_payload)
        r_status_code, r_json = call_once(__send_finalize_req_theater_enablement, tx, s, req)

        try:
            log.debug("do_finalize_transaction, status_code: {0}, response: {1}...".format(r_status_code, r_json.encode('utf-8')))
        except Exception, e:
            log.warn("WARN! do_finalize_transaction, status_code and response logging...")
            log.error(e)

        d_json = json.loads(r_json)

        if r_status_code != 200:
            log.warn("ERROR!, do_finalize_transaction, request status_code not equal to 200...")

            if 'errorCode' in d_json:
                log.warn("ERROR!, do_finalize_transaction, existing key errorCode in json response...")

                namespace = 'power-plant-mall'
                response_code = d_json['errorCode']
                response_message = d_json['errorMessage'] if 'errorMessage' in d_json else ''

                tx.error_info['error_msg'] = response_message
                return 'error', translate_message_via_error_code(namespace, response_code, response_message)

            tx.error_info['error_msg'] = r_json
            return 'error', DEFAULT_ERROR_MESSAGE

        if 'response' not in d_json:
            log.warn("ERROR!, do_finalize_transaction, missing key response in json response...")

            tx.error_info['error_msg'] = "missing key response in json response"
            return 'error', DEFAULT_ERROR_MESSAGE

        if 'id' not in d_json['response']:
            log.warn("ERROR!, do_finalize_transaction, missing key id in json response...")

            tx.error_info['error_msg'] = "missing key id in json response"
            return 'error', DEFAULT_ERROR_MESSAGE

        if 'seatTransactions' not in d_json['response']:
            log.warn("ERROR!, do_finalize_transaction, missing key seatTransactions in json response...")

            tx.error_info['error_msg'] = "missing key seatTransactions in json response"
            return 'error', DEFAULT_ERROR_MESSAGE

        if reservation_id != d_json['response']['id']:
            log.warn("ERROR!, do_finalize_transaction, reservation_id and response_id not matched...")

            tx.error_info['error_msg'] = "reservation_id (%s) and response_id (%s) not matched" % (reservation_id, d_json['response']['id'])
            return 'error', DEFAULT_ERROR_MESSAGE

        tx.workspace['RSVP:seat_txs_dict'] = map_seats_label_to_tx(tx.workspace['seat_label_id_mapping'], d_json['response']['seatTransactions'])

        log.debug("do_finalize_transaction, RSVP:seat_txs_dict: {}".format(tx.workspace['RSVP:seat_txs_dict']))

        return 'success', None
    elif is_greenhills_paynamics(tx) and not is_token_paynamics(tx):
        log.debug("SKIP!, do_finalize_transaction, Greenhills Malls via PaynamicsPayment...")

        return 'success', None
    elif is_greenhills_gcash(tx) or is_greenhills_promocode(tx) or (is_token_paynamics(tx) and is_greenhills_paynamics(tx)):
        log.debug("do_finalize_transaction, Greenhills Malls via GCashPayment or PromoCodePayment or PaynamicsPaymentWithToken...")

        reservation_id = tx.workspace['RESERVATION_ID']
        rsvp_payload['id'] = reservation_id

        # add extra params for Paynamics with Token
        if is_token_paynamics(tx) and is_greenhills_paynamics(tx):
            rsvp_payload['paymentCode'] = tx.workspace['response_code'] if 'response_code' in tx.workspace else ''
            rsvp_payload['paymentMessage'] = tx.workspace['response_message'] if 'response_message' in tx.workspace else ''
            rsvp_payload['responseId'] = tx.payment_reference if tx.payment_reference else ''

        log.debug("do_finalize_transaction, endpoint: {0}, payload: {1}...".format(GREENHILLS_ENDPOINT_SUCCESS, rsvp_payload))

        s = requests.Session()
        req = create_greenhills_request(GREENHILLS_ENDPOINT_SUCCESS, rsvp_payload)
        r_status_code, r_json = call_once(__send_finalize_req_theater_enablement, tx, s, req)

        try:
            log.debug("do_finalize_transaction, status_code: {0}, response: {1}...".format(r_status_code, r_json.encode('utf-8')))
            if r_status_code != 200:
                tx.error_info['error_msg'] = r_json
        except Exception, e:
            log.warn("WARN! do_finalize_transaction, status_code and response logging...")
            log.error(e)

        return 'success', None
    elif is_cinema76_paynamics(tx) or is_cinema76_gcash(tx) or is_cinema76_promocode(tx):
        log.debug("do_finalize_transaction, Cinema 76 Malls via Paynamics or GCashPayment or PromoCodePayment...")

        reservation_id = tx.workspace['RESERVATION_ID']
        rsvp_payload['id'] = reservation_id
        rsvp_payload['paymentCode'] = tx.workspace['response_code'] if 'response_code' in tx.workspace else ''
        rsvp_payload['paymentMessage'] = tx.workspace['response_message'] if 'response_message' in tx.workspace else ''

        log.debug("do_finalize_transaction, endpoint: {0}, payload: {1}...".format(CINEMA76_ENDPOINT_SUCCESS, rsvp_payload))

        s = requests.Session()
        req = create_cinema76_request(CINEMA76_ENDPOINT_SUCCESS, rsvp_payload)
        r_status_code, r_json = call_once(__send_finalize_req_theater_enablement, tx, s, req)

        try:
            log.debug("do_finalize_transaction, status_code: {0}, response: {1}...".format(r_status_code, r_json.encode('utf-8')))
        except Exception, e:
            log.warn("WARN! do_finalize_transaction, status_code and response logging...")
            log.error(e)

        d_json = json.loads(r_json)
        if r_status_code != 200:
            log.warn("ERROR!, do_finalize_transaction, request status_code not equal to 200...")

            # send email to TE and GMovies support. This should be for refund.
            send_email_support(tx, "ERROR in do_finalize_transaction (Cinema 76 - FOR REFUND?): " + r_json, "TE")

            if 'errorCode' in d_json:
                log.warn("ERROR!, do_finalize_transaction, existing key errorCode in json response...")

                namespace = 'cinema76-malls'
                response_code = d_json['errorCode']
                response_message = d_json['errorMessage'] if 'errorMessage' in d_json else ''

                tx.error_info['error_msg'] = response_message
                return 'error', translate_message_via_error_code(namespace, response_code, response_message)

            tx.error_info['error_msg'] = r_json
            return 'error', DEFAULT_ERROR_MESSAGE

        # Check if transaction is for refund, no need to change state. Just send the success api call to trigger refund.
        if tx.state == 101:
            log.debug("do_finalize_transaction, Cinema 76 - tx (%s) is for refund..." % (tx.key.id()))
            return 'error', DEFAULT_ERROR_MESSAGE

        return 'success', None
    elif is_globeevents_paynamics(tx) or is_globeevents_gcash(tx) or is_globeevents_promocode(tx):
        log.debug("do_finalize_transaction, Globe Events via Paynamics or GCashPayment or PromoCodePayment...")

        reservation_id = tx.workspace['RESERVATION_ID']
        rsvp_payload['id'] = reservation_id
        rsvp_payload['paymentCode'] = tx.workspace['response_code'] if 'response_code' in tx.workspace else ''
        rsvp_payload['paymentMessage'] = tx.workspace['response_message'] if 'response_message' in tx.workspace else ''

        log.debug("do_finalize_transaction, endpoint: {0}, payload: {1}...".format(GLOBEEVENTS_ENDPOINT_SUCCESS, rsvp_payload))

        s = requests.Session()
        req = create_globeevents_request(GLOBEEVENTS_ENDPOINT_SUCCESS, rsvp_payload)
        r_status_code, r_json = call_once(__send_finalize_req_theater_enablement, tx, s, req)

        try:
            log.debug("do_finalize_transaction, status_code: {0}, response: {1}...".format(r_status_code, r_json.encode('utf-8')))
        except Exception, e:
            log.warn("WARN! do_finalize_transaction, status_code and response logging...")
            log.error(e)

        d_json = json.loads(r_json)
        if r_status_code != 200:
            log.warn("ERROR!, do_finalize_transaction, request status_code not equal to 200...")

            # send email to TE and GMovies support. This should be for refund.
            send_email_support(tx, "ERROR in do_finalize_transaction (Globe Events - FOR REFUND?): " + r_json, "TE")

            if 'errorCode' in d_json:
                log.warn("ERROR!, do_finalize_transaction, existing key errorCode in json response...")

                namespace = 'globe-events'
                response_code = d_json['errorCode']
                response_message = d_json['errorMessage'] if 'errorMessage' in d_json else ''

                tx.error_info['error_msg'] = response_message
                return 'error', translate_message_via_error_code(namespace, response_code, response_message)

            tx.error_info['error_msg'] = r_json
            return 'error', DEFAULT_ERROR_MESSAGE

        # Check if transaction is for refund, no need to change state. Just send the success api call to trigger refund.
        if tx.state == 101:
            log.debug("do_finalize_transaction, Globe Events - tx (%s) is for refund..." % (tx.key.id()))
            return 'error', DEFAULT_ERROR_MESSAGE

        return 'success', None
    elif is_smmalls_paynamics(tx) or is_smmalls_gcash(tx) or is_smmalls_promocode(tx):
        log.debug("do_finalize_transaction, SM Malls via PaynamicsPayment or GCashPayment or PromoCodePayment...")

        log.info("do_finalize_transaction, SM Malls via PaynamicsPayment or GCashPayment or PromoCodePayment...")

        rsvp_payload['reference_number'] = tx.workspace['create::reference_number']
        rsvp_payload['amount'] = float(tx.workspace['create::amount'])
        rsvp_payload['payment_type'] = tx.workspace['create::payment_type']
        rsvp_payload['payment_reference'] = str(tx.workspace['create::payment_type']) # as per MGi, use same value with payment_type.
        rsvp_payload['pin'] = tx.workspace['create::token::pin']
        rsvp_payload['partner_name'] = 'SM Malls'

        status, r_data = call_once(__send_finalize_req_mgi, tx, rsvp_payload, theaterorg_id, theater_code, True)

        if status == 'error':
            log.warn("ERROR!, do_finalize_transaction, %s..." % r_data['message'])

            # send email to GMovies support if exception errors are encountered
            if 'exception_msg' in r_data:
                send_email_support(tx, "ERROR in do_finalize_transaction (MGI): " + r_data['exception_msg'])

            if tx.payment_type in [ReservationTransaction.allowed_payment_types.GCASH,
                    ReservationTransaction.allowed_payment_types.GCASH_APP]:
                tx.workspace['refund::is_for_refund'] = True

            tx.error_info['error_msg'] = r_data['exception_msg'] if 'exception_msg' in r_data else r_data['message']
            return 'error', DEFAULT_ERROR_MESSAGE

        log.debug("do_finalize_transaction, finalize reservation transaction results:")
        log.debug(json.dumps(r_data))

        tx.workspace['finalize::payment_reference'] = r_data['payment_reference'].encode('utf-8') if r_data['payment_reference'] else r_data['payment_reference']
        tx.workspace['finalize::token::pin'] = r_data['token']

        return 'success', None
    # FOR ROB GCASH: add is_robinsons_gcash
    elif is_robinsons_paynamics(tx) or is_robinsons_promocode(tx) or is_robinsonsmalls_migs(tx) or is_robinsons_gcash(tx):
        log.debug("do_finalize_transaction, Robinsons Malls via PaynamicsPayment or Gcash Payment")

        log.info("do_finalize_transaction, Robinsons Malls via PaynamicsPayment or Gcash Payment")
        
        rsvp_payload['client_info'] = tx.workspace['create::client_info']
        rsvp_payload['reference_number'] = tx.workspace['create::reference_number']
        rsvp_payload['amount'] = float(tx.workspace['create::amount'])
        rsvp_payload['payment_type'] = tx.workspace['create::payment_type']
        rsvp_payload['payment_reference'] = str(tx.workspace['create::payment_type']) # as per MGi, use same value with payment_type.
        rsvp_payload['pin'] = tx.workspace['create::token::pin']
        rsvp_payload['partner_name'] = 'Robinsons Malls'
        # use reference_number for promocode since there is no payment_reference (from payment gw) available
        rsvp_payload['receipt_id'] = tx.payment_reference if tx.payment_reference else tx.workspace['create::reference_number']

        status, r_data = call_once(__send_finalize_req_mgi, tx, rsvp_payload, theaterorg_id, theater_code, True)

        if status == 'error':
            log.warn("ERROR!, do_finalize_transaction, %s..." % r_data['message'])

            # Check first if transaction was finalized by MGI
            log.debug("do_finalize_transaction, check_transaction_finalized, Trying if transaction was finalized...")
            check_tx_status, check_tx_response = check_transaction_finalized(tx.workspace['create::reference_number'])

            tx.workspace['response_code'] = check_tx_response['result']
            tx.workspace['response_message'] = check_tx_response['message']
            tx.workspace['finalize::payment_reference'] = check_tx_response['txn_receipt']

            log.debug("do_finalize_transaction, check_transaction_finalized, {}...".format(check_tx_response))
            if 0 == tx.workspace['response_code']:
                log.debug("do_finalize_transaction, check_transaction_finalized, Transaction was finalized by MGI...")
                return 'success', None
            
            # FOR ROB GCASH: Add condition if payment type is GCash/GCash App
            # then set tx.workspace['refund::is_for_refund'] to True (See sample above in SM if condition)
            if tx.payment_type in [ReservationTransaction.allowed_payment_types.GCASH, ReservationTransaction.allowed_payment_types.GCASH_APP]:
                tx.workspace['refund::is_for_refund'] = True

            # send email to GMovies support if exception errors are encountered
            if 'exception_msg' in r_data:
                send_email_support(tx, "ERROR in do_finalize_transaction (MGI): " + r_data['exception_msg'])
            tx.error_info['error_msg'] = r_data['exception_msg'] if 'exception_msg' in r_data else r_data['message']
            return 'error', DEFAULT_ERROR_MESSAGE

        log.debug("do_finalize_transaction, finalize reservation transaction results:")
        log.debug(json.dumps(r_data))

        tx.workspace['finalize::payment_reference'] = r_data['payment_reference'].encode('utf-8') if r_data['payment_reference'] else r_data['payment_reference']
        tx.workspace['finalize::token::pin'] = r_data['token']

        return 'success', None   
    elif is_mgw_ipay88_payment(tx):
        log.debug("do_finalize_transaction, Megaworld Malls (%s) via IPay88Payment..." % theater_code)

        if theaterorg_id in [str(MEGAWORLD_MALLS)] and theater_code in ALLOWED_MGI_RESERVATION_BRANCH_CODES:
            rsvp_payload['reference_number'] = tx.workspace['create::reference_number']
            rsvp_payload['amount'] = float(tx.workspace['create::amount'])
            rsvp_payload['payment_type'] = tx.workspace['create::payment_type']
            rsvp_payload['payment_reference'] = str(tx.workspace['create::payment_type'])
            rsvp_payload['pin'] = tx.workspace['create::token::pin']
            rsvp_payload['receipt_id'] = tx.payment_reference
            rsvp_payload['email'] = tx.payment_info['email'] if 'email' in tx.payment_info else ''
            rsvp_payload['partner_name'] = 'Megaworld Malls'

            status, r_data = call_once(__send_finalize_req_mgi, tx, rsvp_payload, theaterorg_id, theater_code)

            if status == 'error':
                log.warn("ERROR!, do_finalize_transaction, %s..." % r_data['message'])

                # send email to GMovies support if exception errors are encountered
                if 'exception_msg' in r_data:
                    send_email_support(tx, "ERROR in do_finalize_transaction (MGI): " + r_data['exception_msg'])

                tx.error_info['error_msg'] = r_data['exception_msg'] if 'exception_msg' in r_data else r_data['message']
                return 'error', DEFAULT_ERROR_MESSAGE

            log.debug("do_finalize_transaction, finalize reservation transaction results:")
            log.debug(r_data)

            tx.workspace['finalize::payment_reference'] = r_data['payment_reference']
            tx.workspace['finalize::token:pin'] = r_data['token']
            tx.workspace['finalize::booking_id'] = r_data['booking_id']
            tx.workspace['RSVP:claimcode'] = r_data['booking_id']

            return 'success', None
        else:
            log.warn("ERROR!, IPay88Payment, do_payment_tx, theater not supported cannot finalize transaction...")
            tx.error_info['error_msg'] = "IPay88Payment, do_payment_tx, theater not supported cannot finalize transaction"
    elif is_ayala_gcash(tx):
        if 'is_raw' in tx.workspace and tx.workspace['is_raw']:
            merchant_code = tx.workspace['theaters.org_theater_code'][0]
            schedule_code = tx.workspace['schedules.schedule_code'][0]
            seats = tx.workspace['seats::imploded']
        else:
            merchant_codes = tx.workspace['theaters.org_theater_code']
            slots = tx.workspace['schedules.slot']
            merchant_code = merchant_codes[0]
            schedule_code = slots[0].feed_code
            seats = tx.workspace['seats::imploded']

        rsvp_payload = {'src': SOURCE_ID, 'action': RESERVATION_ACTION, 'code': merchant_code,
            'sid': schedule_code, 'seats': seats, 'type': BUY_TYPE}

        rsvp_payload['mobnum'] = tx.payment_info['gcash_account']
        rsvp_payload['ptype']  = PTYPE_GCASH
        rsvp_payload['email']  = tx.payment_info['email']
        rsvp_payload['amount'] = tx.workspace['RSVP:totalamount']

        log.debug("do_finalize_transaction, sent request with payload: %s..." % rsvp_payload)

        s = requests.Session()
        req = create_sureseats_request(SURESEATS_ENDPOINT_PRIMARY_GCASH, rsvp_payload)
        r_status_code, r_text = call_once(__send_reservation_req, tx, s, req)

        log.debug("do_finalize_transaction, got response: %s..." % str(r_text))

        if r_status_code != 200:
            log.warn("ERROR!, do_finalize_transaction, cannot perform reservation, there was an error calling SureSeats...")

            tx.workspace['refund::is_for_refund'] = True
            tx.error_info['error_msg'] = r_text
            return 'error', DEFAULT_ERROR_MESSAGE

        try:
            xml = etree.fromstring(str(r_text))
            status = xml.xpath('//Transaction/Status/text()')[0]

            if status.upper() == 'SUCCESS':
                log.debug("do_finalize_transaction, status equal to SUCCESS...")

                tx.reservation_reference = xml.xpath('//Transaction/RefNo/text()')[0]
                tx.workspace['RSVP:claimcode'] = xml.xpath('//Transaction/ClaimCode/text()')[0]
                tx.workspace['RSVP:claimdate'] = datetime.strptime(xml.xpath('//Transaction/ClaimDate/text()')[0], '%m/%d/%Y %I:%M:%S %p')
                return 'success', None
            else:
                log.warn("ERROR!, do_finalize_transaction, status not equal to SUCCESS...")
                log.warn("status: %s..." % status)

                namespace = 'ayala-malls'
                response_code = 'FAILED' # default response code to translate the error messages.
                response_message = status

                if status == '1 or more seats have been reserved by another user. Please try again and select other seats.':
                    response_code = 'SEATSTAKEN'
                elif status in ['Could not find movie screening. Select another time.', 'Movie schedule not found. Please try a different schedule.']:
                    response_code = 'SCHEDULENOTFOUND'
                    # send email to GMovies support to remove schedule
                    send_email_support(tx, status)

                tx.workspace['refund::is_for_refund'] = True
                tx.error_info['error_msg'] = status
                return 'error', translate_message_via_error_code(namespace, response_code, response_message)
        except etree.XMLSyntaxError, e:
            log.warn("ERROR!, do_finalize_transaction, XMLSyntaxError...")
            log.error(e)
            tx.error_info['error_msg'] = str(e)
        except Exception, e:
            log.warn("ERROR!, do_finalize_transaction...")
            log.error(e)
            # send email to GMovies support if exception errors are encountered
            send_email_support(tx, "ERROR in do_finalize_transaction: " + str(e))
            tx.error_info['error_msg'] = str(e)
        return 'error', DEFAULT_ERROR_MESSAGE
    elif is_ayala_grewards(tx):
        if 'is_raw' in tx.workspace and tx.workspace['is_raw']:
            merchant_code = tx.workspace['theaters.org_theater_code'][0]
            schedule_code = tx.workspace['schedules.schedule_code'][0]
            seats = tx.workspace['seats::imploded']
        else:
            merchant_codes = tx.workspace['theaters.org_theater_code']
            slots = tx.workspace['schedules.slot']
            merchant_code = merchant_codes[0]
            schedule_code = slots[0].feed_code
            seats = tx.workspace['seats::imploded']

        rsvp_payload = {'src': SOURCE_ID, 'action': RESERVATION_ACTION, 'code': merchant_code,
            'sid': schedule_code, 'seats': seats, 'type': BUY_TYPE}

        rsvp_payload['mobnum'] = tx.payment_info['grewards_account']
        rsvp_payload['ptype']  = PTYPE_GREWARDS
        rsvp_payload['email']  = tx.payment_info['email']
        rsvp_payload['amount'] = tx.workspace['RSVP:totalamount']

        log.debug("do_finalize_transaction, sent request with payload: %s..." % rsvp_payload)

        s = requests.Session()
        req = create_sureseats_request(SURESEATS_ENDPOINT_PRIMARY, rsvp_payload)
        r_status_code, r_text = call_once(__send_reservation_req, tx, s, req)

        log.debug("do_finalize_transaction, got response: %s..." % str(r_text))

        if r_status_code != 200:
            log.warn("ERROR!, do_finalize_transaction, cannot perform reservation, there was an error calling SureSeats...")

            tx.workspace['refund::is_for_refund'] = True
            tx.error_info['error_msg'] = r_text
            return 'error', DEFAULT_ERROR_MESSAGE

        try:
            xml = etree.fromstring(str(r_text))
            status = xml.xpath('//Transaction/Status/text()')[0]

            if status.upper() == 'SUCCESS':
                log.debug("do_finalize_transaction, status equal to SUCCESS...")

                tx.reservation_reference = xml.xpath('//Transaction/RefNo/text()')[0]
                tx.workspace['RSVP:claimcode'] = xml.xpath('//Transaction/ClaimCode/text()')[0]
                tx.workspace['RSVP:claimdate'] = datetime.strptime(xml.xpath('//Transaction/ClaimDate/text()')[0], '%m/%d/%Y %I:%M:%S %p')
                return 'success', None
            else:
                log.warn("ERROR!, do_finalize_transaction, status not equal to SUCCESS...")
                log.warn("status: %s..." % status)

                namespace = 'ayala-malls'
                response_code = 'FAILED' # default response code to translate the error messages.
                response_message = status

                if status == '1 or more seats have been reserved by another user. Please try again and select other seats.':
                    response_code = 'SEATSTAKEN'
                elif status in ['Could not find movie screening. Select another time.', 'Movie schedule not found. Please try a different schedule.']:
                    response_code = 'SCHEDULENOTFOUND'
                    # send email to GMovies support to remove schedule
                    send_email_support(tx, status)

                tx.workspace['refund::is_for_refund'] = True
                tx.error_info['error_msg'] = status
                return 'error', translate_message_via_error_code(namespace, response_code, response_message)
        except etree.XMLSyntaxError, e:
            log.warn("ERROR!, do_finalize_transaction, XMLSyntaxError...")
            log.error(e)
            tx.error_info['error_msg'] = str(e)
        except Exception, e:
            log.warn("ERROR!, do_finalize_transaction...")
            log.error(e)
            # send email to GMovies support if exception errors are encountered
            send_email_support(tx, "ERROR in do_finalize_transaction: " + str(e))
            tx.error_info['error_msg'] = str(e)
        return 'error', DEFAULT_ERROR_MESSAGE
    else:
        log.warn("ERROR!, do_finalize_transaction, theaterorg not supported cannot finalize transaction...")
        tx.error_info['error_msg'] = "theaterorg not supported cannot finalize transaction"

    return 'error', DEFAULT_ERROR_MESSAGE

# this where we cancel the reservation in core/third-party systems after payment completion if errors are encountered.
def do_cancel_transaction(tx):
    def __send_cancel_req_theater_enablement(s, req):
        r = s.send(req, timeout=TIMEOUT_DEADLINE)

        return r.status_code, r.text

    theater_keys, cinemas, schedule_keys, times, time_strs, seats = zip(*[(r.theater, r.cinema,
        r.schedule, r.time, str(r.time), r.seat_id) for r in tx.reservations])

    from .reservation import non_transactional_get_multi
    entity_keys = theater_keys
    entities = non_transactional_get_multi(entity_keys)
    theaters = entities[:len(theater_keys)]
    theaterorg_id = [t.key.parent().id() for t in theaters][0]
    theaterorg_list = {str(AYALA_MALLS):'AYL', str(GREENHILLS_MALLS):'GH', str(SM_MALLS):'SM', str(ROBINSONS_MALLS):'RMW',
        str(MEGAWORLD_MALLS):'MGW', str(CINEMA76_MALLS):'C76', str(GLOBE_EVENTS):'GE'}
    theater_code = theaterorg_list[theaterorg_id]

    rsvp_payload = {}

    if (is_cinema76_paynamics(tx) or is_cinema76_gcash(tx) or is_cinema76_promocode(tx) or
           is_globeevents_paynamics(tx) or is_globeevents_gcash(tx) or is_globeevents_promocode(tx)):
        log.debug("do_cancel_transaction, %s via Paynamics or GCashPayment or PromoCodePayment..." % (theater_code))

        #cancel only if there is a reservation id on Cinema 76 / Globe Events.
        if 'RESERVATION_ID' in tx.workspace and tx.workspace['RESERVATION_ID']:
            reservation_id = tx.workspace['RESERVATION_ID']
            rsvp_payload['id'] = reservation_id
            rsvp_payload['paymentCode'] = tx.workspace['response_code'] if 'response_code' in tx.workspace else ''
            rsvp_payload['paymentMessage'] = tx.workspace['response_message'] if 'response_message' in tx.workspace else ''

            cancel_url = CINEMA76_ENDPOINT_CANCEL if theaterorg_id == str(CINEMA76_MALLS) else GLOBEEVENTS_ENDPOINT_CANCEL
            log.debug("do_cancel_transaction, endpoint: {0}, payload: {1}...".format(cancel_url, rsvp_payload))

            s = requests.Session()
            if theaterorg_id == str(CINEMA76_MALLS):
                req = create_cinema76_request(cancel_url, rsvp_payload)
            else:
                req = create_globeevents_request(cancel_url, rsvp_payload)
            r_status_code, r_json = call_once(__send_cancel_req_theater_enablement, tx, s, req)

            try:
                log.debug("do_cancel_transaction, status_code: {0}, response: {1}...".format(r_status_code, r_json.encode('utf-8')))
            except Exception, e:
                log.warn("WARN! do_cancel_transaction, status_code and response logging...")
                log.error(e)

            if r_status_code != 200:
                log.warn("ERROR!, do_cancel_transaction, request status_code not equal to 200...")

                # send email to TE and GMovies support. This should be for refund.
                send_email_support(tx, "ERROR in do_cancel_transaction ({0} - CANCELLATION - for refund?): {1}".format(theater_code, r_json.encode('utf-8')), "TE")

                tx.error_info['error_msg'] = r_json
                return 'error', DEFAULT_ERROR_MESSAGE
        return 'success', None
    elif is_token_paynamics(tx) and is_greenhills_paynamics(tx):
        log.debug("do_cancel_transaction, Greenhills Malls via PaynamicsPaymentWithToken...")

        #cancel only if there is a reservation id on Greenhills.
        if 'RESERVATION_ID' in tx.workspace and tx.workspace['RESERVATION_ID']:
            reservation_id = tx.workspace['RESERVATION_ID']
            rsvp_payload['id'] = reservation_id
            rsvp_payload['paymentCode'] = tx.workspace['response_code'] if 'response_code' in tx.workspace else ''
            rsvp_payload['paymentMessage'] = tx.workspace['response_message'] if 'response_message' in tx.workspace else ''
            rsvp_payload['responseId'] = tx.payment_reference if tx.payment_reference else ''

            log.debug("do_cancel_transaction, endpoint: {0}, payload: {1}...".format(GREENHILLS_ENDPOINT_CANCEL, rsvp_payload))

            s = requests.Session()
            req = create_greenhills_request(GREENHILLS_ENDPOINT_CANCEL, rsvp_payload)
            r_status_code, r_json = call_once(__send_cancel_req_theater_enablement, tx, s, req)

            try:
                log.debug("do_cancel_transaction, status_code: {0}, response: {1}...".format(r_status_code, r_json.encode('utf-8')))
            except Exception, e:
                log.warn("WARN! do_cancel_transaction, status_code and response logging...")
                log.error(e)

            if r_status_code != 200:
                log.warn("ERROR!, do_cancel_transaction, request status_code not equal to 200...")

                # send email to TE and GMovies support. This should be for refund.
                send_email_support(tx, "ERROR in do_cancel_transaction (GH - FOR REFUND?): {0}".format(r_json.encode('utf-8')), "TE")

                tx.error_info['error_msg'] = r_json.encode('utf-8')
                return 'error', DEFAULT_ERROR_MESSAGE
        return 'success', None
    else:
        log.warn("ERROR!, do_cancel_transaction, theaterorg not supported cannot cancel transaction...")

    return 'error', DEFAULT_ERROR_MESSAGE

# this where we refund the reservation and payment (if applicable) in core/third-party systems
def do_refund_transaction(tx, refund_reason=None):
    def __send_cancel_req_theater_enablement(s, req):
        r = s.send(req, timeout=TIMEOUT_DEADLINE)

        return r.status_code, r.text

    theater_keys, cinemas, schedule_keys, times, time_strs, seats = zip(*[(r.theater, r.cinema,
        r.schedule, r.time, str(r.time), r.seat_id) for r in tx.reservations])

    from .reservation import non_transactional_get_multi
    entity_keys     = theater_keys
    entities        = non_transactional_get_multi(entity_keys)
    theaters        = entities[:len(theater_keys)]
    theaterorg_id   = [t.key.parent().id() for t in theaters][0]
    theaterorg_list = {str(AYALA_MALLS):'AYL', str(GREENHILLS_MALLS):'GH', str(SM_MALLS):'SM', str(ROBINSONS_MALLS):'RMW',
        str(MEGAWORLD_MALLS):'MGW', str(CINEMA76_MALLS):'C76', str(GLOBE_EVENTS):'GE'}
    theater_code    = theaterorg_list[theaterorg_id]

    is_refunded = False
    payment_gateway = None
    # check payment type
    
    #if reserve, remove reserved seat
    if tx.payment_type == ReservationTransaction.allowed_payment_types.RESERVE:
        payment_gateway = 'cash'
        if theaterorg_id == str(AYALA_MALLS):
            log.info("do_refund_transaction, removing seat lock for reservation...")
            status = seat_cancellation(tx.ticket.code, tx.key.parent().id())
            log.debug('status return is : {}'.format(status['status']))
            if status['status'] != 1:
                log.warn("ERROR!, do_refund_transaction.seat_cancellation, response : {}".format(status['message']))
                # send email to GMovies support.
                send_email_support(tx, "ERROR in do_refund_transaction.seat_cancellation (CALLING DIGITAL VENTURES REFUND API): {0}".format(status['message']))
                tx.error_info['error_msg'] = status['message']
                return 'error', "{}".format(status['message'])
            else:
                log.debug("Success Response message : {}".format(status['message']))
                log.debug("Complete api response : {}".format(status))
                log.debug("Seat ticket code # {} has been successfully cancelled.".format(tx.ticket.code))
            is_refunded     = True
    
    # if claim code, reset claim code
    if tx.payment_type == ReservationTransaction.allowed_payment_types.PROMO_CODE:
        payment_gateway = 'claim_code'
        claim_code      = tx.payment_info['promo_code'] if 'promo_code' in tx.payment_info else ''
        mobnum          = tx.payment_info['mobile'] if 'mobile' in tx.payment_info else ''
        log.debug("do_refund_transaction, reseting claimcode %s with mobnum %s, from %s..." % (claim_code, mobnum, PROMOCODE_RESET_ENDPOINT))
        r               = requests.get(PROMOCODE_RESET_ENDPOINT, params={'promo_code': claim_code, 'mobnum': mobnum})
        log.debug("do_refund_transaction, claim code reset status_code, %s..." % r.status_code)
        send_refund_claimcodetx_notification(tx.payment_info['email'], tx.payment_info['full_name'], tx.payment_info['promo_code'])
        is_claimcode    = True
        #free seat if refunded/specific for ayala
        if theaterorg_id == str(AYALA_MALLS):
            status = seat_cancellation(tx.ticket.code, tx.key.parent().id())
            log.debug('status return is : {}'.format(status['status']))
            if status['status'] != 1:
                log.warn("ERROR!, do_refund_transaction.seat_cancellation, response : {}".format(status['message']))
                # send email to GMovies support.
                send_email_support(tx, "ERROR in do_refund_transaction.seat_cancellation (CALLING DIGITAL VENTURES REFUND API): {0}".format(status['message']))
                tx.error_info['error_msg'] = status['message']
                return 'error', "{}".format(status['message'])
            else:
                log.debug("Success Response message : {}".format(status['message']))
                log.debug("Complete api response : {}".format(status))
                log.debug("Seat ticket code # {} has been successfully cancelled.".format(tx.ticket.code))
            is_refunded     = True

    # if grewards, gcash, pesopay/ discounted card pesopay call dv refund api
    if ((tx.payment_type in [ReservationTransaction.allowed_payment_types.CLIENT_INITIATED,
            ReservationTransaction.allowed_payment_types.GCASH, ReservationTransaction.allowed_payment_types.GCASH_APP, ReservationTransaction.allowed_payment_types.GREWARDS]) or
            (tx.payment_type in CARDHOLDER_PROMOS and theaterorg_id in PESOPAY_THEATERORGS and
            tx.discount_info['payment_gateway'] == 'pesopay')
            ):
        payment_gateway = 'pesopay'
        if (tx.payment_type in [ReservationTransaction.allowed_payment_types.GCASH,
                ReservationTransaction.allowed_payment_types.GCASH_APP]):
            payment_gateway = 'gcash'
        if (tx.payment_type in [ReservationTransaction.allowed_payment_types.GREWARDS]):
            payment_gateway = 'grewards'
        log.debug("do_refund_transaction, calling DIGITAL VENTURES REFUND API...")
        status, result  = trigger_refund(tx)
        if status != 1:
            log.warn("ERROR!, do_refund_transaction, request status_code not equal to 1...")
            # send email to GMovies support.
            send_email_support(tx, "ERROR in do_refund_transaction (CALLING DIGITAL VENTURES REFUND API): {0}".format(result.encode('utf-8')))
            tx.error_info['error_msg'] = result.encode('utf-8')
            return 'error', "Error in calling Digital Ventures Refund API."
        is_refunded     = True

    # if paynamics call refund function
    # send auto email to payment gateway, cc support
    if ((tx.payment_type in [ReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT]) or
            (tx.payment_type in CARDHOLDER_PROMOS and theaterorg_id in PAYNAMICS_THEATERORGS and
            tx.discount_info['payment_gateway'] == 'paynamics')):

        payment_gateway = 'paynamics'
        # Check tx date if eligible for refund
        log.debug("do_refund_transaction, date_created: %s, date_refund: %s" % (tx.date_created, datetime.now()))
        '%m/%d/%Y'
        dt_created      = date.strftime(tx.date_created, '%m/%d/%Y')
        dt_today        = date.strftime(date.today(), '%m/%d/%Y')

        # if transaction date == current date do reversal instead of refund
        is_reversal = False
        if dt_created == dt_today:
            is_reversal = True

        settings     = _get_client_initiated_payment_settings(theater_keys[0], 'paynamics')
        merchant_id  = settings.settings.get('paynamics::merchant-id')
        merchant_key = settings.settings.get('paynamics::merchant-key')
        token        = tx.payment_info['token'] if 'token' in tx.payment_info else ''
        if token:
            if theaterorg_id in [str(CINEMA76_MALLS), str(SM_MALLS)]:
                log.debug("do_refund_transaction, 3d tokenized transaction...")
                merchant_id  = settings.settings.get('paynamics::3d-token-merchant-id')
                merchant_key = settings.settings.get('paynamics::3d-token-merchant-key')
            else:
                log.debug("do_refund_transaction, tokenized transaction...")
                merchant_id  = settings.settings.get('paynamics::moto-merchant-id')
                merchant_key = settings.settings.get('paynamics::moto-merchant-key')
        log.debug("do_refund_transaction, merchant_id: %s" % (merchant_id))
        request_id       = tx.reservation_reference.split('~')[-1] if str(theaterorg_id) not in [str(SM_MALLS), str(ROBINSONS_MALLS), str(CINEMA76_MALLS), str(GLOBE_EVENTS)] else tx.reservation_reference
        ip_address       = settings.default_params['ip_address']
        response_id      = tx.payment_reference
        amount           = tx.ticket.extra['total_amount']
        notification_url = settings.default_params['notification_url']
        response_url     = settings.default_params['response_url']

        # make request_id unique
        suffix     = "REV" if is_reversal else "REF"
        request_id = request_id + suffix
        # generate signature
        seed       = [merchant_id, request_id, response_id, ip_address,
                notification_url, response_url, amount, merchant_key]
        seed_str   = ''.join(seed)
        signature  = hashlib.sha512(seed_str.encode('utf-8')).hexdigest()

        if is_reversal:
            log.debug("do_refund_transaction, trigger reversal instead of refund...")
            refund_status, refund_response = reversal(merchant_id, request_id, ip_address, response_id,
            amount, notification_url, response_url, signature)
        else:
            log.debug("do_refund_transaction, trigger refund...")
            refund_status, refund_response = refund(merchant_id, request_id, ip_address, response_id,
            amount, notification_url, response_url, signature)

        if refund_status == 'error':
            log.warn("ERROR!, do_refund_transaction: %s" % refund_response['message'])

            # send email to GMovies support if exception errors are encountered
            if 'exception_msg' in refund_response:
                send_email_support(tx, "ERROR in PaynamicsPayment Auto Refund: " + refund_response['exception_msg'])

            tx.error_info['refund_error_msg'] = refund_response['exception_msg'] if 'exception_msg' in refund_response else refund_response['message']
            return 'error', "Error in processing Paynamics Refund: %s." % refund_response['exception_msg']

        # Log response for error details
        tx.error_info['refund_response_code']    = refund_response['response_code'] if 'response_code' in refund_response else ''
        tx.error_info['refund_response_message'] = refund_response['response_message'] if 'response_message' in refund_response else ''
        tx.error_info['refund_response_id']      = refund_response['response_id']
        tx.put()

        # GR033 - Transaction Pending
        if refund_response['response_code'] not in ['GR001', 'GR002', 'GR033']:
            log.warn("ERROR, do_refund_transaction, response_code not in ['GR001', 'GR002', 'GR033']...")
            send_email_support(tx, "ERROR in PaynamicsPayment Auto Refund: " +
                refund_response['response_code'] + " " + refund_response['response_message'])
            return 'error', "Error in processing Paynamics Refund: %s." % refund_response['response_message']

        send_email_support(tx, "SUCCESSFUL in PaynamicsPayment Auto Refund: " +
            refund_response['response_code'] + " " + refund_response['response_message'], 'INFO')
        # # send email to paynamics
        # status, result = send_email_refund_payment_gateway(tx, theaterorg_id, 'paynamics', theater_code)
        # if status != 'success':
            # log.warn("ERROR!, do_refund_transaction, send_email_refund_payment_gateway result not equal to 'success'...")
            # # send email to GMovies support.
            # send_email_support(tx, "ERROR in do_refund_transaction (SENDING REFUND NOTIFICATION EMAIL TO PAYMENT_GATEWAY): {0}".format(result.encode('utf-8')))
            # tx.error_info['error_msg'] = result.encode('utf-8')
            # return 'error', "Error in sending refund notification to Payment Gateway: %s." % payment_gateway
        is_refunded = True

    # if ipay88/migs, send auto email to payment gateway, cc support
    if (tx.payment_type in [ReservationTransaction.allowed_payment_types.MIGS_PAYMENT,
            ReservationTransaction.allowed_payment_types.IPAY88_PAYMENT]):
        payment_gateway = 'migs'
        if ReservationTransaction.allowed_payment_types.IPAY88_PAYMENT == tx.payment_type:
            payment_gateway = 'ipay88'
        log.debug("do_refund_transaction, sending refund request to Payment Gateway: %s..." % payment_gateway)
        status, result = send_email_refund_payment_gateway(tx, theaterorg_id, payment_gateway, theater_code)
        if status != 'success':
            log.warn("ERROR!, do_refund_transaction, send_email_refund_payment_gateway result not equal to 'success'...")
            # send email to GMovies support.
            send_email_support(tx, "ERROR in do_refund_transaction (SENDING REFUND REQUEST EMAIL TO PAYMENT_GATEWAY): {0}".format(result.encode('utf-8')))
            tx.error_info['error_msg'] = result.encode('utf-8')
            return 'error', "Error in sending refund request to Payment Gateway: %s." % payment_gateway

    # if GH, C76, GLOBE EVENTS send void request
    if theaterorg_id in [str(GREENHILLS_MALLS), str(CINEMA76_MALLS), str(GLOBE_EVENTS)]:
        request_id                  = tx.reservation_reference.split('~')[-1]
        rsvp_payload                = {}
        rsvp_payload['bookingCode'] = request_id
        rsvp_payload['remark']      = refund_reason
        log.debug("do_refund_transaction, calling %s voiding API..." % (theater_code))
        voiding_url = GLOBEEVENTS_ENDPOINT_VOID
        if theaterorg_id == str(GREENHILLS_MALLS):
            voiding_url = GREENHILLS_ENDPOINT_VOID
        elif theaterorg_id == str(CINEMA76_MALLS):
            voiding_url = CINEMA76_ENDPOINT_VOID

        log.debug("do_refund_transaction, endpoint: {0}, payload: {1}...".format(voiding_url, rsvp_payload))

        s = requests.Session()
        if theaterorg_id == str(GREENHILLS_MALLS):
            req = create_greenhills_request(voiding_url, rsvp_payload)
        elif theaterorg_id == str(CINEMA76_MALLS):
            req = create_cinema76_request(voiding_url, rsvp_payload)
        else:
            req = create_globeevents_request(voiding_url, rsvp_payload)
        r_status_code, r_json = call_once(__send_cancel_req_theater_enablement, tx, s, req)

        try:
            log.debug("do_refund_transaction, status_code: {0}, response: {1}...".format(r_status_code, r_json.encode('utf-8')))
        except Exception, e:
            log.warn("WARN! do_refund_transaction, status_code and response logging...")
            log.error(e)
        if r_status_code != 200:
            log.warn("ERROR!, do_refund_transaction, request status_code not equal to 200...")
            # send email to GMovies support.
            send_email_support(tx, "ERROR in do_refund_transaction ({0} - VOIDING): {1}".format(theater_code, r_json.encode('utf-8')))
            tx.error_info['error_msg'] = r_json.encode('utf-8')
            return 'error', "Error in calling %s voiding of transaction." % (theater_code)

    if payment_gateway not in ['claim_code', 'cash']:
        # if successful send email to customer, cc support
        log.debug("do_refund_transaction, sending refund email to Customer...")
        status, result = send_email_refund_user(tx, payment_gateway, is_refunded)
        if status != 'success':
            log.warn("ERROR!, do_refund_transaction, send_email_refund_user result not equal to 'success'...")
            # send email to GMovies support.
            send_email_support(tx, "ERROR in do_refund_transaction (SENDING REFUND EMAIL TO CUSTOMER): {0}".format(result.encode('utf-8')))
            tx.error_info['error_msg'] = result.encode('utf-8')
            return 'error', "Error in sending refund email to Customer."

    return 'success', 'Successfully processed request for refund.', payment_gateway
