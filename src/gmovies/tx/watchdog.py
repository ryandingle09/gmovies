from datetime import datetime, timedelta

from google.appengine.ext.ndb import transactional

from gmovies.models import ReservationTransaction
from .actions import cancel
from .state_machine import state
from .scheduler import reschedule

ACTIVE_WINDOW = timedelta(minutes=10)

def watchdog():
    stuck_tx_timestamp = datetime.now() - ACTIVE_WINDOW
    stuck_tx_q = ReservationTransaction.query(ReservationTransaction.is_active == True,
                                              ReservationTransaction.date_created <= stuck_tx_timestamp)

    for tx in stuck_tx_q:
        if tx.state not in TX_WAIT_STATES:
            nudge_stuck_tx(tx)
        else:
            # Cancel this transaction
            cancel(tx.key.parent().id(), tx.key.id())


@transactional
def nudge_stuck_tx(tx):
    reschedule(tx)
