################################################################################
#
# Deprecated (10/21/2016). Remove or delete this file in the future.
# Use the mgi.py for the cinema partners under MGi integration.
#
################################################################################


# Megaworld-related methods

import logging
log = logging.getLogger(__name__)

from flask import jsonify
from gmovies.ws.connector import WSAuth, WSTransaction
from gmovies.ws.endpoints import LCT_SESSION_API, LCT_TRANSACTION_API

from gmovies.settings import LCT_USERNAME, LCT_PASSWORD

def mgi_auth_session(theater_code, client_id):
    # LCT is currently the enabled theater for Megaworld
    if theater_code == 'LCT':
        auth = WSAuth(LCT_SESSION_API)
        status, session_data =  auth.get_session_and_authenticate(LCT_USERNAME, LCT_PASSWORD)
        return (status, session_data)

def mgi_create_transaction(client_id, payload, theater_code=None):
    if not client_id:
        return ('error', 'Missing client id')

    #re-aquire session if empty
    #if not session_token and theater_code is not None:
    #    status, authenticated_session = mgi_auth_session(theater_code, client_id)
    #    if status == 'error':
    #        return ('error','Failed to authenticate session')

    if theater_code == 'LCT':
        transaction = WSTransaction(LCT_TRANSACTION_API, LCT_SESSION_API, LCT_USERNAME, LCT_PASSWORD, client_id)
        st, r_data = transaction.reserve_tx(int(payload["branch_id"]), int(payload["mct_key"]), payload["seat_ids"], payload["seating_type"] )
        return (st, r_data)
    else: 
        return ('error','Invalid theater code')


def mgi_finalize_transaction(client_id, payload, theater_code=None):
    if not client_id:
        return ('error', 'Missing client id')

    #re-aquire session if empty
    #if not session_token and theater_code is not None:
    #    status, authenticated_session = mgi_auth_session(theater_code, client_id)
    #    if status == 'error':
    #        return ('error','Failed to authenticate session')

    if theater_code == 'LCT':
        transaction = WSTransaction(LCT_TRANSACTION_API, LCT_SESSION_API, LCT_USERNAME, LCT_PASSWORD, client_id)
        st, r_data = transaction.finalize_tx(payload)
        return (st, r_data)
    else:
        return ('error','Invalid theater code')
        


