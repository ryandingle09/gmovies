import hashlib
import logging
import requests

from google.appengine.ext.ndb import transactional

from gmovies.exceptions.api import AlreadyCancelledException, TransactionConflictException
from gmovies.models import ReservationTransaction
from gmovies.tx.query import query_info
from gmovies.tx.scheduler import reschedule, cancel_reaping
from gmovies.tx.state_machine import state, transition_state
from gmovies.tx.util import call_once
from gmovies.ws.endpoints import PAYNAMICS_PNXQUERY_URL, PAYNAMICS_PNXQUERY_TARGETNAMESPACE
from gmovies.ws.soap_connector import CCServiceWSPaynamics
from gmovies.tx.mgi import check_transaction_finalized


log = logging.getLogger(__name__)


@transactional
def requery_stale_tx(device_id, tx_id):
    tx = query_info(device_id, tx_id)
    tx.workspace['requery_task'] = None

    try:
        if tx.payment_type == ReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT:
            log.debug("requery_stale_tx, trigger Paynamics Requery command...")

            payment_engine = tx.workspace['payment::engine']
            merchant_id = payment_engine.form_parameters['mid']
            merchant_key = payment_engine.form_parameters['merchantkey']
            request_id = 'RQ' + tx.reservation_reference
            org_trxid = ''
            org_trxid2 = tx.reservation_reference
            seed = [merchant_id, request_id, org_trxid, org_trxid2, merchant_key]
            seed_str = ''.join(seed)
            signature = hashlib.sha512(seed_str).hexdigest()

            ccservicepaynamics = CCServiceWSPaynamics(PAYNAMICS_PNXQUERY_URL, PAYNAMICS_PNXQUERY_TARGETNAMESPACE)
            requery_response = ccservicepaynamics.do_requery(merchant_id, request_id, org_trxid, org_trxid2, signature)

            tx.workspace['response_code'] = requery_response['response_code'] if 'response_code' in requery_response else ''
            tx.workspace['response_message'] = requery_response['response_message'] if 'response_message' in requery_response else ''

            if 'txns::0' in requery_response:
                tx.payment_reference = requery_response['txns::0']['response_id']
                tx.workspace['response_code'] = requery_response['txns::0']['response_code']
                tx.workspace['response_message'] = requery_response['txns::0']['response_message']
                tx.payment_info['response_id'] = tx.payment_reference

                # add cc tokenization details
                tx.payment_info['token_id'] = requery_response['txns::0']['token_id'] if 'token_id' in requery_response['txns::0'] else ''
                tx.payment_info['token_info'] = requery_response['txns::0']['token_info'] if 'token_info' in requery_response['txns::0'] else ''
                tx.payment_info['bin'] = requery_response['txns::0']['bin'] if 'bin' in requery_response['txns::0'] else ''

                log.debug("requery_stale_tx, txns::0, {}...".format(requery_response['txns::0']))
                log.debug("requery_stale_tx, completing, %s~%s..." % (device_id, tx_id))

                trigger_completion_requery(tx)

                log.debug("requery_stale_tx, requery, triggered completion...")
        elif tx.payment_type == ReservationTransaction.allowed_payment_types.MIGS_PAYMENT:
            log.debug("requery_stale_tx, trigger MIGS CheckTransactionFinalized command...")

            check_tx_status, check_tx_response = check_transaction_finalized(tx.workspace['create::reference_number'])

            tx.workspace['response_code'] = check_tx_response['result']
            tx.workspace['response_message'] = check_tx_response['message']
            tx.payment_reference = check_tx_response['txn_receipt']
            tx.workspace['secure_hash'] = ''
            tx.workspace['vpc_command'] = 'queryDR'
            tx.workspace['response_parameters'] = {}

            log.debug("requery_stale_tx, check_transaction_finalized, {}...".format(check_tx_response))
            log.debug("requery_stale_tx, completing, %s~%s..." % (device_id, tx_id))

            trigger_completion_requery(tx)

            log.debug("requery_stale_tx, requery, triggered completion...")
        else:
            log.warn("ERROR!, requery_stale_tx, requery not allowed for this payment_type, %s..." % tx.payment_type)

        # Log response for error details
        tx.error_info['response_code'] = tx.workspace['response_code'] if 'response_code' in tx.workspace else ''
        tx.error_info['response_message'] = tx.workspace['response_message'] if 'response_message' in tx.workspace else ''

    except TransactionConflictException, ignored:
        log.warn("ERROR!, requery_stale_tx, requery task fired on a transaction in the wrong state; may have been retried?...")
        log.error(ignored)
    except AlreadyCancelledException, ignored:
        log.warn("ERROR!, requery_stale_tx, requery task fired on an already cancelled transaction. will ignore....")
        log.error(ignored)
    except Exception, e:
        log.warn("ERROR!, requery_stale_tx, something went wrong...")
        log.error(e)

    tx.was_requery = True
    tx.put()

@transactional
def trigger_completion_requery(tx):
    if tx.state != state.TX_CLIENT_PAYMENT_HOLD:
        log.warn("ERROR!, trigger_completion_requery, attempt to complete TX %s, which is not in TX_CLIENT_PAYMENT_HOLD; callback in error? - tx.state: %s" % (tx.key.id(), tx.state))

        return

    cancel_reaping(tx)
    transition_state(tx, state.TX_CLIENT_PAYMENT_HOLD, state.TX_FINALIZE)
    reschedule(tx)
