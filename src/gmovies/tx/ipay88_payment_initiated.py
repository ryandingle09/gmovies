import logging
import os

from datetime import datetime

from google.appengine.ext.ndb import Key, transactional
from google.appengine.ext import deferred

import state_machine

from gmovies.models import Listener, DATETIME_FORMAT
from gmovies.settings import RWPP_REAP_TIMEOUT

from .payment import do_payment
from .query import query_reservation_reference
from .scheduler import reschedule, cancel_reaping


log = logging.getLogger(__name__)

CHANNEL_NAME = 'ipay88-payment-complete'
LISTENER_ID = '$1$ipay88_payment_completion_listener'
TIMEOUT = 1200


def listener_bind():
    version = os.environ['CURRENT_VERSION_ID'].split('.')
    major_version = version[0]
    if Listener.query(Listener.event_channel==CHANNEL_NAME,
                      Listener.version==major_version,
                      Listener.listener_id==LISTENER_ID).count() == 0:
        l = Listener()
        l.event_channel = CHANNEL_NAME
        l.version = major_version
        l.listener_id = LISTENER_ID

        l.callback = payment_completion_listener
        log.info("Bound listener for channel: %s", CHANNEL_NAME)
        l.put()

def bind_listeners(tx):
    log.debug("(deferred) Binding listener for TX %s", tx.key.id())
    deferred.defer(listener_bind)

def payment_completion_listener(outstanding):
    for e in outstanding:
        log.debug("Processing: %s", e)
        if e.payload:
            if 'RefNo' not in e.payload:
                log.info('Cannot find Reference Number in callback response')
                continue
            reference = e.payload['RefNo']
            ipay88_payment_id = e.payload["TransId"]
            log.info("Got Reference Number: {0} Payment Transaction ID: {1}".format(reference, ipay88_payment_id))
            tx = query_reservation_reference(reference)
            if tx:
                log.info("Appending iPay88 payment transaction id to transaction payment reference field")
                tx.payment_reference = ipay88_payment_id
                tx.workspace["payment_status"] = e.payload["Status"]
                tx.workspace["payment_error_desc"] = e.payload["ErrDesc"]
                tx.workspace["ipay88_tx_signature"] = e.payload["Signature"]

                # Log response for error details
                tx.error_info['response_code'] = tx.workspace["payment_status"]
                tx.error_info['response_message'] = tx.workspace["payment_error_desc"]

                tx.put()

                log.debug("Completing: %s", tx.key.id())
                trigger_completion(tx)
                log.debug("Triggered completion")
                e.read = True
            else:
                log.info('Cannot find Transaction Reference: {}'.format(reference))

@transactional
def trigger_completion(tx):
    if tx.state != state_machine.state.TX_EXTERNAL_PAYMENT_HOLD:
        log.error("Attempt to complete TX %s, which is not in TX_EXTERNAL_PAYMENT_HOLD; callback in error? - tx.state: %s" % (tx.key.id(), tx.state))
        return

    cancel_reaping(tx)
    log.info("Finalizing seat reservation!")
    #r, msg = do_payment(tx)
    #log.info("Transitioning to generate ticket")
    #state_machine.transition_state(tx, state_machine.state.TX_EXTERNAL_PAYMENT_HOLD, state_machine.state.TX_GENERATE_TICKET)
    state_machine.transition_state(tx, state_machine.state.TX_EXTERNAL_PAYMENT_HOLD, state_machine.state.TX_FINALIZE)
    log.info("...rescheduling..")
    reschedule(tx)