import logging
import datetime

from google.appengine.api import memcache
from google.appengine.ext.ndb import Key
from google.appengine.ext import ndb
from gmovies.util import id_encoder
from gmovies.admin.email import notify_changed_schedules, notify_deleted_schedules
from google.appengine.api.app_identity import get_application_id
from gmovies.models import ReservationTransaction, Device, CSRCPayment, OtpRandom, OtpNumber, Schedule, Theater, TheaterOrganization, Movie, ConvenienceFeeSettings, TheaterOrganization
from gmovies import orgs
from gmovies.util import make_enum

state = make_enum('state', TX_START=0, TX_PREPARE=1, TX_STARTED=2,
        TX_RESERVATION_HOLD=3, TX_RESERVATION_OK=4, TX_PAYMENT_HOLD=5,
        TX_GENERATE_TICKET=6, TX_CLIENT_PAYMENT_HOLD=7, TX_FINALIZE=8,
        TX_EXTERNAL_PAYMENT_HOLD=9, TX_FINALIZE_HOLD=10, TX_PREPARE_ERROR=13,
        TX_RESERVATION_ERROR=11, TX_PAYMENT_ERROR=12, TX_DONE=100,
        TX_CANCELLED=101, TX_REFUNDED=102, TX_STATE_ERROR=-1)

def to_state_str(s):
    if s not in state.reverse_mapping:
        return 'TX_UNKNOWN'

    return state.reverse_mapping[s]

log = logging.getLogger(__name__)

SCREENING_TYPE = {'1': 'Reserved Seating', '2': 'Guaranteed Seats', '3': 'Free Seating'}

def date_string_to_time(tm):
    return str(datetime.datetime.strptime(tm,'%Y-%m-%dT%H:%M:%S').time())

def db_time_to_string(tm):
    return datetime.time.strftime(tm, '%H:%M:%S')

def theater_id(org_code, org_uuid):
    q = Theater.query()
    q = q.filter(Theater.org_theater_code==org_code)
    theater = q.fetch(1)[0]

    return theater

def find_canonical_title(movie_key):
    cache_key = "movie::canonical_title::%s" % movie_key.id()

    if not memcache.get(cache_key):
        memcache.set(cache_key, movie_key.get().canonical_title)

    return memcache.get(cache_key)
    
def check_schedule(schedule_id, org_code, org_uuid, schedule_api, feed_codes):
    theater = theater_id(org_code, org_uuid)
    schedule_key = ndb.Key(Schedule, schedule_id, parent=theater.key)
    schedule = schedule_key.get()

    schedule_from_db = schedule
    schedule_from_api = schedule_api
    screening_date_full = datetime.date.strftime(schedule_from_db.show_date, '%B %d, %Y')
    screening_date = datetime.date.strftime(schedule_from_db.show_date, '%Y-%m-%d')

    if str(org_code) == 'SMSJ':
        log.warn("SCHEDULE_DATA_BY_ID_FROM_DB: %s" % (schedule_from_db))
        log.warn("SCHEDULE_DATA_BY_ID_FROM_API: %s" % (schedule_from_api))
            
        details = {}

        details['url'] = "admin/theaters/%s/schedules/%s/" % (id_encoder.encoded_theater_id(theater.key), schedule_id.replace(" ","%20"))
        details['cinema_partner'] = str(theater.name)
        details['ciname_name'] = str(schedule_from_db.cinema)
        details['showing_date'] = screening_date_full
        details['app_id'] = get_application_id()

        # for testing in array data not in dict
        updated_start_time = schedule_from_api['Start_Time'] if schedule_from_api['Start_Time'] else getattr(schedule_from_api, 'Start_Time', '')
        updated_feed_code = schedule_from_api['ScheduleID'] if schedule_from_api['ScheduleID'] else getattr(schedule_from_api, 'ScheduleID', '')
        updated_movie_name = schedule_from_api['MovieCode'] if schedule_from_api['MovieCode'] else getattr(schedule_from_api, 'MovieCode', '')
        updated_seating_type = SCREENING_TYPE[str(schedule_from_api['Screening_Type'])] if schedule_from_api['Screening_Type'] else getattr(schedule_from_api, 'Screening_Type', '')
        updated_price = schedule_from_api['Price'] if schedule_from_api['Price'] else getattr(schedule_from_api, 'Price', '')
        updated_variant = schedule_from_api['FilmType'] if schedule_from_api['FilmType'] else getattr(schedule_from_api, 'FilmType', '')

        # updated_start_time = getattr(schedule_from_api, 'Start_Time', '')
        # updated_feed_code = getattr(schedule_from_api, 'ScheduleID', '')
        # updated_movie_name = getattr(schedule_from_api, 'MovieCode', '')
        # updated_seating_type = getattr(schedule_from_api, 'Screening_Type', '')
        # updated_price = getattr(schedule_from_api, 'Price', '')
        # updated_variant = getattr(schedule_from_api, 'FilmType', '')

        updated_start_time = date_string_to_time(updated_start_time)
        updated_price = int(float(updated_price))

        details['updated_feed_code'] = updated_feed_code
        details['updated_start_time'] = updated_start_time
        details['updated_movie_name'] = updated_movie_name
        details['updated_seating_type'] = updated_seating_type
        details['updated_price'] = updated_price
        details['updated_variant'] = updated_variant

        old_feed_code = ''
        old_start_time = ''
        old_movie_name = ''
        old_seating_type = ''
        old_price = ''
        old_variant = ''
        title_db = ''
        title_code = ''

        for slots in schedule_from_db.sorted_slots():
            # for deleted schedule detection
            if int(len(feed_codes)) != int(0):
                if slots.feed_code not in feed_codes:
                    log.warn("DELETED_FEED_CODE: %s" % (slots.feed_code))
                    details2 = {}
                    details2['url'] = "admin/theaters/%s/schedules/%s/" % (id_encoder.encoded_theater_id(theater.key), schedule_id.replace(" ","%20"))
                    details2['cinema_partner'] = str(theater.name)
                    details2['ciname_name'] = str(schedule_from_db.cinema)
                    details2['showing_date'] = screening_date_full
                    details2['app_id'] = get_application_id()
                    details2['status'] = 'One item in the schedule has been deleted.'
                    details2['old_feed_code'] = str(slots.feed_code)
                    details2['old_start_time'] = db_time_to_string(slots.start_time)
                    details2['old_movie_name'] = str(find_canonical_title(slots.movie))
                    details2['old_seating_type'] = str(slots.seating_type)
                    details2['old_price'] = int(slots.price)
                    details2['old_variant'] = str(slots.variant)
                    log.warn("SENDING_DELETED_NOTIFICATION: %s" % (details2))
                    notify_deleted_schedules(details2)

            if str(updated_feed_code) == str(slots.feed_code):
                old_feed_code = str(slots.feed_code)
                old_start_time = db_time_to_string(slots.start_time)
                old_movie_name = str(find_canonical_title(slots.movie))
                old_seating_type = str(slots.seating_type)
                old_price = int(slots.price)
                old_variant = str(slots.variant)

                details['old_feed_code'] = old_feed_code
                details['old_start_time'] = old_start_time
                details['old_movie_name'] = old_movie_name
                details['old_seating_type'] = old_seating_type
                details['old_price'] = old_price
                details['old_variant'] = old_variant

                title_db = str(old_movie_name)
                title_code = str(updated_movie_name)

                if '-' in title_code:
                    title_code = title_code[:title_code.rfind("-")]
                elif ')' in title_db:
                    title_db = title_db[title_db.rfind(") "):]

                title_db = title_db.replace(" ","").lower().replace(")","")
                title_code = title_code.replace(" ","").lower().replace(")","")

                log.debug("TITLE_DB: %s" % (title_db))
                log.debug("TITLE_API: %s" % (title_code))

                if title_code in title_db:
                    details['updated_movie_name'] = old_movie_name
                else:
                    details['updated_movie_name'] = updated_movie_name

                if str(old_start_time) != str(updated_start_time):
                    log.debug("old_start_time: %s, updated_start_time: %s" % (old_start_time, updated_start_time))
                    details['status'] = 'One of the Schedule has been Changed on <b>Screening Time</b>.'
                    log.warn("SENDING_EMAIL_NOTIFICATIONS: %s" % (details))
                    notify_changed_schedules(details)

                if str(title_code) not in str(title_db):
                    log.debug("old_movie_name: %s, updated_movie_name: %s" % (title_db, title_code))
                    details['status'] = 'One of the Schedule has been Changed on <b>Screening Time</b>.'
                    log.warn("SENDING_EMAIL_NOTIFICATIONS: %s" % (details))
                    notify_changed_schedules(details)
                
                if str(old_seating_type) != str(updated_seating_type):
                    log.debug("old_seating_type: %s, updated_seating_type: %s" % (old_seating_type, updated_seating_type))
                    details['status'] = 'One of the Schedule has been Changed on <b>Seating Type</b>.'
                    log.warn("SENDING_EMAIL_NOTIFICATIONS: %s" % (details))
                    notify_changed_schedules(details)
                
                if str(old_price) != str(updated_price):
                    log.debug("old_price: %s, updated_price: %s" % (old_price, updated_price))
                    details['status'] = 'One of the Schedule has been Changed on <b>Ticket Price</b>.'
                    log.warn("SENDING_EMAIL_NOTIFICATIONS: %s" % (details))
                    notify_changed_schedules(details)
                
                if str(old_variant) != str(updated_variant):
                    log.debug("old_variant: %s, updated_variant: %s" % (old_variant, updated_variant))
                    details['status'] = 'One of the Schedule has been Changed on <b>Movie Variant</b>.'
                    log.warn("SENDING_EMAIL_NOTIFICATIONS: %s" % (details))
                    notify_changed_schedules(details)

        log.warn("DETAILS_DATA: %s" % (details))

def query_info(device_id, tx_id):
    log.debug("Querying for TX entity %s" % tx_id)
    tx = ReservationTransaction.get_by_id(tx_id, parent=Key(Device, device_id))
    return tx

def transaction_array(transaction, tx_id = False):
    primary_info        = []
    payment_info        = {}
    user_info           = []
    reservation_info    = []
    ticket_info         = {}

    primary_info.append({
        "transaction_id"        : tx_id,
        "transaction_date"      : str(transaction.date_created.strftime('%Y-%m-%d %H:%M:%S')) if transaction.date_created else '',
        "payment_reference"     : transaction.payment_reference,
        "reservation_reference" : transaction.reservation_reference,
        "platform"              : transaction.platform,
        "state"                 : to_state_str(transaction.state),
        "status"                : transaction.status if transaction.status else 'unclaimed',
    })

    log.debug("primary_info data : %s" % primary_info)

    if transaction.reservations:
        for i,r in enumerate(transaction.reservations):
            reservation_info.append({ 
                "time" : str(r.time),
                "cinema" : r.cinema,
                "seat_id" : r.seat_id
            })

    else:
        reservation_info.append({"Transaction has no reservations?"})

    log.debug("reservation_info data : %s" % reservation_info)

    if transaction.payment_info:
        payment_info.update({"payment_type" : transaction.payment_type })

        if transaction.payment_type == 'mpass':
            payment_info.update({"email" : transaction.payment_info['email']})

        elif transaction.payment_type == 'credit-card':
            payment_info.update({"email" : transaction.payment_info['email']})

        elif transaction.payment_type == 'globe-promo':
            payment_info.update({"full_name"    : transaction.payment_info['full_name']})
            payment_info.update({"email"        : transaction.payment_info['email']})
            payment_info.update({"mobile"       : transaction.payment_info['mobile']})

        elif transaction.payment_type in ['gcash','g-cash-app']:
            payment_info.update({"email"            : transaction.payment_info['email']})
            payment_info.update({"full_name"        : transaction.payment_info['full_name']})
            payment_info.update({"mobile"           : transaction.payment_info['mobile']})
            payment_info.update({"gcash_account"    : transaction.payment_info['gcash_account']})

        elif transaction.payment_type in ['grewards']:
            payment_info.update({"email"            : transaction.payment_info['email']})
            payment_info.update({"full_name"        : transaction.payment_info['full_name']})
            payment_info.update({"mobile"           : transaction.payment_info['mobile']})
            payment_info.update({"grewards_account" : transaction.payment_info['grewards_account']})

        elif transaction.payment_type == 'client-initiated':
            payment_info.update({"email" : transaction.payment_info['email']})
            
        elif transaction.payment_type == 'paynamics-payment-initiated':
            payment_info.update({"email"        : transaction.payment_info['email']})
            payment_info.update({"first_name"   : transaction.payment_info['fname']})
            payment_info.update({"middle_name"  : transaction.payment_info['mname']})
            payment_info.update({"last_name"    : transaction.payment_info['lname']})
            payment_info.update({"mobile"       : transaction.payment_info['mobile']})

        elif transaction.payment_type == 'migs-payment-initiated':
            payment_info.update({"email"        : transaction.payment_info['email']})
            payment_info.update({"full_name"    : '%s %s' % (transaction.payment_info['first_name'], transaction.payment_info['last_name'])})
            payment_info.update({"first_name"   : transaction.payment_info['fname']})
            payment_info.update({"middle_name"  : transaction.payment_info['mname']})
            payment_info.update({"last_name"    : transaction.payment_info['lname']})
            payment_info.update({"mobile"       : transaction.payment_info['mobile']})
            
        elif transaction.payment_type in ['bpi-cardholder-promo', 'citibank-cardholder-promo', 'pnb-cardholder-promo', 'bdo-cardholder-promo', 'g-cash-cardholder-promo', 'metrobank-cardholder-promo', 'chinabank-cardholder-promo', 'robinsonsbank-cardholder-promo']:
            if transaction.discount_info:
                if 'payment_gateway' in transaction.discount_info:
                    if transaction.discount_info['payment_gateway'] == 'pesopay':
                        payment_info.update({"email" : transaction.payment_info['email']})

                    elif transaction.discount_info['payment_gateway'] == 'paynamics':
                        payment_info.update({"email"        : transaction.payment_info['email']})
                        payment_info.update({"full_name"    : '%s %s' % (transaction.payment_info['first_name'], transaction.payment_info['last_name'])})
                        payment_info.update({"first_name"   : transaction.payment_info['fname']})
                        payment_info.update({"middle_name"  : transaction.payment_info['mname']})
                        payment_info.update({"last_name"    : transaction.payment_info['lname']})
                        payment_info.update({"mobile"       : transaction.payment_info['mobile']})

                    elif transaction.discount_info['payment_gateway'] == 'migs':
                        payment_info.update({"email"        : transaction.payment_info['email']})
                        payment_info.update({"full_name"    : '%s %s' % (transaction.payment_info['first_name'], transaction.payment_info['last_name'])})
                        payment_info.update({"first_name"   : transaction.payment_info['fname']})
                        payment_info.update({"middle_name"  : transaction.payment_info['mname']})
                        payment_info.update({"last_name"    : transaction.payment_info['lname']})
                        payment_info.update({"mobile"       : transaction.payment_info['mobile']})

                    elif transaction.discount_info['payment_gateway'] == 'ipay88':
                        payment_info.update({"email"        : transaction.payment_info['email']})
                        payment_info.update({"full_name"    : '%s %s' % (transaction.payment_info['first_name'], transaction.payment_info['last_name'])})
                        payment_info.update({"mobile"       : transaction.payment_info['contact_number']})
    else:
        payment_info.update({"Transaction has no payment info ?"})

    log.debug("payment_info data : %s" % payment_info)

    if transaction.user_info:
        user_info.append({
            "lbid"        : transaction.user_info['lbid'],
            "uuid"        : transaction.user_info['uuid'],
            "first_name"  : transaction.user_info['first_name'],
            "last_name"   : transaction.user_info['last_name'],
            "email"       : transaction.user_info['email'],
            "mobile"      : transaction.user_info['mobile']
        })
    else:
        user_info.append({"Transaction has no user info?"})

    log.debug("user_info data : %s" % user_info)

    if transaction.ticket:
        ticket_info.update({"reservation_reference"    : transaction.ticket.ref })
        ticket_info.update({"ticket_code"              : transaction.ticket.code })
        ticket_info.update({"ticket_date"              : str(transaction.ticket.date.strftime('%Y-%m-%d')) if transaction.ticket.date else '' })

        movie_title     = ''
        cinema_partner  = ''

        if 'movie_canonical_title' in transaction.ticket.extra and transaction.ticket.extra['movie_canonical_title']:
            movie_title = transaction.ticket.extra['movie_canonical_title']

        elif 'movie_title' in transaction.ticket.extra and transaction.ticket.extra['movie_title']:
            movie_title = transaction.ticket.extra['movie_title']

        if 'theater_name' in transaction.ticket.extra and transaction.ticket.extra['theater_name']:
            cinema_partner = transaction.ticket.extra['theater_name']

        elif 'theater_and_cinema_name' in transaction.ticket.extra and transaction.ticket.extra['theater_and_cinema_name']:
            cinema_partner = transaction.ticket.extra['theater_and_cinema_name'][0].split('-')[0].strip()

        reservations_theater = transaction.reservations
        theater_organization = ''

        if str(orgs.AYALA_MALLS) in str(reservations_theater):
            theater_organization = 'AYALA MALLS'

        elif str(orgs.SM_MALLS) in str(reservations_theater):
            theater_organization = 'SM MALLS'

        elif str(orgs.ROBINSONS_MALLS) in str(reservations_theater):
            theater_organization = 'ROBINSONS MALLS'

        elif str(orgs.GREENHILLS_MALLS) in str(reservations_theater):
            theater_organization = 'GREENHILLS MALLS'

        elif str(orgs.MEGAWORLD_MALLS) in str(reservations_theater):
            theater_organization = 'MEGAWORLDS MALLS'

        elif str(orgs.CITY_MALLS) in str(reservations_theater):
            theater_organization = 'CITY MALLS'

        elif str(orgs.CINEMA76_MALLS) in str(reservations_theater):
            theater_organization = 'CINEMA76 MALLS'

        ticket_info.update({"theater_organization"  : theater_organization })
        ticket_info.update({"cinema_partner"        : cinema_partner })
        ticket_info.update({"movie_title"           : movie_title })

        if 'original_total_amount' in transaction.ticket.extra:
            ticket_info.update({"original_total_amount" : transaction.ticket.extra['original_total_amount'] })

        else:
            ticket_info.update({"original_total_amount" : transaction.ticket.extra['total_amount'] })

        ticket_info.update({"total_amount" : transaction.ticket.extra['total_amount'] })

        if 'total_discount' in transaction.ticket.extra:
            ticket_info.update({"total_discount" : transaction.ticket.extra['total_discount'] })
        else:
            ticket_info.update({"total_discount" : "0.00"})
    
    log.debug("ticket_info data : %s" % ticket_info)

    result = [{
        "transaction":  {
            "primary_info"      : primary_info,
            "payment_info"      : [payment_info],
            "user_info"         : user_info,
            "reservation_info"  : reservation_info,
            "ticket_info"       : [ticket_info]
        } 
    }]

    return result

def update_otp():
    log.debug("Update otp pin")
    q = OtpRandom.query()
    return q.fetch(1)[0]

def query_otp(code):
    log.debug("Querying for OTP code verify %s" % code)
    q = OtpRandom.query()
    
    if q.count() == 1:
        return q.fetch(1)[0]

    elif q.count() > 1:
        pass

    else:
        return None

def query_otp_number():
    log.debug("Querying for OTP default number")
    q = OtpNumber.query()

    if q.count() == 1:
        return q.fetch(1)[0]

    elif q.count() > 1:
        pass

    else:
        return None

def query_state(device_id, tx_id):
    log.debug("Querying for TX state %s" % tx_id)
    tx = ReservationTransaction.get_by_id(tx_id, parent=Key(Device, device_id))
    if not tx:
        return None, None

    if 'msg' in tx.workspace:
        msg = tx.workspace['msg']
    else:
        msg = None

    return tx.state, msg

def query_reservation_reference(reference):
    q = ReservationTransaction.query()
    q = q.filter(ReservationTransaction.reservation_reference==reference)

    if q.count() == 1:
        return q.fetch(1)[0]

    elif q.count() > 1:
        # TODO: Raise an exception, integrity failure
        log.info("SKIP!, query_reservation_reference: duplicate reservation_reference.")
        pass

    else:
        log.info("SKIP!, query_reservation_reference: something went wrong.")
        return None


def query_csrc_payment_reference(reference):
    q = CSRCPayment.query()
    q = q.filter(CSRCPayment.process_reference==reference)

    if q.count() == 1:
        return q.fetch(1)[0]

    elif q.count() > 1:
        # TODO: Raise an exception, integrity failure
        pass

    else:
        return None

def query_cf(org_id):
    q = ConvenienceFeeSettings.query(ancestor=Key(TheaterOrganization, org_id))

    if q.count() == 1:
        log.info("FOUND, query_cf: %s" % (q.fetch(1)[0].convenience_fees))
        return q.fetch(1)[0].convenience_fees

    else:
        log.info("SKIP!, query_cf: something went wrong.")
        return None
