import logging
log = logging.getLogger(__name__)

import os
from datetime import datetime
from google.appengine.ext.ndb import Key, transactional
from google.appengine.ext import deferred

from gmovies.models import Listener, DATETIME_FORMAT, CSRCPayment
from gmovies.util.admin import camelcase
from gmovies.admin.email import send_csr_campaign_payment_notification, donor_payment_notification
from gmovies.bank_transfer import AccountSettings

from .query import query_csrc_payment_reference

CHANNEL_NAME='ipay88-csrc-payment-complete'
LISTENER_ID='$1$ipay88_csrc_payment_completion_listener'

def listener_bind():
    version = os.environ['CURRENT_VERSION_ID'].split('.')
    major_version = version[0]
    if Listener.query(Listener.event_channel==CHANNEL_NAME,
                      Listener.version==major_version,
                      Listener.listener_id==LISTENER_ID).count() == 0:
        l = Listener()
        l.event_channel = CHANNEL_NAME
        l.version = major_version
        l.listener_id = LISTENER_ID

        l.callback = process_completion_listener
        log.info("Bound listener for channel: %s", CHANNEL_NAME)
        l.put()

def bind_listeners(tx):
    log.debug("(deferred) Binding listener for TX %s", tx.key.id())
    deferred.defer(listener_bind)

def process_completion_listener(outstanding):
    log.info("Entering completion listener")
    for e in outstanding:
        log.info("Processing: %s", e)
        if e.payload:
            log.info("Has Payload...")
            if 'RefNo' not in e.payload:
                continue
            log.info("Reference Number found: {}".format(e.payload['RefNo']))
            reference = e.payload['RefNo']
            ipay88_payment_id = e.payload["TransId"]

            tx = query_csrc_payment_reference(reference)
            log.info("TX By Ref: {}".format(tx))
            if tx and tx.state == CSRCPayment.proc_state.TX_PAYMENT_HOLD:
                log.info("Appending iPay88 payment transaction id to transaction payment reference field")
                tx.bank_tx_reference = ipay88_payment_id
                tx.workspace["payment_status"] = e.payload["Status"]
                tx.workspace["payment_error_desc"] = e.payload["ErrDesc"]
                tx.workspace["ipay88_tx_signature"] = e.payload["Signature"]

                # Log response for error details
                tx.error_info['response_code'] = tx.workspace["payment_status"]
                tx.error_info['response_message'] = tx.workspace["payment_error_desc"]

                tx.callback_data = dict(e.payload)
                log.debug("Completing: %s", tx.key.id())
                finalize_tx(tx, e)
                tx.put()
                deferred.defer(send_email, reference)
                e.read = True

@transactional
def finalize_tx(tx, event):
    log.info("Current State: {}".format(CSRCPayment.stringify_state(tx.state)))
    if tx.state != CSRCPayment.proc_state.TX_PAYMENT_HOLD:
        log.error("Attempt to complete CSR Campaign Payment TX %s, which is not in TX_PAYMENT_HOLD; callback in error? - tx.state: %s" % (tx.key.id(), tx.state))
    elif event.payload["Status"] == "0":
        log.error("Attempt to complete CSR Campaign Payment TX {}, but got a FAILED status response with message: {}?".format(tx.key.id(), event.payload["ErrDesc"]))
        tx.state = CSRCPayment.proc_state.TX_PAYMENT_ERROR
    else:
        log.info("CSR Campaign Payment for TX {} completed".format(tx.key.id()))
        tx.state = CSRCPayment.proc_state.TX_DONE
        tx.completed = datetime.now()
        #deferred.defer(register_donors, tx.process_reference)
    return

# def register_donors(reference):
#     log.info("Registering Donors...")
#     account = AccountSettings.get_by_id('csrc_settings')
#     tx = query_csrc_payment_reference(reference)
    
#     donor = { 
#                 'campaign' : camelcase(str(tx.deposit_info["campaign"])),
#                 'username' : tx.deposit_info['first_name'] + ' ' + tx.deposit_info['last_name'],
#                 'email_address' : tx.deposit_info["email"], 
#                 'contact_number' : tx.deposit_info["contact_number"], 
#                 'amount' : tx.deposit_info["amount"], 
#                 'reference_code' : tx.process_reference, 
#                 'trans_id' : tx.bank_tx_reference
#             }

#     log.info("Donor Data: {}".format(donor))

#     if account.donors_list is None:
#         log.info("No donors found. Creating a list for appending")
#         donors = []
#         donors.append(donor)
#         account.donors_list = donors
#     else:
#         donors = account.donors_list
#         log.info("Donors in datastore: {}".format(donors))
#         donors.append(donor)
#         account.donors_list = donors

#     # last ten donors
#     if account.donors_last_10 is None:
#         log.info("No last 10 donors found. Creating a list for appending")
#         donors = []
#         donors.append(donor)
#         account.donors_last_10 = donors
#     else:
#         donors = account.donors_last_10
#         log.info("Latest 10 donors in datastore: {}".format(donors))
#         donors.append(donor)
#         account.donors_last_10 = donors    

#     account.put()

def send_email(reference):
    tx = query_csrc_payment_reference(reference)
    if tx.state == 100:
        send_csr_campaign_payment_notification(campaign=camelcase(str(tx.deposit_info["campaign"])),
                                                username=tx.deposit_info['first_name'] + ' ' + tx.deposit_info['last_name'],
                                                email_address=tx.deposit_info["email"], 
                                                contact_number=tx.deposit_info["contact_number"], 
                                                amount=tx.deposit_info["amount"], 
                                                reference_code=tx.process_reference, 
                                                trans_id=tx.bank_tx_reference, 
                                                auth_code=tx.callback_data["AuthCode"])

        donor_payment_notification(campaign=camelcase(str(tx.deposit_info["campaign"])), 
                                    email_address=tx.deposit_info["email"],
                                    amount=tx.deposit_info["amount"],
                                    reference_code=tx.process_reference,
                                    trans_id=tx.bank_tx_reference,
                                    auth_code=tx.callback_data["AuthCode"])

        log.info("Email notification sent!")
    elif tx.state == -1:
        log.info("Didn't send email due to failed TX status.")
