import logging
log = logging.getLogger(__name__)

from google.appengine.ext.ndb import transactional
from .query import query_info
from .actions import cancel
from gmovies.exceptions.api import AlreadyCancelledException, TransactionConflictException

@transactional
def reap_stale_tx(device_id, tx_id):
    tx = query_info(device_id, tx_id)
    tx.workspace['reap_task'] = None

    try:
        cancel(device_id, tx_id)
    except TransactionConflictException, ignored:
        log.warn("Reap task fired on a transaction in the wrong state; may have been retried?")
    except AlreadyCancelledException, ignored:
        log.warn("Reap task fired on an already cancelled transaction. Will ignore.")

    tx.was_reaped = True
    tx.put()
