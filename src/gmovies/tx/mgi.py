import logging

from gmovies import orgs
from gmovies.settings import (EPLUS_MERCHANT_ID, EPLUS_MERCHANT_CODE, EPLUS_SECRET_KEY,
        SM_USERNAME_MGI, SM_PASSWORD_MGI, LCT_USERNAME_MGI, LCT_PASSWORD_MGI, ROBINSONS_USERNAME_MGI, ROBINSONS_PASSWORD_MGI)
from gmovies.ws.soap_connector import EPlusWSMGi, TransactionsWSMGi, CCServiceWSPaynamics
from gmovies.ws.endpoints import (LCT_SESSION_API, LCT_TRANSACTION_API,
        SM_EPLUS_API, SM_SESSION_API, SM_TRANSACTION_API, ROBINSONS_SESSION_API, ROBINSONS_TRANSACTION_API,
        PAYNAMICS_CCSERVICE_URL, PAYNAMICS_TARGETNAMESPACE)

log = logging.getLogger(__name__)

ALLOWED_MGI_RESERVATION_BRANCH_CODES = ['LCT'] # MGI Hosted Megaworld Theaters
MGW_LCT_DICTKEY = '%s::LCT' % str(orgs.MEGAWORLD_MALLS)
SESSION_ENDPOINTS = {str(orgs.SM_MALLS): SM_SESSION_API, MGW_LCT_DICTKEY: LCT_SESSION_API, str(orgs.ROBINSONS_MALLS): ROBINSONS_SESSION_API}
TRANSACTION_ENDPOINTS = {str(orgs.SM_MALLS): SM_TRANSACTION_API, MGW_LCT_DICTKEY: LCT_TRANSACTION_API, str(orgs.ROBINSONS_MALLS): ROBINSONS_TRANSACTION_API}
MGI_USERNAMES = {str(orgs.SM_MALLS): SM_USERNAME_MGI, MGW_LCT_DICTKEY: LCT_USERNAME_MGI, str(orgs.ROBINSONS_MALLS): ROBINSONS_USERNAME_MGI}
MGI_PASSWORDS = {str(orgs.SM_MALLS): SM_PASSWORD_MGI, MGW_LCT_DICTKEY: LCT_PASSWORD_MGI, str(orgs.ROBINSONS_MALLS): ROBINSONS_PASSWORD_MGI}


def create_transaction_mgi(payload, theaterorg_uuid, theater_code=None, is_multiple_branch=False):
    # default, assuming this transaction encountered an error.
    transaction = {}
    transaction['token'] = '' # use for pin parameter in FinalizeTransaction.
    transaction['reference_number'] = ''
    transaction['message'] = 'An error occurred during your transaction. Kindly contact support@gmovies.ph to address your issue.'

    ENDPOINT_DICTKEY = str(theaterorg_uuid)

    if not is_multiple_branch and theater_code:
        ENDPOINT_DICTKEY = '%s::%s' % (ENDPOINT_DICTKEY, theater_code)

    if (ENDPOINT_DICTKEY in TRANSACTION_ENDPOINTS and ENDPOINT_DICTKEY in SESSION_ENDPOINTS and
            ENDPOINT_DICTKEY in MGI_USERNAMES and ENDPOINT_DICTKEY in MGI_PASSWORDS):
        MGI_USERNAME = MGI_USERNAMES[ENDPOINT_DICTKEY]
        MGI_PASSWORD = MGI_PASSWORDS[ENDPOINT_DICTKEY]
        SESSION_API = SESSION_ENDPOINTS[ENDPOINT_DICTKEY]
        TRANSACTION_API = TRANSACTION_ENDPOINTS[ENDPOINT_DICTKEY]
    else:
        log.warn("SKIP!, Theater Organization UUID ----- doesn't exist...")

        return 'error', transaction

    try:
        log.info("create transaction payload: {}...".format(payload))

        mgi_transactions = TransactionsWSMGi(TRANSACTION_API, SESSION_API, MGI_USERNAME, MGI_PASSWORD)
        transaction = mgi_transactions.do_create_transaction(**payload)
    except Exception, e:
        log.warn("ERROR!, create_transaction_mgi...")
        log.error(e)

    status = transaction.pop('status', 'error')

    return status, transaction

def finalize_transaction_mgi(payload, theaterorg_uuid, theater_code=None, is_multiple_branch=False):
    # default, assuming this transaction encountered an error.
    transaction = {}
    transaction['token'] = ''
    transaction['payment_reference'] = ''
    transaction['message'] = 'An error occurred during your transaction. Kindly contact support@gmovies.ph to address your issue.'

    ENDPOINT_DICTKEY = str(theaterorg_uuid)

    if not is_multiple_branch and theater_code:
        ENDPOINT_DICTKEY = '%s::%s' % (ENDPOINT_DICTKEY, theater_code)

    if (ENDPOINT_DICTKEY in TRANSACTION_ENDPOINTS and ENDPOINT_DICTKEY in SESSION_ENDPOINTS and
            ENDPOINT_DICTKEY in MGI_USERNAMES and ENDPOINT_DICTKEY in MGI_PASSWORDS):
        MGI_USERNAME = MGI_USERNAMES[ENDPOINT_DICTKEY]
        MGI_PASSWORD = MGI_PASSWORDS[ENDPOINT_DICTKEY]
        SESSION_API = SESSION_ENDPOINTS[ENDPOINT_DICTKEY]
        TRANSACTION_API = TRANSACTION_ENDPOINTS[ENDPOINT_DICTKEY]
    else:
        log.warn("SKIP!, Theater Organization UUID ----- doesn't exist...")

        return 'error', transaction

    try:
        log.info("finalize transaction payload: {}...".format(payload))

        mgi_transactions = TransactionsWSMGi(TRANSACTION_API, SESSION_API, MGI_USERNAME, MGI_PASSWORD)
        transaction = mgi_transactions.do_finalize_transaction(**payload)
    except Exception, e:
        log.warn("ERROR!, finalize_transaction_mgi...")
        log.error(e)

    status = transaction.pop('status', 'error')

    return status, transaction

# FOR Robinsons MIGS Payment type Requery
def check_transaction_finalized(reference_number):
    # default, assuming this transaction encountered an error.
    transaction = {}
    transaction['result'] = ''
    transaction['txn_receipt'] = ''
    transaction['message'] = 'An error occurred during your transaction. Kindly contact support@gmovies.ph to address your issue.'

    ENDPOINT_DICTKEY = str(orgs.ROBINSONS_MALLS)

    if (ENDPOINT_DICTKEY in TRANSACTION_ENDPOINTS and ENDPOINT_DICTKEY in SESSION_ENDPOINTS and
            ENDPOINT_DICTKEY in MGI_USERNAMES and ENDPOINT_DICTKEY in MGI_PASSWORDS):
        MGI_USERNAME = MGI_USERNAMES[ENDPOINT_DICTKEY]
        MGI_PASSWORD = MGI_PASSWORDS[ENDPOINT_DICTKEY]
        SESSION_API = SESSION_ENDPOINTS[ENDPOINT_DICTKEY]
        TRANSACTION_API = TRANSACTION_ENDPOINTS[ENDPOINT_DICTKEY]
    else:
        log.warn("SKIP!, Theater Organization UUID ----- doesn't exist...")

        return 'error', transaction

    try:
        log.info("check_transaction_finalized payload: ReferenceNumber {}...".format(reference_number))

        mgi_transactions = TransactionsWSMGi(TRANSACTION_API, SESSION_API, MGI_USERNAME, MGI_PASSWORD)
        transaction = mgi_transactions.do_check_transaction_finalized(reference_number)
    except Exception, e:
        log.warn("ERROR!, check_transaction_finalized...")
        log.error(e)

    status = transaction.pop('status', 'error')

    return status, transaction

def create_transaction_eplus(card_number, pin, payload):
    # default, assuming this transaction encountered an error.
    transaction = {}
    transaction['reference_number'] = ''
    transaction['eplus_remaining_balance'] = ''
    transaction['message'] = 'An error occurred during your transaction. Kindly contact support@gmovies.ph to address your issue.'

    try:
        log.info("E-Plus, card_number: %s, pin: %s..." % (card_number, pin))
        log.info("E-Plus, create transaction payload: {}...".format(payload))

        eplus_transactions = EPlusWSMGi(SM_EPLUS_API)
        transaction = eplus_transactions.do_online_wallet_member_transaction(card_number, pin, EPLUS_SECRET_KEY, **payload)
    except Exception, e:
        log.warn("ERROR!, create_transaction_eplus...")
        log.error(e)

    status = transaction.pop('status', 'error')

    return status, transaction

def check_balance_eplus(card_number, theaterorg_uuid, theater_code=None, is_multiple_branch=False):
    # default, assuming this transaction encountered an error.
    check_balance = {}
    check_balance['message'] = 'An error occurred during your transaction. Kindly contact support@gmovies.ph to address your issue.'

    ENDPOINT_DICTKEY = str(theaterorg_uuid)

    if not is_multiple_branch and theater_code:
        ENDPOINT_DICTKEY = '%s::%s' % (ENDPOINT_DICTKEY, theater_code)

    if ENDPOINT_DICTKEY in SESSION_ENDPOINTS and ENDPOINT_DICTKEY in MGI_USERNAMES and ENDPOINT_DICTKEY in MGI_PASSWORDS:
        MGI_USERNAME = MGI_USERNAMES[ENDPOINT_DICTKEY]
        MGI_PASSWORD = MGI_PASSWORDS[ENDPOINT_DICTKEY]
        SESSION_API = SESSION_ENDPOINTS[ENDPOINT_DICTKEY]
    else:
        log.warn("SKIP!, Theater Organization UUID ----- doesn't exist...")

        return 'error', check_balance

    try:
        log.info("E-Plus, card_number: %s..." % card_number)

        eplus_transactions = EPlusWSMGi(SM_EPLUS_API)
        check_balance = eplus_transactions.do_online_wallet_member_check_balance(SESSION_API, MGI_USERNAME, MGI_PASSWORD, card_number)
    except Exception, e:
        log.warn("ERROR!, check_balance_eplus...")
        log.error(e)

    status = check_balance.pop('status', 'error')

    return status, check_balance

# Paynamics CC TOKENIZATION
def rebill_with_token(merchant_id, request_id, ip_address, response_id, token_id, trx_type,
                amount, notification_url, response_url, signature):
    # default, assuming this transaction encountered an error.
    rebill_with_token_response = {}
    rebill_with_token_response['message'] = 'Sorry, an error occurred. Please try re-launching the app or contact support@gmovies.ph.'

    try:
        ccservicepaynamics = CCServiceWSPaynamics(PAYNAMICS_CCSERVICE_URL, PAYNAMICS_TARGETNAMESPACE)
        rebill_with_token_response = ccservicepaynamics.do_rebill_with_token(merchant_id,
                request_id, ip_address, response_id, token_id, trx_type,
                amount, notification_url, response_url, signature)
    except Exception, e:
        log.warn("ERROR!, rebill_with_token...")
        log.error(e)

    status = rebill_with_token_response.pop('status', 'error')

    return status, rebill_with_token_response

def rebill_with_token_3d(merchant_id, request_id, ip_address, response_id, token_id, trx_type,
                amount, notification_url, response_url, secure3d_policy, signature):
    # default, assuming this transaction encountered an error.
    rebill_with_token_response = {}
    rebill_with_token_response['message'] = 'Sorry, an error occurred. Please try re-launching the app or contact support@gmovies.ph.'

    try:
        ccservicepaynamics = CCServiceWSPaynamics(PAYNAMICS_CCSERVICE_URL, PAYNAMICS_TARGETNAMESPACE)
        rebill_with_token_response = ccservicepaynamics.do_rebill_with_token_3d(merchant_id,
                request_id, ip_address, response_id, token_id, trx_type,
                amount, notification_url, response_url, secure3d_policy, signature)
    except Exception, e:
        log.warn("ERROR!, rebill_with_token_3d...")
        log.error(e)

    status = rebill_with_token_response.pop('status', 'error')

    return status, rebill_with_token_response

# Paynamics Payment Reversal
def reversal(merchant_id, request_id, ip_address, response_id,
                amount, notification_url, response_url, signature):
    # default, assuming this transaction encountered an error.
    reversal_response = {}
    reversal_response['message'] = 'Sorry, an error occurred. Please try re-launching the app or contact support@gmovies.ph.'

    try:
        ccservicepaynamics = CCServiceWSPaynamics(PAYNAMICS_CCSERVICE_URL, PAYNAMICS_TARGETNAMESPACE)
        reversal_response = ccservicepaynamics.do_reversal(merchant_id,
                request_id, ip_address, response_id,
                amount, notification_url, response_url, signature)
    except Exception, e:
        log.warn("ERROR!, reversal...")
        log.error(e)

    status = reversal_response.pop('status', 'error')

    return status, reversal_response

# Paynamics Payment Refund
def refund(merchant_id, request_id, ip_address, response_id,
                amount, notification_url, response_url, signature):
    # default, assuming this transaction encountered an error.
    refund_response = {}
    refund_response['message'] = 'Sorry, an error occurred. Please try re-launching the app or contact support@gmovies.ph.'

    try:
        ccservicepaynamics = CCServiceWSPaynamics(PAYNAMICS_CCSERVICE_URL, PAYNAMICS_TARGETNAMESPACE)
        refund_response = ccservicepaynamics.do_refund(merchant_id,
                request_id, ip_address, response_id,
                amount, notification_url, response_url, signature)
    except Exception, e:
        log.warn("ERROR!, refund...")
        log.error(e)

    status = refund_response.pop('status', 'error')

    return status, refund_response