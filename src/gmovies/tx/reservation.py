from __future__ import absolute_import

import logging
import json
import requests
import traceback

from datetime import datetime
from lxml import etree

from google.appengine.api import urlfetch
from google.appengine.ext.ndb import get_multi, non_transactional, Key

from gmovies.orgs import (AYALA_MALLS, ROCKWELL_MALLS, GREENHILLS_MALLS, SM_MALLS, MEGAWORLD_MALLS,
        ROBINSONS_MALLS, CINEMA76_MALLS, GLOBE_EVENTS)
from gmovies.models import ConvenienceFeeSettings, PaymentOptionSettings, ReservationInfo, ReservationTransaction, TheaterOrganization
from gmovies.settings import ROCKWELL_ENDPOINT_PRIMARY, GREENHILLS_ENDPOINT_PRIMARY, CINEMA76_ENDPOINT_PRIMARY, GLOBEEVENTS_ENDPOINT_PRIMARY
from gmovies.admin.email import send_email_support

from .sureseats import create_sureseats_request
from .rockwell import create_rockwell_request
from .greenhills import create_greenhills_request
from .cinema76 import create_cinema76_request
from .globeevents import create_globeevents_request
from .mgi import create_transaction_mgi, ALLOWED_MGI_RESERVATION_BRANCH_CODES
from .util import call_once, translate_message_via_error_code, DEFAULT_ERROR_MESSAGE
from .payment import is_token_paynamics, is_3d_token_paynamics, WS_PAYMENT_ENGINE

log = logging.getLogger(__name__)

SURESEATS_ENDPOINT_PRIMARY = 'http://api.sureseats.com/globe.asp'
SURESEATS_ENDPOINT_SECONDARY = 'http://api.sureseats.com/index_globe.asp'
ROCKWELL_ENDPOINT_RESERVE_SEATS = ROCKWELL_ENDPOINT_PRIMARY + 'book-movie/lock-seats'
GREENHILLS_ENDPOINT_RESERVE_SEATING = GREENHILLS_ENDPOINT_PRIMARY + 'book-movie/lock-seats'
GREENHILLS_ENDPOINT_FREE_SEATING = GREENHILLS_ENDPOINT_PRIMARY + 'book-movie/lock-seats-free-seating'
GREENHILLS_SEATS_FREE_SEATING_FEED_URL = GREENHILLS_ENDPOINT_PRIMARY + 'movie-schedules/seats-free-seating?movieScheduleId=%s&showtimeId=%s'
CINEMA76_SEATS_FREE_SEATING_FEED_URL = CINEMA76_ENDPOINT_PRIMARY + 'movie-schedules/seats-free-seating?movieScheduleId=%s&showtimeId=%s'
CINEMA76_ENDPOINT_FREE_SEATING = CINEMA76_ENDPOINT_PRIMARY + 'book-movie/lock-seats-free-seating'
GLOBEEVENTS_SEATS_FREE_SEATING_FEED_URL = GLOBEEVENTS_ENDPOINT_PRIMARY + 'movie-schedules/seats-free-seating?movieScheduleId=%s&showtimeId=%s'
GLOBEEVENTS_ENDPOINT_FREE_SEATING = GLOBEEVENTS_ENDPOINT_PRIMARY + 'book-movie/lock-seats-free-seating'

TIMEOUT_DEADLINE = 45
RESERVATION_ACTION = 'TRANSACT2'
RESERVATION_TYPE = 'RESERVE'
BUY_TYPE = 'BUY'
SOURCE_ID = '9'

# RESERVATION TRANSACTION PAYMENT TYPES EQUIVALENT TO PAYMENT OPTIONS IDENTIFIERS
BPI_CARDHOLDER_PROMO_PESOPAY = '%s-pesopay' % ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO
BPI_CARDHOLDER_PROMO_PAYNAMICS = '%s-paynamics' % ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO
BPI_CARDHOLDER_PROMO_MIGS = '%s-migs' % ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO
BPI_CARDHOLDER_PROMO_IPAY88 = '%s-ipay88' % ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO

CITIBANK_CARDHOLDER_PROMO_PESOPAY = '%s-pesopay' % ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO
CITIBANK_CARDHOLDER_PROMO_PAYNAMICS = '%s-paynamics' % ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO
CITIBANK_CARDHOLDER_PROMO_MIGS = '%s-migs' % ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO
CITIBANK_CARDHOLDER_PROMO_IPAY88 = '%s-ipay88' % ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO

PNB_CARDHOLDER_PROMO_PESOPAY = '%s-pesopay' % ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO
PNB_CARDHOLDER_PROMO_PAYNAMICS = '%s-paynamics' % ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO
PNB_CARDHOLDER_PROMO_MIGS = '%s-migs' % ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO
PNB_CARDHOLDER_PROMO_IPAY88 = '%s-ipay88' % ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO

BDO_CARDHOLDER_PROMO_PESOPAY = '%s-pesopay' % ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO
BDO_CARDHOLDER_PROMO_PAYNAMICS = '%s-paynamics' % ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO
BDO_CARDHOLDER_PROMO_MIGS = '%s-migs' % ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO
BDO_CARDHOLDER_PROMO_IPAY88 = '%s-ipay88' % ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO

GCASH_CARDHOLDER_PROMO_PESOPAY = '%s-pesopay' % ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO
GCASH_CARDHOLDER_PROMO_PAYNAMICS = '%s-paynamics' % ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO
GCASH_CARDHOLDER_PROMO_MIGS = '%s-migs' % ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO
GCASH_CARDHOLDER_PROMO_IPAY88 = '%s-ipay88' % ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO

METROBANK_CARDHOLDER_PROMO_PESOPAY = '%s-pesopay' % ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO
METROBANK_CARDHOLDER_PROMO_PAYNAMICS = '%s-paynamics' % ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO
METROBANK_CARDHOLDER_PROMO_MIGS = '%s-migs' % ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO
METROBANK_CARDHOLDER_PROMO_IPAY88 = '%s-ipay88' % ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO

CHINABANK_CARDHOLDER_PROMO_PESOPAY = '%s-pesopay' % ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO
CHINABANK_CARDHOLDER_PROMO_PAYNAMICS = '%s-paynamics' % ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO
CHINABANK_CARDHOLDER_PROMO_MIGS = '%s-migs' % ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO
CHINABANK_CARDHOLDER_PROMO_IPAY88 = '%s-ipay88' % ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO

UNIONBANK_CARDHOLDER_PROMO_PESOPAY = '%s-pesopay' % ReservationTransaction.allowed_payment_types.UNIONBANK_CARDHOLDER_PROMO
UNIONBANK_CARDHOLDER_PROMO_PAYNAMICS = '%s-paynamics' % ReservationTransaction.allowed_payment_types.UNIONBANK_CARDHOLDER_PROMO
UNIONBANK_CARDHOLDER_PROMO_MIGS = '%s-migs' % ReservationTransaction.allowed_payment_types.UNIONBANK_CARDHOLDER_PROMO
UNIONBANK_CARDHOLDER_PROMO_IPAY88 = '%s-ipay88' % ReservationTransaction.allowed_payment_types.UNIONBANK_CARDHOLDER_PROMO

ROBINSONSBANK_CARDHOLDER_PROMO_PESOPAY = '%s-pesopay' % ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO
ROBINSONSBANK_CARDHOLDER_PROMO_PAYNAMICS = '%s-paynamics' % ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO
ROBINSONSBANK_CARDHOLDER_PROMO_MIGS = '%s-migs' % ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO
ROBINSONSBANK_CARDHOLDER_PROMO_IPAY88 = '%s-ipay88' % ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO

PAYMENTTYPE_EQUIVALENTTO_PAYMENTOPTIONSIDENTIFIER = {
        ReservationTransaction.allowed_payment_types.CLIENT_INITIATED: 'pesopay',
        ReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT: 'paynamics',
        ReservationTransaction.allowed_payment_types.MIGS_PAYMENT: 'migs',
        ReservationTransaction.allowed_payment_types.IPAY88_PAYMENT: 'ipay88',
        ReservationTransaction.allowed_payment_types.PROMO_CODE: 'claim_code',
        ReservationTransaction.allowed_payment_types.MPASS: 'mpass',
        ReservationTransaction.allowed_payment_types.EPLUS: 'eplus',
        ReservationTransaction.allowed_payment_types.GCASH: 'gcash',
        ReservationTransaction.allowed_payment_types.GREWARDS: 'grewards',
        ReservationTransaction.allowed_payment_types.GCASH_APP: 'g_cash_app',
        ReservationTransaction.allowed_payment_types.RESERVE: 'cash',
        ReservationTransaction.allowed_payment_types.GLOBE_OPERATOR_BILLING: 'globe_operator_billing',
        ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO: 'bpi_cardholder_promo',
        ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO: 'citibank_cardholder_promo',
        ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO: 'pnb_cardholder_promo',
        ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO: 'bdo_cardholder_promo',
        ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO: 'g_cash_cardholder_promo',
        ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO: 'metrobank_cardholder_promo',
        ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO: 'chinabank_cardholder_promo',
        ReservationTransaction.allowed_payment_types.UNIONBANK_CARDHOLDER_PROMO: 'unionbank_cardholder_promo',
        ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO: 'robinsonsbank_cardholder_promo',
        BPI_CARDHOLDER_PROMO_PESOPAY: 'bpi_cardholder_promo_pesopay',
        BPI_CARDHOLDER_PROMO_PAYNAMICS: 'bpi_cardholder_promo_paynamics',
        BPI_CARDHOLDER_PROMO_MIGS: 'bpi_cardholder_promo_migs',
        BPI_CARDHOLDER_PROMO_IPAY88: 'bpi_cardholder_promo_ipay88',
        CITIBANK_CARDHOLDER_PROMO_PESOPAY: 'citibank_cardholder_promo_pesopay',
        CITIBANK_CARDHOLDER_PROMO_PAYNAMICS: 'citibank_cardholder_promo_paynamics',
        CITIBANK_CARDHOLDER_PROMO_MIGS: 'citibank_cardholder_promo_migs',
        CITIBANK_CARDHOLDER_PROMO_IPAY88: 'citibank_cardholder_promo_ipay88',
        PNB_CARDHOLDER_PROMO_PESOPAY: 'pnb_cardholder_promo_pesopay',
        PNB_CARDHOLDER_PROMO_PAYNAMICS: 'pnb_cardholder_promo_paynamics',
        PNB_CARDHOLDER_PROMO_MIGS: 'pnb_cardholder_promo_migs',
        PNB_CARDHOLDER_PROMO_IPAY88: 'pnb_cardholder_promo_ipay88',
        BDO_CARDHOLDER_PROMO_PESOPAY: 'bdo_cardholder_promo_pesopay',
        BDO_CARDHOLDER_PROMO_PAYNAMICS: 'bdo_cardholder_promo_paynamics',
        BDO_CARDHOLDER_PROMO_MIGS: 'bdo_cardholder_promo_migs',
        BDO_CARDHOLDER_PROMO_IPAY88: 'bdo_cardholder_promo_ipay88',
        GCASH_CARDHOLDER_PROMO_PESOPAY: 'g_cash_cardholder_promo_pesopay',
        GCASH_CARDHOLDER_PROMO_PAYNAMICS: 'g_cash_cardholder_promo_paynamics',
        GCASH_CARDHOLDER_PROMO_MIGS: 'g_cash_cardholder_promo_migs',
        GCASH_CARDHOLDER_PROMO_IPAY88: 'g_cash_cardholder_promo_ipay88',
        METROBANK_CARDHOLDER_PROMO_PESOPAY: 'metrobank_cardholder_promo_pesopay',
        METROBANK_CARDHOLDER_PROMO_PAYNAMICS: 'metrobank_cardholder_promo_paynamics',
        METROBANK_CARDHOLDER_PROMO_MIGS: 'metrobank_cardholder_promo_migs',
        METROBANK_CARDHOLDER_PROMO_IPAY88: 'metrobank_cardholder_promo_ipay88',
        CHINABANK_CARDHOLDER_PROMO_PESOPAY: 'chinabank_cardholder_promo_pesopay',
        CHINABANK_CARDHOLDER_PROMO_PAYNAMICS: 'chinabank_cardholder_promo_paynamics',
        CHINABANK_CARDHOLDER_PROMO_MIGS: 'chinabank_cardholder_promo_migs',
        CHINABANK_CARDHOLDER_PROMO_IPAY88: 'chinabank_cardholder_promo_ipay88',
        UNIONBANK_CARDHOLDER_PROMO_PESOPAY: 'unionbank_cardholder_promo_pesopay',
        UNIONBANK_CARDHOLDER_PROMO_PAYNAMICS: 'unionbank_cardholder_promo_paynamics',
        UNIONBANK_CARDHOLDER_PROMO_MIGS: 'unionbank_cardholder_promo_migs',
        UNIONBANK_CARDHOLDER_PROMO_IPAY88: 'unionbank_cardholder_promo_ipay88',
        ROBINSONSBANK_CARDHOLDER_PROMO_PESOPAY: 'robinsonsbank_cardholder_promo_pesopay',
        ROBINSONSBANK_CARDHOLDER_PROMO_PAYNAMICS: 'robinsonsbank_cardholder_promo_paynamics',
        ROBINSONSBANK_CARDHOLDER_PROMO_MIGS: 'robinsonsbank_cardholder_promo_migs',
        ROBINSONSBANK_CARDHOLDER_PROMO_IPAY88: 'robinsonsbank_cardholder_promo_ipay88',
}

def prepare_reservation(tx):
    log.debug("prepare_reservation, preparing transaction...")
    log.info("Preparing reservation: ID: {}; WORKSPACE: {}".format(tx.key.id(), tx.workspace))
    
    try:
        if 'is_raw' in tx.workspace and tx.workspace['is_raw']:
            reservations = tx.workspace['reservations_raw'][0]

            # for raw reservations, we need to substitute values for workspace keys we can't fulfill.
            tx.workspace['theaters.key'] = [None]
            tx.workspace['theaters.theater_cinema_name'] = ['XXXXXXXXXXX']
            tx.workspace['theaters.org_theater_code'] = reservations['theater_codes']
            tx.workspace['theaters.org_id'] = [str(AYALA_MALLS)]
            tx.workspace['schedules.movie'] = [None]
            tx.workspace['schedules.schedule_code'] = reservations['schedule_codes']
            tx.workspace['schedules.price'] = ['XX.XX']
            tx.workspace['schedules.popcorn_price'] = ['XX.XX']
            tx.workspace['seats'] = reservations['seats']
            tx.workspace['seats::imploded'] = ','.join(reservations['seats'])
            tx.workspace['is_bank_deposit'] = False
        else:
            theater_keys, cinemas, schedule_keys, times, time_strs, seats = zip(*[(r.theater, r.cinema,
                    r.schedule, r.time, str(r.time), r.seat_id) for r in tx.reservations])

            # So we don't spend two RPC calls to fetch all of the theater
            # and schedule entities, we join the two key lists and then do
            # a single batch fetch, splitting it up as necessary
            seats = list(set(seats))
            entity_keys = theater_keys + schedule_keys
            entities = non_transactional_get_multi(entity_keys)
            theaters = entities[:len(theater_keys)]
            schedules = entities[len(theater_keys):]
    
            log.debug("theater_name:  %s" % (theaters[0].name))

            payment_options_identifiers = _get_payment_options_identifiers(theaters[0], tx.platform)

            log.debug("payment_options_identifiers: %s" % (payment_options_identifiers) )

            if 'gcash' == tx.payment_type:
                if ('application' in tx.payment_info) and ('gcash' == tx.payment_info['application']):
                    log.debug("prepare_reservation, transaction is gcash from gcash aplication.")
                    tx.payment_type = ReservationTransaction.allowed_payment_types.GCASH_APP
            
            if 'grewards' == tx.payment_type:
                if ('application' in tx.payment_info) and ('grewards' == tx.payment_info['application']):
                    log.debug("prepare_reservation, transaction is grewards from gmovies microsite for ayala.")
                    tx.payment_type = ReservationTransaction.allowed_payment_types.GREWARDS

            theater_name = str(theaters[0].name).encode('ascii', 'ignore').decode('ascii')

            if 'robinsons' in theater_name.lower():
                if tx.payment_type == 'robinsonsbank-cardholder-promo':
                    theaters[0].payment_gateway = 'paynamics'

            is_payment_option_enabled = payment_options_handler(tx.payment_type, theaters[0].payment_gateway, payment_options_identifiers)
            
            log.debug("is_payment_option_enabled: %s" % (is_payment_option_enabled))
            log.debug("%s, %s, %s" % (tx.payment_type, theaters[0].payment_gateway, payment_options_identifiers))

            if not theaters[0].allow_reservation:
                log.warn("ERROR!, prepare_reservation, not allow_reservation...")

                tx.error_info['error_msg'] = "not allow_reservation"
                return 'error', DEFAULT_ERROR_MESSAGE

            if tx.platform not in theaters[0].allow_reservation_platform:
                log.warn("ERROR!, prepare_reservation, platform not in allow_reservation_platform...")

                tx.error_info['error_msg'] = "platform not in allow_reservation_platform"
                return 'error', DEFAULT_ERROR_MESSAGE

            if not is_payment_option_enabled:
                log.warn("ERROR!, prepare_reservation, payment_type not in payment_options...")

                tx.error_info['error_msg'] = "payment_type not in payment_options"
                return 'error', DEFAULT_ERROR_MESSAGE

            convenience_fee = '0.00' # default convenience fee
            slots = [s.get_timeslot(t) for (s, t) in zip(schedules, times)]
            movie_keys = [s.movie for s in slots]
            movies = non_transactional_get_multi(movie_keys)
            theater_cinemas = zip(theaters, cinemas)
            schedule_times = zip(schedules, times)
            theaterorg_id = [t.key.parent().id() for t in theaters][0]

            # get convenience fee for credit-card/gcash transactions.
            if tx.payment_type in [ReservationTransaction.allowed_payment_types.CLIENT_INITIATED,
                    ReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT,
                    ReservationTransaction.allowed_payment_types.MIGS_PAYMENT,
                    ReservationTransaction.allowed_payment_types.IPAY88_PAYMENT,
                    ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO,
                    ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO,
                    ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO,
                    ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO,
                    ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO,
                    ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO,
                    ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO,
                    ReservationTransaction.allowed_payment_types.UNIONBANK_CARDHOLDER_PROMO,
                    ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO]:
                convenience_fee = _get_convenience_fee_settings(theaters[0].key.parent(), 'theaterorgs', 'credit_card')
            elif tx.payment_type in [ReservationTransaction.allowed_payment_types.GCASH,
                    ReservationTransaction.allowed_payment_types.GCASH_APP]:
                convenience_fee = _get_convenience_fee_settings(theaters[0].key.parent(), 'theaterorgs', 'gcash')
            elif tx.payment_type in [ReservationTransaction.allowed_payment_types.GREWARDS,
                    ReservationTransaction.allowed_payment_types.GREWARDS]:
                convenience_fee = _get_convenience_fee_settings(theaters[0].key.parent(), 'theaterorgs', 'gcash')
            elif tx.payment_type in [ReservationTransaction.allowed_payment_types.PROMO_CODE, 
                    ReservationTransaction.allowed_payment_types.RESERVE]:
                convenience_fee = _get_convenience_fee_settings(theaters[0].key.parent(), 'theaterorgs', 'bank_payment')
            log.info("Convenience_fee: {}".format(convenience_fee))
            tx.workspace['schedules.slot'] = slots
            tx.workspace['theaters.key'] = theater_keys
            tx.workspace['theaters.org_id'] = [t.key.parent().id() for t in theaters]
            tx.workspace['theaters.org_theater_code'] = [t.org_theater_code for t in theaters]
            tx.workspace['theaters.theater_cinema_name'] = ['%s - Cinema %s' % (t.name, c) if len(c) <= 2 else '%s - %s' % (t.name, c) for t, c in theater_cinemas]
            tx.workspace['theaters.theater_name'] = theaters[0].name
            tx.workspace['theaters.cinemas.cinema_name'] = cinemas[0]
            tx.workspace['theaters.is_multiple_tickets'] = theaters[0].is_multiple_tickets
            tx.workspace['theaters.allow_discount_code'] = theaters[0].allow_discount_code
            tx.workspace['theaters.payment_options_identifiers'] = payment_options_identifiers
            tx.workspace['theaters.payment_options_settings'] = _get_payment_options_settings(tx.payment_type, theaters[0].payment_gateway)
            tx.workspace['theaters.payment_gateway'] = theaters[0].payment_gateway
            tx.workspace['schedules.movie'] = movies
            tx.workspace['schedules.schedule_code'] = [sl.feed_code for sl in slots]
            tx.workspace['schedules.show_datetime'] = [datetime.combine(s.show_date, time) for s, time in schedule_times]
            tx.workspace['schedules.price'] = [sl.price for sl in slots]
            tx.workspace['schedules.popcorn_price'] = [sl.popcorn_price for sl in slots]
            tx.workspace['schedules.variant'] = [sl.variant for sl in slots]
            tx.workspace['seating_type'] = seating_type = [sl.seating_type for sl in slots][0]
            tx.workspace['convenience_fee'] = convenience_fee
            tx.workspace['discount::seats_counter'] = 0
            tx.workspace['discount::is_discounted'] = False
            tx.workspace['refund::is_for_refund'] = False
            tx.workspace['is_bank_deposit'] = False
            
            if theaterorg_id == str(AYALA_MALLS):
                log.debug("prepare_reservation, Ayala Malls...")
                log.info("Ticket: {}".format(tx))
                tx.workspace['seats'] = seats
                tx.workspace['seats::imploded'] = ','.join(seats)
            elif theaterorg_id == str(ROCKWELL_MALLS):
                log.debug("prepare_reservation, Rockwell (Power Plant Mall)...")

                cinema = get_cinema_entity(theaters[0], cinemas[0])
                seat_map = cinema.feed_seat_map
                seat_ids = [s['seat_id'] for sm in seat_map for s in sm if s['seat_name'] in seats]
                seat_label_id_mapping = {s['seat_id']: s['seat_name'] for sm in seat_map for s in sm if s['seat_name'] in seats}

                tx.workspace['movie_schedule_id'] = int(slots[0].feed_code_schedule)
                tx.workspace['showtime_id'] = int(slots[0].feed_code)
                tx.workspace['seats'] = seats
                tx.workspace['seats::imploded'] = ','.join(seats)
                tx.workspace['seat_ids'] = seat_ids
                tx.workspace['seat_label_id_mapping'] = seat_label_id_mapping
                tx.workspace['seats::quantity'] = tx.reservation_count()
            elif theaterorg_id == str(GREENHILLS_MALLS):
                log.debug("prepare_reservation, Greenhills Malls...")

                cinema = get_cinema_entity(theaters[0], cinemas[0])
                seat_map = cinema.feed_seat_map
                seat_ids = [s['seat_id'] for sm in seat_map for s in sm if s['seat_name'] in seats]
                seat_label_id_mapping = {s['seat_id']:s['seat_name']  for sm in seat_map for s in sm if s['seat_name'] in seats}

                tx.workspace['movie_schedule_id'] = int(slots[0].feed_code_schedule)
                tx.workspace['showtime_id'] = int(slots[0].feed_code)
                tx.workspace['seats'] = seats
                tx.workspace['seats::imploded'] = ','.join(seats)
                tx.workspace['seat_ids'] = seat_ids
                tx.workspace['seat_label_id_mapping'] = seat_label_id_mapping
                tx.workspace['seats::quantity'] = tx.reservation_count()

                if seating_type in ['Free Seating', 'Guaranteed Seats']:
                    seats_imploded = []

                    for i in range(1, tx.reservation_count()+1):
                        seats_imploded.append(str(i))

                    tx.workspace['seats::imploded'] = ','.join(seats_imploded)
            elif theaterorg_id == str(CINEMA76_MALLS):
                log.debug("prepare_reservation, Cinema 76 Malls...")

                cinema = get_cinema_entity(theaters[0], cinemas[0])
                seat_map = cinema.feed_seat_map
                seat_label_id_mapping = {s['seat_id']:s['seat_name']  for sm in seat_map for s in sm if s['seat_name'] in seats}

                tx.workspace['movie_schedule_id'] = int(slots[0].feed_code_schedule)
                tx.workspace['showtime_id'] = int(slots[0].feed_code)
                tx.workspace['seat_label_id_mapping'] = seat_label_id_mapping
                tx.workspace['seats::quantity'] = tx.reservation_count()

                if seating_type in ['Free Seating', 'Guaranteed Seats']:
                    seats = [str(i) for i in range(tx.reservation_count())]
                    seat_ids = [' ' for i in range(tx.reservation_count())]
                    seats_imploded = []
                else:
                    log.debug("prepare_reservation, Cinema 76 Malls only supports 'Free / Guaranteed Seating', seating_type: %s..." % (seating_type))
                    return 'error', DEFAULT_ERROR_MESSAGE
                tx.workspace['seats'] = seats
                tx.workspace['seat_ids'] = seat_ids
                tx.workspace['seats::imploded'] = seating_type
            elif theaterorg_id == str(GLOBE_EVENTS):
                log.debug("prepare_reservation, Globe Events...")

                cinema = get_cinema_entity(theaters[0], cinemas[0])
                seat_map = cinema.feed_seat_map
                seat_label_id_mapping = {s['seat_id']:s['seat_name']  for sm in seat_map for s in sm if s['seat_name'] in seats}

                tx.workspace['movie_schedule_id'] = int(slots[0].feed_code_schedule)
                tx.workspace['showtime_id'] = int(slots[0].feed_code)
                tx.workspace['seat_label_id_mapping'] = seat_label_id_mapping
                tx.workspace['seats::quantity'] = tx.reservation_count()

                if seating_type in ['Free Seating', 'Guaranteed Seats']:
                    seats = [str(i) for i in range(tx.reservation_count())]
                    seat_ids = [' ' for i in range(tx.reservation_count())]
                    seats_imploded = []
                else:
                    log.debug("prepare_reservation, Globe Events only supports 'Free / Guaranteed Seating', seating_type: %s..." % (seating_type))
                    return 'error', DEFAULT_ERROR_MESSAGE
                tx.workspace['seats'] = seats
                tx.workspace['seat_ids'] = seat_ids
                tx.workspace['seats::imploded'] = seating_type
            elif theaterorg_id in [str(SM_MALLS), str(ROBINSONS_MALLS)]:
                log_mall = 'SM Malls' if theaterorg_id == str(SM_MALLS) else 'Robinsons Malls'
                log.debug("prepare_reservation, %s..." % (log_mall))

                seat_ids = []
                cinema = get_cinema_entity(theaters[0], cinemas[0])
                tx.workspace['showtime_id'] = int(slots[0].feed_code)
                tx.workspace['mgi::branch_key'] = int(theaters[0].branch_id)
                tx.workspace['mgi::schedule_key'] = int(slots[0].feed_code)

                if seating_type in ['Free Seating', 'Guaranteed Seats']:
                    log.info("%s, Free Seating or Guaranteed Seats..." % (log_mall))

                    seats = [str(i) for i in range(tx.reservation_count())]
                    seat_ids = [' ' for i in range(tx.reservation_count())]

                    tx.workspace['seats::imploded'] = seating_type
                else:
                    log.info("%s, Reserved Seating..." % (log_mall))

                    seat_label_id_mapping = {}
                    seatmap_details = cinema.feed_seat_map

                    for row in seatmap_details:
                        for seat in row:
                            if seat['seat_name'] in seats:
                                seat_label_id_mapping[seat['seat_id']] = seat['seat_name']
                                seat_ids.append(seat['seat_id'])

                    tx.workspace['seats::imploded'] = ','.join(seats)
                    tx.workspace['seat_label_id_mapping'] = seat_label_id_mapping

                tx.workspace['seats'] = seats
                tx.workspace['seat_ids'] = seat_ids
            elif theaterorg_id == str(MEGAWORLD_MALLS):
                log.debug("prepare_reservation, Megaworld Malls...")

                if tx.workspace['theaters.org_theater_code'][0] in ALLOWED_MGI_RESERVATION_BRANCH_CODES:
                    seat_ids = []
                    cinema = get_cinema_entity(theaters[0], cinemas[0])
                    tx.workspace['showtime_id'] = int(slots[0].feed_code)
                    tx.workspace['mgi::branch_key'] = int(theaters[0].branch_id)
                    tx.workspace['mgi::schedule_key'] = int(slots[0].feed_code)

                    if seating_type in ['Free Seating', 'Guaranteed Seats']:
                        log.debug("Megaworld (%s), Free Seating or Guaranteed Seats..." % tx.workspace['theaters.org_theater_code'][0])

                        for seat in range(tx.reservation_count()):
                            seat_ids.append(' ')

                        tx.workspace['seats::imploded'] = seating_type
                    else:
                        log.debug("Megaworld (%s), Reserved Seating..." % tx.workspace['theaters.org_theater_code'][0])

                        seat_label_id_mapping = {}
                        seatmap_details = cinema.feed_seat_map

                        for row in seatmap_details:
                            for seat in row:
                                if seat['seat_name'] in seats:
                                    seat_label_id_mapping[seat['seat_id']] = seat['seat_name']
                                    seat_ids.append(seat['seat_id'])

                        tx.workspace['seats::imploded'] = ','.join(seats)
                        tx.workspace['seat_label_id_mapping'] = seat_label_id_mapping

                    tx.workspace['seats'] = seats
                    tx.workspace['seat_ids'] = seat_ids
                else:
                    log.warn("ERROR!, prepare_reservation, reservation is not supported...")

                    tx.error_info['error_msg'] = "reservation is not supported"
                    return 'error', DEFAULT_ERROR_MESSAGE
            else:
                log.warn("ERROR!, prepare_reservation, reservation is not supported...")

                tx.error_info['error_msg'] = "reservation is not supported"
                return 'error', DEFAULT_ERROR_MESSAGE

        tx.put()

        return 'success', None
    except Exception, e:
        log.warn("ERROR!, prepare_reservation...")
        log.error(traceback.format_exc(e))

    tx.error_info['error_msg'] = str(e)
    return 'error', DEFAULT_ERROR_MESSAGE

def cancel_reservation(tx):
    pass

def get_cinema_entity(theater, cinema_name):
    cinemas = theater.cinemas

    for c in cinemas:
        if cinema_name == c.name:

            return c

def convert_date(d):
    return datetime.strptime(d, '%m/%d/%Y %I:%M:%S %p')

def convert_date2(d):
    return datetime.strptime(d, '%Y-%m-%d')

@non_transactional
def non_transactional_get_multi(keys):
    return get_multi(keys)

@non_transactional
def payment_options_handler(payment_type, payment_gateway, payment_options_identifiers):
    is_enabled = False
    payment_options_identifier = None

    if payment_type in [ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.UNIONBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO]:
        payment_type = '%s-%s' % (payment_type, payment_gateway) if payment_gateway else payment_type

        log.debug("payment_options_handler , payment_type: %s" % (payment_type))

    if payment_type in PAYMENTTYPE_EQUIVALENTTO_PAYMENTOPTIONSIDENTIFIER:
        payment_options_identifier = PAYMENTTYPE_EQUIVALENTTO_PAYMENTOPTIONSIDENTIFIER[payment_type]

        log.debug("payment_options_handler , payment_options_identifier: %s" % (payment_options_identifier))

    if payment_options_identifier in payment_options_identifiers:
        is_enabled = True

    log.debug("payment_options_handler , is_enabled: %s" % (is_enabled))

    return is_enabled

@non_transactional
def _get_payment_options_identifiers(theater, platform):
    payment_options_identifiers = []

    if platform == 'ios':
        payment_options_identifiers = theater.payment_options
    elif platform == 'android':
        payment_options_identifiers = theater.payment_options_android
    elif platform == 'website':
        payment_options_identifiers = theater.payment_options_website

    return payment_options_identifiers

@non_transactional
def _get_payment_options_settings(payment_type, payment_gateway=None):
    payment_options_identifier = None
    payment_options_settings = None

    if payment_type in [ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.UNIONBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO]:
        payment_type = '%s-%s' % (payment_type, payment_gateway) if payment_gateway else payment_type

    if payment_type in PAYMENTTYPE_EQUIVALENTTO_PAYMENTOPTIONSIDENTIFIER:
        payment_options_identifier = PAYMENTTYPE_EQUIVALENTTO_PAYMENTOPTIONSIDENTIFIER[payment_type]

    if payment_options_identifier:
        payment_options_settings_key = Key(PaymentOptionSettings, payment_options_identifier)
        payment_options_settings = payment_options_settings_key.get()

    return payment_options_settings

@non_transactional
def _get_convenience_fee_settings(parent_key, entity_class, payment_option):
    settings = ConvenienceFeeSettings.get_settings(parent_key, entity_class)

    if payment_option in settings.convenience_fees:
        if settings.convenience_fees[payment_option]:
            convenience_fee = settings.convenience_fees[payment_option]
            log.info('%s, get convenience fee %s' % (payment_option, convenience_fee))

            return convenience_fee

    if payment_option == 'credit_card':
        convenience_fee = '20.00'
        log.info('Credit Card, get default convenience fee %s' % convenience_fee)
    else:
        convenience_fee = '0.00'
        log.info('Get default convenience fee %s' % convenience_fee)

    return convenience_fee

@non_transactional
def map_seats_label_to_tx(seat_label_id, seat_id_tx):
    log.debug("map_seats_label_to_tx...")

    return [(idl, idx['transactionCode']) for idi, idl in seat_label_id.iteritems() for idx in seat_id_tx if idi == idx['seatId']]

def do_reservation(tx):
    def __send_reservation_req(s, req):
        log.info("######################### START ############################")
        log.info(req.method)
        log.info(req.url)
        log.info("########################## END #############################")

        r = s.send(req, timeout=TIMEOUT_DEADLINE)

        return r.status_code, r.text

    if 'is_raw' in tx.workspace and tx.workspace['is_raw']:
        merchant_code = tx.workspace['theaters.org_theater_code'][0]
        schedule_code = tx.workspace['schedules.schedule_code'][0]
        seats = tx.workspace['seats::imploded']
    else:
        merchant_codes = tx.workspace['theaters.org_theater_code']
        slots = tx.workspace['schedules.slot']
        merchant_code = merchant_codes[0]
        schedule_code = slots[0].feed_code
        seats = tx.workspace['seats::imploded']

    rsvp_payload = {'src': SOURCE_ID, 'action': RESERVATION_ACTION, 'code': merchant_code,
            'sid': schedule_code, 'seats': seats, 'type': BUY_TYPE}
    theaterorg_id = tx.workspace['theaters.org_id'][0]
    theaterorg_key = Key(TheaterOrganization, theaterorg_id)

    if tx.payment_type in [ReservationTransaction.allowed_payment_types.MPASS,
            ReservationTransaction.allowed_payment_types.PROMO_CODE]:
        rsvp_payload['id'] = tx.workspace['MPASS:id']
        rsvp_payload['ptype'] = 'M-Pass'
        rsvp_payload['email'] = 'xxxxxxxxx@xxxxxxxxxxx.xx'
        tx.workspace['RSVP:fee'] = '0.00' # XXX: Hard-coded per-seat reservation fee
        
        if 'convenience_fee' in tx.workspace:
            convenience_fee = tx.workspace['convenience_fee']
        else:
            convenience_fee = _get_convenience_fee_settings(theaterorg_key, 'theaterorgs', 'bank_payment')
        tx.workspace['RSVP:fee'] = convenience_fee    
    elif tx.payment_type in [ReservationTransaction.allowed_payment_types.GCASH,
            ReservationTransaction.allowed_payment_types.GCASH_APP]:

        if 'convenience_fee' in tx.workspace:
            convenience_fee = tx.workspace['convenience_fee']
        else:
            convenience_fee = _get_convenience_fee_settings(theaterorg_key, 'theaterorgs', 'gcash')

        tx.workspace['RSVP:movie']       = tx.workspace['schedules.movie'][0].canonical_title
        tx.workspace['RSVP:seats']       = [res.seat_id for res in tx.reservations]
        tx.workspace['RSVP:fee']         = convenience_fee
        tx.workspace['RSVP:totalamount'] = str(tx.total_amount(tx.workspace['RSVP:fee'], True))

        if tx.workspace['discount::is_discounted']:
                log.debug("do_reservation, SUCCESS, Ayala Malls, transaction is discounted...")

                tx.workspace['RSVP:discounttype'] = tx.workspace['discount::discount_type']
                tx.workspace['RSVP:totalamount'] = tx.workspace['discount::total_amount']
                tx.workspace['RSVP:totaldiscount'] = tx.workspace['discount::total_discount']
        tx.put()
        return 'success', None
    elif tx.payment_type in [ReservationTransaction.allowed_payment_types.GREWARDS]:

        if 'convenience_fee' in tx.workspace:
            convenience_fee = tx.workspace['convenience_fee']
        else:
            convenience_fee = _get_convenience_fee_settings(theaterorg_key, 'theaterorgs', 'gcash')

        tx.workspace['RSVP:movie']       = tx.workspace['schedules.movie'][0].canonical_title
        tx.workspace['RSVP:seats']       = [res.seat_id for res in tx.reservations]
        tx.workspace['RSVP:fee']         = convenience_fee
        tx.workspace['RSVP:totalamount'] = str(tx.total_amount(tx.workspace['RSVP:fee'], True))

        if tx.workspace['discount::is_discounted']:
                log.debug("do_reservation, SUCCESS, Ayala Malls, transaction is discounted...")

                tx.workspace['RSVP:discounttype'] = tx.workspace['discount::discount_type']
                tx.workspace['RSVP:totalamount'] = tx.workspace['discount::total_amount']
                tx.workspace['RSVP:totaldiscount'] = tx.workspace['discount::total_discount']
        tx.put()
        return 'success', None
    elif tx.payment_type in [ReservationTransaction.allowed_payment_types.CREDIT_CARD,
            ReservationTransaction.allowed_payment_types.CLIENT_INITIATED,
            ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.UNIONBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO]:
        if 'convenience_fee' in tx.workspace:
            convenience_fee = tx.workspace['convenience_fee']
        else:
            convenience_fee = _get_convenience_fee_settings(theaterorg_key, 'theaterorgs', 'credit_card')

        rsvp_payload['ptype'] = 'Credit Card'
        rsvp_payload['email'] = tx.payment_info['email']
        tx.workspace['RSVP:fee'] = convenience_fee

    elif tx.payment_type in [ReservationTransaction.allowed_payment_types.RESERVE]:
        log.debug("do_reservation, payment type RESERVE...")
        rsvp_payload = {'src': SOURCE_ID, 'action': RESERVATION_ACTION, 'code': merchant_code,
            'sid': schedule_code, 'seats': seats, 'type': RESERVATION_TYPE}
            
        if 'convenience_fee' in tx.workspace:
            convenience_fee = tx.workspace['convenience_fee']
        else:
            convenience_fee = _get_convenience_fee_settings(theaterorg_key, 'theaterorgs', 'bank_payment')
        tx.workspace['RSVP:fee'] = convenience_fee
        
        # rsvp_payload['id'] = tx.workspace['MPASS:id']
        # rsvp_payload['email'] = 'xxxxxxxxx@xxxxxxxxxxx.xx'
        rsvp_payload['email'] = tx.payment_info['email']

    log.debug("do_reservation, sent request with payload: %s..." % rsvp_payload)

    s = requests.Session()
    req = create_sureseats_request(SURESEATS_ENDPOINT_PRIMARY, rsvp_payload)
    r_status_code, r_text = call_once(__send_reservation_req, tx, s, req)

    log.debug("do_reservation, got response: %s..." % str(r_text))

    if r_status_code != 200:
        log.warn("ERROR!, do_reservation, cannot perform reservation, there was an error calling SureSeats...")

        tx.error_info['error_msg'] = r_text
        return 'error', DEFAULT_ERROR_MESSAGE

    try:
        xml = etree.fromstring(str(r_text))
        status = xml.xpath('//Transaction/Status/text()')[0]

        if status.upper() == 'SUCCESS':
            log.debug("do_reservation, status equal to SUCCESS...")

            tx.reservation_reference = xml.xpath('//Transaction/RefNo/text()')[0]
            tx.workspace['RSVP:claimcode'] = xml.xpath('//Transaction/ClaimCode/text()')[0]
            tx.workspace['RSVP:claimdate'] = convert_date(xml.xpath('//Transaction/ClaimDate/text()')[0])
            tx.workspace['RSVP:totalamount'] = xml.xpath('//Transaction/TotalAmount/text()')[0]
            
            if 'RSVP:fee' in tx.workspace and tx.workspace['RSVP:fee']:
                tx.workspace['RSVP:totalamount'] = str(tx.total_amount(tx.workspace['RSVP:fee'], True))
            tx.workspace['RSVP:movie'] = xml.xpath('//Transaction/Movie/text()')[0]
            tx.workspace['RSVP:seats'] = [res.seat_id for res in tx.reservations]
            tx.bind_cancellation_callback(cancel_reservation)

            if tx.workspace['discount::is_discounted']:
                log.debug("do_reservation, SUCCESS, Ayala Malls, transaction is discounted...")

                tx.workspace['RSVP:discounttype'] = tx.workspace['discount::discount_type']
                tx.workspace['RSVP:totalamount'] = tx.workspace['discount::total_amount']
                tx.workspace['RSVP:totaldiscount'] = tx.workspace['discount::total_discount']

            tx.put()

            return 'success', None
        elif status.upper() == 'PENDING':
            log.debug("do_reservation, status equal to PENDING...")

            # credit card transaction (ugh, ugly hack)
            tx.reservation_reference = xml.xpath('//Transaction/RefNo/text()')[0]
            tx.workspace['RSVP:totalamount'] = xml.xpath('//Transaction/TotalAmount/text()')[0]
            if 'is_bank_deposit' in tx.workspace and tx.workspace['is_bank_deposit']:
                if 'RSVP:fee' in tx.workspace and tx.workspace['RSVP:fee']:
                    tx.workspace['RSVP:totalamount'] = str(tx.total_amount(tx.workspace['RSVP:fee'], True))
            tx.workspace['RSVP:customerid'] = xml.xpath('//Transaction/CustID/text()')[0]
            tx.workspace['RSVP:movie'] = xml.xpath('//Transaction/Movie/text()')[0]
            tx.bind_cancellation_callback(cancel_reservation)

            if tx.workspace['discount::is_discounted']:
                log.debug("do_reservation, PENDING, Ayala Malls, transaction is discounted...")

                tx.workspace['RSVP:discounttype'] = tx.workspace['discount::discount_type']
                tx.workspace['RSVP:totalamount'] = tx.workspace['discount::total_amount']
                tx.workspace['RSVP:totaldiscount'] = tx.workspace['discount::total_discount']

            tx.put()

            return 'success', None
        else:
            log.warn("ERROR!, do_reservation, status not equal to SUCCESS or PENDING...")
            log.warn("status: %s..." % status)

            namespace = 'ayala-malls'
            response_code = 'FAILED' # default response code to translate the error messages.
            response_message = status

            if status == '1 or more seats have been reserved by another user. Please try again and select other seats.':
                response_code = 'SEATSTAKEN'
            elif status == 'You do not have enough credits in your M-Pass card':
                response_code = 'INSUFFICIENTMPASSCREDITS'
            elif status in ['Could not find movie screening. Select another time.', 'Movie schedule not found. Please try a different schedule.']:
                response_code = 'SCHEDULENOTFOUND'
                # send email to GMovies support to remove schedule
                send_email_support(tx, status)

            tx.error_info['error_msg'] = status
            return 'error', translate_message_via_error_code(namespace, response_code, response_message)
    except etree.XMLSyntaxError, e:
        log.warn("ERROR!, do_reservation, XMLSyntaxError...")
        log.error(e)
        tx.error_info['error_msg'] = str(e)
    except Exception, e:
        log.warn("ERROR!, do_reservation...")
        log.error(e)
        # send email to GMovies support if exception errors are encountered
        send_email_support(tx, "ERROR in do_reservation: " + str(e))
        tx.error_info['error_msg'] = str(e)

    return 'error', DEFAULT_ERROR_MESSAGE

def do_reservation_powerplantmall(tx):
    def __send_reservation_req(s, req):
        r = s.send(req, timeout=TIMEOUT_DEADLINE)

        return r.status_code, r.text

    rsvp_payload = {}
    payment_origin = "MOBILE" # set to MOBILE as default.
    theaterorg_id = tx.workspace['theaters.org_id'][0]
    theaterorg_key = Key(TheaterOrganization, theaterorg_id)

    if tx.payment_type in [ReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT,
            ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.UNIONBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO]:
        log.debug("do_reservation_powerplantmall, Credit Card Payment (Paynamics)...")

        payment_type = 'CC'

        if tx.is_discounted and tx.discount_info and 'discount_value' in tx.discount_info and 'discount_type' in tx.discount_info:
            discount_value_coretool = tx.discount_info['discount_value']
            discount_type_coretool = tx.discount_info['discount_type']

            if tx.discount_info['discount_type'] in ['amount','convenience_fee']:
                discount_type_coretool = 'bynetamount'
            elif tx.discount_info['discount_type'] == 'percent':
                discount_type_coretool = 'bypercentage'

            if discount_value_coretool and discount_type_coretool in ['bynetamount', 'bypercentage']:
                payment_type = 'CCG'
                rsvp_payload['promoValue'] = discount_value_coretool
                rsvp_payload['promoType'] = discount_type_coretool
            else:
                log.warn("ERROR!, do_reservation_powerplantmall, cannot process the discounts of claim code coretool...")

                tx.error_info['error_msg'] = "cannot process the discounts of claim code coretool: %s - %s" % (discount_type_coretool, discount_value_coretool)
                return 'error', DEFAULT_ERROR_MESSAGE

        rsvp_payload['lastName'] = tx.payment_info['lname']
        rsvp_payload['firstName'] = tx.payment_info['fname']
        rsvp_payload['emailAddress'] = tx.payment_info['email']
        rsvp_payload['mobileNumber'] = tx.payment_info['mobile']
    elif tx.payment_type in [ReservationTransaction.allowed_payment_types.GCASH,
            ReservationTransaction.allowed_payment_types.GCASH_APP]:
        log.debug("do_reservation_powerplantmall, GCash Payment...")

        payment_type = 'GCASH'
        full_name = tx.payment_info['full_name'].split(' ')
        last_name = full_name.pop(-1)
        first_name = ' '.join(full_name)

        rsvp_payload['lastName'] = last_name
        rsvp_payload['firstName'] = first_name
        rsvp_payload['emailAddress'] = tx.payment_info['email']
        rsvp_payload['mobileNumber'] = tx.payment_info['mobile']
    elif tx.payment_type in [ReservationTransaction.allowed_payment_types.PROMO_CODE]:
        log.debug("do_reservation_powerplantmall, PromoCode Payment...")

        payment_type = 'CLAIM_CODE'
        last_name = 'Promo Code'
        first_name = 'GMovies'

        if 'full_name' in tx.payment_info:
            full_name = tx.payment_info['full_name'].split(' ')
            last_name = full_name.pop(-1)
            first_name = ' '.join(full_name)

        rsvp_payload['lastName'] = last_name
        rsvp_payload['firstName'] = first_name
        rsvp_payload['emailAddress'] = tx.payment_info['email'] if 'email' in tx.payment_info else ''
        rsvp_payload['mobileNumber'] = tx.payment_info['mobile'] if 'mobile' in tx.payment_info else ''
    else:
        log.warn("ERROR!, do_reservation_powerplantmall, payment_type is not supported...")

        tx.error_info['error_msg'] = "payment_type is not supported"
        return 'error', DEFAULT_ERROR_MESSAGE

    rsvp_payload['status'] = 2
    rsvp_payload['movieScheduleId'] = tx.workspace['movie_schedule_id']
    rsvp_payload['showtimeId'] = tx.workspace['showtime_id']
    rsvp_payload['seatIds'] = tx.workspace['seat_ids']
    rsvp_payload['paymentType'] = payment_type
    rsvp_payload['origin'] = payment_origin

    tx.workspace['payment_type'] = payment_type
    tx.workspace['payment_origin'] = payment_origin

    try:
        log.debug("do_reservation_powerplantmall, lock-seats request with payload...")
        log.info(rsvp_payload)
        log.debug("do_reservation_powerplantmall, sending lock-seats request...")

        s = requests.Session()
        req = create_rockwell_request(ROCKWELL_ENDPOINT_RESERVE_SEATS, rsvp_payload)
        r_status_code, r_json = call_once(__send_reservation_req, tx, s, req)
        d_json = json.loads(r_json)

        log.debug("do_reservation_powerplantmall, lock-seats response, status_code: %s..." % str(r_status_code))
        log.info(r_json.encode('utf-8'))

        if r_status_code != 200:
            log.warn("ERROR!, do_reservation_powerplantmall, status_code not equal to 200...")

            if 'errorCode' in d_json:
                log.warn("ERROR!, do_reservation_powerplantmall, existing key errorCode in json response...")

                namespace = 'power-plant-mall'
                response_code = d_json['errorCode']
                response_message = d_json['errorMessage'] if 'errorMessage' in d_json else ''

                tx.error_info['error_msg'] = response_message
                return 'error', translate_message_via_error_code(namespace, response_code, response_message)

            tx.error_info['error_msg'] = r_json
            return 'error', DEFAULT_ERROR_MESSAGE

        tx.workspace['decoded_reservation_reference'] = d_json['response']['transactionCode']
        tx.workspace['RESERVATION_ID'] = d_json['response']['id']
        tx.workspace['RSVP:claimcode'] = d_json['response']['transactionCode']
        tx.workspace['RSVP:claimdate'] = convert_date2(d_json['response']['movieSchedule']['showingDate'])
        tx.workspace['RSVP:movie'] = tx.workspace['schedules.movie'][0].canonical_title
        tx.workspace['RSVP:seats'] = [res.seat_id for res in tx.reservations]
        tx.workspace['RSVP:fee'] = tx.workspace['convenience_fee']

        total_amount = str(tx.total_amount(tx.workspace['RSVP:fee'], True))
        total_seat_price = str(tx.total_seat_price(tx.workspace['RSVP:fee'], True))
        rf = tx.workspace['theaters.org_theater_code'][0] + '~' + d_json['response']['transactionCode']
        tx.reservation_reference = rf

        tx.workspace['RSVP:originalseatprice'] = total_seat_price
        tx.workspace['RSVP:originaltotalamount'] = total_amount

        # process for discount codes.
        if tx.workspace['discount::is_discounted']:
            log.debug("do_reservation_powerplantmall, Rockwell (Power Plant Mall), transaction is discounted...")

            tx.workspace['RSVP:discounttype'] = tx.workspace['discount::discount_type']
            tx.workspace['RSVP:totaldiscount'] = tx.workspace['discount::total_discount']
            tx.workspace['RSVP:totalamount'] = tx.workspace['discount::total_amount']
            tx.workspace['RSVP:totalseatprice'] = total_seat_price
        else:
            log.info("do_reservation_powerplantmall, Rockwell (Power Plant Mall), transaction is not discounted...")

            tx.workspace['RSVP:totalseatprice'] = total_seat_price
            tx.workspace['RSVP:totalamount'] = total_amount

        log.debug("do_reservation_powerplantmall, reservation_reference: %s..." % tx.reservation_reference)

        tx.bind_cancellation_callback(cancel_reservation)
        tx.put()
    except Exception, e:
        log.warn("ERROR!, do_reservation_powerplantmall...")
        log.error(e)
        # send email to GMovies support if exception errors are encountered
        send_email_support(tx, "ERROR in do_reservation: " + str(e))

        tx.error_info['error_msg'] = str(e)
        return 'error', DEFAULT_ERROR_MESSAGE

    return 'success', None

def do_reservation_greenhills(tx):
    def __send_reservation_req(s, req):
        r = s.send(req, timeout=TIMEOUT_DEADLINE)

        return r.status_code, r.text

    rsvp_payload = {}
    payment_origin = "MOBILE" # set to MOBILE as default.
    theaterorg_id = tx.workspace['theaters.org_id'][0]
    theaterorg_key = Key(TheaterOrganization, theaterorg_id)

    if tx.payment_type in [ReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT,
            ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.UNIONBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO]:
        log.debug("do_reservation_greenhills, Credit Card Payment (Paynamics)...")

        payment_type = "CC"

        # used for tokenization
        rsvp_payload['isTokenUse'] = 1 if is_token_paynamics(tx) else 0

        if tx.is_discounted and tx.discount_info and 'discount_value' in tx.discount_info and 'discount_type' in tx.discount_info:
            discount_value_coretool = tx.discount_info['discount_value']
            discount_type_coretool = tx.discount_info['discount_type']
            actual_discounted_seats = tx.discount_info['actual_discounted_seats']

            if tx.discount_info['discount_type'] == 'amount':
                discount_type_coretool = 'bynetamount'
            elif tx.discount_info['discount_type'] == 'percent':
                discount_type_coretool = 'bypercentage'
            elif tx.discount_info['discount_type'] == 'convenience_fee':
                discount_type_coretool = 'waivedconveniencefee'
            elif tx.discount_info['discount_type'] == 'special':
                discount_type_coretool = 'freeticket'
            elif tx.discount_info['discount_type'] == 'rush-rewards':
                discount_type_coretool = 'reward'

            # ADD rsvp_payload for number of tickets with discount
            # for discount_type 'convenience_fee' and 'special'
            if discount_value_coretool and discount_type_coretool in ['bynetamount', 'bypercentage', 'waivedconveniencefee', 'freeticket', 'reward']:
                payment_type = 'CCG'
                rsvp_payload['promoValue'] = discount_value_coretool
                rsvp_payload['promoType'] = discount_type_coretool
                if discount_type_coretool in ['waivedconveniencefee', 'freeticket', 'reward']:
                    rsvp_payload['promoQuantity'] = actual_discounted_seats
            else:
                log.warn("ERROR!, do_reservation_greenhills, cannot process the discounts of coretool claim code...")

                tx.error_info['error_msg'] = "cannot process the discounts of claim code coretool: %s - %s" % (discount_type_coretool, discount_value_coretool)
                return 'error', DEFAULT_ERROR_MESSAGE

        rsvp_payload['lastName'] = tx.payment_info['lname']
        rsvp_payload['firstName'] = tx.payment_info['fname']
        rsvp_payload['emailAddress'] = tx.payment_info['email']
        rsvp_payload['mobileNumber'] = tx.payment_info['mobile']
    elif tx.payment_type in [ReservationTransaction.allowed_payment_types.GCASH,
            ReservationTransaction.allowed_payment_types.GCASH_APP]:
        log.debug("do_reservation_greenhills, GCash Payment...")

        payment_type = 'GCASH'
        full_name = tx.payment_info['full_name'].split(' ')
        last_name = full_name.pop(-1)
        first_name = ' '.join(full_name)

        rsvp_payload['lastName'] = last_name
        rsvp_payload['firstName'] = first_name
        rsvp_payload['emailAddress'] = tx.payment_info['email']
        rsvp_payload['mobileNumber'] = tx.payment_info['mobile']
    elif tx.payment_type in [ReservationTransaction.allowed_payment_types.PROMO_CODE]:
        log.debug("do_reservation_greenhills, PromoCode Payment...")

        payment_type = "CLAIM_CODE"
        last_name = 'Promo Code'
        first_name = 'GMovies'

        if 'full_name' in tx.payment_info:
            full_name = tx.payment_info['full_name'].split(' ')
            full_name = filter(None, full_name)
            last_name = full_name.pop(-1)
            first_name = ' '.join(full_name)

        rsvp_payload['lastName'] = last_name
        rsvp_payload['firstName'] = first_name
        rsvp_payload['emailAddress'] = tx.payment_info['email'] if 'email' in tx.payment_info else ''
        rsvp_payload['mobileNumber'] = tx.payment_info['mobile'] if 'mobile' in tx.payment_info else ''
        rsvp_payload['hasConvenienceFee'] = 1 if tx.workspace['is_bank_deposit'] else 0

        if tx.is_discounted and tx.discount_info and 'discount_value' in tx.discount_info and 'discount_type' in tx.discount_info:
            discount_value_coretool = tx.discount_info['discount_value']
            discount_type_coretool = tx.discount_info['discount_type']
            actual_discounted_seats = tx.discount_info['actual_discounted_seats']

            if tx.discount_info['discount_type'] == 'amount':
                discount_type_coretool = 'bynetamount'
            elif tx.discount_info['discount_type'] == 'percent':
                discount_type_coretool = 'bypercentage'
            elif tx.discount_info['discount_type'] == 'convenience_fee':
                discount_type_coretool = 'waivedconveniencefee'
            elif tx.discount_info['discount_type'] == 'special':
                discount_type_coretool = 'freeticket'
            elif tx.discount_info['discount_type'] == 'rush-rewards':
                discount_type_coretool = 'reward'

            # ADD rsvp_payload for number of tickets with discount
            # for discount_type 'convenience_fee' and 'special'
            if discount_value_coretool and discount_type_coretool in ['bynetamount', 'bypercentage', 'waivedconveniencefee', 'freeticket', 'reward']:
                rsvp_payload['promoValue'] = discount_value_coretool
                rsvp_payload['promoType'] = discount_type_coretool
                if discount_type_coretool in ['waivedconveniencefee', 'freeticket', 'reward']:
                    rsvp_payload['promoQuantity'] = actual_discounted_seats
            else:
                log.warn("ERROR!, do_reservation_greenhills, cannot process the discounts of coretool claim code...")

                tx.error_info['error_msg'] = "cannot process the discounts of claim code coretool: %s - %s" % (discount_type_coretool, discount_value_coretool)
                return 'error', DEFAULT_ERROR_MESSAGE
    else:
        log.warn("ERROR!, do_reservation_greenhills, payment_type is not supported...")

        tx.error_info['error_msg'] = "payment_type is not supported"
        return 'error', DEFAULT_ERROR_MESSAGE

    rsvp_payload['status'] = 2
    rsvp_payload['movieScheduleId'] = tx.workspace['movie_schedule_id']
    rsvp_payload['showtimeId'] = tx.workspace['showtime_id']
    rsvp_payload['paymentType'] = payment_type
    rsvp_payload['origin'] = payment_origin
    # rsvp_payload['convenienceFee'] = float(tx.workspace['convenience_fee']) if 'convenience_fee' in tx.workspace else 0.0

    tx.workspace['payment_type'] = payment_type
    tx.workspace['payment_origin'] = payment_origin

    try:
        if tx.workspace['seating_type'] in ['Free Seating', 'Guaranteed Seats']:
            remaining_seats = 0
            seats_free_seating_url = GREENHILLS_SEATS_FREE_SEATING_FEED_URL % (tx.workspace['movie_schedule_id'], tx.workspace['showtime_id'])
            result = urlfetch.fetch(seats_free_seating_url, deadline=TIMEOUT_DEADLINE)

            if result.status_code != 200:
                log.warn("ERROR!, do_reservation_greenhills, Free Seating / Guaranteed Seats, status_code not equal to 200...")

                tx.error_info['error_msg'] = "Free Seating / Guaranteed Seats, status_code not equal to 200"
                return 'error', DEFAULT_ERROR_MESSAGE

            if result.content:
                log.info('do_reservation_greenhills, Greenhills Malls, available_seats: %s...' % result.content)

                remaining_seats = int(result.content)

            if remaining_seats < int(tx.workspace['seats::quantity']):
                log.warn("ERROR!, do_reservation_greenhills, Greenhills Malls, not enough seats available...")

                tx.error_info['error_msg'] = "not enough seats available"
                return 'error', 'Sorry, not enough seats available.'

            rsvp_payload['quantity'] = tx.workspace['seats::quantity']
            GREENHILLS_LOCK_SEATS_ENDPOINT = GREENHILLS_ENDPOINT_FREE_SEATING
        else:
            rsvp_payload['seatIds'] = tx.workspace['seat_ids']
            GREENHILLS_LOCK_SEATS_ENDPOINT = GREENHILLS_ENDPOINT_RESERVE_SEATING

        log.debug("do_reservation_greenhills, lock-seats request with payload...")
        log.info(rsvp_payload)
        log.debug("do_reservation_greenhills, sending lock-seats request...")

        s = requests.Session()
        req = create_greenhills_request(GREENHILLS_LOCK_SEATS_ENDPOINT, rsvp_payload)
        r_status_code, r_json = call_once(__send_reservation_req, tx, s, req)
        d_json = json.loads(r_json)

        log.debug("do_reservation_greenhills, lock-seats response, status_code: %s..." % str(r_status_code))
        log.info(r_json.encode('utf-8'))

        if r_status_code != 200:
            log.warn("ERROR!, do_reservation_greenhills, status_code not equal to 200...")

            if 'errorCode' in d_json:
                log.warn("ERROR!, do_reservation_greenhills, existing key errorCode in json response...")

                namespace = 'greenhills-malls'
                response_code = d_json['errorCode']
                response_message = d_json['errorMessage'] if 'errorMessage' in d_json else ''

                tx.error_info['error_msg'] = response_message
                return 'error', translate_message_via_error_code(namespace, response_code, response_message)

            tx.error_info['error_msg'] = r_json
            return 'error', DEFAULT_ERROR_MESSAGE

        tx.workspace['decoded_reservation_reference'] = d_json['response']['transactionCode']
        tx.workspace['RESERVATION_ID'] = d_json['response']['id']
        tx.workspace['RSVP:claimcode'] = d_json['response']['transactionCode']
        tx.workspace['RSVP:claimdate'] = convert_date2(d_json['response']['movieSchedule']['showingDate'])
        tx.workspace['RSVP:movie'] = tx.workspace['schedules.movie'][0].canonical_title
        tx.workspace['RSVP:seats'] = [res.seat_id for res in tx.reservations]
        tx.workspace['RSVP:fee'] = tx.workspace['convenience_fee']

        total_amount = str(tx.total_amount(tx.workspace['RSVP:fee'], True))
        total_seat_price = str(tx.total_seat_price(tx.workspace['RSVP:fee'], True))
        rf = tx.workspace['theaters.org_theater_code'][0] + '~' + d_json['response']['transactionCode']
        tx.reservation_reference = rf

        tx.workspace['RSVP:originalseatprice'] = total_seat_price
        tx.workspace['RSVP:originaltotalamount'] = total_amount

        log.debug("total_seat_price: %s" % (total_seat_price))
        log.debug("total_amount: %s" % (total_amount))

        # process for multiple tickets.
        if 'seatTransactions' in d_json['response']:
            log.debug("do_reservation_greenhills, FOUND KEY!, d_json['response'] has seatTransactions key...")

            if tx.workspace['seating_type'] in ['Free Seating', 'Guaranteed Seats']:
                tx.workspace['RSVP:seat_txs_dict'] = []
                seat_counter = 0

                for seat_tx in d_json['response']['seatTransactions']:
                    seat_counter += 1
                    seat_number = 'Ticket No. %d' % seat_counter
                    tx.workspace['RSVP:seat_txs_dict'].append((seat_number, seat_tx['transactionCode']))
            else:
                tx.workspace['RSVP:seat_txs_dict'] = map_seats_label_to_tx(tx.workspace['seat_label_id_mapping'], d_json['response']['seatTransactions'])

            log.debug("do_reservation_greenhills, seat transactions: {}...".format(tx.workspace['RSVP:seat_txs_dict']))
        else:
            log.warn("ERROR!, do_reservation_greenhills, MISSING KEY!, Couldn't find seatTransactions in d_json['response']...")

            tx.error_info['error_msg'] = "Couldn't find seatTransactions in d_json['response']"
            return 'error', DEFAULT_ERROR_MESSAGE

        # process for discount codes.
        if tx.workspace['discount::is_discounted']:
            log.debug("do_reservation_greenhills, Greenhills Malls, transaction is discounted...")

            tx.workspace['RSVP:discounttype'] = tx.workspace['discount::discount_type']
            tx.workspace['RSVP:totaldiscount'] = tx.workspace['discount::total_discount']
            tx.workspace['RSVP:totalamount'] = tx.workspace['discount::total_amount']
            tx.workspace['RSVP:totalseatprice'] = total_seat_price
        else:
            log.debug("do_reservation_greenhills, Greenhills Malls, transaction is not discounted...")
            tx.workspace['RSVP:totalseatprice'] = total_seat_price
            tx.workspace['RSVP:totalamount'] = total_amount

        log.debug("do_reservation_greenhills, reference_number: %s..." % tx.reservation_reference)

        tx.bind_cancellation_callback(cancel_reservation)
        tx.put()
    except Exception, e:
        log.warn("ERROR!, do_reservation_greenhills...")
        log.error(e)
        # send email to GMovies support if exception errors are encountered
        send_email_support(tx, "ERROR in do_reservation: " + str(e))

        tx.error_info['error_msg'] = str(e)
        return 'error', DEFAULT_ERROR_MESSAGE

    return 'success', None

def do_reservation_cinema76(tx):
    def __send_reservation_req(s, req):
        r = s.send(req, timeout=TIMEOUT_DEADLINE)

        return r.status_code, r.text

    rsvp_payload = {}
    payment_origin = "MOBILE" # set to MOBILE as default.
    theaterorg_id = tx.workspace['theaters.org_id'][0]
    theaterorg_key = Key(TheaterOrganization, theaterorg_id)

    if tx.payment_type in [ReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT,
            ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.UNIONBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO]:
        log.debug("do_reservation_cinema76, Credit Card Payment (Paynamics)...")

        payment_type = "CC"

        if tx.is_discounted and tx.discount_info and 'discount_value' in tx.discount_info and 'discount_type' in tx.discount_info:
            discount_value_coretool = tx.discount_info['discount_value']
            discount_type_coretool = tx.discount_info['discount_type']
            actual_discounted_seats = tx.discount_info['actual_discounted_seats']

            if tx.discount_info['discount_type'] == 'amount':
                discount_type_coretool = 'bynetamount'
            elif tx.discount_info['discount_type'] == 'percent':
                discount_type_coretool = 'bypercentage'
            elif tx.discount_info['discount_type'] == 'convenience_fee':
                discount_type_coretool = 'waivedconveniencefee'
            elif tx.discount_info['discount_type'] == 'special':
                discount_type_coretool = 'freeticket'
            elif tx.discount_info['discount_type'] == 'rush-rewards':
                discount_type_coretool = 'reward'

            # ADD rsvp_payload for number of tickets with discount
            # for discount_type 'convenience_fee' and 'special'
            if discount_value_coretool and discount_type_coretool in ['bynetamount', 'bypercentage', 'waivedconveniencefee', 'freeticket', 'reward']:
                payment_type = 'CCG'
                rsvp_payload['promoValue'] = discount_value_coretool
                rsvp_payload['promoType'] = discount_type_coretool
                if discount_type_coretool in ['waivedconveniencefee', 'freeticket', 'reward']:
                    rsvp_payload['promoQuantity'] = actual_discounted_seats
            else:
                log.warn("ERROR!, do_reservation_cinema76, cannot process the discounts of coretool claim code...")
                tx.error_info['error_msg'] = "cannot process the discounts of claim code coretool: %s - %s" % (discount_type_coretool, discount_value_coretool)

                return 'error', DEFAULT_ERROR_MESSAGE

        rsvp_payload['lastName'] = tx.payment_info['lname']
        rsvp_payload['firstName'] = tx.payment_info['fname']
        rsvp_payload['emailAddress'] = tx.payment_info['email']
        rsvp_payload['mobileNumber'] = tx.payment_info['mobile']
    elif tx.payment_type in [ReservationTransaction.allowed_payment_types.GCASH]:
        log.debug("do_reservation_cinema76, GCash Payment...")

        payment_type = 'GCASH'
        full_name = tx.payment_info['full_name'].split(' ')
        last_name = full_name.pop(-1)
        first_name = ' '.join(full_name)

        rsvp_payload['lastName'] = last_name
        rsvp_payload['firstName'] = first_name
        rsvp_payload['emailAddress'] = tx.payment_info['email']
        rsvp_payload['mobileNumber'] = tx.payment_info['mobile']
    elif tx.payment_type in [ReservationTransaction.allowed_payment_types.PROMO_CODE]:
        log.debug("do_reservation_cinema76, PromoCode Payment...")

        payment_type = "CLAIM_CODE"
        last_name = 'Promo Code'
        first_name = 'GMovies'

        if 'full_name' in tx.payment_info:
            full_name = tx.payment_info['full_name'].split(' ')
            last_name = full_name.pop(-1)
            first_name = ' '.join(full_name)

        rsvp_payload['lastName'] = last_name
        rsvp_payload['firstName'] = first_name
        rsvp_payload['emailAddress'] = tx.payment_info['email'] if 'email' in tx.payment_info else ''
        rsvp_payload['mobileNumber'] = tx.payment_info['mobile'] if 'mobile' in tx.payment_info else ''
        rsvp_payload['hasConvenienceFee'] = 1 if tx.workspace['is_bank_deposit'] else 0

        if tx.is_discounted and tx.discount_info and 'discount_value' in tx.discount_info and 'discount_type' in tx.discount_info:
            discount_value_coretool = tx.discount_info['discount_value']
            discount_type_coretool = tx.discount_info['discount_type']
            actual_discounted_seats = tx.discount_info['actual_discounted_seats']

            if tx.discount_info['discount_type'] == 'amount':
                discount_type_coretool = 'bynetamount'
            elif tx.discount_info['discount_type'] == 'percent':
                discount_type_coretool = 'bypercentage'
            elif tx.discount_info['discount_type'] == 'convenience_fee':
                discount_type_coretool = 'waivedconveniencefee'
            elif tx.discount_info['discount_type'] == 'special':
                discount_type_coretool = 'freeticket'
            elif tx.discount_info['discount_type'] == 'rush-rewards':
                discount_type_coretool = 'reward'

            # ADD rsvp_payload for number of tickets with discount
            # for discount_type 'convenience_fee' and 'special'
            if discount_value_coretool and discount_type_coretool in ['bynetamount', 'bypercentage', 'waivedconveniencefee', 'freeticket', 'reward']:
                rsvp_payload['promoValue'] = discount_value_coretool
                rsvp_payload['promoType'] = discount_type_coretool
                if discount_type_coretool in ['waivedconveniencefee', 'freeticket', 'reward']:
                    rsvp_payload['promoQuantity'] = actual_discounted_seats
            else:
                log.warn("ERROR!, do_reservation_cinema76, cannot process the discounts of coretool claim code...")
                tx.error_info['error_msg'] = "cannot process the discounts of claim code coretool: %s - %s" % (discount_type_coretool, discount_value_coretool)

                return 'error', DEFAULT_ERROR_MESSAGE
    else:
        log.warn("ERROR!, do_reservation_cinema76, payment_type is not supported...")

        tx.error_info['error_msg'] = "payment_type is not supported"
        return 'error', DEFAULT_ERROR_MESSAGE

    rsvp_payload['status'] = 2
    rsvp_payload['movieScheduleId'] = tx.workspace['movie_schedule_id']
    rsvp_payload['showtimeId'] = tx.workspace['showtime_id']
    rsvp_payload['paymentType'] = payment_type
    rsvp_payload['origin'] = payment_origin
    # rsvp_payload['convenienceFee'] = float(tx.workspace['convenience_fee']) if 'convenience_fee' in tx.workspace else 0.0

    tx.workspace['payment_type'] = payment_type
    tx.workspace['payment_origin'] = payment_origin

    try:
        if tx.workspace['seating_type'] in ['Free Seating', 'Guaranteed Seats']:
            remaining_seats = 0
            seats_free_seating_url = CINEMA76_SEATS_FREE_SEATING_FEED_URL % (tx.workspace['movie_schedule_id'], tx.workspace['showtime_id'])
            result = urlfetch.fetch(seats_free_seating_url, deadline=TIMEOUT_DEADLINE)

            if result.status_code != 200:
                log.warn("ERROR!, do_reservation_cinema76, Free Seating, status_code not equal to 200...")

                d_json = json.loads(result.content)
                if 'errorCode' in d_json:
                    log.warn("ERROR!, do_reservation_cinema76, existing key errorCode in json response...")

                    namespace = 'cinema76-malls'
                    response_code = d_json['errorCode']
                    response_message = d_json['errorMessage'] if 'errorMessage' in d_json else ''

                    tx.error_info['error_msg'] = response_message
                    return 'error', translate_message_via_error_code(namespace, response_code, response_message)
                tx.error_info['error_msg'] = result.content
                return 'error', DEFAULT_ERROR_MESSAGE

            if result.content:
                log.info('do_reservation_cinema76, Cinema 76 Malls, available_seats: %s...' % result.content)

                remaining_seats = int(result.content)

            if remaining_seats < int(tx.workspace['seats::quantity']):
                log.warn("ERROR!, do_reservation_cinema76, Cinema 76 Malls, not enough seats available...")

                namespace = 'cinema76-malls'
                response_code = 'NOTENOUGHSEATS'
                response_message = DEFAULT_ERROR_MESSAGE

                tx.error_info['error_msg'] = "not enough seats available"
                return 'error', translate_message_via_error_code(namespace, response_code, response_message)

            rsvp_payload['quantity'] = tx.workspace['seats::quantity']
        else:
            log.debug("ERROR!, do_reservation_cinema76, only supports 'Free / Guaranteed Seating', seating_type: %s..." % (tx.workspace['seating_type']))
            tx.error_info['error_msg'] = "Cinema76, only supports 'Free / Guaranteed Seating', seating_type: %s..." % (tx.workspace['seating_type'])
            return 'error', DEFAULT_ERROR_MESSAGE

        log.debug("do_reservation_cinema76, lock-seats request with payload...")
        log.info(rsvp_payload)
        log.debug("do_reservation_cinema76, sending lock-seats request...")

        s = requests.Session()
        req = create_cinema76_request(CINEMA76_ENDPOINT_FREE_SEATING, rsvp_payload)
        r_status_code, r_json = call_once(__send_reservation_req, tx, s, req)
        d_json = json.loads(r_json)

        log.debug("do_reservation_cinema76, lock-seats response, status_code: %s..." % str(r_status_code))
        log.info(r_json.encode('utf-8'))

        if r_status_code != 200:
            log.warn("ERROR!, do_reservation_cinema76, status_code not equal to 200...")

            if 'errorCode' in d_json:
                log.warn("ERROR!, do_reservation_cinema76, existing key errorCode in json response...")

                namespace = 'greenhills-malls'
                response_code = d_json['errorCode']
                response_message = d_json['errorMessage'] if 'errorMessage' in d_json else ''

                tx.error_info['error_msg'] = response_message
                return 'error', translate_message_via_error_code(namespace, response_code, response_message)

            tx.error_info['error_msg'] = r_json
            return 'error', DEFAULT_ERROR_MESSAGE

        tx.workspace['decoded_reservation_reference'] = d_json['response']['transactionCode']
        tx.workspace['reservation_reference'] = d_json['response']['transactionCode']
        tx.workspace['RESERVATION_ID'] = d_json['response']['id']
        tx.workspace['RSVP:claimcode'] = d_json['response']['transactionCode']
        tx.workspace['RSVP:claimdate'] = convert_date2(d_json['response']['movieSchedule']['showingDate'])
        tx.workspace['RSVP:movie'] = tx.workspace['schedules.movie'][0].canonical_title
        tx.workspace['RSVP:seats'] = [res.seat_id for res in tx.reservations]
        tx.workspace['RSVP:fee'] = tx.workspace['convenience_fee']

        total_amount = str(tx.total_amount(tx.workspace['RSVP:fee'], True))
        total_seat_price = str(tx.total_seat_price(tx.workspace['RSVP:fee'], True))
        rf = tx.workspace['theaters.org_theater_code'][0] + '~' + d_json['response']['transactionCode']
        tx.reservation_reference = rf

        tx.workspace['RSVP:originalseatprice'] = total_seat_price
        tx.workspace['RSVP:originaltotalamount'] = total_amount

        # process for multiple tickets.
        if 'seatTransactions' in d_json['response']:
            log.debug("do_reservation_cinema76, FOUND KEY!, d_json['response'] has seatTransactions key...")

            if tx.workspace['seating_type'] in ['Free Seating', 'Guaranteed Seats']:
                tx.workspace['RSVP:seat_txs_dict'] = []
                seat_counter = 0

                for seat_tx in d_json['response']['seatTransactions']:
                    seat_counter += 1
                    seat_number = 'Ticket No. %d' % seat_counter
                    tx.workspace['RSVP:seat_txs_dict'].append((seat_number, seat_tx['transactionCode']))
            else:
                tx.workspace['RSVP:seat_txs_dict'] = map_seats_label_to_tx(tx.workspace['seat_label_id_mapping'], d_json['response']['seatTransactions'])

            log.debug("do_reservation_cinema76, seat transactions: {}...".format(tx.workspace['RSVP:seat_txs_dict']))
        else:
            log.warn("ERROR!, do_reservation_cinema76, MISSING KEY!, Couldn't find seatTransactions in d_json['response']...")

            tx.error_info['error_msg'] = "Couldn't find seatTransactions in d_json['response']"
            return 'error', DEFAULT_ERROR_MESSAGE

        # process for discount codes.
        if tx.workspace['discount::is_discounted']:
            log.debug("do_reservation_cinema76, Cinema 76 Malls, transaction is discounted...")

            tx.workspace['RSVP:discounttype'] = tx.workspace['discount::discount_type']
            tx.workspace['RSVP:totaldiscount'] = tx.workspace['discount::total_discount']
            tx.workspace['RSVP:totalamount'] = tx.workspace['discount::total_amount']
            tx.workspace['RSVP:totalseatprice'] = total_seat_price
        else:
            log.debug("do_reservation_cinema76, Cinema 76 Malls, transaction is not discounted...")
            tx.workspace['RSVP:totalseatprice'] = total_seat_price
            tx.workspace['RSVP:totalamount'] = total_amount

        log.debug("do_reservation_cinema76, reference_number: %s..." % tx.reservation_reference)

        # only do this for tokenized 3d transactions
        if is_3d_token_paynamics(tx):
            log.debug("do_reservation_cinema76, transaction is 3d tokenized...")
            payment_engine = tx.workspace[WS_PAYMENT_ENGINE]
            prepare_3d_status = ''
            prepare_3d_message = ''
            if tx.workspace['discount::is_discounted']:
                log.debug(payment_engine.payment_engine)
                prepare_3d_status, prepare_3d_message = payment_engine.payment_engine.prepare_paynamics_tokenization_3d(tx)
            else:
                prepare_3d_status, prepare_3d_message = payment_engine.prepare_paynamics_tokenization_3d(tx)
            if 'error' == prepare_3d_status:
                return prepare_3d_status, prepare_3d_message

        tx.bind_cancellation_callback(cancel_reservation)
        tx.put()
    except Exception, e:
        log.warn("ERROR!, do_reservation_cinema76...")
        log.error(e)
        # send email to GMovies support if exception errors are encountered
        send_email_support(tx, "ERROR in do_reservation: " + str(e))

        tx.error_info['error_msg'] = str(e)
        return 'error', DEFAULT_ERROR_MESSAGE

    return 'success', None

def do_reservation_globeevents(tx):
    def __send_reservation_req(s, req):
        r = s.send(req, timeout=TIMEOUT_DEADLINE)

        return r.status_code, r.text

    rsvp_payload = {}
    payment_origin = "MOBILE" # set to MOBILE as default.
    theaterorg_id = tx.workspace['theaters.org_id'][0]
    theaterorg_key = Key(TheaterOrganization, theaterorg_id)

    if tx.payment_type in [ReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT,
            ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.UNIONBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO]:
        log.debug("do_reservation_globeevents, Credit Card Payment (Paynamics)...")

        payment_type = "CC"

        if tx.is_discounted and tx.discount_info and 'discount_value' in tx.discount_info and 'discount_type' in tx.discount_info:
            discount_value_coretool = tx.discount_info['discount_value']
            discount_type_coretool = tx.discount_info['discount_type']
            actual_discounted_seats = tx.discount_info['actual_discounted_seats']

            if tx.discount_info['discount_type'] == 'amount':
                discount_type_coretool = 'bynetamount'
            elif tx.discount_info['discount_type'] == 'percent':
                discount_type_coretool = 'bypercentage'
            elif tx.discount_info['discount_type'] == 'convenience_fee':
                discount_type_coretool = 'waivedconveniencefee'
            elif tx.discount_info['discount_type'] == 'special':
                discount_type_coretool = 'freeticket'
            elif tx.discount_info['discount_type'] == 'rush-rewards':
                discount_type_coretool = 'reward'

            # ADD rsvp_payload for number of tickets with discount
            # for discount_type 'convenience_fee' and 'special'
            if discount_value_coretool and discount_type_coretool in ['bynetamount', 'bypercentage', 'waivedconveniencefee', 'freeticket', 'reward']:
                payment_type = 'CCG'
                rsvp_payload['promoValue'] = discount_value_coretool
                rsvp_payload['promoType'] = discount_type_coretool
                if discount_type_coretool in ['waivedconveniencefee', 'freeticket', 'reward']:
                    rsvp_payload['promoQuantity'] = actual_discounted_seats
            else:
                log.warn("ERROR!, do_reservation_globeevents, cannot process the discounts of coretool claim code...")
                tx.error_info['error_msg'] = "cannot process the discounts of claim code coretool: %s - %s" % (discount_type_coretool, discount_value_coretool)

                return 'error', DEFAULT_ERROR_MESSAGE

        rsvp_payload['lastName'] = tx.payment_info['lname']
        rsvp_payload['firstName'] = tx.payment_info['fname']
        rsvp_payload['emailAddress'] = tx.payment_info['email']
        rsvp_payload['mobileNumber'] = tx.payment_info['mobile']
    elif tx.payment_type in [ReservationTransaction.allowed_payment_types.GCASH]:
        log.debug("do_reservation_globeevents, GCash Payment...")

        payment_type = 'GCASH'
        full_name = tx.payment_info['full_name'].split(' ')
        last_name = full_name.pop(-1)
        first_name = ' '.join(full_name)

        rsvp_payload['lastName'] = last_name
        rsvp_payload['firstName'] = first_name
        rsvp_payload['emailAddress'] = tx.payment_info['email']
        rsvp_payload['mobileNumber'] = tx.payment_info['mobile']
    elif tx.payment_type in [ReservationTransaction.allowed_payment_types.PROMO_CODE]:
        log.debug("do_reservation_globeevents, PromoCode Payment...")

        payment_type = "CLAIM_CODE"
        last_name = 'Promo Code'
        first_name = 'GMovies'

        if 'full_name' in tx.payment_info:
            full_name = tx.payment_info['full_name'].split(' ')
            last_name = full_name.pop(-1)
            first_name = ' '.join(full_name)

        rsvp_payload['lastName'] = last_name
        rsvp_payload['firstName'] = first_name
        rsvp_payload['emailAddress'] = tx.payment_info['email'] if 'email' in tx.payment_info else ''
        rsvp_payload['mobileNumber'] = tx.payment_info['mobile'] if 'mobile' in tx.payment_info else ''
        # rsvp_payload['hasConvenienceFee'] = 1 if tx.workspace['is_bank_deposit'] else 0

        if tx.is_discounted and tx.discount_info and 'discount_value' in tx.discount_info and 'discount_type' in tx.discount_info:
            discount_value_coretool = tx.discount_info['discount_value']
            discount_type_coretool = tx.discount_info['discount_type']
            actual_discounted_seats = tx.discount_info['actual_discounted_seats']

            if tx.discount_info['discount_type'] == 'amount':
                discount_type_coretool = 'bynetamount'
            elif tx.discount_info['discount_type'] == 'percent':
                discount_type_coretool = 'bypercentage'
            elif tx.discount_info['discount_type'] == 'convenience_fee':
                discount_type_coretool = 'waivedconveniencefee'
            elif tx.discount_info['discount_type'] == 'special':
                discount_type_coretool = 'freeticket'
            elif tx.discount_info['discount_type'] == 'rush-rewards':
                discount_type_coretool = 'reward'

            # ADD rsvp_payload for number of tickets with discount
            # for discount_type 'convenience_fee' and 'special'
            if discount_value_coretool and discount_type_coretool in ['bynetamount', 'bypercentage', 'waivedconveniencefee', 'freeticket', 'reward']:
                rsvp_payload['promoValue'] = discount_value_coretool
                rsvp_payload['promoType'] = discount_type_coretool
                if discount_type_coretool in ['waivedconveniencefee', 'freeticket', 'reward']:
                    rsvp_payload['promoQuantity'] = actual_discounted_seats
            else:
                log.warn("ERROR!, do_reservation_globeevents, cannot process the discounts of coretool claim code...")
                tx.error_info['error_msg'] = "cannot process the discounts of claim code coretool: %s - %s" % (discount_type_coretool, discount_value_coretool)

                return 'error', DEFAULT_ERROR_MESSAGE
    else:
        log.warn("ERROR!, do_reservation_globeevents, payment_type is not supported...")

        tx.error_info['error_msg'] = "payment_type is not supported"
        return 'error', DEFAULT_ERROR_MESSAGE

    rsvp_payload['status'] = 2
    rsvp_payload['movieScheduleId'] = tx.workspace['movie_schedule_id']
    rsvp_payload['showtimeId'] = tx.workspace['showtime_id']
    rsvp_payload['paymentType'] = payment_type
    rsvp_payload['origin'] = payment_origin
    rsvp_payload['convenienceFee'] = float(tx.workspace['convenience_fee']) if 'convenience_fee' in tx.workspace else 0.0

    tx.workspace['payment_type'] = payment_type
    tx.workspace['payment_origin'] = payment_origin

    try:
        if tx.workspace['seating_type'] in ['Free Seating', 'Guaranteed Seats']:
            remaining_seats = 0
            seats_free_seating_url = GLOBEEVENTS_SEATS_FREE_SEATING_FEED_URL % (tx.workspace['movie_schedule_id'], tx.workspace['showtime_id'])
            result = urlfetch.fetch(seats_free_seating_url, deadline=TIMEOUT_DEADLINE)

            if result.status_code != 200:
                log.warn("ERROR!, do_reservation_globeevents, Free Seating, status_code not equal to 200...")

                d_json = json.loads(result.content)
                if 'errorCode' in d_json:
                    log.warn("ERROR!, do_reservation_globeevents, existing key errorCode in json response...")

                    namespace = 'globe-events'
                    response_code = d_json['errorCode']
                    response_message = d_json['errorMessage'] if 'errorMessage' in d_json else ''

                    tx.error_info['error_msg'] = response_message
                    return 'error', translate_message_via_error_code(namespace, response_code, response_message)
                tx.error_info['error_msg'] = result.content
                return 'error', DEFAULT_ERROR_MESSAGE

            if result.content:
                log.info('do_reservation_globeevents, Globe Events, available_seats: %s...' % result.content)

                remaining_seats = int(result.content)

            if remaining_seats < int(tx.workspace['seats::quantity']):
                log.warn("ERROR!, do_reservation_globeevents, Globe Events, not enough seats available...")

                namespace = 'globe-events'
                response_code = 'NOTENOUGHSEATS'
                response_message = DEFAULT_ERROR_MESSAGE

                tx.error_info['error_msg'] = "not enough seats available"
                return 'error', translate_message_via_error_code(namespace, response_code, response_message)

            rsvp_payload['quantity'] = tx.workspace['seats::quantity']
        else:
            log.debug("ERROR!, do_reservation_globeevents, only supports 'Free / Guaranteed Seating', seating_type: %s..." % (tx.workspace['seating_type']))
            tx.error_info['error_msg'] = "Globe Events, only supports 'Free / Guaranteed Seating', seating_type: %s..." % (tx.workspace['seating_type'])
            return 'error', DEFAULT_ERROR_MESSAGE

        log.debug("do_reservation_globeevents, lock-seats request with payload...")
        log.info(rsvp_payload)
        log.debug("do_reservation_globeevents, sending lock-seats request...")

        s = requests.Session()
        req = create_globeevents_request(GLOBEEVENTS_ENDPOINT_FREE_SEATING, rsvp_payload)
        r_status_code, r_json = call_once(__send_reservation_req, tx, s, req)
        d_json = json.loads(r_json)

        log.debug("do_reservation_globeevents, lock-seats response, status_code: %s..." % str(r_status_code))
        log.info(r_json.encode('utf-8'))

        if r_status_code != 200:
            log.warn("ERROR!, do_reservation_globeevents, status_code not equal to 200...")

            if 'errorCode' in d_json:
                log.warn("ERROR!, do_reservation_globeevents, existing key errorCode in json response...")

                namespace = 'greenhills-malls'
                response_code = d_json['errorCode']
                response_message = d_json['errorMessage'] if 'errorMessage' in d_json else ''

                tx.error_info['error_msg'] = response_message
                return 'error', translate_message_via_error_code(namespace, response_code, response_message)

            tx.error_info['error_msg'] = r_json
            return 'error', DEFAULT_ERROR_MESSAGE

        tx.workspace['decoded_reservation_reference'] = d_json['response']['transactionCode']
        tx.workspace['reservation_reference'] = d_json['response']['transactionCode']
        tx.workspace['RESERVATION_ID'] = d_json['response']['id']
        tx.workspace['RSVP:claimcode'] = d_json['response']['transactionCode']
        tx.workspace['RSVP:claimdate'] = convert_date2(d_json['response']['movieSchedule']['showingDate'])
        tx.workspace['RSVP:movie'] = tx.workspace['schedules.movie'][0].canonical_title
        tx.workspace['RSVP:seats'] = [res.seat_id for res in tx.reservations]
        tx.workspace['RSVP:fee'] = tx.workspace['convenience_fee']

        total_amount = str(tx.total_amount(tx.workspace['RSVP:fee'], True))
        total_seat_price = str(tx.total_seat_price(tx.workspace['RSVP:fee'], True))
        rf = tx.workspace['theaters.org_theater_code'][0] + '~' + d_json['response']['transactionCode']
        tx.reservation_reference = rf

        tx.workspace['RSVP:originalseatprice'] = total_seat_price
        tx.workspace['RSVP:originaltotalamount'] = total_amount

        # process for multiple tickets.
        if 'seatTransactions' in d_json['response']:
            log.debug("do_reservation_globeevents, FOUND KEY!, d_json['response'] has seatTransactions key...")

            if tx.workspace['seating_type'] in ['Free Seating', 'Guaranteed Seats']:
                tx.workspace['RSVP:seat_txs_dict'] = []
                seat_counter = 0

                for seat_tx in d_json['response']['seatTransactions']:
                    seat_counter += 1
                    seat_number = 'Ticket No. %d' % seat_counter
                    tx.workspace['RSVP:seat_txs_dict'].append((seat_number, seat_tx['transactionCode']))
            else:
                tx.workspace['RSVP:seat_txs_dict'] = map_seats_label_to_tx(tx.workspace['seat_label_id_mapping'], d_json['response']['seatTransactions'])

            log.debug("do_reservation_globeevents, seat transactions: {}...".format(tx.workspace['RSVP:seat_txs_dict']))
        else:
            log.warn("ERROR!, do_reservation_globeevents, MISSING KEY!, Couldn't find seatTransactions in d_json['response']...")

            tx.error_info['error_msg'] = "Couldn't find seatTransactions in d_json['response']"
            return 'error', DEFAULT_ERROR_MESSAGE

        # process for discount codes.
        if tx.workspace['discount::is_discounted']:
            log.debug("do_reservation_globeevents, Globe Events, transaction is discounted...")

            tx.workspace['RSVP:discounttype'] = tx.workspace['discount::discount_type']
            tx.workspace['RSVP:totaldiscount'] = tx.workspace['discount::total_discount']
            tx.workspace['RSVP:totalamount'] = tx.workspace['discount::total_amount']
            tx.workspace['RSVP:totalseatprice'] = total_seat_price
        else:
            log.debug("do_reservation_globeevents, Globe Events, transaction is not discounted...")
            tx.workspace['RSVP:totalseatprice'] = total_seat_price
            tx.workspace['RSVP:totalamount'] = total_amount

        log.debug("do_reservation_globeevents, reference_number: %s..." % tx.reservation_reference)

        tx.bind_cancellation_callback(cancel_reservation)
        tx.put()
    except Exception, e:
        log.warn("ERROR!, do_reservation_globeevents...")
        log.error(e)
        # send email to GMovies support if exception errors are encountered
        send_email_support(tx, "ERROR in do_reservation: " + str(e))

        tx.error_info['error_msg'] = str(e)
        return 'error', DEFAULT_ERROR_MESSAGE

    return 'success', None

def do_reservation_smmalls(tx):
    rsvp_payload = {}
    # payment_type = 15 # default, as per MGi, use 15 as payment type to create transaction, gcash via eplus.
    payment_type = 9 # new for gcash and claim code
    theaterorg_id = tx.workspace['theaters.org_id'][0]
    theater_code = tx.workspace['theaters.org_theater_code'][0]

    def __send_reservation_req(payload, org_uuid):
        status, r_data = create_transaction_mgi(payload, org_uuid, is_multiple_branch=True)

        return status, r_data

    if tx.payment_type in [ReservationTransaction.allowed_payment_types.GCASH,
            ReservationTransaction.allowed_payment_types.GCASH_APP]:
        log.debug("do_reservation_smmalls, GCashPayment...")

        rsvp_payload['full_name'] = tx.payment_info['full_name']
        rsvp_payload['email'] = tx.payment_info['email']
        rsvp_payload['payment_type'] = payment_type
    elif tx.payment_type in [ReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT,
            ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.UNIONBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO]:
        log.debug("do_reservation_smmalls, Credit Card Payment (Paynamics)...")
        payment_type = 16  # payment_type for PaynamicsPayment as per MGi

        full_name = '%s %s' % (tx.payment_info['fname'], tx.payment_info['lname'])
        rsvp_payload['full_name'] = full_name.strip()
        rsvp_payload['email'] = tx.payment_info['email']
        rsvp_payload['payment_type'] = payment_type
    elif tx.payment_type in [ReservationTransaction.allowed_payment_types.PROMO_CODE]:
        log.debug("do_reservation_smmalls, PromoCodePayment...")

        rsvp_payload['full_name'] = tx.payment_info['full_name']
        rsvp_payload['email'] = tx.payment_info['email']
        rsvp_payload['payment_type'] = payment_type # ask MGi if the payment_type for PromoCodePayment is different from GCashPayment.
    else:
        log.warn("ERROR, do_reservation_smmalls invalid payment option...")

        tx.error_info['error_msg'] = "invalid payment option"
        return 'error', DEFAULT_ERROR_MESSAGE

    try:
        rsvp_payload['branch_key'] = tx.workspace['mgi::branch_key']
        rsvp_payload['schedule_key'] = tx.workspace['mgi::schedule_key']
        rsvp_payload['seat_list'] = tx.workspace['seat_ids']
        rsvp_payload['seating_type'] = tx.workspace['seating_type']
        rsvp_payload['send_sms'] = False
        rsvp_payload['partner_name'] = 'SM Malls'

        status, r_data = call_once(__send_reservation_req, tx, rsvp_payload, theaterorg_id)

        if status == 'error':
            log.warn("ERROR!, do_reservation_smmalls. %s..." % r_data['message'])

            namespace = 'sm-malls'
            response_code = 'FAILED'
            response_message = r_data['message']

            if 'already taken' in response_message.lower():
                response_code = 'SEATSTAKEN'

            # send email to GMovies support if exception errors are encountered
            if 'exception_msg' in r_data:
                send_email_support(tx, "ERROR in do_reservation (MGI): " + r_data['exception_msg'])

            tx.error_info['error_msg'] = r_data['exception_msg'] if 'exception_msg' in r_data else response_message
            return 'error', translate_message_via_error_code(namespace, response_code, response_message)

        log.debug("do_reservation_smmalls, create transaction results: {}...".format(r_data))

        # use to finalize the transaction.
        tx.workspace['create::reference_number'] = r_data['reference_number']
        tx.workspace['create::token::pin'] = r_data['token']
        tx.workspace['create::amount'] = r_data['message']
        tx.workspace['create::payment_type'] = payment_type

        reference_number = r_data['reference_number']
        tx.reservation_reference = '%s~%s' % (theater_code, reference_number)
        tx.workspace['reservation_reference'] = reference_number
        tx.workspace['RSVP:claimcode'] = 'WEB-%s' % reference_number
        tx.workspace['RSVP:claimdate'] = tx.workspace['schedules.show_datetime'][0].date()
        tx.workspace['RSVP:movie'] = tx.workspace['schedules.movie'][0].canonical_title
        tx.workspace['RSVP:fee'] = tx.workspace['convenience_fee']

        total_amount = str(tx.total_amount(tx.workspace['RSVP:fee'], True))
        total_seat_price = str(tx.total_seat_price(tx.workspace['RSVP:fee'], True))

        if tx.workspace['discount::is_discounted']:
            log.debug("SM MALLS: do_reservation_smmalls, transaction is discounted...")

            tx.workspace['RSVP:discounttype'] = tx.workspace['discount::discount_type']
            tx.workspace['RSVP:totaldiscount'] = tx.workspace['discount::total_discount']
            tx.workspace['RSVP:totalamount'] = tx.workspace['discount::total_amount']
            tx.workspace['RSVP:totalseatprice'] = total_seat_price
        else:
            log.debug("SM MALLS: SUCCES!, do_reservation_smmalls, transaction is not discounted...")

            tx.workspace['RSVP:totalamount'] = total_amount
            tx.workspace['RSVP:totalseatprice'] = total_seat_price

        log.debug("do_reservation_smmalls, reference_number: %s..." % tx.reservation_reference)

        # only do this for tokenized 3d transactions
        if is_3d_token_paynamics(tx):
            log.debug("do_reservation_smmalls, transaction is 3d tokenized...")
            payment_engine = tx.workspace[WS_PAYMENT_ENGINE]
            prepare_3d_status = ''
            prepare_3d_message = ''
            if tx.workspace['discount::is_discounted']:
                log.debug(payment_engine.payment_engine)
                prepare_3d_status, prepare_3d_message = payment_engine.payment_engine.prepare_paynamics_tokenization_3d(tx)
            else:
                prepare_3d_status, prepare_3d_message = payment_engine.prepare_paynamics_tokenization_3d(tx)
            if 'error' == prepare_3d_status:
                return prepare_3d_status, prepare_3d_message

        tx.bind_cancellation_callback(cancel_reservation)
        tx.put()
    except Exception, e:
        log.warn("ERROR!, do_reservation_smmalls...")
        log.error(traceback.format_exc(e))
        # send email to GMovies support if exception errors are encountered
        send_email_support(tx, "ERROR in do_reservation (MGI): " + str(e))

        tx.error_info['error_msg'] = str(e)
        return 'error', DEFAULT_ERROR_MESSAGE

    return 'success', None

def do_reservation_robinsons(tx):
    rsvp_payload = {}
    payment_type = 1 # used in CreateTransactionWithSession
    payment_gateway_id = 3 # used in CreateTransactionWithSession, value given by MGI
    theaterorg_id = tx.workspace['theaters.org_id'][0]
    theater_code = tx.workspace['theaters.org_theater_code'][0]
    client_info = ''

    def __send_reservation_req(payload, org_uuid):
        status, r_data = create_transaction_mgi(payload, org_uuid, is_multiple_branch=True)

        return status, r_data

    # FOR ROB GCASH: Add if condition for GCash / GCash App payment_type (See sample in "do_reservation_smmalls, GCashPayment")
    # For the payment_type confirm with MGI what value you're gonna use.
    # For SM Gcash transactions it is 15
    if tx.payment_type in [ReservationTransaction.allowed_payment_types.GCASH,
            ReservationTransaction.allowed_payment_types.GCASH_APP]:
        log.debug("do_reservation_robinsonsmalls, GCashPayment...")

        rsvp_payload['full_name'] = tx.payment_info['full_name']
        rsvp_payload['email'] = tx.payment_info['email']
        rsvp_payload['payment_type'] = payment_type

        full_name = tx.payment_info['full_name'].split(' ')
        last_name = full_name.pop(-1)
        first_name = ' '.join(full_name)
        middle_name = ''
        email = tx.payment_info['email']
        mobile = tx.payment_info['mobile']
        client_info = [last_name, first_name, middle_name, email, mobile]

        rsvp_payload['client_info'] = '|'.join(client_info)

    elif tx.payment_type in [ReservationTransaction.allowed_payment_types.MIGS_PAYMENT,
            ReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT,
            ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.UNIONBANK_CARDHOLDER_PROMO,
            ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO]:
        log.debug("do_reservation_robinsons, Credit Card Payment via Paynamics or MIGS payment gateway...")

        if 'theaters.payment_gateway' in tx.workspace and tx.workspace['theaters.payment_gateway'] == 'migs':
            log.debug("do_reservation_robinsons, MIGS, overide value for payment_gateway_id...")

            payment_gateway_id = 0

        last_name = tx.payment_info['lname']
        first_name = tx.payment_info['fname']
        middle_name = tx.payment_info['mname']
        email = tx.payment_info['email']
        mobile = tx.payment_info['mobile']
        client_info = [last_name, first_name, middle_name, email, mobile]

        rsvp_payload['client_info'] = '|'.join(client_info)
    elif tx.payment_type in [ReservationTransaction.allowed_payment_types.PROMO_CODE]:
        log.debug("do_reservation_robinsons, PromoCodePayment...")

        full_name = tx.payment_info['full_name'].split(' ')
        last_name = full_name.pop(-1)
        first_name = ' '.join(full_name)
        middle_name = ''
        email = tx.payment_info['email']
        mobile = tx.payment_info['mobile']
        client_info = [last_name, first_name, middle_name, email, mobile]

        rsvp_payload['client_info'] = '|'.join(client_info)
    else:
        log.warn("ERROR, do_reservation_robinsons invalid payment option...")

        tx.error_info['error_msg'] = "invalid payment option"
        return 'error', DEFAULT_ERROR_MESSAGE

    try:
        rsvp_payload['email'] = tx.payment_info['email']
        rsvp_payload['payment_type'] = payment_type
        rsvp_payload['payment_gw_id'] = payment_gateway_id
        rsvp_payload['branch_key'] = tx.workspace['mgi::branch_key']
        rsvp_payload['schedule_key'] = tx.workspace['mgi::schedule_key']
        rsvp_payload['seat_list'] = tx.workspace['seat_ids']
        rsvp_payload['send_sms'] = False
        rsvp_payload['food_bundle'] = ['' for i in range(tx.reservation_count())] # needed for CreateTransactionWithSession
        rsvp_payload['partner_name'] = 'Robinsons Malls'

        status, r_data = call_once(__send_reservation_req, tx, rsvp_payload, theaterorg_id)

        if status == 'error':
            log.warn("ERROR!, do_reservation_robinsons. %s..." % r_data['message'])

            namespace = 'robinsons-malls'
            response_code = 'FAILED'
            response_message = r_data['message']

            if 'already taken' in response_message.lower():
                response_code = 'SEATSTAKEN'
            elif 'is cinema house seat' in response_message.lower():
                response_code = 'ISCINEMAHOUSESEAT'
                response_message = r_data['message'].replace('Transaction Error: ', '') + ' Please make another selection.'

            # send email to GMovies support if exception errors are encountered
            if 'exception_msg' in r_data:
                send_email_support(tx, "ERROR in do_reservation (MGI): " + r_data['exception_msg'])

            tx.error_info['error_msg'] = r_data['exception_msg'] if 'exception_msg' in r_data else response_message
            return 'error', translate_message_via_error_code(namespace, response_code, response_message)

        log.debug("do_reservation_robinsons, create transaction results: {}...".format(r_data))

        # use to finalize the transaction.
        tx.workspace['create::reference_number'] = r_data['reference_number']
        tx.workspace['create::token::pin'] = r_data['token']
        tx.workspace['create::amount'] = r_data['message']
        tx.workspace['create::payment_type'] = payment_type
        tx.workspace['create::client_info'] = client_info

        reference_number = r_data['reference_number']
        tx.reservation_reference = '%s~%s' % (theater_code, reference_number)
        if ReservationTransaction.allowed_payment_types.MIGS_PAYMENT == tx.payment_type:
            tx.reservation_reference = reference_number
        tx.workspace['reservation_reference'] = reference_number
        tx.workspace['RSVP:claimcode'] = reference_number
        tx.workspace['RSVP:claimdate'] = tx.workspace['schedules.show_datetime'][0].date()
        tx.workspace['RSVP:movie'] = tx.workspace['schedules.movie'][0].canonical_title
        tx.workspace['RSVP:fee'] = tx.workspace['convenience_fee']

        total_amount = str(tx.total_amount(tx.workspace['RSVP:fee'], True))
        total_seat_price = str(tx.total_seat_price(tx.workspace['RSVP:fee'], True))

        if tx.workspace['discount::is_discounted']:
            log.debug("ROBINSONS MALLS: do_reservation_robinsons, transaction is discounted...")

            tx.workspace['RSVP:discounttype'] = tx.workspace['discount::discount_type']
            tx.workspace['RSVP:totaldiscount'] = tx.workspace['discount::total_discount']
            tx.workspace['RSVP:totalamount'] = tx.workspace['discount::total_amount']
            tx.workspace['RSVP:totalseatprice'] = total_seat_price
        else:
            log.debug("ROBINSONS MALLS: SUCCESS!, do_reservation_robinsons, transaction is not discounted...")

            tx.workspace['RSVP:totalamount'] = total_amount
            tx.workspace['RSVP:totalseatprice'] = total_seat_price

        tx.bind_cancellation_callback(cancel_reservation)
        tx.put()
    except Exception, e:
        log.warn("ERROR!, do_reservation_robinsons...")
        log.error(e)
        # send email to GMovies support if exception errors are encountered
        send_email_support(tx, "ERROR in do_reservation (MGI): " + str(e))

        tx.error_info['error_msg'] = str(e)
        return 'error', DEFAULT_ERROR_MESSAGE

    return 'success', None

def do_reservation_megaworld(tx):
    rsvp_payload = {}
    payment_type = 1 # default for credit card transaction.
    theaterorg_id = tx.workspace['theaters.org_id'][0]
    theater_code = tx.workspace['theaters.org_theater_code'][0]

    def __send_reservation_req_mgi(payload, org_uuid, theater_code, is_multiple_branch=False):
        status, r_data = create_transaction_mgi(payload, org_uuid, theater_code=theater_code, is_multiple_branch=is_multiple_branch)

        return status, r_data

    if tx.payment_type in [ReservationTransaction.allowed_payment_types.IPAY88_PAYMENT]:
        log.debug("Megaworld (%s) Credit Card Payment..." % theater_code)

        rsvp_payload['lastName'] = tx.payment_info['last_name']
        rsvp_payload['firstName'] = tx.payment_info['first_name']
        rsvp_payload['emailAddress'] = tx.payment_info['email']
        rsvp_payload['mobileNumber'] = tx.payment_info['contact_number']
    else:
        log.warn("ERROR!, do_reservation_megaworld (%s) invalid payment option..." % theater_code)

        tx.error_info['error_msg'] = "invalid payment option"
        return 'error', DEFAULT_ERROR_MESSAGE

    try:
        if theater_code in ALLOWED_MGI_RESERVATION_BRANCH_CODES:
            rsvp_payload['email'] = tx.payment_info['email']
            rsvp_payload['branch_key'] = tx.workspace['mgi::branch_key']
            rsvp_payload['schedule_key'] = tx.workspace['mgi::schedule_key']
            rsvp_payload['seat_list'] = tx.workspace['seat_ids']
            rsvp_payload['seating_type'] = tx.workspace['seating_type']
            rsvp_payload['send_sms'] = False
            rsvp_payload['partner_name'] = 'Megaworld Malls'

            status, r_data = call_once(__send_reservation_req_mgi, tx, rsvp_payload, theaterorg_id, theater_code)

            if status == 'error':
                log.warn("ERROR!, do_reservation_megaworld (%s). %s..." % (theater_code, r_data['message']))

                namespace = 'megaworld-malls'
                response_code = 'FAILED'
                response_message = r_data['message']

                if 'already taken' in response_message.lower():
                    response_code = 'SEATSTAKEN'

                # send email to GMovies support if exception errors are encountered
                if 'exception_msg' in r_data:
                    send_email_support(tx, "ERROR in do_reservation (MGI): " + r_data['exception_msg'])

                tx.error_info['error_msg'] = r_data['exception_msg'] if 'exception_msg' in r_data else response_message
                return 'error', translate_message_via_error_code(namespace, response_code, response_message)

            log.debug("do_reservation_megaworld ({0}), create transaction results: {1}".format(theater_code, r_data))

            # use to finalize the transaction.
            tx.workspace['create::reference_number'] = r_data['reference_number']
            tx.workspace['create::token::pin'] = r_data['token']
            tx.workspace['create::amount'] = r_data['message']
            tx.workspace['create::payment_type'] = payment_type

            reference_number = r_data['reference_number']
            tx.reservation_reference = '%s~%s' % (theater_code, reference_number)
            tx.workspace['reservation_reference'] = reference_number
            tx.workspace['RSVP:claimdate'] = tx.workspace['schedules.show_datetime'][0].date()
            tx.workspace['RSVP:movie'] = tx.workspace['schedules.movie'][0].canonical_title
            tx.workspace['RSVP:fee'] = tx.workspace['convenience_fee']

            total_amount = str(tx.total_amount(tx.workspace['RSVP:fee'], True))
            total_seat_price = str(tx.total_seat_price(tx.workspace['RSVP:fee'], True))
            tx.workspace['RSVP:totalamount'] = total_amount
            tx.workspace['RSVP:totalseatprice'] = total_seat_price
        else:
            log.warn("ERROR!, Megaworld (%s) has no supported third-party system..." % theater_code)

            tx.error_info['error_msg'] = "Megaworld (%s) has no supported third-party system..." % theater_code
            return 'error', DEFAULT_ERROR_MESSAGE

        tx.bind_cancellation_callback(cancel_reservation)
        tx.put()
    except Exception, e:
        log.warn("ERROR!, do_reservation_megaworld (%s)..." % theater_code)
        log.error(e)
        # send email to GMovies support if exception errors are encountered
        send_email_support(tx, "ERROR in do_reservation (MGI): " + str(e))

        tx.error_info['error_msg'] = str(e)
        return 'error', DEFAULT_ERROR_MESSAGE

    return 'success', None
