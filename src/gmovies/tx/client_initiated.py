import logging
log = logging.getLogger(__name__)

import os
from datetime import datetime
from google.appengine.ext.ndb import Key, transactional
from google.appengine.ext import deferred

from gmovies.models import Listener, DATETIME_FORMAT
from gmovies.settings import CLIENT_INITIATED_REAP_TIMEOUT

from .scheduler import reschedule, cancel_reaping
from .query import query_reservation_reference
import state_machine

CHANNEL_NAME='payment-complete'
LISTENER_ID='$1$payment_completion_listener'

TIMEOUT = 1200 # 20 minutes

def listener_bind():
    version = os.environ['CURRENT_VERSION_ID'].split('.')
    major_version = version[0]
    if Listener.query(Listener.event_channel==CHANNEL_NAME,
                      Listener.version==major_version,
                      Listener.listener_id==LISTENER_ID).count() == 0:
        l = Listener()
        l.event_channel = CHANNEL_NAME
        l.version = major_version
        l.listener_id = LISTENER_ID

        l.callback = payment_completion_listener
        log.info("Bound listener for channel: %s", CHANNEL_NAME)
        l.put()

def bind_listeners(tx):
    log.debug("(deferred) Binding listener for TX %s", tx.key.id())
    deferred.defer(listener_bind)

def payment_completion_listener(outstanding):
    for e in outstanding:
        log.debug("Processing: %s", e)
        if e.payload:
            if 'reference' not in e.payload:
                continue
            reference = e.payload['reference']

            if 'claim-code' not in e.payload:
                continue
            claim_code = e.payload['claim-code']

            if 'claim-date' not in e.payload:
                continue
            claim_date = convert_date(e.payload['claim-date'])

            tx = query_reservation_reference(reference)
            if tx:
                log.debug("Completing: %s", tx.key.id())
                # Log response for error details
                tx.error_info['response_code'] = claim_code
                tx.error_info['response_message'] = reference
                tx.put()

                trigger_completion(tx, claim_code, claim_date)
                log.debug("Triggered completion")
                e.read = True

@transactional
def trigger_completion(tx, claim_code, claim_date):
    if tx.state != state_machine.state.TX_CLIENT_PAYMENT_HOLD:
        log.error("Attempt to complete TX %s, which is not in TX_CLIENT_PAYMENT_HOLD; callback in error? - tx.state: %s" % (tx.key.id(), tx.state))
        return

    tx.workspace['RSVP:claimcode'] = claim_code
    tx.workspace['RSVP:claimdate'] = claim_date
    cancel_reaping(tx)

    if tx.discount_info and tx.is_discounted:
        state_machine.transition_state(tx, state_machine.state.TX_CLIENT_PAYMENT_HOLD, state_machine.state.TX_FINALIZE)
    else:
        state_machine.transition_state(tx, state_machine.state.TX_CLIENT_PAYMENT_HOLD, state_machine.state.TX_GENERATE_TICKET)

    reschedule(tx)

def convert_date(d):
    return datetime.strptime(d, DATETIME_FORMAT)
