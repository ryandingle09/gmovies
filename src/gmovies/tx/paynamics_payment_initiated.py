import logging
import os

from datetime import datetime
from lxml import etree

from google.appengine.ext.ndb import Key, transactional
from google.appengine.ext import deferred

import state_machine

from gmovies.models import Listener, DATETIME_FORMAT

from .payment import (do_payment, is_smmalls_paynamics, is_robinsons_paynamics,
        is_cinema76_paynamics, is_globeevents_paynamics, do_finalize_transaction, WS_PAYMENT_ENGINE)
from .query import query_reservation_reference
from .scheduler import reschedule, cancel_reaping, cancel_requery


log = logging.getLogger(__name__)

CHANNEL_NAME ='paynamics-payment-complete'
LISTENER_ID ='$1$paynamics_payment_completion_listener'
TIMEOUT = 1320 # 22 minutes
TIMEOUT_RW_GH = 1500 # 25 minutes, to provide buffer for RW and GH callback
TIMEOUT_REQUERY = 1200 # 20 minutes


def listener_bind():
    version = os.environ['CURRENT_VERSION_ID'].split('.')
    major_version = version[0]

    if Listener.query(Listener.event_channel==CHANNEL_NAME,
            Listener.version==major_version,
            Listener.listener_id==LISTENER_ID).count() == 0:
        l = Listener()
        l.event_channel = CHANNEL_NAME
        l.version = major_version
        l.listener_id = LISTENER_ID
        l.callback = payment_completion_listener

        log.info("Bound listener for channel: %s", CHANNEL_NAME)

        l.put()

def bind_listeners(tx):
    log.debug("(deferred) Binding listener for TX %s", tx.key.id())

    deferred.defer(listener_bind)

def payment_completion_listener(outstanding):
    for e in outstanding:
        log.debug("Paynamics, payment_completion_listener, processing: %s...", e)

        if e.payload:
            if 'request_id' not in e.payload:
                log.warn("SKIP!, Paynamics, payment_completion_listener, missing key request_id...")

                continue

            theater_code = e.payload["theater_code"]
            request_id = e.payload['request_id']
            reservation_reference = theater_code + '~' + request_id
            paynamics_payment_reference = e.payload["response_id"]

            log.info("Paynamics, payment_completion_listener, reservation_reference: %s..." % reservation_reference)
            log.info("Paynamics, payment_completion_listener, paynamics_payment_reference: %s..." % paynamics_payment_reference)

            tx = query_reservation_reference(reservation_reference)

            if tx:
                log.debug("Appending Paynamics payment transaction_id to transaction payment_reference field...")

                # add cc tokenization details
                tx.payment_info['token_id'] = e.payload['token_id'] if 'token_id' in e.payload else ''
                tx.payment_info['token_info'] = e.payload['token_info'] if 'token_info' in e.payload else ''
                tx.payment_info['bin'] = e.payload['bin'] if 'bin' in e.payload else ''
                tx.payment_info['response_id'] = paynamics_payment_reference

                tx.workspace["response_code"] = e.payload['response_code'] if 'response_code' in e.payload else ''
                tx.workspace["response_message"] = e.payload['response_message'] if 'response_message' in e.payload else ''
                tx.payment_reference = paynamics_payment_reference
                # Log response for error details
                if hasattr(tx, 'error_info') and tx.error_info is not None:
                    tx.error_info['response_code'] = tx.workspace['response_code'] if 'response_code' in tx.workspace else ''
                    tx.error_info['response_message'] = tx.workspace['response_message'] if 'response_message' in tx.workspace else ''
                tx.put()

                log.debug("Paynamics, payment_completion_listener, completing: %s...", tx.key.id())

                trigger_completion(tx)

                log.debug("Paynamics, payment_completion_listener, triggered completion...")
            else:
                log.warn("SKIP!, Paynamics, payment_completion_listener, not found reservation_reference: %s..." % reservation_reference)

        e.read = True

@transactional
def trigger_completion(tx):
    if tx.state != state_machine.state.TX_CLIENT_PAYMENT_HOLD:
        log.error("Attempt to complete TX %s, which is not in TX_CLIENT_PAYMENT_HOLD; callback in error? - tx.state: %s" % (tx.key.id(), tx.state))

        # For Cinema 76 / Globe Events check if transaction can be refunded
        if (is_cinema76_paynamics(tx) or is_globeevents_paynamics(tx)) and tx.state == state_machine.state.TX_CANCELLED:
            if tx.workspace["response_code"] in ['GR001', 'GR002']:
                log.warn("trigger_completion, multiple callback, PaynamicsPayment, failed then success, for refund response_code: %s..." % (tx.workspace["response_code"]))

                log.debug('trigger_completion, for refund, triggering do_finalize_transaction for: %s' % (tx.key.id()))
                do_finalize_transaction(tx)

        # if transaction is in TX_CANCELLED state, trigger refund for GR013 Bank / Processor Timeout
        if tx.state == state_machine.state.TX_CANCELLED:
            if tx.workspace["response_code"] in ['GR013']:
                log.debug('trigger_completion, for refund, triggering do_reversal for: %s' % (tx.key.id()))
                engine = tx.workspace[WS_PAYMENT_ENGINE]
                if tx.workspace['discount::is_discounted']:
                    log.debug(engine.payment_engine)
                    engine = engine.payment_engine
                status, message = engine.do_reversal_tx(tx)
        return

    if is_smmalls_paynamics(tx) or is_robinsons_paynamics(tx) or is_cinema76_paynamics(tx) or is_globeevents_paynamics(tx):
        log.debug("trigger_completion, is_smmalls_paynamics / is_robinsons_paynamics / is_cinema76_paynamics, do cancel_requery...")

        cancel_requery(tx)

    cancel_reaping(tx)

    log.debug("trigger_completion, finalizing seat reservation...")

    state_machine.transition_state(tx, state_machine.state.TX_CLIENT_PAYMENT_HOLD, state_machine.state.TX_FINALIZE)

    log.debug("trigger_completion, rescheduling...")

    reschedule(tx)