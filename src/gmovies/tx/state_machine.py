import logging
import traceback

from google.appengine.ext.ndb import transactional
from google.appengine.api import memcache

import client_initiated
import ipay88_payment_initiated
import migs_payment_initiated
import paynamics_payment_initiated

from .query import query_info
from .payment import (do_payment, prepare_payment, is_client_initiated,
        is_powerplantmall_paynamics, is_powerplantmall_gcash, is_powerplantmall_promocode,
        is_greenhills_paynamics, is_greenhills_gcash, is_greenhills_promocode,
        is_cinema76_paynamics, is_cinema76_gcash, is_cinema76_promocode,
        is_globeevents_paynamics, is_globeevents_gcash, is_globeevents_promocode,
        is_smmalls_paynamics, is_smmalls_gcash, is_smmalls_promocode,
        # FOR ROB GCASH: add is_robinsons_gcash
        is_robinsons_paynamics, is_robinsons_promocode, is_robinsonsmalls_migs, is_robinsons_gcash,
        is_mgw_ipay88_payment, is_token_paynamics)
from .reservation import (prepare_reservation, do_reservation, do_reservation_powerplantmall,
        do_reservation_greenhills, do_reservation_cinema76, do_reservation_globeevents,
        do_reservation_smmalls, do_reservation_megaworld, do_reservation_robinsons)
from .ticket import do_ticket
from .scheduler import reschedule, schedule_for_reaping, schedule_for_requery
from .mutex import mutex_acquire, mutex_release
from .util import trigger_send_email_blasts_txdone, trigger_ticket_generation

from gmovies.admin import send_ticket_details, send_email_blasts_txdone
from gmovies.util import make_enum


log = logging.getLogger(__name__)
state = make_enum('state', TX_START=0, TX_PREPARE=1, TX_STARTED=2,
        TX_RESERVATION_HOLD=3, TX_RESERVATION_OK=4, TX_PAYMENT_HOLD=5,
        TX_GENERATE_TICKET=6, TX_CLIENT_PAYMENT_HOLD=7, TX_FINALIZE=8,
        TX_EXTERNAL_PAYMENT_HOLD=9, TX_FINALIZE_HOLD=10, TX_PREPARE_ERROR=13,
        TX_RESERVATION_ERROR=11, TX_PAYMENT_ERROR=12, TX_DONE=100,
        TX_CANCELLED=101, TX_REFUNDED=102, TX_STATE_ERROR=-1, TX_CANCELLED_CASH=103)

TX_TASK_MAX_RETRY=10


def to_state_str(s):
    if s not in state.reverse_mapping:
        return 'TX_UNKNOWN'

    return state.reverse_mapping[s]

def on_start(tx, params):
    log.info("Entering ON_START state...")
    tx_id = tx.key.id()
    log.info("TX %s\tstate TX_START\t\t Preparing transaction " % tx_id)
    transition_state(tx, state.TX_START, state.TX_PREPARE)
    r, msg = prepare_transaction(tx)
    reschedule(tx, r=r, msg=msg)

def on_started(tx, params):
    log.info("Entering ON_STARTED state...")
    tx_id = tx.key.id()
    log.info("on_started, transaction workspace: {}".format(tx.workspace))
    log.info("TX %s\tstate TX_STARTED\t\t Running reservation" % tx_id)
    transition_state(tx, state.TX_STARTED, state.TX_RESERVATION_HOLD)

    if is_powerplantmall_paynamics(tx) or is_powerplantmall_gcash(tx) or is_powerplantmall_promocode(tx):
        log.debug("on_started, do_reservation_powerplantmall...")

        r, msg = do_reservation_powerplantmall(tx)
    elif is_greenhills_paynamics(tx) or is_greenhills_gcash(tx) or is_greenhills_promocode(tx):
        log.debug("on_started, do_reservation_greenhills...")

        r, msg = do_reservation_greenhills(tx)
    elif is_cinema76_paynamics(tx) or is_cinema76_gcash(tx) or is_cinema76_promocode(tx):
        log.debug("on_started, do_reservation_cinema76...")

        r, msg = do_reservation_cinema76(tx)
    elif is_globeevents_paynamics(tx) or is_globeevents_gcash(tx) or is_globeevents_promocode(tx):
        log.debug("on_started, do_reservation_globeevents...")

        r, msg = do_reservation_globeevents(tx)
    elif is_smmalls_paynamics(tx) or is_smmalls_gcash(tx) or is_smmalls_promocode(tx):
        log.debug("on_started, do_reservation_smmalls...")

        r, msg = do_reservation_smmalls(tx)
    # FOR ROB GCASH: add is_robinsons_gcash
    elif is_robinsons_paynamics(tx) or is_robinsons_promocode(tx) or is_robinsonsmalls_migs(tx) or is_robinsons_gcash(tx):
        log.debug("on_started, do_reservation_robinsons...")

        r, msg = do_reservation_robinsons(tx)
    elif is_mgw_ipay88_payment(tx):
        log.debug("on_started, do_reservation_megaworld...")

        r, msg = do_reservation_megaworld(tx)
    else:
        log.debug("on_started, do_reservation...")

        r, msg = do_reservation(tx)

    tx.put()
    log.info("Leaving ON_STARTED state...")
    log.info("DO RESERVATION STATUS: {0} MSG: {1}".format(r, msg))
    reschedule(tx, r=r, msg=msg)

def on_reservation_ok(tx, params):
    log.info("Entering ON_RESERVATION_OK state...")
    tx_id = tx.key.id()
    log.info("TX %s\tstate TX_RESERVATION_OK\t\t Running payment" % tx_id)

    if not is_token_paynamics(tx):
        if is_client_initiated(tx):
            log.info("TX %s\t\t\tTransaction flagged for client-initiated payment (Ayala Malls)..." % tx_id)
            transition_state(tx, state.TX_RESERVATION_OK, state.TX_CLIENT_PAYMENT_HOLD)
            tx.workspace['msg'] = tx.reservation_reference
            client_initiated.bind_listeners(tx)
            schedule_for_reaping(tx, client_initiated.TIMEOUT)
            log.info("Leaving ON_RESERVATION_OK state...")

            return
        elif is_powerplantmall_paynamics(tx) or is_greenhills_paynamics(tx):
            log.info("TX %s\t\t\tTransaction flagged for Paynamics payment (Power Plant Mall and Greenhills Malls)..." % tx_id)
            transition_state(tx, state.TX_RESERVATION_OK, state.TX_CLIENT_PAYMENT_HOLD)
            tx.workspace['msg'] = tx.reservation_reference
            paynamics_payment_initiated.bind_listeners(tx)
            schedule_for_reaping(tx, paynamics_payment_initiated.TIMEOUT_RW_GH)
            log.info("Leaving ON_RESERVATION_OK state...")

            return
        elif (is_smmalls_paynamics(tx) or is_robinsons_paynamics(tx) or
                is_cinema76_paynamics(tx) or is_globeevents_paynamics(tx)):
            log.info("TX %s\t\t\tTransaction flagged for Paynamics payment (SM Malls / Robinsons malls / Cinema 76 malls / Globe Events)..." % tx_id)
            transition_state(tx, state.TX_RESERVATION_OK, state.TX_CLIENT_PAYMENT_HOLD)
            tx.workspace['msg'] = tx.reservation_reference
            paynamics_payment_initiated.bind_listeners(tx)
            schedule_for_reaping(tx, paynamics_payment_initiated.TIMEOUT)
            schedule_for_requery(tx, paynamics_payment_initiated.TIMEOUT_REQUERY)
            log.info("Leaving ON_RESERVATION_OK state...")

            return
        elif is_robinsonsmalls_migs(tx):
            log.info("TX %s\t\t\tTransaction flagged for MIGS payment (Robinsons malls)..." % tx_id)
            transition_state(tx, state.TX_RESERVATION_OK, state.TX_CLIENT_PAYMENT_HOLD)
            tx.workspace['msg'] = tx.reservation_reference
            migs_payment_initiated.bind_listeners(tx)
            schedule_for_reaping(tx, migs_payment_initiated.TIMEOUT)
            schedule_for_requery(tx, migs_payment_initiated.TIMEOUT_REQUERY)
            log.info("Leaving ON_RESERVATION_OK state...")

            return
        elif is_mgw_ipay88_payment(tx):
            log.info("TX %s\t\t\tTransaction flagged for iPay88 payment" % tx_id)
            transition_state(tx, state.TX_RESERVATION_OK, state.TX_EXTERNAL_PAYMENT_HOLD)
            tx.workspace['msg'] = tx.reservation_reference
            ipay88_payment_initiated.bind_listeners(tx)
            schedule_for_reaping(tx, ipay88_payment_initiated.TIMEOUT)
            log.info("Leaving ON_RESERVATION_OK state...")

            return

    transition_state(tx, state.TX_RESERVATION_OK, state.TX_PAYMENT_HOLD)
    r, msg = do_payment(tx)
    tx.put()
    log.info("Leaving ON_RESERVATION_OK state...")
    reschedule(tx, r=r, msg=msg)

def on_finalize(tx, params):
    log.info("Entering ON_FINALIZE state...")
    tx_id = tx.key.id()
    log.info("TX %s\tstate TX_FINALIZE\t\t Finalizing transaction" % tx_id)

    if not is_token_paynamics(tx):
        if (is_powerplantmall_paynamics(tx) or is_greenhills_paynamics(tx) or
                is_cinema76_paynamics(tx) or is_globeevents_paynamics(tx) or
                is_smmalls_paynamics(tx) or is_robinsons_paynamics(tx) or is_robinsonsmalls_migs(tx) or
                is_client_initiated(tx) or is_mgw_ipay88_payment(tx)):
            log.info("TX %s\t\t\tTransaction flagged for external transaction finalization" % tx_id)
            transition_state(tx, state.TX_FINALIZE, state.TX_FINALIZE_HOLD)
            r, msg = do_payment(tx)
            tx.put()
            log.info("Leaving ON_FINALIZE state...")
            reschedule(tx, r=r, msg=msg)

def on_finalize_hold(tx, params):
    log.info("Entering ON_FINALIZE_HOLD state...")
    tx_id = tx.key.id()
    log.info("TX %s\tstate TX_FINALIZE_HOLD\t\t Finalization hold " % tx_id)

    if not is_token_paynamics(tx):
        if (is_powerplantmall_paynamics(tx) or is_greenhills_paynamics(tx) or
                is_cinema76_paynamics(tx) or is_globeevents_paynamics(tx) or
                is_smmalls_paynamics(tx) or is_robinsons_paynamics(tx) or is_robinsonsmalls_migs(tx) or
                is_client_initiated(tx) or is_mgw_ipay88_payment(tx)):
            log.info("TX %s\t\t\tTransaction held for external transaction finalization" % tx_id)
            transition_state(tx, state.TX_FINALIZE_HOLD, state.TX_GENERATE_TICKET)
            log.info("Leaving ON_FINALIZE_HOLD state...")
            reschedule(tx)

def on_ticket_generate(tx, params):
    log.info("Entering ON_TICKET_GENERATE state...")
    tx_id = tx.key.id()
    log.debug("TX %s\tstate TX_GENERATE_TICKET\t\t Generating ticket" % tx_id)
    do_ticket(tx)
    transition_state(tx, state.TX_GENERATE_TICKET, state.TX_DONE)
    log.info("Leaving ON_TICKET_GENERATE state...")
    reschedule(tx)

def on_done(tx, params):
    log.info("Entering ON_DONE state...")
    tx_id = tx.key.id()

    robinsons_flag = False # needed for checking if email will be sent to client
    # FOR ROB GCASH: add is_robinsons_gcash
    if is_robinsons_paynamics(tx) or is_robinsons_promocode(tx) or is_robinsonsmalls_migs(tx) or is_robinsons_gcash(tx):
        robinsons_flag = True

    if tx.payment_type == 'globe-promo':
        message = None

        if 'msg' in tx.workspace:
            message = tx.workspace['msg']

        log.debug("TX %s\tstate TX_DONE\t\t Deleting workspace" % tx_id)
        tx.workspace = {}

        if message:
            log.debug("TX %s\tstate TX_DONE\t\t Appending message" % tx_id)
            tx.workspace['msg'] = message

        # sending ticket details via email.
        # COMMENTED since email blast is already being sent - 01/13/2017
        # try:
            # if 'email' in tx.payment_info:
                # log.info("TX %s, Sending email..." % tx_id)
                # send_ticket_details(tx.payment_info['email'], tx.ticket)
        # except Exception, e:
            # log.warn("ERROR, TX %s, failed on sending email..." % tx_id)
            # log.error(e)
    else:
        log.debug("TX %s\tstate TX_DONE\t\t Deleting workspace" % tx_id)
        tx.workspace = {}

    # sending email blast for successful transactions.
    try:
        # GMovies API will send the email
        email_status, email_return = trigger_send_email_blasts_txdone(tx)
        if 1 != email_status:
            log.warn("ERROR, TX %s, failed on trigger_send_email_blasts_txdone, status: %s, message: %s..." % (tx_id, email_status, email_return))

        # Commented for now, GMovies API will send email with badge details.
        # if not robinsons_flag: # don't send email for Robinsons Malls
            # if 'email' in tx.user_info:
                # log.info("TX %s, sending email blast..." % tx_id)
                # first_name = tx.user_info.get('first_name', "")
                # send_email_blasts_txdone(tx.user_info['email'], first_name, tx.platform, tx.ticket)

        # if g-cash-app trigger ticket generation
        if 'g-cash-app' == tx.payment_type:
            ticket_status, ticket_return = trigger_ticket_generation(tx)
            if 'error' == ticket_status:
                log.warn("ERROR, TX %s, failed on trigger_ticket_generation, status: %s, message: %s..." % (tx_id, ticket_status, ticket_return))

    except Exception, e:
        log.warn("ERROR, TX %s, failed on sending email blast..." % tx_id)
        log.error(traceback.format_exc(e))

    log.debug("TX %s\tstate TX_DONE\t\t Ensuring payment info is elided" % tx_id)
    tx.elide_payment_details()
    log.info("Leaving ON_DONE state...")


HOLD_NEXT_STATES = {
    state.TX_PREPARE: (state.TX_STARTED, state.TX_PREPARE_ERROR),
    state.TX_RESERVATION_HOLD: (state.TX_RESERVATION_OK, state.TX_RESERVATION_ERROR),
    state.TX_PAYMENT_HOLD: (state.TX_GENERATE_TICKET, state.TX_PAYMENT_ERROR),
    state.TX_FINALIZE: (state.TX_FINALIZE_HOLD, state.TX_PAYMENT_ERROR),
    state.TX_FINALIZE_HOLD: (state.TX_GENERATE_TICKET, state.TX_PAYMENT_ERROR),
}


def bind_on_hold(which):
    new_success_state, new_error_state = HOLD_NEXT_STATES[which]

    def on_hold(tx, params):
        tx_id = tx.key.id()
        ret = params['r']

        if ret == 'success':
            log.info("TX %s\tstate %s\t\t Success" % (tx_id, which))

            transition_state(tx, which, new_success_state)
            reschedule(tx)
        else:
            msg = params['msg']
            log.info("TX %s\tstate %s\t\t Error %s" % (tx_id, which, msg))
            tx.workspace['msg'] = msg
            transition_state(tx, which, new_error_state)
            schedule_for_reaping(tx)

    return on_hold


state_handlers = {
    state.TX_START: on_start,
    state.TX_PREPARE: bind_on_hold(state.TX_PREPARE),
    state.TX_STARTED: on_started,
    state.TX_PAYMENT_HOLD: bind_on_hold(state.TX_PAYMENT_HOLD),
    state.TX_RESERVATION_OK: on_reservation_ok,
    state.TX_RESERVATION_HOLD: bind_on_hold(state.TX_RESERVATION_HOLD),
    state.TX_FINALIZE: on_finalize,
    state.TX_FINALIZE_HOLD: bind_on_hold(state.TX_FINALIZE_HOLD),
    state.TX_GENERATE_TICKET: on_ticket_generate,
    state.TX_DONE: on_done,
}


def current_state(tx):
    return tx.state

def transition_state(tx, current_state, new_state):
    tx_id = tx.key.id()

    if tx.state == current_state or not current_state:
        log.info("TX_ID: {} -- State movement from {} to {}".format(tx_id, current_state, new_state))
        tx.state = new_state
        tx.previous_state = current_state
        tx.ver += 1
        tx.put()
    else:
        log.warn("Transitioning to an older state; did a transaction fail? (current: %s, expected: %s)" % (to_state_str(tx.state), to_state_str(current_state)))

def next_state(params):
    device_id = params['device_id']
    tx_id = params['tx_id']

    mutex_acquire(tx_id)

    try:
        state = __in_transaction_next_state(device_id, tx_id, params)
    finally:
        mutex_release(tx_id)

    log.info("next_state on Device %s -> TX %s => %s", device_id, tx_id, to_state_str(state))

    return state

@transactional(retries=0)
def __in_transaction_next_state(device_id, tx_id, params):
    retry = int(params.get('retry', '0'))
    tx_ver = int(params['tx_ver'])

    if retry >= TX_TASK_MAX_RETRY:
        log.warn("TX %s on device %s reached max retry; bailing.", tx_id, device_id)

        return None

    tx = query_info(device_id, tx_id)
    current_tx_state = current_state(tx)

    if current_tx_state == state.TX_CANCELLED:
        log.info("TX %s\tstate %s\t\t Transaction was cancelled" % (tx_id, to_state_str(current_tx_state)))

        return current_tx_state

    if tx_ver < tx.ver:
        log.warn("TX %s\tstate %s\t\t Retried for spurious transaction failure; exiting", tx_id, to_state_str(current_tx_state))

        return current_tx_state

    if current_tx_state not in state_handlers:
        if tx_ver > tx.ver:
            log.warn("TX %s\tstate %s\t Txaction was scheduled! Shouldn't be at this state" % (tx_id, to_state_str(current_tx_state)))
            log.warn("Might be due to datastore contention; intentionally rescheduling transaction (incrementing retry)")

            reschedule(tx, retry=str(retry+1))
        else:
            log.fatal("TX %s: Should not have been scheduled in this state: %s", tx_id, to_state_str(current_tx_state))

            assert current_tx_state in state_handlers, 'Unexpected state'
    else:
        try:
            state_handlers[current_tx_state](tx, params)
        except Exception, e:
            log.warn("Exception occurred in state_handler, rescheduling transaction tasklet")
            log.exception(e)

            # Reset state to current_tx_state (we need to do this,
            # also because we cache state in memcache, which doesn't
            # participate in the transaction)
            transition_state(tx, None, current_tx_state)

            raise e

    return current_tx_state

def prepare_transaction(tx):
    r, msg = prepare_reservation(tx)

    if r == 'success':
        log.info("Reservation Preparation Success --> Preparing Payment")
        r, msg = prepare_payment(tx)

    tx.put()
    return r, msg
