# Greenhills-related methods

import logging
log = logging.getLogger(__name__)

import requests
import json
from flask import jsonify
from gmovies.settings import GREENHILLS_ACCESS_TOKEN

def create_greenhills_request(endpoint, params):
    str_params = json.dumps(params)
    log.info('Endpoint: {0} Params: {1}'.format(endpoint, str_params))
    headers = {'Content-type': 'application/json', 'Authorization': 'Bearer ' + GREENHILLS_ACCESS_TOKEN}
    r = requests.Request('POST', url=endpoint, headers=headers, data=str_params).prepare()
    return r

def translate_error(error_code, error_message):
    log.info("Error message response: {}".format(error_message))

    if error_code == 1000:
        return "Could not process your request."
    elif error_code == 1001:
        return "Invalid data. Could not process your request."
    elif error_code == 1002:
        return "Invalid reservation status. Could not process your request."
    elif error_code == 1003:
        return "An empty request body is sent. Could not process your request."
    elif error_code == 1004:
        return "Invalid data format. Could not process your request."
    elif error_code == 1005:
        return "Missing Content-type header. Could not process your request."
    elif error_code == 1006:
        return "Required field(s) missing. Please check your input(s)."
    elif error_code == 1007:
        return "Movie schedule does not exist. Could not process your request."
    elif error_code == 1009:
        return "The seat is not available and is already taken. Could not process your request."
    elif error_code == 1010:
        return "Movie is inactive. Could not process your request."
    elif error_code == 1011:
        return "Requested seat is not available for this cinema. Could not process your request."
    elif error_code == 1012:
        return "No movie poster found."
    elif error_code == 1013:
        return "No movie trailer found."
    elif error_code == 1014:
        return "Seat is not available. Could not process your request."
    elif error_code == 1015:
        return "Lock for these seats had already expired. Could not process your request."
    elif error_code == 1016:
        return "Invalid transaction. Could not process your request."
    else:
        return "Unknown error. Could not process your request."