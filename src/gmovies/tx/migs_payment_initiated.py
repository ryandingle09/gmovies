import logging
import os

from datetime import datetime

from google.appengine.ext.ndb import Key, transactional
from google.appengine.ext import deferred

import state_machine

from gmovies.models import Listener, DATETIME_FORMAT

from .payment import do_payment, is_robinsonsmalls_migs
from .query import query_reservation_reference
from .scheduler import reschedule, cancel_reaping, cancel_requery


log = logging.getLogger(__name__)

CHANNEL_NAME ='migs-payment-complete'
LISTENER_ID ='$1$migs_payment_completion_listener'
TIMEOUT = 1200 # 20 minutes
TIMEOUT_REQUERY = 1080 # 18 minutes


def listener_bind():
    version = os.environ['CURRENT_VERSION_ID'].split('.')
    major_version = version[0]

    if Listener.query(Listener.event_channel==CHANNEL_NAME, Listener.listener_id==LISTENER_ID,
            Listener.version==major_version).count() == 0:
        l = Listener()
        l.event_channel = CHANNEL_NAME
        l.listener_id = LISTENER_ID
        l.version = major_version
        l.callback = payment_completion_listener

        log.info("MIGS, listener_bind, bound listener for channel: %s...", CHANNEL_NAME)

        l.put()

def bind_listeners(tx):
    log.debug("MIGS, bind_listeners, (deferred) binding listener for TX, %s...", tx.key.id())

    deferred.defer(listener_bind)

def payment_completion_listener(outstanding):
    for e in outstanding:
        log.debug("MIGS, payment_completion_listener, processing: %s...", e)

        if e.payload:
            if 'vpc_MerchTxnRef' not in e.payload:
                log.warn("SKIP!, MIGS, payment_completion_listener, missing key vpc_MerchTxnRef...")

                continue

            payment_reference_list = []

            if 'vpc_TransactionNo' in e.payload and 'vpc_ReceiptNo' in e.payload:
                payment_reference_list = [e.payload['vpc_TransactionNo'], e.payload['vpc_ReceiptNo']]

            reservation_reference = e.payload['vpc_MerchTxnRef']
            migs_payment_reference = '-'.join(payment_reference_list)

            log.info("MIGS, payment_completion_listener, reservation_reference: %s..." % reservation_reference)
            log.info("MIGS, payment_completion_listener, migs_payment_reference: %s..." % migs_payment_reference)

            tx = query_reservation_reference(reservation_reference)

            if tx:
                log.debug("MIGS, appending MIGS payment transactionNo and receiptNo to transaction payment_reference field...")

                tx.workspace['response_code'] = e.payload['vpc_TxnResponseCode'] if 'vpc_TxnResponseCode' in e.payload else ''
                tx.workspace['response_message'] = e.payload['vpc_Message'] if 'vpc_Message' in e.payload else ''
                tx.workspace['secure_hash'] = e.payload['vpc_SecureHash'] if 'vpc_SecureHash' in e.payload else ''
                tx.workspace['vpc_command'] = e.payload['vpc_Command'] if 'vpc_Command' in e.payload else ''
                tx.workspace['response_parameters'] = e.payload if e.payload else {}
                tx.payment_reference = migs_payment_reference
                # Log response for error details
                tx.error_info['response_code'] = tx.workspace['response_code'] if 'response_code' in tx.workspace else ''
                tx.error_info['response_message'] = tx.workspace['response_message'] if 'response_message' in tx.workspace else ''
                tx.put()

                log.debug("MIGS, payment_completion_listener, completing: %s...", tx.key.id())

                trigger_completion(tx)

                log.debug("MIGS, payment_completion_listener, triggered completion...")
            else:
                log.warn("SKIP!, MIGS, payment_completion_listener, not found reservation_reference: %s..." % reservation_reference)

        e.read = True

@transactional
def trigger_completion(tx):
    if tx.state != state_machine.state.TX_CLIENT_PAYMENT_HOLD:
        log.warn("MIGS, attempt to complete TX %s, which is not in TX_CLIENT_PAYMENT_HOLD; callback in error? - tx.state: %s" % (tx.key.id(), tx.state))

        return

    if is_robinsonsmalls_migs(tx):
        log.debug("trigger_completion, is_robinsonsmalls_migs, do cancel_requery...")

        cancel_requery(tx)

    cancel_reaping(tx)

    log.debug("MIGS, trigger_completion, finalizing seat reservation...")

    state_machine.transition_state(tx, state_machine.state.TX_CLIENT_PAYMENT_HOLD, state_machine.state.TX_FINALIZE)

    log.debug("MIGS, trigger_completion, rescheduling...")

    reschedule(tx)
