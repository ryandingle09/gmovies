# QR Code Generator

from __future__ import absolute_import

from StringIO import StringIO

from qrcode import QRCode, constants


def generate_ticket_qrcode(tx, version=1, box_size=10, border=1,
        is_multiple_tickets=False, seat_tx_code=None):
    ticket_qrcode = StringIO()
    ticket_code = tx.ticket.code
    qrcode_gen = QRCode(version=version,
            error_correction=constants.ERROR_CORRECT_L,
            box_size=box_size, border=border)

    if seat_tx_code and seat_tx_code is not None:
        ticket_code = seat_tx_code

    qrcode_gen.add_data(ticket_code)
    qrcode_gen.make(fit=True)
    qrcode_image = qrcode_gen.make_image()
    qrcode_image.save(ticket_qrcode)
    tx.ticket.qrcode = ticket_qrcode.getvalue()
    tx.ticket.qrcode_list.append(tx.ticket.qrcode)


def generate_sample_ticket_qrcode(ticket_code, version=1, box_size=10, border=1):
    ticket_qrcode = StringIO()
    qrcode_gen = QRCode(version=version,
            error_correction=constants.ERROR_CORRECT_L,
            box_size=box_size, border=border)
    qrcode_gen.add_data(ticket_code)
    qrcode_gen.make(fit=True)
    qrcode_image = qrcode_gen.make_image()
    qrcode_image.save(ticket_qrcode)

    return ticket_qrcode.getvalue()
