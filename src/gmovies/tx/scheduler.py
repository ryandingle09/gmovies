import logging
log = logging.getLogger(__name__)

import os
from google.appengine.api import taskqueue
from flask import url_for

from gmovies.settings import REAP_TIMEOUT

def reschedule(tx, **kwargs):
    tx_id = tx.key.id()
    device_id = tx.key.parent().id()
    params = dict(tx_id=tx_id, device_id=device_id, tx_ver=str(tx.ver), **kwargs)
    version = os.environ['CURRENT_VERSION_ID'].split('.')
    major_version = version[0]

    log.info("reschedule, rescheduling transaction with params: {}...".format(params))

    taskqueue.add(url=url_for('task.process_tx'), target=major_version, transactional=True, queue_name=tx.get_queue(), params=params)

def schedule_for_reaping(tx, timeout=REAP_TIMEOUT):
    tx_id = tx.key.id()
    device_id = tx.key.parent().id()
    params = {'tx_id': tx_id, 'device_id': device_id}
    task = taskqueue.add(url=url_for('task.reap_tx'), params=params, queue_name=tx.get_queue(), transactional=True, countdown=timeout)

    log.info("schedule_for_reaping, scheduled for reaping, TX: {0} DEVICE: {1} Countdown: {2}...".format(tx_id, device_id, timeout))

    tx.workspace['reap_task'] = task
    tx.put()

    return task

def schedule_for_requery(tx, timeout=1200):
    log.debug("schedule_for_requery, scheduling requery...")

    tx_id = tx.key.id()
    device_id = tx.key.parent().id()
    params = {'tx_id': tx_id, 'device_id': device_id}
    task = taskqueue.add(url=url_for('task.requery_tx'), params=params, queue_name=tx.get_queue_requery(), transactional=True, countdown=timeout)
    tx.workspace['requery_task'] = task
    tx.put()

    log.debug("schedule_for_requery, do requery, TX: {0}, DEVICE: {1}, COUNTDOWN: {2}...".format(tx_id, device_id, timeout))

    return task

def cancel_reaping(tx):
    if 'reap_task' not in tx.workspace:
        log.warn("SKIP!, cancel_reaping, key reap_task not in tx.workspace...")

        return

    log.info("cancel_reaping, reaping cancelled for tx %s..." % tx.key.id())

    task = tx.workspace['reap_task']
    queue = taskqueue.Queue(name=tx.get_queue())
    queue.delete_tasks(task)

    del tx.workspace['reap_task']

    tx.put()

def cancel_requery(tx):
    log.debug("cancel_requery, requery cancelled for tx, %s..." % tx.key.id())

    if 'requery_task' not in tx.workspace:
        log.warn("ERROR!, cancel_requery, key requery_task not in tx.workspace...")

        return

    task = tx.workspace['requery_task']

    if task:
        queue = taskqueue.Queue(name=tx.get_queue_requery())
        queue.delete_tasks(task)

    del tx.workspace['requery_task']

    tx.put()
