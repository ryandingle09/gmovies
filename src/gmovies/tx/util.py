import logging
import pickle
import requests
import json
import time
import traceback

from google.appengine.api import memcache
from gmovies.exceptions.api import GMoviesCancelFailedException
from gmovies.settings import GMOVIES_DV_GLOBAL_SERVER, REFUND_TOKEN, CLAIMCODE_GLOBAL_SERVER

from gmovies.settings import IS_STAGING
from gmovies.util import admin

if IS_STAGING:
    SURESEATS_FREE_SEATS_ENDPOINT = 'https://globe-sureseats-staging-169504.appspot.com/api/0/sureseats-gmovies/cancel_transaction_by_claim_code/'
else:
    SURESEATS_FREE_SEATS_ENDPOINT = 'https://globe-sureseats.appspot.com/api/0/sureseats-gmovies/cancel_transaction_by_claim_code/'

log = logging.getLogger(__name__)
DEFAULT_ERROR_MESSAGE = 'Sorry, an error occurred. Please try re-launching the app or contact support@gmovies.ph.'

EMAIL_TXDONE_TOKEN_ENDPOINT = '%s/v1/token/get' % GMOVIES_DV_GLOBAL_SERVER
EMAIL_TXDONE_ENDPOINT       = '%s/v1/email/ticket' % GMOVIES_DV_GLOBAL_SERVER
REPORT_TXDONE_ENDPOINT      = '%s/v1/transaction/save' % GMOVIES_DV_GLOBAL_SERVER
REFUND_ENDPOINT             = '%s/v1/transaction/refund' % GMOVIES_DV_GLOBAL_SERVER
SMS_SEND_ENDPOINT           = '%s/gmovies/sms/send' % CLAIMCODE_GLOBAL_SERVER
TICKET_GENERATION_ENDPOINT  = '%s/v1/ticket/generate/' % GMOVIES_DV_GLOBAL_SERVER
TAG_REFUND_ENDPOINT         = '%s/v1/refund/<tx_id>/tag' % GMOVIES_DV_GLOBAL_SERVER
TIMEOUT_DEADLINE            = 60
EMAIL_MAX_RETRY             = 3
EMAIL_RETRY_SLEEP           = 2 # retry sending email after n seconds
SMS_TX_FAILED_NOTIFICATION  = "Sorry, your transaction has failed. Please try again. For more concerns, send us a message at support@gmovies.ph."
LBAPITOKEN                  = 'Mc78beJQcwdEbWWcBqkF4yT7gQ38eWVH2Ws5VeUJT6jEpuGYmYDj4jYAwCT3mjpRr6QW2b6S2VZsS7QzuMcJPhzF'
REFUND_TAG_TOKEN            = 'tDO5kVMtMDJWDDjHAMp500U9nD86zQDkGdBPlDuxY3Lp3FDyytxp6hq0xepKsuX4qGq14U2i4vQ92xBysZdMB0J6'

def call_once(fn, tx, *args, **kwargs):
    idemp_key = "tx(%s|%s)::call=%s" % (tx.key.id(), tx.ver, fn_name(fn))
    retval = memcache.get(idemp_key)

    log.info("CALL ONCE: IDEMP-KEY:%s RETVAL:%s ARGS: %s KWARGS: %s CALLING-FUNCTION:%s", idemp_key, retval, args, kwargs, fn)

    if not retval:
        retval = fn(*args, **kwargs)
        memcache.add(idemp_key, retval)

    return retval

def fn_name(fn):
    return fn.__name__

def translate_message_via_error_code(namespace, error_code, error_message):
    log.debug("translate_message_via_error_code, start...")
    log.info("namespace: %s..." % namespace)
    log.info("error_code: %s..." % error_code)
    log.info("error_message: %s..." % error_message)

    messages = {}

    # Ayala Malls
    messages['ayala-malls'] = {}
    messages['ayala-malls']['SEATSTAKEN'] = 'Sorry, selected seat/s have been taken. Please make another selection.'
    messages['ayala-malls']['INSUFFICIENTMPASSCREDITS'] = 'You have insufficient M-Pass load. Please reload or select another payment method.'
    messages['ayala-malls']['SCHEDULENOTFOUND'] = 'Sorry, this schedule has been made unavailable. Please select a different schedule.'

    # Rockwell (Power Plant Mall)
    messages['power-plant-mall'] = {}
    messages['power-plant-mall']['1009'] = 'Sorry, selected seat/s have been taken. Please make another selection.'

    # Greenhills Malls
    messages['greenhills-malls'] = {}
    messages['greenhills-malls']['1009'] = 'Sorry, selected seat/s have been taken. Please make another selection.'

    # Cinema76 Malls
    messages['cinema76-malls'] = {}
    messages['cinema76-malls']['1025'] = 'Sorry, this schedule has been made unavailable. Please select a different schedule.'
    messages['cinema76-malls']['NOTENOUGHSEATS'] = 'Sorry, not enough seats available. Please make another selection.'

    # Globe Events
    messages['globe-events'] = {}
    messages['globe-events']['1025'] = 'Sorry, but the number of tickets you are booking exceeds the number of available slots.'
    messages['globe-events']['NOTENOUGHSEATS'] = 'Sorry, but the number of tickets you are booking exceeds the number of available slots.'

    # SM Malls
    messages['sm-malls'] = {}
    messages['sm-malls']['SEATSTAKEN'] = 'Sorry, selected seat/s have been taken. Please make another selection.'

    # Robinsons Malls
    messages['robinsons-malls'] = {}
    messages['robinsons-malls']['SEATSTAKEN'] = 'Sorry, selected seat/s have been taken. Please make another selection.'
    messages['robinsons-malls']['ISCINEMAHOUSESEAT'] = error_message

    # Megaworld Malls
    messages['megaworld-malls'] = {}
    messages['megaworld-malls']['SEATSTAKEN'] = 'Sorry, selected seat/s have been taken. Please make another selection.'

    # Paynamics
    messages['paynamics'] = {}
    messages['paynamics']['FR017'] = error_message
    messages['paynamics']['GR004'] = error_message
    messages['paynamics']['GR005'] = error_message
    messages['paynamics']['GR006'] = error_message
    messages['paynamics']['GR008'] = error_message
    messages['paynamics']['GR010'] = error_message
    messages['paynamics']['GR023'] = error_message
    messages['paynamics']['GR039'] = error_message
    messages['paynamics']['GR053'] = error_message

    # MIGS
    messages['migs'] = {}
    messages['migs']['?'] = 'Transaction status is unknown.'
    messages['migs']['E'] = 'Referred.'
    messages['migs']['1'] = 'Transaction Declined.'
    messages['migs']['2'] = 'Bank Declined Transaction.'
    messages['migs']['3'] = 'No Reply from Bank.'
    messages['migs']['4'] = 'Expired Card.'
    messages['migs']['5'] = 'Insufficient funds.'
    messages['migs']['6'] = 'Error Communicating with Bank.'
    messages['migs']['7'] = 'Payment Server detected an error.'
    messages['migs']['8'] = 'Transaction Type Not Supported.'
    messages['migs']['9'] = 'Bank declined transaction (Do not contact Bank).'
    messages['migs']['A'] = 'Transaction Aborted.'
    messages['migs']['B'] = 'Fraud Risk Blocked.'
    messages['migs']['C'] = 'Transaction Cancelled.'
    messages['migs']['D'] = 'Deferred transaction has been received and is awaiting processing.'
    messages['migs']['E'] = 'Transaction Declined - Refer to card issuer.'
    messages['migs']['F'] = '3D Secure Authentication failed.'
    messages['migs']['I'] = 'Card Security Code verification failed.'
    messages['migs']['L'] = 'Shopping Transaction Locked (Please try the transaction again later).'
    messages['migs']['M'] = 'Transaction Submitted (No response from acquirer).'
    messages['migs']['N'] = 'Cardholder is not enrolled in Authentication scheme.'
    messages['migs']['P'] = 'Transaction has been received by the Payment Adaptor and is being processed.'
    messages['migs']['R'] = 'Transaction was not processed - Reached limit of retry attempts allowed.'
    messages['migs']['S'] = 'Duplicate SessionID (Amex Only).'
    messages['migs']['T'] = 'Address Verification Failed.'
    messages['migs']['U'] = 'Card Security Code Failed.'
    messages['migs']['V'] = 'Address Verification and Card Security Code Failed.'
    # MIGS - AVS Result Description
    messages['migs']['AVS::Unsupported'] = 'AVS not supported or there was no AVS data provided.'
    messages['migs']['AVS::X'] = 'Exact match - address and 9 digit ZIP/postal code.'
    messages['migs']['AVS::Y'] = 'Exact match - address and 5 digit ZIP/postal code.'
    messages['migs']['AVS::W'] = '9 digit ZIP/postal code matched, Address not Matched.'
    messages['migs']['AVS::S'] = 'Service not supported or address not verified (international transaction).'
    messages['migs']['AVS::G'] = 'Issuer does not participate in AVS (international transaction).'
    messages['migs']['AVS::C'] = 'Street Address and Postal Code not verified for International Transaction due to incompatible formats.'
    messages['migs']['AVS::I'] = 'Visa Only. Address information not verified for international transaction.'
    messages['migs']['AVS::A'] = 'Address match only.'
    messages['migs']['AVS::Z'] = '5 digit ZIP/postal code matched, Address not Matched.'
    messages['migs']['AVS::R'] = 'Issuer system is unavailable.'
    messages['migs']['AVS::U'] = 'Address unavailable or not verified.'
    messages['migs']['AVS::E'] = 'Address and ZIP/postal code not provided.'
    messages['migs']['AVS::B'] = 'Street Address match for international transaction. Postal Code not verified due to incompatible formats.'
    messages['migs']['AVS::N'] = 'Address and ZIP/postal code not matched.'
    messages['migs']['AVS::O'] = 'AVS not requested.'
    messages['migs']['AVS::D'] = 'Street Address and postal code match for international transaction.'
    messages['migs']['AVS::M'] = 'Street Address and postal code match for international transaction.'
    messages['migs']['AVS::P'] = 'Postal Codes match for international transaction but street address not verified due to incompatible formats.'
    messages['migs']['AVS::K'] = 'Card holder name only matches.'
    messages['migs']['AVS::F'] = 'Street address and postal code match. Applies to U.K. only.'
    # MIGS - CSC Result Description
    messages['migs']['CSC::Unsupported'] = 'CSC not supported or there was no CSC data provided.'
    messages['migs']['CSC::M'] = 'Exact code match.'
    messages['migs']['CSC::S'] = 'Merchant has indicated that CSC is not present on the card (MOTO situation).'
    messages['migs']['CSC::P'] = 'Code not processed.'
    messages['migs']['CSC::U'] = 'Card issuer is not registered and/or certified.'
    messages['migs']['CSC::N'] = 'Code invalid or not matched.'

    # iPay88
    messages['ipay88'] = {}

    # Claim Code (GAE)
    messages['claim-code-gae'] = {}
    messages['claim-code-gae']['INVALIDCODES'] = error_message
    messages['claim-code-gae']['ERRORPARAMS'] = 'Claim code is not valid. Please try again or contact support.'
    messages['claim-code-gae']['CLAIMCODEINVALIDPROMO'] = 'Promo code is not applicable for Claim Code payment method. Please choose another payment method.'

    # Claim Code (Core Tool)
    messages['claim-code-coretool'] = {}

    # GCash
    messages['gcash'] = {}
    messages['gcash']['NOTREGISTERED'] = 'Your mobile number is not yet registered on GCash. Please select another payment method.'
    messages['gcash']['INSUFFICIENTFUNDS'] = 'You have insufficient GCash funds. Please cash-in or select another payment method.'

    # GRewards
    messages['grewards'] = {}
    messages['grewards']['NOTREGISTERED'] = 'Your mobile number is not yet registered on GRewards. Please select another payment method.'
    messages['grewards']['INSUFFICIENTFUNDS'] = 'You have insufficient GRewards Points. Please cash-in or select another payment method.'

    # MPass
    messages['mpass'] = {}
    messages['mpass']['INCORRECTUSERNAMEPASSWORD'] = 'Your username and/or password is incorrect. Please try again.'

    # EPlus
    messages['eplus'] = {}

    if namespace in messages:
        if str(error_code) in messages[namespace]:
            return messages[namespace][str(error_code)]

    return DEFAULT_ERROR_MESSAGE

def call_gmovies_api(s, req):
    r = s.send(req, timeout=TIMEOUT_DEADLINE)
    return r.status_code, r.text

def get_token():
    log.debug("get_token, URL: %s..." % EMAIL_TXDONE_TOKEN_ENDPOINT)
    r = requests.get(EMAIL_TXDONE_TOKEN_ENDPOINT)

    log.debug("get_token, status_code: %s..." % (r.status_code))
    log.debug("get_token, response: %s..." % (r.text))

    if r.status_code != 200:
        log.warn("ERROR!, get_token, status_code not equal to 200: %s..." % (r.status_code))
        return r.status_code, 'Status_code not equal to 200.'

    d_json = {}
    try:
        d_json = json.loads(r.text)
    except ValueError, e:
        log.warn("ERROR!, get_token, response not in json format...")
        return 'error', 'Response not in json format.'
    else:
        if 'data' not in d_json:
            log.warn("ERROR!, get_token, missing key 'data' in json response...")
            return 'error', "Missing key 'data' in json response."
        if 'token' not in d_json['data']:
            log.warn("ERROR!, get_token, missing key 'token' in json response...")
            return 'error', "Missing key 'token' in json response."
    return r.status_code, d_json['data']['token']

def trigger_send_email_blasts_txdone(tx):
    # get token
    token_status_code, token_return = get_token()
    if token_status_code != 200:
        log.warn("ERROR!, trigger_send_email_blasts_txdone, get_token, status_code not equal to 200: %s..." % (token_status_code))
        return token_status_code, token_return

    headers = {}
    form_data = {}

    headers['gmovies-token'] = token_return
    headers['Content-type']  = 'application/json'
    form_data['id'] = tx.key.id()
    form_data['transaction'] = tx.to_entity2()
    str_payload = json.dumps(form_data)
    log.debug("trigger_send_email_blasts_txdone, headers: {0}...".format(headers))
    log.debug("trigger_send_email_blasts_txdone, params: {0}...".format(form_data))

    log.debug("trigger_send_email_blasts_txdone, URL: %s..." % EMAIL_TXDONE_ENDPOINT)
    req = requests.Request('POST', url=EMAIL_TXDONE_ENDPOINT, headers=headers, data=str_payload).prepare()

    r_status_code = 0
    ctr = 0
    while r_status_code != 200 and ctr < EMAIL_MAX_RETRY:
        s = requests.Session()
        r_status_code, r_json = call_gmovies_api(s, req)
        log.debug("trigger_send_email_blasts_txdone, ctr: %s..." % (ctr))
        log.debug("trigger_send_email_blasts_txdone, status_code: %s..." % (r_status_code))
        log.debug("trigger_send_email_blasts_txdone, response: %s..." % (r_json))
        if r_status_code != 200:
            ctr += 1
            time.sleep(EMAIL_RETRY_SLEEP)

    if r_status_code != 200:
        log.warn("ERROR!, trigger_send_email_blasts_txdone, status_code not equal to 200: %s..." % (r_status_code))
        return r_status_code, 'Status_code not equal to 200.'

    d_json = {}
    try:
        d_json = json.loads(r_json)
    except ValueError, e:
        log.warn("ERROR!, trigger_send_email_blasts_txdone, response not in json format...")
        # return 'error', 'Response not in json format.'
    else:
        if 'status' not in d_json:
            log.warn("ERROR!, trigger_send_email_blasts_txdone, missing key 'status' in json response...")
            return 'error', "Missing key 'status' in json response."
        if 'message' not in d_json:
            log.warn("ERROR!, trigger_send_email_blasts_txdone, missing key 'message' in json response...")
            return 'error', "Missing key 'message' in json response."

    # send transaction details to gmovies dashboard -----------------------------------------------------------
    log.debug("send transaction details to gmovies dashboard")
    log.debug("endpoint : %s" % (REPORT_TXDONE_ENDPOINT))

    tx_record = []

    tx_ent = tx.to_entity_reports()
    tx_ent['merchant_name'] = admin.THEATER_ORGANIZATIONS[tx_ent['merchant_id']] if tx_ent['merchant_id'] in admin.THEATER_ORGANIZATIONS else ''

    tx_record.append(tx_ent)

    headers['gmovies-platform'] = tx.platform
    headers['gmovies-device-id'] = tx.key.parent().id()

    log.debug("form post payload: %s" % (json.dumps(tx_record[0])))
    log.debug("request header: %s" % (headers))

    req2 = requests.Request('POST', url=REPORT_TXDONE_ENDPOINT, headers=headers, data=json.dumps(tx_record[0])).prepare()
    s2 = requests.Session()
    r_status_code2, r_json2 = call_gmovies_api(s2, req2)

    log.debug("response from %s : %s" % (REPORT_TXDONE_ENDPOINT, r_json2))
    log.debug("response status code : %s" % (r_status_code2))

    return d_json['status'], d_json['message']

def trigger_refund(tx):
    # get token
    token_status_code, token_return = get_token()
    if token_status_code != 200:
        log.warn("ERROR!, trigger_refund, get_token, status_code not equal to 200: %s..." % (token_status_code))
        return token_status_code, token_return

    headers = {}
    form_data = {}

    headers['gmovies-token'] = token_return
    headers['refund-token'] = REFUND_TOKEN
    headers['Content-type']  = 'application/json'
    form_data['txn_id'] = tx.key.parent().id() + '~' + tx.key.id()
    form_data['ticket_code'] = tx.ticket.code if (hasattr(tx, 'ticket') and hasattr(tx.ticket, 'code')) else ''

    str_payload = json.dumps(form_data)
    log.debug("trigger_refund, headers: {0}...".format(headers))
    log.debug("trigger_refund, params: {0}...".format(form_data))

    log.debug("trigger_refund, URL: %s..." % REFUND_ENDPOINT)
    req = requests.Request('POST', url=REFUND_ENDPOINT, headers=headers, data=str_payload).prepare()

    s = requests.Session()
    r_status_code, r_json = call_gmovies_api(s, req)
    log.debug("trigger_refund, status_code: %s..." % (r_status_code))
    log.debug("trigger_refund, response: %s..." % (r_json))

    if r_status_code != 200:
        log.warn("ERROR!, trigger_refund, status_code not equal to 200: %s..." % (r_status_code))
        return r_status_code, 'Status_code not equal to 200.'

    d_json = {}
    try:
        d_json = json.loads(r_json)
    except ValueError, e:
        log.warn("ERROR!, trigger_refund, response not in json format...")
        return 'error', 'Response not in json format.'
    else:
        if 'status' not in d_json:
            log.warn("ERROR!, trigger_refund, missing key 'status' in json response...")
            return 'error', "Missing key 'status' in json response."
        if 'message' not in d_json:
            log.warn("ERROR!, trigger_refund, missing key 'message' in json response...")
            return 'error', "Missing key 'message' in json response."
    return d_json['status'], d_json['message']

def trigger_send_sms_notification(tx, message=None):
    headers = {}
    form_data = {}

    headers['LBAPIToken'] = LBAPITOKEN
    headers['Content-type']  = 'application/json'
    mobile = ''
    if 'mobile' in tx.payment_info:
        mobile = tx.payment_info['mobile']
    elif tx.user_info and 'mobile' in tx.user_info:
        mobile = tx.user_info['mobile']

    log.debug("trigger_send_sms_notification, mobile: %s, message: %s ..." % (mobile, message))
    if mobile is None or message is None:
        log.debug("trigger_send_sms_notification, mobile or message is None ...")
        return 'error', 'mobile or message parameter is None'

    form_data['mobile'] = mobile
    form_data['message'] = message
    form_data['send_by'] = 'gae'
    str_payload = json.dumps(form_data)
    log.debug("trigger_send_sms_notification, headers: {0}...".format(headers))
    log.debug("trigger_send_sms_notification, params: {0}...".format(form_data))

    log.debug("trigger_send_sms_notification, URL: %s..." % SMS_SEND_ENDPOINT)
    req = requests.Request('POST', url=SMS_SEND_ENDPOINT, headers=headers, data=str_payload).prepare()

    s = requests.Session()
    r_status_code, r_json = call_gmovies_api(s, req)
    log.debug("trigger_send_sms_notification, status_code: %s..." % (r_status_code))
    log.debug("trigger_send_sms_notification, response: %s..." % (r_json))

    if r_status_code != 200:
        log.warn("ERROR!, trigger_send_sms_notification, status_code not equal to 200: %s..." % (r_status_code))
        return r_status_code, 'Status_code not equal to 200.'

    d_json = {}
    try:
        d_json = json.loads(r_json)
    except ValueError, e:
        log.warn("ERROR!, trigger_send_sms_notification, response not in json format...")
        return 'error', 'Response not in json format.'
    else:
        if 'status' not in d_json:
            log.warn("ERROR!, trigger_send_sms_notification, missing key 'status' in json response...")
            return 'error', "Missing key 'status' in json response."
        if 'msg' not in d_json:
            log.warn("ERROR!, trigger_send_sms_notification, missing key 'msg' in json response...")
            return 'error', "Missing key 'msg' in json response."
    return d_json['status'], d_json['msg']

def trigger_ticket_generation(tx):
    # get token
    token_status_code, token_return = get_token()
    if token_status_code != 200:
        log.warn("ERROR!, trigger_ticket_generation, get_token, status_code not equal to 200: %s..." % (token_status_code))
        return token_status_code, token_return

    headers                      = {}
    headers['gmovies-token']     = token_return
    headers['gmovies-device-id'] = tx.key.parent().id()
    p_type                       = tx.payment_type

    if 'g-cash-app' == p_type:
        p_type = 'gcash'
    elif 'grewards' == p_type:
        p_type = 'grewards'
        
    headers['type']              = p_type
    log.debug("trigger_ticket_generation, headers: {0}...".format(headers))

    ticket_endpoint = TICKET_GENERATION_ENDPOINT + tx.key.id()
    log.debug("trigger_ticket_generation, URL: %s..." % ticket_endpoint)

    req = requests.Request('GET', url=ticket_endpoint, headers=headers).prepare()

    s = requests.Session()
    r_status_code, r_json = call_gmovies_api(s, req)
    log.debug("trigger_ticket_generation, status_code: %s..." % (r_status_code))
    log.debug("trigger_ticket_generation, response: %s..." % (r_json))

    if r_status_code != 200:
        log.warn("ERROR!, trigger_ticket_generation, status_code not equal to 200: %s..." % (r_status_code))
        return r_status_code, 'Status_code not equal to 200.'

    d_json = {}
    try:
        d_json = json.loads(r_json)
    except ValueError, e:
        log.warn("ERROR!, trigger_ticket_generation, response not in json format...")
        return 'error', 'Response not in json format.'
    except Exception, e:
        log.warn("ERROR!, trigger_ticket_generation, exception encountered...")
        log.warn(e)
        return 'error', 'Exception encountered'
    else:
        if 'status' not in d_json:
            log.warn("ERROR!, trigger_ticket_generation, missing key 'status' in json response...")
            return 'error', "Missing key 'status' in json response."
    return d_json['status'], ''

def trigger_tag_refunded(tx):
    # get token
    token_status_code, token_return = get_token()
    if token_status_code != 200:
        log.warn("ERROR!, trigger_tag_refunded, get_token, status_code not equal to 200: %s..." % (token_status_code))
        return token_status_code, token_return

    headers                      = {}
    headers['gmovies-token']     = token_return
    headers['gae-token']  = REFUND_TAG_TOKEN
    log.debug("trigger_tag_refunded, headers: {0}...".format(headers))

    tag_endpoint = TAG_REFUND_ENDPOINT.replace('<tx_id>', tx.key.parent().id() + "~" + tx.key.id())
    req = requests.Request('POST', url=tag_endpoint, headers=headers).prepare()

    s = requests.Session()
    r_status_code, r_json = call_gmovies_api(s, req)
    log.debug("trigger_tag_refunded, status_code: %s..." % (r_status_code))
    log.debug("trigger_tag_refunded, response: %s..." % (r_json))

    if r_status_code != 200:
        log.warn("ERROR!, trigger_tag_refunded, status_code not equal to 200: %s..." % (r_status_code))
        return r_status_code, 'Status_code not equal to 200.'

    d_json = {}
    try:
        d_json = json.loads(r_json)
    except ValueError, e:
        log.warn("ERROR!, trigger_tag_refunded, response not in json format...")
        return 'error', 'Response not in json format.'
    except Exception, e:
        log.warn("ERROR!, trigger_tag_refunded, exception encountered...")
        log.warn(e)
        return 'error', 'Exception encountered'
    else:
        if 'status' not in d_json:
            log.warn("ERROR!, trigger_tag_refunded, missing key 'status' in json response...")
            return 'error', "Missing key 'status' in json response."
    return d_json['status'], ''

#this is where the seat of the refunded ticket will be avaialable again to use in other reservation
def seat_cancellation(ticket_code, device_id):
    d_json = {}

    try:
        headers = {
            'X-Globe-SureSeats-DeviceId' : device_id,
            'Content-Type': 'application/json'
        }

        payload = {"claim_code": ticket_code}
        r = requests.post(SURESEATS_FREE_SEATS_ENDPOINT, data=json.dumps(payload), headers=headers)
        d = r.json()    
        
        if 'error_code' in d:
            if d['error_code'] == 'CANCEL_FAILED':
                log.debug("result : {}".format(r.content))
                log.debug("Error cancellation of seats : {}".format(d['message']))
                d_json = {'status': 1, 'message': d['message']}
            else:
                log.debug("result : {}".format(r.content))
                log.debug("do_refund_transaction.do_free_seat, Successfully free the seat from ticket code {}".format(ticket_code))
                d_json = {'status': 1, 'message': d['message']}
        else:
            d_json = {'status': 1, 'message': 'Seat has been successfully cancelled.'}

    except Exception as e:
        log.error("Error Message : {}".format(traceback.format_exc()))
        raise GMoviesCancelFailedException('Seat Not Cancelled.')

    return d_json

def otp_verify(pin, mobile):
    endpoind = 'https://core.gmovies.ph/api/gmovies/sms/send'

    headers = {
        "LBAPIToken" : "Mc78beJQcwdEbWWcBqkF4yT7gQ38eWVH2Ws5VeUJT6jEpuGYmYDj4jYAwCT3mjpRr6QW2b6S2VZsS7QzuMcJPhzF",
        "Accept" : "application/json"
    }

    payload = {
        'message': 'Your Verification Code is %s' % (pin),
        'mobile': mobile,
        'send_by': 'gae',
    }

    log.debug('OTP payload : %s' % (payload))
    log.debug('OTP headers : %s' % (headers))
    
    if ',' in mobile:
        numbers = mobile.split(",")
        for number in numbers:
            payload = {
                'message': 'OPS Verification Code is %s' % (pin),
                'mobile': number,
                'send_by': 'gae',
            }
            r = requests.post(endpoind, data=payload, headers=headers)
            d = r.json()
    else:
        r = requests.post(endpoind, data=payload, headers=headers)
        d = r.json()

    log.debug('Response Headers : %s' % (r.headers))
    log.debug('Post Request : %s' % (r))
    log.debug('OTP response from %s : %s' % (endpoind, d))
