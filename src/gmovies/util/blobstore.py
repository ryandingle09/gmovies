from __future__ import absolute_import

import Image
import logging
import os
import StringIO

import cloudstorage as gcs

# Deprecated. 05/25/2015.
# Will remove in the future, because it uses the deprecated Files API.
# from google.appengine.api import files

from google.appengine.api.app_identity import get_default_gcs_bucket_name
from google.appengine.ext.blobstore import create_gs_key, BlobInfo, BlobKey

from gmovies.settings import GMOVIES_GCS_BUCKET_NAME


log = logging.getLogger(__name__)


# Deprecated. 05/25/2015.
# Will remove in the future, because it uses the deprecated Files API.
# def write_to_blobstore(data, mime_type_args):
#     # Create the file
#     filename = files.blobstore.create(mime_type=mime_type_args)
#
#     # Open the file and write to it
#     with files.open(filename, 'a') as f:
#         f.write(data)
#
#     # Finalize the file. Do this before attempting to read it.
#     log.debug("Writing to blobstore with filename %s" % filename)
#     files.finalize(filename)
#
#     # Get the file's blob key
#     blob_key = files.blobstore.get_blob_key(filename)
#
#     return blob_key


def write_to_blobstore_using_gcs(data, filename, content_type, folder=''):
    log.info("Writing to blobstore using Google Cloud Storage...")

    gcs_filename = GMOVIES_GCS_BUCKET_NAME + folder + filename

    # Open the file and write to it
    with gcs.open(gcs_filename, 'w', content_type=content_type) as f:
        f.write(data)

    blobstore_filename = '/gs' + GMOVIES_GCS_BUCKET_NAME + folder + filename

    # Create Google Storage Key
    blob_key = create_gs_key(blobstore_filename)

    return BlobKey(blob_key)


# Deprecated. 05/25/2015.
# Will remove in the future, because it uses the deprecated Files API.
# def get_image(key):
#     blob_info = BlobInfo.get(key)
#     blob_reader = blob_info.open()
#     value = blob_reader.read()
#     original_image = Image.open(StringIO.StringIO(value))
#
#     return (blob_info, original_image)


def get_image_gcs_info(filename, folder=''):
    gcs_filename = GMOVIES_GCS_BUCKET_NAME + folder + filename

    gcs_info = gcs.stat(gcs_filename)
    gcs_reader = gcs.open(gcs_filename)
    value = gcs_reader.read()

    original_image = Image.open(StringIO.StringIO(value))

    return gcs_info, original_image