from __future__ import absolute_import

def camelcase(s):
  s = s.replace('_',' ')
  #return re.sub(r'\w+', lambda m:m.group(0).capitalize(), s)
  return ' '.join(''.join([w[0].upper(), w[1:].lower()]) for w in s.split())
