# builtin
from __future__ import absolute_import

import datetime
import logging
import re
import requests
import StringIO
import time
import xlwt

from decimal import Decimal
from itertools import groupby
from operator import attrgetter
from uuid import uuid4 as uuid_gen

from google.appengine.ext import ndb
from google.appengine.api import memcache
from google.appengine.api import taskqueue

from gmovies.models import (AnalyticsService, AnalyticsServiceHistory, Cinema,
        CSRCPayment, Device, Feed, Movie, MovieTitles, MovieTitlesLookup,
        MultiResImageBlob, PromoReservationTransaction, ReservationTransaction,
        Schedule, TicketTemplateImageBlob, Theater, TheaterOrganization,
        DATE_FORMAT, TIME_FORMAT, DATETIME_FORMAT, PaymentOptionSettings, FreeTicketData)
from gmovies.heuristics import movies, schedules, scraper_schedules
from gmovies.heuristics.rottentomatoes import rottentomatoes_transform_movies
from gmovies.util import cache_movie_runtime, id_encoder, get_movie_runtime, parse_cast
from gmovies.files import parser
from gmovies import orgs
from gmovies.util import id_encoder
from gmovies.feeds.rottentomatoes import RT

log = logging.getLogger(__name__)

ALLOWED_EXTENSIONS = set(['csv', 'txt'])
ALLOWED_IMAGE_CONTENT_TYPE = ['image/png', 'image/jpg', 'image/jpeg', 'image/gif']
GLOBE = ndb.Key(TheaterOrganization, str(orgs.GLOBE))
THEATER_ORGANIZATIONS = {str(orgs.AYALA_MALLS): 'Ayala Malls', str(orgs.ROCKWELL_MALLS): 'Rockwell',
        str(orgs.GREENHILLS_MALLS): 'Greenhills', str(orgs.SM_MALLS): 'SM Malls',
        str(orgs.MEGAWORLD_MALLS): 'Megaworld', str(orgs.ROBINSONS_MALLS): 'Robinsons',
        str(orgs.SHANGRILA_MALLS): 'Shangri-la', str(orgs.CINEMA2000_MALLS): 'Cinema 2000',
        str(orgs.FESTIVAL_MALLS): 'Festival', str(orgs.GATEWAY_MALLS): 'Gateway',
        str(orgs.GLOBE): 'Globe', str(orgs.ROTTEN_TOMATOES): 'Rotten Tomatoes',
        str(orgs.CINEMA76_MALLS): 'Cinema 76', str(orgs.GLOBE_EVENTS): 'Globe Events',
        str(orgs.CITY_MALLS): 'CityMall'}
ISO_WEEKDAY = {'1': 'Monday', '2': 'Tuesday', '3': 'Wednesday', '4': 'Thursday', '5': 'Friday', '6': 'Saturday', '7': 'Sunday'}


def create_theaterorg_params(form):
    theaterorg = TheaterOrganization(key=ndb.Key(TheaterOrganization, str(uuid_gen())))
    form.populate_obj(theaterorg)
    theaterorg.put()

    return theaterorg

def create_theater_params(form, theaterorg_id):
    theaterorg_key = ndb.Key(TheaterOrganization, str(theaterorg_id))
    theater = Theater(parent=theaterorg_key)
    form.populate_obj(theater)

    if form.latitude.data and form.longitude.data:
        theater.location = ndb.GeoPt(form.latitude.data, form.longitude.data)

    theater.put()

    return theater

def create_cinema_params(form, encoded_theater_id):
    geo_pt = None
    theater = get_theater(encoded_theater_id)

    if form.latitude.data and form.longitude.data:
        geo_pt = ndb.GeoPt(form.latitude.data, form.longitude.data)

    cinema = Cinema()
    form.populate_obj(cinema)
    cinema.seat_map = []
    cinema.location = geo_pt
    theater.cinemas.append(cinema)
    theater.put()

# generates the characters from `c1` to `c2`, inclusive.
def char_range(c1, c2):
    for c in xrange(ord(c1), ord(c2)+1):
        yield c

def generate_seat_map(row_list, number_list):
    seat_map = []

    for r in row_list:
        for n in number_list:
            seat_map.append(str(r)+str(n))

    return seat_map

def get_cinema_entity(theater, cinema_name):
    cinemas = theater.cinemas

    for c in cinemas:
        if cinema_name == c.name:
            return c

def get_cinema_entity_using_feed_code(theater, feed_code):
    cinemas = theater.cinemas

    for c in cinemas:
        if feed_code == c.feed_code:
            return c

def add_seats(seats, extant_rows):
    seat_map = [r.split(',') for r in extant_rows]

    if seats.strip() != '':
        add_seats = seats.split(',')
        seat_map.append(filter(lambda s: s != '', add_seats))

    return seat_map

def check_theaterorg(name):
    theaterorgs = TheaterOrganization.query(TheaterOrganization.name==name).fetch()

    if len(theaterorgs) > 0:
        return False

    return True

def check_theater(name, parent_key):
    theaterorg_key = ndb.Key(TheaterOrganization, str(parent_key))
    theaters = Theater.query(Theater.name==name, ancestor=theaterorg_key).fetch()

    if len(theaters) > 0:
        return False

    return True

def check_cinema(name, encoded_theater_id):
    is_not_existing = False
    theater = get_theater(encoded_theater_id)
    cinema_names = [c.name for c in theater.cinemas]

    if name not in cinema_names:
        is_not_existing = True

    return is_not_existing

def get_theater(encoded_theater_id):
    (parent_org, theater_id) = id_encoder.decoded_theater_id(encoded_theater_id)
    theater_key = ndb.Key(Theater, theater_id, parent=ndb.Key(TheaterOrganization, str(parent_org)))

    return theater_key.get()

def get_theaterorg(theaterorg_id):
    theaterorg_key = ndb.Key(TheaterOrganization, str(theaterorg_id))

    return theaterorg_key.get()

def create_movie(form):
    movie_dict = {}
    movie_dict['movie_title'] = form.canonical_title.data
    movie_dict['cast'] = parse_cast(form.cast_list.data)
    movie_dict['genre'] = form.genre.data
    movie_dict['synopsis'] = form.synopsis.data
    movie_dict['runtime_mins'] = float(form.runtime_mins.data) if form.runtime_mins.data else None
    movie_dict['advisory_rating'] = form.advisory_rating.data
    movie_dict['release_date'] = form.release_date.data
    movie_list = movies.transform({str(orgs.GLOBE): [movie_dict]})

    for m in movie_list:
        memcache.set('movie::new::key', m)

def check_movie(old_correlation_titles, new_correlation_titles, key):
    is_not_existing = True
    queryset = Movie.query(Movie.correlation_title.IN(new_correlation_titles), Movie.is_inactive==False)
    movie = queryset.get()

    if movie and movie.key != key:
       is_not_existing = False

    if old_correlation_titles != new_correlation_titles:
        for t in old_correlation_titles:
            cached_title = memcache.get("movie::correlation_title::%s" % t)

            if cached_title is not None:
                memcache.delete("movie::correlation_title::%s" % t)

    return is_not_existing

def create_schedule(form, theater):
    schedule_dict = {}
    schedule_dict['id'] = ''
    schedule_dict['movie_title'] = form.movie_title.data
    schedule_dict['screening'] = str(form.screening.data)
    schedule_dict['theater_code'] = theater.org_theater_code
    schedule_dict['cinema'] = form.cinema_code.data
    schedule_dict['variant'] = form.variant.data
    schedule_dict['uuid'] = str(orgs.GLOBE)
    schedule = schedules.convert_schedule(schedule_dict)

    return schedule

def create_service(form):
    if check_service(form.service_code.data) == False:
        return 'error','Service code already exist'

    log.info("creating service...")
    service = AnalyticsService()
    form.populate_obj(service)
    service.put()
    log_history(service, None, None)

    return 'success', service

def log_history(service, current_state, current_platform):
    log.info("Logging history --- Prev Status: {0} -- Current Status: {1}".format(service.active, current_state))
    log.info("Logging history --- Prev Platform: {0} -- Current Platform: {1}".format(service.platform, current_platform))

    if ((current_state is not None and service.active != current_state) or
            (current_platform is not None and service.platform != current_platform)):
        log.info("Logging {0} history state {1}".format(service.service_name, current_state))
        log.info("Logging {0} history platform {1}".format(service.service_name, current_platform))

        history = AnalyticsServiceHistory(parent=service.key)
        history.state = current_state
        history.platform = current_platform
        history.put()

def check_service(service_code):
    service_query = AnalyticsService.query(AnalyticsService.service_code==service_code)
    services_count = service_query.count()
    log.info("Services count: {}".format(services_count))

    if services_count >= 1:
        return False

    return True

def check_payment_option_settings(payment_option_identifier):
    payment_option_key = ndb.Key(PaymentOptionSettings, payment_option_identifier)
    payment_option = payment_option_key.get()

    if payment_option:
        return False

    return True

def update_show_times(schedule, show_times):
    # assume that the order of fields are the same with order of fields in entity
    # it works for now with assumption

    zipped_list = zip(schedule.start_times,show_times)

    for z in zipped_list:
        if z[0] in schedule.map_time_to_code and z[1] not in schedule.start_times:
            schedule_code = schedule.map_time_to_code[z[0]]
            del schedule.map_time_to_code[z[0]]
            schedule.map_time_to_code[z[1]] = schedule_code
            schedule.map_code_to_time[schedule_code] = z[1]

    for i, s in enumerate(schedule.start_times):
        if z[0] == s:
            show_time = datetime.datetime.strptime(z[1],'%I:%M:%S %p')
            show_time = datetime.time(show_time.hour, show_time.minute)
            schedule.start_times[i] = show_time

def check_upload_file(uploaded_file, error_list, file_type, theaterorg=None):
    file_errors = []
    theater_file_errors = []
    movie_file_errors = []
    schedule_file_errors = []

    if uploaded_file and not(allowed_file(uploaded_file.filename)):
        error_list.append('Invalid file format for %s' % (uploaded_file.filename))
    elif uploaded_file and allowed_file(uploaded_file.filename):
        filestream = uploaded_file.stream

        if file_type == 'theater':
            (theater_list, theater_file_errors) = parser.parse_theater_file(filestream, uploaded_file.filename, theaterorg)
            error_list.extend(theater_file_errors)
        elif file_type == 'movies':
            (movie_list, movie_file_errors) = parser.parse_movie_file(filestream, uploaded_file.filename)
            movies.transform({ str(orgs.GLOBE): movie_list })
            error_list.extend(movie_file_errors)
        else:
            (schedule_list, schedule_file_errors) = parser.parse_schedule_file(filestream, uploaded_file.filename)
            scraper_schedules.scraper_transform_schedules(schedule_list, is_file_upload=True)

            try:
                taskqueue.add(queue_name='default', url='/tasks/refresh-movies', method='GET')
            except (taskqueue.TaskAlreadyExistsError, taskqueue.TombstonedTaskError):
                pass

            error_list.extend(schedule_file_errors)

def check_seatmap_upload_file(uploaded_file):
    rows = []
    error_list = []

    if uploaded_file and not allowed_file(uploaded_file.filename):
        error_list.append('Invalid file format for %s' % (uploaded_file.filename))
    elif uploaded_file and allowed_file(uploaded_file.filename):
        filestream = uploaded_file.stream

        seat_map, seat_count, file_errors = parser.parse_seatmap_file(filestream, uploaded_file.filename)
        error_list.extend(file_errors)

    return seat_map, seat_count, error_list

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

def get_reservation_info(transaction, info_index):
    return transaction.reservations[int(info_index)]

def format_schedule_reservation_info(schedule):
    schedule_text = schedule.show_date.strftime(DATE_FORMAT) + ' - ' + schedule.movie.get().canonical_title + ' - Cinema' + schedule.cinema

    if schedule.variant != None:
        schedule_text = schedule_text + ' - ' + schedule.variant

    return schedule_text

def get_transaction(transaction_id):
    (device_id, txn_id) = id_encoder.decode_id_paths(transaction_id)
    device_key = ndb.Key(Device, device_id)
    transaction_key = ndb.Key(ReservationTransaction, txn_id, parent=device_key)

    return transaction_key.get()

def get_csrc_transaction(transaction_id):
    (device_id, txn_id) = id_encoder.decode_id_paths(transaction_id)
    device_key = ndb.Key(Device, device_id)
    transaction_key = ndb.Key(CSRCPayment, txn_id, parent=device_key)

    return transaction_key.get()

def get_promo_transaction(transaction_id):
    (device_id, txn_id) = id_encoder.decode_id_paths(transaction_id)
    device_key = ndb.Key(Device, device_id)
    transaction_key = ndb.Key(PromoReservationTransaction, txn_id, parent=device_key)

    return transaction_key.get()

def format_time_to_string(time_parameter):
    return time_parameter.strftime(TIME_FORMAT)

def format_date_to_string(date_parameter):
    return date_parameter.strftime(DATE_FORMAT)

def format_datetime_to_string(datetime_parameter):
    return datetime_parameter.strftime(DATETIME_FORMAT)

def convert_string_to_time(time_parameter):
    if not isinstance(time_parameter, datetime.time) and time_parameter != None:
        times = re.split('\:', time_parameter)

        return datetime.time(int(times[0]), int(times[1]), int(times[2]))

    return time_parameter

def mask_cc_numbers(cc_number):
    cc_number = str(cc_number)

    return "*" * (len(cc_number) - 4) + cc_number[-4:];

def convert_time_to_datetime(t, delta_mins=None):
    strtime = format_time_to_string(t)
    dt = time.strptime(strtime, "%H:%M")
    d = datetime.datetime(*dt[:6])
   
    if delta_mins is None:
        return d

    return d + datetime.timedelta(minutes=delta_mins)


def is_conflict(slots, current_slot, index):
    MINIMUM_MOVIE_RUNTIME = 40

    # empty params
    if slots is None or current_slot is None:
        return False

    slots_size = len(slots)

    # there are 0 or 1 slot only
    if slots_size == 0 or slots_size == 1:
        return False

    # if next slot reached the slot limit, ignore
    if index + 1 == slots_size:
        return False

    # next slot if less than slot size else use previous slot
    next_slot = slots[index + 1] #if index < slot_size else slots[index-1]
    runtime = get_movie_runtime(current_slot.movie.id())
    movie_runtime = runtime if runtime is not None else MINIMUM_MOVIE_RUNTIME

    current_dt = convert_time_to_datetime(current_slot.start_time, movie_runtime)
    next_dt = convert_time_to_datetime(next_slot.start_time)

    # compare current slot start time + movie duration and next slot start time
    if current_dt > next_dt:
        return True

    return False

def has_slots_conflict(schedule_id, theater_id):
    if schedule_id is None or theater_id is None:
        return False

    theater = get_theater(theater_id)
    schedule_key = ndb.Key(Schedule, schedule_id, parent=theater.key)
    schedule = schedule_key.get()
    sorted_slots = schedule.sorted_slots()

    if len(sorted_slots) == 0:
        return False

    conflict_ctr = 0

    for idx, current_slot in enumerate(sorted_slots):
        if is_conflict(sorted_slots, current_slot, idx):
            conflict_ctr =+ 1

    conflicted = True if conflict_ctr > 0 else False

    return conflicted

def fetch_schedules_on_demand():
    try:
        taskqueue.add(queue_name='default', url='/tasks/fetch/schedules/ayala', method='GET')
    except (taskqueue.TaskAlreadyExistsError, taskqueue.TombstonedTaskError):
        pass

def is_ayala(org_name):
    return True if re.match('Ayala', org_name) is not None else False

# TODO: make this dynamic, create a payment enabled option property in TheaterOrganization.
def is_payment_enabled(org_uuid):
    is_enabled = False
    payment_enabled_orgs = (str(orgs.AYALA_MALLS), str(orgs.SM_MALLS), str(orgs.ROCKWELL_MALLS),
            str(orgs.GREENHILLS_MALLS), str(orgs.MEGAWORLD_MALLS), str(orgs.ROBINSONS_MALLS),
            str(orgs.CINEMA76_MALLS), str(orgs.GLOBE_EVENTS), str(orgs.CITY_MALLS))

    if org_uuid in payment_enabled_orgs:
        is_enabled = True

    return is_enabled

def update_org_client_control(org_id, theater_id, allowed):
    org_key = ndb.Key(TheaterOrganization, org_id)
    org = org_key.get()
    theaters = []
    log.info("Current Org Default Client Control: {}".format(org.default_client_control))

    if org.default_client_control is not None:
        theaters = org.default_client_control['theater_whitelist']
        # reservation is allowed
        if allowed:
            # is it listed?
            if theater_id in theaters:
                log.warn('Theater ID {} already exists'.format(theater_id))
            # no? append it
            else:
                log.info('Appending new theater: {}'.format(theater_id))
                theaters.append(theater_id)
        # reservation is not allowed
        else:
            # is it not listed?
            if theater_id not in theaters:
                log.warn('Theater ID {} is not listed'.format(theater_id))
            # yes? remove it
            else:
                log.info('Delisting theater: {}'.format(theater_id))
                theaters.remove(theater_id)
        org.default_client_control['theater_whitelist'] =  theaters
    else: # not empty
        # is allowed
        if allowed:
            theaters.append(theater_id)
            org.default_client_control = {'theater_whitelist': theaters}
    
    log.info('Saving updated whitelist {}'.format(theaters))
    org.put()

def is_old_variant_strip(movie_title):
    variant = movies.variant_from_title(movie_title)

    if variant is not None:
        return True

    return False

def save_ticket_template_image(image_data, parent, template_type='generic'):
    if template_type:
        queryset = TicketTemplateImageBlob.query(TicketTemplateImageBlob.template_type == template_type, ancestor=parent)
    else:
        queryset = TicketTemplateImageBlob.query(ancestor=parent)

    if queryset.count() != 0:
        template_image_blob = queryset.get()
        template_image_blob.image = image_data
        template_image_blob.template_type = template_type
        template_image_blob.put()

        return template_image_blob

    template_image_blob = TicketTemplateImageBlob()
    template_image_blob.key = ndb.Key(TicketTemplateImageBlob, str(uuid_gen()), parent=parent)
    template_image_blob.image = image_data
    template_image_blob.template_type = template_type
    template_image_blob.put()

    return template_image_blob

def rockwell_get_selected_seat_ids(theater, cinema_name, selected_seats):
    log.info('Rockwell: get selected seat ids...')

    selected_seat_ids = []
    cinema = get_cinema_entity(theater, cinema_name)
    rockwell_cinema_id = cinema.rockwell_cinema_id
    rockwell_cinema_name = cinema.rockwell_cinema_name
    seatmap = cinema.seat_map

    for rows in seatmap:
        for columns in rows:
            if columns['seat_name'] in selected_seats:
                selected_seat_ids.append(columns['seat_id'])

    log.info('Rockwell selected seat ids: {}'.format(selected_seat_ids))

    return selected_seat_ids

def camelcase(s):
  s = s.replace('_',' ')

  return ' '.join(''.join([w[0].upper(), w[1:].lower()]) for w in s.split())

def parse_date(date):
    try:
        date = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
    except ValueError:
        try:
            date = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M')
        except ValueError:
            date = None

    return date

def parse_string_to_date(sdate, fdate='%Y-%m-%d'):
    ddate = None

    try:
        ddate = datetime.datetime.strptime(sdate, fdate)
    except ValueError, e:
        log.warn("ERROR!, parse_string_to_date, ValueError, failed to parse sdate(%s) to date..." % sdate)
        log.error(e)
    except Exception, e:
        log.warn("ERROR!, parse_string_to_date, failed to parse sdate(%s) to date..." % sdate)
        log.warn(e)

    return ddate

def parse_string_to_datetime(sdatetime, fdatetime='%Y-%m-%d %H:%M:%S'):
    ddatetime = None

    try:
        ddatetime = datetime.datetime.strptime(sdatetime, fdatetime)
    except ValueError, e:
        log.warn("ERROR!, parse_string_to_datetime, ValueError, failed to parse sdatetime(%s) to datetime..." % sdatetime)
        log.error(e)
    except Exception, e:
        log.warn("ERROR!, parse_string_to_datetime, failed to parse sdatetime(%s) to datetime..." % sdatetime)
        log.error(e)

    return ddatetime

def parse_date_to_string(ddate, fdate='%B %d, %Y'):
    sdate = None

    try:
        sdate = datetime.date.strftime(ddate, fdate)
    except ValueError, e:
        log.warn("ERROR!, parse_date_to_string, ValueError, failed to parse ddate(%s) to string..." % ddate)
        log.error(e)
    except Exception, e:
        log.warn("ERROR!, parse_date_to_string, failed to parse ddate(%s) to string..." % ddate)
        log.error(e)

    return sdate

def parse_time_to_string(ttime, ftime='%I:%M:%S %p'):
    stime = None

    try:
        stime = datetime.time.strftime(ttime, ftime)
    except ValueError, e:
        log.warn("ERROR!, parse_time_to_string, ValueError, failed to parse ttime(%s) to string..." % ttime)
        log.error(e)
    except Exception, e:
        log.warn("ERROR!, parse_time_to_string, failed to parse ttime(%s) to string..." % ttime)
        log.error(e)

    return stime

def parse_datetime_to_string(ddatetime, fdatetime='%m/%d/%Y %I:%M:%S %p'):
    sdatetime = None

    try:
        sdatetime = datetime.datetime.strftime(ddatetime, fdatetime)
    except ValueError, e:
        log.warn("ERROR!, parse_datetime_to_string, ValueError, failed to parse ddatetime(%s) to string..." % ddatetime)
        log.error(e)
    except Exception, e:
        log.warn("ERROR!, parse_datetime_to_string, failed to parse ddatetime(%s) to string..." % ddatetime)
        log.error(e)

    return sdatetime

def asia_manila_to_utc(asia_manila_datetime):
    return asia_manila_datetime - datetime.timedelta(hours=8)

def get_transaction_time_cluster(hour):
    cluster = ''

    if hour in [0, 1, 2, 3, 4, 5]:
        cluster = 'B1'
    elif hour in [6, 7, 8, 9]:
        cluster = 'B2'
    elif hour in [10, 11, 12, 13]:
        cluster = 'B3'
    elif hour in [14, 15, 16, 17]:
        cluster = 'B4'
    elif hour in [18, 19, 20]:
        cluster = 'B5'
    elif hour in [21, 22, 23]:
        cluster = 'B6'

    return cluster

def get_ticket_time_cluster(hour):
    cluster = ''

    if hour in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]:
        cluster = 'S1'
    elif hour in [10, 11, 12, 13, 14]:
        cluster = 'S2'
    elif hour in [15, 16, 17]:
        cluster = 'S3'
    elif hour in [18, 19, 20]:
        cluster = 'S4'
    elif hour in [21, 22, 23]:
        cluster = 'S5'

    return cluster

def apply_sort(q, cls, sort_col, sortable_columns):
    if sort_col:
        if sort_col[0] == '-':
            key = sort_col[1:]
        else:
            key = sort_col

        if key not in sortable_columns:
            log.info("Column %s is not sortable..." % key)
            return q

        if not hasattr(cls, key):
            log.info("Has no attribute %s..." % key)
            return q

        prop = getattr(cls, key)

        if sort_col[0] == '-':
            log.info('Sort Descending...')
            q = q.order(-prop)
        else:
            log.info('Sort Ascending...')
            q = q.order(prop)

    return q


##########
#
# Movie Title Bank
#
##########

def create_movie_title(form, is_lookup=False):
    movie_title_dictionary = {}
    rottentomatoes_info = {}

    if form.rt_id.data:
        rottentomatoes_info = RT.info(str(form.rt_id.data))

    if is_lookup:
        lookup_key = ndb.Key(MovieTitlesLookup, int(form.movie_title_lookup.data))
        lookup = lookup_key.get()

        if lookup:
            if not rottentomatoes_info or 'error' in rottentomatoes_info:
                movie_title_dictionary['title'] = lookup.movie_title
                movie_title_dictionary['correlation_title'] = lookup.correlation_title
            else:
                log.info("Fetch Rotten Toamtoes details...")
                movie_title_dictionary = rottentomatoes_info
    else:
        if not rottentomatoes_info or 'error' in rottentomatoes_info:
            movie_title_dictionary['title'] = form.title.data
        else:
            log.info("Fetch Rotten Tomatoes details!")
            movie_title_dictionary = rottentomatoes_info

    movie_title_list = rottentomatoes_transform_movies([movie_title_dictionary])

    for movie_title in movie_title_list:
        memcache.set('movie_title::new::key', movie_title)

    if is_lookup:
        log.info("Added movie title from MovieTitlesLookup...")
        log.info("Deleted movie title in MovieTitlesLookup...")

        lookup_key.delete()

    return movie_title_list[0]

def check_movie_title(old_correlation_titles, new_correlation_titles, key):
    is_not_existing = True
    queryset = MovieTitles.query(ndb.OR(
                    MovieTitles.correlation_title.IN(new_correlation_titles),
                    MovieTitles.secondary_titles.correlation_title_version_one.IN(new_correlation_titles),
                    MovieTitles.secondary_titles.correlation_title_version_two.IN(new_correlation_titles),
                    MovieTitles.secondary_titles.correlation_title_version_three.IN(new_correlation_titles),
                    MovieTitles.secondary_titles.correlation_title_version_four.IN(new_correlation_titles)),
            MovieTitles.is_active == True)
    movie_title = queryset.get()

    if movie_title and movie_title.key != key:
       is_not_existing = False

    if old_correlation_titles != new_correlation_titles:
        for t in old_correlation_titles:
            cached_movie_title = memcache.get("movie_title::correlation_title::%s" % t)

            if cached_movie_title and cached_movie_title is not None:
                memcache.delete("movie_title::correlation_title::%s" % t)

    return is_not_existing


##########
#
# GET Rotten Tomatoes Details
#
##########

def get_rottentomatoes_details(movie_key):
    movie = movie_key.get()
    rt_key = ndb.Key(Feed, str(orgs.ROTTEN_TOMATOES))
    rt_corr = filter(lambda corr: corr.org_key == rt_key, movie.movie_correlation)

    rt_feed = rt_key.get()
    field_blacklist = rt_feed.movie_fields_blacklist
    field_whitelist = rt_feed.movie_fields_whitelist
    check_field = movies.build_whitelist_blacklist_check(field_blacklist, field_whitelist)

    if rt_corr:
        rt_mapping = rt_corr[0].movie_id
    else:
        return None

    queryset = MovieTitles.query(MovieTitles.rt_details.rt_id == rt_mapping,
            MovieTitles.is_active == True)

    if queryset.count() == 0:
        return None

    rt_dict = queryset.get().to_entity_rottentomatoes()

    if not check_field(rt_dict, 'synopsis'):
        rt_dict.pop('synopsis', None)

    if not check_field(rt_dict, 'cast'):
        rt_dict.pop('cast', None)

    if not check_field(rt_dict, 'release_date'):
        rt_dict.pop('release_date', None)

    if not check_field(rt_dict, 'genre'):
        rt_dict.pop('genre', None)

    if not check_field(rt_dict, 'runtime_mins'):
        rt_dict.pop('runtime_mins', None)

    if not check_field(rt_dict, 'advisory_rating'):
        rt_dict.pop('advisory_rating', None)

    if check_field(rt_dict, 'ratings'):
        rt_dict['ratings'] = movies.get_rottentomatoes_ratings(queryset)

        movies.build_ratings_property(movie, rt_dict, rt_key.id())

    movie = transform_rottentomatoes_details(movie, rt_dict)
    movie.put()

    return True

def transform_rottentomatoes_details(movie, rt_dict):
    if 'cast' in rt_dict:
        movie.cast = parse_cast(','.join(cast['name'] for cast in rt_dict['cast']))

    if 'synopsis' in rt_dict:
        movie.synopsis = rt_dict['synopsis']

    if 'runtime_mins' in rt_dict:
        movie.runtime_mins = rt_dict['runtime_mins']

    if 'advisory_rating' in rt_dict:
        movie.advisory_rating = rt_dict['advisory_rating']

    movie.is_details_locked = True

    return movie


###############
#
# Utils for Uploading Poster Image
#
###############

def check_uploaded_file(image_file):
    if image_file:
        image_content_type = image_file.content_type

        if image_content_type.lower() in ALLOWED_IMAGE_CONTENT_TYPE:
            return True

    return False

def generate_sales_reports_xls(tx_from, tx_to, theaterorg=None, theater=None,
        payment_type=None, platform=[]):
    queryset = ReservationTransaction.query(ReservationTransaction.state==100,
            ReservationTransaction.date_created>=tx_from, ReservationTransaction.date_created<=tx_to)

    if theaterorg is not None:
        if theater is not None:
            queryset = queryset.filter(ReservationTransaction.reservations.theater==theater.key)
        else:
            theaters = Theater.query(ancestor=theaterorg.key).fetch(keys_only=True)

            if theaters:
                queryset = queryset.filter(ReservationTransaction.reservations.theater.IN(theaters))

    if payment_type is not None:
        queryset = queryset.filter(ReservationTransaction.payment_type==payment_type)

    # generate xls file
    row_counter = 0
    workbook = xlwt.Workbook(encoding='utf-8')
    worksheet = workbook.add_sheet("Sheet 1")
    headers_font_style = xlwt.XFStyle()
    headers_font_style.font.bold = True
    font_style = xlwt.XFStyle()
    font_style.font.bold = False

    worksheet.write(0, 0, 'PURCHASE DATE', headers_font_style)
    worksheet.write(0, 1, 'TRANSACTIONS', headers_font_style)
    worksheet.write(0, 2, 'TICKETS SOLD', headers_font_style)
    worksheet.write(0, 3, 'NET SALES', headers_font_style)
    worksheet.write(0, 4, 'NET REVENUE', headers_font_style)

    # sales reports for all platforms.
    mixed_dict = transaction_groupings(queryset)
    sorted_mixed_dict = sorted(list(mixed_dict))
    worksheet, row_counter = transaction_worksheets(sorted_mixed_dict, mixed_dict,
            worksheet, row_counter, headers_font_style, font_style)

    if 'android' in platform:
        android_dict = transaction_groupings(queryset, platform='android')
        sorted_android_dict = sorted(list(android_dict))
        worksheet, row_counter = transaction_worksheets(sorted_android_dict, android_dict,
                worksheet, row_counter, headers_font_style, font_style, platform='android')

    if 'ios' in platform:
        ios_dict = transaction_groupings(queryset, platform='ios')
        sorted_ios_dict = sorted(list(ios_dict))
        worksheet, row_counter = transaction_worksheets(sorted_ios_dict, ios_dict,
                worksheet, row_counter, headers_font_style, font_style, platform='ios')

    if 'website' in platform:
        website_dict = transaction_groupings(queryset, platform='website')
        sorted_website_dict = sorted(list(website_dict))
        worksheet, row_counter = transaction_worksheets(sorted_website_dict, website_dict,
                worksheet, row_counter, headers_font_style, font_style, platform='website')

    sales_reports = StringIO.StringIO()
    workbook.save(sales_reports)

    if theaterorg is not None:
        file_name = 'GMoviesSalesReports-%s.xls' % theaterorg.name
    else:
        file_name = 'GMoviesSalesReports.xls'

    return file_name, sales_reports.getvalue()

def transaction_groupings(queryset, platform=None):
    transactions = {}

    if platform is not None:
        if platform == 'android':
            queryset = queryset.filter(ndb.OR(ReservationTransaction.platform==platform,
                    ReservationTransaction.platform==None))
        else:
            queryset = queryset.filter(ReservationTransaction.platform==platform)

    for ent in queryset:
        date_to_string = parse_date_to_string(ent.date_created, fdate='%m/%d/%Y')

        if date_to_string:
            if date_to_string in transactions:
                transactions[date_to_string]['transactions'] += 1
                transactions[date_to_string]['tickets'] += len(ent.reservations)
                transactions[date_to_string]['gross'] += Decimal(ent.ticket.extra['total_amount'])
            else:
                transactions[date_to_string] = {}
                transactions[date_to_string]['transactions'] = 1
                transactions[date_to_string]['tickets'] = len(ent.reservations)
                transactions[date_to_string]['gross'] = Decimal(ent.ticket.extra['total_amount'])

    return transactions

def transaction_worksheets(purchased_dates, transactions, worksheet,
        row_counter, headers_font_style, font_style, platform=None):
    if platform is None:
        platform = 'MIXED'
    else:
        platform = platform.upper()

    row_counter += 1
    worksheet.write_merge(row_counter, row_counter, 0, 4, platform, headers_font_style)

    if purchased_dates:
        for purchased_date in purchased_dates:
            row_counter += 1
            worksheet.write(row_counter, 0, purchased_date, font_style)

            if purchased_date in transactions:
                worksheet.write(row_counter, 1, transactions[purchased_date]['transactions'], font_style)
                worksheet.write(row_counter, 2, transactions[purchased_date]['tickets'], font_style)
                worksheet.write(row_counter, 3, transactions[purchased_date]['gross'], font_style)
    else:
        row_counter += 1
        worksheet.write_merge(row_counter, row_counter, 0, 4, 'No completed transactions.', font_style)

    return worksheet, row_counter

def generate_transaction_reports_xls(tx_from, tx_to, month, year, theaterorg=None, theater=None,
        payment_type=None, report_type=None):
    tx_from = convert_timezone(tx_from, 8, '-') # convert to UTC.
    tx_to = convert_timezone(tx_to, 8, '-') # convert to UTC.

    log.debug("generate_transaction_reports_xls, tx_from: %s, tx_to: %s" % (tx_from, tx_to))
    log.debug("generate_transaction_reports_xls, month: %s, year: %s" % (month, year))
    log.debug("generate_transaction_reports_xls, theaterorg: %s" % (theaterorg.key))
    log.debug("generate_transaction_reports_xls, payment_type: %s" % (payment_type))
    queryset = ReservationTransaction.query(ReservationTransaction.state.IN([100,102]),
            ReservationTransaction.date_created>=tx_from, ReservationTransaction.date_created<=tx_to)

    if theaterorg is not None:
        if theater is not None:
            queryset = queryset.filter(ReservationTransaction.reservations.theater==theater.key)
        else:
            theaters = Theater.query(ancestor=theaterorg.key).fetch(keys_only=True)

            if theaters:
                queryset = queryset.filter(ReservationTransaction.reservations.theater.IN(theaters))

    if payment_type is not None:
        queryset = queryset.filter(ReservationTransaction.payment_type==payment_type)

    # generate xls file
    row_counter = 0
    workbook = xlwt.Workbook(encoding='utf-8')
    worksheet = workbook.add_sheet("Sheet 1")
    headers_font_style = xlwt.XFStyle()
    headers_font_style.font.bold = True
    font_style = xlwt.XFStyle()
    font_style.font.bold = False

    if 'daily' == report_type:
        worksheet.write(0, 0, 'WEEK NUMBER', headers_font_style)
        worksheet.write(0, 1, 'TRANSACTION DAY', headers_font_style)
        worksheet.write(0, 2, 'TRANSACTION DATE', headers_font_style)
        worksheet.write(0, 3, 'TRANSACTION TIME', headers_font_style)
        worksheet.write(0, 4, 'MOVIE TITLE', headers_font_style)
        worksheet.write(0, 5, 'CINEMA PARTNER', headers_font_style)
        worksheet.write(0, 6, 'CINEMA NUMBER', headers_font_style)
        worksheet.write(0, 7, 'EMAIL', headers_font_style)
        worksheet.write(0, 8, 'TICKET SOLD', headers_font_style)
        worksheet.write(0, 9, 'NET SALES', headers_font_style)
        worksheet.write(0, 10, 'TICKET DATE', headers_font_style)
        worksheet.write(0, 11, 'TICKET TIME', headers_font_style)
        worksheet.write(0, 12, 'PROMO_CODE', headers_font_style)
        worksheet.write(0, 13, 'PROMO DISCOUNT', headers_font_style)
        worksheet.write(0, 14, 'RESERVATION CODE', headers_font_style)
        worksheet.write(0, 15, 'CLAIM CODE', headers_font_style)
        worksheet.write(0, 16, 'STATE', headers_font_style)
    else:
        worksheet.write(0, 0, 'WEEK NUMBER', headers_font_style)
        worksheet.write(0, 1, 'TRANSACTION DAY', headers_font_style)
        worksheet.write(0, 2, 'TRANSACTION DATE', headers_font_style)
        worksheet.write(0, 3, 'TRANSACTION TIME', headers_font_style)
        worksheet.write(0, 4, 'TRANSACTION TIME CLUSTER', headers_font_style)
        worksheet.write(0, 5, 'MOVIE TITLE', headers_font_style)
        worksheet.write(0, 6, 'CINEMA PARTNER', headers_font_style)
        worksheet.write(0, 7, 'CINEMA NUMBER', headers_font_style)
        #worksheet.write(0, 8, 'EMAIL', headers_font_style)
        #worksheet.write(0, 9, 'MOBILE', headers_font_style)
        worksheet.write(0, 8, 'TICKET SOLD', headers_font_style)
        worksheet.write(0, 9, 'NET SALES', headers_font_style)
        worksheet.write(0, 10, 'TICKET DATE', headers_font_style)
        worksheet.write(0, 11, 'TICKET TIME', headers_font_style)
        worksheet.write(0, 12, 'TICKET TIME CLUSTER', headers_font_style)
        worksheet.write(0, 13, 'PLATFORM', headers_font_style)
        worksheet.write(0, 14, 'PROMO_CODE', headers_font_style)
        worksheet.write(0, 15, 'PROMO DISCOUNT', headers_font_style)
        worksheet.write(0, 16, 'DEVICE ID', headers_font_style)
        worksheet.write(0, 17, 'TRANSACTION ID', headers_font_style)
        worksheet.write(0, 18, 'RESERVATION CODE', headers_font_style)
        worksheet.write(0, 19, 'CLAIM CODE', headers_font_style)
        worksheet.write(0, 20, 'STATE', headers_font_style)
        worksheet.write(0, 21, 'REFUND DATE', headers_font_style)
        worksheet.write(0, 22, 'REFUND REASON', headers_font_style)
        worksheet.write(0, 23, 'REFUNDED BY', headers_font_style)

    for transaction in queryset:
        row_counter += 1
        movie_title = ''
        cinema_partner = ''
        cinema_number = ''
        email_address = ''
        mobile_number = 'N/A'
        gross_sales = ''
        promo_code = ''
        promo_discount = ''
        claim_code = 'N/A'
        transaction_refund_datetime = 'N/A'
        transaction_refund_reason   = 'N/A'
        transaction_refunded_by     = 'N/A'

        if 'email' in transaction.payment_info and transaction.payment_info['email']:
            email_address = transaction.payment_info['email']

        if 'mobile' in transaction.payment_info and transaction.payment_info['mobile']:
            mobile_number = transaction.payment_info['mobile']
        elif transaction.user_info and 'mobile' in transaction.user_info and transaction.user_info['mobile']:
            mobile_number = transaction.user_info['mobile']
        elif 'contact_number' in transaction.payment_info and transaction.payment_info['contact_number']:
            mobile_number = transaction.payment_info['contact_number']

        if 'movie_canonical_title' in transaction.ticket.extra and transaction.ticket.extra['movie_canonical_title']:
            movie_title = transaction.ticket.extra['movie_canonical_title']
        elif 'movie_title' in transaction.ticket.extra and transaction.ticket.extra['movie_title']:
            movie_title = transaction.ticket.extra['movie_title']

        if 'theater_name' in transaction.ticket.extra and transaction.ticket.extra['theater_name']:
            cinema_partner = transaction.ticket.extra['theater_name']
        elif 'theater_and_cinema_name' in transaction.ticket.extra and transaction.ticket.extra['theater_and_cinema_name']:
            cinema_partner = transaction.ticket.extra['theater_and_cinema_name'][0].split('-')[0].strip()

        if 'cinema_name' in transaction.ticket.extra and transaction.ticket.extra['cinema_name']:
            cinema_number = transaction.ticket.extra['cinema_name']
        elif 'theater_and_cinema_name' in transaction.ticket.extra and transaction.ticket.extra['theater_and_cinema_name']:
            cinema_number = transaction.ticket.extra['theater_and_cinema_name'][0].split('-')[-1].replace('Cinema:', '').strip()

        if len(cinema_number) <= 2:
            cinema_number = 'Cinema %s' % cinema_number

        if 'total_amount' in transaction.ticket.extra and transaction.ticket.extra['total_amount']:
            gross_sales = transaction.ticket.extra['total_amount']

        if transaction.discount_info and 'claim_code' in transaction.discount_info and transaction.discount_info['claim_code']:
            promo_code = transaction.discount_info['claim_code']

        if transaction.payment_info and 'promo_code' in transaction.payment_info and transaction.payment_info['promo_code']:
            claim_code = transaction.payment_info['promo_code']

        if transaction.discount_info and 'total_discount' in transaction.discount_info and transaction.discount_info['total_discount']:
            promo_discount = transaction.discount_info['total_discount']

        if transaction.date_cancelled:
            transaction_refund_datetime = convert_timezone(transaction.date_cancelled, 8, '+')
            transaction_refund_datetime = parse_date_to_string(transaction_refund_datetime, fdate='%m/%d/%Y %I:%M %p')
        if transaction.refund_reason:
            transaction_refund_reason = transaction.refund_reason
        if transaction.refunded_by:
            transaction_refunded_by = transaction.refunded_by
        transaction_datetime = convert_timezone(transaction.date_created, 8, '+')
        isoyear, isoweek, isoday = transaction_datetime.isocalendar()

        week_number = isoweek
        transaction_day = ISO_WEEKDAY[str(isoday)]
        transaction_date = parse_date_to_string(transaction_datetime.date(), fdate='%m/%d/%Y')
        transaction_time = parse_time_to_string(transaction_datetime.time(), ftime='%I:%M %p')
        transaction_time_cluster = get_transaction_time_cluster(transaction_datetime.time().hour) if transaction_datetime else ''

        ticket_datetime = None

        if 'show_datetime' in transaction.ticket.extra and transaction.ticket.extra['show_datetime']:
            ticket_datetime = parse_string_to_datetime(transaction.ticket.extra['show_datetime'], '%m/%d/%Y %I:%M %p')

        ticket_date = parse_date_to_string(ticket_datetime.date(), fdate='%m/%d/%Y') if ticket_datetime else ''
        ticket_time = parse_time_to_string(ticket_datetime.time(), ftime='%I:%M %p') if ticket_datetime else ''
        ticket_time_cluster = get_ticket_time_cluster(ticket_datetime.time().hour) if ticket_datetime else ''

        if 'daily' == report_type:
            worksheet.write(row_counter, 0, week_number, font_style)
            worksheet.write(row_counter, 1, transaction_day, font_style)
            worksheet.write(row_counter, 2, transaction_date, font_style)
            worksheet.write(row_counter, 3, transaction_time, font_style)
            worksheet.write(row_counter, 4, movie_title, font_style)
            worksheet.write(row_counter, 5, cinema_partner, font_style)
            worksheet.write(row_counter, 6, cinema_number, font_style)
            worksheet.write(row_counter, 7, email_address, font_style)
            worksheet.write(row_counter, 8, len(transaction.reservations), font_style)
            worksheet.write(row_counter, 9, gross_sales, font_style)
            worksheet.write(row_counter, 10, ticket_date, font_style)
            worksheet.write(row_counter, 11, ticket_time, font_style)
            worksheet.write(row_counter, 12, promo_code, font_style)
            worksheet.write(row_counter, 13, promo_discount, font_style)
            worksheet.write(row_counter, 14, transaction.reservation_reference, font_style)
            worksheet.write(row_counter, 15, claim_code, font_style)
            from gmovies.tx.state_machine import to_state_str
            worksheet.write(row_counter, 16, to_state_str(transaction.state), font_style)
        else:
            worksheet.write(row_counter, 0, week_number, font_style)
            worksheet.write(row_counter, 1, transaction_day, font_style)
            worksheet.write(row_counter, 2, transaction_date, font_style)
            worksheet.write(row_counter, 3, transaction_time, font_style)
            worksheet.write(row_counter, 4, transaction_time_cluster, font_style)
            worksheet.write(row_counter, 5, movie_title, font_style)
            worksheet.write(row_counter, 6, cinema_partner, font_style)
            worksheet.write(row_counter, 7, cinema_number, font_style)
            #worksheet.write(row_counter, 8, email_address, font_style)
            #worksheet.write(row_counter, 9, mobile_number, font_style)
            worksheet.write(row_counter, 8, len(transaction.reservations), font_style)
            worksheet.write(row_counter, 9, gross_sales, font_style)
            worksheet.write(row_counter, 10, ticket_date, font_style)
            worksheet.write(row_counter, 11, ticket_time, font_style)
            worksheet.write(row_counter, 12, ticket_time_cluster, font_style)
            worksheet.write(row_counter, 13, transaction.platform, font_style)
            worksheet.write(row_counter, 14, promo_code, font_style)
            worksheet.write(row_counter, 15, promo_discount, font_style)
            worksheet.write(row_counter, 16, transaction.key.parent().id(), font_style)
            worksheet.write(row_counter, 17, transaction.key.id(), font_style)
            worksheet.write(row_counter, 18, transaction.reservation_reference, font_style)
            worksheet.write(row_counter, 19, claim_code, font_style)
            from gmovies.tx.state_machine import to_state_str
            worksheet.write(row_counter, 20, to_state_str(transaction.state), font_style)
            worksheet.write(row_counter, 21, transaction_refund_datetime, font_style)
            worksheet.write(row_counter, 22, transaction_refund_reason, font_style)
            worksheet.write(row_counter, 23, transaction_refunded_by, font_style)

    tx_reports = StringIO.StringIO()
    workbook.save(tx_reports)

    if theaterorg is not None:
        file_name = 'GMoviesTransactionReports-%s-%s %s' % (theaterorg.name, year, month)
    else:
        file_name = 'GMoviesTransactionReports-%s %s' % (year, month)

    if payment_type:
        file_name = file_name + ("-%s" % payment_type.upper())

    file_name = file_name + ".xls"
    return file_name, tx_reports.getvalue()

def parse_float_to_string(float_parameter, string_format='{:.2f}'):
    return string_format.format(float_parameter)

def convert_timezone(date_parameter, hours, operation):
    if operation == '+':
        result = date_parameter + datetime.timedelta(hours=hours)
    else:
        result = date_parameter - datetime.timedelta(hours=hours)

    return result

def get_date_range_string(start_date, end_date, dates=[]):
    start_date = parse_string_to_date(start_date)
    end_date = parse_string_to_date(end_date)

    for i in range(int((end_date - start_date).days) + 1):
        current_date = start_date + datetime.timedelta(days=i)
        current_date_string = parse_date_to_string(current_date, fdate='%m/%d/%Y')

        if type(dates) == dict:
            dates[current_date_string] = {'transactions': 0, 'seats': 0, 'amount': 0, 'net_revenue': 0}
        else:
            dates.append(current_date_string)

    return dates

def get_net_revenue(org_uuid, total_ticket_price, convenience_fee, seats):
    total_ticket_price = float(total_ticket_price)
    convenience_fee = float(convenience_fee) * seats

    if org_uuid == str(orgs.AYALA_MALLS):
        total_deductions = 0.034
        convenience_fee_share = 0.5
        malls_share = 3.0
    elif org_uuid == str(orgs.ROCKWELL_MALLS):
        total_deductions = 0.036
        convenience_fee_share = 0.7
        malls_share = 0.0
    elif org_uuid == str(orgs.GREENHILLS_MALLS):
        total_deductions = 0.0385
        convenience_fee_share = 1.0
        malls_share = 0.0
    elif org_uuid == str(orgs.MEGAWORLD_MALLS):
        total_deductions = 0.034
        convenience_fee_share = 1.0
        malls_share = 0.0
    else:
        return 0.0

    net_revenue = (convenience_fee - ((total_ticket_price * total_deductions) + malls_share)) * convenience_fee_share

    return net_revenue

def get_filtered_transactions(tx_state, tx_from=None, tx_to=None, payment_type=[],
        org_uuids=[], theater_id=None, platform=None, payment_reference=None, reservation_reference=None, status=None):
    theaters = []
    today = datetime.date.today()

    if not tx_from or tx_from is None:
        tx_from = today

    if not tx_to or tx_to is None:
        tx_to = today

    tx_from = convert_timezone(tx_from, 8, '-') # convert to UTC.
    tx_to = convert_timezone(tx_to, 8, '-') # convert to UTC.

    # for payment and reservation reference, query regardles of the date_created field
    if payment_reference or reservation_reference:
        if list == type(tx_state):
            queryset = ReservationTransaction.query(ReservationTransaction.state.IN(tx_state))
        else:
            queryset = ReservationTransaction.query(ReservationTransaction.state==tx_state)

        if payment_reference:
            queryset = queryset.filter(ReservationTransaction.payment_reference == payment_reference)

        if reservation_reference:
            queryset = queryset.filter(ReservationTransaction.reservation_reference == reservation_reference)
    else:
        if list == type(tx_state):
            queryset = ReservationTransaction.query(ReservationTransaction.state.IN(tx_state),
                    ReservationTransaction.date_created>=tx_from, ReservationTransaction.date_created<=tx_to)
        else:
            queryset = ReservationTransaction.query(ReservationTransaction.state==tx_state,
                    ReservationTransaction.date_created>=tx_from, ReservationTransaction.date_created<=tx_to)

        for org_uuid in org_uuids:
            theaterorg_key = ndb.Key(TheaterOrganization, str(org_uuid))
            theaterorg = theaterorg_key.get()

            if theaterorg is not None:
                if theater_id and theater_id is not None:
                    theater_id = int(theater_id)
                    theater_key = ndb.Key(Theater, theater_id, parent=theaterorg_key)
                    theater = theater_key.get()

                    if theater is not None:
                        theaters.append(theater_key)
                else:
                    theater_keys = Theater.query(ancestor=theaterorg_key).fetch(keys_only=True)
                    theaters.extend(theater_keys)

        if org_uuids and theaters:
        # if org_uuids:
            queryset = queryset.filter(ReservationTransaction.reservations.theater.IN(theaters))

        if payment_type and payment_type is not None:
            queryset = queryset.filter(ReservationTransaction.payment_type.IN(payment_type))

        if platform and platform is not None and platform != 'not-yet-implemented':
            queryset = queryset.filter(ReservationTransaction.platform == platform)
        elif platform == 'not-yet-implemented':
            queryset = queryset.filter(ReservationTransaction.platform == None)

        if status and status is not None:
            queryset = queryset.filter(ReservationTransaction.status.IN(status))

    queryset = queryset.order(-ReservationTransaction.date_created, ReservationTransaction.key)

    return queryset

def get_filtered_free_tickets(is_valid, tx_from=None, tx_to=None, discount_type=[], org_uuids=[], theater_id=None, platform=None):
    theaters = []
    today = datetime.date.today()

    if not tx_from or tx_from is None:
        tx_from = today

    if not tx_to or tx_to is None:
        tx_to = today

    tx_from = convert_timezone(tx_from, 8, '-') # convert to UTC.
    tx_to = convert_timezone(tx_to, 8, '-') # convert to UTC.
    queryset = FreeTicketData.query(FreeTicketData.is_valid==is_valid,
            FreeTicketData.transaction_date>=tx_from, FreeTicketData.transaction_date<=tx_to)

    for org_uuid in org_uuids:
        theaterorg_key = ndb.Key(TheaterOrganization, str(org_uuid))
        theaterorg = theaterorg_key.get()

        if theaterorg is not None:
            if theater_id and theater_id is not None:
                theater_id = int(theater_id)
                theater_key = ndb.Key(Theater, theater_id, parent=theaterorg_key)
                theater = theater_key.get()

                if theater is not None:
                    theaters.append(theater_key)
            else:
                theater_keys = Theater.query(ancestor=theaterorg_key).fetch(keys_only=True)
                theaters.extend(theater_keys)

    if org_uuids:
        queryset = queryset.filter(FreeTicketData.theater.IN(theaters))

    if discount_type and discount_type is not None:
        queryset = queryset.filter(FreeTicketData.discount_type.IN(discount_type))

    if platform and platform is not None and platform != 'not-yet-implemented':
        queryset = queryset.filter(FreeTicketData.platform == platform)
    elif platform == 'not-yet-implemented':
        queryset = queryset.filter(FreeTicketData.platform == None)

    queryset = queryset.order(-FreeTicketData.transaction_date, FreeTicketData.key)
    return queryset


def refresh_movie_gmoviesdigitalventures(movie_id):
    log.debug("refresh_movie_gmoviesdigitalventures, movie_id: %s..." % movie_id)

    try:
        taskqueue.add(url='/tasks/refresh-movies/gmovies-digital-ventures', method='POST', params={'movie_id': movie_id})
    except Exception, e:
        log.warn("ERROR!, refresh_movie_gmoviesdigitalventures...")
        log.error(e)

def gmoviesdigitalventures_blockedscreening():
    log.debug("gmoviesdigitalventures_blockedscreening, start...")

    try:
        taskqueue.add(url='/tasks/gmovies-digital-ventures/blocked-screening', method='GET')
    except Exception, e:
        log.warn("ERROR!, gmoviesdigitalventures_blockedscreening...")
        log.error(e)