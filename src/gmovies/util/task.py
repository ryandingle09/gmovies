from __future__ import absolute_import

import logging
import re

from urlparse import urlparse, urljoin


log = logging.getLogger(__name__)


def is_from_flixster(imgurl):
    """
    If image source came from flixster, replace image source from 'ori', which is the largest
	resolution and size flixster provides causing GAE to raise connection error, with '320' for a 
	smaller resolution
    """

    imgsrc = urlparse(imgurl)

    if re.search('resizing.flixster.com', imgsrc.netloc) is not None:
        return None

    if re.search('poster_default_thumb', imgsrc.path) is not None:
        return None

    if re.search('flixster.com', imgsrc.netloc) is not None:
        new_size = None

        if re.search('_ori', imgsrc.path) is not None:
            log.info("Image Size: Original")
            new_size = imgsrc.path.replace('_ori', '_320')
        elif re.search('_tmb', imgsrc.path) is not None:
            log.info("Image Size: Thumbnail")
            new_size = imgsrc.path.replace('_tmb', '_320')
        elif re.search('_del', imgsrc.path) is not None:
            log.info("Image Size: Detailed")
            new_size = imgsrc.path.replace('_del', '_320')
        elif re.search('_pro', imgsrc.path) is not None:
            log.info("Image Size: Profile")
            new_size = imgsrc.path.replace('_pro', '_320')

        new_src = urljoin(imgsrc.geturl(), new_size)

        log.info("Fetching movie poster from ORIGINAL: {0} -- NEW SRC: {1}".format(imgurl, new_src))

        return new_src

    return imgurl