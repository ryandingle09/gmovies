from __future__ import absolute_import

import logging

from StringIO import StringIO
import flask

class InRequestContextLoggingHandler(logging.Handler):
    def __init__(self, level=logging.NOTSET):
        logging.Handler.__init__(self, level)

        # Always define a format
        self.__default_formatter = logging.Formatter("%(asctime)s|%(levelname)s\t  %(message)s", 
                                                     datefmt="%I:%M:%S %p")

    def emit(self, rec):
        if flask.has_request_context():
            self.__bind_stream_if_necessary()
            return flask.g.gmovies_logger.emit(rec)
        else:
            return None

    def __bind_stream_if_necessary(self):
        if not getattr(flask.g, 'gmovies_logger', None):
            log_target = StringIO()
            flask.g.gmovies_logger_target = log_target
            flask.g.gmovies_logger = logging.StreamHandler(stream=log_target)
            flask.g.gmovies_logger.setFormatter(self.__default_formatter)

def get_current_context_log():
    return flask.g.gmovies_logger_target.getvalue()

def bind_context_logger_to_root():
    root_logger = logging.getLogger()
    root_logger.addHandler(LOG_HANDLER)

    global __bind_count
    __bind_count += 1

def unbind_context_logger_from_root():
    global __bind_count
    __bind_count -= 1

    if __bind_count <= 0:
        root_logger = logging.getLogger()
        root_logger.removeHandler(LOG_HANDLER)
        __bind_count = 0


LOG_HANDLER = InRequestContextLoggingHandler()
__bind_count = 0
