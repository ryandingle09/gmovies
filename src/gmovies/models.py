import base64
import binascii
import json
import logging
import os
import random
import re
import requests
import string

from Crypto.Hash import SHA
from datetime import date, datetime, time, timedelta
from decimal import Decimal
from itertools import groupby
from operator import attrgetter
from uuid import uuid4 as uuid_gen, UUID
from warnings import warn

from google.appengine.api import memcache
from google.appengine.ext import ndb
from google.appengine.api.images import get_serving_url

import bank_transfer as bt_settings

from gmovies.exceptions.api import BadValueException
from gmovies.indexes import geolocation
from gmovies.util import make_enum
from gmovies.util.id_encoder import decoded_theater_id, encoded_theater_id, encode_uuid
from gmovies.util.model_utils import camelcase


log = logging.getLogger(__name__)

DATE_FORMAT = '%d %b %Y'
TIME_FORMAT = '%H:%M'
DATETIME_FORMAT = DATE_FORMAT + ' ' + TIME_FORMAT
SLOT_KEY_FORMAT = '%Y%m%d'
DEFAULT_RUNTIME_MINS = '180.0'


# Custom Decimal Property
# Decimal property ideal to store currency values, such as $20.34
# See https://developers.google.com/appengine/docs/python/ndb/subclassprop
class DecimalProperty(ndb.IntegerProperty):
    def _validate(self, value):
        if not isinstance(value, (Decimal, str, unicode, int, long)):
            raise TypeError('Expected a Decimal, str, unicode, int or long an got instead %s' % repr(value))

    def _to_base_type(self, value):
        return int(Decimal(value) * 100)

    def _from_base_type(self, value):
        return Decimal(value)/Decimal(100)


def _handle_callbacks(cb_key):
    def _bind_handler(tx, cb):
        if cb_key not in tx.workspace:
            tx.workspace[cb_key] = []

        tx.workspace[cb_key].append(cb)

    def _trigger_cb_handler(tx, *args, **kwargs):
        if cb_key in tx.workspace:
            for cb in tx.workspace[cb_key]:
                cb(tx, *args, **kwargs)

    return _bind_handler, _trigger_cb_handler


# Movies and movie schedule-related entities

class Feed(ndb.Model):
    name = ndb.StringProperty(required=True)
    image_density = ndb.StringProperty(required=True)
    movie_fields_blacklist = ndb.StringProperty(repeated=True)
    movie_fields_whitelist = ndb.StringProperty(repeated=True)
    allow_movie_creation = ndb.BooleanProperty(default=True)
    heuristics_priority = ndb.IntegerProperty(default=99)


class TheaterOrganization(ndb.Model):
    name = ndb.StringProperty(required=True)
    pubkey = ndb.TextProperty()
    default_client_control = ndb.JsonProperty()
    client_control = ndb.JsonProperty()
    template = ndb.StringProperty(indexed=False)
    template_image = ndb.BlobProperty()
    ticket_remarks = ndb.JsonProperty(indexed=False)

    # additional field/s for movie ticket. added 09/15/2014.
    movie_ticket_template = ndb.StringProperty(indexed=False)

    # additional field/s for QRCode. added 10/23/2014.
    is_qrcode_ticket = ndb.BooleanProperty(default=False)

    # additional field for old ticket template for Ayala Malls. Added 11/3/2014.
    is_old_ticket_template = ndb.BooleanProperty(default=False)

    # access token property.
    access_code = ndb.StringProperty(indexed=False)

    # additional ticket template. added 2/12/2015.
    theaterorg_ticket_template = ndb.StringProperty(indexed=False)
    is_theaterorg_ticket_template = ndb.BooleanProperty(default=False)

    # additional field/s for adding new theater organizations. added 4/7/2015.
    is_published = ndb.BooleanProperty(default=False)
    sort_priority = ndb.IntegerProperty(default=99)

    # additional field for master switch in enabling/disabling reservations. added 9/17/2018
    allow_reservation_master_switch = ndb.IntegerProperty(default=0)

    def to_entity(self):
        return {'name': self.name, 'default_client_control': self.default_client_control, 'client_control': self.client_control}

    @classmethod
    def get_access_code(cls, theaterorg_key):
        theater_org = ndb.Key(TheaterOrganization, theaterorg_key).get()

        return theater_org.access_code

class ClientInitiatedPaymentSettingsCinema(ndb.Model):
    payment_identifier = ndb.StringProperty()
    default_params = ndb.PickleProperty() # default parameters for client-initiated payment form.
    settings = ndb.PickleProperty()
    cinema = ndb.StringProperty()

class ClientInitiatedPaymentSettings(ndb.Model):
    payment_identifier = ndb.StringProperty()
    default_params = ndb.PickleProperty() # default parameters for client-initiated payment form.
    settings = ndb.PickleProperty()
    # Added 10/17/2017 for ATC multiple MID
    cinema_settings = ndb.StructuredProperty(ClientInitiatedPaymentSettingsCinema, repeated=True)

    @classmethod
    def get_settings(cls, key, gateway, cinema=None):
        settings = ndb.Key(ClientInitiatedPaymentSettings, gateway, parent=key).get()

        if settings:
            cinema_names = []

            if settings.cinema_settings:
                cinema_names = [s.cinema for s in settings.cinema_settings]

            if cinema and cinema in cinema_names:
                cinema_settings = settings.get_cinema_settings(cinema)

                if cinema_settings:
                    return cinema_settings

            return settings

        parent_key = key.parent()

        if parent_key:
            return cls.get_settings(parent_key, gateway)
        else:
            settings = cls()
            settings.default_params = {}
            settings.settings = {}

        return settings

    def get_cinema_settings(self, cinema):
        for settings in self.cinema_settings:
            if cinema == settings.cinema:
                return settings

        return None

    def check_cinema_settings(self, cinema_new, cinema_old=None):
        is_not_existing = False
        cinema_names = [settings.cinema for settings in self.cinema_settings]

        if cinema_old and cinema_old in cinema_names:
            cinema_names.remove(cinema_old)

        if cinema_new not in cinema_names:
            is_not_existing = True

        return is_not_existing

class PaymentOptionSettings(ndb.Model):
    name = ndb.StringProperty(required=True)
    type = ndb.StringProperty()
    payment_gateway = ndb.StringProperty()
    image_url = ndb.StringProperty()
    header_url = ndb.StringProperty()
    instruction_url = ndb.StringProperty()
    default_params = ndb.StringProperty(repeated=True)
    allow_discount_codes_whitelist = ndb.JsonProperty()
    is_required_discount_codes = ndb.BooleanProperty(default=False)

    def get_paymentoption_image(self):
        image_url = ''

        try:
            if self.image_url:
                log.debug("get_paymentoption_image, image_url attribute...")

                return self.image_url

            cache_key = 'paymentoption::image::identifier::%s' % self.key.id()
            cache_hit = memcache.get(cache_key)

            if cache_hit:
                log.debug("get_paymentoption_image, %s image cache hit on, %s..." % (self.key.id(), cache_key))

                return cache_hit

            image_blob = AssetsImageBlob.query(AssetsImageBlob.assets_group=='payment-options-image', ancestor=self.key).get()

            if image_blob and image_blob.image:
                image_url = get_serving_url(image_blob.image)

                memcache.set(cache_key, image_url)
        except Exception, e:
            log.warn("ERROR!, get_paymentoption_image, %s..." % self.key.id())
            log.error(e)

        return image_url

    def get_promoheader_image(self):
        image_url = ''

        try:
            if self.header_url:
                log.debug("get_promoheader_image, header_url attribute...")

                return self.header_url

            cache_key = 'paymentoption::image::header::identifier::%s' % self.key.id()
            cache_hit = memcache.get(cache_key)

            if cache_hit:
                log.debug("get_promoheader_image, %s image cache hit on, %s..." % (self.key.id(), cache_key))

                return cache_hit

            image_blob = AssetsImageBlob.query(AssetsImageBlob.assets_group=='payment-options-image-header', ancestor=self.key).get()

            if image_blob and image_blob.image:
                image_url = get_serving_url(image_blob.image)

                memcache.set(cache_key, image_url)
        except Exception, e:
            log.warn("ERROR!, get_promoheader_image, %s..." % self.key.id())
            log.error(e)

        return image_url

    def is_discount_codes_whitelisted(self, theater):
        if not theater:
            return False

        if not self.allow_discount_codes_whitelist:
            return False

        if type(self.allow_discount_codes_whitelist) != dict:
            return False

        if theater.key.parent().id() not in self.allow_discount_codes_whitelist:
            return False

        if encoded_theater_id(theater.key) in self.allow_discount_codes_whitelist[theater.key.parent().id()]:
            return True

        return False

    def to_entity(self, theater=None):
        e = self.to_dict()
        e.pop('allow_discount_codes_whitelist', None)
        e.pop('header_url', None)
        e.pop('image_url', None)

        e['allow_discount_codes'] = self.is_discount_codes_whitelisted(theater)
        e['payment_option_image'] = self.get_paymentoption_image()
        e['promo_header_image'] = self.get_promoheader_image()

        return e


class Cinema(ndb.Model):
    name = ndb.StringProperty(required=True)
    seat_count = ndb.StringProperty()
    seat_map = ndb.PickleProperty()
    location = ndb.GeoPtProperty()

    # additional fields for Rockwell. added 10/08/2014. deprecated.
    rockwell_cinema_id = ndb.IntegerProperty()
    rockwell_cinema_name = ndb.StringProperty()

    # additional fields for the seat mapping. added 03/11/2015.
    feed_code = ndb.StringProperty() # cinema id from third party backend.
    feed_cinema_name = ndb.StringProperty() # cinema name from third party backend.
    feed_seat_map = ndb.PickleProperty() # seat map info (list of list of dictionary).

    # Added field for Rockwell Premium Cinema. added 01/30/2017
    popcorn_price = ndb.StringProperty()

    def to_entity(self):
        e = self.to_dict()
        e.pop('feed_code', None)
        e.pop('feed_cinema_name', None)
        e.pop('feed_seat_map', None)
        e.pop('rockwell_cinema_id', None)
        e.pop('rockwell_cinema_name', None)

        if e['location']:
            e['location'] = {'lon': self.location.lon, 'lat': self.location.lat}

        if not e['seat_count']:
            e['seat_count'] = '0'

        if not e['popcorn_price']:
            e.pop('popcorn_price', None)

        return e

    def to_entity2(self):
        e = self.to_dict()
        e.pop('feed_code', None)
        e.pop('feed_cinema_name', None)
        e.pop('feed_seat_map', None)
        e.pop('rockwell_cinema_id', None)
        e.pop('rockwell_cinema_name', None)
        e.pop('seat_map', None)

        if e['location']:
            e['location'] = {'lon': self.location.lon, 'lat': self.location.lat}

        if not e['seat_count']:
            e['seat_count'] = '0'

        if not e['popcorn_price']:
            e.pop('popcorn_price', None)

        return e

    def to_minimum_entity(self):
        e = self.to_dict()
        e.pop('feed_code', None)
        e.pop('feed_cinema_name', None)
        e.pop('feed_seat_map', None)
        e.pop('rockwell_cinema_id', None)
        e.pop('rockwell_cinema_name', None)
        e.pop('seat_map', None)

        if e['location']:
            e['location'] = {'lon': self.location.lon, 'lat': self.location.lat}

        if not e['seat_count']:
            e['seat_count'] = '0'

        if not e['popcorn_price']:
            e.pop('popcorn_price', None)

        return e

class Theater(ndb.Model):
    name = ndb.StringProperty(required=True)
    location = ndb.GeoPtProperty()
    cinemas = ndb.StructuredProperty(Cinema, repeated=True)
    org_theater_code = ndb.StringProperty()
    address = ndb.StringProperty(indexed=False)
    is_published = ndb.BooleanProperty(default=False)

    # reservation and discount code on/off switch.
    allow_reservation = ndb.BooleanProperty(default=False)
    allow_reservation_platform = ndb.StringProperty(repeated=True)
    allow_discount_code = ndb.BooleanProperty(default=False)

    # additional field/s for payment options on/off switch.
    payment_options = ndb.StringProperty(repeated=True) # iOS
    payment_options_android = ndb.StringProperty(repeated=True) # Android
    payment_options_website = ndb.StringProperty(repeated=True) # Website
    payment_gateway = ndb.StringProperty() # Tagging for BPI Cardholder Promo (Credit-Card).

    # additional field/s for MGi integrated cinema partners.
    branch_id = ndb.StringProperty()

    # additional field/s for multiple tickets. added 01/06/2015.
    is_multiple_tickets = ndb.BooleanProperty(default=False)

    # additional field/s for error message for non-partner/disabled booking feature. added 04/20/2015.
    is_partner = ndb.BooleanProperty(default=False)
    error_title = ndb.StringProperty()
    error_message = ndb.TextProperty()

    # additional field to get the last updated Theater entity. added 05/06/2015.
    datetime_updated = ndb.DateTimeProperty(auto_now=True)

    # additional field/s for error message for no seat maps. added 05/26/3015.
    error_message_seatmaps_android = ndb.TextProperty()
    error_message_seatmaps_ios = ndb.TextProperty()

    # additional field for theater logo. added 09/28/2016.
    image_url = ndb.StringProperty()

    # additional field for CityMall. added 03/26/2018.
    location_code = ndb.StringProperty()
    
    # additional field for enabling/disabling allow reservation master switch. added 9/20/2018
    previous_reservation_platform = ndb.StringProperty(repeated=True)
    
    blacklist = ['cinemas.name']

    @classmethod
    def query_by_code(cls, org_code):
        t = cls.query(cls.org_theater_code == org_code).fetch()

        return t

    def get_cinema_entity(self, cinema_name):
        cinemas = self.cinemas

        for c in cinemas:
            if cinema_name == c.name:
                return c

    def get_convenience_fees(self, entity_class):
        parent_key = self.key.parent()
        settings = ConvenienceFeeSettings.get_settings(parent_key, entity_class)

        return {'convenience_fees': settings.convenience_fees, 'convenience_fees_message': settings.convenience_fees_message}

    def get_link_name(self):
        link_name = '-'.join(self.name.lower().split(' '))

        return link_name.strip()

    def get_theater_image(self):
        image_url = ''

        try:
            if self.image_url:
                log.debug("get_theater_image, image_url attribute...")

                return self.image_url

            theater_id = encoded_theater_id(self.key)
            cache_key = 'theater::image::key::%s' % theater_id
            cache_hit = memcache.get(cache_key)

            if cache_hit:
                log.debug("get_theater_image, %s image cache hit on, %s..." % (theater_id, cache_key))

                return cache_hit

            image_blob = AssetsImageBlob.query(AssetsImageBlob.assets_group=='theaters-image', ancestor=self.key).get()

            if image_blob and image_blob.image:
                image_url = get_serving_url(image_blob.image)

                memcache.set(cache_key, image_url)
        except Exception, e:
            log.warn("ERROR!, get_theater_image, %s..." % theater_id)
            log.error(e)

        return image_url

    def to_entity(self):
        e = self.to_dict()
        e.pop('datetime_updated', None)
        e.pop('image_url', None)
        e.pop('is_multiple_tickets', None)
        e.pop('error_message_seatmaps_android', None)
        e.pop('error_message_seatmaps_ios', None)

        if not e['address']:
            e['address'] = 'unavailable'

        if not e['name']:
            e['name'] = 'unavailable'

        if not e['error_title']:
            e['error_title'] = 'Oops, sorry!'

        if not e['error_message']:
            e['error_message'] = 'Mobile reservation is currently not available for this cinema.'

        if not e['payment_gateway']:
            e['payment_gateway'] = ''

        if e['location']:
            e['location'] = {'lon': self.location.lon, 'lat': self.location.lat}

        e['cinemas'] = [c.to_entity() for c in self.cinemas]
        e['theaterorg_id'] = str(self.key.parent().id())

        if e['allow_reservation']:
            get_convenience_fees = self.get_convenience_fees('theaterorgs')
            e['convenience_fees'] = get_convenience_fees['convenience_fees']
            e['convenience_fees_message'] = get_convenience_fees['convenience_fees_message']
        else:
            e['convenience_fees'] = {}
            e['convenience_fees_message'] = {}

        return e

    def to_entity2(self, has_seatmap=False):
        e = self.to_dict()
        e.pop('allow_discount_code', None)
        e.pop('branch_id', None)
        e.pop('datetime_updated', None)
        e.pop('error_message_seatmaps_android', None)
        e.pop('error_message_seatmaps_ios', None)
        e.pop('image_url', None)
        e.pop('is_multiple_tickets', None)
        e.pop('is_partner', None)
        e.pop('is_published', None)
        #e.pop('org_theater_code', None)
        e.pop('payment_gateway', None)

        e['convenience_fees'] = {}
        e['convenience_fees_message'] = {}
        e['link_name'] = self.get_link_name()
        e['theater_image'] = self.get_theater_image()
        e['theaterorg_id'] = str(self.key.parent().id())
        #e['org_theater_code'] = str(self.org_theater_code())

        if not e['address']:
            e['address'] = ''

        if not e['error_title']:
            e['error_title'] = 'Oops, sorry!'

        if not e['error_message']:
            e['error_message'] = 'Mobile reservation is currently not available for this cinema.'

        if e['location']:
            e['location'] = {'lon': self.location.lon, 'lat': self.location.lat}

        if e['allow_reservation']:
            get_convenience_fees = self.get_convenience_fees('theaterorgs')

            e['convenience_fees'] = get_convenience_fees['convenience_fees']
            e['convenience_fees_message'] = get_convenience_fees['convenience_fees_message']

        if has_seatmap:
            e['cinemas'] = [c.to_entity() for c in self.cinemas]
        else:
            e['cinemas'] = [c.to_entity2() for c in self.cinemas]

        return e

    def to_entity_seatmap(self, cinema_name):
        e = self.to_dict()
        cinema = self.get_cinema_entity(cinema_name)

        e.pop('allow_discount_code', None)
        e.pop('branch_id', None)
        e.pop('cinemas', None)
        e.pop('datetime_updated', None)
        e.pop('error_message_seatmaps_android', None)
        e.pop('error_message_seatmaps_ios', None)
        e.pop('image_url', None)
        e.pop('is_multiple_tickets', None)
        e.pop('is_partner', None)
        e.pop('is_published', None)
        e.pop('org_theater_code', None)
        e.pop('payment_gateway', None)

        e['convenience_fees'] = {}
        e['convenience_fees_message'] = {}
        e['link_name'] = self.get_link_name()
        e['theater_image'] = self.get_theater_image()
        e['theaterorg_id'] = str(self.key.parent().id())
        e['cinema'] = cinema.to_entity()

        if not e['address']:
            e['address'] = ''

        if not e['error_title']:
            e['error_title'] = 'Oops, sorry!'

        if not e['error_message']:
            e['error_message'] = 'Mobile reservation is currently not available for this cinema.'

        if e['location']:
            e['location'] = {'lon': self.location.lon, 'lat': self.location.lat}

        if e['allow_reservation']:
            get_convenience_fees = self.get_convenience_fees('theaterorgs')

            e['convenience_fees'] = get_convenience_fees['convenience_fees']
            e['convenience_fees_message'] = get_convenience_fees['convenience_fees_message']

        return e

    def to_minimum_entity(self):
        e = self.to_dict()
        e.pop('allow_reservation', None)
        e.pop('allow_reservation_platform', None)
        e.pop('allow_discount_code', None)
        e.pop('datetime_updated', None)
        e.pop('error_message', None)
        e.pop('error_message_seatmaps_android', None)
        e.pop('error_message_seatmaps_ios', None)
        e.pop('error_title', None)
        e.pop('is_multiple_tickets', None)
        e.pop('is_partner', None)
        e.pop('is_published', None)
        e.pop('payment_options', None)
        e.pop('payment_options_android', None)
        e.pop('payment_options_website', None)
        e.pop('previous_reservation_platform', None)

        if not e['address']:
            e['address'] = ''

        if not e['name']:
            e['name'] = ''

        if e['location']:
            e['location'] = {'lon': self.location.lon, 'lat': self.location.lat}

        e['cinemas'] = [c.to_minimum_entity() for c in self.cinemas]

        return e

    def _post_put_hook(self, future):
        geolocation.index_theater(self)


class FeedCorrelation(ndb.Model):
    org_key = ndb.KeyProperty(kind=Feed, required=True)
    movie_id = ndb.StringProperty()
    variant = ndb.StringProperty()

    def to_entity(self):
        raise ValueError("Cannot convert FeedCorrelation directly to API entity")


class TicketTemplateImageBlob(ndb.Model):
    image = ndb.BlobProperty(required=True)
    template_type = ndb.StringProperty() # tag ticket template. generic, movie, theaterorg. added 2/12/2015.


class Movie(ndb.Model):
    canonical_title = ndb.StringProperty(required=True)
    correlation_title = ndb.StringProperty(repeated=True)
    per_org_titlemap = ndb.PickleProperty()
    movie_correlation = ndb.StructuredProperty(FeedCorrelation, repeated=True)
    ratings = ndb.JsonProperty(indexed=True)
    advisory_rating = ndb.StringProperty()
    genre = ndb.StringProperty()
    cast = ndb.StringProperty(repeated=True)
    synopsis = ndb.TextProperty()
    runtime_mins = ndb.FloatProperty()
    slottime_mins = ndb.FloatProperty()
    release_date = ndb.DateProperty()
    variants = ndb.StringProperty(repeated=True)
    is_expired = ndb.BooleanProperty(default=False)
    is_inactive = ndb.BooleanProperty(default=False)
    is_details_locked = ndb.BooleanProperty(default=False)

    # added 09/15/2014.
    is_active_ticket_template = ndb.BooleanProperty(default=False)

    # additional field/s for Rockwell. added 10/08/2014. deprecated. 01/30/2015
    rockwell_movie_ids = ndb.IntegerProperty(repeated=True)

    # additional field/s for CMS. added 01/27/2015.
    alternate_posters = ndb.JsonProperty()
    trailer = ndb.StringProperty()
    searchable_words = ndb.StringProperty(repeated=True)
    is_published = ndb.BooleanProperty(default=True)
    is_featured = ndb.BooleanProperty(default=False)

    # additional field/s for Globe Events. added 05/16/2018.
    is_event = ndb.BooleanProperty(default=False)

    whitelist = ['resolution', 'application_type']

    def has_ticket_template_image(self):
        ticket_template_image = TicketTemplateImageBlob.query(TicketTemplateImageBlob.template_type=='movie', ancestor=self.key).get()

        if ticket_template_image:
            if ticket_template_image.image:
                return True

        return False

    def has_schedules(self):
        schedules_queryset = Schedule.query(Schedule.slots.movie==self.key, Schedule.show_date>=date.today()).order(-Schedule.show_date)

        if schedules_queryset.count() > 0:
            return True

        return False

    def get_poster_url(self, posters_args={}):
        portrait_poster = ''
        landscape_poster = ''
        resolution = posters_args.get('resolution', 'uploaded')
        application_type = posters_args.get('application_type', 'mobile-app')
        cache_key_portrait = "movie::poster::%s::portrait::%s::%s" % (self.key.id(), resolution, application_type)
        cache_key_landscape = "movie::poster::%s::landscape::%s::%s" % (self.key.id(), resolution, application_type)

        try:
            cache_hit_portrait = memcache.get(cache_key_portrait)

            if cache_hit_portrait:
                log.debug("get_poster_url, portrait, %s poster cache hit on: %s" % (self.canonical_title, cache_key_portrait))

                portrait_poster = cache_hit_portrait
                if portrait_poster and portrait_poster[-3:] != '=s0':
                    portrait_poster += "=s0"
            else:
                portrait_poster_image_blob = PosterImageBlob.query(PosterImageBlob.resolution==resolution,
                        PosterImageBlob.orientation=='portrait', PosterImageBlob.application_type==application_type,
                        PosterImageBlob.is_background==False, ancestor=self.key).get()

                if portrait_poster_image_blob and portrait_poster_image_blob.image:
                    portrait_poster = get_serving_url(portrait_poster_image_blob.image)
                    if portrait_poster and portrait_poster[-3:] != '=s0':
                        portrait_poster += "=s0"

                    memcache.set(cache_key_portrait, portrait_poster)
        except Exception, e:
            log.warn("ERROR!, get_poster_url, portrait...")
            log.error(e)

        try:
            cache_hit_landscape = memcache.get(cache_key_landscape)

            if cache_hit_landscape:
                log.debug("get_poster_url, landscape, %s poster cache hit on: %s" % (self.canonical_title, cache_key_landscape))

                landscape_poster = cache_hit_landscape
                if landscape_poster and landscape_poster[-3:] != '=s0':
                    landscape_poster += "=s0"
            else:
                landscape_poster_image_blob = PosterImageBlob.query(PosterImageBlob.resolution==resolution,
                        PosterImageBlob.orientation=='landscape', PosterImageBlob.application_type==application_type,
                        PosterImageBlob.is_background==False, ancestor=self.key).get()

                if landscape_poster_image_blob and landscape_poster_image_blob.image:
                    landscape_poster = get_serving_url(landscape_poster_image_blob.image)
                    if landscape_poster and landscape_poster[-3:] != '=s0':
                        landscape_poster += "=s0"

                    memcache.set(cache_key_landscape, landscape_poster)
        except Exception, e:
            log.warn("ERROR!, get_poster_url, landscape...")
            log.error(e)

        return portrait_poster, landscape_poster

    def get_background_poster_url(self, posters_args={}):
        background_poster = ''
        application_type = posters_args.get('application_type', 'mobile-app')
        cache_key = "movie::background::poster::%s::%s" % (self.key.id(), application_type)

        try:
            cache_hit = memcache.get(cache_key)

            if cache_hit:
                log.debug("get_background_poster_url, %s background poster cache hit on: %s" % (self.canonical_title, cache_key))

                return cache_hit

            background_poster_image_blob = PosterImageBlob.query(PosterImageBlob.application_type==application_type,
                    PosterImageBlob.is_background==True, ancestor=self.key).get()

            if background_poster_image_blob and background_poster_image_blob.image:
                background_poster = get_serving_url(background_poster_image_blob.image)

                memcache.set(cache_key, background_poster)
        except Exception, e:
            log.warn("ERROR!, get_background_poster_url...")
            log.error(e)

        return background_poster

    def get_link_name(self):
        link_name = ''

        try:
            title_words = self.correlation_title[0].split(':')[-1].split('_')
            link_name = '-'.join(title_words)
        except Exception, e:
            log.warn("ERROR!, get_link_name...")
            log.error(e)

        return link_name

    def is_not_future_release_date(self):
        # assume entries without a release date are now showing, for backwards compatibility purposes (data).
        if not self.release_date:
            return True

        return date.today() >= self.release_date

    is_showing = ndb.ComputedProperty(is_not_future_release_date)

    def bind_org_title(self, org_id, variant, title):
        if org_id not in self.per_org_titlemap:
            self.per_org_titlemap[org_id] = {}

        self.per_org_titlemap[org_id][variant] = title

    def to_entity(self, total_reviews={}, average_rating={}, posters_args={}, portrait_default_poster='', landscape_default_poster=''):
        e = self.to_dict()
        portrait_poster, landscape_poster = self.get_poster_url(posters_args=posters_args)

        e.pop('correlation_title', None)
        e.pop('movie_correlation', None)
        e.pop('is_active_ticket_template', None)
        e.pop('is_details_locked', None)
        e.pop('is_featured', None)
        e.pop('is_published', None)
        e.pop('ticket_template_image', None)
        e.pop('rockwell_movie_ids', None)
        e.pop('searchable_words', None)

        if e['release_date']:
            e['release_date'] = e['release_date'].strftime(DATE_FORMAT)

        if not e['runtime_mins']:
            e['runtime_mins'] = 0

        if not self.alternate_posters or self.alternate_posters is None:
            e['alternate_posters'] = {}

        if portrait_poster:
            e['poster'] = portrait_poster
        else:
            e['poster'] = portrait_default_poster

        if landscape_poster:
            e['poster_landscape'] = landscape_poster
        else:
            e['poster_landscape'] = landscape_default_poster

        e['background_poster'] = self.get_background_poster_url(posters_args=posters_args)
        e['has_schedules'] = self.has_schedules()
        e['link_name'] = self.get_link_name()

        if str(self.key.id()) in total_reviews:
            e['total_reviews'] = total_reviews[str(self.key.id())]
        else:
            e['total_reviews'] = 0

        if str(self.key.id()) in average_rating:
            e['average_rating'] = average_rating[str(self.key.id())]
        else:
            e['average_rating'] = "0.0"

        return e

    def to_entity2(self, posters_args={}, portrait_default_poster='', landscape_default_poster=''):
        e = self.to_dict()
        portrait_poster, landscape_poster = self.get_poster_url(posters_args=posters_args)

        e.pop('alternate_posters', None)
        e.pop('correlation_title', None)
        e.pop('movie_correlation', None)
        e.pop('is_active_ticket_template', None)
        e.pop('is_details_locked', None)
        e.pop('is_inactive', None)
        e.pop('is_published', None)
        e.pop('per_org_titlemap', None)
        e.pop('rockwell_movie_ids', None)
        e.pop('searchable_words', None)
        e.pop('slottime_mins', None)
        e.pop('ticket_template_image', None)

        e['has_schedules'] = self.has_schedules()
        e['link_name'] = self.get_link_name()
        e['poster'] = portrait_default_poster
        e['poster_landscape'] = landscape_default_poster

        # ensure that the value is rotten tomatoes ratings.
        # ef93f6b2-71b7-4041-bd3c-46e9784c1dbe is the org/feed uuid for rotten tomatoes.
        if e['ratings'] and 'ef93f6b2-71b7-4041-bd3c-46e9784c1dbe' in e['ratings']:
            e['ratings'] = e['ratings']['ef93f6b2-71b7-4041-bd3c-46e9784c1dbe']
        else:
            e['ratings'] = {}

        if e['release_date']:
            e['release_date'] = e['release_date'].strftime(DATE_FORMAT)

        if not e['runtime_mins']:
            e['runtime_mins'] = 0.0

        if not e['genre']:
            e['genre'] = ''

        if not e['trailer']:
            e['trailer'] = ''

        if portrait_poster:
            e['poster'] = portrait_poster

        if landscape_poster:
            e['poster_landscape'] = landscape_poster

        return e

    def to_entity_movie_info(self, posters_args={}, portrait_default_poster='', landscape_default_poster=''):
        e = self.to_dict()
        portrait_poster, landscape_poster = self.get_poster_url(posters_args=posters_args)

        e.pop('correlation_title', None)
        e.pop('is_active_ticket_template', None)
        e.pop('is_details_locked', None)
        e.pop('is_featured', None)
        e.pop('is_inactive', None)
        e.pop('is_published', None)
        e.pop('movie_correlation', None)
        e.pop('rockwell_movie_ids', None)
        e.pop('searchable_words', None)
        e.pop('ticket_template_image', None)

        if e['release_date']:
            e['release_date'] = e['release_date'].strftime(DATE_FORMAT)

        if not e['runtime_mins']:
            e['runtime_mins'] = 0

        if not self.alternate_posters or self.alternate_posters is None:
            e['alternate_posters'] = {}

        if portrait_poster:
            e['poster'] = portrait_poster
        else:
            e['poster'] = portrait_default_poster

        if landscape_poster:
            e['poster_landscape'] = landscape_poster
        else:
            e['poster_landscape'] = landscape_default_poster

        e['background_poster'] = self.get_background_poster_url(posters_args=posters_args)
        e['link_name'] = self.get_link_name()

        return e

    def to_entity_comingsoon(self, posters_args={}, portrait_default_poster='', landscape_default_poster=''):
        e = self.to_dict()
        portrait_poster, landscape_poster = self.get_poster_url(posters_args=posters_args)

        e.pop('correlation_title', None)
        e.pop('is_active_ticket_template', None)
        e.pop('is_details_locked', None)
        e.pop('is_expired', None)
        e.pop('is_inactive', None)
        e.pop('is_showing', None)
        e.pop('is_published', None)
        e.pop('movie_correlation', None)
        e.pop('per_org_titlemap', None)
        e.pop('rockwell_movie_ids', None)
        e.pop('searchable_words', None)
        e.pop('slottime_mins', None)
        e.pop('ticket_template_image', None)
        e.pop('variants', None)

        if e['release_date']:
            e['release_date'] = e['release_date'].strftime(DATE_FORMAT)

        if not e['runtime_mins']:
            e['runtime_mins'] = 0

        if not self.alternate_posters or self.alternate_posters is None:
            e['alternate_posters'] = {}

        if portrait_poster:
            e['poster'] = portrait_poster
        else:
            e['poster'] = portrait_default_poster

        if landscape_poster:
            e['poster_landscape'] = landscape_poster
        else:
            e['poster_landscape'] = landscape_default_poster

        e['background_poster'] = self.get_background_poster_url(posters_args=posters_args)
        e['has_schedules'] = self.has_schedules()
        e['link_name'] = self.get_link_name()

        return e

    def to_entity_schedule_movie_info(self, average_rating={}, posters_args={},
            portrait_default_poster='', landscape_default_poster=''):
        e = self.to_dict()
        portrait_poster, landscape_poster = self.get_poster_url(posters_args=posters_args)

        e.pop('alternate_posters', None)
        e.pop('cast', None)
        e.pop('correlation_title', None)
        e.pop('is_active_ticket_template', None)
        e.pop('is_details_locked', None)
        e.pop('is_featured', None)
        e.pop('is_expired', None)
        e.pop('is_inactive', None)
        e.pop('is_published', None)
        e.pop('is_showing', None)
        e.pop('movie_correlation', None)
        e.pop('per_org_titlemap', None)
        e.pop('ratings', None)
        e.pop('release_date', None)
        e.pop('rockwell_movie_ids', None)
        e.pop('searchable_words', None)
        e.pop('slottime_mins', None)
        e.pop('synopsis', None)
        e.pop('ticket_template_image', None)
        e.pop('variants', None)

        if not e['runtime_mins']:
            e['runtime_mins'] = 0

        if portrait_poster:
            e['poster'] = portrait_poster
        else:
            e['poster'] = portrait_default_poster

        if landscape_poster:
            e['poster_landscape'] = landscape_poster
        else:
            e['poster_landscape'] = landscape_default_poster

        if str(self.key.id()) in average_rating:
            e['average_rating'] = average_rating[str(self.key.id())]
        else:
            e['average_rating'] = "0.0"

        e['movie_id'] = self.key.id()
        e['link_name'] = self.get_link_name()

        return e

class MultiResImageBlob(ndb.Model):
    resolution = ndb.StringProperty(required=True)
    image = ndb.BlobKeyProperty(required=True)
    source_url = ndb.StringProperty()
    etag = ndb.StringProperty()
    lock_image = ndb.BooleanProperty(default=False)


class ScheduleTimeSlot(ndb.Model):
    start_time = ndb.TimeProperty(required=True)
    feed_code = ndb.StringProperty()
    feed_code_schedule = ndb.StringProperty()
    movie = ndb.KeyProperty(kind=Movie, required=True)
    variant = ndb.StringProperty()
    seating_type = ndb.StringProperty()
    price = DecimalProperty()

    # additional field/s for Rockwell. added 10/15/2014
    rockwell_movie_schedule_id = ndb.IntegerProperty()

    # additional field/s for Rockwell. added 01/26/2017.
    popcorn_price = DecimalProperty()

class Schedule(ndb.Expando):
    cinema = ndb.StringProperty()
    show_date = ndb.DateProperty()
    slots = ndb.StructuredProperty(ScheduleTimeSlot, repeated=True)
    reverse_lookup = ndb.PickleProperty()
    feed_codes = ndb.StringProperty(repeated=True)
    movie_schedule_id = ndb.IntegerProperty()

    # additional field/s for Rockwell. added 10/13/2014.
    rockwell_movie_schedule_ids = ndb.IntegerProperty(repeated=True)

    whitelist = ['theater', 'location.lat', 'location.lon', 'movie']
    filter_mappings = {'movie' : 'slots.movie', 'seating_type': 'slots.seating_type'}

    @classmethod
    def create_schedule(cls, **kwargs):
        if 'id' not in kwargs:
            kwargs['id'] = cls.make_slot_id(kwargs['cinema'], kwargs['show_date'])

        sched = cls(**kwargs)

        return sched

    def sorted_slots(self):
        return sorted(self.slots, key=attrgetter('start_time'))

    def add_timeslot(self, **kwargs):
        if not self.slots:
            self.slots = []

        timeslot = ScheduleTimeSlot(**kwargs)
        self.slots.append(timeslot)

        return timeslot

    def get_timeslot(self, time):
        for s in self.slots:
            if time == s.start_time:
                return s

        return None

    def get_feed_code(self, feed_code):
        for s in self.slots:
            if feed_code == s.feed_code:
                return s

        return None

    def _start_times(self):
        warn("_start_times is deprecated; caller must use slots property directly", DeprecationWarning, stacklevel=2)

        return [s.start_time for s in self.slots]

    def _feed_codes(self):
        warn("_feed_codes is deprecated; caller must use slots property directly", DeprecationWarning, stacklevel=2)

        return [s.feed_code for s in self.slots]

    def _to_entity_parent(self):
        return encoded_theater_id(self.key.parent())

    def __to_entity_id(self, movie_tup):
        encoded_movie_id = encode_uuid(UUID(movie_tup[0].id()))

        return "%s,%s,%s" % (self.key.id(), encoded_movie_id, movie_tup[1])

    def to_entity_with_slot_group(self, (movie, variant, price, seating_type, popcorn_price), slot_group):
        e = self.to_dict()
        e.pop('feed_codes', None)
        e.pop('movie_schedule_id', None)
        e.pop('reverse_lookup', None)
        e.pop('rockwell_movie_schedule_ids', None)
        e.pop('slots', None)

        slots = sorted([s for s in slot_group], key=attrgetter('start_time'))
        theater = self.key.parent().get()

        extra = {}
        extra['allow_discount_code'] = theater.allow_discount_code
        extra['allow_reservation'] = theater.allow_reservation
        extra['allow_reservation_platform'] = theater.allow_reservation_platform if theater.allow_reservation_platform else []

        e['extra'] = extra
        e['movie'] = movie.id() if movie else None
        e['price'] = str(price)
        e['seating_type'] = seating_type
        e['show_date'] = e['show_date'].strftime(DATE_FORMAT) if e['show_date'] else ''
        e['theater'] = encoded_theater_id(self.key.parent())
        e['variant'] = variant
        e['popcorn_price'] = str(popcorn_price)

        ###############
        #
        # Quick Fix: Greenhills 40 minutes on movie scheudles.
        # Will remove in the future when the Greenhills POS is ready/implemented.
        #
        ###############

        if str(self.key.parent().parent().id()) == '546029d5-8d61-4aa1-b788-bfd69cc6c96d':
            log.info("Greenhills Schedules, %s..." % str(self.key.parent().parent().id()))

            today = datetime.now() + timedelta(hours=8)
            timeslot_cutoff = today + timedelta(minutes=40)
            e['start_times'] = []

            for s in slots:
                if timeslot_cutoff <= datetime.strptime(e['show_date'] + " " + s.start_time.strftime(TIME_FORMAT), "%d %b %Y %H:%M"):
                    log.info("Greenhills Cutoff: %s" % str(timeslot_cutoff))

                    e['start_times'].append(s.start_time.strftime(TIME_FORMAT))
        else:
            log.info("Non-Greenhills Schedules, %s..." % str(self.key.parent().parent().id()))

            e['start_times'] = [s.start_time.strftime(TIME_FORMAT) for s in slots]

        if "None" == e['popcorn_price']:
            e.pop('popcorn_price', None)

        return e

    # used for website APIs/Endpoints.
    def to_entity_with_slot_group_and_movie_info(self, (movie, variant, price, seating_type, popcorn_price),
            slot_group, average_rating, posters_args, portrait_default_poster, landscape_default_poster):
        e = self.to_dict()
        e.pop('feed_codes', None)
        e.pop('movie_schedule_id', None)
        e.pop('reverse_lookup', None)
        e.pop('rockwell_movie_schedule_ids', None)
        e.pop('slots', None)

        movie = movie.get()
        theater = self.key.parent().get()
        slots = sorted([s for s in slot_group], key=attrgetter('start_time'))

        e['movie'] = None
        e['variant'] = variant
        e['price'] = str(price)
        e['theater'] = encoded_theater_id(self.key.parent())
        e['seating_type'] = seating_type
        e['popcorn_price'] = str(popcorn_price)
        e['start_times'] = [s.start_time.strftime(TIME_FORMAT) for s in slots]

        if e['show_date']:
            e['show_date'] = e['show_date'].strftime(DATE_FORMAT)

        if movie:
            e['movie'] = movie.to_entity_schedule_movie_info(average_rating=average_rating, posters_args=posters_args,
                    portrait_default_poster=portrait_default_poster, landscape_default_poster=landscape_default_poster)

        if not theater.allow_reservation:
            e['seating_type'] = ''

        if "None" == e['popcorn_price']:
            e.pop('popcorn_price', None)

        return e

    def to_entity(self):
        raise Exception('to_entity() currently not supported for ScheduleSlots: caller must use to_entities()')

    # create the necessary API entities represented by this single ScheduleSlots instance.
    def to_entities(self):
        ordered_slots = sorted(self.slots, key=attrgetter('movie', 'variant', 'start_time'))
        grouped_slots = groupby(ordered_slots, attrgetter('movie', 'variant', 'price', 'seating_type', 'popcorn_price'))
        ret = [(self.__to_entity_id(movie_tup), self.to_entity_with_slot_group(movie_tup, slot_group)) for movie_tup, slot_group in grouped_slots]

        return ret

    # used for website APIs/Endpoints.
    # create the necessary API entities represented by this single ScheduleSlots instance.
    def to_entities_with_movie_info(self, average_rating={}, posters_args={}, portrait_default_poster='', landscape_default_poster=''):
        ordered_slots = sorted(self.slots, key=attrgetter('movie', 'variant', 'start_time'))
        grouped_slots = groupby(ordered_slots, attrgetter('movie', 'variant', 'price', 'seating_type', 'popcorn_price'))
        ret = [(self.__to_entity_id(movie_tup), self.to_entity_with_slot_group_and_movie_info(movie_tup, slot_group,
                average_rating, posters_args, portrait_default_poster, landscape_default_poster)) for movie_tup, slot_group in grouped_slots]

        return ret

    @classmethod
    def make_slot_id(cls, cinema_id, show_date):
        return "%s::%s" % (cinema_id, show_date.strftime(SLOT_KEY_FORMAT))

    @classmethod
    def convert_schedule(cls, sched):
        def get_time(t):
            if isinstance(t, time):
                return t
            elif isinstance(t, datetime):
                return t.time()
            else:
                raise ValueError(t)

        # defaults for really old schema schedules.
        price = None
        seating_type = None
        variant = None
        popcorn_price = None

        slot_id = cls.make_slot_id(sched.cinema, sched.show_date)
        slot_key = ndb.Key(cls, slot_id, parent=sched.key.parent())
        slot = memcache.get("schedule:new_schema::%s" % slot_key.urlsafe())
        
        if not slot:
            slot = cls(id=slot_id, parent=sched.key.parent())
            slot.cinema = sched.cinema
            slot.show_date = sched.show_date

        map_time_to_code = json.loads(sched.map_time_to_code)

        if 'price' in sched._properties:
            price = Decimal(sched.price) / Decimal(100)

        if 'seating_type' in sched._properties:
            seating_type = sched.seating_type

        if 'variant' in  sched._properties:
            variant = sched.variant

        if 'popcorn_price' in  sched._properties:
            popcorn_price = Decimal(sched.popcorn_price) / Decimal(100)

        [slot.add_timeslot(start_time=get_time(t), feed_code=map_time_to_code.get(str(get_time(t)), None), movie=sched.movie,
                variant=variant, seating_type=seating_type, price=price, popcorn_price=popcorn_price) for t in sched.start_times if get_time(t) not in slot._start_times()]

        memcache.set("schedule:new_schema::%s" % slot_key.urlsafe(), slot)

        return slot

    @classmethod
    def find_schedule(cls, theater_key, cinema_name, show_datetime):
        if isinstance(show_datetime, str) or isinstance(show_datetime, unicode):
            show_datetime = datetime.strptime(show_datetime, DATETIME_FORMAT)

        if isinstance(show_datetime, datetime):
            show_date = show_datetime.date()
            show_time = show_datetime.time()
        else:
            show_date = show_datetime
            show_time = None

        slot_id = cls.make_slot_id(cinema_name, show_date)
        slot_key = ndb.Key(cls, slot_id, parent=theater_key)
        slot = slot_key.get()

        if show_time and slot and show_time not in slot._start_times():
            # .warn("show_time not in start_times for schedule slot: %s" % show_time)
            return None

        return slot


###############
#
# Models: Transaction Related Entities
#
###############

class Device(ndb.Model):
    device_class = ndb.StringProperty()


class ReservationInfo(ndb.Model):
    schedule = ndb.KeyProperty(Schedule)
    theater = ndb.KeyProperty(Theater)
    time = ndb.TimeProperty()
    cinema = ndb.StringProperty()
    seat_id = ndb.StringProperty()

    def to_entity(self):
        e = self.to_dict()
        e.pop('seat_id', None)

        e['theater'] = encoded_theater_id(e['theater'])
        e['schedule'] = e['schedule'].id()
        e['time'] = e['time'].strftime(TIME_FORMAT)
        e['seat'] = self.seat_id

        return e

class TicketData(ndb.Model):
    ref = ndb.StringProperty()
    code = ndb.StringProperty()
    date = ndb.DateProperty()
    extra = ndb.JsonProperty()
    barcode = ndb.BlobProperty()
    image = ndb.BlobProperty() # deprecated October 2014.

    # additional field/s for QRCode. added 10/23/2014.
    qrcode = ndb.BlobProperty()

    # additional field/s for multiple tickets. added 12/03/2014.
    barcode_list = ndb.BlobProperty(repeated=True)
    qrcode_list = ndb.BlobProperty(repeated=True)
    seat_transaction_codes = ndb.JsonProperty()
    transaction_code_list = ndb.StringProperty(repeated=True) # added 9/5/2016.

    def to_entity(self):
        e = self.to_dict()
        e.pop('barcode_list', None)
        e.pop('qrcode_list', None)
        e.pop('seat_transaction_codes', None)
        e.pop('transaction_code_list', None)

        if 'qrcode' not in e:
            e['qrcode'] = ''

        if e['date']:
            e['date'] = e['date'].strftime(DATETIME_FORMAT)

        if e['barcode']:
            e['barcode'] = e['barcode'].encode('base64')

        if e['qrcode']:
            e['qrcode'] = e['qrcode'].encode('base64')

        if e['image']:
            e['image'] = e['image'].encode('base64')

        if e['extra']:
            if 'runtime' not in e['extra']:
                e['extra']['runtime'] = DEFAULT_RUNTIME_MINS

            if 'ticket_ui_version' not in e['extra']:
                e['extra']['ticket_ui_version'] = '0'

            if 'is_multiple_tickets' not in e['extra']:
                e['extra']['is_multiple_tickets'] = False

            if 'movie_title' in e['extra']:
                e['extra']['movie_title'] = e['extra']['movie_title'].title()

            if 'movie_id' not in e['extra']:
                e['extra']['movie_id'] = ''

            if 'is_discounted' not in e['extra']:
                e['extra']['is_discounted'] = False

            if 'original_seat_price' not in e['extra']:
                if 'per_ticket_price' in e['extra'] and 'reservation_fee' in e['extra']:
                    original_seat_price = Decimal(e['extra']['per_ticket_price']) + Decimal(e['extra']['reservation_fee'])
                    e['extra']['original_seat_price'] = str(original_seat_price)
                else:
                    e['extra']['original_seat_price'] = ''

            if 'original_total_amount' not in e['extra']:
                e['extra']['original_total_amount'] = e['extra']['total_amount']

            if 'total_discount' not in e['extra']:
                e['extra']['total_discount'] = '0.00'

            if 'total_seat_discount' not in e['extra']:
                e['extra']['total_seat_discount'] = '0.00'

            if 'total_seat_price' not in e['extra']:
                if 'per_ticket_price' in e['extra'] and 'reservation_fee' in e['extra']:
                    total_seat_price = Decimal(e['extra']['per_ticket_price']) + Decimal(e['extra']['reservation_fee'])
                    e['extra']['total_seat_price'] = str(total_seat_price)
                else:
                    e['extra']['total_seat_price'] = ''

            if 'seating_type' not in e['extra']:
                if 'seats' in e['extra']:
                    if e['extra']['seats'] in ['Free Seating', 'Guaranteed Seats']:
                        e['extra']['seating_type'] = e['extra']['seats']
                    else:
                        e['extra']['seating_type'] = 'Reserved Seating'

        if self.seat_transaction_codes:
            if len(self.seat_transaction_codes.keys()) == 1:
                e['code'] = self.seat_transaction_codes[self.seat_transaction_codes.keys()[0]]
                e['extra']['is_multiple_tickets'] = False

        return e

    def to_entity2(self):
        e = self.to_dict()
        qrcode_list = []
        transaction_codes = []
        ticket_type = 'barcode'

        e.pop('image', None)
        e.pop('barcode', None)
        e.pop('qrcode', None)
        e.pop('barcode_list', None)
        e.pop('qrcode_list', None)
        e.pop('seat_transaction_codes', None)
        e.pop('transaction_code_list', None)

        if e['date']:
            e['date'] = e['date'].strftime(DATE_FORMAT)

        if e['extra']:
            if 'platform' not in e['extra']:
                e['extra']['platform'] = ''

            if 'is_discounted' not in e['extra']:
                e['extra']['is_discounted'] = False

            if 'movie_id' not in e['extra']:
                e['extra']['movie_id'] = ''

            if 'movie_rating' not in e['extra'] or not e['extra']['movie_rating']:
                e['extra']['movie_rating'] = ''

            if 'original_total_amount' not in e['extra']:
                e['extra']['original_total_amount'] = e['extra']['total_amount']

            if 'reservation_fee' not in e['extra']:
                e['extra']['reservation_fee'] = '0.00'

            if 'per_ticket_price' not in e['extra']:
                e['extra']['per_ticket_price'] = '0.00'

            if 'runtime' not in e['extra']:
                 e['extra']['runtime'] = DEFAULT_RUNTIME_MINS

            if 'seating_type' not in e['extra']:
                e['extra']['seating_type'] = ''

            if 'total_discount' not in e['extra']:
                e['extra']['total_discount'] = '0.00'

            if 'theater_name' not in e['extra'] or not e['extra']['theater_name']:
                theater_name = ''

                if 'theater_and_cinema_name' in e['extra'] and e['extra']['theater_and_cinema_name']:
                    theater_name = e['extra']['theater_and_cinema_name'][0].split('-')[0].strip()

                e['extra']['theater_name'] = theater_name

            if 'cinema_name' not in e['extra'] or not e['extra']['cinema_name']:
                cinema_name = ''

                if 'theater_and_cinema_name' in e['extra'] and e['extra']['theater_and_cinema_name']:
                    cinema_name = e['extra']['theater_and_cinema_name'][0].split('-')[-1].replace('Cinema:', '').strip()

                e['extra']['cinema_name'] = cinema_name

            if 'ticket_ui_version' in e['extra'] and e['extra']['ticket_ui_version'] == '2':
                ticket_type = 'qrcode'

            e['extra']['ticket_type'] = ticket_type

            e['extra'].pop('discount_type', None)
            e['extra'].pop('is_multiple_tickets', None)
            e['extra'].pop('movie_title', None)
            e['extra'].pop('original_seat_price', None)
            # e['extra'].pop('per_ticket_price', None)
            # e['extra'].pop('theater_and_cinema_name', None)
            # e['extra'].pop('ticket_ui_version', None)
            e['extra'].pop('total_discounted_seats', None)
            e['extra'].pop('total_seat_discount', None)
            e['extra'].pop('total_seat_price', None)

        if not self.transaction_code_list:
            if self.seat_transaction_codes:
                transaction_codes = self.seat_transaction_codes.values()
            elif self.code:
                transaction_codes = [self.code]
        else:
            transaction_codes = self.transaction_code_list

        if ticket_type == 'barcode':
            for barcode_blob in self.barcode_list:
                if barcode_blob:
                    qrcode_list.append(barcode_blob.encode('base64'))

            if not qrcode_list and self.barcode:
                qrcode_list = [self.barcode.encode('base64')]
        else:
            for qrcode_blob in self.qrcode_list:
                if qrcode_blob:
                    qrcode_list.append(qrcode_blob.encode('base64'))

            if not qrcode_list and self.qrcode:
                qrcode_list = [self.qrcode.encode('base64')]

        e['qrcodes'] = qrcode_list
        e['transaction_codes'] = transaction_codes

        return e

    def to_entity_minimize(self):
        e = self.to_dict()

        e.pop('ref', None)
        e.pop('code', None)
        e.pop('date', None)
        e.pop('image', None)
        e.pop('barcode', None)
        e.pop('qrcode', None)
        e.pop('barcode_list', None)
        e.pop('qrcode_list', None)
        e.pop('seat_transaction_codes', None)
        e.pop('transaction_code_list', None)

        if e['extra']:
            if 'movie_id' not in e['extra']:
                e['extra']['movie_id'] = ''

            if 'movie_rating' not in e['extra'] or not e['extra']['movie_rating']:
                e['extra']['movie_rating'] = ''

            if 'reservation_fee' not in e['extra']:
                e['extra']['reservation_fee'] = '0.00'

            if 'runtime' not in e['extra']:
                 e['extra']['runtime'] = DEFAULT_RUNTIME_MINS

            if 'seating_type' not in e['extra']:
                e['extra']['seating_type'] = ''

            if 'total_discount' not in e['extra']:
                e['extra']['total_discount'] = '0.00'

            if 'theater_name' not in e['extra'] or not e['extra']['theater_name']:
                theater_name = ''

                if 'theater_and_cinema_name' in e['extra'] and e['extra']['theater_and_cinema_name']:
                    theater_name = e['extra']['theater_and_cinema_name'][0].split('-')[0].strip()

                e['extra']['theater_name'] = theater_name

            if 'cinema_name' not in e['extra'] or not e['extra']['cinema_name']:
                cinema_name = ''

                if 'theater_and_cinema_name' in e['extra'] and e['extra']['theater_and_cinema_name']:
                    cinema_name = e['extra']['theater_and_cinema_name'][0].split('-')[-1].replace('Cinema:', '').strip()

                e['extra']['cinema_name'] = cinema_name

            e['extra'].pop('discount_type', None)
            e['extra'].pop('is_discounted', None)
            e['extra'].pop('is_multiple_tickets', None)
            e['extra'].pop('movie_title', None)
            e['extra'].pop('original_total_amount', None)
            e['extra'].pop('original_seat_price', None)
            e['extra'].pop('per_ticket_price', None)
            e['extra'].pop('theater_and_cinema_name', None)
            # e['extra'].pop('ticket_ui_version', None)
            e['extra'].pop('total_discounted_seats', None)
            e['extra'].pop('total_seat_discount', None)
            e['extra'].pop('total_seat_price', None)

        if not self.transaction_code_list:
            if self.seat_transaction_codes:
                e['extra']['transaction_codes'] = self.seat_transaction_codes.values()
            elif self.code:
                e['extra']['transaction_codes'] = [self.code]
            else:
                e['extra']['transaction_codes'] = []
        else:
            e['extra']['transaction_codes'] = self.transaction_code_list

        e = e['extra'] if 'extra' in e else {}

        return e

    # deprecated. remove to_minimum_entity in the future.
    def to_minimum_entity(self):
        e = self.to_dict()
        e.pop('image', None)
        e.pop('barcode', None)
        e.pop('qrcode', None)

        if 'qrcode' not in e:
            e['qrcode'] = ''

        if e['date']:
            e['date'] = e['date'].strftime(DATETIME_FORMAT)

        if e['barcode_list']:
            for index, barcode in enumerate(e['barcode_list']):
                if barcode:
                    e['barcode_list'][index] = barcode.encode('base64')

        if e['qrcode_list']:
            for index, qrcode in enumerate(e['qrcode_list']):
                if qrcode:
                    e['qrcode_list'][index] = qrcode.encode('base64')

        if e['extra']:
            if 'is_multiple_tickets' not in e['extra']:
                e['extra']['is_multiple_tickets'] = False

            if 'movie_title' in e['extra']:
                e['extra']['movie_title'] = e['extra']['movie_title'].title()

        if e['seat_transaction_codes']:
            e['seat_transaction_codes'] = e['seat_transaction_codes'].values()

        if self.seat_transaction_codes:
            if len(self.seat_transaction_codes.keys()) == 1:
                e['extra']['is_multiple_tickets'] = False

        return e


class TicketImageBlob(ndb.Model):
    image = ndb.BlobProperty(required=True)

    # additional field/s for multiple tickets. added 12/05/2014.
    seat = ndb.StringProperty()
    transaction_code = ndb.StringProperty()

    def get_reservation_info_entity(self, tx):
        for reservation_info in tx.reservations:
            if reservation_info.seat_id == self.seat:
                return reservation_info.to_entity()

        if tx.reservations:
            return tx.reservations[0].to_entity()

    def total_seat_price(self, price, reservation_fee):
        return Decimal(price) + Decimal(reservation_fee)

    def to_entity(self, tx):
        reservation_ent = self.get_reservation_info_entity(tx)
        ticket_ent = tx.ticket.to_entity() if tx.ticket else {}
        payment_info = tx.payment_info
        date_created = tx.date_created.strftime(DATETIME_FORMAT) if tx.date_created else None
        total_seat_price = self.total_seat_price(ticket_ent['extra']['per_ticket_price'], ticket_ent['extra']['reservation_fee'])

        ticket_ent['extra']['is_multiple_tickets'] = False
        ticket_ent['extra']['seats'] = self.seat
        ticket_ent['extra']['ticket_count'] = 1
        ticket_ent['extra']['theater_and_cinema_name'] = [ticket_ent['extra']['theater_and_cinema_name'][0]]
        ticket_ent['code'] = self.transaction_code
        ticket_ent['image'] = self.image.encode('base64')
        ticket_ent['barcode'] = ''
        ticket_ent['qrcode'] = ''

        if 'is_discounted' not in ticket_ent['extra']:
            ticket_ent['extra']['is_discounted'] = False

        if 'original_seat_price' not in ticket_ent['extra']:
            ticket_ent['extra']['original_seat_price'] = str(total_seat_price)

        if 'original_total_amount' not in ticket_ent['extra']:
            ticket_ent['extra']['original_total_amount'] = ticket_ent['extra']['total_amount']

        if 'total_discount' not in ticket_ent['extra']:
            ticket_ent['extra']['total_discount'] = '0.00'

        if 'total_seat_discount' not in ticket_ent['extra']:
            ticket_ent['extra']['total_seat_discount'] = '0.00'

        if 'total_seat_price' not in ticket_ent['extra']:
            ticket_ent['extra']['total_seat_price'] = str(total_seat_price)

        return {'payment_info': payment_info, 'reservations': [reservation_ent], 'ticket': ticket_ent, 'date_created': date_created}


class ReservationTransaction(ndb.Model):
    allowed_payment_types = make_enum('payment_types', CREDIT_CARD='credit-card', MPASS='mpass', EPLUS='eplus',
            PROMO_CODE='globe-promo', CLIENT_INITIATED='client-initiated', IPAY88_PAYMENT='ipay88-payment-initiated',
            PAYNAMICS_PAYMENT='paynamics-payment-initiated', MIGS_PAYMENT='migs-payment-initiated', GCASH='gcash', GCASH_APP='g-cash-app',
            RESERVE='cash', GLOBE_OPERATOR_BILLING='globe-operator-billing', BPI_CARDHOLDER_PROMO='bpi-cardholder-promo',
            CITIBANK_CARDHOLDER_PROMO='citibank-cardholder-promo', PNB_CARDHOLDER_PROMO='pnb-cardholder-promo',
            BDO_CARDHOLDER_PROMO='bdo-cardholder-promo', GCASH_CARDHOLDER_PROMO='g-cash-cardholder-promo', 
            METROBANK_CARDHOLDER_PROMO='metrobank-cardholder-promo', CHINABANK_CARDHOLDER_PROMO='chinabank-cardholder-promo',
            UNIONBANK_CARDHOLDER_PROMO='unionbank-cardholder-promo',
            ROBINSONSBANK_CARDHOLDER_PROMO='robinsonsbank-cardholder-promo',
            GREWARDS='grewards')

    payment_type = ndb.StringProperty(required=True)
    payment_info = ndb.JsonProperty(required=True)
    reservations = ndb.StructuredProperty(ReservationInfo, repeated=True)
    ticket = ndb.StructuredProperty(TicketData)
    state = ndb.IntegerProperty(required=True)
    previous_state = ndb.IntegerProperty()
    workspace = ndb.PickleProperty()
    payment_reference = ndb.StringProperty()
    reservation_reference = ndb.StringProperty()
    was_reaped = ndb.BooleanProperty()
    date_created = ndb.DateTimeProperty(auto_now_add=True)
    ver = ndb.IntegerProperty(default=1)
    status = ndb.StringProperty(default='unclaimed')

    # additional field for tagging device platform (iOS/Andriod/Website). added 04/21/2015.
    platform = ndb.StringProperty()

    # additional field for discount codes. added 10/13/2015.
    discount_info = ndb.JsonProperty()
    is_discounted = ndb.BooleanProperty(default=False)

    # additional field for user information. added 9/23/2016.
    user_info = ndb.JsonProperty()

    # additional field for requery tagging. added 12/6/2016.
    was_requery = ndb.BooleanProperty(default=False)

    # additional filed for refund/cancel transaction. added 2/21/2017.
    date_cancelled = ndb.DateTimeProperty()

    # additional field for payment and error information. added 8/15/2017.
    error_info = ndb.JsonProperty()

    # additional field for refund/cancel transaction. added 10/03/2017.
    refund_reason = ndb.StringProperty()
    refunded_by   = ndb.StringProperty()

    @classmethod
    def generate_tx_id(cls):
        return str(uuid_gen())

    @classmethod
    def create_transaction(cls, device_id, tx_info):
        tx_id = cls.generate_tx_id()
        tx = cls(key=ndb.Key(Device, device_id, cls, tx_id))
        tx.copy_api_entity(tx_info)
        tx.workspace = {}
        tx.error_info = {}

        if 'is_theaterorg_ticket_template' in tx_info:
            tx.workspace['is_theaterorg_ticket_template'] = tx_info['is_theaterorg_ticket_template']

        return tx

    @classmethod
    def create_transaction_raw(cls, device_id, tx_info):
        tx_id = cls.generate_tx_id()
        tx = cls(key=ndb.Key(Device, device_id, cls, tx_id))
        tx.workspace = {'is_raw': True, 'reservations_raw': tx_info['reservations']}
        tx.error_info = {}
        payment_info = dict(tx_info['payment'])

        if 'type' not in payment_info:
            raise BadValueException('type', 'No payment type specified')

        tx.payment_type = payment_info.pop('type', None)
        tx.payment_info = payment_info

        return tx

    def ticket_count(self):
        return len(self.reservations)

    def reservation_count(self):
        return len(self.reservations)

    def total_amount(self, reservation_fee, use_workspace=False):
        if not use_workspace:
            schedules = ndb.get_multi([r.schedule for r in self.reservations])
            schedule_times = zip(schedules, [r.time for r in self.reservations])

            extra = 0
            for s, tt in schedule_times:
                popcorn_price = s.get_timeslot(tt).popcorn_price
                if popcorn_price:
                    extra += popcorn_price

            return sum([(s.get_timeslot(tt).price + Decimal(reservation_fee)) for s, tt in schedule_times]) + extra

        else:
            extra = 0
            if 'schedules.popcorn_price' in self.workspace:
                for pp in self.workspace['schedules.popcorn_price']:
                    if pp:
                        extra += pp

            return sum([p+Decimal(reservation_fee) for p in self.workspace['schedules.price']]) + extra

    def total_seat_price(self, reservation_fee, use_workspace=False):
        if not use_workspace:
            schedule = self.reservations[0].schedule.get()
            schedule_time = self.reservations[0].time

            extra = 0
            if schedule.get_timeslot(schedule_time).popcorn_price:
                extra += schedule.get_timeslot(schedule_time).popcorn_price

            return schedule.get_timeslot(schedule_time).price + Decimal(reservation_fee) + extra
        else:
            extra = 0
            if 'schedules.popcorn_price' in self.workspace:
                if self.workspace['schedules.popcorn_price'][0]:
                    extra += self.workspace['schedules.popcorn_price'][0]

            return self.workspace['schedules.price'][0] + Decimal(reservation_fee) + extra

    def get_queue(self):
        queue_id = self.workspace.get('queue_id', None)

        return "txqueue%d" % queue_id if queue_id else 'transactions'

    def get_queue_requery(self):
        queue_id = self.workspace.get('queue_id', None)

        return "txqueuerequery%d" % queue_id if queue_id else 'txqueuerequery'

    def check_transaction_information(self, tx_info):
        try:
            payment = tx_info['payment']
            payment_type = payment['type']

            if payment_type == ReservationTransaction.allowed_payment_types.CREDIT_CARD:
                payment['cardholder']
                payment['card_number']
                payment['expiration_month']
                payment['expiration_year']
                payment['cvv']
                payment['address']
                payment['email']
            elif payment_type == ReservationTransaction.allowed_payment_types.MPASS:
                payment['username']
                payment['password']
                payment['email']
            elif payment_type == ReservationTransaction.allowed_payment_types.PROMO_CODE:
                payment['promo_code']
            elif payment_type == ReservationTransaction.allowed_payment_types.RESERVE:
                pass
            elif payment_type == ReservationTransaction.allowed_payment_types.CLIENT_INITIATED:
                payment['reference-parameter']
                payment['form-method']
                payment['form-target']
                payment['form-parameters']
            elif payment_type == ReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT:
                payment['email']
                payment['fname']
                payment['lname']
                payment['mname']
                payment['address1']
                payment['address2']
                payment['city']
                payment['state']
                payment['country']
                payment['zip']
                payment['phone']
                payment['mobile']
            elif payment_type == ReservationTransaction.allowed_payment_types.MIGS_PAYMENT:
                payment['email']
                payment['fname']
                payment['lname']
                payment['mname']
                payment['mobile']
            elif payment_type == ReservationTransaction.allowed_payment_types.IPAY88_PAYMENT:
                payment['first_name']
                payment['last_name']
                payment['email']
                payment['contact_number']
            elif payment_type == ReservationTransaction.allowed_payment_types.GCASH:
                payment['full_name']
                payment['email']
                payment['mobile']
                payment['gcash_account']
                payment['otp']
            elif payment_type == ReservationTransaction.allowed_payment_types.GREWARDS:
                payment['full_name']
                payment['email']
                payment['mobile']
                payment['grewards_account']
                payment['otp']
            elif payment_type in [ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO,
                    ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO,
                    ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO,
                    ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO,
                    ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO,
                    ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO,
                    ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO,
                    ReservationTransaction.allowed_payment_types.UNIONBANK_CARDHOLDER_PROMO,
                    ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO]:
                payment['discount']
                payment['discount']['claim_code']
                payment['discount']['user_id']
                payment['discount']['mobile_number']
                payment['discount']['claim_code_type']
                payment['discount']['payment_gateway']

                if payment['discount']['payment_gateway'] == 'pesopay':
                    payment['reference-parameter']
                    payment['form-method']
                    payment['form-target']
                    payment['form-parameters']
                elif payment['discount']['payment_gateway'] == 'paynamics':
                    payment['email']
                    payment['fname']
                    payment['lname']
                    payment['mname']
                    payment['address1']
                    payment['address2']
                    payment['city']
                    payment['state']
                    payment['country']
                    payment['zip']
                    payment['phone']
                    payment['mobile']
                elif payment['discount']['payment_gateway'] == 'migs':
                    payment['email']
                    payment['fname']
                    payment['lname']
                    payment['mname']
                    payment['mobile']
                elif payment['discount']['payment_gateway'] == 'ipay88':
                    payment['first_name']
                    payment['last_name']
                    payment['email']
                    payment['contact_number']
                else:
                    raise BadValueException('type', "Unknown payment type: %s" % payment_type)
            else:
                raise BadValueException('type', "Unknown payment type: %s" % payment_type)
        except KeyError, e:
            raise BadValueException(e[0], "Missing %s params for payment" % e[0])

        try:
            reservations = tx_info['reservations']

            for reservation in reservations:
                reservation['theater']
                reservation['cinema']
                reservation['movie']
                reservation['show_datetime']
                reservation['seat']
        except KeyError, e:
            raise BadValueException(e, "Missing %s params for reservation" % e)

    def copy_api_entity(self, tx_info):
        self.check_transaction_information(tx_info)

        if 'reservations' in tx_info:
            self.reservations = [self.__build_reservation(r) for r in tx_info['reservations']]

        payment_info = dict(tx_info['payment'])

        if 'type' not in payment_info:
            raise BadValueException('type', 'No payment type specified')

        self.payment_type = payment_info.pop('type', None)
        self.discount_info = payment_info.pop('discount', None)
        self.payment_info = payment_info
        self.platform = tx_info.pop('platform', None)
        self.user_info = tx_info.pop('user', None)

    # ensure always executed outside of a transaction.
    @ndb.non_transactional
    def __build_reservation(self, r):
        org_uuid, t_id = decoded_theater_id(r['theater'])

        if org_uuid is None or t_id is None:
            raise BadValueException('reservations', "Theater not found: %s" % r)

        t = ndb.Key(TheaterOrganization, str(org_uuid), Theater, t_id)
        m = ndb.Key(Movie, r['movie'])
        c = r['cinema']
        s = Schedule.find_schedule(t, r['cinema'], r['show_datetime'])
        show_datetime = datetime.strptime(r['show_datetime'], DATETIME_FORMAT)
        time = show_datetime.time()

        if not s:
            raise BadValueException('reservations', "Schedule not found: %s" % r)

        if show_datetime < datetime.now():
            raise BadValueException('reservations', "Attempting to reserve for an expired schedule: %s" % r)

        reserv = ReservationInfo(theater=t, cinema=c, schedule=s.key, time=time)
        reserv.seat_id = r['seat']

        return reserv

    def get_ticket_image_blob(self):
        ticket_image_blob = TicketImageBlob.query(ancestor=self.key).fetch()

        return ticket_image_blob

    def get_sorted_ticket_image_blob(self, sort_cols=['transaction_code']):
        ticket_image_blob = TicketImageBlob.query(ancestor=self.key).fetch()
        ticket_image_blob = sorted(ticket_image_blob, key=attrgetter(*sort_cols))

        return ticket_image_blob

    def to_entity(self):
        ticket_ent = self.ticket.to_entity() if self.ticket else {}
        date_created = self.date_created.strftime(DATETIME_FORMAT) if self.date_created else None

        if 'image' in ticket_ent:
            if 'is_multiple_tickets' in ticket_ent['extra']:
                if ticket_ent['extra']['is_multiple_tickets'] == True:
                    if not ticket_ent['image']:
                        ticket_image_blob = self.get_sorted_ticket_image_blob()

                        if ticket_image_blob:
                            if ticket_image_blob[0].image:
                                ticket_ent['image'] = ticket_image_blob[0].image.encode('base64')
                else:
                    if not ticket_ent['image']:
                        ticket_image_blob = self.get_ticket_image_blob()

                        if ticket_image_blob:
                            if ticket_image_blob[0].image:
                                ticket_ent['image'] = ticket_image_blob[0].image.encode('base64')
            else:
                ticket_ent['extra']['is_multiple_tickets'] = False

                if not ticket_ent['image']:
                    ticket_image_blob = self.get_ticket_image_blob()

                    if ticket_image_blob:
                        if ticket_image_blob[0].image:
                            ticket_ent['image'] = ticket_image_blob[0].image.encode('base64')

        return {'payment_info': self.payment_info, 'reservations': [r.to_entity() for r in self.reservations],
                'ticket': ticket_ent, 'date_created': date_created}

    def to_entity2(self):
        date_created = self.date_created.strftime(DATETIME_FORMAT) if self.date_created else ''
        reservation_entities = [r.to_entity() for r in self.reservations]
        ticket_entity = self.ticket.to_entity2() if self.ticket else {}
        discount_info = self.discount_info if self.discount_info else {}
        user_info = self.user_info if self.user_info else {}

        # added for reports on transaction errors
        # populate ticket -> extra for failed transactions
        state = {0: 'TX_START', 1: 'TX_PREPARE', 2: 'TX_STARTED', 3: 'TX_RESERVATION_HOLD', 4: 'TX_RESERVATION_OK',
            5: 'TX_PAYMENT_HOLD', 6: 'TX_GENERATE_TICKET', 7: 'TX_CLIENT_PAYMENT_HOLD', 8: 'TX_FINALIZE',
            9: 'TX_EXTERNAL_PAYMENT_HOLD', 10: 'TX_FINALIZE_HOLD', 11: 'TX_RESERVATION_ERROR',
            12: 'TX_PAYMENT_ERROR', 13: 'TX_PREPARE_ERROR', 100: 'TX_DONE', 101: 'TX_CANCELLED',
            102: 'TX_REFUNDED', -1: 'TX_STATE_ERROR'}

        previous_state = None
        if self.previous_state:
            previous_state = state[self.previous_state]

        if not ticket_entity:
            # specify defautl values
            ticket = {}
            ticket['per_ticket_price']    = '0.00'
            ticket['reservation_fee']     = '0.00'
            ticket['seating_type']        = '0.00'
            ticket['total_amount']        = '0.00'
            ticket['total_discount']      = '0.00'
            ticket['total_popcorn_price'] = '0.00'

            if self.workspace:
                if 'schedules.price' in self.workspace:
                    prices = self.workspace['schedules.price']
                    ticket['per_ticket_price'] = str(prices[0])
                if 'convenience_fee' in self.workspace:
                    ticket['reservation_fee'] = self.workspace['convenience_fee']
                if 'RSVP:fee' in self.workspace:
                    ticket['reservation_fee'] = self.workspace['RSVP:fee']
                if 'seating_type' in self.workspace:
                    ticket['seating_type'] = self.workspace['seating_type']
                if 'RSVP:totalamount' in self.workspace:
                    ticket['total_amount'] = self.workspace['RSVP:totalamount']
                if 'RSVP:totaldiscount' in self.workspace:
                    ticket['total_discount'] = self.workspace['RSVP:totaldiscount']
                if 'schedules.popcorn_price' in self.workspace:
                    popcorn_prices = self.workspace['schedules.popcorn_price']
                    if popcorn_prices[0]:
                        ticket['total_popcorn_price'] = str(popcorn_prices[0] * self.ticket_count())
            ticket_entity['extra'] = ticket

        # added for error description on failed transaction
        error_info = {}
        error_info['error_msg'] = ""
        error_info['response_code'] = ""
        error_info['response_message'] = ""
        if self.error_info:
            error_info = self.error_info
            error_info['error_msg'] = self.error_info['error_msg'] if 'error_msg' in self.error_info else ''
            error_info['response_code'] = self.error_info['response_code'] if 'response_code' in self.error_info else ''
            error_info['response_message'] = self.error_info['response_message'] if 'response_message' in self.error_info else ''
        else:
            if self.workspace:
                if 'msg' in self.workspace:
                    error_info['error_msg'] = self.workspace['msg']
                if 'err_message' in self.workspace:
                    error_info['error_msg'] = self.workspace['err_message']
                if 'response_code' in self.workspace:
                    error_info['response_code'] = self.workspace['response_code']
                if 'response_message' in self.workspace:
                    error_info['response_message'] = self.workspace['response_message']
                if 'payment_status' in self.workspace:
                    error_info['response_code'] = self.workspace['payment_status']
                if 'payment_error_desc' in self.workspace:
                    error_info['response_message'] = self.workspace['payment_error_desc']

        return {'previous_state': previous_state, 'error_info': error_info, 'date_created': date_created, 'discount_info': discount_info, 'payment_info': self.payment_info,
                'reservations': reservation_entities, 'ticket': ticket_entity, 'user_info': user_info}

    def to_entity_minimize(self):
        ticket_entity = self.ticket.to_entity_minimize() if self.ticket else {}

        return ticket_entity

    # deprecated. remove to_minimum_entity() in the future.
    def to_minimum_entity(self):
        ticket_ent = self.ticket.to_minimum_entity() if self.ticket else {}
        date_created = self.date_created.strftime(DATETIME_FORMAT) if self.date_created else None

        return {'payment_info': self.payment_info, 'reservations': [r.to_entity() for r in self.reservations],
                'ticket': ticket_ent, 'date_created': date_created}

    def to_entity_reports(self):
        org_uuid = ''
        show_datetime = ''
        ticket_ent = self.ticket.to_entity() if self.ticket else {}
        reservation_ent = self.reservations[0].to_entity() if self.reservations else {}
        discount_info = self.discount_info if self.discount_info else {}

        if 'theater' in reservation_ent:
            org_uuid, theater_id = decoded_theater_id(reservation_ent['theater'])

        if 'show_datetime' in ticket_ent['extra']:
            show_datetime = datetime.strptime(ticket_ent['extra']['show_datetime'], '%m/%d/%Y %I:%M %p')

        tx_ent = {}
        tx_ent['transaction_id'] = '%s~%s' % (self.key.parent().id(), self.key.id())
        tx_ent['transaction_date'] = self.date_created.strftime('%Y-%m-%d %H:%M:%S') if self.date_created else ''
        tx_ent['ref_no'] = self.reservation_reference
        tx_ent['payment_reference'] = self.payment_reference
        tx_ent['ticket_code'] = ticket_ent['code']
        tx_ent['seats'] = ','.join([r.seat_id for r in self.reservations if r.seat_id])
        tx_ent['seats_count'] = str(self.reservation_count())
        tx_ent['seating_type'] = ticket_ent['extra']['seating_type'] if 'seating_type' in ticket_ent['extra'] else ''
        tx_ent['customer_name'] = self._get_customer_name()
        tx_ent['customer_email'] = self.payment_info['email'] if 'email' in self.payment_info else ''
        tx_ent['customer_mobile'] = self.payment_info['mobile'] if 'mobile' in self.payment_info else ''
        tx_ent['customer_address'] = self.payment_info['address1'] if 'address1' in self.payment_info else ''
        tx_ent['claim_code'] = self._get_claim_code()
        tx_ent['cinema_name'] = reservation_ent['cinema'] if 'cinema' in reservation_ent else ''
        tx_ent['payment_method'] = self._get_payment_type()
        tx_ent['merchant_id'] = str(org_uuid)
        tx_ent['merchant_name'] = ''
        tx_ent['theater_id'] = reservation_ent['theater'] if 'theater' in reservation_ent else ''
        tx_ent['theater_name'] = ticket_ent['extra']['theater_and_cinema_name'][0].split('-')[0].strip() if 'theater_and_cinema_name' in ticket_ent['extra'] else ''
        tx_ent['movie_id'] = ticket_ent['extra']['movie_id'] if 'movie_id' in ticket_ent['extra'] else ''
        tx_ent['movie_name'] = ticket_ent['extra']['movie_canonical_title'] if 'movie_canonical_title' in ticket_ent['extra'] else ''
        tx_ent['amount'] = ticket_ent['extra']['original_total_amount'] if 'original_total_amount' in ticket_ent['extra'] else ticket_ent['extra']['total_amount']
        tx_ent['unit_convenience_fee'] = ticket_ent['extra']['reservation_fee'] if 'reservation_fee' in ticket_ent['extra'] else '0.00'
        tx_ent['discount_type'] = discount_info['discount_type'] if 'discount_type' in discount_info else ''
        tx_ent['discount_value'] = ''
        tx_ent['discount_amount'] = discount_info['total_discount'] if 'total_discount' in discount_info else '0.00'
        tx_ent['show_date_time'] = show_datetime.strftime('%Y-%m-%d %H:%M:%S') if show_datetime else ''
        tx_ent['platform'] = self.platform if self.platform else ''

        if self.ticket.seat_transaction_codes:
            if len(self.ticket.seat_transaction_codes.values()) > 1:
                tx_ent['ticket_code'] = ','.join(self.ticket.seat_transaction_codes.values())

        return tx_ent

    def _get_customer_name(self):
        customer_name = ''

        if self.payment_type == ReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT:
            customer_name = '%s %s' % (self.payment_info['fname'], self.payment_info['lname'])
        elif self.payment_type == ReservationTransaction.allowed_payment_types.IPAY88_PAYMENT:
            customer_name = '%s %s' % (self.payment_info['first_name'], self.payment_info['last_name'])
        elif self.payment_type == ReservationTransaction.allowed_payment_types.GCASH:
            customer_name = self.payment_info['full_name']
        elif self.payment_type == ReservationTransaction.allowed_payment_types.GREWARDS:
            customer_name = self.payment_info['full_name']
        elif self.payment_type in [ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.UNIONBANK_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO]:
            if self.discount_info['payment_gateway'] == 'paynamics':
                customer_name = '%s %s' % (self.payment_info['fname'], self.payment_info['lname'])
            elif self.discount_info['payment_gateway'] == 'ipay88':
                customer_name = '%s %s' % (self.payment_info['first_name'], self.payment_info['last_name'])

        return customer_name.strip()

    def _get_claim_code(self):
        claim_code = ''

        if self.payment_type == ReservationTransaction.allowed_payment_types.PROMO_CODE:
            claim_code = self.payment_info['promo_code']
        elif self.payment_type in [ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO,
                    ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO,
                    ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO,
                    ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO,
                    ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO,
                    ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO,
                    ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO,
                    ReservationTransaction.allowed_payment_types.UNIONBANK_CARDHOLDER_PROMO,
                    ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO]:
            claim_code = self.discount_info['claim_code']

        return claim_code

    def _get_payment_type(self):
        payment_type = self.payment_type

        if self.payment_type in [ReservationTransaction.allowed_payment_types.BPI_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.CITIBANK_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.PNB_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.BDO_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.GCASH_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.METROBANK_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.CHINABANK_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.UNIONBANK_CARDHOLDER_PROMO,
                ReservationTransaction.allowed_payment_types.ROBINSONSBANK_CARDHOLDER_PROMO]:
            payment_type = '%s-%s' % (self.payment_type, self.discount_info['payment_gateway'])

        return payment_type

    def elide_payment_details(self):
        if self.payment_type == ReservationTransaction.allowed_payment_types.CREDIT_CARD:
            self.payment_info = self.__elide_cc_details(self.payment_info)
        elif self.payment_type == ReservationTransaction.allowed_payment_types.MPASS:
            self.payment_info = self.__elide_mpass_details(self.payment_info)
        else:
            pass

        self.put()

    def __elide_ipay88_details(self, info):
        new_info = dict(info)
        new_info['first_name'] = None
        new_info['last_name'] = None
        new_info['contact_number'] = None
        new_info['email'] = None

        return new_info

    def __elide_cc_details(self, info):
        new_info = dict(info)
        cc = new_info['card_number']
        cc = ('X' * len(cc[:-4])) + cc[-4:]
        new_info['card_number'] = cc
        new_info['cvv'] = None

        return new_info

    def __elide_mpass_details(self, info):
        new_info = dict(info)
        new_info['password'] = None

        return new_info

    bind_cancellation_callback, trigger_cancellation = _handle_callbacks('__cancellation_callbacks')
    bind_update_callback, trigger_update = _handle_callbacks('__update_callbacks')

class FreeTicketData(ndb.Model):
    transaction = ndb.KeyProperty(ReservationTransaction)
    transaction_date = ndb.DateTimeProperty()
    reservation_reference = ndb.StringProperty()
    payment_reference = ndb.StringProperty()
    movie = ndb.StringProperty()
    email = ndb.StringProperty()
    mobile = ndb.StringProperty()
    show_datetime = ndb.DateTimeProperty()
    # if from special (bulk incentive) or rewards (rush integration) etc.
    discount_type = ndb.StringProperty()
    platform = ndb.StringProperty()
    theater = ndb.KeyProperty(Theater)
    cinema = ndb.StringProperty()
    seat_id = ndb.StringProperty()
    seat_name = ndb.StringProperty()
    is_valid = ndb.BooleanProperty(default=True)

###############
#
# Models: Client API Authentication
#
###############

class Client(ndb.Model):
    name = ndb.StringProperty(required=True)
    pubkey = ndb.TextProperty()


###############
#
# Models: Callback Related Entities
#
###############

class Event(ndb.Model):
    event_channel = ndb.StringProperty(required=True)
    org_id = ndb.StringProperty(required=True)
    timestamp = ndb.IntegerProperty()
    payload = ndb.JsonProperty(indexed=False)
    read = ndb.BooleanProperty(default=False)

    @classmethod
    def get_events(cls, channel_name):
        return cls.query(cls.event_channel==channel_name, cls.read==False)


class Listener(ndb.Model):
    event_channel = ndb.StringProperty(required=True)
    listener_id = ndb.StringProperty(required=True)
    callback = ndb.PickleProperty()
    version = ndb.StringProperty()

    @classmethod
    def get_listeners(cls, channel_name):
        return cls.query(cls.event_channel==channel_name)

    @classmethod
    def get_listeners_by_version(cls, channel_name, version):
        return cls.query(cls.event_channel==channel_name, cls.version==version)


###############
#
# Models: CSRC Payment Related Entities
#
###############

class CSRCPayment(ndb.Model):
    proc_state = make_enum('state', TX_STARTED=1, TX_PAYMENT_HOLD=2, TX_DONE=100, TX_PAYMENT_ERROR = -1)

    created = ndb.DateTimeProperty(auto_now_add=True)
    bank_tx_reference = ndb.StringProperty()
    process_reference = ndb.StringProperty()
    deposit_info = ndb.JsonProperty()
    form_parameters = ndb.PickleProperty()
    callback_data = ndb.PickleProperty()
    state = ndb.IntegerProperty(required=True)
    completed = ndb.DateTimeProperty()
    workspace = ndb.PickleProperty()
    for_notification_bulk = ndb.BooleanProperty(default=False)
    for_notification_wave = ndb.BooleanProperty(default=False)

    @classmethod
    def generate_proc_id(cls):
        return str(uuid_gen())

    @classmethod
    def start_process(cls, device_id, payment_info):
        proc_id = cls.generate_proc_id()
        proc = cls(key=ndb.Key(Device, device_id, cls, proc_id))
        proc.copy_api_entity(payment_info)
        proc.state = cls.proc_state.TX_STARTED
        proc.workspace = {}
        proc.put()

        return proc

    def copy_api_entity(self, proc_info):
        if "payment" not in proc_info:
            raise BadValueException('payment', 'No payment data specified')

        depositors_info = dict(proc_info['payment'])
        self.deposit_info = depositors_info

    def to_entity(self):
        return self.to_dict()

    def query_state(self):
        return self.state

    def get_form_data(self):
        self.generate_form()
        e = self.to_dict()
        return e["form_parameters"]

    def generate_form(self):
        if self.form_parameters is None:
            amount = self.deposit_info['amount']
            tx_code = self.generate_tx_code()
            self.process_reference = tx_code
            signature = self.generate_signature(tx_code)

            form_parameters = {}
            form_parameters['MerchantCode'] = bt_settings.GMOVIES_MERCHANT_CODE     #Mandatory: Will be provided by Ipay88
            form_parameters['PaymentId']    = '1'           #Optional: Credit Card Payment only
            form_parameters['RefNo']        = tx_code       #Mandatory: Reference Number
            form_parameters['Amount']       = amount        #Mandatory: Total Amount of purchased ticket. 
                                                            #Payment amount with two decimals and thousand symbols.
                                                            #Example: 1,278.99
            form_parameters['Currency']     = 'PHP'         #Mandatory: Currency(PHP)
            form_parameters['ProdDesc']     = 'GMovies CSR Campaign'#Mandatory: Product Description. 
            form_parameters['UserName']     = self.deposit_info['first_name'] + ' ' + self.deposit_info['last_name'] #Mandatory: Customer Name   
            form_parameters['UserEmail']    = self.deposit_info['email']            #Mandatory: Customer Email Address
            form_parameters['UserContact']  = self.deposit_info['contact_number']   #Mandatory: Customer Contact Number
            form_parameters['Remark']       = ''     #Optional: Merchant Remark
            form_parameters['Lang']         = 'UTF-8'       #Optional: Language Encoding  
            form_parameters['Signature']    = signature     #Mandatory:
            #Change this parameters in production
            #form_parameters['ResponseURL']  = GMOVIES_RECIEVEOK_RESPONSE_URL     #Mandatory:
            form_parameters['BackendURL']   = bt_settings.GMOVIES_CSRC_PAYMENT_CALLBACK        #Mandatory:
            self.form_parameters = form_parameters
        self.state = self.proc_state.TX_PAYMENT_HOLD
        self.put()

    def generate_signature(self, transaction_code):
        merchant_key = bt_settings.GMOVIES_MERCHANT_KEY
        merchant_code = bt_settings.GMOVIES_MERCHANT_CODE
        currency_code = "PHP"
        reference_number = transaction_code
        #remove commas and periods from string
        amount = re.sub('[,.]','',str(self.deposit_info['amount']))
        #make a list out of string
        seed = [ merchant_key, merchant_code, reference_number, amount, currency_code ]
        seed_str = ''.join(seed)
        #generate hash
        log.info("String to Hash: {}".format(seed_str))
        seed_hash = SHA.new(seed_str).digest()
        return base64.b64encode(seed_hash)

    def generate_tx_code(self):
        return binascii.hexlify(os.urandom(10))

    @classmethod
    def stringify_state(cls,s):
        if s not in cls.proc_state.reverse_mapping:
            return 'TX_UNKNOWN'
        return cls.proc_state.reverse_mapping[s]

    @classmethod
    def get_last_10(cls):
        donors = cls.query(cls.for_notification_wave == False).order(-cls.completed).fetch()
        if donors is not None and len(donors) >= 10:
            donors_list = []
            log.info("Found {} donors".format(donors.count()))
            for d in donors:
                donor = {   'campaign' : camelcase(str(d.deposit_info["campaign"])),
                            'username' : d.deposit_info['first_name'] + ' ' + d.deposit_info['last_name'],
                            'email_address' : d.deposit_info["email"], 
                            'contact_number' : d.deposit_info["contact_number"], 
                            'amount' : d.deposit_info["amount"], 
                            'reference_code' : d.process_reference, 
                            'trans_id' : d.bank_tx_reference,
                            'trans_date': d.completed.date()}

                donors_list.append(donor)
                d.for_notification_wave = True

            ndb.put_multi(donors)
            return donors_list
        else:
            return None


    @classmethod
    def get_bulk_monthly(cls, thirty_days_ago):
        donors = cls.query(cls.for_notification_bulk == False, cls.completed >= thirty_days_ago).order(-cls.completed).fetch()
        if donors is not None:
            donors_list = []
            log.info("Found {} donors".format(len(donors)))
            for d in donors:
                donor = {   'campaign' : camelcase(str(d.deposit_info["campaign"])),
                            'username' : d.deposit_info['first_name'] + ' ' + d.deposit_info['last_name'],
                            'email_address' : d.deposit_info["email"], 
                            'contact_number' : d.deposit_info["contact_number"], 
                            'amount' : d.deposit_info["amount"], 
                            'reference_code' : d.process_reference, 
                            'trans_id' : d.bank_tx_reference,
                            'trans_date': d.completed.date()}

                donors_list.append(donor)
                d.for_notification_bulk = True

            ndb.put_multi(donors)
            return donors_list
        else:
            return None


###############
#
# Mdoesl: Analytics Service Related Entities
#
###############

class AnalyticsService(ndb.Model):
    service_name = ndb.StringProperty(required=True)
    description = ndb.StringProperty()
    active = ndb.BooleanProperty(default=False)
    service_code = ndb.StringProperty(required=True)
    value = ndb.StringProperty()
    platform = ndb.StringProperty(repeated=True)

    @classmethod
    def get_services(cls):
        return cls.query().order(cls.service_name).fetch()

    @classmethod
    def get_service_status(cls,service_code):
        return cls.query(cls.service_code==service_code).fetch(limit=1)

    def to_entity(self, platform=''):
        e = {}
        e['id'] = self.key.id()
        e['name'] = self.service_name
        e['code'] = self.service_code
        e['value'] = self.value

        if self.active:
            e['status'] = 'active'
        else:
            e['status'] = 'inactive'

        if platform not in self.platform:
            e['status'] = 'inactive'

        return e


class AnalyticsServiceHistory(ndb.Model):
    datetime = ndb.DateTimeProperty(auto_now_add=True)
    state = ndb.BooleanProperty()
    platform = ndb.StringProperty(repeated=True)

    @classmethod
    def get_history(cls, service):
        return cls.query(ancestor=service.key).order(-cls.datetime).fetch(limit=15)


##########
#
# Models for Movie Title Bank (January 8, 2015)
#
##########

class RottenTomatoesDetails(ndb.Model):
    rt_title = ndb.StringProperty()
    rt_id = ndb.StringProperty()
    alternate_ids = ndb.JsonProperty() # keys: imdb(str)
    year = ndb.IntegerProperty()
    casts = ndb.JsonProperty() # keys: name(str), characters(list), id(str)
    release_dates = ndb.JsonProperty() # keys: theater(str), dvd(str)
    synopsis = ndb.TextProperty()
    runtime = ndb.IntegerProperty()
    ratings = ndb.JsonProperty() # keys: critics_score(int), audience_score(int), critics_rating(str), audience_rating(str)
    mpaa_rating = ndb.StringProperty()
    posters = ndb.JsonProperty() # keys: profile(str), detailed(str), thumbnail(str), original(str)


class SecondaryMovieTitles(ndb.Model):
    movie_title = ndb.StringProperty()
    correlation_title_version_one = ndb.StringProperty()
    correlation_title_version_two = ndb.StringProperty()
    correlation_title_version_three = ndb.StringProperty()
    correlation_title_version_four = ndb.StringProperty()


class MovieTitles(ndb.Model):
    title = ndb.StringProperty(required=True)
    correlation_title = ndb.StringProperty(repeated=True)
    searchable_title = ndb.StringProperty()
    searchable_words = ndb.StringProperty(repeated=True)
    rt_details = ndb.StructuredProperty(RottenTomatoesDetails)
    secondary_titles = ndb.StructuredProperty(SecondaryMovieTitles, repeated=True)
    is_title_locked = ndb.BooleanProperty(default=False)
    is_active = ndb.BooleanProperty(default=False)

    def to_entity_rottentomatoes(self):
        e = self.to_dict()
        rt_dict = {}

        if 'rt_details' in e:
            rt_dict = e['rt_details']

            if 'casts' in rt_dict:
                rt_dict['cast'] = rt_dict.pop('casts', None)

            if 'release_dates' in rt_dict:
                rt_dict['release_date'] = rt_dict.pop('release_dates', None)

            if 'runtime' in rt_dict:
                rt_dict['runtime_mins'] = rt_dict.pop('runtime', None)

            if 'mpaa_rating' in rt_dict:
                rt_dict['advisory_rating'] = rt_dict.pop('mpaa_rating', None)

        return rt_dict


class MovieTitlesLookup(ndb.Model):
    movie_title = ndb.StringProperty()
    correlation_title = ndb.StringProperty(repeated=True)
    date_created = ndb.DateProperty(auto_now_add=True)
    datetime_created = ndb.DateTimeProperty(auto_now_add=True)


###############
#
# Models for Uploading Posters and Assests Image
#
###############

class PosterImageBlob(ndb.Model):
    resolution = ndb.StringProperty(required=True)
    image = ndb.BlobKeyProperty(required=True)
    orientation = ndb.StringProperty()
    application_type = ndb.StringProperty()
    is_background = ndb.BooleanProperty(default=False)
    is_default = ndb.BooleanProperty(default=False)


class AssetsImageBlob(ndb.Model):
    name = ndb.StringProperty(required=True)
    image = ndb.BlobKeyProperty(required=True)
    assets_group = ndb.StringProperty()
    platform = ndb.StringProperty(repeated=True)
    is_active = ndb.BooleanProperty(default=False)


class MemberPasswordReset(ndb.Model):
    email = ndb.StringProperty()
    theater = ndb.KeyProperty(Theater)
    new_password = ndb.StringProperty()
    datetime_created = ndb.DateTimeProperty(auto_now_add=True)
    is_active = ndb.BooleanProperty(default=True)


class ConvenienceFeeSettings(ndb.Model):
    entity_class = ndb.StringProperty(required=True) # theaters / theaterorgs
    convenience_fees = ndb.PickleProperty()
    convenience_fees_message = ndb.PickleProperty()

    date_created = ndb.DateProperty(auto_now_add=True)
    is_active = ndb.BooleanProperty(default=True)

    @classmethod
    def get_settings(cls, key, entity_class):
        settings = cls.query(cls.entity_class==entity_class, ancestor=key).get()

        if settings:
            return settings

        parent_key = key.parent()

        if parent_key:
            return cls.get_settings(parent_key)
        else:
            settings = cls()
            settings.convenience_fees = {}
            settings.convenience_fees_message = {}

        return settings


###############
#
# GMovies Promo Models
#
###############

class Promo(ndb.Model):
    name = ndb.StringProperty(required=True)
    available_date = ndb.DateProperty(required=True)
    mechanics = ndb.TextProperty()

    # movie details
    movie = ndb.KeyProperty(Movie)

    # schedule details
    start_time = ndb.TimeProperty()
    end_time = ndb.TimeProperty()
    price = DecimalProperty()
    convenience_fees = ndb.JsonProperty()
    convenience_fees_message = ndb.JsonProperty()

    # theater details
    theater = ndb.StringProperty()
    theater_id = ndb.StringProperty()
    theater_code = ndb.StringProperty()
    theater_address = ndb.StringProperty()
    cinema = ndb.StringProperty()
    seat_map = ndb.PickleProperty()
    seat_count = ndb.IntegerProperty()
    seats_whitelist = ndb.StringProperty(repeated=True)
    seats_blacklist = ndb.StringProperty(repeated=True)
    seats_locked = ndb.StringProperty(repeated=True)
    seats_purchased = ndb.StringProperty(repeated=True)
    payment_options_ios = ndb.StringProperty(repeated=True)
    payment_options_android = ndb.StringProperty(repeated=True)
    payment_options_website = ndb.StringProperty(repeated=True)
    allowed_seats = ndb.IntegerProperty()

    ticket_template = ndb.StringProperty(indexed=False)
    is_qrcode = ndb.BooleanProperty(default=False)
    is_block_screening = ndb.BooleanProperty(default=False)
    is_featured = ndb.BooleanProperty(default=False)
    is_active = ndb.BooleanProperty(default=False)
    activate_requery = ndb.BooleanProperty(default=False)
    platform = ndb.StringProperty(repeated=True)
    image_urls = ndb.JsonProperty()

    whitelist = ['resolution', 'application_type']

    def get_theater_logo(self):
        theater_logo = ''

        try:
            cache_key = "promo::assets::theater_logo::%s" % (self.key.id())
            cache_hit = memcache.get(cache_key)

            if cache_hit:
                log.debug("Promo, get_theater_logo, %s logo cache hit on, %s..." % (self.key.id(), cache_key))
                log.info("Promo, get_theater_logo, %s logo cache hit on, %s..." % (self.key.id(), cache_key))

                return cache_hit

            image_blob = AssetsImageBlob.query(AssetsImageBlob.assets_group=='promos-theater-logo', ancestor=self.key).get()

            if image_blob and image_blob.image:
                theater_logo = get_serving_url(image_blob.image)

                memcache.set(cache_key, theater_logo)
        except Exception, e:
            log.warn("ERROR! Promo, get_theater_logo, %s failed to get logo..." % self.key.id())
            log.error(e)

        return theater_logo

    def get_landscape_poster_url(self, posters_args={}):
        poster = ''
        resolution = posters_args.get('resolution', 'uploaded')
        application_type = posters_args.get('application_type', 'mobile-app')

        try:
            cache_key = "promo::poster::%s::landscape::%s::%s" % (self.key.id(), resolution, application_type)
            cache_hit = memcache.get(cache_key)

            if cache_hit:
                log.debug("Promo, get_landscape_poster_url, %s poster cache hit on, %s..." % (self.key.id(), cache_key))
                log.info("Promo, get_landscape_poster_url, %s poster cache hit on, %s..." % (self.key.id(), cache_key))

                return cache_hit

            poster_image_blob = PosterImageBlob.query(PosterImageBlob.resolution==resolution, PosterImageBlob.orientation=='landscape',
                    PosterImageBlob.application_type==application_type, PosterImageBlob.is_background==False, ancestor=self.key).get()

            if poster_image_blob and poster_image_blob.image:
                poster = get_serving_url(poster_image_blob.image)

                memcache.set(cache_key, poster)
        except Exception, e:
            log.warn("ERROR!, Promo, get_landscape_poster_url, %s failed to get poster..." % self.key.id())
            log.error(e)

        return poster

    def get_portrait_poster_url(self, posters_args={}):
        poster = ''
        resolution = posters_args.get('resolution', 'uploaded')
        application_type = posters_args.get('application_type', 'mobile-app')

        try:
            cache_key = "promo::poster::%s::portrait::%s::%s" % (self.key.id(), resolution, application_type)
            cache_hit = memcache.get(cache_key)

            if cache_hit:
                log.debug("Promo, get_portrait_poster_url, %s poster cache hit on, %s..." % (self.key.id(), cache_key))
                log.info("Promo, get_portrait_poster_url, %s poster cache hit on, %s..." % (self.key.id(), cache_key))

                return cache_hit

            poster_image_blob = PosterImageBlob.query(PosterImageBlob.resolution==resolution, PosterImageBlob.orientation=='portrait',
                    PosterImageBlob.application_type==application_type, PosterImageBlob.is_background==False, ancestor=self.key).get()

            if poster_image_blob and poster_image_blob.image:
                poster = get_serving_url(poster_image_blob.image)

                memcache.set(cache_key, poster)
        except Exception, e:
            log.warn("ERROR, Promo, get_portrait_poster_url, %s failed to get poster..." % self.key.id())
            log.error(e)

        return poster

    def to_entity(self, posters_args={}):
        e = self.to_dict()
        e.pop('allowed_seats', None)
        e.pop('cinema', None)
        e.pop('movie', None)
        e.pop('is_block_screening', None)
        e.pop('is_qrcode', None)
        e.pop('seat_count', None)
        e.pop('seat_map', None)
        e.pop('seats_whitelist', None)
        e.pop('seats_blacklist', None)
        e.pop('seats_locked', None)
        e.pop('seats_purchased', None)
        e.pop('theater', None)
        e.pop('theater_id', None)
        e.pop('theater_code', None)
        e.pop('theater_address', None)
        e.pop('ticket_template', None)
        e.pop('payment_options_ios', None)
        e.pop('payment_options_android', None)
        e.pop('payment_options_website', None)
        e.pop('activate_requery', None)
        e.pop('platform', None)
        e.pop('image_urls', None)

        movie_dict = {}
        theater_dict = {}
        org_uuid = None
        landscape_poster = self.get_landscape_poster_url(posters_args=posters_args)

        if e['available_date']:
            e['available_date'] = e['available_date'].strftime(DATE_FORMAT)
        else:
            e['available_date'] = ''

        if e['start_time']:
            e['start_time'] = e['start_time'].strftime(TIME_FORMAT)
        else:
            e['start_time'] = ''

        if e['end_time']:
            e['end_time'] = e['end_time'].strftime(TIME_FORMAT)
        else:
            e['end_time'] = ''

        if e['price']:
            e['price'] = str(e['price'])
        else:
            e['price'] = '0.00'

        if not e['mechanics']:
            e['mechanics'] = ''

        if not e['convenience_fees']:
            e['convenience_fees'] = {}

        if not e['convenience_fees_message']:
            e['convenience_fees_message'] = {}

        if self.theater:
            if self.theater_id:
                org_uuid, theater_id = decoded_theater_id(self.theater_id)

            if org_uuid is None:
                org_uuid = ''

            theater_dict['name'] = self.theater
            theater_dict['code'] = self.theater_code
            theater_dict['address'] = self.theater_address
            theater_dict['cinema'] = self.cinema
            theater_dict['theaterorg_id'] = org_uuid
            theater_dict['theater_id'] = self.theater_id
            theater_dict['theater_logo'] = self.get_theater_logo()

        if self.movie:
            movie = self.movie.get()

            if movie:
                movie_dict['movie_id'] = movie.key.id()
                movie_dict['canonical_title'] = movie.canonical_title

                if movie.advisory_rating:
                    movie_dict['advisory_rating'] = movie.advisory_rating
                else:
                    movie_dict['advisory_rating'] = ''

                if movie.runtime_mins:
                    movie_dict['runtime_mins'] = movie.runtime_mins
                else:
                    movie_dict['runtime_mins'] = 0

        e['poster'] = landscape_poster
        e['poster_portrait'] = ''
        e['poster_landscape'] = landscape_poster
        e['movie'] = movie_dict
        e['theater'] = theater_dict

        return e

    def to_entity2(self, posters_args={}):
        e = self.to_dict()
        e.pop('allowed_seats', None)
        e.pop('cinema', None)
        e.pop('convenience_fees', None)
        e.pop('convenience_fees_message', None)
        e.pop('movie', None)
        e.pop('is_block_screening', None)
        e.pop('is_qrcode', None)
        e.pop('seat_count', None)
        e.pop('seat_map', None)
        e.pop('seats_whitelist', None)
        e.pop('seats_blacklist', None)
        e.pop('seats_locked', None)
        e.pop('seats_purchased', None)
        e.pop('theater', None)
        e.pop('theater_id', None)
        e.pop('theater_code', None)
        e.pop('theater_address', None)
        e.pop('ticket_template', None)
        e.pop('payment_options_ios', None)
        e.pop('payment_options_android', None)
        e.pop('payment_options_website', None)
        e.pop('activate_requery', None)
        e.pop('image_urls', None)

        org_uuid = None
        movie = self.movie.get() if self.movie else None

        if self.theater_id:
            org_uuid, dt_id = decoded_theater_id(self.theater_id)

        movie_dict = {}
        movie_dict['movie_id'] = movie.key.id() if movie else ''
        movie_dict['canonical_title'] = movie.canonical_title if movie and movie.canonical_title else ''
        movie_dict['link_name'] = '-'.join(movie.canonical_title.lower().split(' ')) if movie and movie.canonical_title else ''
        movie_dict['advisory_rating'] = movie.advisory_rating if movie and movie.advisory_rating else ''
        movie_dict['runtime_mins'] = movie.runtime_mins if movie and movie.runtime_mins else 0

        theater_dict = {}
        theater_dict['name'] = self.theater if self.theater else ''
        theater_dict['code'] = self.theater_code if self.theater_code else ''
        theater_dict['address'] = self.theater_address if self.theater_address else ''
        theater_dict['cinema'] = self.cinema if self.cinema else ''
        theater_dict['link_name'] = '-'.join(self.theater.lower().split(' ')) if self.theater else ''
        theater_dict['theaterorg_id'] = org_uuid if self.theater_id else ''
        theater_dict['theater_id'] = self.theater_id if self.theater_id else ''
        theater_dict['theater_logo'] = self.get_theater_logo()

        e['available_date'] = e['available_date'].strftime(DATE_FORMAT) if e['available_date'] else ''
        e['start_time'] = e['start_time'].strftime(TIME_FORMAT) if e['start_time'] else ''
        e['end_time'] = e['end_time'].strftime(TIME_FORMAT) if e['end_time'] else ''
        e['price'] = str(e['price']) if e['price'] else '0.00'
        e['mechanics'] = e['mechanics'] if e['mechanics'] else ''
        e['movie'] = movie_dict
        e['theater'] = theater_dict
        e['poster'] = self.get_portrait_poster_url(posters_args=posters_args)
        e['poster_landscape'] = self.get_landscape_poster_url(posters_args=posters_args)

        return e


class PromoTicketData(ndb.Model):
    ref = ndb.StringProperty()
    code = ndb.StringProperty()
    date = ndb.DateProperty()
    extra = ndb.JsonProperty()
    barcode = ndb.BlobProperty()
    qrcode = ndb.BlobProperty()
    image = ndb.BlobProperty()

    def to_entity(self):
        e = self.to_dict()
        e.pop('barcode', None)
        e.pop('qrcode', None)

        if e['date']:
            e['date'] = e['date'].strftime(DATETIME_FORMAT)

        if e['image']:
            e['image'] = e['image'].encode('base64')

        return e

    def to_entity2(self):
        e = self.to_dict()
        e.pop('image', None)
        e.pop('barcode', None)
        e.pop('qrcode', None)

        ticket_type = 'barcode'

        e['date'] = e['date'].strftime(DATE_FORMAT) if e['date'] else ''
        e['transaction_codes'] = [e['code']] if e['code'] else []

        if e['extra']:
            e['extra']['runtime'] = e['extra']['movies_runtime_mins'] if e['extra']['movies_runtime_mins'] else ''
            e['extra']['promo_id'] = e['extra']['promo_id'].replace('@promo', '').strip() if e['extra']['promo_id'] else ''
            e['extra']['seating_type'] = 'Reserved Seating' # default seating_type since it is a promo transaction.

            if 'ticket_ui_version' in e['extra'] and e['extra']['ticket_ui_version'] == '2':
                ticket_type = 'qrcode'

            e['extra']['ticket_type'] = ticket_type

            e['extra'].pop('available_date', None)
            e['extra'].pop('start_time', None)
            e['extra'].pop('end_time', None)
            e['extra'].pop('is_blocked_screening', None)
            e['extra'].pop('is_multiple_tickets', None)
            e['extra'].pop('movie_advisory_rating', None)
            e['extra'].pop('movie_title', None)
            e['extra'].pop('movies_runtime_mins', None)
            e['extra'].pop('original_seat_price', None)
            e['extra'].pop('price_per_seat', None)
            e['extra'].pop('theater_and_cinema_name', None)
            e['extra'].pop('ticket_ui_version', None)
            e['extra'].pop('total_seat_discount', None)
            e['extra'].pop('total_seat_price', None)

        if ticket_type == 'barcode':
            e['qrcodes'] = [self.barcode.encode('base64')] if self.barcode else []
        else:
            e['qrcodes'] = [self.qrcode.encode('base64')] if self.qrcode else []

        return e

    def to_entity_minimize(self):
        e = self.to_dict()
        e.pop('ref', None)
        e.pop('code', None)
        e.pop('date', None)
        e.pop('image', None)
        e.pop('barcode', None)
        e.pop('qrcode', None)

        if e['extra']:
            e['extra']['runtime'] = e['extra']['movies_runtime_mins'] if e['extra']['movies_runtime_mins'] else ''
            e['extra']['promo_id'] = e['extra']['promo_id'].replace('@promo', '').strip() if e['extra']['promo_id'] else ''
            e['extra']['transaction_codes'] = [self.code] if self.code else []
            e['extra']['seating_type'] = 'Reserved Seating' # default seating_type since it is a promo transaction.

            e['extra'].pop('available_date', None)
            e['extra'].pop('start_time', None)
            e['extra'].pop('end_time', None)
            e['extra'].pop('is_blocked_screening', None)
            e['extra'].pop('is_multiple_tickets', None)
            e['extra'].pop('movie_advisory_rating', None)
            e['extra'].pop('movie_title', None)
            e['extra'].pop('movies_runtime_mins', None)
            e['extra'].pop('original_seat_price', None)
            e['extra'].pop('price_per_seat', None)
            e['extra'].pop('theater_and_cinema_name', None)
            e['extra'].pop('ticket_ui_version', None)
            e['extra'].pop('total_seat_discount', None)
            e['extra'].pop('total_seat_price', None)

        return e['extra']


class PromoReservationInfo(ndb.Model):
    promo = ndb.KeyProperty(Promo)
    seat_id = ndb.StringProperty()

    def to_entity(self):
        e = self.to_dict()
        e.pop('seat_id', None)

        e['promo'] = e['promo'].id()
        e['seat'] = self.seat_id

        return e


class PromoReservationTransaction(ndb.Model):
    allowed_payment_types = make_enum('payment_types', PAYNAMICS_PAYMENT='paynamics-payment-initiated', PROMO_CODE='gmovies-promo')

    reservations = ndb.StructuredProperty(PromoReservationInfo, repeated=True)
    ticket = ndb.StructuredProperty(PromoTicketData)
    payment_type = ndb.StringProperty(required=True)
    payment_info = ndb.JsonProperty(required=True)
    reservation_reference = ndb.StringProperty()
    payment_reference = ndb.StringProperty()
    platform = ndb.StringProperty()
    workspace = ndb.PickleProperty()
    state = ndb.IntegerProperty(required=True)
    ver = ndb.IntegerProperty(default=1)
    was_reaped = ndb.BooleanProperty(default=False)
    was_requery = ndb.BooleanProperty(default=False)
    is_verified = ndb.BooleanProperty(default=False)
    is_claimed = ndb.BooleanProperty(default=False)
    date_created = ndb.DateTimeProperty(auto_now_add=True)
    date_verified = ndb.DateTimeProperty()
    date_claimed = ndb.DateTimeProperty()

    @classmethod
    def generate_tx_id(cls):
        return str(uuid_gen())

    @classmethod
    def create_transaction(cls, device_id, tx_info):
        tx_id = cls.generate_tx_id()
        tx = cls(key=ndb.Key(Device, device_id, cls, tx_id))
        tx.copy_api_entity(tx_info)
        tx.workspace = {}

        return tx

    @classmethod
    def generate_reservation_reference(cls, timestamp):
        while(True):
            reservation_reference = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(5))
            # reservation_reference += str(timestamp)
            queryset = cls.query(cls.reservation_reference==reservation_reference)

            if queryset.get() == None:
                counter = 1
                cached_reservation_reference = memcache.get('reservation_reference::%s::count' % reservation_reference)

                if cached_reservation_reference:
                    counter = cached_reservation_reference + 1

                memcache.set('reservation_reference::%s::count' % reservation_reference, counter)
                break

        return reservation_reference

    def check_transaction_information(self, tx_info):
        try:
            payment = tx_info['payment']
            payment_type = payment.get('type', 'gmovies-promo')

            if payment_type == PromoReservationTransaction.allowed_payment_types.PROMO_CODE:
                payment['promo_code']
            elif payment_type == PromoReservationTransaction.allowed_payment_types.PAYNAMICS_PAYMENT:
                payment['email']
                payment['fname']
                payment['lname']
                payment['mname']
                payment['address1']
                payment['address2']
                payment['city']
                payment['state']
                payment['country']
                payment['zip']
                payment['phone']
                payment['mobile']
            else:
                raise BadValueException('type', "Unknown payment type: %s" % payment_type)
        except KeyError, e:
            raise BadValueException(e[0], "Missing %s params for payment" % e[0])

        try:
            reservations = tx_info['reservations']

            for reservation in reservations:
                reservation['promo']
                reservation['seat']
        except KeyError, e:
            raise BadValueException(e, "Missing %s params for reservation" % e)

    def copy_api_entity(self, tx_info):
        self.check_transaction_information(tx_info)

        if 'reservations' in tx_info:
            self.reservations = [self.__build_reservation(r) for r in tx_info['reservations']]

        payment_info = dict(tx_info['payment'])
        self.payment_type = payment_info.pop('type', 'gmovies-promo')
        self.payment_info = payment_info
        self.platform = tx_info.pop('platform', None)

    @ndb.non_transactional
    def __build_reservation(self, r):
        if 'promo' not in r:
            raise BadValueException('reservations', "Missing key promo ID...")

        promo_uuid = r['promo'].replace('@promo', '').strip()
        promo_key = ndb.Key(Promo, promo_uuid)

        if not promo_key.get():
            raise BadValueException('reservations', "Not Found Promo...")

        reservation_info = PromoReservationInfo()
        reservation_info.promo = promo_key
        reservation_info.seat_id = r['seat']

        return reservation_info

    @ndb.non_transactional
    def _do_lock_seats(self):
        promo_key = ndb.Key(Promo, self.workspace['promos.promo_id'][0])
        promo = promo_key.get()

        if not promo:
            return 'error', 'Promo not found.'

        if not self.workspace['seats']:
            return 'error', 'Please select seats.'

        seats_taken = list(set(promo.seats_purchased + promo.seats_locked + promo.seats_blacklist))
        seats_conflicts = list(set(seats_taken) & set(self.workspace['seats']))

        if seats_conflicts:
            return 'error', 'The seat is not available and is already taken.'

        self.workspace['seats::locked'] = self.workspace['seats']
        promo.seats_locked.extend(self.workspace['seats'])
        promo.put()

        return 'success', None

    @ndb.non_transactional
    def _do_reserve_seats(self):
        promo_key = ndb.Key(Promo, self.workspace['promos.promo_id'][0])
        promo = promo_key.get()
        promo.seats_purchased.extend(self.workspace['seats::locked'])
        promo.seats_locked = list(set(promo.seats_locked) - set(self.workspace['seats::locked']))
        promo.put()

    @ndb.non_transactional
    def _do_free_seats(self):
        promo_key = ndb.Key(Promo, self.workspace['promos.promo_id'][0])
        promo = promo_key.get()
        promo.seats_locked = list(set(promo.seats_locked) - set(self.workspace['seats::locked']))
        promo.put()

    def get_queue(self):
        queue_id = self.workspace.get('queue_id', None)

        return "txpromoqueue%d" % queue_id if queue_id else 'transactionspromo'

    def get_requery_queue(self):
        queue_id = self.workspace.get('queue_id', None)

        return "txpromorequeryqueue%d" % queue_id if queue_id else 'txpromorequeryqueue'

    def total_amount(self, reservation_fee, use_workspace=False):
        if not use_workspace:
            promos = ndb.get_multi([r.promo for r in self.reservations])

            return sum([(promo.price+Decimal(reservation_fee)) for promo in promos])
        else:
            return sum([price+Decimal(reservation_fee) for price in self.workspace['promos.prices']])

    def total_seat_price(self, reservation_fee, use_workspace=False):
        if not use_workspace:
            promo = self.reservations[0].promo.get()

            return promo.price + Decimal(reservation_fee)
        else:
            return self.workspace['promos.prices'][0] + Decimal(reservation_fee)

    def reservation_count(self):
        return len(self.reservations)

    def to_entity(self):
        ticket_ent = self.ticket.to_entity() if self.ticket else {}
        date_created = self.date_created.strftime(DATETIME_FORMAT) if self.date_created else None

        return {'payment_info': self.payment_info, 'reservations': [r.to_entity() for r in self.reservations],
                'ticket': ticket_ent, 'date_created': date_created}

    def to_entity2(self):
        ticket_ent = self.ticket.to_entity2() if self.ticket else {}
        date_created = self.date_created.strftime(DATETIME_FORMAT) if self.date_created else ''

        return {'payment_info': self.payment_info, 'reservations': [r.to_entity() for r in self.reservations],
                'ticket': ticket_ent, 'date_created': date_created, 'discount_info': {}, 'user_info': {}}

    def to_entity_minimize(self):
        ticket_entity = self.ticket.to_entity_minimize() if self.ticket else {}

        return ticket_entity

    bind_cancellation_callback, trigger_cancellation = _handle_callbacks('__cancellation_callbacks')
    bind_update_callback, trigger_update = _handle_callbacks('__update_callbacks')

class OtpRandom(ndb.Model):
    code = ndb.IntegerProperty(default=1234)

class OtpNumber(ndb.Model):
    number = ndb.StringProperty(default='639065601556')
