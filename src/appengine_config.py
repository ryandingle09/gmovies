appstats_CALC_RPC_COSTS = True
appstats_MAX_STACK = 15

def webapp_add_wsgi_middleware(app):
    from google.appengine.ext.appstats import recording
    app = recording.appstats_wsgi_middleware(app)
    return app


def appstats_normalize_path(path):
    from gmovies import app
    import os
    adapt = app.url_map.bind('globe-gmovies.appspot.com')
    try:
        return adapt.match(path, 'GET', return_rule=True)[0].rule
    except:
        return path
